* -------------------------------------
*   documentation des process ctenat
* -------------------------------------

* -------------------------------------
* admin
* -------------------------------------


[[~/d7/modules/co2/views/admin/index.php]]
  driven by 
    [[~/d7/modules/co2/assets/js/admin/panel.js]]
    [[~/d7/modules/co2/assets/js/admin/directory.js]]
  sets adminPanel
  overloads admin.js 
    array_push($cssJsCostum, '/js/'.$slugContext.'/admin.js');

[[~/d7/modules/costum/assets/js/ctenat/admin.js]]
all views available 
  adminPanel.views.importctenat
  adminPanel.views.indicators
  adminPanel.views.actionsCTE
  adminPanel.views.candidatures
  adminPanel.views.territory
  adminPanel.views.adminambitions
  adminPanel.views.strategyCter
adminCter
/home/oceatoon/d7/modules/costum/assets/js/ctenat/pageProfil.js

* -------------------------------------
* fiches actions
* -------------------------------------
  ** candiature 
    :view: [[~/d7/modules/survey/views/custom/ctenat/dossierEditCTENat.php]]
  ** changement de status d'une action d'un cter 
    [[~/d7/modules/costum/controllers/actions/ctenat/admin/PrioAction.php]] 
      Ctenat::updatePrioAnswers
      all states Ctenat.php
      - modifie l'attribut :priorisation: dans :answers:
  ** export des ACTIONs nationales
    {{https://cte.ecologique-solidaire.gouv.fr/costum/ctenat/answerscsv/slug/ctenat/admin/true}} 
    * process 
        [[~/d7/modules/costum/controllers/actions/ctenat/api/AnswerscsvAction.php]] 
        - get all projects.CTER
        - can use search to filter results 

* -------------------------------------
* CTER home 
* -------------------------------------
  * petit chiffre de homepage  
  [[~/d7/modules/costum/views/custom/ctenat/cterrHPChiffres.php ]] 
    million : [[~/d7/modules/costum/views/custom/ctenat/graph/pieMillionCTErr.php]]
  ** onglet
    *** /home/oceatoon/d7/modules/costum/assets/js/ctenat/pageProfil.js
    *** en chiffre
    {{http://127.0.0.1/ph/costum/ctenat/dashboard/slug/cteDuGrandArrasSigneLe11Octobre2018}}
      [[~/d7/modules/costum/views/custom/ctenat/dashboard.php ]]
      {{http://127.0.0.1/ph/graph/co/dash/g/costum.views.custom.ctenat.graph.barActionsCTErr/id/barActionsCTErr}}
        [[~/d7/modules/graph/views/co/bar.php]]
      {{http://127.0.0.1/ph/graph/co/dash/g/costum.views.custom.ctenat.graph.pieMany/id/pieMany}}
        [[~/d7/modules/costum/views/custom/ctenat/graph/pieMany.php ]]

* -------------------------------------
* DASHBOARD
* -------------------------------------
:ajax:{{http://127.0.0.1/ph/costum/ctenat/dashboard/url/costum.views.custom.ctenat.dashboard}}
    :view:[[~/d7/modules/costum/views/custom/ctenat/dashboard.php]]
      :ajax:{{http://127.0.0.1/ph/graph/co/dash/g/costum.views.custom.ctenat.graph.lineCTE/size/S/id/lineCTE}}
      :ajax:{{http://127.0.0.1/ph/graph/co/dash/g/costum.views.custom.ctenat.graph.barPorteurbyDomaine/size/S/id/barPorteurbyDomaine}}
        :view:[[~/d7/modules/costum/views/custom/ctenat/graph/barPorteurbyDomaine.php]]
      :ajax:{{http://127.0.0.1/ph/graph/co/dash/g/costum.views.custom.ctenat.graph.pieFinance/size/S/id/pieFinance}}
        :view:[[~/d7/modules/graph/views/co/piePorteurbyDomaine.php]]
      
* -------------------------------------
* PDF
* -------------------------------------
  ** copil 
  [[~/d7/modules/costum/controllers/actions/ctenat/pdf/GenerateCopilAction.php]]
  ** fiche action 
  [[~/d7/modules/citizenToolKit/controllers/export/PdfElementAction.php]] 
  ** orientation
  [[~/d7/modules/costum/controllers/actions/ctenat/pdf/GenerateOrientationAction.php]] 


* -------------------------------------
* Badge 
* -------------------------------------
** on peut créer un badge dans l'admin globale
[[http://communecter.org/costum/co/index/slug/ctenat#admin.view.badges]]
collection 
attr category : cibleDD, domainAction, strategy
db.getCollection('badges').find({category:"domainAction"})

** puis les territoires peuvent s'octroyer des badges 
parent.idxxxx = {type:projects , name : xxxx}


