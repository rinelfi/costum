<?php

class CmcasHauteBretagne {
	const COLLECTION = "costums";
	const CONTROLLER = "costum";
	const MODULE = "costum";
	
	public static function getEvents($params){

        $results["result"] = false;

        date_default_timezone_set('UTC');

        $startDate = strtotime(date("Y-m-d H:i"));

        $where = array(
            "source.key" => $params,
            "startDate"   =>  array('$gte'   =>  new MongoDate($startDate))
        );

        $event = PHDB::find(Event::COLLECTION,$where);

        if (!empty($event)) {
            $res = array();

            $results["result"] = true;

            $res = self::createResults($event);

            return array_merge($results,$res);
        }
        return $results;
    }

    private static function createResults($event){

        $data["element"] = array();

        foreach ($event as $key => $value) {

            $description = (!empty($value["shortDescription"])) ? $value["shortDescription"] : "Veuillez ajouter une courte description";
            $email = (!empty($value["email"])) ? $value["email"] : "";
            $date_start = date(DateTime::ISO8601, $value["startDate"]->sec);
            $address = (!empty($value["address"]["addressLocality"])) ? $value["address"]["addressLocality"] : "";
            $startDay = (!empty(date('l',strtotime($date_start)))) ? date('l',strtotime($date_start)) : "";
            $startMonth = (!empty(date('F',strtotime($date_start)))) ? date('F',strtotime($date_start)) : "";
            $nbrAttendees = count(@$value["links"]["attendees"]);
            $phone = (!empty($value["phone"])) ? $value["phone"] : "";


            $data["element"][] = [
                "id"            =>  (string) $value["_id"],
                "description"   =>  $description,
                "slug"          =>  $value["slug"],
                "name"          =>  $value["name"],
                "startDate"     =>  date('d',strtotime($date_start)),
                "startMonth"    =>  Yii::t("translate",$startMonth),
                "startHour"     =>  $date_start,
                "startDay"      =>  Yii::t("translate",$startDay),
                "address"       =>  $address,
                "email"         =>  $email,
                "phone"         =>  $phone,
                "nbrattendees"  =>  $nbrAttendees,
                "attendees"     =>  @$value["links"]["attendees"],
                "collection"    =>  $value["collection"]
            ];
        }
        // var_dump($data);exit;
        return $data;
    }

    public static function getArticles($params){

        $results["dataArticles"] = false;

        $where = array(
            "source.key" => $params,
            "type"   =>  "article"
        );
        
        $article = Poi::getPoiByWhereSortAndLimit($where,array("updated" => -1),4);
        // var_dump($article);exit;
        if (!empty($article)) {
            $results["dataArticles"] = true;

            $resArticles = array();

            $resArticles = self::createResultsArticle($article);

            return array_merge($results,$resArticles);
        }
        return $results;
    }

    private static function createResultsArticle($article){
        $data["article"] = array();

        foreach ($article as $key => $value) {
            $dCreate = date('d',$value["created"]);
            $mCreate = date('F',$value["created"]);
            $yCreate = date('Y',$value["created"]);
            $imgMedium = (!empty($value["profilMediumImageUrl"])) ? $value["profilMediumImageUrl"] : "Aucune image pour cette actus"; 

            $data["article"][] = [
                "id"            =>  (string) $value["_id"],
                "name"          =>  $value["name"],
                "collection"    =>  $value["collection"],
                "dCreate"       =>  $dCreate,
                "mCreate"       =>  Yii::t("translate",$mCreate),
                "yCreate"       =>  $yCreate,
                "imgMedium"     =>  $imgMedium
            ]; 
        }
        return $data;
    }

    public static function updateBlock($params){
        $block = $params["block"];
        $collection = $params["typeElement"];
        $id = $params["id"];
        $res = array();
        try {

            if($block == "info"){
                if(isset($params["name"])){
                    $res[] = Element::updateField($collection, $id, "name", $params["name"]);
                    /*PHDB::update( $collection,  array("_id" => new MongoId($id)), 
                                                    array('$unset' => array("hasRC"=>"") ));*/
                }
                if(isset($params["username"]) && $collection == Person::COLLECTION)
                    $msgError = Yii::t("common","Username cannot be changed.");
                    //$res[] = Element::updateField($collection, $id, "username", $params["username"]);
                if(isset($params["avancement"]) && $collection == Project::COLLECTION)
                    $res[] = Element::updateField($collection, $id, "avancement", $params["avancement"]);
                if(isset($params["tags"]))
                    $res[] = Element::updateField($collection, $id, "tags", $params["tags"]);
                if(isset($params["type"])  && ( $collection == Event::COLLECTION || $collection == Organization::COLLECTION) )
                    $res[] = Element::updateField($collection, $id, "type", $params["type"]);
                if(isset($params["thematique"])  &&  $collection == Event::COLLECTION )
                    PHDB::update( $collection, array("_id" => new MongoId($params["id"])), 
                                                        array('$set' => array("thematique" => $params["thematique"])));
                    // $res[] = Element::updateField($collection, $id, "thematique", $params["thematique"]);
                if(isset($params["phone"])  &&  $collection == Event::COLLECTION )
                    PHDB::update( $collection, array("_id" => new MongoId($params["id"])), 
                                                        array('$set' => array("phone" => $params["phone"])));
                    // $res[] = Element::updateField($collection, $id, "phone", $params["phone"]);
                if(isset($params["email"])){
                    if(!empty($params["email"])){
                        $mail = Mail::authorizationMail($params["email"]);
                        if($mail == false){
                            unset($params["email"]);
                            throw new CTKException("Vous ne pouvez pas renseigner cette adresse mail.");
                        }
                        
                    }

                    $resEmail=Element::updateField($collection, $id, "email", $params["email"]);
                    $res[] = $resEmail;
                    // Mail reference inivite on communecter
                    if($resEmail["result"] && in_array($collection,[Organization::COLLECTION,Project::COLLECTION,Event::COLLECTION])){
                        if(@$params["email"] && !empty($params["email"]) && $params["email"]!=@Yii::app()->session["userEmail"]){
                            Mail::referenceEmailInElement($collection, $id, $params["email"]);
                        }
                    }
                }

                if(isset($params["slug"])){
                    $el = PHDB::findOne($collection,array("_id"=>new MongoId($id)));
                    $oldslug = @$el["slug"];
                    if(!empty(Slug::getByTypeAndId($collection,$id)))
                        Slug::update($collection,$id,$params["slug"]);
                    else
                        Slug::save($collection,$id,$params["slug"]);
                    $res[] = Element::updateField($collection, $id, "slug", $params["slug"]);
                }
                //update RC channel name if exist
                if(@$el["hasRC"]){
                    RocketChat::rename( $oldslug, $params["slug"], @$el["preferences"]["isOpenEdition"] );
                }
                if(isset($params["url"]))
                    $res[] = Element::updateField($collection, $id, "url", Element::getAndCheckUrl($params["url"]));
                if(isset($params["birthDate"]) && $collection == Person::COLLECTION)
                    $res[] = Element::updateField($collection, $id, "birthDate", $params["birthDate"]);
                if(isset($params["fixe"]))
                    $res[] = Element::updateField($collection, $id, "fixe", $params["fixe"]);
                if(isset($params["fax"]))
                    $res[] = Element::updateField($collection, $id, "fax", $params["fax"]);
                if(isset($params["mobile"]))
                    $res[] = Element::updateField($collection, $id, "mobile", $params["mobile"]);
                
                if( !empty($params["parentId"]) ){
                    $parent["parentId"] = $params["parentId"] ;
                    $parent["parentType"] = ( !empty($params["parentType"]) ? $params["parentType"] : "dontKnow" ) ;
                    $resParent = Element::updateField($collection, $id, "parent", $parent);
                    if($parent["parentType"] != "dontKnow" && $parent["parentId"] != "dontKnow")
                        $resParent["value"]["parent"] = Element::getByTypeAndId( $params["parentType"], $params["parentId"]);
                    $res[] = $resParent;
                }
                if( !empty($params["parent"]) ){
                    //$parent["parentId"] = $params["parentId"] ;
                    //$parent["parentType"] = ( !empty($params["parentType"]) ? $params["parentType"] : "dontKnow" ) ;
                    $resParent = Element::updateField($collection, $id, "parent", $params["parent"]);
                    foreach($resParent["value"] as $key => $value) {
                        //var_dump($value["type"]); exit;
                        $elt=Element::getElementById($key, $value["type"], null, array("name", "slug","profilThumbImageUrl"));
                        $resParent["value"][$key]=array_merge($resParent["value"][$key], $elt);
                    }
                    $res[] = $resParent;
                }
                if(!empty($params["organizerId"]) ){
                    $organizer["organizerId"] = $params["organizerId"] ;
                    $organizer["organizerType"] = ( !empty($params["organizerType"]) ? $params["organizerType"] : "dontKnow" ) ;
                    $resOrg = Element::updateField($collection, $id, "organizer", $organizer);

                    if($params["organizerType"]!="dontKnow" && $params["organizerId"] != "dontKnow"){
                        $resOrg["value"]["organizer"] = Element::getByTypeAndId( $params["organizerType"], $params["organizerId"]);
                    }
                    $res[] = $resOrg;
                }
                if( !empty($params["organizer"]) ){
                    //$parent["parentId"] = $params["parentId"] ;
                    //$parent["parentType"] = ( !empty($params["parentType"]) ? $params["parentType"] : "dontKnow" ) ;
                    $resOrg = Element::updateField($collection, $id, "organizer", $params["organizer"]);
                    foreach($resOrg["value"] as $key => $value) {
                        $elt=Element::getElementById($key, $value["type"], null, array("name", "slug","profilThumbImageUrl"));
                        $resOrg["value"][$key]=array_merge($resOrg["value"][$key], $elt);
                    }
                    $res[] = $resOrg;
                }

            }else if($block == "network"){
                if(isset($params["telegram"]) && $collection == Person::COLLECTION)
                    $res[] = Element::updateField($collection, $id, "telegram", $params["telegram"]);
                if(isset($params["facebook"]))
                    $res[] = Element::updateField($collection, $id, "facebook", Element::getAndCheckUrl($params["facebook"]));
                if(isset($params["twitter"]))
                    $res[] = Element::updateField($collection, $id, "twitter", Element::getAndCheckUrl($params["twitter"]));
                if(isset($params["github"]))
                    $res[] = Element::updateField($collection, $id, "github", Element::getAndCheckUrl($params["github"]));
                if(isset($params["gpplus"]))
                    $res[] = Element::updateField($collection, $id, "gpplus", Element::getAndCheckUrl($params["gpplus"]));
                if(isset($params["skype"]))
                    $res[] = Element::updateField($collection, $id, "skype", Element::getAndCheckUrl($params["skype"]));
                if(isset($params["diaspora"]))
                    $res[] = Element::updateField($collection, $id, "diaspora", Element::getAndCheckUrl($params["diaspora"]));
                if(isset($params["mastodon"]))
                    $res[] = Element::updateField($collection, $id, "mastodon", Element::getAndCheckUrl($params["mastodon"]));
                if(isset($params["instagram"]))
                    $res[] = Element::updateField($collection, $id, "instagram", Element::getAndCheckUrl($params["instagram"]));

            }else if( $block == "when" && ( $collection == Event::COLLECTION || $collection == Project::COLLECTION) ) {
                
                if(isset($params["allDayHidden"]) && $collection == Event::COLLECTION)
                    $res[] = Element::updateField($collection, $id, "allDay", (($params["allDayHidden"] == "true") ? true : false));
                if(isset($params["startDate"]))
                    $res[] = Element::updateField($collection, $id, "startDate", $params["startDate"],@$params["allDay"]);
                if(isset($params["endDate"]))
                    $res[] = Element::updateField($collection, $id, "endDate", $params["endDate"],@$params["allDay"]);
            
            }else if($block == "toMarkdown"){

                $res[] = Element::updateField($collection, $id, "description", $params["value"]);
                $res[] = Element::updateField($collection, $id, "descriptionHTML", null);

            }else if($block == "descriptions"){

                if(isset($params["tags"]))
                    $res[] = Element::updateField($collection, $id, "tags", $params["tags"]);

                if(isset($params["description"])){
                    $res[] = Element::updateField($collection, $id, "description", $params["description"]);
                    Element::updateField($collection, $id, "descriptionHTML", null);
                }
                
                if(isset($params["shortDescription"]))
                    $res[] = Element::updateField($collection, $id, "shortDescription", strip_tags($params["shortDescription"]));

                if(isset($params["category"]))
                    $res[] = Element::updateField($collection, $id, "category", strip_tags($params["category"]));
                // $res[] = Costum::sameFunction("category", $params["category"]);
            
            }else if($block == "activeCoop"){

                if(isset($params["status"]))
                    $res[] = Element::updateField($collection, $id, "status", $params["status"]);
                if(isset($params["voteActivated"]))
                    $res[] = Element::updateField($collection, $id, "voteActivated", $params["voteActivated"]);
                if(isset($params["amendementActivated"]))
                    $res[] = Element::updateField($collection, $id, "amendementActivated", $params["amendementActivated"]);
            
            }else if($block == "amendement"){

                if(isset($params["txtAmdt"]) && isset($params["typeAmdt"]) && isset($params["id"]) && @Yii::app()->session['userId']){
                    $proposal = Proposal::getById($params["id"]);
                    $amdtList = @$proposal["amendements"] ? $proposal["amendements"] : array();
                    $rand = rand(1000, 100000);
                    while(isset($amdtList[$rand])){ $rand = rand(1000, 100000); }

                    $amdtList[$rand] = array(
                                        "idUserAuthor"=> Yii::app()->session['userId'],
                                        "typeAmdt" => $params["typeAmdt"],
                                        "textAdd"=> $params["txtAmdt"]);
                    Notification::constructNotification ( ActStr::VERB_AMEND, array("id" => Yii::app()->session["userId"],"name"=> Yii::app()->session["user"]["name"]), array("type"=>$proposal["parentType"],"id"=>$proposal["parentId"]),array( "type"=>Proposal::COLLECTION,"id"=> $params["id"] ) );
                    $res[] = Element::updateField($collection, $id, "amendements", $amdtList);
                }
            
            }else if($block == "curiculum.skills"){
                $parent = Element::getByTypeAndId($params["typeElement"], $params["id"]);
                $cv = @$parent["curiculum"] ? $parent["curiculum"] : array();

                $CVAttrs = array("competences", "mainQualification", "hasVehicle", "languages",
                                "motivation", "driverLicense", "url");
                foreach ($CVAttrs as $att) {
                    if(@$params[$att]) 
                    $cv["skills"][$att] = @$params[$att];
                }
                $res[] = Element::updateField($collection, $id, "curiculum", $cv);
                //var_dump($params);
            }else if($block == "curiculum.lifepath"){
                $parent = Element::getByTypeAndId($params["typeElement"], $params["id"]);
                $cv = @$parent["curiculum"] ? $parent["curiculum"] : array();
                $indexLP = @$cv["lifepath"] ? sizeof($cv["lifepath"]) : 0;
                
                $CVAttrs = array("title", "description", "startDate", "endDate",
                                "location");
                foreach ($CVAttrs as $att) {
                    if(@$params[$att]) 
                    $cv["lifepath"][$indexLP][$att] = @$params[$att];
                }
                $res[] = Element::updateField($collection, $id, "curiculum", $cv);
                //var_dump($params);
            }else if($block == "localities"){

                $set = array();
                $unset = array();
                if(!empty($params["address"])){
                    $set["address"] = $params["address"];
                } else {
                    $unset["address"] = array();
                }

                if(!empty($params["geo"])){

                    $set["geo"] = SIG::getFormatGeo($params["geo"]["latitude"], $params["geo"]["longitude"]);
                    $set["geoPosition"] = SIG::getFormatGeoPosition($params["geo"]["latitude"], $params["geo"]["longitude"]);
                } else {
                    $unset["geo"] = array();
                    $unset["geoPosition"] = array();
                }

                if(!empty($params["addresses"])){
                    $set["addresses"] = $params["addresses"];
                } else {
                    $unset["addresses"] = array();
                }

                //Rest::json($set) ; exit;
                if(!empty($set)){
                    PHDB::update(   $collection, 
                                        array("_id" => new MongoId($id)), 
                                        array('$set' => $set ) );
                }

                if(!empty($unset)){
                    PHDB::update(   $collection, 
                                        array("_id" => new MongoId($id)), 
                                        array('$unset' => $unset ) );
                }
                

                $res[] = array("result"=>true, "value" => $set, "fieldName"=> "localities");
            }

            if(Import::isUncomplete($id, $collection)){
                Import::checkWarning($id, $collection, Yii::app()->session['userId'] );
            }

            if( $collection == Event::COLLECTION || $collection == Project::COLLECTION || $collection == Organization::COLLECTION ){
                $el = PHDB::findOneById($collection,$id,array('slug'));
                Slug::updateElemTime( $el['slug'], time() );
            }


            $result = array("result"=>true);
            $resultGoods = array();
            $resultErrors = array();
            $values = array();
            $msg = "";
            $msgError = "";
            foreach ($res as $key => $value) {
                if($value["result"] == true){
                    if($msg != "")
                        $msg .= ", ";
                    $msg .= Yii::t("common",$value["fieldName"]);
                    $values[$value["fieldName"]] = $value["value"];
                }else{
                    if($msgError != "")
                        $msgError .= ". ";
                    $msgError .= $value["mgs"];
                }
            }

            if($msg != ""){
                $resultGoods["result"]=true;
                $resultGoods["msg"]= Yii::t("common", "<i class='fa fa-check' aria-hidden='true'></i> Les attibuts ont bien été mis à jour");
                $resultGoods["values"] = $values ;
                $result["resultGoods"] = $resultGoods ;
                $result["result"] = true ;
            }

            if($msgError != ""){
                $resultErrors["result"]=false;
                $resultErrors["msg"]=Yii::t("common", $msgError);
                $result["resultErrors"] = $resultErrors ;
            }
        } catch (CTKException $e) {
            $resultErrors["result"]=false;
            $resultErrors["msg"]=$e->getMessage();
            $result["resultErrors"] = $resultErrors ;
        }
        return $result;
    }
}
?>