<?php 
class AAP {
    const COLLECTION = "AAP";

    public static function generateConfig($post){
        if(isset($post["aapConfig"]))
            $addAapConfig = PHDB::insert(Form::COLLECTION,$post["aapConfig"]);
        if(isset($post["formParent"]))
            $addFormParent = PHDB::insert(Form::COLLECTION,$post["formParent"]);
        if(isset($post["stepInputs"])){
            $steps = array();
            foreach ($post["stepInputs"] as $ks => $vs) {
                $steps[$ks] = $vs;
            }
            if(!empty($steps))
                $addSteps = PHDB::batchInsert("inputs",$steps);
        }

        if($addAapConfig && $addFormParent && $addSteps)
            return Rest::json(array("result"=>true,"msg"=>Yii::t("common","Information updated")));
    }
}
?>