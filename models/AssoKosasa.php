<?php 
/**
 * 
 */
class AssoKosasa {
  const COLLECTION = "costum";
  const CONTROLLER = "costum";
  const MODULE = "costum";

  public static function getCommunity($post){
   $where = array("source.key" => $post["sourceKey"]);
   $orga = [];

   $params = array(
    "result" => false
  );
   $community = PHDB::find(Organization::COLLECTION,$where);
   if ($community) {
    foreach ($community as $key => $value) {

      $orga[$key] = Element::getElementById($key,$value["type"] ,null, array("_id","name", "profilImageUrl","profilRealBannerUrl","links","slug", "address","geo", "geoPosition", "profilThumbImageUrl", "profilMarkerImageUrl","type" ,"tags","category"));
    }
  }

  if (isset($orga)) {

    $params = array(
      "result" => true
    );

    $res["elt"] = array();
            // var_dump($orga);exit;
    foreach ($community as $key => $value) {

      if(@$value["type"] != "")
        $value["typeOrga"] = $value["type"];
      $value["type"] = "organizations";
      $value["typeSig"] = Organization::COLLECTION;
      
      $imgMedium = isset($value["profilMediumImageUrl"]) ? $value["profilMediumImageUrl"] : "none";
      $img = isset($value["profilImageUrl"]) ? $value["profilImageUrl"] : "none";
      $imgBanner = isset($value["profilRealBannerUrl"]) ? $value["profilRealBannerUrl"] : "none";

      $res["elt"][$key] = array(
        "_id"               => isset($value["_id"])?(String) $value["_id"]:"",
        "name"             =>  isset($value["name"])?$value["name"]:"",
        "imgMedium"        =>  $imgMedium,
        "img"              =>  $img,
        "imgBanner"        =>  $imgBanner,
        "type"             => $value["typeSig"],
        "profilMarkerImageUrl" => isset($value["profilMarkerImageUrl"])?$value["profilMarkerImageUrl"]:"",
        "address"          =>  isset($value["address"])?$value["address"]:"",
        "geo"              =>  isset($value["geo"])?$value["geo"]:"",
        "geoPosition"      =>  isset($value["geoPosition"])?$value["geoPosition"]:"",
        "profilThumbImageUrl" => isset($value["profilThumbImageUrl"])?$value["profilThumbImageUrl"]:"",
        "slug"             =>  $value["slug"],
        "tags"             => @$value["tags"],
        "category"             => @$value["category"]
      );
    }
    return array_merge($params,$res);
  }
  return $params;
}

public static function getActuality($post){
  $where = array("source.key" => $post["sourceKey"]);

  $params = array(
    "result" => false
  );
  $actualite = PHDB::findAndLimitAndIndex(Poi::COLLECTION,$where);

  if ($actualite) {

    $params = array(
      "result" => true
    );

    $res["element"] = array();

    foreach ($actualite as $key => $value) {
      array_push($res["element"],array(
        "id"  =>  (String) $value["_id"],
        "type"  =>  isset($value["type"])?$value["type"]:"",
        "name"  =>  isset($value["name"])?$value["name"]:"",
        "shortDescription"  =>  isset($value["shortDescription"])?$value["shortDescription"]:"",
        "description"  =>  isset($value["description"])?$value["description"]:"",
        "profilImageUrl"  =>  isset($value["profilImageUrl"])?$value["profilImageUrl"]:"",
        "profilMediumImageUrl"  =>  isset($value["profilMediumImageUrl"])?$value["profilMediumImageUrl"]:"",
        "profilThumbImageUrl"  =>  isset($value["profilThumbImageUrl"])?$value["profilThumbImageUrl"]:""
      ));
    }
    return array_merge($params,$res);
  }
  return $params;
}
public static function getAllActeur($post) {
  $acteurs = PHDB::find(Organization::COLLECTION, [
    "tags"=>"acteurs kosasa",
    "source.key" => $post["sourceKey"]
  ]);
 return $acteurs;
}
public static function getActeur($post) {
  $nameActeur = isset($post["nameActeur"])?$post["nameActeur"]:null;
  $tags = isset($post["tagsActeur"])? $post["tagsActeur"]:null;
  $codePostal = isset($post["codePostal"])?$post["codePostal"]:null;

  $acteur = array();
  $where = [];
  if (!empty($nameActeur)) {
    $where["name"] = array('$regex' => $nameActeur);
    $where["tags"] = "acteurs kosasa";
  }
  if(!empty($tags))
    $where["tags"] = array('$in' => array($tags));
  if(!empty($codePostal)) 
    $where["address.postalCode"] = array('$regex' => $codePostal);
  if(!empty($post["sourceKey"])) 
    $where["source.key"] = $post["sourceKey"];

  $acteur = PHDB::find(Organization::COLLECTION, $where);

 return $acteur;
}
public static function getVideo ($post){
  $video =PHDB::find(Bookmark::COLLECTION, array("parentId" => (String)$post["acteurId"]));
  return ($video);
}
public static function getImage ($post){
  $exposition = PHDB::findOne(Folder::COLLECTION,array("name"=>$post["nameAlbum"], "contextId" => (String) $post["acteurId"]) );
  $imageExposition =Array();
  if (isset($exposition)) {
    $imageExposition = Document::getListDocumentsWhere(array(
      "id"=>(String) $post["acteurId"],
      "type"=>'organizations',
      "folderId"=>(String)$exposition["_id"]
    ),"file");
    //var_dump($imageExposition);
  }  else {
    $imageExposition =[];
  }
  return ($imageExposition);
}

}
?>