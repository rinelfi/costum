<?php 
class CoEvent{
	const COLLECTION = "costum";
	const CONTROLLER = "costum";
    const MODULE = "costum";
    
    public static function getCommunity($data){

        $element = array();

        if (!empty($data)) {
            foreach ($data as $key => $value) {
                if (!empty($value["type"]) && isset($value["type"])) {
                    $element[$key] = Element::getByTypeAndId($value["type"],$key);
                }
            }
        }

        if(@$element){
           $res = self::createRequestCommunity($element);
        }
        return $res;
    }

    private static function createRequestCommunity($dataSubEvent){

        $organizer = array();

        if (!empty($dataSubEvent)) {
            foreach ($dataSubEvent as $key => $value) {
                if (!empty($value["links"]) && !empty($value["links"]["organizer"])) {
                    foreach ($value["links"]["organizer"] as $kOrg => $vOrg) {
                        $organizer[$kOrg] = $vOrg;
                    }
                }
            }
        }
        

        if (@$organizer) {
            $res = self::createDataOrganizer($organizer);
        }
        return $res;
    }

    private static function createDataOrganizer($organizer){

        $params = array(
           "result" => false
        );

        $dataOrganizer = array();

        foreach ($organizer as $key => $value) {
            $dataOrganizer[$key] = Element::getByTypeAndId($value["type"],$key);
        }

        if (@$dataOrganizer) {
            $params = array(
                "result" => true
            );

            $res = self::createResultsCommunity($dataOrganizer);
            return array_merge($params,$res);
        }
        return $params;
    }

    private static function createResultsCommunity($params){

        $res["element"] = array();
        
        foreach($params as $key => $value){
            $imgMed = (@$value["profilMediumImageUrl"] ? $value["profilMediumImageUrl"] : "none");
            $img = (@$value["profilImageUrl"] ? $value["profilImageUrl"] : "none");
            $description = (@$value["shortDescription"] ? $value["shortDescription"] : "Aucune description courte");

           array_push($res["element"], array(
               "id"               => (String) $value["_id"],
               "name"             =>  $value["name"],
               "imgMedium"        =>  $imgMed,
               "typeSig"          =>  $value["typeSig"],
               "type"             =>  $value["type"],
               "shortDescription" =>  $description,
               "img"              =>  $img,
               "slug"             =>  $value["slug"]
           ));
       }
       return $res;
    }
}