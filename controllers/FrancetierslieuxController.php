<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
class FrancetierslieuxController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
	        
	        'dashboard'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\DashboardAction::class,
	        'existingreference' =>	\PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\ExistingreferenceAction::class,
	        'getnetwork'		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\GetNetworkAction::class,
	        'multireference' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux\MultiReferenceAction::class,
	    );
	}
	//public function actionPersondah
	/*public function actionIndex() 
	{
    	if(Yii::app()->request->isAjaxRequest)
	        echo $this->renderPartial("../default/index");
	    else
    		$this->render("index");
    	//$this->redirect(Yii::app()->createUrl( "/".Yii::app()->params["module"]["parent"] ));	
  	}*/
}
