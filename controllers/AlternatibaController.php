<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
/**
 * AlternatibaController.php
 *
 */
class AlternatibaController extends CommunecterController {

  public function beforeAction($action)
  {
	  return parent::beforeAction($action);
  }

  public function actions() {
      return array(
      	'getpoiaction'     => \PixelHumain\PixelHumain\modules\costum\controllers\actions\alternatiba\GetPoiAction::class,
        'getcommunityaction'     => \PixelHumain\PixelHumain\modules\costum\controllers\actions\alternatiba\GetCommunityAction::class,
      	'geteventcommunity'	=>	\PixelHumain\PixelHumain\modules\costum\controllers\actions\alternatiba\GetEventCommunityAction::class,
      );
  }

}