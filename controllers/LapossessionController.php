<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class LapossessionController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
	    	'getcommunityaction'	=> 	\PixelHumain\PixelHumain\modules\costum\controllers\actions\lapossession\GetCommunityAction::class,
	    	'getorganization'		=>	\PixelHumain\PixelHumain\modules\costum\controllers\actions\lapossession\GetOrgaAction::class,
	        'element'				=> 	\PixelHumain\PixelHumain\modules\costum\controllers\actions\lapossession\ElementAction::class,
	        'updateblock'  			=> 	\PixelHumain\PixelHumain\modules\costum\controllers\actions\lapossession\UpdateBlockAction::class
	    );
	}
}
