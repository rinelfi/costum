<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class CoeventController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions()
	{
	    return array(
            'getcommunityaction'    => \PixelHumain\PixelHumain\modules\costum\controllers\actions\coevent\GetCommunityAction::class,
            'getinfotypeaction'  =>  \PixelHumain\PixelHumain\modules\costum\controllers\actions\coevent\GetInfoTypeAction::class,
            'getpdfaction'		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\coevent\pdf\GetPdfAction::class,
	    );
	}

	/*public function actionIndex() 
	{
    	if(Yii::app()->request->isAjaxRequest)
	        echo $this->renderPartial("../default/index");
	    else
    		$this->render("index");
    	//$this->redirect(Yii::app()->createUrl( "/".Yii::app()->params["module"]["parent"] ));	
  	}*/
}
