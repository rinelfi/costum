<?php
namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;

class MeirController extends CommunecterController {


    public function beforeAction($action) {
		return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
	        'elementhome'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\meir\HomeAction::class
        );
    }
}