<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class NotragoraController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
	        'addlink'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\notragora\AddLinkAction::class,
	        'deletelink' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\notragora\DeleteLinkAction::class,
	        'updatecategory'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\notragora\UpdateCategoryAction::class,
	        'getproductionbypartner'  		=> 'costum.controllers.actions.notragora.GetProductionByPartnerAction',
	    );
	}
	//public function actionPersondah
	/*public function actionIndex() 
	{
    	if(Yii::app()->request->isAjaxRequest)
	        echo $this->renderPartial("../default/index");
	    else
    		$this->render("index");
    	//$this->redirect(Yii::app()->createUrl( "/".Yii::app()->params["module"]["parent"] ));	
  	}*/
}
