<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\cressReunion\admin;
use CAction;
use CressReunion;
use Rest;

class SaveOrgaAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
    	header('Content-Type: application/json');
    	
        $params = $_POST;
        $res = CressReunion::saveOrga($params);
        //$params = Import::setWikiDataID($_POST);
        return Rest::json($res);
    }
}
