<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\coevent\pdf;

use CAction, Pdf;
class GetPdfAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
		$controller=$this->getController();
		// var_dump($eventOrga);exit;
		$html = $controller->renderPartial('costum.views.custom.coevent.pdf', [], true);
		$params["html"] = $html;
		Pdf::createPdf($params);
    }
}
?>