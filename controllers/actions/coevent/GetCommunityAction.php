<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\coevent;
use CAction;
use CoEvent;
use Rest;

class GetCommunityAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $controller = $this->getController();
        $params = CoEvent::getCommunity($_POST);

        return Rest::json($params);
    }
}