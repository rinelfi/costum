<?php

class GetProductionByPartnerAction extends CAction {
    
    public function run($id) {
	    //assert('!empty($_POST["childType"])'); //The child type is mandatory');
	    $query = array('$or' => array(
	    	array("producors.".$id => array('$exists' => 1)),
	    	array("supports.".$id => array('$exists' => 1)),
	    	array("partner.".$id => array('$exists' => 1))
	    ) );

	    $productions = PHDB::find(Poi::COLLECTION, $query);
		
	 	//$result = array("result"=>true, "msg" => $msg,"productions" => $productions);
		
		Rest::json($productions);
    }
}