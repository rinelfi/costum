<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms;

use CAction, Element, Cms, PHDB, MongoId, Yii, Rest;
class loadBlocAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run()
    {
    	$controller=$this->getController();
        $el = Element::getByTypeAndId($controller->costum["contextType"], $controller->costum["contextId"] );
        $blockcms = [
            "_id" => "",
            "name"=>"Super bloc",
            "type"=>"blockCopy",
            "path"=> "tpls.blockCms.superCms.container",
            "page"=> "welcome",
            "parent"=>  array ("603511ddda83ef4d138b4580"=>array("type"=>"organizations","name"=>"soytest")),
            "haveTpl"=> "false",
            "collection"=> "cms",
            "source"=> array("insertOrign"=>"costum","key"=>"soytest","keys"=>array(0=>"soytest"))
        ];

    	$params = [
    		"cmsList"   =>  [],
    		"blockKey"  => $_POST["idblock"],
    		"blockCms"  =>  $blockcms,
    		"page"      =>  "",
    		"canEdit"   =>  true,
    		"type"      =>  "",
    		"kunik"     =>  "",
    		"content"   =>  "",
    		"el"        => $el,
    		'costum'    => "",
    		"defaultImg" => ""
    	];
        return $controller->renderPartial( "costum.views.".$_POST["path"], $params).'
    <div class="modal-footer">
      <a class="btn btn-primary thisTpls" data-path="'.$_POST["path"].'" data-collection="cms">'.Yii::t("cms", "Use this bloc").'</a>
      <a class="btn btn-primary editDescBlock" data-id="'.$_POST["idblock"].'" data-collection="cms">'.Yii::t("common", "Edit").'</a>
      <a class="btn btn-primary deleteCms" data-id="'.$_POST["idblock"].'" data-collection="cms" >'.Yii::t("common", "Delete").'</a>
    </div>';
    }
}