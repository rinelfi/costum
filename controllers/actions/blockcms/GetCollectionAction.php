<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms;

use CAction, PHDB, Person, Rest;
class GetCollectionAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $controller = $this->getController();
        $where = [
        	"source.key" => $_POST["sourceKey"],
        	"roles.tobeactivated" => array('$exists' => false )
        ];
        $result = PHDB::find(Person::COLLECTION,$where);
        
        return Rest::json($result);
    }
}