<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\blockcms;

use CAction, PHDB, Event, Rest;
class GetEventsAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $controller = $this->getController();
        $where = [
        	"source.key" => $_POST["sourceKey"]
        ];

        if(isset($_POST["type"]) && !is_null($_POST["type"]))
        	$where["type"] = array('$in'=>$_POST["type"]);

        $result = PHDB::findAndSortAndLimitAndIndex(Event::COLLECTION,$where);
        
        return Rest::json($result);
    }
}