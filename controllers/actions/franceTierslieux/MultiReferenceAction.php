<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\franceTierslieux;

use CAction, Element, Rest;
use PixelHumain\PixelHumain\modules\citizenToolKit\models\Admin;

class MultiReferenceAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($type= null){
        $controller = $this->getController();

        $elts = Element::getBySourceTagsAndZone($type,$_POST["tags"],"TiersLieux",$_POST["zoneNb"],$_POST["zoneId"],"franceTierslieux");
      //var_dump($elts);exit;

        foreach ($elts as $id => $value){
            if((!isset($value["reference"]) && !in_array($_POST["costumSlug"],$value["source"]["keys"])) || (isset($value["reference"]["costum"]) && !in_array($_POST["costumSlug"],$value["reference"]["costum"]))){
                Admin::addSourceInElement($id,$value["collection"],$_POST["costumSlug"],"reference");
            }
        }

        
        return Rest::json($elts);
    }
    	
}
    	
