<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions;
use Authorisation;
use CacheHelper;
use CAction;
use CO2;
use Costum;
use Yii;

class IndexAction extends \PixelHumain\PixelHumain\components\Action
{
    //$id 
    // if given with a type it concerns an element
    // otherwise it's just costum can be the slugname or the digital id 
    //$type : 
        // if given with an id it concerns an element
    //$slug 
        // it's the slug of an element
    //$init this action can render a result for a page request
    
    public function run($id=null,$type=null,$slug=null, $view=null,$page=null,$test=null)
    { 	

        //CacheHelper::flush();die;
        //TODO faire tout ca dans Costum::init()
    	//$activeCOs = array("cocampagne");
    	$controller=$this->getController();
        $controller->layout = "//layouts/mainSearch";

        if(empty(CacheHelper::get("appConfig")))
          CacheHelper::set("appConfig", CO2::getThemeParams());
        
        $controller->appConfig = CacheHelper::get("appConfig");


         if($slug || $id)
             $slugCache = !empty($slug) ? $slug : $id;
         else 
             $slugCache = !empty(@$_GET["host"]) ? $_GET["host"] : null;
        
        if(!$controller->cacheCostumInit($slugCache)){
            $returnCostum = Costum::init ($controller,$id,$type,$slug,$view,$test,"costum/controllers/actions/IndexAction.php");
            $controller->cacheCostumInit($slugCache,$returnCostum);
        }

        $controller->meta($controller->costum);
        //var_dump($this->getController()->appConfig);
        //var_dump($this->getController()->costum);



        $canEdit = false;

        if( isset(Yii::app()->session["userId"])  && isset($controller->costum["contextType"]) && isset($controller->costum["contextId"]) )
            $canEdit = Authorisation::canEditItem(Yii::app()->session["userId"],@$controller->costum["contextType"], @$controller->costum["contextId"]);
        
        //TODO if no canEdit et test exist then 
          //redirect unTpl 

        $params = ["canEdit" => $canEdit];
        if( isset($test) ){
            $params["tpl"]=$id;
            $params["test"]=$test;
        }
        //use another page inside the same costum folder
    	if($page && $controller->costum["welcomeTpl"])
        {
            //pages are relative to the current context
            //will sit in the same destination foilder as the welcomeTpl of the costum
            $path = explode(".", $controller->costum["welcomeTpl"]);
            array_pop($path);
            $path[] = $page;
            if(Yii::app()->request->isAjaxRequest){
                return $controller->renderPartial(implode(".", $path), $params, true);
            }
            else
              return $controller->render(implode(".", $path), $params, true);
        }
	  	else if( isset($controller->costum["welcomeTpl"])){
            //$controller->costum["paramx"] = ["canEdit"=>$canEdit];//$params;
            // var_dump($controller->costum["slug"]);
            // var_dump($controller->costum["welcomeTpl"]);
            // exit;
            if(Yii::app()->request->isAjaxRequest){
                return $controller->renderPartial("co2.views.app.welcome", $params, true);
            }
            else
               return $controller->render( "co2.views.app.welcome",$params );

        }
	  	else 
	  	   return $controller->render("index",[ "canEdit" => $canEdit ] );
		
    	/*if(!empty($id)){
			if(strlen($id) == 24 && ctype_xdigit($id)  )
		        $c = PHDB::findOne( Costum::COLLECTION , array("_id"=> new MongoId($id)));
		    else 
		   		$c = PHDB::findOne( Costum::COLLECTION , array("slug"=> $id));
	   	}else if(@$_GET["host"]){
	   		$c = PHDB::findOne( Costum::COLLECTION , array("host"=> $_GET["host"]));
	   	}*/

	  	//contient des éléments partagé par tout les costums
	  	//echo $this->renderPartial("costum.views.common",array("el"=>$el),true);

    }
}


