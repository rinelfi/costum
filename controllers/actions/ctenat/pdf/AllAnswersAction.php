<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\pdf;

use CAction, Ctenat;
class AllAnswersAction extends \PixelHumain\PixelHumain\components\Action{
	public function run($slug = null, $admin=null, $id=null, $idElt=null, $cterId=null, $status=null, $text=null) {
		$controller=$this->getController();
		ini_set('max_execution_time',1000);
		$params["controller"] = $controller;
		$params["slug"] = $slug;
		$params["admin"] = $admin;
		$params["id"] = $id;
		$params["idElt"] = $idElt;
		$params["cterId"] = $cterId;
		if (!empty($status)){
		    $params["status"] = $status;
        }
        if (!empty($text) && $text != ""){
            $params["text"] = $text;
        }
		Ctenat::pdfElement($params);

	}
}