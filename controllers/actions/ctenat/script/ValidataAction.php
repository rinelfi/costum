<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\script;

use CAction, Role, Yii, PHDB, Organization, Project, Form, Ctenat, MongoId;
 /**
  * TEst Data Tree structure and show statistics for a source key 
  * @param String $srcKey show by source key
  * echo 
  - red : blockers
  - orange : warnings 
  - black : informations ...
  */
class ValidataAction extends \PixelHumain\PixelHumain\components\Action
{

	/*
	params 
	no params 
	by source.key
	by cter slug

	*/
	const SOURCEKEY = "ctenat";
	const SEP = "<br/><hr style='border-top: 5px solid red;'/><br/>";
	public function run( $srcKey=null, $cter=null, $errorsOnly = null ){
		$controller = $this->getController();

		if( Role::isSuperAdmin(Role::getRolesUserId(Yii::app()->session["userId"])))
		{
			

			/*
			test si orga.source.key:ctenat openEdition:fasle > warning red
			*/
			echo self::SEP;
			echo "[X] organizations openEdition";
			$elms=PHDB::find(Organization::COLLECTION, ["source.key"=>self::SOURCEKEY,"preferences.isOpenEdition"=>"true"],["name","slug"]);
			if(count($elms)){
				echo "[X] organizations.source.key:".self::SOURCEKEY." openEdition:true".count($elms)."<br/>";
	  			foreach($elms as $k => $v){
	  				if(!$errorsOnly)echo "<span style='color:orange'><a href='/costum/co/index/id/".self::SOURCEKEY."#@".$v["slug"]."'>".$v["name"]."</a></span><br/>";
	  			}
	  		} 
			/*
			test si projects.source.key:ctenat openEdition:fasle > warning red
			*/
			echo self::SEP;
			echo "[X] projects openEdition";
			$elms=PHDB::find(Project::COLLECTION, ["source.key"=>self::SOURCEKEY,"preferences.isOpenEdition"=>"true"],["name","slug"]);
			if(count($elms)){
				
				echo "[X] projects.source.key:".self::SOURCEKEY." openEdition:true".count($elms)."<br/>";
	  			foreach($elms as $k => $v){
	  				if(!$errorsOnly)echo "<span style='color:orange'><a href='/costum/co/index/id/".self::SOURCEKEY."#@".$v["slug"]."'>".$v["name"]."</a></span><br/>";
	  			}
	  		} 

	  		/*
			test count answers.source.key:ctenat
			*/
			echo self::SEP;
			echo "[X] Nb de Fiche Actions (answers.source.key:".self::SOURCEKEY.") : <b>".PHDB::count(Form::ANSWER_COLLECTION,["source.key"=>self::SOURCEKEY]).'</b>';

			/*
			test projects.catgeory.cteR
			*/
			echo self::SEP;
			echo "[X] Nb de Fiche Actions (projects.category.cteR ) : <b>".PHDB::count(Project::COLLECTION,["category"=>"cteR"])."</b>";
			echo "<br/>[X] Nb de Fiche Actions avec status(projects.category.cteR && source.status.ctenat ) : <b>".PHDB::count(Project::COLLECTION,["category"=>"cteR","source.status.ctenat"=>['$exists'=>1]])."</b>";
			


			/*
				count projects.category.ficheAction 
			*/
			echo self::SEP;
			echo "[X] Nb de Fiche Actions (projects.category.ficheAction ) : <b>".PHDB::count(Project::COLLECTION,["category"=>"ficheAction"])."</b>";
			echo "<br/>[X] Nb de Fiche Actions avec status(projects.category.ficheAction && source.status.ctenat ) : <b>".PHDB::count(Project::COLLECTION,["category"=>"ficheAction","source.status.ctenat"=>['$exists'=>1]])."</b>";

			
			$actions = PHDB::find(Form::ANSWER_COLLECTION,["source.key"=>"ctenat"]);
				/*
				check source.status.ctenat dans cette liste
					//refactor à rapatrier dans le costum 
			*/
			$actionStatus = [
				Ctenat::STATUT_ACTION_COMPLETED ,
			    Ctenat::STATUT_ACTION_VALID ,
			    Ctenat::STATUT_ACTION_MATURATION ,
			    Ctenat::STATUT_ACTION_LAUREAT ,
			    Ctenat::STATUT_ACTION_REFUSE ,
			    Ctenat::STATUT_ACTION_CANDIDAT ,
			    Ctenat::STATUT_ACTION_CONTRACT ,
			];
			$countNoProject = 0;
			echo self::SEP;
			echo "<h3> Nb de Fiche Actions sans PROJECT</h3>";
			foreach ($actions as $id => $a) {
				if( !isset($a["source"]["status"]) || !in_array($a["source"]["status"],$actionStatus )  ){
					if( isset($a["answers"]) && isset($a["answers"][ $a["formId"] ] ["answers"]["project"]["id"]) )
						if(isset($a["answers"]))
						{
							$p = PHDB::findOne(Project::COLLECTION,[
												"_id" => new MongoId( $a["answers"][ $a["formId"] ] ["answers"]["project"]["id"] ) 
												] );
							if(!isset($p) )
							{
								echo "<br/><span style='font-size:10px;'>answer id : ".$id."</span>";
							 	echo "<br/><span style='color:orange'>project not exist :  (".$a["answers"][ $a["formId"] ] ["answers"]["project"]["id"].") cte : ".$a["formId"]."</span>";
							 	echo "<br/><span style='font-size:10px;'>>>>>".$a["answers"][ $a["formId"] ] ["answers"]["project"]["name"]."</span>";
							 	//echo "<br/><span style='font-size:10px;'>>>>>".@$a["source"]["status"]."</span>";
							 	echo "<br/><span style='font-size:10px;'>>>>>".( (!empty($a["priorisation"])) ? $a["priorisation"] : "<b style='color:red'>no status</b>")."</span>";
							 	$countNoProject++;
							 	if(isset($a["answers"][ $a["formId"] ] ["answers"]["project"]["name"] ) )
								{
									$p = PHDB::findOne(Project::COLLECTION,[
											"name" => $a["answers"][ $a["formId"] ] ["answers"]["project"]["name"] ] );
									if(isset($p) )
										echo "<br/><span style='font-size:10px;color:red'>project found by name:  (".$p["_id"].") cte : ".$a["formId"]."</span>";
									
								}
								echo "<br/>";
							} 
						} else {
							echo "<br/><span style='color:red'>".$a["formId"]."_NO ANWSER</span>";
							$countNoProject++;
						}
						
						
					} else {
						$countNoProject++;
						echo "<br/><span style='color:orange'>no project: ".$a["_id"]."</span>";
					}
			}

			echo "<br/>[X] Nb de Fiche Actions sans project : <b>".$countNoProject."/".count($actions)."</b>";

			$countNoPorteur = 0;
			echo self::SEP;
			echo "<h3> Nb de Fiche Actions sans PORTEUR</h3>";
			foreach ($actions as $id => $a) {
//<<<<<<< HEAD
				
					if( isset($a["answers"][ $a["formId"] ] ["answers"]["organization"]["id"]) )
					{
						
							$o = PHDB::findOne(Organization::COLLECTION,[
												"_id" => new MongoId( $a["answers"][ $a["formId"] ] ["answers"]["organization"]["id"] ) 
												] );
							if(!isset($o) )
							{
								echo "<br/><span style='font-size:10px;'>answer id : ".$id."</span>";
							 	echo "<br/><span style='color:orange'>organization not exist :  (".$a["answers"][ $a["formId"] ] ["answers"]["organization"]["id"].") cte : ".$a["formId"]."</span>";
							 	echo "<br/><span style='font-size:10px;'>>>>>".$a["answers"][ $a["formId"] ] ["answers"]["organization"]["name"]."</span>";
							 	//echo "<br/><span style='font-size:10px;'>>>>>".@$a["source"]["status"]."</span>";
							 	echo "<br/><span style='font-size:10px;'>>>>>".( (!empty($a["priorisation"])) ? $a["priorisation"] : "<b style='color:red'>no status</b>")."</span>";
							 	$countNoPorteur++;
							 	
								echo "<br/>";
							} 
					}
// =======
// 				if( !isset($a["source"]["status"]) || !in_array($a["source"]["status"],$actionStatus ) )
// 					//$p = PHDB::findOne(Project::COLLECTION,["_id"=>new MongoId()]);
// 					if(isset($a["answers"][ $a["formId"] ] ["answers"]["project"])){
// 						if(!$errorsOnly)echo "<br/><span style='color:orange'><a href=''>".$a["answers"][ $a["formId"] ] ["answers"]["project"]["name"]."</a></span>";
// 					}
// 					else
// 						echo "<br/><span style='color:red'>No Project on answer ".$a["_id"]." : cte : ".$a["formId"]."</span>";
// >>>>>>> master
			}

			echo "<br/>[X] Nb de Fiche Actions sans PORTEUR : <b>".$countNoPorteur."/".count($actions)."</b>";

			//all projects of CTER
			if(isset($cter)){
				//db.getCollection('answers').find({category:"ficheAction",formId:""})
				$answers=PHDB::find(Form::ANSWER_COLLECTION, ["formId"=>$cter]);
				if(count($answers)){
		  			foreach($answers as $k => $v){
		  				if(isset($v["answers"][$cter]["answers"]["project"])){
			  				$p = PHDB::findOne(Project::COLLECTION,["_id"=>new MongoId($v["answers"][$cter]["answers"]["project"]["id"])]);
			  				if(!$errorsOnly)echo "<br/><span style='color:orange'><a href='/costum/co/index/id/".self::SOURCEKEY."#@".$p["slug"]."'>".$v["answers"][$cter]["answers"]["project"]["name"]."</a></span>";
		  				} else 
		  					echo "<br/><span style='color:red'>No Project on answer ".$v["_id"]."</span>";
		  			}
		  		} 
			}
			/*
			test pour chaque answers test 
				project exist
					links contributors valid 
					links organisations valid 
					links projects 
					links answers
				orga 
				actionPrincipale valid badge 
				actionsecondaire valid badge 
				ciblePrincipale valid badge
				cibleSecondaire valid badge
				indicateur valid POI
					checlk list domaineAction et objectifDD valid 
					 $indicators = PHDB::find(Poi::COLLECTION, $whereI, array("name", "domainAction", "objectifDD"));
						

				check priorisation status dans cette liste
					//refactor à rapatrier dans le costum 
					list : [
						"Action validée",
						"Action en maturation",
						"Action lauréate",
						"Action refusée",
						"Action Candidate"
						]

			*/

			echo self::SEP;
			echo "<h2>Feature List</h2>";
			echo self::SEP;
			echo "IMPORT BADGE<br/>";
			echo "> create badge <br/>";
			echo "> Link <br/>";
			echo self::SEP;
			echo "IMPORT INDICATEUR<br/>";
			echo "> create poi.indicator <br/>";
			echo self::SEP;
			echo "IMPORT ACTIONS <br/>";
			echo "> create cter <br/>";
			echo "> create answer <br/>";
			echo "> create orga <br/>";
			echo "> create project(action) <br/>";
			echo "> create all links <br/>";


			$params = array() ;
			if(Yii::app()->request->isAjaxRequest)
				return $controller->renderPartial("/custom/ctenat/".$page,$params,true);
			
		}else{
  			echo "Accès réservé au Big Bosses !";
  		}	
	}
}