<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\element;

use CAction, Yii;
 /**
  * Display the directory of back office
  * @param String $id Not mandatory : if specify, look for the person with this Id. 
  * Else will get the id of the person logged
  * @return type
  */
class DashboardAccountAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run(){
		$controller = $this->getController();
		$params = array() ;
		$page = "element/dashboard";
		if(Yii::app()->request->isAjaxRequest)
			return $controller->renderPartial("/custom/ctenat/".$page,$_POST,true);
		
		
	}
}
