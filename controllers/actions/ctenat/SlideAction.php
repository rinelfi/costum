<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat;

use CAction, Yii;
class SlideAction extends \PixelHumain\PixelHumain\components\Action{
	public function run($s=null) {
		$controller=$this->getController();
		
		$tpl = "costum.views.custom.ctenat.slide";
		if(isset($s) ) 
			$tpl = $tpl."s.".$s;
		
		if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial($tpl,null,true);              
        else 
    	return	$controller->render($tpl,null);
        

	}
}