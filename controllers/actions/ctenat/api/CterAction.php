<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ctenat\api;

use CAction, PHDB, Project, MongoId, Rest, Yii;
class CterAction extends \PixelHumain\PixelHumain\components\Action{
	public function run($slug = null,$format=null) {
		$controller=$this->getController();

		$what = ["name","scope","source"];
		if(isset($slug))
			$cters = PHDB::findOne( Project::COLLECTION, ["category"=>"cteR","slug"=>$slug], $what );
		else
			$cters = PHDB::find( Project::COLLECTION, ["category"=>"cteR"], $what );
		
		$result = [];
		foreach ($cters as $key => $cter) {
			if( isset($cter['scope']) ){

				//colonne 1 - siren EPCI; 
				//colonne 2 - nom INSEE EPCI; 
				//colonne 3 - nom du CTE; 
				//colonne 4 - statut
				$lineCount = 1;
				foreach ($cter['scope'] as $lvl => $s) {
					if($lineCount == 1){
						$line = [];
						$zone = PHDB::findOne( "zones", ["_id"=> new MongoId($s['id']) ], ["epci"] );

						$line["epciCode"] = (isset($zone["epci"])) ? $zone["epci"] : "";
						$line["epciNom"] = $s["name"];
						$line["cterNom"] = $cter["name"];
						$line["cterStatus"] = (isset($cter["source"]["status"]["ctenat"])) ? $cter["source"]["status"]["ctenat"] : "";
					}
					$lineCount++;
				}	
			}else 
				$line = $cter;
			$result[] = $line;
		}
		if(isset($format) && $format == "count")
			echo count($result);
		else if(isset($format) && $format == "csv"){
			header("Content-type: text/csv");
			header("Content-Disposition: attachment; filename=file.csv");
			header("Pragma: no-cache");
			header("Expires: 0");
			foreach ($result as $i => $l) {
				echo '"'.@$l["epciCode"].'";'.'"'.@$l["epciNom"].'";'.'"'.@$l["cterNom"].'";'.'"'.@$l["cterStatus"].'"'."\n";
			}
			//Rest::csv($result);
		}
		else 
			return Rest::json($result);

	}
}