<?php
class GetProjectAction extends CAction{
    public function run($id){
        $project = Project::getById($id);
        $res = [];
        if($project){
            /* if(isset($project["links"]))
                $res = Element::getAllLinks($project["links"], Project::COLLECTION, $id);
            else */
            $res[$id] = $project;
        }
        Rest::json(["results"=>$res]);
    }
}