<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\cordCollaborativeOpenRd;
use CAction;
use Form;
use PHDB;
use Yii;

class OcecodashboardAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($answerId=null)
    {
    	$controller = $this->getController();    	


    	$tpl = "costum.views.custom.cordCollaborativeOpenRd.dashboard";

        $formsParent = PHDB::find( Form::COLLECTION,  array( "id" => "opalProcess" ) );

        $formsParent = array_shift($formsParent);

        $answers = PHDB::findOneById(Form::ANSWER_COLLECTION, $answerId);

        $action = [];
        $project = [];
        $title = "";

        if (!empty($answers["project"]["id"])) {
            $action = PHDB::find( Actions::COLLECTION,  array( "parentId" => new mongoId($answers["project"]["id"]) ) );
        }

        if (isset($answers["project"])) {
            $project = $answers["project"];
        }

        if (isset($answers["answers"]["opalProcess1"]["titre"])) {
            $title = $answers["answers"]["opalProcess1"]["titre"];
        }

        // $answers = array_shift($answers);

        $feedback = [];
        if (isset($answers["feedback"])) {
            $feedback = $answers["feedback"];
        }

        if (isset($answers["answers"]["opalProcess1"]["depense"])) {
            $answers = $answers["answers"]["opalProcess1"]["depense"];
        }

    	$params = [
            "allData" => $answers,
            "project" => $project,
            "action" => $action,
            "title" => $title,
            "feedback" => $feedback,
        ];	
		
    	if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial($tpl,$params,true);              
        else {
    		$this->getController()->layout = "//layouts/empty";
    		return $this->getController()->render($tpl,$params);
        }
    }
}
    	