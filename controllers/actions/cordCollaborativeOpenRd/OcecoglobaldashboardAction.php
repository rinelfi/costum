<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\cordCollaborativeOpenRd;

use CAction, PHDB, Form, Yii;
class OcecoglobaldashboardAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run($contextid=null)
    {
    	$controller = $this->getController();      

        $tpl = "costum.views.custom.cordCollaborativeOpenRd.globaldashboard";

        $formsParent = PHDB::find( Form::COLLECTION,  array( "id" => "opalProcess" ) );

        $formsParent = array_shift($formsParent);

        $answers = PHDB::find(Form::ANSWER_COLLECTION, array("form" => (string)$formsParent["_id"], "context.".$contextid => array('$exists'=>1)));

        foreach ($answers as $key => $value) {
            if (!isset($answers[$key]["answers"])) {
                unset($answers[$key]);
            } 
        }

        $allData = $answers;

        foreach ($answers as $key => $value) {
            if (isset($answers[$key]["answers"]["opalProcess1"]["depense"])) {
            $answers[$key] = $answers[$key]["answers"]["opalProcess1"]["depense"];
            } else {
                $answers[$key] = [];
            }
        }

        $action = [];
        $project = [];
        $title = "";

        $params = [
            "allData" => $allData,
            "allDataAns" => $answers,
            "project" => $project,
            "action" => $action,
            "title" => $title,
        ];  
        
        if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial($tpl,$params,true);              
        else {
            $this->getController()->layout = "//layouts/empty";
            return $this->getController()->render($tpl,$params);
        }
    }
}
    	