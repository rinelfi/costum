<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\cocity;

use CAction, Cocity, Rest;
class GetOrgaFiliereAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = Cocity::getOrgaFiliere($_POST);
        
        return Rest::json($params);
    }
}