<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\assokosasa;
use AssoKosasa;
use CAction;
use Rest;

class GetCommunityAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = AssoKosasa::getCommunity($_POST);
        
        return Rest::json($params);
    }
}