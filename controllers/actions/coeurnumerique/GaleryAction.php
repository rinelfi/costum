<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\coeurnumerique;

use CAction, CoeurNumerique, Rest;
class GaleryAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $controller = $this->getController();
        $params = CoeurNumerique::galery($_POST);

        return Rest::json($params);
    }
}