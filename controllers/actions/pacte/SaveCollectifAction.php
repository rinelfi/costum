<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\pacte;

use CAction, SiteDuPactePourLaTransition, Rest;
class SaveCollectifAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run(){
    	header('Content-Type: application/json');
    	ini_set('max_execution_time',10000000);
		ini_set('memory_limit', '512M');
		//Rest::json($_POST); exit;
        $params = SiteDuPactePourLaTransition::saveCollectif($_POST);
        //$params = Import::setWikiDataID($_POST);
        return Rest::json($params);
    }
}
?>