<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\pacte;

use CAction, CacheHelper, PHDB, Organization, ArrayHelper, Rest;
class CollectifSignedCsvAction extends \PixelHumain\PixelHumain\components\Action{
	public function run($typecsv = null) {
		$controller=$this->getController();
		$costum = CacheHelper::getCostum();
		ini_set('max_execution_time',10000000);
		ini_set('memory_limit', '-1');
		// On récupere la donnée en fonction des filtres
		$resS = PHDB::find(Organization::COLLECTION, array('category'=>array( '$in'=>["actif", "actifSigned"])));
	
		$newList = array();
		$allPath = array();
		
		// On parcourt les answers afin de restructurer la donnée pour le parser
		foreach ($resS as $key => $collectif) {
			if($collectif["name"]!= "Collectif local du Pacte pour la Transition"){
			$referentMail=(!isset($collectif["referentMail"]) && isset($collectif["email"])) ? $collectif["email"] : @$collectif["referentMail"];
			$newColl=array(
				"Nom collectif" => $collectif["name"],
				"Mail publique" => @$collectif["publicMail"],
				"Mail de contact" => $referentMail,
				"Mailing pacte" => @$collectif["email"],
				"Adresse de modification"=> "https://www.pacte-transition.org/#@".@$collectif["slug"].".edit.".$key
			);
			$allPath = ArrayHelper::getAllPathJson(json_encode($newColl), $allPath, false);
			$newList[] = $newColl;	
			}
		}

		// L'ordre d'affichage des collone pour le csv
		$order = array(
			"Nom collectif",
			"Mail publique",
			"Mail de contact",
			"Mailing pacte",
			"Adresse de modification"
		);

		$res = array("results" => $newList, "fields"=> $order, "allPath"=>$allPath);

		return Rest::json($res); 

	}
}