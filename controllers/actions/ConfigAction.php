<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions;
use CacheHelper;
use CAction;
use CO2;
use Costum;
use Slug;
use Yii;

class ConfigAction extends \PixelHumain\PixelHumain\components\Action
{
    
    public function run($id=null,$type=null,$slug=null, $view=null,$page=null,$test=null)
    { 	

        $controller=$this->getController();
        $controller->layout = "//layouts/mainSearch";
        
        if(empty(CacheHelper::get("appConfig"))){
          CacheHelper::set("appConfig", CO2::getThemeParams());
        }
        $this->getController()->appConfig = CacheHelper::get("appConfig");

        if($slug || $id){
            $slugCache = !empty($slug) ? $slug : $id;
        }else {
            $slugCache = !empty(@$_GET["host"]) ? $_GET["host"] : null;
        }
        if(!$this->getController()->cacheCostumInit($slugCache)){
        $returnCostum = Costum::init ($this->getController(),$id,$type,$slug,$view,$test, \PixelHumain\PixelHumain\modules\costum\controllers\actions\IndexAction::class);
        $this->getController()->cacheCostumInit($slugCache,$returnCostum);
        }

        if(Yii::app()->request->isAjaxRequest)
            return $controller->renderPartial("costum.views.co.config", ["slug"=>$slug, "el"=> Slug::getElementBySlug($slug)], true);
        else
	  	return	$controller->render("costum.views.co.config",["slug"=>$slug, "el"=> Slug::getElementBySlug($slug)]);
		

    }
}


