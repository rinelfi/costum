<?php
class MigrateSpotsAction extends CAction{
    public function run(){
        if(isset($_SESSION["userId"])){
            $el = PHDB::findOne(Organization::COLLECTION, ["slug"=>"meteolamer"]);
            if(isset($el["links"]["members"][$_SESSION["userId"]]) && $el["links"]["members"][$_SESSION["userId"]]["isAdmin"]){
                Meteolamer::migrateSpots();
            }else{
                echo "You are not authorized to perform this action";
            }
        }else{
            echo "You must log in to perform this operation";
        }
    }
}