<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\meteolamer;

use CAction, Yii, Rest;
class ImportSpotsAction extends \PixelHumain\PixelHumain\components\Action{
    private $collection = "--meteolamer-spots";
    public function run(){
        $type_spots = [
            "region" => [
                [
                    "name"=>"reunion",
                    "label"=>"La Réunion",
                    "lat"=>"-21",
                    "lon"=>"55"
                ],
                [
                    "name"=>"maurice",
                    "label"=>"Maurice",
                    "lat"=>"-20",
                    "lon"=>"57.5"
                ],
                [
                    "name"=>"mayotte",
                    "label"=>"Mayotte",
                    "lat"=>"-11.5",
                    "lon"=>"45"
                ],
                [
                    "name"=>"seychelles",
                    "label"=>"Seychelles",
                    "lat"=>"-5",
                    "lon"=>"55"
                ],
                [
                    "name"=>"toamasina",
                    "label"=>"Toamasina",
                    "lat"=>"-18",
                    "lon"=>"50"
                ],
                [
                    "name"=>"toliara",
                    "label"=>"Toliara",
                    "lat"=>"-24",
                    "lon"=>"42.5"
                ],
                [
                    "name"=>"durban",
                    "label"=>"Durban",
                    "lat"=>"-30",
                    "lon"=>"32.5"
                ],
                [
                    "name"=>"kerguelen",
                    "label"=>"Kerguelen",
                    "lat"=>"-48",
                    "Lon"=>"70"
                ],
            ],
            "spot" =>  [
                [
                    "name"=>"boucan",
                    "label"=>"Boucan",
                    "lat"=>"-21.0283",
                    "lon"=>"5.2168"
                ],
                [
                    "name"=>"etangsal",
                    "label"=>"Etang Salé",
                    "lat"=>"-21.2692",
                    "lon"=>"55.3278"
                ],
                [
                    "name"=>"leport",
                    "label"=>"Le Port",
                    "lat"=>"-20.9157",
                    "lon"=>"55.3154"
                ],
                [
                    "name"=>"portoues",
                    "label"=>"Le Port Ouest",
                    "lat"=>"-20.9375",
                    "lon"=>"55.267"
                ],
                [
                    "name"=>"lhermit",
                    "label"=>"L'hermitage",
                    "lat"=>"-21.0897",
                    "lon"=>"55.2192"
                ],
                [
                    "name"=>"manapany",
                    "label"=>"Manapany",
                    "lat"=>"-21.3794",
                    "lon"=>"55.5844"
                ],
                [
                    "name"=>"roches",
                    "label"=>"Roches Noires",
                    "lat"=>"-21.0521",
                    "lon"=>"55.219"
                ],
                [
                    "name"=>"stbenoit",
                    "label"=>"St Benoit",
                    "lat"=>"-21.0255",
                    "lon"=>"55.7207"
                ],
                [
                    "name"=>"stdenis",
                    "label"=>"St Denis",
                    "lat"=>"-20.872",
                    "lon"=>"55.4591"
                ],
                [
                    "name"=>"stpaul",
                    "label"=>"St Paul",
                    "lat"=>"-21.005",
                    "lon"=>"55.2697"
                ],
                [
                    "name"=>"stleu",
                    "label"=>"St Leu",
                    "lat"=>"-21.1648",
                    "lon"=>"55.2795"
                ],
                [
                    "name"=>"stpierre",
                    "label"=>"St Pierre",
                    "lat"=>"-21.3526",
                    "lon"=>"55.472"
                ],
                [
                    "name"=>"troisbas",
                    "label"=>"Trois Bassins",
                    "lat"=>"-21.114",
                    "lon"=>"55.2505"
                ],
            ]
        ];
        foreach($type_spots as $type => $spots){
            foreach($spots as $spot){
                $spot["type"] = $type;
                Yii::app()->mongodb->selectCollection($this->collection)->insert($spot);
            }
        }
        return Rest::json(["status"=>1]);
    }
}