<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\templatecostum;
use CAction;
use Rest;
use TemplateCostum;

class GetEventAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = TemplateCostum::getEvent($_GET);
        
        return Rest::json($params);
    }
}