<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\miks;

use CAction, Miks, Rest;
class GetProjectAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run( $tpl=null, $view=null ){
		$controller = $this->getController();
		$results = Miks::getProjects($_POST["contextType"],$_POST["contextSlug"]);

		return Rest::json($results);
	}
}
