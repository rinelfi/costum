<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\aap;

use Form;
use MongoId;
use Person;
use PHDB;
use Rest;
use Yii;

class PublicRateAction extends \PixelHumain\PixelHumain\components\Action
{
    public function run() {
    	$controller=$this->getController();
        if(Person::logguedAndValid()){
            $me = Yii::app()->session['userId'];
            if(!empty($_POST["id"]) && !empty($_POST["value"])){
                PHDB::update( Form::ANSWER_COLLECTION,
						    [ "_id" => new MongoId($_POST["id"]) ], 
						    [ '$set' => [ "publicRating.".$me => $_POST["value"] ] ]);
                $msg=Yii::t("common","Everything is allRight");
                return Rest::json(array("result"=>true, "msg"=>$msg,"params"=> $_POST["value"]));
            }
        }else
            return Rest::json(array("result"=>false,"msg"=>Yii::t("common","Please Login First")));
    }
}