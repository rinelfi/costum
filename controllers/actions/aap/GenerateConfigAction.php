<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\aap;

use CAction, AAP, Rest;
class GenerateConfigAction extends \PixelHumain\PixelHumain\components\Action{
    public function run(){
        $controller = $this->getController();
        $result = AAP::generateConfig($_POST);
        return Rest::json($result);
    }
}