<?php
class SearchTagsAction extends CAction
{
    public function run($q=null) {
    	    $controller=$this->getController();
            $regex = SearchNew::accentToRegex($q);
            $limitSearch = 50;
            $params= array("answers.aapStep1.tags" => ['$in' => [new MongoRegex("/".$regex."/i")]] );
            $tagsList = PHDB::find(Form::ANSWER_COLLECTION, $params,array("answers.aapStep1.tags"));
            $tags = [];
            foreach ($tagsList as $key => $value) {
                $vtags = $value["answers"]["aapStep1"]["tags"];
                foreach ($vtags as $k => $v) {
                    if(Api::stringStartsWith(strtolower($v),strtolower($q))){
                        if(!in_array(["tag"=>$v],$tags))
                            $tags[] = array("tag"=>$v);
                    }
                        
                }
            }
              return Rest::json($tags);
    }
}