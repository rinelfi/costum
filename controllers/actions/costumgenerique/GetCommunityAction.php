<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\costumgenerique;
use CAction;
use CostumGenerique;
use Rest;

class GetCommunityAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = CostumGenerique::getCommunity($_POST);
        return Rest::json($params);
    }
}