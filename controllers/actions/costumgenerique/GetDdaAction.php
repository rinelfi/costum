<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\costumgenerique;

use CAction, CostumGenerique, Rest;
/**
 * 
 */
class GetDdaAction extends \PixelHumain\PixelHumain\components\Action
{
	
	public function run($id=null, $type=null, $slug=null, $view=null, $page=null)
	{
		$controller = $this->getController();
		$params = CostumGenerique::getDda($_POST);
		return Rest::json($params);
	}
}
?>