<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\costumgenerique;

use CAction, CostumGenerique, Rest;
class GetRessourcesAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = CostumGenerique::getRessources($_POST);
        return Rest::json($params);
    }
}