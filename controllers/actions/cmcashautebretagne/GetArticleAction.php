<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\cmcashautebretagne;

use CAction, CmcasHauteBretagne, Rest;
/**
 * 
 */
class GetArticleAction extends \PixelHumain\PixelHumain\components\Action
{
	public function run(){
		$controller = $this->getController();
		$params = CmcasHauteBretagne::getArticles($_POST["sourceKey"]);

		return Rest::json($params);
	}
}

?>