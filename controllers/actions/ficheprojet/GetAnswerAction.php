<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\ficheprojet;

use CAction, FicheProjet, Rest;
class GetAnswerAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = FicheProjet::getAnswer($_POST);
        
        return Rest::json($params);
    }
}