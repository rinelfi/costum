<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\alternatiba;
use Alternatiba;
use CAction;
use Rest;

class GetEventCommunityAction extends \PixelHumain\PixelHumain\components\Action{
    public function run($id = null,$type= null,$slug = null, $view = null, $page =null){
        $controller = $this->getController();
        $params = Alternatiba::getCommunity($_POST);
        
        return Rest::json($params);
    }
}