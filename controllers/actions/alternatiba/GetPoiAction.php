<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers\actions\alternatiba;
use Alternatiba;
use CAction;
use Rest;

    class GetPoiAction extends \PixelHumain\PixelHumain\components\Action
    {
        public function run() {
            $controller = $this->getController();
            $params = Alternatiba::getCommunity($_POST);

            return Rest::json($params);
        }
}
