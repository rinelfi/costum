<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;

/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class CoController extends CommunecterController {


    public function beforeAction($action) {
		//parent::initPage();

    	//login auto from cookie if user not connected and checked remember
	    parent::connectCookie();
	    return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
			'index' 	=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\IndexAction::class,
			'dashboard' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\DashboardAction::class,
			'config'  	=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\ConfigAction::class,
			'export'  	=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\ExportAction::class,
	    );
	}

	/*public function actionIndex() 
	{
    	if(Yii::app()->request->isAjaxRequest)
			echo $this->renderPartial("../default/index");
	    else
    		$this->render("index");
    	//$this->redirect(Yii::app()->createUrl( "/".Yii::app()->params["module"]["parent"] ));	
  	}*/
}
