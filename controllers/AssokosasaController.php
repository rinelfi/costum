<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
class AssokosasaController extends CommunecterController {


	public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
	}

	public function actions()
	{
		return array(
			'getcommunityaction'    => \PixelHumain\PixelHumain\modules\costum\controllers\actions\assokosasa\GetCommunityAction::class,
			'getactualityaction'    => \PixelHumain\PixelHumain\modules\costum\controllers\actions\assokosasa\GetActualityAction::class,
			'getacteuraction'=>\PixelHumain\PixelHumain\modules\costum\controllers\actions\assokosasa\GetActeurAction::class,
			'getallacteuraction'=>\PixelHumain\PixelHumain\modules\costum\controllers\actions\assokosasa\GetAllActeurAction::class,
			'getvideoaction'=>\PixelHumain\PixelHumain\modules\costum\controllers\actions\assokosasa\GetVideoAction::class,
			'getimagesaction'=>\PixelHumain\PixelHumain\modules\costum\controllers\actions\assokosasa\GetImageAction::class,
			
		);
	}
}
?>