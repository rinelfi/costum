<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
class AapController extends CommunecterController {

    public function beforeAction($action)
    {
        return parent::beforeAction($action);
    }

    public function actions() {
        return array(
            'generateconfig'     => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\GenerateConfigAction::class,
            'getviewbypath' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\GetViewByPath::class,
            'deleteaap' => 'costum.controllers.actions.aap.DeleteAapAction',
            'publicrate' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\aap\PublicRateAction::class,
            'searchtags' => 'costum.controllers.actions.aap.SearchTagsAction',
        );
    }

}