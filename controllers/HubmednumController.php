<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
/**
 * CoController.php
 *
 * Cocontroller always works with the PH base 
 *
 * @author: Tibor Katelbach <tibor@pixelhumain.com>
 * Date: 14/03/2014
 */
class HubmednumController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions()
	{
	    return array(
            'getcommunityaction'    => \PixelHumain\PixelHumain\modules\costum\controllers\actions\hubmednum\GetCommunityAction::class,
            'getcommunitynewsaction'    => \PixelHumain\PixelHumain\modules\costum\controllers\actions\hubmednum\GetCommunityNewsAction::class,
            'geteventcommunityaction'		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\hubmednum\GetEventCommunityAction::class
	    );
	}
}
