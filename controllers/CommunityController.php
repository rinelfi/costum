<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
class CommunityController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
	        'dashboard'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\community\DashboardAction::class,
	        'home'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\community\HomeAction::class,
	    );
	}
	//public function actionPersondah
	/*public function actionIndex() 
	{
    	if(Yii::app()->request->isAjaxRequest)
	        echo $this->renderPartial("../default/index");
	    else
    		$this->render("index");
    	//$this->redirect(Yii::app()->createUrl( "/".Yii::app()->params["module"]["parent"] ));	
  	}*/
}