<?php

namespace PixelHumain\PixelHumain\modules\costum\controllers;

use CommunecterController;
class CordcollaborativeopenrdController extends CommunecterController {


    public function beforeAction($action) {
        //parent::initPage();
		return parent::beforeAction($action);
  	}

  	public function actions(){
	    return array(
	        
	        'dashboard'  		=> \PixelHumain\PixelHumain\modules\costum\controllers\actions\cordCollaborativeOpenRd\DashboardAction::class,
	        'ocecodashboard' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\cordCollaborativeOpenRd\OcecodashboardAction::class,
	        'ocecoglobaldashboard' => \PixelHumain\PixelHumain\modules\costum\controllers\actions\cordCollaborativeOpenRd\OcecoglobaldashboardAction::class
	    );
	}
	//public function actionPersondah
	/*public function actionIndex() 
	{
    	if(Yii::app()->request->isAjaxRequest)
	        echo $this->renderPartial("../default/index");
	    else
    		$this->render("index");
    	//$this->redirect(Yii::app()->createUrl( "/".Yii::app()->params["module"]["parent"] ));	
  	}*/
}
