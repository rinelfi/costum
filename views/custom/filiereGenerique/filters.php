<?php

if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
	$el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
}

?>

<script type="text/javascript">
    var pageApp= window.location.href.split("#")[1];
    var appConfig=<?php echo json_encode(@$appConfig); ?>;

	var thematic = null;

	var defaultType = {};

	if(costum && typeof costum.lists != "undefined" && typeof costum.lists.types != "undefined"){
		defaultType = Object.keys(costum.lists.types);
		//Object.assign(defaultType, costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["group"]["options"]);
	}else{
		Object.assign(defaultType, {"NGO" : trad.ong,
				"LocalBusiness" : trad.entreprise,
				"GovernmentOrganization" : trad.servicepublic})
	}

	if(costum && typeof costum.lists != "undefined" && typeof costum.lists.theme != "undefined"){
		thematic = {};
		Object.assign(thematic, costum.lists.theme);
	}

	var defaultScopeList = [];

	if(costum && costum.slug && costum.slug=="ries"){
		defaultScopeList = ["IT"];
	}else{
		defaultScopeList = ["FR", "RE"];
	}

	var paramsFilter= {
		container : "#filters-nav",
	 	loadEvent : {
	 		default : "scroll"
	 	},
		defaults : {
		 	types : (appConfig.filters && appConfig.filters.types)?appConfig.filters.types:["organizations"],
		},
		filters : {
		 	theme : {
	 			view : "megaMenuDropdown",
	 			type : "tags",
	 			remove0: true,
	 			countResults: true,
	 			name : "<?php echo Yii::t("common", "search by theme")?>",
	 			event : "tags",
	 			keyValue: true,
	 			list : thematic
	 		},
	 		types : {
		 		view : "types",
		 		type : "types",
		 		name : "<?php echo Yii::t("common", "search by type")?>",
		 		event : "types",
		 		lists : defaultType
		 	},
		 	network : {
		 		view : "dropdownList",
		 		type : "filters",
		 		field: "extraInfo.reteofapartment",
		 		name : "<?php echo Yii::t("common", "search by network")?>",
		 		event : "filters",
		 		lists : ["RIES", "RESS ROMA"]
		 	},
 			
 			scopeList : {
	 			name : "<?php echo Yii::t("common", "Search by place")?>",
	 			params : {
	 				countryCode : defaultScopeList, 
	 				level : ["3"]
	 			}
	 		},
	 		
	 		text : true
	 	},
	 	results : {
			renderView: "directory.classifiedPanelHtml",
		 	smartGrid : true
		},
	}

	if(pageApp == "projects"){
		delete paramsFilter.filters["type"];
	}

	if(thematic==null || pageApp=="projects"){
		delete paramsFilter.filters["theme"];
	}

	if(costum.slug!="ries" || pageApp=="projects"){
		delete paramsFilter.filters["network"];
	}

	if(costum["dataSource"]){
		paramsFilter["defaults"]["sourceKey"] = costum["dataSource"];
	}

	jQuery(document).ready(function() {
		filterSearch = searchObj.init(paramsFilter);
		$(".count-badge-filter").remove();
		
		if(typeof paramsFilter.filters["network"] != "undefined"){filterSearch.filters.actions.themes.callBack = function(data){
				filterSearch.filters.actions.themes.isLoaded=false;
				let network = [];
				for (const [key, value] of Object.entries(data)) {
					//paramsFilter.filters.network 
					if(typeof value.extraInfo != "undefined" && typeof value.extraInfo.reteofapartment != "undefined" && value.extraInfo.reteofapartment != ""){
						if(Array.isArray(value.extraInfo.reteofapartment)){
							let rete = value.extraInfo.reteofapartment;
							network.push(...rete);
						}else{
							network.push(value.extraInfo.reteofapartment);
						}
					}
				}
				paramsFilter.filters["network"]["list"] = Array.from(new Set(network))
				filterSearch.filters.initViews(filterSearch, paramsFilter);
				filterSearch.filters.initEvents(filterSearch, paramsFilter);
				filterSearch.filters.initDefaults(filterSearch, paramsFilter);

				filterSearch.filters.actions.themes.setThemesCounts(filterSearch);
				filterSearch.filters.actions.themes.callBack = function(res){}
			}
		}
	});

</script>