<?php
$cssAnsScriptFilesModule = array(
  '/js/filiereGenerique/filiereGenerique_index.js',
  '/js/filiereGenerique/filters.js',
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->getModule( Costum::MODULE )->getAssetsUrl() );
?>

<style>
  .main-container{
    padding-left: 0px !important;
    padding-top: 0px !important;
  }

  #search-content{
    padding-left:0px !important;
    margin-left:0px !important;
    min-height: 10px !important;
    overflow: hidden !important
  }

  .container-filters-menu, #filters-nav{
    left: 0px !important;
  }

  button.filter-btn-hide-map, .btn-show-map{
    display: none !important;
  }

  .leaflet-popup-tip-container {
    bottom: -29px;
  }

  #mapContent{
    min-height: 400px !important;
    left: 0px !important;
    top: 0px !important;
  }

</style>

<script type="text/javascript">
  
  jQuery(document).ready(function() {
    $("#search").remove();
    $("#menuLeft, .btn-more, #vertical, .modal").remove();
    $(".headerSearchContainer").remove();
    $(".footerSearchContainer").remove();
    createFilters("#filters-nav", true);
    filterSearch = searchObj.init(paramsFilter);
  });
</script>