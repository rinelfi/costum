<?php 
$cssJS = array(
    // '/plugins/gridstack/css/gridstack.min.css',
    // '/plugins/gridstack/js/gridstack.js',
    // '/plugins/gridstack/js/gridstack.jQueryUI.min.js'

);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl);


HtmlHelper::registerCssAndScriptsFiles(array( 
  '/css/graphbuilder.css',
  '/js/form.js',
  '/js/dashboard.js'
  ), Yii::app()->getModule( Survey::MODULE )->getAssetsUrl() );

echo $this->renderPartial("costum.views.custom.cordCollaborativeOpenRd.dashboardstyle", []);
?>
<!-- #ebebeb -->
<div class="ocecoform-body">
    <div class="col-md-12" style="margin-bottom: 40px; ">
        <div class="col-md-offset-2 col-md-8">
            <h1 class="" style="text-transform: none; text-align: center">Observatoire du projet <?php echo (isset($title) ? $title : "" ) ?> </h1>
        </div>
    </div>

    <div class="col-md-12" id="bodygraph" >
      
        <!-- level 01 -->
        <div class="surveys grid survey-grid" style="display: flex;background-color: #ebebeb">

              <!-- change to dymamic  -->
              <div class="survey-item squareTile flex1 visiblehovercontainer grid-stack-item" data-gs-width="4" data-gs-height="2" id="">
                    <div class="grid-stack-item-content" >
                        <div class="tilelabel ocecotitle oceco-gradient" style="">
                              Evolution
                        </div>
                        <div class="tilelabel ocecotitle oceco-gradient" style="">
                              des tâches
                        </div>


                        <div id="progressTaskChart" class="pdt15">
                        </div>
                    </div>
              </div>

              <div class="survey-item headerTile flex8 visiblehovercontainer grid-stack-item" data-gs-width="4" data-gs-height="2" style="height: 100px;" id="">
                    <div class="grid-stack-item-content" >
                        <div class="ocecotitle step" style="">
                          Tâches
                          <!-- <div class="progress">
                              <div class="progress-bar" role="progressbar" aria-valuenow="72"
                              aria-valuemin="0" aria-valuemax="100" style="width:69%">
                                <span class="sr-only">79% Complete</span>
                              </div>
                            </div> -->
                        </div>

                        <div id="" class="divitemheader">
                            <div class="itemheaderTile">
                                <!-- <div class="ocecotitle step"> Proposer</div>
                                <div class="ocecotitle"><i class="fa fa-3 fa-check-circle success" aria-hidden="true"></i></div> -->
                                <div class="ocecotitle state success">Au total</div> 
                                <div class="ocecotitle step3" id="totalTasks"></div>
                                <div style="margin: 5px; text-align: center;">
                                   
                                </div>
                            </div>
                            
                            <div class="dividerT"></div>
                                
                            <div class="itemheaderTile">
                                <!-- <div class="ocecotitle step"> Décider</div>
                                <div class="ocecotitle"><i class="fa fa-3 fa-check-circle success" aria-hidden="true"></i></div> -->
                                <div class="ocecotitle state success">En cours</div> 
                                <div class="ocecotitle step3" id="currentTasks"></div>
                                <div style="margin: 5px; text-align: center;">
                                  <div class="" style="font-size: 13px;color : green"><i class="fa fa-caret-up"></i> + <span id="totalTaskstoday"></span> aujourd'hui</div>
                                  <div class="" style="font-size: 13px;color : green"><i class="fa fa-caret-up"></i> + <span id="totalTasksweek"></span> cette semaine</div>
                                </div>
                            </div>

                            <div class="dividerT"></div>

                            <div class="itemheaderTile">
                                <!-- <div class="ocecotitle step"> Financer</div>
                                <div class="ocecotitle" style="position: relative;"><div class="fifi" id="headerFinancerChart"></div></div>
                                <div class="ocecotitle state success">En cours</div> -->
                                <div class="ocecotitle state success">Términée</div> 
                                <div class="ocecotitle step3" id="completeTasks"></div>
                                  <div style="margin: 5px; text-align: center;">
                                  <div class="" style="font-size: 13px;color : green"><i class="fa fa-caret-up"></i> + <span id="completeTaskstoday"></span> aujourd'hui</div>
                                  <div class="" style="font-size: 13px;color : green"><i class="fa fa-caret-up"></i> + <span id="completeTasksweek"></span> cette semaine </div> 
                                </div>
                            </div>

                            <div class="dividerT"></div>
                            
                            <div class="itemheaderTile">
                                <!-- <div class="ocecotitle step"> Suivre</div>
                                <div class="ocecotitle"><i class="fa fa-3 fa-circle warning" aria-hidden="true"></i></div>
                                <div class="ocecotitle state success">En attente</div> -->
                                <div class="ocecotitle state success">Validée</div> 
                                <div class="ocecotitle step3" id="validTasks"></div>
                                  <div style="margin: 5px; text-align: center;">
                                  <div class="" style="font-size: 13px;color : green"><i class="fa fa-caret-up"></i> + <span id="validTaskstoday"></span> aujourd'hui</div>
                                  <div class="" style="font-size: 13px;color : green"><i class="fa fa-caret-up"></i> + <span id="validTasksweek"></span> cette semaine </div> 
                                </div>

                            </div>

                        </div>
                    </div>
              </div>

              <div class="survey-item squareTile successSquare flex1 visiblehovercontainer grid-stack-item" data-gs-width="4" data-gs-height="2" id="">
                    <div class="grid-stack-item-content" >
                        <div class="tilelabel ocecotitle" style="">
                              Date de
                        </div>
                        <div class="tilelabel ocecotitle" style="">
                              démarrage
                        </div>

                        <div class="tilelabel ocecotitle oceco-gradient">
                                <i class="fa fa-3 fa-flag-checkered success" aria-hidden="true"></i>
                        </div>

                        <div class="itemheaderTile">
                                <div class="ocecotitle state success" id="startDate"></div>
                                <div class="ocecotitle step"> il y a <span id="startDateC"></span>j</div>
                        </div>
                        </div>
                    </div>
        </div>

        <!-- level 02 -->
        <div class="surveys grid survey-grid" style="display: flex;background-color: #ebebeb">

              <!-- change to dymamic  -->
              <div class="survey-item squareTile flex1 visiblehovercontainer grid-stack-item" data-gs-width="4" data-gs-height="2" id="">
                    <div class="grid-stack-item-content" >
                        <div class="tilelabel ocecotitle oceco-gradient" style="">
                               tache fermée
                        </div>
                       
                        <div id="activetask" class="pdt15">
                            
                        </div>
                    </div>
              </div>

              <div class="survey-item headerTile flex8 visiblehovercontainer grid-stack-item" data-gs-width="4" data-gs-height="2" style="height: 100px;" id="">
                    <div class="grid-stack-item-content" >
                        <!-- <div class="" style="">
                          <div class="progress">
                              <div class="progress-bar" role="progressbar" aria-valuenow="72"
                              aria-valuemin="0" aria-valuemax="100" style="width:69%">
                                <span class="sr-only">79% Complete</span>
                              </div>
                            </div>
                        </div> -->

                        <div id="" class="divitemheader">
                            <div class="itemheaderTile" id="proposerStep">
                            </div>
                            
                            <div class="dividerT"></div>
                                
                            <div class="itemheaderTile" id="">
                              <div class="ocecotitle step"> Decider</div>
                                <div class="ocecotitle" style="position: relative;"><div class="fifi ocecotitle" id="deciderStep"></div></div>
                                <div class="ocecotitle state success" id = "deciderStepL">Completé</div>
                                <div class="ocecotitle"><button class="apercu"><i class = "fa fa-folder-open-o"></i></button></div>
                            </div>

                            <div class="dividerT"></div>

                            <div class="itemheaderTile">
                                <div class="ocecotitle step"> Financer</div>
                                <div class="ocecotitle" style="position: relative;"><div class="fifi ocecotitle" id="financerStep"></div></div>
                                <div class="ocecotitle state success" id = "financerStepL"></div>
                                <div class="ocecotitle"><button class="apercu"><i class = "fa fa-folder-open-o"></i></button></div>
                            </div>

                            <div class="dividerT"></div>
                            
                            <div class="itemheaderTile">
                                <div class="ocecotitle step"> Suivre</div>
                                <div class="ocecotitle" style="position: relative;"><div class="fifi ocecotitle" id="suivreStep"></div></div>
                                <div class="ocecotitle state success" id = "suivreStepL"></div>
                                <div class="ocecotitle"><button class="apercu"><i class = "fa fa-folder-open-o"></i></button></div>
                            </div>

                        </div>
                    </div>
              </div>

              <div class="survey-item squareTile successSquare flex1 visiblehovercontainer grid-stack-item" data-gs-width="4" data-gs-height="2" id="">
                    <div class="grid-stack-item-content" >
                        <div class="tilelabel ocecotitle" style="">
                              Date de
                        </div>
                        <div class="tilelabel ocecotitle" style="">
                              fin
                        </div>

                        <div class="tilelabel ocecotitle oceco-gradient">
                                <i class="fa fa-3 fa-clock-o success" aria-hidden="true"></i>
                        </div>

                        <div class="itemheaderTile">
                                <div class="ocecotitle state success" id="endDate"></div>
                                <div class="ocecotitle step"> dans <span id="endDateC"></span>j</div>
                        </div>
                        </div>
                    </div>
        </div>

        <!--level 03 -->
        <div class="surveys grid survey-grid" style="display: flex;background-color: #ebebeb">

              <div class="survey-item squareTileL flex3 visiblehovercontainer grid-stack-item " data-gs-width="4" data-gs-height="2" style="height: 100px;" id="">
                    <div class="grid-stack-item-content" >
                        <div class="tilelabel ocecotitle oceco-gradient oceco-h1" style="">
                             Tache en cours par personne
                        </div>
                        <div class="">
                          <div class="pdt15" id = "tasksperperson">
    
                          </div>
                        </div>
                    </div>
              </div>

              <!-- change to dymamic  -->

              <!--               <div class="survey-item squareTileL flex3 visiblehovercontainer grid-stack-item overflowsc" data-gs-width="4" data-gs-height="2" style="height: 100px;" id="">
                    <div class="grid-stack-item-content" >
                        <div class="tilelabel ocecotitle" style="">
                             Timeline des taches fermée
                        </div>
                        <div class="">
                          <div class="ocecotimeline-items" id = "completetaskstimeline">
    
                          </div>
                        </div>
                    </div>
              </div> -->

              <div class="survey-item squareTileL flex3 visiblehovercontainer grid-stack-item overflowsc" data-gs-width="4" data-gs-height="2" style="height: 100px;" id="">
                    <div class="grid-stack-item-content" >
                        <div class="tilelabel ocecotitle oceco-gradient oceco-h1" style="">
                             Timeline des taches fermée
                        </div>
                        <div class="">
                          <div class="ocecotimeline-items">
                            <table class="ocecotable-list">
                              <thead>
                                <tr>
                                  <th class="ocecotable-th"></th>
                                  <th class="ocecotable-th"><div>Tâche</div></th>
                                  <th class="ocecotable-th"><div>Membre</div></th>
                                </tr>
                              </thead>
                              <tbody id = "completetaskstimeline">
                                
                              </tbody>
                            </table>
    
                          </div>
                        </div>
                    </div>
              </div>

                <!--               <div class="survey-item squareTileL flex2 visiblehovercontainer grid-stack-item overflowsc" data-gs-width="4" data-gs-height="2" style="height: 100px;" id="">
                    <div class="grid-stack-item-content" >
                        <div class="tilelabel ocecotitle" style="">
                             Taches en retard
                        </div>
                        <div class="">
                          <div class="ocecocard-items" id = "outdatetaskscard">
    
                          </div>
                        </div>

                </div>
              </div> -->
              <div class="survey-item squareTileL flex3 visiblehovercontainer grid-stack-item overflowsc" data-gs-width="4" data-gs-height="2" style="height: 100px;" id="">
                    <div class="grid-stack-item-content" >
                        <div class="tilelabel ocecotitle oceco-h1 oceco-gradient" style="">
                              Taches en retard 
                        </div>
                        <div class="">
                          <div class="ocecocard-items" >
                            <table class="ocecotable-list">
                              <thead>
                                <tr>
                                  <th class="ocecotable-th"><div>Tâche</div></th>
                                  <th class="ocecotable-th"><div>Membre</div></th>
                                  <th class="ocecotable-th"><div>Date fin</div></th>
                                </tr>
                              </thead>
                              <tbody id = "outdatetaskscard">
                                
                              </tbody>
                            </table>
    
                          </div>
                        </div>

                </div>
              </div>

        </div>

        <!--level 04 -->
        <div class="surveys grid survey-grid" style="display: flex;background-color: #ebebeb">

              <!-- change to dymamic  -->



              <div class="survey-item squareTileL flex3 visiblehovercontainer grid-stack-item " data-gs-width="4" data-gs-height="2" style="height: 100px;" id="">
                    <div class="grid-stack-item-content" >
                        <div class="tilelabel ocecotitle oceco-gradient oceco-h1" style="">
                             Financement par type de travaux 
                        </div>
                        <div class="">
                          <div class=" " id = "finpertype" >
    
                          </div>
                        </div>
                        <div class="tilelabel ocecotitle" style="">
                             
                        </div>
                    </div>
              </div> 

              <div class="survey-item squareTileL flex3 visiblehovercontainer grid-stack-item evopaymentchart" data-gs-width="4" data-gs-height="2" style="height: 100px;" id="">
                    <div class="grid-stack-item-content" >
                        <div class="tilelabel ocecotitle ocecotitle1 oceco-gradient" style="">
                             Evolution des payement
                        </div>
                        <div class="tilelabel ocecotitle" style="color: grey">
                             par semaine <i class="fa fa-caret-down" style="font-size: 15px" ></i>
                        </div>

                        <div id="financerline" class=""></div>

                </div>
              </div>

              <div class="survey-item squareTileL flex2 visiblehovercontainer grid-stack-item evopaymentnum" data-gs-width="4" data-gs-height="2" style="height: 100px;" id="">
                    <div class="grid-stack-item-content" >
                        <div class="tilelabel ocecotitle" style="">
                             <br>
                        </div>
                        <div class="tilelabel ocecotitle" style="">
                             <br>
                        </div>
                        <div id="" class="">
                          <div class="">
                          <div class="ocecoval-items" id = "">
                            <div class="ocecoval-item">
                                
                                <div class="ocecoval-desc">
                                    <span class="timelinetitle">Montant demandé à financer</span>
                                    <p class="timelinetext" id="amounttofin">0</p>
                                </div>
                            </div>

                            <div class="ocecoval-item ">
                                
                                <div class="ocecoval-desc">
                                    <span class="timelinetitle">Montant Financer dépensé </span>
                                    <p class="timelinetext" id="amountinfin">0</p>
                                </div>
                            </div>

                            <div class="ocecoval-item">
                                
                                <div class="ocecoval-desc">
                                    <span class="timelinetitle">Montant Financer en cours de réalisation </span>
                                    <p class="timelinetext" id="amountfin">0</p>
                                </div>
                            </div>

                          </div>
                        </div>
      
                        </div>

                </div>
              </div>
             
        </div>

        <!--level 05 -->
        <div class="surveys grid survey-grid" style="display: flex;background-color: #ebebeb">

            <div class="survey-item squareTileL flex3 visiblehovercontainer grid-stack-item " data-gs-width="4" data-gs-height="2" style="height: 100px;" id="">
                    <div class="grid-stack-item-content" >
                        <div class="tilelabel ocecotitle oceco-gradient oceco-h1" style="">
                             Retour qualité
                        </div>
                        <div class="">
                          <div class="ocecotimeline-items" id = "feedback">
    
                          </div>
                        </div>
                    </div>
              </div>

             
        </div>

    </div>
    
</div>

<script type="text/javascript">
    jQuery(document).ready(function() {

        //overall progress
        // ajaxPost('#overallProgressChart', baseUrl+'/graph/co/dash/g/graph.views.co.observatory.halfDoughnut'+"/id/overallProgressChart", null, function(){},"html");

        //header financer
        // ajaxPost('#headerFinancerChart', baseUrl+'/graph/co/dash/g/graph.views.co.observatory.doughnut'+"/id/headerFinancerChart", null, function(){},"html");

        // ajaxPost('#fibarChart', baseUrl+'/graph/co/dash/g/graph.views.co.ocecoform.bar'+"/id/fibarChart", null, function(){},"html");
        // ajaxPost('#pietaChart', baseUrl+'/graph/co/dash/g/graph.views.co.ocecoform.pie'+"/id/pietaChart", null, function(){},"html");
        // ajaxPost('#bartChart', baseUrl+'/graph/co/dash/g/graph.views.co.ocecoform.bart'+"/id/bartChart", null, function(){},"html");
        // ajaxPost('#evoChart', baseUrl+'/graph/co/dash/g/graph.views.co.ocecoform.line'+"/id/evoChart", null, function(){},"html");

    //        ocecoform.allData = <?php echo json_encode($allData) ?>;

    // ocecoform.initvalues(ocecoform);
    // ocecoform.initviews(ocecoform);
    });

      var standartcolors = [
            'rgba(46, 204, 113, 1)',
            'rgba(224, 224, 0, 1)',
            'rgba(221, 221, 221, 1)'
        ];

      var bigstandartcolors = [
            // "#F7464A",
            'rgba(46, 204, 113, 1)',
            "#46BFBD",
            "#949FB1",
            "#4D5360",
      ];

      var defaultimgprofil = '<?php echo Yii::app()->getModule('co2')->assetsUrl.'/images/thumb/default_citoyens.png'; ?>';

      var groupe = ["Feature" , "Costume" , "Chef de projet" , "Data" , "Maintenance"];

    var ocecoform = {
      allData : <?php echo json_encode($allData) ?> ,

      project : <?php echo json_encode($project) ?>,

      actions : <?php echo json_encode($action) ?> ,

      title : '<?php echo $title; ?>',

      feedback : <?php echo json_encode($feedback) ?> ,

      tiles : {
        "totalTasks" : {
            type : "html",
            getandsetDatafunc : function(ocecoform){
              var initialValue = [];
              var tasks = ocecoform.mergeArray(Object.values(ocecoform.actions), "path", "tasks");
              ocecoform.tiles["totalTasks"].values = tasks.length;
              ocecoform.tiles["totalTaskstoday"].values = ocecoform.countToday(tasks, "createdAt");
              ocecoform.tiles["totalTasksweek"].values = ocecoform.countWeek(tasks, "createdAt");
            },
            values : 0
        },
        "totalTaskstoday" : {
            type : "html",
            getandsetDatafunc : function(ocecoform){
             
            },
            values : 0
        },
        "totalTasksweek" : {
            type : "html",
            getandsetDatafunc : function(ocecoform){
             
            },
            values : 0
        },
        "currentTasks" : {
            type : "html",
            getandsetDatafunc : function(ocecoform){
              var tasks = ocecoform.mergeArray(Object.values(ocecoform.actions), "path", "tasks");
              var com = ocecoform.selectOccur(tasks, true, "checked");
              ocecoform.tiles["currentTasks"].values = tasks.length - com.length;
            },
            values : 0
        },
        "completeTasks" : {
            type : "html",
            getandsetDatafunc : function(ocecoform){
              var tasks = ocecoform.mergeArray(Object.values(ocecoform.actions), "path", "tasks");
              var curr = ocecoform.selectOccur(tasks, true, "checked");
              ocecoform.tiles["completeTasks"].values = curr.length;
              ocecoform.tiles["completeTaskstoday"].values = ocecoform.countToday(curr, "checkedAt");
              ocecoform.tiles["completeTasksweek"].values = ocecoform.countWeek(curr, "checkedAt");
            },
            values : 0
        },
        "completeTaskstoday" : {
            type : "html",
            getandsetDatafunc : function(ocecoform){
              
            },
            values : 0
        },
        "completeTasksweek" : {
            type : "html",
            getandsetDatafunc : function(ocecoform){
              
            },
            values : 0
        },
        "validTasks" : {
            type : "html",
            getandsetDatafunc : function(ocecoform){
              var tasks = ocecoform.mergeArray(Object.values(ocecoform.actions), "path", "tasks");
              var curr = ocecoform.selectOccur(tasks, true, "checked");
              ocecoform.tiles["validTasks"].values = curr.length;
              ocecoform.tiles["validTaskstoday"].values = ocecoform.countToday(curr, "checkedAt");
              ocecoform.tiles["validTasksweek"].values = ocecoform.countWeek(curr, "checkedAt");
            },
            values : 0
        },  
        "validTaskstoday" : {
            type : "html",
            getandsetDatafunc : function(ocecoform){
              var tasks = ocecoform.mergeArray(Object.values(ocecoform.actions), "path", "tasks");
              var curr = ocecoform.selectOccur(tasks, true, "checked");
              ocecoform.tiles["validTasks"].values = curr.length;
            },
            values : 0
        },
        "validTasksweek" : {
            type : "html",
            getandsetDatafunc : function(ocecoform){
              var tasks = ocecoform.mergeArray(Object.values(ocecoform.actions), "path", "tasks");
              var curr = ocecoform.selectOccur(tasks, true, "checked");
              ocecoform.tiles["validTasks"].values = curr.length;
            },
            values : 0
        },
        "progressTaskChart" : {
            id : "progressTaskChart",
            type : "chart",
            getandsetDatafunc : function(ocecoform){
              var progressTaskChartData = {};
              if (ocecoform.tiles["totalTasks"].values != 0) {
                progressTaskChartData.percentLabel = (ocecoform.tiles["completeTasks"].values / ocecoform.tiles["totalTasks"].values ) * 100;
                progressTaskChartData.data = [ocecoform.tiles["completeTasks"].values, 0, ocecoform.tiles["currentTasks"].values ];
                progressTaskChartData.label = '# of Votes';
                progressTaskChartData.labels = ["Done", "P", "UnDone"];
                progressTaskChartData.g = 'graph.views.co.ocecoform.halfDoughnut';
                progressTaskChartData.colors = standartcolors;
                ocecoform.tiles["progressTaskChart"].values = progressTaskChartData;
              }
            },
            values : {
                percentLabel : 0,
                data : [0, 0, 100],
                label : '# of Votes',
                labels : ["Done", "P", "UnDone"],
                g : 'graph.views.co.ocecoform.halfDoughnut',
                colors : standartcolors
            }
        },
        "startDate" : {
            type : "html",
            getandsetDatafunc : function(ocecoform){
              if (typeof ocecoform.project != "undefined" && typeof ocecoform.project.startDate != "undefined") {
                  ocecoform.tiles["startDate"].values = ocecoform.project.startDate;
              }
            },
            values : "Soon"
        }, 
        "startDateC" : {
            type : "html",
            getandsetDatafunc : function(ocecoform){
              if (typeof ocecoform.project != "undefined" && typeof ocecoform.project.startDate != "undefined") {
                  ocecoform.tiles["startDateC"].values = ocecoform.countDays(ocecoform.convertDate(ocecoform.project.startDate), new Date());
              }
            },
            values : "---"
        }, 
        "endDate" : {
            type : "html",
            getandsetDatafunc : function(ocecoform){
                var tasks = ocecoform.mergeArray(Object.values(ocecoform.actions), "path", "tasks");
                var orderedTasks = ocecoform.sortbyDate(tasks, "endDate", false);
                if (typeof orderedTasks[0] != "undefined" && typeof orderedTasks[0].endDate != "undefined") {
                    ocecoform.tiles["endDate"].values = orderedTasks[0].endDate;
                }
            },
            values : "Soon"
        },
        "endDateC" : {
            type : "html",
            getandsetDatafunc : function(ocecoform){
              if (ocecoform.tiles["endDate"].values != "Soon") {
                  ocecoform.tiles["endDateC"].values = ocecoform.countDays( new Date(), ocecoform.convertDate(ocecoform.tiles["endDate"].values));
              }
            },
            values : "---"
        }, 
        "proposerStep" : {
            type : "html",
            getandsetDatafunc : function(ocecoform){
              if (ocecoform.title != "" && ocecoform.allData) {
                  ocecoform.tiles["proposerStep"].values = '<div class="ocecotitle step"> Proposer</div><div class="ocecotitle"><i class="fa fa-3 fa-check-circle success" aria-hidden="true"></i></div><div class="ocecotitle state success">Completé</div><div class="ocecotitle"><button class="apercu"><i class = "fa fa-folder-open-o"></i></button></div>';
              } else {
                  ocecoform.tiles["proposerStep"].values = '<div class="ocecotitle step"> Proposer</div><div class="ocecotitle"><i class="fa fa-3 fa-circle warning" aria-hidden="true"></i></div><div class="ocecotitle state success">En cours</div><div class="ocecotitle"><button class="apercu"><i class = "fa fa-folder-open-o"></i></button></div>';
              } 
            },
            values : '<div class="ocecotitle step"> Proposer</div><div class="ocecotitle"><i class="fa fa-3 fa-circle warning" aria-hidden="true"></i></div><div class="ocecotitle state success">En cours</div><div class="ocecotitle"><button class="apercu"><i class = "fa fa-folder-open-o"></i></button></div>'
        }, 
        "deciderStep" : {
            type : "html",
            callback : function(){
                if (ocecoform.tiles["deciderStep"].type == "chart") {
                  $('#deciderStepL').html("En cours");
                }
            },
            getandsetDatafunc : function(ocecoform){
              var tt = ocecoform.mergeArray(Object.values(ocecoform.allData), "path", "votes");
              var allarrdec = [];

              $.each(tt, function(ttid, ttval){
                  var ghg = ocecoform.mergeArray(Object.values(tt[ttid]), "path", "vote");
                  allarrdec = [...allarrdec, ...ghg];
              });

              var stt = allarrdec.reduce(function(a, b){
                  return parseInt(a) + parseInt(b);
              }, 0);

              if ((stt / allarrdec.length) == 100 ) {
                  ocecoform.tiles["deciderStep"].values = '<i class="fa fa-3 fa-check-circle success" aria-hidden="true"></i>';
              } else {
                  ocecoform.tiles["deciderStep"].type = "chart";


                  var deciderStepChartData = {};

                    deciderStepChartData.percentLabel = (stt / allarrdec.length);
                    // deciderStepChartData.percentLabel = Math.round((tt.length / ocecoform.allData.length ) * 100);

                    deciderStepChartData.data = [stt / allarrdec.length, 0, 100 - (stt / tt.length) ];

                    deciderStepChartData.label = '';

                    deciderStepChartData.labels = ["Done", "", "UnDone"];
                    deciderStepChartData.g = 'graph.views.co.ocecoform.doughnut';
                    deciderStepChartData.id = "deciderStep";

                    deciderStepChartData.colors = standartcolors;
                    ocecoform.tiles["deciderStep"].values = deciderStepChartData;

              } 
            },
            values : '<i class="fa fa-3 fa-circle warning" aria-hidden="true"></i>'
        },    
        "financerStep" : {
            type : "html",
            callback : function(){
                if (ocecoform.tiles["financerStep"].type == "chart") {
                  $('#financerStepL').html("En cours");
                }
            },
            getandsetDatafunc : function(ocecoform){
              var price = ocecoform.mergeArray(Object.values(ocecoform.allData), "path", "price");
              var sumprice = ocecoform.sumInt(price);

              var tt = ocecoform.mergeArray(Object.values(ocecoform.allData), "path", "financer");
              var ttjk = ocecoform.mergeArray(tt, "path", "amount");
              var sumamount = ocecoform.sumInt(ttjk);

              var percentv = (sumamount / sumprice) * 100;
              
              if (percentv > 100) {
                  ocecoform.tiles["financerStep"].values = '<i class="fa fa-3 fa-check-circle success" aria-hidden="true"></i>';
              } else {
                  ocecoform.tiles["financerStep"].type = "chart";


                  var financerStepChartData = {};

                    financerStepChartData.percentLabel = percentv;

                    financerStepChartData.data = [percentv, 0, 100 - percentv ];

                    financerStepChartData.label = '';

                    financerStepChartData.labels = ["Done", "", "UnDone"];
                    financerStepChartData.g = 'graph.views.co.ocecoform.doughnut';
                    financerStepChartData.id = "financerStep";

                    financerStepChartData.colors = standartcolors;
                    ocecoform.tiles["financerStep"].values = financerStepChartData;

              } 
            },
            values : '<i class="fa fa-3 fa-circle warning" aria-hidden="true"></i>'
        }, 
        "suivreStep" : {
            type : "html",
            getandsetDatafunc : function(ocecoform){
              if (ocecoform.tiles.progressTaskChart.values.percentLabel == 100) {
                  ocecoform.tiles["suivreStep"].values = '<i class="fa fa-3 fa-check-circle success" aria-hidden="true"></i>';
              } else if(typeof ocecoform.project != "undefined" && typeof ocecoform.project.id != "undefined") {
                  ocecoform.tiles["suivreStep"].type = "chart";


                  var suivreStepChartData = {};

                    suivreStepChartData = {...ocecoform.tiles.progressTaskChart.values};

                    suivreStepChartData.g = 'graph.views.co.ocecoform.doughnut';
                    suivreStepChartData.id = "suivreStep";

                    ocecoform.tiles["suivreStep"].values = suivreStepChartData;

                    ocecoform.tiles["suivreStep"].callback = function(){
                        $('#suivreStepL').html("en cours");
                    }

              } else {
                ocecoform.tiles["suivreStep"].type = "html";
                  ocecoform.tiles["suivreStep"].values = '<i class="fa fa-3 fa-circle warning" aria-hidden="true"></i>';
                  ocecoform.tiles["suivreStep"].callback = function(){
                        $('#suivreStep').removeClass("fifi");
                        $('#suivreStepL').html("en attente");
                  }
              }
            },
            values : '<i class="fa fa-3 fa-circle warning" aria-hidden="true"></i>'
        }, 
        "activetask" : {
          type : "chart",
          getandsetDatafunc : function(ocecoform){
            var tasks = ocecoform.mergeArray(Object.values(ocecoform.actions), "path", "tasks");
            var curr = ocecoform.selectOccur(tasks, true, "checked");
            var kkkd = ocecoform.groupWeeks(curr, "checkedAt" ,"", "count", ocecoform.tiles["totalTasks"].values, true);
                ocecoform.tiles["activetask"].values.data = ocecoform.mergeArray(kkkd, "path", "count" );
                ocecoform.tiles["activetask"].values.labels = ocecoform.mergeArray(kkkd, "path", "weekStart" );
           },
          values : {
                id : "activetask",
                data : [5, 6, 20, 18, 17, 15, 10, 9, 8, 7, 2],
                label : '# of Votes',
                labels : ["a","a","a","a","a","a","a","a","a","a","a"],
                g : 'graph.views.co.ocecoform.line',
                colors : standartcolors
            }
        },
        "completetaskstimeline" : {
            type : "list",
            getandsetDatafunc : function(ocecoform){
                var tasks = ocecoform.mergeArray(Object.values(ocecoform.actions), "path", "tasks");

                var newtasks = [];
                
                $.each(tasks , function(tsId, tsVl){
                  if (typeof tsVl.endDate != "undefined") {
                    if (ocecoform.countDays(ocecoform.convertDate(tsVl.endDate), new Date()) < 0) {

                      var profilpicture = "";
                      if (typeof tsVl.contributors != "undefined") {
                        
                        $.each(tsVl.contributors , function(tsIdcon, tsVlcon){

                          var infodata = [];

                          infodata = ocecoform.getUserData(tsIdcon);

                          if (
                            typeof infodata != "undefined" &&
                            typeof infodata.map != "undefined" &&
                            typeof infodata.map.profilImageUrl != "undefined" &&
                            infodata.map.profilImageUrl != ""
                            ) {
                            profilpicture += '<a href="javascript:;" class="oceco-avatar" data-bs-toggle="tooltip" data-bs-placement="bottom" title="" data-bs-original-title="'+infodata.map.name+'"><img  id="menu-thumb-profil"  src="'+infodata.map.profilImageUrl+'" alt="image"></a>';
                          }else{
                            profilpicture += '<a href="javascript:;" class="oceco-avatar" data-bs-toggle="tooltip" data-bs-placement="bottom" title="" data-bs-original-title="'+infodata.map.name+'"><img class="" id="menu-thumb-profil" src="'+defaultimgprofil+'" alt="image"></a>';
                          }
                        });
                      }
                      tasks[tsId]["profilpicture"] = profilpicture;

                      newtasks.push(tasks[tsId]);
                    }
                  }
                });

                var orderedTasks = ocecoform.sortbyDate(newtasks, "checkedAt", true);
                ocecoform.tiles["completetaskstimeline"].values = orderedTasks;
            },
            template : function(listvalue){
              // return '<div class="ocecotimeline-item">'+
              //         '<div class="ocecotimeline-icon">'+
              //             '<i class="fa fa-circle"></i>'+
              //         '</div>'+
              //         '<div class="ocecotimeline-desc">'+
              //             '<span class="timelinetitle">'+listvalue.task+'</span>'+
              //             '<p class="timelinetext">'+new Date().getHours()+':'+new Date().getMinutes()+' '+new Date().getDate()+'/'+new Date().getMonth()+'/'+new Date().getYear()+'</p>'+
              //         '</div>'+
              //       '</div>';
              return '<tr class="ocecotable-tr">'+
                        '<td >'+
                          '<div class="">'+
                            '<div class="ocecotable-h-div">'+
                                new Date(listvalue.checkedAt).getHours() + ':' + new Date(listvalue.checkedAt).getMinutes()+
                            '</div>'+
                            '<div class="ocecotable-d-div" >'+
                                new Date(listvalue.checkedAt).getDay() + '/' + new Date(listvalue.checkedAt).getMonth()+ '/' +  new Date(listvalue.checkedAt).getFullYear()+ 
                            '</div>'+
                          '</div>'+
                        '</td>'+
                        '<td>'+
                            '<div class="ocecotable-task-div">'+
                                listvalue.task+
                            '</div>'+
                        '</td>'+
                        '<td>'+
                            '<div class="oceco-avatar-group">'+
                                listvalue.profilpicture+
                            '</div>'+
                        '</td>'+
                      '</tr>';
            },
            values : {
                
            }
        } ,

        "outdatetaskscard" : {
            type : "list",
            getandsetDatafunc : function(ocecoform){
                var tasks = ocecoform.mergeArray(Object.values(ocecoform.actions), "path", "tasks");
                 tasks = ocecoform.selectOccur(tasks, "null/false", "checked");
                var newtasks = [];
                
                $.each(tasks , function(tsId, tsVl){
                  if (ocecoform.countDays(ocecoform.convertDate(tsVl.endDate), new Date()) < 0) {

                    var profilpicture = "";
                    if (typeof tsVl.contributors != "undefined") {
                      
                      $.each(tsVl.contributors , function(tsIdcon, tsVlcon){

                        var infodata = [];

                        infodata = ocecoform.getUserData(tsIdcon);

                        if (
                          typeof infodata != "undefined" &&
                          typeof infodata.map != "undefined" &&
                          typeof infodata.map.profilImageUrl != "undefined" &&
                          infodata.map.profilImageUrl != ""
                          ) {
                          profilpicture += '<a href="javascript:;" class="oceco-avatar" data-bs-toggle="tooltip" data-bs-placement="bottom" title="" data-bs-original-title="'+infodata.map.name+'"><img  id="menu-thumb-profil"  src="'+infodata.map.profilImageUrl+'" alt="image"></a>';
                        }else{
                          profilpicture += '<a href="javascript:;" class="oceco-avatar" data-bs-toggle="tooltip" data-bs-placement="bottom" title="" data-bs-original-title="'+infodata.map.name+'"><img class="" id="menu-thumb-profil" src="'+defaultimgprofil+'" alt="image"></a>';
                        }
                      });
                    }
                    tasks[tsId]["profilpicture"] = profilpicture;

                    newtasks.push(tasks[tsId]);
                  }
                })
                var orderedTasks = ocecoform.sortbyDate(newtasks, "endDate", true);
                ocecoform.tiles["outdatetaskscard"].values = orderedTasks;
            },
            template : function(listvalue){
              // return '<div class="ocecocard-item">'+
              //         // '<div class="ocecocard-icon">'+
              //         //     '<i class="fa fa-circle"></i>'+
              //         // '</div>'+
              //         '<div class="ocecocard-desc">'+
              //             '<span class="timelinetitle">'+listvalue.task+'</span>'+
              //             '<p class="timelinetext">'+new Date().getDate()+'/'+new Date().getMonth()+'/'+new Date().getYear()+'</p>'+
              //         '</div>'+
              //         '<div class="ocecocard-icon">'+
              //              listvalue.profilpicture+
              //         '</div>'+
              //       '</div>';
              return '<tr class="ocecotable-tr">'+
                        
                        '<td>'+
                            '<div class="ocecotable-task-div">'+
                                listvalue.task+
                            '</div>'+
                        '</td>'+
                        '<td>'+
                            '<div class="oceco-avatar-group">'+
                                listvalue.profilpicture+
                            '</div>'+
                        '</td>'+
                        '<td >'+
                          '<div class="">'+
                            '<div class="ocecotable-d-div" >'+
                                ocecoform.convertDate(listvalue.endDate).getDay() + '/' + ocecoform.convertDate(listvalue.endDate).getMonth()+ '/' +  ocecoform.convertDate(listvalue.endDate).getFullYear()+ 
                            '</div>'+
                          '</div>'+
                        '</td>'+
                      '</tr>';
            },
            values : {
                
            }
        } ,

        "financerline" : {
            type : "chart",
            getandsetDatafunc : function(ocecoform){
                var finn = ocecoform.mergeArray(Object.values(ocecoform.allData), "path", "financer");
                var kkkd = ocecoform.groupWeeks(finn, "date" ,"amount", "countpath", 0, false);
                ocecoform.tiles["financerline"].values.data[0] = ocecoform.mergeArray(kkkd, "path", "count" );
                ocecoform.tiles["financerline"].values.labels[0] = ocecoform.mergeArray(kkkd, "path", "weekStart" );


                var payy = ocecoform.mergeArray(Object.values(ocecoform.allData), "path", "payement");
                var pppa = ocecoform.groupWeeks(payy, "date" ,"amount", "countpath",0, false);
                ocecoform.tiles["financerline"].values.data[1] = ocecoform.mergeArray(pppa, "path", "count" );
                ocecoform.tiles["financerline"].values.labels[1] = ocecoform.mergeArray(pppa, "path", "weekStart" );

                var ghg = [...new Set([...ocecoform.tiles["financerline"].values.labels[1],...ocecoform.tiles["financerline"].values.labels[0]])];

                var finamount = [];
                var finamount2 = [];

                $.each(ghg, function(ih, vh){

                    if (ocecoform.tiles["financerline"].values.labels[0].indexOf(vh) != -1 ) {
                      
                      finamount.push(ocecoform.tiles["financerline"].values.data[0][ocecoform.tiles["financerline"].values.labels[0].indexOf(vh)]);
                    }else{
                      finamount.push(0);
                    }

                    if (ocecoform.tiles["financerline"].values.labels[1].indexOf(vh) != -1 ) {

                      finamount2.push(ocecoform.tiles["financerline"].values.data[1][ocecoform.tiles["financerline"].values.labels[1].indexOf(vh)]);
                    }else{
                      finamount2.push(0);
                    }

                });

                finamount = ocecoform.sumCumul(finamount);
                finamount2 = ocecoform.sumCumul(finamount2);

                ocecoform.tiles["financerline"].values.labels[0] = ocecoform.tiles["financerline"].values.labels[1] = ghg;

                ocecoform.tiles["financerline"].values.data[0] = finamount;

                ocecoform.tiles["financerline"].values.data[1] = finamount2;

                var price = ocecoform.mergeArray(Object.values(ocecoform.allData), "path", "price");
                ocecoform.tiles["financerline"].values.objectif = ocecoform.sumInt(price);
            },
            values : {
                id : "financerline",
                data : [[0,0], [0,0]],
                label : '# of Votes',
                labels : [["0","0"],["0","0"]],
                objectif : 0,
                g : 'graph.views.co.ocecoform.multiline',
                colors : standartcolors,
                withlabel : true
            }
        } ,

        "tasksperperson" : {
          type : "chart",
          getandsetDatafunc : function(ocecoform){
            var tasks = ocecoform.mergeArray(Object.values(ocecoform.actions), "path", "tasks");
             tasks = ocecoform.selectOccur(tasks, "null/false", "checked");
            var taskscontr = ocecoform.mergeArray(tasks, "path", "contributors");
            var tpp = ocecoform.group(taskscontr , "")
            var groupbyuser = {};
            $.each(taskscontr, function(ind2, val2){
              $.each(val2, function(ind, val){
                groupbyuser[ind] = (typeof groupbyuser[ind] != "undefined" ? groupbyuser[ind] : 0) + 1 
                
              });
            });
            var todata = [];
            var tolabel = [];
            $.each(groupbyuser, function(index, val){
              todata.push(val);
              infodata = ocecoform.getUserData(index);
              if (typeof infodata.map != "undefined" && typeof infodata.map.name != "undefined") {
                tolabel.push(infodata.map.name);
              } else {
                tolabel.push("inconue");
              }
            });

            ocecoform.tiles["tasksperperson"].values.data = todata;
            ocecoform.tiles["tasksperperson"].values.labels = tolabel;

          },
          values : {
                id : "tasksperperson",
                data : [0, 0, 0],
                label : 'nombre de taches',
                labels : ["","",""],
                g : 'graph.views.co.ocecoform.bar',
                colors : bigstandartcolors
            }
        },

        "finpertype" : {
          type : "chart",
          getandsetDatafunc : function(ocecoform){
            var datafinpertype = [];
            $.each(groupe, function(labid, lab){
              var price = ocecoform.selectOccur(ocecoform.allData, labid, "group");

              var priceaaray = ocecoform.mergeArray(price, "path", "price");

              ocecoform.tiles["finpertype"].values.data[labid] = ocecoform.sumInt(priceaaray);
            });
          },
          values : {
                id : "finpertype",
                data : [0,0, 0,0,0],
                label : '# of Votes',
                labels : groupe,
                g : 'graph.views.co.ocecoform.pie',
                colors : bigstandartcolors
            }
        },

        "amounttofin" : {
            type : "html",
            getandsetDatafunc : function(ocecoform){
              var price = ocecoform.mergeArray(Object.values(ocecoform.allData), "path", "price");
              ocecoform.tiles["amounttofin"].values = ocecoform.sumInt(price)+"€";
            },
            values : 0
        },

        "amountfin" : {
            type : "html",
            getandsetDatafunc : function(ocecoform){

              var tt = ocecoform.mergeArray(Object.values(ocecoform.allData), "path", "financer");
              var ttjk = ocecoform.mergeArray(tt, "path", "amount");
              ocecoform.tiles["amountfin"].values = ocecoform.sumInt(ttjk)+"€";

            },
            values : 0
        },

        "amountinfin" : {
            type : "html",
            getandsetDatafunc : function(ocecoform){

              var tt = ocecoform.mergeArray(Object.values(ocecoform.allData), "path", "payement");
              var ttjk = ocecoform.mergeArray(tt, "path", "amount");
              ocecoform.tiles["amountinfin"].values = ocecoform.sumInt(ttjk)+"€";

            },
            values : 0
        },

        "feedbackmoy" : {
            type : "html",
            getandsetDatafunc : function(ocecoform){
                var ii = ocecoform.group(Object.values(ocecoform.feedback), "star");
                var totalcount = 0;
                $.each(ii, function(index, value){
                  // totalcount += parseInt(value.count);
                });
                // ocecoform.tiles["feedbackmoy"].values
                
              
            },
            values : 0
        },
        "feedback1" : {
            type : "html",
            getandsetDatafunc : function(ocecoform){


            },
            values : 0
        },
        "feedback2" : {
            type : "html",
            getandsetDatafunc : function(ocecoform){

            
            },
            values : 0
        },
        "feedback3" : {
            type : "html",
            getandsetDatafunc : function(ocecoform){

             

            },
            values : 0
        },
        "feedback4" : {
            type : "html",
            getandsetDatafunc : function(ocecoform){

             

            },
            values : 0
        },
        "feedback5" : {
            type : "html",
            getandsetDatafunc : function(ocecoform){

              // var ii = ocecoform.group(ocecoform.feedback, "star");
              

            },
            values : 0
        },






      },

      init : function(pInit = null){
          var copyFilters = jQuery.extend(true, {}, formObj);
          copyFilters.initVar(pInit);
          return copyFilters;
      },

      initvalues : function(ocecoform){
          $.each( ocecoform.tiles , function( dataId, dataValue ) {
              ocecoform.tiles[dataId].getandsetDatafunc(ocecoform);
          });
      },

      initviews : function(ocecoform){
          $.each( ocecoform.tiles , function( tilesId, tilesValue ) {
                if(tilesValue.type == "html"){
                    $("#"+tilesId).html(ocecoform.tiles[tilesId].values);
                } else if (tilesValue.type == "chart") {
                    if (typeof tilesValue.callback != "undefined") {
                        var cb = tilesValue.callback;
                    } else {
                        var cb = function(){};
                    }
                    ajaxPost("#"+tilesId, baseUrl+'/graph/co/chart/', ocecoform.tiles[tilesId].values, cb ,"html");
                } else if (tilesValue.type == "list") {
                    $.each(ocecoform.tiles[tilesId].values, function(tId, tVal){
                        $("#"+tilesId).append(ocecoform.tiles[tilesId].template(tVal));
                    })      
                }
          });
      },

      updateviewData : function(arData, chart){
        if (typeof tiles[chart] !== "undefined" && arData[chart]){
            if (typeof tiles[chart]["type"] == "html" ) {
                $('#'.chart).html(arData[chart]);
            }
        }
      },

      mergeArray : function(arr, type, path){
           var r = arr.reduce(function(accumulator, item){
              if (type == "root" && typeof item != "undefined") {
                accumulator = accumulator.concat(item);
                return accumulator;
              }
              else if (type == "path" && typeof item != "undefined" && item != null && typeof item[path] != "undefined") {
                accumulator = accumulator.concat(item[path]);
                return accumulator;
              } else {
                return accumulator;
              }
          },[]);
        return r;
      },

      selectOccur : function(arr, key, path){
        if (key != "null/false") {
           var r = arr.reduce(function(accumulator, item){
              if (typeof item != "undefined" && item != null && item[path] == key) {
                accumulator = accumulator.concat(item);
              }
              return accumulator;
          },[]);
          return r;
        } else {
          var r = arr.reduce(function(accumulator, item){
              if (typeof item != "undefined" && (typeof item[path] == "undefined" || item[path] == null || item[path] == false)) {
                accumulator = accumulator.concat(item);
              }
              return accumulator;
          },[]);
          return r;
        }
      }, 

      convertDate : function(dateString){
        var dateParts = dateString.split("/");

        var dateObject = new Date(+dateParts[2], dateParts[1] - 1, +dateParts[0]); 
        return dateObject;
      },

      countDays : function(startDate, endDate){
          var difference_In_Time = startDate.getTime() - endDate.getTime();
          var difference_In_Days = difference_In_Time / (1000 * 3600 * 24);
          return Math.round(difference_In_Days);
      },

      countToday : function(arr, path){
          arr = arr.filter(function( va ) {
            return (new Date(va[path]).getDay() == new Date().getDay()) && (new Date(va[path]).getMonth() == new Date().getMonth()) && (new Date(va[path]).getYear() == new Date().getYear());
          });
          return arr.length;
      },

      countWeek : function(arr, path){
          arr = arr.filter(function( va ) {
            return ocecoform.getWeekStart(new Date(va[path])) == ocecoform.getWeekStart(new Date());
          });
          return arr.length;
      },

      sortbyDate : function(arr, path, isISO, direction = "asc"){
        var operators = {
          'asc': function(a, b) { return a - b },
          'desc': function(a, b) { return b - a }
        };

        if (path != "root") {
          arr = arr.filter(function( va ) {
              return typeof va[path] !== 'undefined';
          });
          if (isISO) {
            return arr.sort(function(a,b){return operators[direction](new Date(b[path]), new Date(a[path]));
            });
          } else {
              return arr.sort(function(a,b){return operators[direction](ocecoform.convertDate(b[path]), ocecoform.convertDate(a[path]));
            });
          }
        } else {
          if (isISO) {
            return arr.sort(function(a,b){return operators[direction](new Date(b) , new Date(a));
            });
          } else {
              return arr.sort(function(a,b){return operators[direction](ocecoform.convertDate(b) , ocecoform.convertDate(a));
            });
          }
        }
      },

      getAttr : function(arr, path){
          var r = arr.reduce(function(accumulator, item){
              if (typeof item != "undefined" && typeof item[path] != "undefined" ) {
                accumulator = accumulator.concat([item]);
              }
              return accumulator;
          },[]);
        return r;
      },

      getWeekStart : function (date) {
        var offset = new Date(date).getDay();
        return new Date(new Date(date) - offset * 24 * 60 * 60 * 1000)
          .toISOString()
          .slice(0, 10);
      },

      groupWeeks : function (dates, path, countpath, type, countreverse = 0, isISO) {
        
        const groupsByWeekNumber = dates.reduce(function(acc, item) {
          var jsdate = item[path];
          if (!isISO) {
              jsdate = ocecoform.convertDate(item[path])
          }
          const today = new Date(jsdate);
          const weekNumber = today.getWeek();

          // check if the week number exists
          if (typeof acc[weekNumber] === 'undefined') {
            acc[weekNumber] = [];
          }

          acc[weekNumber].push(item);

          return acc;
        }, []);

        if (type == "countpath" || type == "") {
            
          
          return groupsByWeekNumber.map(function(group) {
            return {
              weekStart: ocecoform.getWeekStart(ocecoform.convertDate(group[0][path])),
              count: group.reduce(function(acc, item) {
                return acc + parseInt(item[countpath]);
              }, 0)
            };
          });

        } else if (type == "count"){

          if (!isISO) {
            return groupsByWeekNumber.map(function(group) {
              return {
                weekStart: ocecoform.getWeekStart(ocecoform.convertDate(group[0][path])),
                count: group.reduce(function(acc, item) {
                  return acc = acc + 1;
                }, 0)
              };
            });
          } else {
            return groupsByWeekNumber.map(function(group) {
              return {
                weekStart: ocecoform.getWeekStart(group[0][path]),
                count: group.reduce(function(acc, item) {
                  return acc = acc + 1;
                }, 0)
              };
            });
          }

        } else if (type == "countreverse"){

          if (!isISO) {
            return groupsByWeekNumber.map(function(group) {
              return {
                weekStart: ocecoform.getWeekStart(ocecoform.convertDate(group[0][path])),
                count: group.reduce(function(acc, item) {
                  return countreverse - 1;
                }, 0)
              };
            });
          } else {
            return groupsByWeekNumber.map(function(group) {
              return {
                weekStart: ocecoform.getWeekStart(group[0][path]),
                count: group.reduce(function(acc, item) {
                  return countreverse = countreverse - 1;
                }, 0)
              };
            });
          }

        }

      },

      sumInt : function(arr){
          var sum = arr.reduce(function(a, b){
                  return parseInt(a) + parseInt(b);
          }, 0);
          return sum;
      }, 

      sumCumul : function(arr){

        const accumulate = arr => arr.map((sum => value => sum += value)(0));

        return accumulate(arr);  
      },

      getUserData : function(id) {
        var returngetUserData;
          ajaxPost("",
            baseUrl+"/co2/element/get/type/citoyens/id/"+id,
            null,
            function(data) {

              if(typeof data != "undefined"){
                 returngetUserData = data;
                  }
            },
            null,
            "json",
            {async : false}
          );
        return returngetUserData;

      },

      group : function(arr, path){
          var gr = arr.reduce(function(res, obj){
            res[obj[path]] = { 
              count: (obj[path] in res ? res[obj[path]].count : 0) + 1 
            }
            return res;      
          }, []);
          return gr;
      },

    };

        Date.prototype.getWeek = function(dowOffset) {
        /*getWeek() was developed by Nick Baicoianu at MeanFreePath: http://www.epoch-calendar.com */

        dowOffset = typeof dowOffset == 'int' ? dowOffset : 0; //default dowOffset to zero
        var newYear = new Date(this.getFullYear(), 0, 1);
        var day = newYear.getDay() - dowOffset; //the day of week the year begins on
        day = day >= 0 ? day : day + 7;
        var daynum =
          Math.floor(
            (this.getTime() -
              newYear.getTime() -
              (this.getTimezoneOffset() - newYear.getTimezoneOffset()) * 60000) /
              86400000
          ) + 1;
        var weeknum;
        //if the year starts before the middle of a week
        if (day < 4) {
          weeknum = Math.floor((daynum + day - 1) / 7) + 1;
          if (weeknum > 52) {
            nYear = new Date(this.getFullYear() + 1, 0, 1);
            nday = nYear.getDay() - dowOffset;
            nday = nday >= 0 ? nday : nday + 7;
            /*if the next year starts before the middle of
                 the week, it is week #1 of that year*/
            weeknum = nday < 4 ? 1 : 53;
          }
        } else {
          weeknum = Math.floor((daynum + day - 1) / 7);
        }
        return weeknum;
      };

  ocecoform.initvalues(ocecoform);
  ocecoform.initviews(ocecoform);

</script>
