 <style>
.oceco-h1 {
  font-size: 20px;
}

.oceco-gradient {
  background: -webkit-linear-gradient(#17ad37, #98ec2d);
  -webkit-background-clip: text;
  -webkit-text-fill-color: transparent;
}


.oceco-avatar {
  color: #fff;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  font-size: 1rem;
  border-radius: 0.75rem;
width: 34px !important;
  height: 34px !important;
  font-size: 0.75rem;
  transition: all .2s ease-in-out;
}

.oceco-avatar img {
    border-radius: 50% !important;
    width: 100%;
    vertical-align: middle;
}

.oceco-avatar-group .oceco-avatar {
  position: relative;
  z-index: 2;
  border: 2px solid #fff;
  border-radius: 50% !important;
}

.oceco-avatar-group .oceco-avatar:hover {
  z-index: 3;
}

.oceco-avatar-group .oceco-avatar+.oceco-avatar {
  margin-left: -1rem;
}

.ocecotable-list> :not(:last-child)> :last-child>* {
    border-bottom-color: #e9ecef;
}

.ocecotable-list {
    width: 100%;
}

.ocecotable-th{
    font-weight: bold;
    font-family: 'Montserrat';
    color : #67748e;
    text-align: -webkit-center;
    padding: 5px;
    border-bottom: 1px solid #e9ecef;
}

.ocecotable-tr{
    border-bottom: 1px solid #e9ecef;
}

.ocecotable-h-div{
    font-weight: bold;
    font-family: 'Montserrat';
}

.ocecotable-d-div{
    font-weight: bold;
    font-family: 'Montserrat';
    color : #7e8299;
}

.ocecotable-task-div{
    font-weight: bold;
    font-family: 'Montserrat';
}

#fi {
    padding-top: 0;
}

.myFixedHeightContainer {
    height: 200px;
}

.ocecotitle1 {
    font-size: 17px;
}

.evopaymentchart {
    margin-right: 0 !important;
}

.evopaymentnum {
    margin-left: 0 !important;
}


.ocecoval-yellow {
    background-color: #fffaca;
}

.ocecoval-green {
    background-color: #e4fae5;
}

.ocecoval-black {
    background-color: #eee5fe;
}

.ocecoval-items {
    margin: 0;
    padding: 0;
    margin-top: 10px;
}

.ocecoval-item {
    text-align: center;
    margin-left: 0;
    /*background-color: #e4fae5;*/
    padding: 0;
    position: relative;
    box-shadow: 0 2px 1px rgba(170, 170, 170, 0.25);
    border-raduis : 5px;
}

.ocecoval-icon {
    position: absolute;
    top: 0;
    right: 0;
    border-radius: 100%;
    width: 35px;
    height: 35px;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    line-height: 0;
    background-color: #eee5ff!important;
        color: black;
}

.ocecoval-desc {
    padding: 1px 4px 0 0;
}

/*.ocecocard-desc::before {
    background-color: #eee5ff;
}

.ocecocard-desc::before {
    content: "";
    position: absolute;
    width: 4px;
    height: calc(100% - 32px);
    background-color: #ebedf3;
    border-radius: 6px;
    top: 40px;
    left: 16px;
}*/

.ocecoval-desc .timelinetitle {
    font-weight: 600!important;
    /*color: #61cd73!important;*/
}

.ocecoval-desc .timelinetext {
    color: #46a07a!important;
    font-weight: bold !important;
    /*padding-bottom: .5rem!important;*/
    margin-top: 0;
    margin-bottom: 1rem;
    font-size: 18px !important;
}



.overflowsc {
    overflow-y: scroll;
}


.ocecocard-items {
    margin: 0;
    padding: 0;
    margin-top: 10px;
}

.ocecocard-item {
    margin-left: 0;
    padding: 0;
    position: relative;
}

.ocecocard-icon {
    position: absolute;
    top: 0;
    /*border-radius: 100%;*/
    width: 35px;
    height: 35px;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    line-height: 0;
    /*background-color: #61cd73!important;*/
        color: white;
}

.ocecocard-desc {
    padding: 9px 0 0 44px;
}

/*.ocecocard-desc::before {
    background-color: #eee5ff;
}

.ocecocard-desc::before {
    content: "";
    position: absolute;
    width: 4px;
    height: calc(100% - 32px);
    background-color: #ebedf3;
    border-radius: 6px;
    top: 40px;
    left: 16px;
}*/

.ocecocard-desc .timelinetitle {
    font-weight: 600!important;
    /*color: #61cd73!important;*/
}

.ocecocard-desc .timelinetext {
    color: #7e8299!important;
    font-weight: 400!important;
    padding-bottom: .5rem!important;
    margin-top: 0;
    margin-bottom: 1rem;
    font-size: 14px;
}


.ocecotimeline-items {
    margin: 0;
    padding: 0;
    margin-top: 10px;
}

.ocecotimeline-item {
    margin-left: 0;
    padding: 0;
    position: relative;
}

.ocecotimeline-icon {
    position: absolute;
    top: 0;
    border-radius: 100%;
    width: 35px;
    height: 35px;
    display: -webkit-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;
    -webkit-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    line-height: 0;
    background-color: #61cd73!important;
        color: white;
}

.ocecotimeline-desc {
    padding: 9px 0 0 44px;
}

.ocecotimeline-desc::before {
    background-color: #eee5ff;
}

.ocecotimeline-desc::before {
    content: "";
    position: absolute;
    width: 4px;
    height: calc(100% - 32px);
    background-color: #ebedf3;
    border-radius: 6px;
    top: 40px;
    left: 16px;
}

.ocecotimeline-desc .timelinetitle {
    font-weight: 600!important;
    /*color: #61cd73!important;*/
}

.ocecotimeline-desc .timelinetext {
    color: #7e8299!important;
    font-weight: 400!important;
    padding-bottom: .5rem!important;
    margin-top: 0;
    margin-bottom: 1rem;
    font-size: 14px;
}

    .ocecoform-body{
        background-color: #ebebeb;
        font-family: 'Raleway', sans-serif;
    }

    .ocecoform-body .tilelabel{
        font-weight: bold !important;
    }

    .squareTile{
        width: 20%;
        height: 180px !important;
    }

    .squareTileL{
        width: 20%;
        height: 260px !important;
    }

    .headerTile{
        width: 70%;
        height: 180px !important;
    }

    .flex1{
        flex: 1!important;
    }

    .flex2{
        flex: 2!important;
    }

    .flex5{
        flex: 5!important;
    }

    .flex6{
        flex: 6!important;
    }

    .flex8{
        flex: 8!important;
    }

    .flex3{
        flex: 3!important;
    }


    .ocecotitle{
        display : flex;
        -webkit-box-pack: center!important;
        -ms-flex-pack: center!important;
        justify-content: center!important;
    }

    .pdt15{
        padding-top: 25px;
    }

    .outeroceco {
      position: relative;
    }
    canvas {
      position: absolute;
    }
    .ocecopercent {
     position: absolute;
        left: 50%;
        transform: translate(-50%, 0);
        font-size: 20px;
        font-weight: bold;
        top: 60%;
        /*bottom: 0px;*/
    }
    .progress {
        border-radius: 20px;
    }
    .progress-bar {
        background-color: #61cd73;
    }
    .dividerT {
      width: 1px;
      margin: 6px 0;
      background: #dddddd;
    }

    .divitemheader{
        display: flex;
        flex-direction: row;
        flex: 1;
    }

    .itemheaderTile {
      flex: 1 auto;
      display: flex;
      flex-direction: column;
      padding-top: 15px;
    }

    .ocecotitle i{
      font-size: 50px;
    }

    .ocecotitle .apercu {
        background-color: transparent;
        border: 0px solid grey;
        border-radius: 3px;
    }

    .apercu i {
        font-size: 15px !important ;
    }

    .ocecotitle.step{
        font-size: 20px;
        font-weight: bold;
    }

    .ocecotitle.step2{
        font-size: 17px;
        font-weight: bold;
    }

    .ocecotitle.step3{
        font-size: 25px;
        font-weight: bold;
    }

    .ocecotitle .state{
        font-size: 15px;
    }

    .ocecotitle i.success, .ocecotitle .state.success{
      color: #61cd73;
    }

    .ocecotitle i.warning, .ocecotitle .state.warning{
      color: #ecb22f;
    }

    .ocecotitle i.info{
      color: #61cd73;
    }

    .ocecotitle i.warning{
      color: #ecb22f;
    }

    .fifi{
      height: 50px;
      width: 100px;
    }

    .fifi .ocecoouter{
        position: relative !important;
    }

    .ocecopercentfi {
        position: absolute;
        left: 50%;
        transform: translate(-50%, 0);
        font-size: 15px;
        font-weight: bold;
        top: 12px;
        /*bottom: 0px;*/
    }

    .successSquare{
        background-color: #e5ffe5;
    }

    .dangerSquare{
        background-color: #f1e1c9;
    }

    .itemheaderTileI {
        padding-top: 10px !important;
    }
</style>