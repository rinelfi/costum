
   
        <!--================ Frist hader Area =================-->
        <header class="header_area">
            <div class="container">
                <nav class="navbar navbar-default">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="index.html">Mirana Sylvany</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="active"><a href="#about">MON PROPOS </a></li>
                            <li><a href="#skill">COMPETENCES</a></li>
                            <li><a href="#education">EDUCATION</a></li>
                            <li><a href="#service">EXPERIENCES</a></li>
                            <li><a href="#contact">CONTACT</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </nav>
            </div>
        </header>
        <!--================End Footer Area =================-->
        
        <!--================Total container Area =================-->
        <div class="container main_container">
            <div class="content_inner_bg row m0">
                <section class="about_person_area pad" id="about">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="person_img">
                               <img class="avatar-prof a-img" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/images/mirana/cv.png" data-toggle="tooltip" data-placement="bottom"  aria-hidden="true">                        
                              
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="row person_details">
                                <h3> <span>HASINAVOLOLONA Mirana Sylvany</span></h3>
                                <h4>Développeur</h4>
                                <div class="person_information">
                                    <ul>
                                        <li><a href="#">Age</a></li>
                                        <li><a href="#">Nationalité</a></li>
                                        <li><a href="#">Adresse</a></li>
                                        <li><a href="#">Phone</a></li>
                                        <li><a href="#">Skype</a></li>
                                        <li><a href="#">Email</a></li>
                                    </ul>
                                    <ul>
                                        <li><a href="#">22 Août 1998 (22 ans)</a></li>
                                        <li><a href="#">Malagasy (Madagascar)</a></li>
                                        <li><a href="#">près lot 035A15 Ampopoka Golf</a></li>
                                        <li><a href="#">(+261) 34 55 724 30</a></li>
                                        <li><a href="#">Mirana Sylvany</a></li>
                                        <li><a href="#">miranasylvany@gmail.com</a></li>
                                    </ul>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </section>
                <section class="myskill_area pad" id="skill">
                    <div class="main_title">
                        <h2>COMPETENCES EN INFORMATIQUE </h2>
                    </div>
                    <div class="row">
                        <div class="col-md-6 wow fadeInUp animated">
                            <div class="skill_text">
                                <h4>Technologie web</h4>                                
                            </div>
                            <div class="skill_item_inner">
                                <div class="single_skill">
                                    <h4>Html & Css</h4>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: 85%">
                                            <div class="progress_parcent"><span class="counter">85</span>%</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="single_skill">
                                    <h4>Php</h4>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: 70%">
                                            <div class="progress_parcent"><span class="counter">70</span>%</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="single_skill">
                                    <h4>JavaScript</h4>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: 60%">
                                            <div class="progress_parcent"><span class="counter">60</span>%</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="single_skill">
                                    <h4>ASP.NET</h4>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: 40%">
                                            <div class="progress_parcent"><span class="counter">40</span>%</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 wow fadeInUp animated">
                            <div class="skill_text">
                                <h4>Language de programmation et autre </h4>
                            </div>
                            <div class="skill_item_inner">
                                <div class="single_skill">
                                    <h4>Java</h4>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: 65%">
                                            <div class="progress_parcent"><span class="counter">65</span>%</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="single_skill">
                                    <h4>Python</h4>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: 45%">
                                            <div class="progress_parcent"><span class="counter">45</span>%</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="single_skill">
                                    <h4>MySQL, PostegreSQL, SQL Server</h4>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: 90%">
                                            <div class="progress_parcent"><span class="counter">90</span>%</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="single_skill">
                                    <h4>UML, MERISE II</h4>
                                    <div class="progress">
                                        <div class="progress-bar" role="progressbar" style="width: 60%">
                                            <div class="progress_parcent"><span class="counter">80</span>%</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="education_area pad" id="education">
                    <div class="main_title">
                        <h2>Education</h2>
                    </div>
                    <div class="education_inner_area">
                        <div class="education_item wow fadeInUp animated" data-line="S">
                            <h6>2017-2019</h6>
                            <a href="#"><h4>Université de Fianarantsoa </h4></a>
                            <h5>Ecole de Management et d'innovation technologique </h5>
                            <p>Master en Modélisation et Ingénierie Informatique</p>
                        </div>
                        <div class="education_item wow fadeInUp animated" data-line="H">
                            <h6>2014-2017</h6>
                            <a href="#"><h4>Université de Fianarantsoa </h4></a>
                            <h5>Ecole de Management et d'innovation technologique</h5>
                            <p>Licence en Développement d’Application Internet/Intranet</p>
                        </div>
                        <div class="education_item wow fadeInUp animated" data-line="C">
                            <h6>2013-2014</h6>
                            <a href="#"><h4>Secondaire</h4></a>
                            <h5>Lycée privéé Fo Masin’i Jesoa Talatamaty Fianarantsoa </h5>
                            <p>BACC série C</p>
                        </div>                        
                    </div>
                </section>
                <section class="service_area" id="service">
                    <div class="main_title">
                        <h2>STAGES ET EXPERIENCES PROFESSIONNELLES</h2>
                    </div>
                    <div class="service_inner row">
                        <div class="col-md-6">
                            <div class="service_item wow fadeInUp animated">
                               <h3> 2017-2018 </h3> 
                                <a href="#"><h4>Mini-mémoire pour le passage en Master 2</h4></a>
                                <p>Langage de programmation : Python</p>
                                <p>Framework : Tensorflow</p>
                                <p>Thème : Système de reconnaissance automatique d’une plaque d’immatriculation</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="service_item wow fadeInUp animated">
                                <h3>2016-2017</h3>
                                <a href="#t2" class= "lbh-preview-element" ><h4>Stage pratique au sein de T2 Développement Mahamasina Sud</h4></a>
                                <p>LDurée du stage : quatre (4) mois</p>
                                <p>Outils Informatique utilisés : Windows, Visual studio, DevExpress,
                                Microsoft SQL Server Management Studio Express.</p>
                                <p>Thème : Mise en place d’une application web pour la passation de marché</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="service_item wow fadeInUp animated">
                                <h3>2016-2017</h3>
                                <a href="#"><h4>Projet de réalisation</h4></a>
                                <p><u>* Projet en JSP</u></p>
                                <p>SGBD : MySQL</p>
                                <p>Thème : Création d’une application de gestion de gestion de vente de Pneu</p>
                                <p><u>* Projet PHP</u></p>
                                <p>Framework : CodeIgniter ,SGBD : MySQL</p>
                                <p>Thème : Conception et réalisation d’une application de gestion des touristes</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="service_item wow fadeInUp animated">
                                <h3>2015-2016</h3>
                                <a href="#"><h4>Projet de réalisation</h4></a>
                                <p><u>* Projet en Java</u></p>
                                <p>SGBD : MySQL</p>
                                <p>Thème : Conception et réalisation d’un logiciel pour la « gestion de station-service »</p>
                                <p><u>* Projet PHP</u></p>
                                <p>Framework : CodeIgniter ,SGBD : MySQL</p>
                                <p>Thème : Conception et réalisation d’une application de gestion des touristes</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="service_item wow fadeInUp animated">
                                <h3>2015-2016</h3>
                                <a href="#"><h4>Stage pratique au sein de Madagascar National Parks à Ranomafana</h4></a>
                                <p>Durée du stage : trois (3) mois</p>
                                <p>Langage de programmation : JAVA</p>
                                <p>SGBD : MySQL</p>
                                <p>Thème : Conception et réalisation d’une application web pour la « gestion de vente
                                de Pneu »</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="service_item wow fadeInUp animated">
                                <h3>2014-2015</h3>
                                <a href="#"><h4>Projet de réalisation</h4></a>
                                <p>❖ Projet HTML5 et CSS</p>
                                <p>❖ Projet d’application de Base de données sous Ms Access</p>
                            </div>
                        </div>
                    </div>
                </section>
                
                
            </div>
        </div>
        <!--================End Total container Area =================-->
        
        <!--================footer Area =================-->
        <footer class="footer_area">
            
            <div class="footer_copyright">
                <div class="container">
                    <div class="pull-left">
                        <h5><p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;2020 Mirana Sylvany </a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
</p></h5>
                    </div>
                    <div class="pull-right">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="active"><a href="#">MON PROPOS </a></li>
                            <li><a href="#">COMPETENCES</a></li>
                            <li><a href="#">EDUCATIONS</a></li>
                            <li><a href="#">EXPERIENCES</a></li>
                            <li><a href="#">CONTACT</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
       
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>/js/mirana/theme.js"></script>
       