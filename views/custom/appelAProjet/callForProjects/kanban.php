<div class="col-md-12 col-sm-12 col-xs-12" style="z-index: 1;">
    <div class="form-group col-md-4 col-sm-4 col-xs-12">
        <label for="sel1">Affichage</label>
        <select class="form-control" id="kanban-view-chooser">
            <option value="kanban_project_list">Tous les projets</option>
            <option value="kanban_follow_action">Suivi d'action</option>
            <option value="kanban_financement">Financement</option>
            <option value="kanban_personal_action">Personnel</option>
        </select>
    </div>
    <div class="col-xs-12 kaban-container"></div>
</div>
<script>
    $(function(){
        var $allAnswers= <?php echo json_encode(PHDB::find(Form::ANSWER_COLLECTION,array('form'=>$el_form_id,'answers'=>['$exists'=>true])))  ?>;
        var $slug = <?php echo json_encode($slug)  ?>;
        var $el_slug = <?php echo json_encode($el_slug)  ?>;
        var $el_configform = <?php echo json_encode($el_configform)  ?>;
        var $el_form = <?php echo json_encode($elform)  ?>;
        var $el = <?php echo json_encode($el)  ?>;
        var $el_form_id = <?php echo json_encode($el_form_id)  ?>;
        
        var kabanObj = {
            events:()=>{
                $this = kabanObj;
                $this.loadView("kanban_project_list");
                $("#kanban-view-chooser").off().on('change',()=>{
                    $this.loadView($("#kanban-view-chooser").val());
                })
            },
            loadView : (view)=>{
                var params =  {
                    elSlug : $el_slug,	
                    el: $el,
                    slug : $slug,
                    elConfigform: $el_configform,
                    elForm: $el_form,
                    elFormId : $el_form_id,
                    allAnswers : $allAnswers,
                };
                setTimeout(() => {
                    ajaxPost(".kaban-container", baseUrl+"/costum/aap/getviewbypath/path/costum.views.custom.appelAProjet.callForProjects.kanban."+view, 
                    params,
                        function(){
                            
                        },null,null,
                        {
                            beforeSend : () => {},
                            async:false

                        }
                    );
                }, 500);
            }
        }
        kabanObj.events();
    })
</script>