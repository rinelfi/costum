<style>
	.searchBarInMenu {
		display: none;
	}
</style>
<div class="row">
	<div class="col-xs-12">
		<div id='filterContainer' class='searchObjCSS'></div>
		<button class="btn btn-primary btn-show-hide-map pull-right" data-showmap="<?= @$elform["showMap"]==true ? "true" : "false" ?>">Map</button>
	</div>
	<div class="content-answer-search col-xs-12 <?= @$elform["showMap"]==true ? "col-md-6" : "col-md-12" ?>">
		<div class="headerSearchContainer col-xs-12 no-padding"></div>
		<div class="col-xs-12 bodySearchContainer margin-top-10">
			<div class="no-padding col-xs-12" id="dropdown_search"></div>
			<div class="no-padding col-xs-12 text-left footerSearchContainer"></div>   
		</div>
	</div>
	<div id="mapOfResultsAnsw" class="col-md-6 col-xs-12" style="height:500px; <?= @$elform["showMap"]==true ? " " : "display:none" ?>"></div>
</div>

<script type="text/javascript">
    var elform = "<?= (string) @$elform["_id"] ?>";

	searchObject.types=["answers"];
	searchObject.indexStep=30;
	
	//creation d'instance de map
	var mapAnsw = new CoMap({
		container : "#mapOfResultsAnsw",
		activePopUp : true,
		mapCustom:{
			tile : "mapbox"
		},
		elts : []
	});

	//definition des paramètres pour le filtre
	var paramsFilter= {
		mapCo: mapAnsw,
		container : "#filterContainer",
		//urlData : baseUrl+"/co2/search/globalautocomplete",
		urlData : baseUrl+"/survey/answer/directory/source/"+costum.slug+"/form/"+elform,
		interface : {
			events : {
				page : true
			}
		},
		filters : {
			//text : true,
			/*address : {
				view: "text",
				event:"text",
				placeholder : "Où (ville, cp) ?"
			},*/
			"answers.aapStep1.titre" : {
				view : "text",
				event : "text",
				placeholder : "Nom"
			},
			/*status : {
				view : "dropdownList",
				type : "filters",
				field : "status",
				name : "Statuts des dossiers",
				event : "filters",
				keyValue : false,
				list : {
					running : "En cours",
					end : "Terminé",
					started : "Commencé"
				}
			},*/
			acceptation : {
				view : "dropdownList",
				type : "filters",
				field : "acceptation",
				name : "Retenu ou Rejeté",
				event : "filters",
				keyValue : false,
				list : {
					"retained" : "Retenu",
					"rejected" : "Rejeté"
				}
			}
		},
		defaults:{
			notSourceKey : true,
			types:["answers"],
			forced: {
				filters:{
					'form' : elform,
				}
			},
			//fields : [],
		},

		header:{
			dom : ".headerSearchContainer",
			options:{
				left:{
					classes :"col-xs-12 no-padding",
					group:{
					count : true,
					map : true
				}
				}
			},
			views : {
				map : function(fObj,v){
				/*return  '<button class="btn-show-map-search pull-right" style="" title="'+trad.showmap+'" alt="'+trad.showmap+'">'+
							'<i class="fa fa-map-marker"></i> '+trad.map+
						'</button>';*/
						return "";
			}
		},
		events : {
			map : function(fObj){
				$(".btn-show-map-search").off().on("click", function(){
					$("#mapOfResultsAnsw").css({"left":"0px", "right":"0px", "bottom":"50px"});
					$("#mapOfResultsAnsw").show(200);
					$(".footerSearchContainer").addClass("affix");
					mapAnsw.map.invalidateSize();
					$("body").css({"overflow":"hidden"});
					$("#mapOfResultsAnsw .btn-hide-map").off().on("click", function(){
						$("#mapOfResultsAnsw").hide(200);
						$("body").css({"overflow":"inherit"});
						$(".footerSearchContainer").removeClass("affix");	
					});

				});
			}
		}
		},
		results :{
			map : {
				showMapWithDirectory :true
			}
		}
	};

	var filterSearch={};

	$(function(){
		var $slug = <?php echo json_encode($slug)  ?>;
		var $el_slug = <?php echo json_encode($el_slug)  ?>;
		filterSearch = searchObj.init(paramsFilter);
		filterSearch.header.set=function(fObj){
			var resultsStr = (fObj.results.numberOfResults > 1) ? " dossiers" : "dossier";
			if(typeof costum.hasRoles != "undefined" && $.inArray("Opérateur", costum.hasRoles)>=0  && !costum.isCostumAdmin)
				resultsStr+= " en attente d'opérateur";
			$(fObj.header.dom+" .countResults").html('<i class=\'fa fa-angle-down\'></i> ' + fObj.results.numberOfResults + ' '+resultsStr+' ');
		};

		filterSearch.results.render=function(fObj, results, data){
			mylog.log("govva",results);
			if(Object.keys(results).length > 0){
					ajaxPost(fObj.results.dom, baseUrl+"/survey/answer/views/tpl/costum.views.custom.appelAProjet.answerPanelHtml", 
					{allAnswers:results,
						form:data.form,
						forms:data.forms,
						el: data.el,
						what: data.what,
						canEdit: data.canEdit,
						slug : $slug,
						el_slug : $el_slug
					},
					function(){
						fObj.results.events(fObj);
					});
			}else 
				$(fObj.results.dom).append(fObj.results.end(fObj));
			fObj.results.events(fObj);
			fObj.results.addToMap(fObj, results);
		};

		filterSearch.results.addToMap = function(fObj, results){
			mylog.log("resultss",results);
			var elts = [];

			$.each(results, function(k, result){
				if(result.answers){
					$.each(result.answers, function(k, answer){
						if(answer.adress && answer.adress.geo)
						elts.push(
							{
								geo: answer.adress.geo,
								name: answer.proposition,
							}
						)
					})
				}
			})
			mylog.log("eltss",elts);
			mapAnsw.addElts(elts);
		};
		filterSearch.search.init(filterSearch);

		$(".btn-show-hide-map").off().on("click",function(){
			$btn = $(this);
			var prms = {
				id: "<?= (string)$elform["_id"]?>",
				collection : "<?= Form::COLLECTION?>",
				path :  "allToRoot",
				format:true,
				value :{
					showMap : !$btn.data("showmap")
				}
			}
			dataHelper.path2Value( prms, function(res) {
				if(res.result){
					urlCtrl.loadByHash(location.hash);
				}
				mylog.log("retenu",res);
			});
		})
	})
</script>