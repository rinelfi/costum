<?php
$starbg = Yii::app()->getModule('costum')->assetsUrl . "/images/blockCmsImg/green-star.png";
$tab = ["progress" => "En cours", "finished" => "Terminé", "abandoned" => "Abandonée", "newaction" => "Nouvelle proposition", "call" => "Appel au participations", "projectstate" => "En Projet", "finance" => "appel au financement", "vote" => "En evaluation"];
$stepEval = "aapStep2";
$stepFinancor = "aapStep3";
if (isset($el_configform["subForms"]["aapStep2"]["params"]["config"]["criterions"]))
{
    $stepEval = "aapStep2";
    $stepFinancor = "aapStep3";
    $stepAction = "aapStep4";
}
elseif (isset($el_configform["subForms"]["aapStep3"]["params"]["config"]["criterions"]))
{
    $stepEval = "aapStep3";
    $stepFinancor = "aapStep4";
    $stepAction = "aapStep5";
}

if(isset($elform["evaluationCriteria"]["activateLocalCriteria"]) && ($elform["evaluationCriteria"]["activateLocalCriteria"] == true)){
    if(isset($elform["evaluationCriteria"]["type"]))
        $el_configform["subForms"][$stepEval]["params"]["config"]["type"] = $elform["evaluationCriteria"]["type"];
    if(isset($elform["evaluationCriteria"]["criterions"]))
        $el_configform["subForms"][$stepEval]["params"]["config"]["criterions"] = $elform["evaluationCriteria"]["criterions"];
}
//var_dump($el_configform["subForms"][$stepEval]["params"]["config"]["criterions"]);

$sumCoeff = 0;
if (isset($el_configform["subForms"][$stepEval]["params"]["config"]["criterions"])){
    foreach ($el_configform["subForms"][$stepEval]["params"]["config"]["criterions"] as $key => $value)
    {
        $sumCoeff = $sumCoeff + (float)$value["coeff"];
    }
}

$idEl = array_keys($elform["parent"]) [0];
$typeEl = $elform["parent"][$idEl]["type"];
$allowedRoles = isset($el_configform["subForms"][$stepEval]["params"]["canEdit"]) ? $el_configform["subForms"][$stepEval]["params"]["canEdit"] : array();
$me = isset(Yii::app()->session['userId']) ? Person::getById(Yii::app()->session['userId']) : null;
$roles = isset($me["links"]["memberOf"][$idEl]["roles"]) ? $me["links"]["memberOf"][$idEl]["roles"] : null;
$isAdmin = isset($me["links"]["memberOf"][$idEl]["isAdmin"]) ? $me["links"]["memberOf"][$idEl]["isAdmin"] : false;
$canEvaluate = false;
$intersectRoles = array_intersect($allowedRoles, $roles);
if (count($intersectRoles) != 0 || $isAdmin) $canEvaluate = true;
$communityWithAdmin = Element::getCommunityByTypeAndId($typeEl, $idEl, "all", "isAdmin", null, null, ["name", "profilMediumImageUrl"]);
$community = Element::getCommunityByTypeAndId($typeEl, $idEl, "all", null, $allowedRoles, null, ["name", "profilMediumImageUrl"]);
$community = array_unique(array_merge($communityWithAdmin, $community) , SORT_REGULAR);

$allAnswersTotal = PHDB::find(Form::ANSWER_COLLECTION,array("form"=>(string)$elform["_id"]['$id'],"answers"=>['$exists'=>true]));
foreach ($community as $kcommunity => $vcommunity) {
    $haveMadeAvote = false;
    foreach ($allAnswersTotal as $kallans => $vallans) {
        if(isset($vallans["answers"][$stepEval]["evaluation"][$kcommunity]))
            $haveMadeAvote = true;
    }
    if($haveMadeAvote == false)
        unset($community[$kcommunity]);
}

$creator = Person::getById($elform["creator"]);
// $community[(string)$creator["_id"]] = $creator;

$badgelabel = [
    "progress" => "En cours",
    "vote" => "En evaluation",
    "finance" => "En financement",
    "call" => "Appel à participation",
    "newaction" => "nouvelle proposition",
    "projectstate" => "En projet",
    "finish" => "Términé",
    "suspend" => "Suspendu"
];
?>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<style type="text/css">
    .questionList{list-style-type: none;}
    .star-rating .star-value{
        background: url("<?=$starbg?>") !important;
        background-size: 20px !important;
        background-repeat-y: no-repeat !important;
    }

    .select2-container--default .select2-selection--multiple .select2-selection__choice {
        background-color: #e4e4e4 !important;
        border: 1px solid #aaa !important;
        border-radius: 4px !important;
        cursor: default !important;
        float: left
        color: black !important;
        margin-right: 5px;
        margin-top: 5px;
        padding: 0 5px;
    }

    .st-enable-btn {
        color: #fff;
        border: 1px solid #7da53d;
        text-align: center;
        font-size: 14px;
        margin: 0px;
        margin-left: 3px;
        padding: 5px 12px;
        background: #7da53d;
    }

    .st-enable-btn:hover {
        color: #7da53d;
        border: 1px solid #7da53d;
        background: #fff;
    }


</style>

<div class="portfolio-modal modal fade" id="formInputM" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content padding-top-15">
        <div class="close-modal" data-dismiss="modal">
            <div class="lr">
                <div class="rl">
                </div>
            </div>
        </div>
        <div class=" container ">
            <ul class="aapinputcontent questionList col-xs-12  no-padding">

            </ul>
        </div>
    </div>

</div>

<div class="<?=@$el_configform["showMap"] ? "cx12 col-xs-12" : "container" ?>">
    <div class="row">
        <div class="list-group proposition_list">
            <?php
            if (!empty($allAnswers) && is_array($allAnswers))
            {
                function sortByPosOrder($a, $b) {
                    $sr = -1;
                    if (!empty(Yii::app()->session['userId']) && !empty($a["visitor_counter"]["user"]) && in_array(Yii::app()->session['userId'], $a["visitor_counter"]["user"])){
                        $sr = 1 ;
                    }

                    if (empty(Yii::app()->session['userId']) || $a["user"] == Yii::app()->session['userId'] ){
                        $sr = 1 ;
                    }

                    return $sr;
                }
                // Commented by GOVA because we need to sort from new proposition to old proposition
                //uasort($allAnswers, sortByPosOrder);

                foreach ($allAnswers as $answerid => $answer)
                {
                    $user = Person::getSimpleUserById(@$answer["user"]);
                    if (!empty($answer["answers"]))
                    {
                        $haveTasks = false;
                        $percentageTasks = "--";
                        $dataPer = 0;
                        $iconTasks = "";
                        $actions = [];
                        $currentDate = new DateTime();
                        $currentDate->format("d/m/Y");
                        $currentTimestamp = $currentDate->getTimestamp();
                        $cur = 0;
                        $nbFinishedTask = 0;
                        $nbNotFinishedTask = 0;
                        $haveDepense = false;
                        $seen = false;

                        if (!empty(Yii::app()->session['userId']) && !empty($answer["visitor_counter"]["user"]) && in_array(Yii::app()->session['userId'], $answer["visitor_counter"]["user"])){
                            $seen = true ;
                        }

                        if (empty(Yii::app()->session['userId']) || $answer["user"] == Yii::app()->session['userId'] ){
                            $seen = true ;
                        }

                        if (!empty($answer["answers"]["aapStep1"]["depense"]))
                            $haveDepense = true;

                        $statusAction = "<span class='label label-default'>Aucun tache</span>";
                        $urgentState = isset($answer['answers']['aapStep1']["multiCheckboxPlusurgency"]) ? "Urgent" : "";
                        //var_dump($answer['answers']['aapStep1']["multiCheckboxPlus"]["value"]);
                        $maxDate = "Inconnu";
                        if (isset($answer["project"]["id"]))
                        {
                            $actions = PHDB::find(Actions::COLLECTION, ["parentId" => new MongoId($answer["project"]["id"]) , "parentType" => Project::COLLECTION,
                                // "name" => $a["poste"]
                            ]);

                            $actions = array_reduce($actions, function ($carry, $item)
                            {
                                if (!empty($item["tasks"]))
                                {
                                    $carry = array_merge($carry, $item["tasks"]);
                                }
                                return $carry;
                            }
                                , []);

                            $percent = count(array_filter($actions, function ($ele)
                                {
                                    return !empty($ele["checked"]) && ($ele["checked"] == "true");
                                })) / count($actions) * 100;
                            $percentageTasks = round($percent) . "%";
                            $dataPer = round($percent, -1);
                            if ($percent == 100)
                            {
                                $iconTasks = "fa-check";
                            }
                            else
                            {
                                $iconTasks = "fa-times";
                            }

                            //get max date in task and status
                            $countTask = count($actions);
                            foreach ($actions as $kaction => $vaction)
                            {
                                if (isset($vaction["checked"]) && ($vaction["checked"] === true || $vaction["checked"] === "true")) $nbFinishedTask++;
                                else $nbNotFinishedTask++;
                                $df = null;
                                if (is_string(@$vaction["endDate"])) $df = DateTime::createFromFormat("d/m/Y", @$vaction["endDate"])->getTimestamp();
                                if ($df > $cur) $cur = $df;
                            }
                            // var_dump($nbFinishedTask);
                            // var_dump($nbNotFinishedTask);
                            // var_dump($countTask);
                            if ($nbNotFinishedTask != 0 && $cur < $currentTimestamp) $statusAction = "<span class='label label-danger'>Date dépassé</span>";
                            /*elseif ($nbFinishedTask == $countTask) $statusAction = "<span class='label label-success'>Terminé</span>";
                            elseif ($countTask != 0 && $cur >= $currentTimestamp) $statusAction = "<span class='label label-info'>En cours</span>";*/

                            //end task story
                            $maxDate = date("d/m/Y", $cur);

                            // check if finished date change
                            if (isset($answer["projectEndDateStory"]) && is_array($answer["projectEndDateStory"]))
                            {
                                if (!in_array($maxDate, $answer["projectEndDateStory"]))
                                {
                                    $answer["projectEndDateStory"][] = $maxDate;
                                    PHDB::update(Form::ANSWER_COLLECTION, array(
                                        "_id" => new MongoId($answer["_id"]['$id'])
                                    ) , array(
                                        '$set' => ["projectEndDateStory" => $answer["projectEndDateStory"]]
                                    ));
                                }
                            }
                        }

                        //get admin and evaluator rating
                        $evaluation = @$answer["answers"][$stepEval]["evaluation"];
                        $votantCounter = isset($answer["answers"][$stepEval]["evaluation"]) ? count($answer["answers"][$stepEval]["evaluation"]) : 0;
                        
                        $votantModal = 
                        '<div id="modal'.(string)$answer["_id"]['$id'].'" class="modal fade govamodal modalValidater" role="dialog" style="z-index:1000000">'.
                            '<div class="votant modal-dialog">'.
                                '<div class="modal-content">'.
                                    '<div class="modal-header">'.
                                        '<button type="button" class="close" data-dismiss="modal">&times;</button>'.
                                        '<h4 class="modal-title">Votants</h4>'.
                                    '</div>'.
                                    '<div class="modal-body">'.
                                        '<div class="list-group">';
                                        if($votantCounter !=0){
                                            foreach($answer["answers"][$stepEval]["evaluation"] as $kvotant => $vvotant){
                                                $userVotant = Person::getSimpleUserById($kvotant);
                                                $votantModal.=
                                                
                                                    '<a href="#page.type.citoyens.id.'.(string)$userVotant["_id"] .'" class="list-group-item lbh-preview-element tooltips" style="min-height:70px">'.
                                                    '<div class="col-xs-4 img-contain">';
                                                    if (isset($userVotant["profilMediumImageUrl"]))
                                                        $votantModal.= '<img src="'.$userVotant["profilMediumImageUrl"].'" alt="'.@$userVotant["name"].'" class="img-responsive img-circle" />';
                                                    else
                                                        $votantModal.='<img src="'.Yii::app()->getModule( "co2" )->assetsUrl.'"/images/thumbnail-default.jpg" alt="'.@$userVotant["name"].'" class="img-responsive img-circle" />';
                                                    $votantModal.=
                                                    '</div>'.
                                                    '<div class="col-xs-8 text-left">'.
                                                        '<span class="name">'.$userVotant["name"].'</span><br/> '.
                                                    '</div>'.
                                                    '</a>';

                                            }
                                        }
                                        $votantModal.=
                                        '</div>'.
                                    '</div>'.
                                '</div>'.

                            '</div>'.
                        '</div>';

                        $criterions = @$el_configform["subForms"][$stepEval]["params"]["config"]["criterions"];

                        foreach ($community as $kcom => $vcom){
                            if (!isset($evaluation[$kcom])){
                                $evaluation[$kcom] = $criterions;
                            }
                            foreach ($criterions as $kcrit => $vcrit){
                                if (!isset($evaluation[$kcom][$kcrit])) $evaluation[$kcom][$kcrit] = $vcrit;
                            }
                            ksort($evaluation[$kcom]);
                        }
                        $cummulMean = 0;
                        foreach ($evaluation as $key => $value){
                            $notes = 0;
                            ksort($value);
                            foreach ($value as $kv => $vv){
                                //var_dump($el_configform["subForms"][$stepEval]["params"]["config"]["criterions"][$kv]);
                                $notes = $notes + ((float)$vv["note"] * (float)@$el_configform["subForms"][$stepEval]["params"]["config"]["criterions"][$kv]["coeff"]);
                            }
                            $cummulMean = $cummulMean + ($notes / $sumCoeff);
                        }
                        $cummulMean = $cummulMean / count($evaluation);
                        
                        //get public rating
                        $publicRating = 0;
                        $myRate = "false";
                        $countLike = 0;
                        if (isset($answer["publicRating"]))
                        {
                            foreach ($answer["publicRating"] as $idPublic => $valuePublic)
                            {
                                if ($idPublic == (string)@$me["_id"]) $myRate = (string)$valuePublic;
                                if ($myRate != "false" && $myRate != "true") $myRate = "false";
                                $publicRating = $publicRating + (int)$valuePublic;
                                if ($valuePublic == "true") $countLike++;
                            }
                            //$publicRating = $publicRating/count($answer["publicRating"]);

                        }
                        $publicVoteConfig = "all";
                        if (isset($el_configform["subForms"][$stepEval]["params"]["config"]["publicVote"])) $publicVoteConfig = $el_configform["subForms"][$stepEval]["params"]["config"]["publicVote"];
                        //name
                        $name = !empty(@$answer["answers"]["aapStep1"]["titre"]) ? @$answer["answers"]["aapStep1"]["titre"] : @$answer["answers"]["aapStep1"]["proposition"];
                        // description
                        $description = @$answer["answers"]["aapStep1"]["description"];
                        $tags = @$answer["answers"]["aapStep1"]["tags"];
                        //img profil
                        $imgAnsw = (isset($answer["profilMediumImageUrl"])) ? Yii::app()->createUrl($answer["profilMediumImageUrl"]) : Yii::app()->getModule(Yii::app()
                                ->params["module"]["parent"])
                                ->getAssetsUrl() . "/images/thumbnail-default.jpg";

                        $retain = ["label" => "En attente", "class" => "btn-primary", "retain" => false, "icon" => "hand-stop-o"];
                        if(isset($answer["acceptation"]) && $answer["acceptation"] == "retained"){
                            $retain["label"] = "Retenu";
                            $retain["class"] = "btn-success";
                            $retain["retain"] = true;
                            $retain["icon"] = "hand-rock-o";

                        }elseif(isset($answer["acceptation"]) && $answer["acceptation"] == "rejected"){
                            $retain["label"] = "Non retenu";
                            $retain["class"] = "btn-success";
                            $retain["retain"] = false;
                            $retain["icon"] = "thumbs-down";
                        }

                        ?>

                        <?php if($view=="compact"){ ?>
                            <div class="col-xs-12 col-md-4 col-sm-6">
                                <a href="javascript:;" data-url = 'slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.sheet.answer.<?php echo (string)$answer["_id"]['$id'] ?>'  class="aapgetaapview">
                                    <div class="div compact-container <?php echo ($seen ? "" : "unseen-proposition bdgdiv"); ?>" data-label="nouv.">
                                        <h6><?= $name ?></h6>
                                        <small><i class="fa fa-user" style="color: #7da53d"></i> Par <b><i><?=@$user["name"] ?></i></b></small>
                                        <small>Le  <i class="fa fa-calendar-o" style="color: #7da53d"></i> <b><i><?php echo date('d/m/Y  ', $answer["created"]); ?></i></b></small>
                                    </div>
                                </a>
                            </div>
                        <?php }else{ ?>
                        <div class="row list-group-item <?php echo ($seen ? "" : "unseen-proposition bdgdiv"); ?>"  style="margin-bottom:20px" data-label="nouv.">
                            <div   class="">
                                <?php if (@$view === "detailed")
                                { ?>
                                    <div class="media col-md-4 col-lg-4 no-padding">
                                        <div class="contain-img-left">
                                            <figure class="">
                                                <a href="javascript:;" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.sheet.answer.<?php echo (string)$answerid; ?>" class="aapgoto aaptitlegoto">
                                                <img src="<?=$imgAnsw
                                                ?>" alt="projet" class="img-responsive " data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.form.answer.<?php echo (string)$answerid; ?>" style="width: 100%;">
                                                </a>
                                            </figure>
                                        </div>
                                    </div>
                                    <?php
                                } ?>
                                <?php if (@$view === "minimalist")
                                { ?>
                                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-center"
                                        style="background-image:url('<?=$imgAnsw?>');background-position: center;background-size: cover;background-repeat: no-repeat;">
                                        <div style="position:relative;height:80px;width:100%">
                                            <div class="prop-compact-progression" style="<?=count($actions) === 0 ? "display:none" : "display:inline-block;" ?>">
                                                <div class="progress" data-percentage="<?php echo $dataPer ?>">
                                                    <span class="progress-left">
                                                        <span class="progress-bar"></span>
                                                    </span>
                                                                    <span class="progress-right">
                                                        <span class="progress-bar"></span>
                                                    </span>
                                                    <div class="progress-value">
                                                        <div>
                                                            <?php echo $percentageTasks ?><br>
                                                            <span><i class="fa <?php echo $iconTasks ?>"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <?php
                                } ?>
                                <div class="<?=@$view === "minimalist" ? "col-lg-4 col-md-4 col-sm-10 col-xs-12" : "col-md-8 col-lg-8 col-xs-12" ?> padding-top-5">
                                    <!-- <h4 class="list-group-item-heading">Blog Title</h4> -->
                                    <h4 class="list-group-item-heading"  <?=$view === "minimalist" ? "style='display: flex;font-size:13px'" : "" ?>>
                                        <a href="javascript:;" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.sheet.answer.<?php echo (string)$answerid; ?>" class="aapgoto aaptitlegoto minimalist-name"> <?php echo $name ?> </a>
                                    </h4>
                                    <?php if (@$view === "detailed")
                                    { ?>
                                        <small><i class="fa fa-user" style="color: #7da53d"></i> Par <b><i><?=@$user["name"] ?></i></b>&nbsp;&nbsp;&nbsp;&nbsp; Le  <i class="fa fa-calendar-o" style="color: #7da53d"></i> <b><i><?php echo date('d/m/Y  ', $answer["created"]); ?></i></b>&nbsp; à &nbsp;<i class="fa fa-clock-o" style="color: #7da53d"></i> <b><i><?php echo date('H:i:s ', $answer["created"]); ?></i></b> </small>
                                        <small class="end-project-date"><i class="fa fa-calendar" style="color: #7da53d"></i> Date fin : <b><i><?php echo @$maxDate; ?></i></b></small> <br>
                                        <hr style="border-top: 2px dashed #3f4e58;margin-bottom: 10px;">
                                        <?/*=$statusAction
                                        */?>

                                        <!--<span class='label <?/*=$urgentState == "Urgent" ? "label-danger" : "hidden" */?> '><?/*=$urgentState */?></span>-->
                                        <small>
                                            <i><b>
                                                    <?=count($actions) !== 0 && isset($answer["projectEndDateStory"]) && count($answer["projectEndDateStory"]) !== 0 ? "La date de fin a été changé " . count($answer["projectEndDateStory"]) . " fois" : "" ?>
                                            </i></b>
                                        </small>
                                        <?php
                                    } ?>

                                    <?php if (@$view === "detailed"){ ?>
                                        <p class="list-group-item-text project-description list-markdown"><?=$description ?></p>
                                    <?php } ?>

                                    <ul class="tag-list margin-top-5">
                                            <?php
                                            if (!empty($tags) && is_array($tags)){
                                                foreach ($tags as $ktag => $vtag){
                                                if(!empty($vtag))
                                                    { ?>
                                                        <a href="javascript:;" class="btn-tag-panel template-tag-panel" data-tag="<?=$vtag ?>">
                                                            <span class="badge  btn-tag tag" data-tag-value="cte" data-tag-label="cte">#<?=$vtag ?></span>
                                                        </a>
                                                    <?php
                                                    }
                                                }
                                            }
                                            ?>
                                    </ul>
                                </div>

                                <div class="<?=@$view === "minimalist" ? "col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding" : "col-md-8 col-lg-8 col-sm-12 col-xs-12" ?> pull-right">
                                    <?php if (@$view === "detailed")
                                    { ?>
                                    <div>
                                        <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12 " >

                                            <span class='label <?=$urgentState == "Urgent" ? " urgent-state" : "hidden" ?> '><i class="fa fa-exclamation-triangle"></i>&nbsp;<?=$urgentState ?></span>

                                            <select data-id="<?= $answerid ?>" class="statustags" multiple disabled>
                                                <option disabled></option>
                                                <?php
                                                foreach ($badgelabel as $bdgid => $bdglbl){
                                                    ?>
                                                    <option <?php if (!empty($answer["status"]) && in_array($bdgid, $answer["status"])) { echo "selected"; } ?> value="<?php echo $bdgid ?>"><?php  echo $bdglbl ?></option>
                                                <?php } ?>

                                            </select>
                                         </div>

                                    </div>
                                    <?php } ?>
                                    <div class="<?=@$view === "minimalist" ? "text-center col-md-6 col-lg-6 col-sm-6 col-xs-12 " : "col-md-5 col-lg-5 col-sm-5 col-xs-12" ?>">

                                        <?php if (@$view === "detailed"){ ?>
                                        <a href="javascript:;" data-url = 'slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.sheet.answer.<?php echo (string)$answer["_id"]['$id'] ?>'  class="aapgetaapview aap-Showmorebtn">
                                            <?php echo Yii::t("form","Read more") ?>
                                        </a>

                                            <?php if (isset(Yii::app()->session['userId']) && (Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) || Form::canAdmin((string)$elform["_id"]['$id']) )){?>

                                                    <a href="javascript:;" class="btn st-enable-btn" data-loc="edit" data-id="<?= $answerid ?>" type="button" ><i class="fa fa-pencil"></i> modifier status</a>

                                            <?php } ?>
                                        <?php } ?>

                                        <?php if (@$view === "minimalist"){ ?>
                                        <div class="btn-group-container">
                                            <div class="btn-group">
                                                <?php if (isset(Yii::app()->session['userId']) && (Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) || Form::canAdmin((string)$elform["_id"]['$id'])))
                                                { ?>
                                                    <button type="button" class="btn btn-sm btn-primary btn-retain-reject" data-id="<?=(string)$answer["_id"]['$id'] ?>" data-retain="<?=$retain["retain"] ? 'true' : 'false' ?>"><?=$retain["label"] ?></button>
                                                    <?php
                                                }
                                                if ($haveDepense){
                                                ?>
                                                    <button type="button" class="btn btn-sm btn-primary aapopenmodalbtn aapgetinputsa tooltips" data-id="<?php echo $answerid ?>" data-form="<?php echo $answer["form"] ?>" data-formstep="<?=$stepAction ?>" data-inputid="suivredepense" data-toggle="tooltip" data-placement="bottom" data-original-title="Action"> <i class="fa fa-pencil-square-o"></i></button>
                                                    <button type="button" class="btn btn-sm btn-primary aapgetobservatory tooltips" data-id="<?php echo (string)$answerid; ?>" data-toggle="tooltip" data-placement="bottom" data-original-title="Observatoire">
                                                        <i class="fa fa-binoculars" aria-hidden="true"></i>
                                                    </button>

                                                    <?php
                                                   } else {
                                                ?>
                                                    <button type="button" class="btn btn-sm btn-primary disable aapopenmodalbtn aapgetinputsa tooltips" disabled data-id="<?php echo $answerid ?>" data-form="<?php echo $answer["form"] ?>" data-formstep="<?=$stepAction ?>" data-inputid="suivredepense" data-toggle="tooltip" data-placement="bottom" data-original-title="Action"> <i class="fa fa-pencil-square-o"></i></button>
                                                    <button type="button" class="btn btn-sm btn-primary disable aapopenmodalbtn aapgetinputsa aapgetobservatory tooltips" data-id="<?php echo (string)$answerid; ?>" data-toggle="tooltip" data-placement="bottom" data-original-title="Observatoire">
                                                        <i class="fa fa-binoculars" aria-hidden="true"></i>
                                                    </button>

                                                <?php
                                                   }
                                                ?>


                                                <?php
                                                if ($haveDepense){
                                                ?>
                                                <button type="button" class="btn btn-sm btn-primary aapopenmodalbtn aapgetinputsa tooltips" data-id="<?php echo $answerid ?>" data-form="<?php echo $answer["form"] ?>" data-formstep="<?=$stepFinancor ?>" data-inputid="financer"  data-toggle="tooltip" data-placement="bottom" data-original-title="Financement">
                                                    <i class="fa fa-euro" aria-hidden="true"></i>
                                                </button>
                                                <?php
                                                   } else {
                                                ?>
                                                    <button type="button" class="btn btn-sm btn-primary disable aapopenmodalbtn aapgetinputsa tooltips" disabled data-id="<?php echo $answerid ?>" data-form="<?php echo $answer["form"] ?>" data-formstep="<?=$stepFinancor ?>" data-inputid="financer"  data-toggle="tooltip" data-placement="bottom" data-original-title="Financement">
                                                        <i class="fa fa-euro" aria-hidden="true"></i>
                                                    </button>
                                                    <?php
                                                }
                                                ?>

                                                <?php if (isset(Yii::app()->session['userId']) && (Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) || Form::canAdmin((string)$elform["_id"]['$id']) )){?>
                                                    <button type="button" class="btn btn-sm btn-primary aapgetaapview tooltips" data-url="slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.form.answer.<?php echo (string)$answerid; ?>" data-toggle="tooltip" data-placement="bottom" data-original-title="Editer cette proposition"> <i class="fa fa-pencil-square"></i></button>
                                                    <button type="button" class="btn btn-sm btn-danger btn-delete-answer tooltips" data-id="<?php echo $answerid ?>" data-toggle="tooltip" data-placement="bottom" data-original-title="Supprimer cette proposition"><i class="fa fa-times"></i></button>
                                                <?php } ?>
                                                 <?php if(isset(Yii::app()->session['userId'])){ ?>
                                                        <a href="javascript:;" class="btn btn-sm btn-primary openAnswersComment tooltips" onclick="commentObj.openPreview('answers','<?= (string)$answerid ?>','<?= (string)$answerid ?>', 'Commentaire')" data-toggle="tooltip" data-placement="bottom" data-original-title="Ajouter un commentaire">
                                                            <?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>(string)$answerid,"contextType"=>"answers", "path"=>(string)$answerid))?> <i class='fa fa-commenting'></i>
                                                        </a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <?php if (@$view === "detailed")
                                    { ?>
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                            <div class="prop-compact-progression" style="<?=count($actions) === 0 ? "display:none" : "display:inline-block;float:right;" ?>">
                                                <div class="progress" data-percentage="<?php echo $dataPer ?>">
                                                    <span class="progress-left">
                                                        <span class="progress-bar"></span>
                                                    </span>
                                                                        <span class="progress-right">
                                                        <span class="progress-bar"></span>
                                                    </span>
                                                    <div class="progress-value">
                                                        <div>
                                                            <?php echo $percentageTasks ?><br>
                                                            <span><i class="fa <?php echo $iconTasks ?>"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    } ?>
                                    <div class="  text-center <?=@$view === "minimalist" ? "col-md-6 col-lg-6 col-sm-6  col-xs-12" : "col-md-4 col-lg-4 col-sm-4 col-xs-12" ?>">
                                        <div class="panel panel-default">
                                            <div class="panel-body evaluator-panel col-xs-12 <?=@$view === "minimalist" ? "padding-5" : "" ?>" <?=@$view === "minimalist" ? "style='display: flex;align-items: center;min-height: 50px;'" : "" ?>>
                                                <div class="col-xs-12 <?=@$view === "minimalist" ? "no-padding" : "" ?> ">
                                                    <div class="col-xs-12 padding-bottom-0">
                                                        <?php
                                                        $totalprice = 0;
                                                        $totalfinance = 0;
                                                        $colorprice = "black";
                                                        if (!empty($answer["answers"]["aapStep1"]["depense"])){
                                                            foreach ($answer["answers"]["aapStep1"]["depense"] as $depId => $dep)
                                                            {
                                                                if (!empty($dep["price"])){
                                                                    $totalprice += $dep["price"];

                                                                    $totalfi = 0;
                                                                    if (!empty($dep["financer"]))
                                                                    {
                                                                        foreach ($dep["financer"] as $fiid => $fi)
                                                                        {
                                                                            if (isset($fi["amount"]))
                                                                            {
                                                                                $totalfi += (int)$fi["amount"];
                                                                            }
                                                                        }

                                                                    }
                                                                    $totalfinance += $totalfi;

                                                                }
                                                            }

                                                            if ($totalprice <= $totalfinance){
                                                                $colorprice = "green";
                                                            } elseif ($totalfinance > 0){
                                                                $colorprice = "orange";
                                                            }

                                                        }

                                                        ?>
                                                        <span >
                                                            <?= $totalfinance?>
                                                         <i class="fa fa-euro"></i> /
                                                            <span style="color: #0bcb9a">
                                                            <?= $totalprice?>
                                                         <i class="fa fa-euro"></i>
                                                                </span>
                                                    </span>

                                                    </div>
                                                    <div class="col-xs-12 padding-bottom-0">
                                                        <?php
                                                        if (@$el_configform["subForms"][$stepEval]["params"]["config"]["type"] == "noteCriterionBased")
                                                        { ?>
                                                            <div class="rater-admin text-center tooltips" data-rating="<?= is_numeric($cummulMean/2) ? $cummulMean/2 : 0 ?>" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="<?=htmlspecialchars("<small>Admins et evaluateurs</small> </br>", ENT_QUOTES, 'UTF-8') . round($cummulMean/2, 3)." / 5" ?>"></div>
                                                            <?php
                                                        }
                                                        elseif (@$el_configform["subForms"][$stepEval]["params"]["config"]["type"] == "starCriterionBased")
                                                        { ?>
                                                            <div class="rater-admin text-center tooltips" data-rating="<?= is_numeric($cummulMean) ? $cummulMean : 0 ?>" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="<?=htmlspecialchars("<small>Admins et evaluateurs</small> </br>", ENT_QUOTES, 'UTF-8') . round($cummulMean, 3)." / 5" ?>"></div><br>
                                                            <?php
                                                        } ?>
                                                    </div>
                                                    <?php if (@$view === "detailed")
                                                    { ?>
                                                        <div class="col-xs-12 padding-top-0">
                                                            <hr class="margin-0" style="border-top: 1px dashed #ddd;margin: 0 auto; width: 80%;">
                                                            <?php if (isset(Yii::app()->session['userId']) && $publicVoteConfig == "members" && Authorisation::isElementMember($el["id"], @$el["type"], Yii::app()->session['userId']))
                                                            { ?>
                                                                <a href="javascript:;" class="like-project letter-green tooltips margin-right-10" data-value="<?=$myRate
                                                                ?>" data-ans="<?=(string)$answer["_id"]['$id'] ?>" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="J'adore">
                                                                    <i class="fa <?=$myRate == "true" ? "fa-heart" : "fa-heart-o" ?>"></i>
                                                                    <small class="counter"><?=$countLike ?></small>
                                                                </a>
                                                                <?php
                                                            } ?>
                                                            <?php if ($publicVoteConfig == "all")
                                                            { ?>
                                                                <a href="javascript:;" class="like-project letter-green tooltips pull-right margin-right-10" data-value="<?=$myRate
                                                                ?>" data-ans="<?=(string)$answer["_id"]['$id'] ?>" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="J'adore">
                                                                    <i class="fa <?=$myRate == "true" ? "fa-heart" : "fa-heart-o" ?>"></i>
                                                                    <small class="counter" style="font-size: 12px"><?=$countLike ?></small>
                                                                </a>
                                                                <?php
                                                            } ?>
                                                            <!-- <a href="javascript:;" class="dislike-project" data-value="dislike"><i class="fa fa-thumbs-o-down"></i></a> -->
                                                            <!-- <div class="rater-public pull-right tooltips" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="<i><span><small>Tous <?=$publicRating ?> | Moi <?=$myRate ?></small></span></i>" data-rating=<?=$publicRating ?>  data-ans="<?=(string)$answer["_id"]['$id'] ?>"></div> -->
                                                            <a href="javascript:;" class="like-project pull-right tooltips margin-right-10" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Nombre de visite">
                                                                <i class="fa fa-eye"></i> <span id ="countv"  style="font-size: 12px"> <?php echo (isset($answer["visitor_counter"]["count"]) ? $answer["visitor_counter"]["count"] : 0 ) ?> </span>
                                                            </a>
                                                            <?= $votantModal ?>
                                                            <a href="javascript:;" class="pull-right tooltips margin-right-10" data-toggle="modal" data-target="#modal<?=  (string)$answer["_id"]['$id'] ?>" onclick="$('#modal<?= $votantCounter !=0 ? (string)$answer["_id"]['$id'] : null ?>').modal('show')">
                                                                <i class="fa fa-gavel"></i> <span id ="countv"  style="font-size: 12px"> <?= $votantCounter ?> </span>
                                                            </a>
                                                            
                                                        </div>
                                                        <?php
                                                    } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <?php if (@$view === "detailed"){ ?>
                                    <div class="col-btn-group-vertical ">
                                        <div class="btn-group-vertical btn-group-custom" style="top: -120px;position: absolute;">
                                            <?php if (isset(Yii::app()->session['userId']) && (Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) || Form::canAdmin((string)$elform["_id"]['$id'])))
                                            { ?>
                                                <button type="button" class="btn btn-sm btn-primary btn-retain-reject tooltips" data-id="<?=(string)$answer["_id"]['$id'] ?>" data-retain="<?=$retain["retain"] ? 'true' : 'false' ?>" data-toggle="tooltip" data-placement="left" data-original-title="<?=$retain["label"] ?>"> <i class="fa fa-<?=$retain["icon"] ?>"></i></button>
                                                <?php
                                            }
                                            if ($haveDepense){
                                                ?>
                                                <button type="button" class="btn btn-sm btn-primary aapopenmodalbtn aapgetinputsa tooltips" data-id="<?php echo $answerid ?>" data-form="<?php echo $answer["form"] ?>" data-formstep="<?=$stepAction ?>" data-inputid="suivredepense" data-toggle="tooltip" data-placement="left" data-original-title="Action"> <i class="fa fa-pencil-square-o"></i></button>
                                                <button type="button" class="btn btn-sm btn-primary aapgetobservatory tooltips" data-id="<?php echo (string)$answerid; ?>" data-toggle="tooltip" data-placement="left" data-original-title="Observatoire">
                                                    <i class="fa fa-binoculars" aria-hidden="true"></i>
                                                </button>

                                                <?php
                                            } else {
                                                ?>
                                                <button type="button" class="btn btn-sm btn-primary disable aapopenmodalbtn aapgetinputsa tooltips" disabled data-id="<?php echo $answerid ?>" data-form="<?php echo $answer["form"] ?>" data-formstep="<?=$stepAction ?>" data-inputid="suivredepense" data-toggle="tooltip" data-placement="left" data-original-title="Action"> <i class="fa fa-pencil-square-o"></i></button>
                                                <button type="button" class="btn btn-sm btn-primary disable aapopenmodalbtn aapgetinputsa tooltips" disabled  data-id="<?php echo (string)$answerid; ?>" data-toggle="tooltip" data-placement="left" data-original-title="Observatoire">
                                                    <i class="fa fa-binoculars" aria-hidden="true"></i>
                                                </button>
                                                <?php
                                            }
                                            ?>


                                            <?php
                                            if ($haveDepense){
                                                ?>
                                                <button type="button" class="btn btn-sm btn-primary aapopenmodalbtn aapgetinputsa tooltips" data-id="<?php echo $answerid ?>" data-form="<?php echo $answer["form"] ?>" data-formstep="<?=$stepFinancor ?>" data-inputid="financer"  data-toggle="tooltip" data-placement="left" data-original-title="Financement">
                                                    <i class="fa fa-credit-card-alt" aria-hidden="true"></i>
                                                </button>
                                                <?php
                                            } else {
                                                ?>
                                                <button type="button" class="btn btn-sm btn-primary disable aapopenmodalbtn aapgetinputsa tooltips" disabled data-id="<?php echo $answerid ?>" data-form="<?php echo $answer["form"] ?>" data-formstep="<?=$stepFinancor ?>" data-inputid="financer"  data-toggle="tooltip" data-placement="left" data-original-title="Financement">
                                                    <i class="fa fa-credit-card-alt" aria-hidden="true"></i>
                                                </button>
                                                <?php
                                            }
                                            ?>

                                            <?php if (isset(Yii::app()->session['userId']) && ((Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) || Form::canAdmin((string)$elform["_id"]['$id']) ) || ($answer["user"] == Yii::app()->session['userId']))){?>
                                                <button type="button" class="btn btn-sm btn-primary aapgetaapview tooltips" data-url="slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.form.answer.<?php echo (string)$answerid; ?>" data-elformid="<?php echo $answer['form']; ?>" data-toggle="tooltip" data-placement="left" data-original-title="Editer cette proposition"> <i class="fa fa-pencil-square"></i></button>

                                            <?php } ?>

                                            <?php if (isset(Yii::app()->session['userId']) && (Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) || Form::canAdmin((string)$elform["_id"]['$id']) )){?>
                                                <button type="button" class="btn btn-sm btn-danger btn-delete-answer tooltips" data-id="<?php echo $answerid ?>" data-toggle="tooltip" data-placement="left" data-original-title="Supprimer cette proposition"><i class="fa fa-times"></i></button>
                                            <?php } ?>

                                            <?php if(isset(Yii::app()->session['userId'])){ ?>
                                                <a href="javascript:;" class="btn btn-sm btn-primary openAnswersComment tooltips" onclick="commentObj.openPreview('answers','<?= (string)$answerid ?>','<?= (string)$answerid ?>', 'Commentaire')" data-toggle="tooltip" data-placement="left" data-original-title="Ajouter un commentaire">
                                                    <?php echo PHDB::count(Comment::COLLECTION, array("contextId"=>(string)$answerid,"contextType"=>"answers", "path"=>(string)$answerid))?> <i class='fa fa-commenting'></i>
                                                </a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                        </div>
                        </div>

                        <?php
                        }
                    }
                }
            }
            ?>
        </div>
    </div>
</div>

<script type="text/javascript">

    var statusicon = { progress : "hourglass" ,finished : "check-circle" ,abandoned : "times-circle", newaction : "plus-circle",call : "tag",projectstate : "rocket",finance : "euro", vote : "gavel"
    };

    jQuery(function ($) {
        function formatState (state) {
            if (!state.id) { return state.text; }
            var colori = "statusicon";
            if(state.id && state.id == "abandoned")
                colori = "statusicondanger";
            var $state = jQuery(
                '<span><i class="'+colori+' fa fa-'+statusicon[state.id]+' +"></i> ' + state.text + '</span>'
            );

            return $state;
        };

        let optionSelect2 = {
            formatSelection: formatState,
            closeOnSelect: false,
            //width: '80%',
            placeholder: "aucun status",
            containerCssClass : 'aapstatus-container',
            dropdownCssClass: "aapstatus-dropdown"
        };

        let $select2 = $(".statustags").select2(optionSelect2);

        $(".st-enable-btn").on("click", function () {
            var thisbtn = $(this);
            if (thisbtn.data('loc') == 'edit'){
                thisbtn.data('loc', 'save');
                thisbtn.html('<i class="fa fa-save"></i> enregistrer');
                $("select[data-id='"+thisbtn.data('id')+"']").prop("disabled", false);
            }else if (thisbtn.data('loc') == 'save'){
                thisbtn.data('loc', 'edit');
                thisbtn.html('<i class="fa fa-pencil"></i> modifier status');
                $("select[data-id='"+thisbtn.data('id')+"']").prop("disabled", true);
                var tplCtx = {};
                tplCtx.id = thisbtn.data('id');
                tplCtx.collection = "answers";
                tplCtx.path = "status";
                tplCtx.value = $("select[data-id='"+thisbtn.data('id')+"']").val();
                dataHelper.path2Value( tplCtx, function(params) {
                    toastr.success('sauvegardé');
                });
            }
        });

        $(".st-save-btn").on("click", function () {
            var thisbtn = $(this);
            thisbtn.removeClass('st-save-btn');

        });


        $('.aapopenmodalbtn').on('click',function(e){
            /*e.stopPropagation();
            $("#formInputM").modal("show");*/
        });
        $.each($(".list-markdown"), function(k,v){
            descHtml = dataHelper.markdownToHtml($(v).html(),{
                parseImgDimensions:true,
                simplifiedAutoLink:true,
                strikethrough:true,
                tables:true,
                openLinksInNewWindow:true
            });
            $(v).html(descHtml);
        });

        $('.aapgetinputsa').on('click',function(e){
            e.stopPropagation();
            var thisid = $(this).data('id');
            coInterface.showLoader(".aapinputcontent");
            var ansid = $(this).data('id');
            var ansform = $(this).data('form');
            var ansformstep = $(this).data('formstep');
            var ansinputid = $(this).data('inputid');
            /*ajaxPost(".aapinputcontent", baseUrl+'/survey/answer/getinput/id/'+ansid+'/form/'+ansform+'/mode/w/formstep/'+ansformstep+'/inputid/'+ansinputid,
                null,
                function(res){

                },null);*/
            smallMenu.openAjaxHTML(baseUrl+'/survey/answer/getinput/id/'+ansid+'/form/'+ansform+'/mode/w/formstep/'+ansformstep+'/inputid/'+ansinputid);
        });

        $('.aapgetobservatory').on('click',function(e){
            e.stopPropagation();
            coInterface.showLoader(".aapinputcontent");
            var ansid = $(this).data('id');

            smallMenu.openAjaxHTML(baseUrl+'/survey/answer/ocecoformdashboard/answerid/'+ansid);
        });

        $('.aapgetaapview').on('click',function(e){
            e.stopPropagation();
            coInterface.showLoader(".aapinputcontent");
            var ansurl = $(this).data('url');
            //alert(baseUrl+'/survey/form/getaapview/urll'+ansurl);
            smallMenu.openAjaxHTML(baseUrl+'/survey/form/getaapview/urll/'+ansurl);
        });

        $('.btnaapp_plusdinfo,.aap_plusdinfo').off().on("click", function () {
            window.location.href = $(this).data("url");
            urlCtrl.loadByHash(location.hash);
        });
        $('.evaluator-panel').click(function(e){
            e.stopPropagation();
        });
        $('.tooltips').tooltip();
        /*$('.rater-public').each(function(i, obj) {
            if(userId != ""){
                var starRating1 = raterJs( {
                    rating : $(obj).data("rating"),
                    starSize:25,
                    step:0.5,
                    element:obj,
                    rateCallback:function rateCallback(rating, done) {
                        var $this = this;
                        this.setRating(rating);
                            var tplCtx ={
                                id : $(obj).data("ans"),
                                collection : "answers",
                                path :"publicRating."+userId,
                                value : rating
                            }
                            ajaxPost(null, baseUrl+'/costum/aap/publicrate',
                            tplCtx,
                            function(res){
                                mylog.log(res,"gova")
                                $(obj).attr("data-original-title","<i><span><small>Tous <?=$publicRating ?> | Moi "+res.params+"</small></span></i>");
                                //urlCtrl.loadByHash(location.hash);
                            },null);
                        done();
                    }
                });
            }else{
                var starRating1 = raterJs( {
                    rating : $(obj).data("rating"),
                    starSize:25,
                    step:0.5,
                    showToolTip:true,
                    element:obj,
                    readOnly:true,
                });
            }
        });*/
        $('.rater-admin').each(function(i, obj) {
            var starRatingAdmin = raterJs( {
                rating : (typeof $(obj).data("rating") =="number" ? $(obj).data("rating") : 0),
                starSize:20,
                readOnly:true,
                showToolTip:false,
                element:obj,
            });
        });
        $('.btn-retain-reject').on('click', function(e){
            e.stopPropagation();
            var btn = $(this);
            var id = btn.data("id");
            var retain = btn.text();
            var params = {
                id : id,
                collection: "answers",
                path : "allToRoot",
                format : true,
                value : {
                    acceptation : ((retain == "Retenu") ? "rejected" : "retained")
                }
            }
            btn.html('<i class="fa fa-refresh fa-spin "></i>');
            dataHelper.path2Value( params, function(res) {
                if(res.result){
                    if(exists(res.elt) && exists(res.elt.acceptation)){
                        if(res.elt.acceptation == "retained"){
                            btn.html("<i class='fa fa-hand-rock-o'></i><span class='hidden'>Retenu</span>").fadeIn(500);
                            btn.attr("data-original-title","Retenu");
                            btn.tooltip();
                            //btn.removeClass("btn-default").addClass("btn-success");
                        }else{
                            btn.html("<i class='fa fa-thumbs-down'></i><span class='hidden'>Non retenu</span>").fadeIn(500);
                            btn.attr("data-original-title","Non retenu");
                            btn.tooltip();
                            //btn.removeClass("btn-success").addClass("btn-default");
                        }
                    }
                }
                mylog.log("retenu",res);
            });
        });
        $('.btn-delete-answer').on("click",function(e){
            e.stopPropagation();
            $this = $(this);
            id = $(this).data("id");
            bootbox.dialog({
                title: "Confirmez la suppression",
                message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
                buttons: [
                    {
                        label: "Ok",
                        className: "btn btn-primary pull-left",
                        callback: function() {
                            getAjax("",baseUrl+"/survey/co/delete/id/"+id,function(){
                                toastr.success("Projet supprimé avec succès !");
                                urlCtrl.loadByHash(location.hash);
                                //$this.parent().parent().parent().remove();
                            },"html");
                        }
                    },
                    {
                        label: "Annuler",
                        className: "btn btn-default pull-left",
                        callback: function() {}
                    }
                ]
            });
        });
        $('.like-project').on('click',function(){
            $btn = $(this);
            $val = $btn.data('value').toString();
            var tplCtx ={
                id : $btn.data("ans"),
                collection : "answers",
                format : true,
                path :"publicRating."+userId,
            }
            if($val=="false"){
                $btn.data('value',"true");
                $btn.find('i').removeClass("fa-heart-o").addClass("fa-heart");
                var count = $btn.find('.counter').text();
                $btn.find('.counter').text(parseInt(count)+1);
                tplCtx.value = $btn.data('value').toString();
                ajaxPost(null, baseUrl+'/costum/aap/publicrate',
                    tplCtx,
                    function(res){
                        // if(res.result)
                        //     toastr.success(res.msg)
                        // else
                        //     toastr.error(res.msg)
                    },null);
            }
            else if($val=="true"){
                $btn.data('value',"false");
                $btn.find('i').removeClass("fa-heart").addClass("fa-heart-o");
                var count = $btn.find('.counter').text();
                $btn.find('.counter').text(parseInt(count)-1);
                tplCtx.value = $btn.data('value').toString();
                ajaxPost(null, baseUrl+'/costum/aap/publicrate',
                    tplCtx,
                    function(res){
                        //    if(res.result)
                        //         toastr.success(res.msg)
                        //     else
                        //         toastr.error(res.msg)
                    },null);
            }
        })
        // $('.proposition_list .project-description').moreAndLess({
        //     showChar : 200,
        //     ellipsestext : "...",
        //     moretext : "Voir plus >",
        //     lesstext : "Voir moins",
        //     class : "letter-green"
        // })  
        setTimeout(() => {
            readMoreLess(150);
        }, 500);  
        
        $(".aapgoto").off().on("click", function(){
            if(isUserConnected == "unlogged")
                return $("#modalLogin").modal();
            mylog.log("azeee", $(this).data("url"));
            window.location.href = $(this).data("url");
            urlCtrl.loadByHash(location.hash);

        });
    });

</script>
