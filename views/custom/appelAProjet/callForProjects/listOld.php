
<?php
    $tab = [
        "progress" => "En cours",
        "finished" => "Terminé",
        "abandoned" => "Abandonée",
        "newaction" => "Nouvelle proposition",
        "call" => "Appel au participations",
        "projectstate" => "En Projet",
        "finance" => "appel au financement",
        "vote" => "En evaluation"
    ];
    $sumCoeff = 0;
    if(isset($el_configform["subForms"]["aapStep3"]["params"]["config"]["criterions"])) {
        foreach ($el_configform["subForms"]["aapStep3"]["params"]["config"]["criterions"] as $key => $value) {
            $sumCoeff = $sumCoeff + (float)$value["coeff"];
        }
    }
    $idEl = array_keys($elform["parent"])[0];
    $typeEl = $elform["parent"][$idEl]["type"];
    $allowedRoles = isset($el_configform["subForms"]["aapStep3"]["params"]["canEdit"]) ? 
    $el_configform["subForms"]["aapStep3"]["params"]["canEdit"] : array();
    $me = isset(Yii::app()->session['userId']) ? Person::getById(Yii::app()->session['userId']) : null;
    $roles = isset($me["links"]["memberOf"][$idEl]["roles"]) ? $me["links"]["memberOf"][$idEl]["roles"] : null;
    $isAdmin = isset($me["links"]["memberOf"][$idEl]["isAdmin"]) ? $me["links"]["memberOf"][$idEl]["isAdmin"] :false;
    $canEvaluate =  false;
    $intersectRoles = array_intersect($allowedRoles,$roles);
    if(count($intersectRoles) != 0 || $isAdmin)
        $canEvaluate =true;
    $community = Element::getCommunityByTypeAndId($typeEl, $idEl, "all", null,"Evaluateur", null, ["name", "profilMediumImageUrl"]);
    $creator = Person::getById($elform["creator"]);
    $community[(string)$creator["_id"]] = $creator;
    ?>
<style>
    /* This for loop creates the necessary css animation names Due to the split circle of progress-left and progress right, we must use the animations on each side. */
    .progress[data-percentage="10"] .progress-right .progress-bar {
        animation: loading-1 1.5s linear forwards;
    }

    .progress[data-percentage="10"] .progress-left .progress-bar {
        animation: 0;
    }

    .progress[data-percentage="20"] .progress-right .progress-bar {
        animation: loading-2 1.5s linear forwards;
    }

    .progress[data-percentage="20"] .progress-left .progress-bar {
        animation: 0;
    }

    .progress[data-percentage="30"] .progress-right .progress-bar {
        animation: loading-3 1.5s linear forwards;
    }

    .progress[data-percentage="30"] .progress-left .progress-bar {
        animation: 0;
    }

    .progress[data-percentage="40"] .progress-right .progress-bar {
        animation: loading-4 1.5s linear forwards;
    }

    .progress[data-percentage="40"] .progress-left .progress-bar {
        animation: 0;
    }

    .progress[data-percentage="50"] .progress-right .progress-bar {
        animation: loading-5 1.5s linear forwards;
    }

    .progress[data-percentage="50"] .progress-left .progress-bar {
        animation: 0;
    }

    .progress[data-percentage="60"] .progress-right .progress-bar {
        animation: loading-5 1.5s linear forwards;
    }

    .progress[data-percentage="60"] .progress-left .progress-bar {
        animation: loading-1 1.5s linear forwards 1.5s;
    }

    .progress[data-percentage="70"] .progress-right .progress-bar {
        animation: loading-5 1.5s linear forwards;
    }

    .progress[data-percentage="70"] .progress-left .progress-bar {
        animation: loading-2 1.5s linear forwards 1.5s;
    }

    .progress[data-percentage="80"] .progress-right .progress-bar {
        animation: loading-5 1.5s linear forwards;
    }

    .progress[data-percentage="80"] .progress-left .progress-bar {
        animation: loading-3 1.5s linear forwards 1.5s;
    }

    .progress[data-percentage="90"] .progress-right .progress-bar {
        animation: loading-5 1.5s linear forwards;
    }

    .progress[data-percentage="90"] .progress-left .progress-bar {
        animation: loading-4 1.5s linear forwards 1.5s;
    }

    .progress[data-percentage="100"] .progress-right .progress-bar {
        animation: loading-5 1.5s linear forwards;
    }

    .progress[data-percentage="100"] .progress-left .progress-bar {
        animation: loading-5 1.5s linear forwards 1.5s;
    }



    /*Style by nico*/
    /*EVALUATION*/
    .single_evaluate, .single_task, .single_funding, .single_governance {
        position: relative;
        margin-bottom: 40px;
        transition: .3s;
        box-shadow: 0 2px 5px 0 rgb(63 78 88), 0 2px 10px 0 rgb(63 78 88);
        border-radius: 3px;
    }

    .single_evaluate img {
        width: 100%;
        /*position: absolute;*/
        min-height: 420px;
        -webkit-clip-path: polygon(25% 0%, 100% 0%, 100% 100%, 25% 100%, 0% 50%);
        clip-path: polygon(25% 0%, 100% 0%, 100% 100%, 25% 100%, 0% 50%);
        transition: .3s;
        border: 1px solid #ddd;
        border-radius: 5px;
        object-fit: cover;
        object-position: center;
    }

    .single_evaluate:hover img{
        -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 0 100%, 0 50%);
        clip-path: polygon(0 0, 100% 0, 100% 100%, 0 100%, 0 50%);

    }

    .evaluate_content h4, .single_task h4, .single_funding h4, .single_governance h4 {
        font-size: 22px;
        font-weight: 700;
        border-bottom: 1px dashed #3f4e58;
        line-height: 2;
        text-transform: none;
        color: #3f4e58;
    }

     .status {
        font-size: 19px;
        line-height: 2;
        color: #3f4e58;
    }

     .status span {
         border: solid 2px #555259;
         margin: 3px;
         padding: 3px;
         border-radius: 5px;
         font-weight: bold;
     }

    .evaluate_content h4 span {
        font-size: 20px;
        font-weight: 800;
        float: right;
        font-style: italic;
        color: #9ebc37;
    }

    .evaluate_content {
        display: flex;
        flex-direction: column;
    }

    .evaluate_content p {
        font-weight: 200;
        font-size: 16px;
        letter-spacing: 1px;

        overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;
        -webkit-line-clamp: 10;
        -webkit-box-orient: vertical;
    }

    .evaluate_content .btn-component, .single_task .btn-component, .single_funding .btn-component, .single_governance .btn-component {
        flex-basis: 100%;
        display: flex;
        flex-direction: row;
        align-items: flex-end;
    }
    .evaluate_content .btn-evaluate, .single_task .btn-participate, .single_funding .btn-financing, .single_governance .btn-opinion {
        color: white;
        background-color: #93C020;
        border-color: #93C020;
        font-size: 18px;
        font-weight: 700;
        border-radius: 2px;
        padding : 6px 25px;
    }


    /*TACHE*/

    .single_task h4 span, .single_funding h4 span, .single_governance h4 span {
        font-size: 24px;
        font-weight: 800;
        float: right;
        color: #3f4e58;
    }
    .task-row {
        padding: 15px;
    }
    /*checkbox*/
    .single_task .input_wrapper{
        width: 70px;
        height: 35px;
        position: relative;
        cursor: pointer;
        margin-bottom: 5px;
    }

    .single_task .input_wrapper input[type="checkbox"]{
        width: 70px;
        height: 35px;
        cursor: pointer;
        -webkit-appearance: none;
        -moz-appearance: none;
        appearance: none;
        background: #3f4e58;
        border-radius: 2px;
        position: relative;
        outline: 0;
        -webkit-transition: all .2s;
        transition: all .2s;
    }

    .single_task .input_wrapper input[type="checkbox"]:after{
        position: absolute;
        content: "";
        top: 3px;
        left: 3px;
        width: 28px;
        height: 28px;
        background: #dfeaec;
        z-index: 2;
        border-radius: 2px;
        -webkit-transition: all .35s;
        transition: all .35s;
    }

    .single_task .input_wrapper svg{
        position: absolute;
        top: 50%;
        -webkit-transform-origin: 50% 50%;
        transform-origin: 50% 50%;
        fill: #fff;
        -webkit-transition: all .35s;
        transition: all .35s;
        z-index: 1;
    }

    .single_task .input_wrapper .is_checked{
        width: 18px;
        left: 15%;
        -webkit-transform: translateX(190%) translateY(-30%) scale(0);
        transform: translateX(190%) translateY(-30%) scale(0);
    }

    .single_task .input_wrapper .is_unchecked{
        width: 15px;
        right: 15%;
        -webkit-transform: translateX(0) translateY(-30%) scale(1);
        transform: translateX(0) translateY(-30%) scale(1);
    }

    /* Checked State */
    .single_task .input_wrapper input[type="checkbox"]:checked{
        background: #93C020;
    }

    .single_task .input_wrapper input[type="checkbox"]:checked:after{
        left: calc(100% - 37px);
    }

    .single_task .input_wrapper input[type="checkbox"]:checked + .is_checked{
        -webkit-transform: translateX(0) translateY(-30%) scale(1);
        transform: translateX(0) translateY(-30%) scale(1);
    }

    .single_task .input_wrapper input[type="checkbox"]:checked ~ .is_unchecked{
        -webkit-transform: translateX(-190%) translateY(-30%) scale(0);
        transform: translateX(-190%) translateY(-30%) scale(0);
    }

    /* TABLE */
    .single_task .align-items-center {
        align-items: center !important;
        text-align: center;
    }
    .single_task .table {
        width: 100%;
        margin-bottom: 1rem;
        background-color: transparent;
    }

    .single_task .table th,
    .single_task .table td {
        padding: 1rem;
        vertical-align: top;
        border-top: 1px solid #e9ecef;
    }

    .single_task .table thead th {
        vertical-align: bottom;
        border-bottom: 2px solid #e9ecef;
    }

    .single_task .table tbody + tbody {
        border-top: 2px solid #e9ecef;
    }

    .single_task .table .table {
        background-color: #f8f9fe;
    }
    .single_task .table-responsive {
        display: block;
        overflow-x: auto;
        width: 100%;
        -webkit-overflow-scrolling: touch;
        -ms-overflow-style: -ms-autohiding-scrollbar;
    }
    .single_task .table thead th {
        font-size: .65rem;
        padding-top: .75rem;
        padding-bottom: .75rem;
        letter-spacing: 1px;
        text-transform: uppercase;
        border-bottom: 1px solid #e9ecef;
    }

    .single_task .table th {
        font-weight: 600;
    }
    .single_task .table td,
    .single_task .table th {
        font-size: .8125rem;
        white-space: nowrap;
    }

    .single_task .table.align-items-center td,
    .single_task .table.align-items-center th {
        vertical-align: middle;
    }

    .single_task .table .thead-dark th {
        color: #4d7bca;
        background-color: #1c345d;
    }

    .single_task .table .thead-light th {
        color: #8898aa;
        background-color: #f6f9fc;
    }

    .single_task .table-flush td,
    .single_task .table-flush th {
        border-right: 0;
        border-left: 0;
    }

    .single_task .table-flush tbody tr:first-child td,
    .single_task .table-flush tbody tr:first-child th {
        border-top: 0;
    }

    .single_task .table-flush tbody tr:last-child td,
    .single_task .table-flush tbody tr:last-child th {
        border-bottom: 0;
    }

    .single_task .text-simple, .single_funding .text-simple, .single_governance .text-simple {
        font-size: 16px;
        color: #3f4e58;
        line-height: 36px;
    }
    .single_task .price, .single_governance .price {
        padding: 10px;
        color: #93C020!important;
        background-color: #ffffff;
        border-radius: 4px;
    }
    .single_task .mb-0 {
        margin-bottom: 0 !important;
    }
    .single_task .media-body, .single_funding  .media-body{
        flex: 1 1;
    }
    .single_task .media, .single_funding  .media {
        display: flex;
    }
    .single_funding  .media {
        padding-top: 15px;
    }
    .single_task .line-dark, .single_funding .line-dark, .single_governance .line-dark {
        background-color: #d4d4d4;
    }
    .single_task .line-light, .single_funding .line-light, .single_governance .line-light{
        background-color: #ebebeb
    }

    .avatar {
        font-size: 1rem;
        display: inline-flex;
        width: 48px;
        height: 48px;
        color: #fff;
        border-radius: 50%;
        background-color: #adb5bd;
        align-items: center;
        justify-content: center;
    }

    .avatar img {
        width: 100%;
        border-radius: 50%;
    }

    .avatar-sm {
        font-size: .875rem;
        width: 36px;
        height: 36px;
    }

    .avatar-group .avatar {
        position: relative;
        z-index: 2;
        border: 1px solid #93C020;
    }

    .avatar-group .avatar:hover {
        z-index: 3;
    }

    .avatar-group .avatar + .avatar {
        margin-left: -1px;
    }
    .justify-center {
        justify-content: center;
    }

    /*FINANCEMENT*/

    .single_funding h4 span {
        font-size: 24px;
        font-weight: 800;
        float: right;
        color: #3f4e58;
    }
    .single_funding .valueAbout{
        border-left: 1px solid #dbdbdb;
    }


    .single_funding .contentInformation{
        border-bottom: 1px solid #dbdbdb;
    }

    .single_funding .labelAbout span{
        width: 20px;
        padding-right: 5px;
        text-align: -moz-center;
        text-align: center;
        text-align: -webkit-center;
        float: left;
    }
    .single_funding .labelAbout span i{
        font-size: 14px;
    }

    .single_funding .md-editor > textarea {
        padding: 10px;
    }

    .single_funding .descriptiontextarea label{
        margin-left:10px;
    }

    .single_funding .progress-bar-success {
        background-color: #93C020;
    }

    .single_funding .progress-bar-info {
        background-color: #fff;
    }

    .single_funding .widget {
        width: 100%;
        float: left;
        margin: 0px;
        list-style: none;
        text-decoration: none;
        -moz-box-shadow: 0px 1px 1px 0px rgba(0, 0, 0, 0.2);
        -webkit-box-shadow: 0px 1px 1px 0px rgb(0 0 0);
        box-shadow: 0px 1px 1px 0px rgb(0 0 0);
        color: #FFF;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
        border-radius: 5px;
        padding: 10px 10px;
        /* margin-bottom: 20px; */
        /* min-height: 120px; */
        position: relative;
    }
    /*.single_funding .widget.widget-black {
      background: #ffffff;
     }*/
    .single_funding .widget .widget-title,
    .single_funding .widget .widget-subtitle,
    .single_funding .widget .widget-int,
    .single_funding .widget .widget-big-int {
        width: 100%;
        float: left;
        text-align: center;
    }
    .single_funding .text-success1{color:green;}
    .single_funding .widget .widget-title {
        font-size: 14px;
        font-weight: 600;
        margin-bottom: 5px;
        line-height: 20px;
        text-transform: none;
        margin-top: 0px!important;
    }
    .single_funding .labelFunding, .single_governance .labelGovernance {
        font-size: 16px;
    }

    /*GOUVERNANCE STYLE*/
    .single_governance .progress {
        width: 60px;
        height: 60px;
        line-height: 150px;
        background: none;
        margin: 0 auto;
        box-shadow: none;
        position: relative;
    }

    .single_governance .progress:after {
        content: "";
        width: 100%;
        height: 100%;
        border-radius: 50%;
        border: 7px solid #ffffff;
        position: absolute;
        top: 0;
        left: 0;
    }

    .single_governance .progress > span {
        width: 50%;
        height: 100%;
        overflow: hidden;
        position: absolute;
        top: 0;
        z-index: 1;
    }

    .single_governance .progress .progress-left {
        left: 0;
    }

    .single_governance .progress .progress-bar {
        width: 100%;
        height: 100%;
        background: none;
        border-width: 7px;
        border-style: solid;
        position: absolute;
        top: 0;
        border-color: #93C020;
    }

    .single_governance .progress .progress-left .progress-bar {
        left: 100%;
        border-top-right-radius: 75px;
        border-bottom-right-radius: 75px;
        border-left: 0;
        -webkit-transform-origin: center left;
        transform-origin: center left;
    }

    .single_governance .progress .progress-right {
        right: 0;
    }

    .single_governance .progress .progress-right .progress-bar {
        left: -100%;
        border-top-left-radius: 75px;
        border-bottom-left-radius: 75px;
        border-right: 0;
        -webkit-transform-origin: center right;
        transform-origin: center right;
    }

    .single_governance .progress .progress-value {
        display: flex;
        border-radius: 50%;
        font-size: 36px;
        text-align: center;
        line-height: 20px;
        align-items: center;
        justify-content: center;
        height: 100%;
        font-weight: 300;
    }

    .single_governance .progress .progress-value div {
        font-size: 15px;
        margin-top: 10px;
    }

    .single_governance .progress .progress-value span {
        font-size: 15px;
        text-transform: uppercase;
    }

    @media (max-width: 767px) {
        .evaluate_content h4 {
            font-size: 20px;
        }
        .single_evaluate img {
            -webkit-clip-path: polygon(0 0, 100% 0, 100% 100%, 0 100%, 0 50%);
            clip-path: polygon(0 0, 100% 0, 100% 100%, 0 100%, 0 50%);
            min-height: auto;
        }
        .evaluate_content p {
            font-weight: 200;
            font-size: 14px;
        }
        .evaluate_content .btn-evaluate, .single_task .btn-participate, .single_funding .btn-financing, .single_governance .btn-opinion {
            font-size: 16px;
            padding : 6px 12px;
        }
        .evaluate_content .btn-component, .single_task .btn-component, .single_funding .btn-component, .single_governance .btn-component {
            justify-content: center;
        }
    }




    /*Liste project*/

    .product_list {
        position: relative;
        margin-bottom: 40px;
        transition: .3s;
        box-shadow: 0 2px 5px 0 rgb(63 78 88), 0 2px 10px 0 rgb(63 78 88);
        border-radius: 3px;
    }

    .product_list img {
        width: 100%;
        /*position: absolute;*/
        min-height: 420px;
        transition: .3s;
        border: 1px solid #ddd;
        border-radius: 5px;
        object-fit: cover;
        object-position: center;
    }



    .product_content h4 {
        font-size: 22px;
        font-weight: 700;
        border-bottom: 1px dashed #3f4e58;
        line-height: 2;
        text-transform: none;
        color: #3f4e58;
    }

    .product_content h4 span {
        font-size: 20px;
        font-weight: 800;
        float: right;
        font-style: italic;
        color: #9ebc37;
    }

    .product_content {
        display: flex;
        flex-direction: column;
    }

    .product_content p {
        font-weight: 200;
        font-size: 16px;
        letter-spacing: 1px;

        overflow: hidden;
        text-overflow: ellipsis;
        display: -webkit-box;
        -webkit-line-clamp: 10;
        -webkit-box-orient: vertical;
    }

    .product_content .btn-component {
        flex-basis: 100%;
        display: flex;
        flex-direction: row;
        align-items: flex-end;
    }
    .product_content .btn-evaluate, .proposition_list .btn-evaluate {
        color: white;
        background-color: #93C020;
        border-color: #93C020;
        font-size: 18px;
        font-weight: 700;
        border-radius: 2px;
        padding : 6px 25px;
    }

    .product_list .progress, .proposition_list .progress {
        width: 75px;
        height: 75px;
        line-height: 150px;
        background: none;
        margin: 0 auto;
        box-shadow: none;
        position: relative;
    }

    .product_list .progress:after, .proposition_list .progress:after {
        content: "";
        width: 100%;
        height: 100%;
        border-radius: 50%;
        border: 10px solid #3f4e58;
        position: absolute;
        top: 0;
        left: 0;
    }

    .product_list .progress > span, .proposition_list .progress > span {
        width: 50%;
        height: 100%;
        overflow: hidden;
        position: absolute;
        top: 0;
        z-index: 1;
    }

    .product_list .progress .progress-left, .proposition_list .progress .progress-left {
        left: 0;
    }

    .product_list .progress .progress-bar, .proposition_list .progress .progress-bar {
        width: 100%;
        height: 100%;
        background: none;
        border-width: 10px;
        border-style: solid;
        position: absolute;
        top: 0;
        border-color: #93C020;
    }

    .product_list .progress .progress-left .progress-bar, .proposition_list .progress .progress-left .progress-bar {
        left: 100%;
        border-top-right-radius: 75px;
        border-bottom-right-radius: 75px;
        border-left: 0;
        -webkit-transform-origin: center left;
        transform-origin: center left;
    }

    .product_list .progress .progress-right, .proposition_list .progress .progress-right  {
        right: 0;
    }

    .product_list .progress .progress-right .progress-bar, .proposition_list .progress .progress-right .progress-bar {
        left: -100%;
        border-top-left-radius: 75px;
        border-bottom-left-radius: 75px;
        border-right: 0;
        -webkit-transform-origin: center right;
        transform-origin: center right;
    }

    .product_list .progress .progress-value, .proposition_list .progress .progress-value {
        display: flex;
        border-radius: 50%;
        font-size: 36px;
        text-align: center;
        line-height: 20px;
        align-items: center;
        justify-content: center;
        height: 100%;
        font-weight: 300;
    }

    .product_list .progress .progress-value div, .proposition_list .progress .progress-value div {
        font-size: 15px;
        margin-top: 10px;
    }

    .product_list .progress .progress-value span, .proposition_list .progress .progress-value span {
        font-size: 15px;
        text-transform: uppercase;
    }

    /*PROPOSITION LIST*/

    .proposition_list {
        position: relative;
        margin-bottom: 40px;
        transition: .3s;
        box-shadow: 0 2px 5px 0 rgb(63 78 88), 0 2px 10px 0 rgb(63 78 88);
        border-radius: 3px;
    }
    .proposition_content h4 {
        font-size: 22px;
        font-weight: 700;
        line-height: 2;
        text-transform: none;
        color: #3f4e58;
    }
    .proposition_content h4 span {
        font-size: 20px;
        font-weight: 800;
        float: right;
        font-style: italic;
        color: #9ebc37;
    }

    .proposition_content h4 span {
        font-size: 20px;
        font-weight: 800;
        float: right;
        font-style: italic;
        color: #9ebc37;
    }

    .proposition_content {
        display: flex;
        flex-direction: column;
    }
    .title-proposition{
        line-height: 4;
        text-align: center;
        color: #3f4e58;
    }
    .proposition_list .btn-component {
        margin-top: 25px;
        flex-basis: 100%;
        display: block;
        flex-direction: row;
        align-items: center;
        text-align: center;
        margin-bottom: 10px;
    }
    @media (max-width: 991px) {
        .proposition_content h4 span {
            width: 100%;
            text-align: center;
            display: block;
        }
        .proposition_content h4 {
            text-align: center;
        }
    }
    @media (max-width: 767px) {
        .proposition_content h4 span {
            font-size: 20px;
            font-weight: 800;
            float: none;
            font-style: italic;
            color: #9ebc37;

        }
        .proposition_list .btn-component {
            margin-top: 0px;
            flex-basis: 100%;
            display: block;
            flex-direction: row;
            align-items: center;
            text-align: center;
            margin-bottom: 10px;
        }

    }


</style>
<div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
    <ul class="breadcrumb">
        <li ><a><?php if (isset($el["el"]["name"])) { echo $el["el"]["name"]; } ?></a></li>
        <li ><a><?php if (isset($elform["name"])) { echo $elform["name"]; } ?></a></li>
        <li class="active"><a class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.page.details ">Liste des propositions</a></li>
    </ul>
</div>

<div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
    <div class="aapconfigdiv">
        <div class="text-center">
            <div class="dropdown" style="display:inline-flex;">
                <button id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" type="button" class="aap-breadrumbbtn dropdown-toggle" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.page.form"><i class="fa fa-plus-square-o"></i> Changer de vue <span class="caret"></span></button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <li><a type="button" class="minimalaap " >Detailé</a></li>
                    <li><a type="button" class=" detaaap" >Minimaliste</a></li>
                </ul>
            </div>
            <button type="button" class="aapgoto aap-breadrumbbtn" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.page.form"><i class="fa fa-plus-square-o"></i> Nouvelle proposition</button>
        </div>
    </div>
</div>

<div class="col-xs-12 col-lg-10 col-lg-offset-1">


    <!--LISTE DES PRODUIT-->
    <?php
    if (!empty($answers) && is_array($answers) ){
        foreach ($answers as $answerid => $answer){
            $user = Person::getSimpleUserById(@$answer["user"]);
            if (!empty($answer["answers"])){
                $haveTasks = false ;
                $percentageTasks = "--";
                $dataPer = 0;
                $iconTasks = "";
                $actions = [];

                if (isset($answer["project"]["id"])){
                    $actions = PHDB::find(Actions::COLLECTION, [
                        "parentId" => new MongoId($answer["project"]["id"]),
                        "parentType" => Project::COLLECTION,
                        // "name" => $a["poste"]
                    ] );

                    $actions = array_reduce($actions, function ($carry, $item){
                        if (!empty($item["tasks"])){
                            $carry = array_merge($carry, $item["tasks"]);
                        }
                        return $carry;
                    }, []);

                    $percent = count(array_filter($actions, function($ele){return !empty($ele["checked"]) && ($ele["checked"] == "true") ;})) / count($actions) *100;
                    $percentageTasks = round($percent)."%";
                    $dataPer = round($percent, -1);
                    if ($percent == 100){
                        $iconTasks = "fa-check";
                    }else{
                         $iconTasks = "fa-times";
                    }
                }

                //get admin and evaluator rating
                $evaluation = @$answer["answers"]["aapStep3"]["evaluation"];
                $criterions = $el_configform["subForms"]["aapStep3"]["params"]["config"]["criterions"];
                foreach ($community as $kcom => $vcom) {
                    if(!isset($evaluation[$kcom])){
                        $evaluation[$kcom] = $criterions;
                    }
                    foreach ($criterions as $kcrit => $vcrit) {
                        if( !isset($evaluation[$kcom][$kcrit]))
                            $evaluation[$kcom][$kcrit] = $vcrit;
                    }
                    ksort($evaluation[$kcom]);
                }
                $cummulMean = 0;
                foreach ($evaluation as $key => $value) {
                    $notes= 0;
                    ksort($value);
                    foreach ($value as $kv => $vv) {
                        $notes = $notes + ((float)$vv["note"] * (float)$vv["coeff"]);
                    }
                    $cummulMean = $cummulMean + ($notes/$sumCoeff);
                }
                $cummulMean =  $cummulMean/ count($evaluation);

                //get public rating
                $publicRating = 0;
                if(isset($answer["publicRating"])){
                    foreach ($answer["publicRating"] as $idPublic => $valuePublic) {
                        $publicRating = $publicRating + (float)$valuePublic;
                    }
                    $publicRating = $publicRating/count($answer["publicRating"]);
                }
                


    ?>
                <div class="proposition_list">
                    <div class="row no-margin">
                        <div class="col-xs-12 col-sm-2 col-md-2 padding-15">
                            <div class="prop-compact-progression">
                                <div class="progress" data-percentage="<?php echo $dataPer ?>">
                        <span class="progress-left">
                            <span class="progress-bar"></span>
                        </span>
                                    <span class="progress-right">
                            <span class="progress-bar"></span>
                        </span>
                                    <div class="progress-value">
                                        <div>
                                            <?php echo $percentageTasks ?><br>
                                            <span><i class="fa <?php echo $iconTasks ?>"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-7 col-md-6 padding-15">
                            <div class="proposition_content">
                                <h4> 
                                    <a href="javascript:;" data-url = '<?php echo Yii::app()->getRequest()->getBaseUrl(true).Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.page.sheet.answer.<?php echo (string)$answer["_id"] ?>' class="aap_plusdinfo">
                                        <?php echo !empty(@$answer["answers"]["aapStep1"]["titre"]) ? @$answer["answers"]["aapStep1"]["titre"] : @$answer["answers"]["aapStep1"]["proposition"] ?>
                                    </a>
                                </h4>
                                <small>Déposé par <b><i><?= @$user["name"] ?></i></b></small> 
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-3 col-md-<?php echo ((!empty($answer["project"]["id"])) ? "2" : "4") ?>">
                            <div class="col-xs-12 padding-top-15">
                                <div class="col-xs-12 padding-bottom-5">
                                    <span>
                                        <b>Admin et evaluateur </b>
                                    </span>
                                    <?php
                                        if($el_configform["subForms"]["aapStep3"]["params"]["config"]["type"]=="noteCriterionBased"){ ?>
                                            <div class="rater-admin pull-right" data-rating=<?= $cummulMean ?> ></div>
                                    <?php       
                                        }elseif($el_configform["subForms"]["aapStep3"]["params"]["config"]["type"]=="starCriterionBased"){ ?>
                                            <div class="rater-admin pull-right" data-rating=<?= $cummulMean/2 ?> ></div><br>
                                    <?php } ?>
                                </div>
                                <div class="col-xs-12 padding-top-5" style="border-top:1px solid black">
                                    <span>
                                        <b>Public  </b>
                                    </span>
                                    <div class="rater-public pull-right" data-rating=<?= $publicRating ?>  data-ans="<?= (string)$answer["_id"] ?>"></div>
                                </div>
                            </div>
                            <div class="btn-component col-xs-12">
                                <button href="javascript:void(0);" data-url = '<?php echo Yii::app()->getRequest()->getBaseUrl(true).Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.page.sheet.answer.<?php echo (string)$answer["_id"] ?>' class="btn btn-default btn-evaluate btnaapp_plusdinfo">
                                    PLUS D'INFO
                                </button>

                            </div>
                        </div>

                        <?php if (!empty($answer["project"]["id"])) {
                                    $el = Element::getElementById($answer["project"]["id"],Project::COLLECTION,null );
//                                    var_dump($el);
                                    ?>

                        <div class="col-xs-12 col-sm-3 col-md-2">
                            <div class="btn-component">


                                <?php
                                    if (empty($el["costum"])){
                                        ?>
                                        <a href="javascript:void(0);" data-url = '<?php Yii::app()->getRequest()->getBaseUrl(true) ?>#page.type.<?php echo Project::COLLECTION.".id.".$answer["project"]["id"] ?>/' class="btn btn-default btn-evaluate btnaapp_plusdinfo">
                                            Page Projet
                                        </a>
                                    <?php } else {?>
                                        <a href="javascript:void(0);" data-url = '<?php echo Yii::app()->getRequest()->getBaseUrl(true).Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$el["slug"]; ?>' class="btn btn-default btn-evaluate btnaapp_plusdinfo">
                                            Costum
                                            <span class="pull-right tooltips ">
												<img src="<?php echo Yii::app()->getModule( "co2" )->assetsUrl."/images/logo_costum.png"; ?>" style="width: 25px;margin-top: -4px">
					                             </span>
                                        </a>
                                    <?php } ?>
                            </div>
                        </div>

                        <?php } ?>


                    </div>
                </div>


                <div class="product_list">
                    <div class="row no-margin">
                        <div class="col-xs-12 col-sm-5 col-md-5 padding-15">
                            <?php
                            $initAnswerFiles=Document::getListDocumentsWhere(array(
                    "id"=>(string)$answer["_id"],
                    "type"=>'answers',
                    "subKey"=>"aapStep1.image"), "image");


                    //echo $this->renderPartial("co2.views.pod.docsList",array("edit"=>false, "documents"=>$initAnswerFiles,"docType"=>"image") );
                    if(!empty($initAnswerFiles)){
                        foreach ($initAnswerFiles as $key => $d) {
                            
                            ?>

                            <img class="img-responsive" src="<?php echo $d["imageThumbPath"] ?>">

                            <?php
                             }
                    } else {
                      ?>
                                <img class="img-responsive" src="<?php echo $this->module->assetsUrl ?>/images/thumbnail-default.jpg">
                      <?php

                    } ?>
<!--                            <img class="img-responsive" src="http://www.place-to-be.fr/wp-content/uploads/2014/11/meilleur-whisky-du-monde-japonais-1050x656.jpg" alt="burger">-->
                        </div>
                        <div class="col-xs-12 col-sm-7 col-md-7 padding-15">
                            <div class="product_content">
                                <h4> <?php echo @$answer["answers"]["aapStep1"]["titre"] ?>
                                    <span>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        </span>
                                </h4>

                                <p>
                                    <?php echo @$answer["answers"]["aapStep1"]["opalProcess41"] ?>
                                </p>

                                <span class="status">
                                    <?php
                                    if (!empty($answer["status"])){
                                            foreach ($answer["status"] as $statusid => $state){
                                    ?>

                                                <span> <?php echo $tab[$statusid]; ?> </span>

                                    <?php
                                            }
                                    }
                                    ?>
                                </span>
                                <div class="btn-component">
                                    <div class="col-xs-6 no-padding">
                                        <a href="javascript:void(0);" data-url = '<?php echo Yii::app()->getRequest()->getBaseUrl(true).Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.page.sheet.answer.<?php echo (string)$answer["_id"] ?>' class="btn btn-default btn-evaluate btnaapp_plusdinfo">
                                            PLUS D'INFO
                                        </a>


                                    </div>

                                    <?php if (!empty($answer["project"]["id"])) {
                                        $el = Element::getElementById($answer["project"]["id"],Project::COLLECTION,null );
//                                        var_dump($el);
                                        ?>

                                    <div class="col-xs-6 no-padding">


                                        <?php
                                        if (empty($el["costum"])){
                                            ?>
                                            <a href="javascript:void(0);" data-url = '<?php Yii::app()->getRequest()->getBaseUrl(true) ?>#page.type.<?php echo Project::COLLECTION.".id.".$answer["project"]["id"] ?>/' class="btn btn-default btn-evaluate btnaapp_plusdinfo">
                                                Page Projet
                                            </a>
                                        <?php } else {?>
                                            <a href="javascript:void(0);" data-url = '<?php echo Yii::app()->getRequest()->getBaseUrl(true).Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$el["slug"]; ?>' class="btn btn-default btn-evaluate btnaapp_plusdinfo">
                                                Costum
                                                <span class="pull-right tooltips ">
												<img src="<?php echo Yii::app()->getModule( "co2" )->assetsUrl."/images/logo_costum.png"; ?>" style="min-height: 25px;width: 25px;margin-top: -4px">
					                             </span>
                                            </a>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                    <div class="col-xs-6 no-padding">
                                        <div class="prop-compact-progression">
                                            <div class="progress" data-percentage="<?php echo $dataPer ?>">
                                                <span class="progress-left">
                                                    <span class="progress-bar"></span>
                                                </span>
                                                <span class="progress-right">
                                                    <span class="progress-bar"></span>
                                                </span>
                                                <div class="progress-value">
                                                    <div>
                                                        <?php echo $percentageTasks ?><br>
                                                        <span><i class="fa <?php echo $iconTasks ?>"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

    <?php
            }
        }
    }
    ?>
</div>

<script type="text/javascript">
    $(document).ready(function() {

        $('.product_list').hide();

        $('.btnaapp_plusdinfo,.aap_plusdinfo').off().on("click", function () {
            window.location.href = $(this).data("url");
            urlCtrl.loadByHash(location.hash);
        });

        $('.minimalaap').off().on("click", function () {
            $('.proposition_list').hide();
            $('.product_list').show();
        });

        $('.detaaap').off().on("click", function () {
            $('.product_list').hide();
            $('.proposition_list').show();
        });
    });


    //sectionDyf.<?//= $kunik ?>//ParamsData = <?php //echo json_encode($paramsData); ?>//;
    //jQuery(document).ready(function () {
    //    sectionDyf.<?//= $kunik ?>//Params = {
    //        "jsonSchema": {
    //            "title": "Configurer votre section",
    //            "description": "Personnaliser votre section",
    //            "icon": "fa-cog",
    //            "properties": {
    //                "buttonLabel": {
    //                    "inputType": "text",
    //                    "label": "Label du button",
    //                },
    //            },
    //            beforeBuild: function () {
    //                uploadObj.set("cms", "<?php //echo $blockKey ?>//");
    //            },
    //            save: function (data) {
    //                tplCtx.value = {};
    //                $.each(sectionDyf.<?//= $kunik ?>//Params.jsonSchema.properties, function (k, val) {
    //                    tplCtx.value[k] = $("#" + k).val();
    //                    if (k == "parent")
    //                        tplCtx.value[k] = formData.parent;
    //
    //                    if (k == "description")
    //                        tplCtx.value[k] = data.description;
    //                });
    //                console.log("save tplCtx", tplCtx);
    //
    //                if (typeof tplCtx.value == "undefined")
    //                    toastr.error('value cannot be empty!');
    //                else {
    //                    dataHelper.path2Value(tplCtx, function (params) {
    //                        dyFObj.commonAfterSave(params, function () {
    //                            toastr.success("Élément bien ajouter");
    //                            $("#ajax-modal").modal('hide');
    //                            dyFObj.closeForm();
    //                            urlCtrl.loadByHash(location.hash);
    //                        });
    //                    });
    //                }
    //
    //            }
    //        }
    //    };
    //    sectionDyf.<?//= $kunik ?>//Params.jsonSchema.properties = Object.assign(sectionDyf.<?//= $kunik ?>//Params.jsonSchema.properties, properties<?//= $kunik ?>//)
    //    $(".edit<?//= $kunik ?>//Params").off().on("click", function () {
    //        tplCtx.id = $(this).data("id");
    //        tplCtx.collection = $(this).data("collection");
    //        tplCtx.path = "allToRoot";
    //        dyFObj.openForm(sectionDyf.<?//= $kunik ?>//Params, null, sectionDyf.<?//= $kunik ?>//ParamsData);
    //    });
    //});
</script>