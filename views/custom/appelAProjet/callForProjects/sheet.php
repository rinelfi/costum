<style>
    .moreLinkaap.btn-xs.btn.btn-default{
        width: 80px;
    }
</style>
<?php
function random_color_part() {
    return str_pad( dechex( mt_rand( 0, 255 ) ), 2, '0', STR_PAD_LEFT);
}

function random_color() {
    return random_color_part() . random_color_part() . random_color_part();
}

$starbg = Yii::app()->getModule('costum')->assetsUrl . "/images/blockCmsImg/green-star.png";
HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/swiper/swiper-bundle.min.css", "/js/blockcms/swiper/swiper-bundle.min.js"], Yii::app()
    ->getModule('costum')
    ->assetsUrl);
$stepEval = "aapStep2";
$stepFinancor = "aapStep3";
if (isset($el_configform["subForms"]["aapStep2"]["params"]["config"]["criterions"]))
{
    $stepEval = "aapStep2";
    $stepFinancor = "aapStep3";
}
elseif (isset($el_configform["subForms"]["aapStep3"]["params"]["config"]["criterions"]))
{
    $stepEval = "aapStep3";
    $stepFinancor = "aapStep4";
}

if(isset($elform["evaluationCriteria"]["activateLocalCriteria"]) && ($elform["evaluationCriteria"]["activateLocalCriteria"] == true)){
    if(isset($elform["evaluationCriteria"]["type"]))
        $el_configform["subForms"][$stepEval]["params"]["config"]["type"] = $elform["evaluationCriteria"]["type"];
    if(isset($elform["evaluationCriteria"]["criterions"]))
        $el_configform["subForms"][$stepEval]["params"]["config"]["criterions"] = $elform["evaluationCriteria"]["criterions"];
}

$sumCoeff = 0;
if (isset($el_configform["subForms"][$stepEval]["params"]["config"]["criterions"]))
{
    foreach ($el_configform["subForms"][$stepEval]["params"]["config"]["criterions"] as $key => $value)
    {
        $sumCoeff = $sumCoeff + (float)$value["coeff"];
    }
}
$idEl = array_keys($elform["parent"]) [0];
$typeEl = $elform["parent"][$idEl]["type"];
$allowedRoles = isset($el_configform["subForms"][$stepEval]["params"]["canEdit"]) ? $el_configform["subForms"][$stepEval]["params"]["canEdit"] : array();
$me = isset(Yii::app()->session['userId']) ? Person::getById(Yii::app()
    ->session['userId']) : null;
$roles = isset($me["links"]["memberOf"][$idEl]["roles"]) ? $me["links"]["memberOf"][$idEl]["roles"] : null;
$isAdmin = isset($me["links"]["memberOf"][$idEl]["isAdmin"]) ? $me["links"]["memberOf"][$idEl]["isAdmin"] : false;
$canEvaluate = false;
$intersectRoles = array_intersect($allowedRoles, $roles);
if (count($intersectRoles) != 0 || $isAdmin) $canEvaluate = true;
$communityWithAdmin = Element::getCommunityByTypeAndId($typeEl, $idEl, "all", "isAdmin", null, null, ["name", "profilMediumImageUrl"]);
$community = Element::getCommunityByTypeAndId($typeEl, $idEl, "all", null, $allowedRoles, null, ["name", "profilMediumImageUrl"]);
$community = array_unique(array_merge($communityWithAdmin, $community) , SORT_REGULAR);
$allAnswers = PHDB::find(Form::ANSWER_COLLECTION,array("form"=>(string)$elform["_id"],"answers"=>['$exists'=>true]));

foreach ($community as $kcommunity => $vcommunity) {
    $haveMadeAvote = false;
    foreach ($allAnswers as $kallans => $vallans) {
        if(isset($vallans["answers"][$stepEval]["evaluation"][$kcommunity]))
            $haveMadeAvote = true;
    }
    if($haveMadeAvote == false)
        unset($community[$kcommunity]);
}

$creator = Person::getById($elform["creator"]);

$paramsData = ["financerTypeList" => Ctenat::$financerTypeList, "limitRoles" => ["Financeur"], "openFinancing" => true];

if (!empty($idEl))
{
    $communityLinks = Element::getCommunityByParentTypeAndId($elform["parent"]);
}
else
{
    $communityLinks = [];
}
$organizations = Link::groupFindByType(Organization::COLLECTION, $communityLinks, ["name", "links"]);

$orgs = [];
foreach ($organizations as $id => $or)
{
    $roles = null;

    if (isset($communityLinks[$id]["roles"])) $roles = $communityLinks[$id]["roles"];

    if ($paramsData["limitRoles"] && !empty($roles))
    {
        foreach ($roles as $i => $r)
        {
            if (in_array($r, $paramsData["limitRoles"])) $orgs[$id] = $or["name"];
        }
    }
}

echo $this->renderPartial("survey.views.tpls.forms.ocecoform.ocecoformcss", []);

if (!empty($answerid) && !empty($answers[$answerid]))
{
    $answer = $answers[$answerid];
    $user = Person::getSimpleUserById(@$answer["user"]);

    //get admin and evaluator rating
    $evaluation = @$answer["answers"][$stepEval]["evaluation"];
    $votantCounter = isset($answer["answers"][$stepEval]["evaluation"]) ? count($answer["answers"][$stepEval]["evaluation"]) : 0;
    $votantModal = "";

    $criterions = @$el_configform["subForms"][$stepEval]["params"]["config"]["criterions"];
    foreach ($community as $kcom => $vcom)
    {
        if (!isset($evaluation[$kcom]))
        {
            $evaluation[$kcom] = $criterions;
        }
        foreach ($criterions as $kcrit => $vcrit)
        {
            if (!isset($evaluation[$kcom][$kcrit])) $evaluation[$kcom][$kcrit] = $vcrit;
        }
        ksort($evaluation[$kcom]);
    }
    $cummulMean = 0;
    foreach ($evaluation as $key => $value)
    {
        $notes = 0;
        ksort($value);
        foreach ($value as $kv => $vv)
        {
            $notes = $notes + ((float)$vv["note"] * (float)$el_configform["subForms"][$stepEval]["params"]["config"]["criterions"][$kv]["coeff"]);
        }
        $cummulMean = $cummulMean + ($notes / $sumCoeff);
    }
    $cummulMean = $cummulMean / count($evaluation);

    //get public rating
    $publicRating = 0;
    $myRate = "false";
    $countLike = 0;
    if (isset($answer["publicRating"]))
    {
        foreach ($answer["publicRating"] as $idPublic => $valuePublic)
        {
            if ($idPublic == (string)@$me["_id"]) $myRate = (string)$valuePublic;
            if ($myRate != "false" && $myRate != "true") $myRate = "false";
            $publicRating = $publicRating + (int)$valuePublic;
            if ($valuePublic == "true") $countLike++;
        }
        //$publicRating = $publicRating/count($answer["publicRating"]);

    }
    $publicVoteConfig = "all";
    if (isset($el_configform["subForms"][$stepEval]["params"]["config"]["publicVote"])) $publicVoteConfig = $el_configform["subForms"][$stepEval]["params"]["config"]["publicVote"];
    $urgentState = isset($answer['answers']['aapStep1']["multiCheckboxPlusurgency"]) ? "Urgent" : "";

    $htmlContributor = "";
    if (!empty($answer["answers"]) && isset($answer["project"]["id"])){
        $project = PHDB::findOneById(Project::COLLECTION,$answer["project"]["id"]);
        $allLink = Element::getAllLinks($project["links"],Project::COLLECTION,(string)$project["_id"]);
        //var_dump($allLink);
        $htmlContributor = "<div class='col-xs-12'>";
        $htmlContributor .= "<h6>Contributeur(s)</h6>";
        foreach ($allLink as $kcontributor => $vcontributor) {
            if(isset($vcontributor["username"])){
                $htmlContributor .= "<div style='position:relative;display:inline;width:50px;height:50px'>";
                $htmlContributor .= '<a href="#page.type.citoyens.id.'.$kcontributor.'" class="lbh-preview-element tooltips" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="'.$vcontributor['name'].'">';
                $htmlContributor .= '<img class="img-contributor" style="border:4px solid #'.random_color().'" src="'.@$vcontributor["profilThumbImageUrl"].'" alt="'.$vcontributor['name'].'">';
                $htmlContributor .= '</a>';
                $htmlContributor .= "</div>";
            }
        }
        $htmlContributor .= "</div>";
        
    }
    ?>
    <style>
        .star-rating .star-value{
            background: url("<?= $starbg ?>") !important;
            background-size: 20px !important;
            background-repeat-y: no-repeat !important;
        }
    </style>

    <div id="sticky-anchor"></div>
    <div id="sticky">
        <div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
            <ul class="breadcrumb">
                <li ><a><?php if (isset($el["el"]["name"])) { echo $el["el"]["name"]; } ?></a></li>
                <li class="active"><a href="javascript:;" class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.list" ><?php if (isset($elform["name"])) { echo $elform["name"]; } ?> </a></li>
                <!-- <li ><a><?php if (isset($elform["name"])) { echo $elform["name"]; } ?></a></li> -->
                <!-- <li class="active"><a class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.details ">Fiche du proposition : <?php echo @$answer["answers"]["aapStep1"]["titre"] ?></a></li> -->
                <li class="active"><a href="javascript:;" class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.sheet.answer.<?php echo (string)$answer["_id"] ?>" >Fiche du proposition : <?php echo @$answer["answers"]["aapStep1"]["titre"] ?> </a></li>
            </ul>
        </div>

        <div class="col-md-offset-1 col-md-10  col-sm-12 col-xs-12 ">
            <div class="aapconfigdiv " >
                <div class="text-center">
                    <?php //if (!empty(Yii::app()->session["userId"]) && ($answer["user"] == Yii::app()->session["userId"] || Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) || Form::canAdmin((string)$elform["_id"]) ))
                    //{ ?>
                        <button type="button" class="aapgoto aap-breadrumbbtn" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.form.answer.<?php echo (string)$answer["_id"]; ?>"><i class="fa fa-plus-square-o"></i> Editer proposition</button>
                        <?php
                    //} ?>
                    <button type="button" class="aapgoto aap-breadrumbbtn" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.formdashboard.answer.<?php echo (string)$answer["_id"]; ?>"><i class="fa fa-plus-square-o"></i> Observatoire </button>
                    <!-- <button type="button" class="aapgoto aap-breadrumbbtn" data-url="<?php echo Yii::app()->createUrl("/costum") ?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.page.timeline.answer.<?php echo (string)$answer["_id"] ?>"><i class="fa fa-plus-square-o"></i> Timeline </button> -->
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-lg-10 col-lg-offset-1 project-detail">
        <div class="single_evaluate">
            <div class="row no-margin">
                <div class="col-xs-12 col-sm-7 col-md-7 padding-15">
                    <div class="evaluate_content">
                        <div class="row">
                            <div class="col-xs-7">
                                <h4> <?php echo @$answer["answers"]["aapStep1"]["titre"] ?> <br>
                                    <small>Déposé par <b><i><?=@$user["name"] ?></i></b></small>
                                </h4>
                                <span class='margin-bottom-10 label <?=$urgentState == "Urgent" ? " urgent-state" : "hidden" ?> '><i class="fa fa-exclamation-triangle"></i>&nbsp;<?=$urgentState ?></span>
                            </div>
                            <div class="col-xs-5">
                                <div class="col-xs-12">
                                    <?php
                                    if (@$el_configform["subForms"][$stepEval]["params"]["config"]["type"] == "noteCriterionBased")
                                    { ?>
                                        <div class="rater-sheet pull-right tooltips" data-rating="<?= is_numeric($cummulMean / 2) ? $cummulMean/2 : 0 ?>" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="<?=htmlspecialchars("<small>Admins et evaluateurs</small> </br>", ENT_QUOTES, 'UTF-8') . round($cummulMean / 2, 3) ?>"></div>
                                        <?php
                                    }
                                    elseif (@$el_configform["subForms"][$stepEval]["params"]["config"]["type"] == "starCriterionBased")
                                    { ?>
                                        <div class="rater-sheet pull-right tooltips" data-rating="<?= is_numeric($cummulMean) ? $cummulMean : 0 ?>" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="<?=htmlspecialchars("<small>Admins et evaluateurs</small> </br>", ENT_QUOTES, 'UTF-8') . round($cummulMean, 3) ?>"></div>
                                        <?php
                                    } ?>
                                </div>
                                <div class="col-xs-12">
                                    <!-- public rating -->
                                    <?php if (isset(Yii::app()->session['userId']) && $publicVoteConfig == "members" && Authorisation::isElementMember($el["id"], @$el["type"], Yii::app()->session['userId']))
                                    { ?>
                                        <a href="javascript:;" class="like-project letter-red pull-right tooltips" data-value="<?=$myRate
                                        ?>" data-ans="<?=(string)$answer["_id"] ?>" style="text-decoration: none;" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="J'adore">
                                            <i class="fa <?=$myRate == "true" ? "fa-heart" : "fa-heart-o" ?>"></i>
                                            <small class="counter"><?=$countLike ?></small>
                                        </a>
                                        <?php
                                    } ?>
                                    <?php if ($publicVoteConfig == "all")
                                    { ?>
                                        <a href="javascript:;" class="like-project letter-red pull-right tooltips margin-right-10"  data-value="<?=$myRate
                                        ?>" data-ans="<?=(string)$answer["_id"] ?>" style="text-decoration: none;" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="J'adore">
                                            <i class="fa <?=$myRate == "true" ? "fa-heart" : "fa-heart-o" ?>"></i>
                                            <small class="counter"><?=$countLike ?></small>
                                        </a>
                                        <?php
                                    } ?>

                                    <a class="like-project pull-right tooltips margin-right-10" data-toggle="tooltip" data-html="true" data-placement="bottom" data-original-title="Nombre de visite">
                                        <i class="fa fa-eye"></i> <span id ="countv" > <?php echo (isset($answer["visitor_counter"]["count"]) ? $answer["visitor_counter"]["count"] : __ ) ?> </span>
                                    </a>
                                    <?= $votantModal ?>
                                    <a href="javascript:;" class="pull-right tooltips margin-right-10" data-toggle="modal" data-target="#modal<?= $votantCounter != 0 ?(string)$answer["_id"]:null ?>" >
                                        <i class="fa fa-gavel"></i> <span id ="countv"  style="font-size: 12px"> <?= $votantCounter ?> </span>
                                    </a>

                                </div>
                            </div>
                        </div>
                        <?= $htmlContributor ?>
                        <div class="project-description sheet-markdown"><?php echo !empty(@$answer["answers"]["aapStep1"]["opalProcess41"]) ? @$answer["answers"]["aapStep1"]["opalProcess41"] : @$answer["answers"]["aapStep1"]["description"] ?></div>
                        <div class="btn-component">
                            <!--<button class="btn btn-default btn-evaluate">
                                Evaluer
                            </button>-->
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-5 col-md-5 padding-0 sheet-swipper" style="overflow: hidden;">
                    <?php
                    $initAnswerFiles = Document::getListDocumentsWhere(array(
                        "id" => (string)$answer["_id"],
                        "type" => 'answers',
                        "subKey" => "aapStep1.image"
                    ) , "image");

                    if (!empty($initAnswerFiles))
                    { ?>
                        <div class="swiper mySwiper">
                            <div class="swiper-wrapper">
                                <?php foreach ($initAnswerFiles as $key => $d)
                                { ?>
                                    <div class="swiper-slide">
                                        <img class="img-responsive" src="<?php echo $d["imageThumbPath"] ?>">
                                    </div>
                                    <?php
                                } ?>
                            </div>
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-pagination"></div>
                        </div>
                        <?php
                    }
                    else
                    { ?>
                        <img class="img-responsive" src="<?php echo Yii::app()->getModule("co2")->assetsUrl ?>/images/thumbnail-default.jpg">
                        <?php
                    } ?>
                </div>

            </div>
        </div>


        <!--TACHE -->
        <div class="single_task">
            <div class="task_header padding-15">
                <h4>Tâche(s)
                    <span>
                    <i class="fa fa-list-ul"></i>
                </span>
                </h4>
            </div>
            <div class="task_body">
                <div class="table-responsive">
                    <table class="table align-items-center table-flush">
                        <tbody>
                        <?php
                        if (!empty($answer["answers"]))
                        {
                            if (isset($answer["project"]["id"]))
                            {   
                                $actions = PHDB::find(Actions::COLLECTION, ["parentId" => new MongoId($answer["project"]["id"]) , "parentType" => Project::COLLECTION,
                                    // "name" => $a["poste"]
                                ]);

                                $actions = array_reduce($actions, function ($carry, $item)
                                {
                                    if (!empty($item["tasks"]))
                                    {
                                        $carry = array_merge($carry, $item["tasks"]);
                                    }
                                    return $carry;
                                }
                                    , []);
                                $i = 1;
                                foreach ($actions as $actionid => $action)
                                {
                                    ?>

                                    <tr class="<?php echo $i % 2 === 0 ? "" : "line-dark"; ?>">
                                        <th scope="row">
                                            <div class="media align-items-center">
                                                <div class="input_wrapper">
                                                    <!--<input type="checkbox" class="switch_4" <?php /*echo ((isset($action["checked"]) && $action["checked"] == "true") ? "checked":"") */ ?> disabled>
                                            <svg class="is_checked" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 426.67 426.67">
                                                <path d="M153.504 366.84c-8.657 0-17.323-3.303-23.927-9.912L9.914 237.265c-13.218-13.218-13.218-34.645 0-47.863 13.218-13.218 34.645-13.218 47.863 0l95.727 95.727 215.39-215.387c13.218-13.214 34.65-13.218 47.86 0 13.22 13.218 13.22 34.65 0 47.863L177.435 356.928c-6.61 6.605-15.27 9.91-23.932 9.91z"/>
                                            </svg>
                                            <svg class="is_unchecked" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 212.982 212.982">
                                                <path d="M131.804 106.49l75.936-75.935c6.99-6.99 6.99-18.323 0-25.312-6.99-6.99-18.322-6.99-25.312 0L106.49 81.18 30.555 5.242c-6.99-6.99-18.322-6.99-25.312 0-6.99 6.99-6.99 18.323 0 25.312L81.18 106.49 5.24 182.427c-6.99 6.99-6.99 18.323 0 25.312 6.99 6.99 18.322 6.99 25.312 0L106.49 131.8l75.938 75.937c6.99 6.99 18.322 6.99 25.312 0 6.99-6.99 6.99-18.323 0-25.313l-75.936-75.936z" fill-rule="evenodd" clip-rule="evenodd"/>
                                            </svg>-->
                                                    <input id="checkbox<?php echo $i ?>" type="checkbox" <?php echo ((isset($action["checked"]) && $action["checked"] == "true") ? "checked" : "") ?> disabled>
                                                    <label for="checkbox<?php echo $i ?>">
                                                    </label>
                                                </div>
                                                <div class="media-body">
                                                    <span class="mb-0 text-simple"><?php echo @$action["task"] ?> </span>
                                                </div>
                                            </div>

                                        </th>
                                        <td>
                                            <span class="text-simple price"><?php echo @$action["credits"] ?> €</span>
                                        </td>

                                        <td>
                                            <div class="media">
                                                <span class="text-simple italic">Réaliser par : &nbsp;</span>
                                                <?php
                                                if (isset($action["contributors"]))
                                                {
                                                    foreach ($action["contributors"] as $ckey => $cvalue)
                                                    {
                                                        $wrkr = PHDB::findOneById(Person::COLLECTION, $ckey);
                                                        if (isset($wrkr["profilImageUrl"]))
                                                        {
                                                            $pdpp = Yii::app()->createUrl('/' . $wrkr["profilImageUrl"]);
                                                        }
                                                        else
                                                        {
                                                            $pdpp = Yii::app()->getModule("co2")->assetsUrl . '/images/thumb/default_organizations.png';
                                                        }

                                                        ?>

                                                        <div class="avatar-group">
                                                            <a href="#" class="avatar avatar-sm nicoss" data-toggle="tooltip" data-original-title="<?php echo $wrkr["username"] ?>">
                                                                <img alt="Image placeholder" src="<?php echo $pdpp ?>" class="rounded-circle">
                                                            </a>
                                                        </div>
                                                        <span class="text-simple bold"> &nbsp; <?php echo $wrkr["username"] ?></span>
                                                        <?php $i++;
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </td>
                                    </tr>

                                    <?php
                                }
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                    <?php
                    if (!isset($answer["project"]["id"]) || (isset($answer["project"]["id"]) && count(@$action) == 0))
                    {
                        echo "<div class=\"col-xs-12 text-center margin-bottom-10\"> il n'y a pas encore de tache sur cette proposition</div>";
                        echo "<style> .single_task{display:none} </style>";
                    }
                    ?>
                </div>
            </div>
        </div>


        <!--FINANCEMENT -->
        <div class="single_funding <?=empty($answer["answers"]["aapStep1"]["depense"]) ? "hidden" : "" ?>">
            <div class="funding_header padding-15">
                <h4>Financement
                    <span>
                    <i class="fa fa-euro"></i>
                </span>
                </h4>
            </div>
            <div class="funding_body">
                <div class="panel-body no-padding">
                    <?php
                    if (!empty($answer["answers"]["aapStep1"]["depense"]))
                    {

                        /*$financements = array_reduce($answer["answers"]["aapStep1"]["depense"], function ($carry, $item){
                                            if (!empty($item["financer"])){
                                                $carry = array_merge($carry, $item["financer"]);
                                            }
                                            return $carry;
                                        }, []);*/
                        $icont = 1;
                        foreach ($answer["answers"]["aapStep1"]["depense"] as $depId => $dep)
                        {
                            ?>

                            <div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding  <?php echo $icont % 2 === 0 ? "line-light" : "line-dark"; ?>">
                                <div class="col-md-5 col-sm-6 hidden-xs labelFunding padding-10">
                                   <?php echo @$dep["poste"]; ?>
                                </div>
                                <div class="col-md-7 col-sm-6 col-xs-12 valueAbout padding-10 <?php echo $icont % 2 === 0 ? "line-dark" : "line-light"; ?>">
                                    <div class="visible-xs col-xs-12 no-padding"><?php echo @$dep["poste"] ?></div>
                                    <div class="col-xs-12 col-sm-12 col-md-5 text-center">
                                        <div class="media justify-center">
                                            <?php
                                            /* if (isset($fi["user"])) {
                                                                        $wrkr = PHDB::findOneById(Person::COLLECTION, $ckey);
                                                                        if (isset($wrkr["profilImageUrl"]))
                                                                        {
                                                                            $pdpp = Yii::app()->createUrl('/'.$wrkr["profilImageUrl"]);
                                                                        } else {
                                                                            $pdpp = Yii::app()->getModule("co2")->assetsUrl.'/images/thumb/default_organizations.png';
                                                                        }*/

                                            ?>

                                            <!--<div class="avatar-group">
                                                <a href="#" class="avatar avatar-sm" data-toggle="tooltip" data-original-title="Ryan Tompson">
            <!--                                        <img  src="" class="rounded-circle">
                                                </a>
                                            </div>-->
                                            <span class="text-simple bold">
                                    <?php
                                    $totalname = "";
                                    if (!empty($dep["financer"]))
                                    {
                                        foreach ($dep["financer"] as $fiid => $fi)
                                        {
                                            if (isset($fi["name"]))
                                            {
                                                $totalname .= " " . $fi["name"] . " -";
                                            }
                                        }
                                    }
                                    echo $totalname;
                                    ?>
                                </span>
                                        </div>
                                    </div>

                                    <div class="col-xs-12 col-sm-12 col-md-7 no-padding">
                                        <div class="widget widget-black shadow">

                                            <div>
                                                <span class="pull-right text-dark">
                                                    <h3 class="widget-title"><?php if (!empty($dep["price"]) && is_numeric($dep["price"]))
                                                        {
                                                            echo rtrim(rtrim(number_format($dep["price"], 3, ".", " ") , '0') , '.');
                                                        } ?> <i class="fa fa-euro"></i></h3>
                                                </span>
                                                <?php
                                                if (isset($dep["price"]) && is_numeric($dep["price"]))
                                                {
                                                    $totalfi = 0;
                                                    $totalde = 0;
                                                    if (!empty($dep["financer"]))
                                                    {
                                                        foreach ($dep["financer"] as $fiid => $fi)
                                                        {
                                                            if (isset($fi["amount"]))
                                                            {
                                                                $totalfi += (int)$fi["amount"];
                                                            }
                                                        }

                                                    }

                                                    if (!empty($dep["payement"]))
                                                    {
                                                        foreach ($dep["payement"] as $deid => $de)
                                                        {
                                                            if (isset($de["amount"]))
                                                            {
                                                                $totalde += (int)$de["amount"];
                                                            }
                                                        }
                                                    }

                                                    $percent = round(($totalfi * 100) / (int)$dep["price"]);
                                                    $depensed = round(($totalde * 100) / (int)$dep["price"]);
                                                }
                                                else
                                                {
                                                    $totalfi = 0;
                                                    $percent = 0;
                                                    $depensed = 0;
                                                }

                                                ?>

                                                <span class="pull-left text-dark">
                                                    <h3 class="widget-title"><?php if (!empty($dep["price"]) && is_numeric($totalfi))
                                                        {
                                                            echo rtrim(rtrim(number_format($totalfi, 3, ".", " ") , '0') , '.');
                                                        } ?><i class="fa fa-euro"></i>

                                                     (<?php if (!empty($dep["price"]) && is_numeric($totalde))
                                                        {
                                                            echo rtrim(rtrim(number_format($totalde, 3, ".", " ") , '0') , '.');
                                                        } ?><i class="fa fa-euro"></i> dépensé)</h3>

                                                </span>

                                            </div>
                                            <div style="clear: both;"> </div>
                                            <div class="" style="max-height: 72px;">
                                                <?php
                                                if (!empty(Yii::app()->session['userId']) && ($answer["user"] == Yii::app()->session['userId'] || Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) || Form::canAdmin((string)$elform["_id"])) ){
                                                    echo '<button style="background-color: #847e7e;" type="button" id="btnFinancer'.($icont - 1).'" data-id="'.$answer["_id"].'" data-budgetpath="depense" data-form="aapStep1" data-pos="'.($icont - 1).'" data-name="'.@$dep["poste"].'" class="btn btn-secondary btn-xs rebtnFinancer">Ajouter</button>';

                                                }
                                                ?>
                                                <div class="progress">
                                                    <?php
                                                    if ($depensed < $percent){
                                                    ?>
                                                        <div class="progress-bar progress-bar-success2" role="progressbar" style="width:<?php echo ($depensed > 100 ? 100 : $depensed) ?>%">
                                                            <small>Financé dépensé<?php echo $depensed ?> %</small>
                                                        </div>
                                                    <div class="progress-bar progress-bar-success" role="progressbar" style="width:<?php echo (($percent - $depensed) > 100 ? 100 : ($percent - $depensed)) ?>%">
                                                        <small>Financé non dépensé<?php echo $percent - $depensed ?> %</small>
                                                    </div>

                                                    <?php
                                                    } elseif($depensed = $percent) {
                                                    ?>
                                                        <div class="progress-bar progress-bar-success" role="progressbar" style="width:<?php echo ($percent > 100 ? 100 : $percent) ?>%">
                                                        <small>Financé et depensé <?php echo $percent ?> %</small>
                                                        </div>
                                                    <?php
                                                    } else {
                                                    ?>
                                                        <div class="progress-bar progress-bar-success2" role="progressbar" style="width:<?php echo ($percent > 100 ? 100 : $percent) ?>%">
                                                            <small>Dépense Financé<?php echo $percent ?> %</small>
                                                        </div>
                                                        <div class="progress-bar progress-bar-success" role="progressbar" style="width:<?php echo (($depensed - $percent) > 100 ? 100 : $percent) ?>%">
                                                            <small>Dépense non financé <?php echo $depensed - $percent ?> %</small>
                                                        </div>

                                                    <?php
                                                    }
                                                    ?>

                                                    <div class="progress-bar progress-bar-warning" role="progressbar" style="width:<?php echo abs(100 - $percent) ?>%">
                                                        <small>Non financé <?php echo 100 - ($percent) ?> %</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="widget-controls hidden">
                                                <a href="#" class="widget-control-right"><span class="fa fa-times"></span></a>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>


                            <?php $icont++;
                        }
                    }

                    ?>

                </div>
            </div>

            <!--<div class="btn-component padding-15 justify-center" >
                <button class="btn btn-default btn-financing">
                    Financer
                </button>
            </div>-->
        </div>



        <!--GOUVERNANCE -->
       <!-- <div class="single_governance <?/*=empty($answer["answers"]["aapStep1"]["depense"]) ? "hidden" : "" */?>">
            <div class="governance_header padding-15">
                <h4>Gouvernance
                    <span>
                    <i class="fa fa-euro"></i>
                </span>
                </h4>
            </div>
            <div class="governance_body">
                <div class="panel-body no-padding">

                    <?php
/*                    if (!empty($answer["answers"]["aapStep1"]["depense"]))
                    {
                    $iconter = 1;
                    foreach ($answer["answers"]["aapStep1"]["depense"] as $depId => $dep)
                    {
                    */?>

                    <div class="col-md-12 col-sm-12 col-xs-12 contentInformation no-padding <?php /*echo $iconter % 2 === 0 ? "line-light" : "line-dark"; */?>">
                        <div class="col-md-5 col-sm-6 hidden-xs labelGovernance padding-10">
                            <?php /*echo @$dep["poste"] */?>
                        </div>
                        <div class="col-md-7 col-sm-6 col-xs-12 valueAbout padding-10 <?php /*echo $iconter % 2 === 0 ? "line-dark" : "line-light"; */?>">
                            <div class="visible-xs col-xs-12 no-padding"><?php /*echo @$dep["poste"] */?></div>
                            <div class="col-xs-4 col-sm-4 col-md-4 text-center padding-top-15">
                                <span class="text-simple price"><?php /*echo @$dep["price"] */?> €</span>
                            </div>

                            <div class="col-xs-4 col-sm-4 col-md-4 text-center"">
                            <div class="prop-compact-progression">
                                <div class="progress" data-percentage="80">
                            <span class="progress-left">
                                <span class="progress-bar"></span>
                            </span>
                                    <span class="progress-right">
                                <span class="progress-bar"></span>
                            </span>

                                </div>
                            </div>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4 text-center padding-top-15">

                        </div>
                    </div>
                </div>

                <?php /*$iconter++;
                }
                }

                */?>


            </div>
        </div>
-->
        <!--<div class="btn-component padding-15 justify-center" >
            <button class="btn btn-default btn-opinion">
                Donnez votre avis
            </button>
        </div>-->
    </div>




    <!--LISTE DES PRODUIT
    <div class="product_list">
        <div class="row no-margin">
            <div class="col-xs-12 col-sm-5 col-md-5 padding-15">
                <img class="img-responsive" src="http://www.place-to-be.fr/wp-content/uploads/2014/11/meilleur-whisky-du-monde-japonais-1050x656.jpg" alt="burger">
            </div>
            <div class="col-xs-12 col-sm-7 col-md-7 padding-15">
                <div class="product_content">
                    <h4>LANCEMENT DU MEILLEUR WHISKY
                        <span>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o"></i>
                            </span>
                    </h4>
                    <p>
                        Le Single Malt Yamazaki Sherry Cask 2013 a remporté le titre de meilleur whisky du monde. Un titre décerné par Jim Murray qui est au whisky ce que Robert Parker est au vin : un dégustateur qui fait la pluie et le beau temps dans le monde de ce spiritueux principalement produit en Écosse. Jim Murray est l’auteur depuis 2003 de la Bible du Whisky qui, dans son édition 2015, a noté sur 100 quelque 4 700 whiskys.</br>C’est un immense honneur pour Suntory de voir son Yamazaki Sherry Cask 2013 consacré "World Whisky of the Year", a déclaré Keita Minari, directeur Europe de Suntory. Cette édition limitée fait partie d’une collection créée pour présenter les quatre différents fûts qui constituent la signature Yamazaki : sherry cask (fût de sherry, NDLR), bourbon barrel (fût de bourbon), puncheon (barrique fabriquée à partir de chêne américain d’une contenance comprise entre 480 et 520 litres) et mizunara (une espèce rare de chêne japonais).
                    </p>
                    <div class="btn-component">
                        <div class="col-xs-6 no-padding">
                            <button class="btn btn-default btn-evaluate">
                                PLUS D'INFO
                            </button>
                        </div>
                        <div class="col-xs-6 no-padding">
                            <div class="prop-compact-progression">
                                <div class="progress" data-percentage="80">
                                    <span class="progress-left">
                                        <span class="progress-bar"></span>
                                    </span>
                                    <span class="progress-right">
                                        <span class="progress-bar"></span>
                                    </span>
                                    <div class="progress-value">
                                        <div>
                                            80%<br>
                                            <span><i class="fa fa-check"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
    -->
    </div>

    <?php
}
?>

            <div id="container" class="new-form-financer" style="display: none">
                <header>
                    <h1>Financeur</h1>
                </header>
                <main class="mainc">
                    <div id="switchcom">
                        <div class="btn-group depenseoceco">
                            <button type="button" id="dcombtn" class="dcombtn switchcombtn btn active" data-switchactbtn="dcombtn" data-removebtn="icombtn" data-switchactdiv="dcomdiv" data-removediv="icomdiv" >Depuis la communauté </button>

                            <button type="button" id="icombtn" class="icombtn switchcombtn btn " data-switchactbtn="icombtn" data-removebtn="dcombtn" data-switchactdiv="icomdiv" data-removediv="dcomdiv">Inexistant dans la communauté</button>
                        </div>

                        <div style="clear: both;"></div>
                    </div>

                    <div class="dcomdiv">
                        <label class="label-esti">Financeur</label>
                    </div>
                    <div id="dcomdiv" class="dcomdiv switchcomdiv">
                        <select id="financer" class="fi-financer todoinput">
                            <option value=0 >Quel financeur</option>
                            <?php foreach ($orgs as $v => $f)
                            {
                                echo "<option value='" . $v . "'>" . $f . "</option>";
                            } ?>
                        </select>
                        <button class="btn btn-default sup-btn">Ajouter</button>

                        <div style="clear: both;"></div>
                    </div>

                    <div class="icomdiv ocecowebview  hide">
                        <label class="label-esti">Nom</label>
                        <label class="label-esti">Email</label>
                    </div>
                    <div class="icomdiv hide">
                        <!-- <label class=" spanfi-name">Nom</label><label class=" spanfi-mail">Email</label> -->
                        <label class="label-mobi">Nom</label>
                        <input id="" class="todoinput fi-name" type="text" spellcheck="false" placeholder="Nom" onfocus="this.placeholder=''" onblur="this.placeholder='Nom'" />
                        <label class="label-mobi">Email</label>
                        <input id="new-credit-value" class="fi-mail todoinput" type="text" spellcheck="false" placeholder="Email" onfocus="this.placeholder=''" onblur="this.placeholder='Email'" />
                        <div style="clear: both;"></div>

                        <!--  <label class=" spanfi-fond">Fonds, enveloppe ou budget mobilisé</label><label class=" spanfi-montant">Montant Financé</label><label class=" spanfi-btn"></label> -->

                    </div>

                    <div class="ocecowebview">
                        <label class="label-esti">Fonds, enveloppe ou budget mobilisé</label>
                        <label class="label-esti">Montant Financé</label>
                    </div>

                    <div class="">

                        <label class="label-mobi">Fonds, enveloppe ou budget mobilisé</label>
                        <input id="" class="todoinput fi-fond" type="text" spellcheck="false" placeholder="Fonds, enveloppe ou budget mobilisé" onfocus="this.placeholder=''" onblur="this.placeholder='Fonds, enveloppe ou budget mobilisé'" />
                        <label class="label-mobi">Montant Financé</label>
                        <input id="new-credit-value" class="fi-montant todoinput" type="number" spellcheck="false" placeholder="Montant Financé" onfocus="this.placeholder=''" onblur="this.placeholder='Montant Financé'" />

                        <div style="clear: both;"></div>
                    </div>

                    <div id="single-line"></div>

                    <ul id="financer-list">
                    </ul>
                </main>
            </div>


<script>
    $(function(){
        $('p').each(function() {
            var $this = $(this);
            if($this.html().replace(/\s|&nbsp;/g, '').length == 0)
                $this.remove();
        });
        $('.rater-sheet').each(function(i, obj) {
            var starRatingAdmin = raterJs( {
                rating : (typeof $(obj).data("rating") =="number" ? $(obj).data("rating") : 0),
                starSize:20,
                readOnly:true,
                showToolTip:false,
                element:obj,
            });
        });
        $('.rebtnFinancer').off().click(function() {
            tplCtx.pos = $(this).data("pos");
            tplCtx.budgetpath = $(this).data("budgetpath");
            tplCtx.collection = "answers";
            tplCtx.id = $(this).data("id");
            tplCtx.form = $(this).data("form");
            prioModal = bootbox.dialog({
                message: $(".new-form-financer").html(),
                title: "Ajouter un Financeur",
                className: 'financerdialog',
                show: false,
                size: "large",
                buttons: {
                    success: {
                        label: trad.save,
                        className: "btn-primary",
                        callback: function () {

                            //var formInputsHere = formInputs;
                            // var financersCount = ( typeof eval("answerObj.answers."+tplCtx.budgetpath+"["+tplCtx.pos+"].financer") != "undefined" ) ? eval("answerObj.answers."+tplCtx.budgetpath+"["+tplCtx.pos+"].financer").length : 0;

                            tplCtx.path = "answers.aapStep1";

                            tplCtx.path = tplCtx.path+"."+tplCtx.budgetpath+"."+tplCtx.pos+".financer."+financerList[tplCtx.pos].length;

                            // if( notNull(formInputs [tplCtx.form]) )
                            // 	tplCtx.path = "answers."+tplCtx.budgetpath+"."+tplCtx.pos+".financer."+financersCount;

                            var today = new Date();
                            today = today.getDate() + '/' + (today.getMonth()+1) + '/' + today.getFullYear();
                            tplCtx.value = {
                                line   : $(".financerdialog .fi-fond").val(),
                                amount : $(".financerdialog .fi-montant").val(),
                                user   : userId,
                                date   : today
                            };

                            if($(".financerdialog .fi-financer").val() != 0 ){
                                tplCtx.value.id = $(".financerdialog .fi-financer").val();
                                tplCtx.value.name = $(".financerdialog .fi-financer option:selected").text();
                            }else if($(".financerdialog .fi-name").val() != "" ){
                                tplCtx.value.name = $(".financerdialog .fi-name").val();
                                tplCtx.value.email = $(".financerdialog .fi-mail").val();
                            }

                            delete tplCtx.pos;
                            delete tplCtx.budgetpath;
                            mylog.log("btnFinancer save",tplCtx);
                            dataHelper.path2Value( tplCtx, function(params) {
                                prioModal.modal('hide');
                                //saveLinks(answerObj._id.$id,"financerAdded",userId,function(){
                                prioModal.modal('hide');
                                //reloadInput("<?php //echo $key
                                ?>//", "<?php //echo @$form["id"]
                                ?>//");
                                // });
                                location.reload();
                            } );
                        }
                    },
                    cancel: {
                        label: trad.cancel,
                        className: "btn-secondary",
                        callback: function() {
                        }
                    }
                },
                onEscape: function() {
                    prioModal.modal("hide");
                }
            });
            prioModal.modal("show");

            var thisbtn = $(this);

            prioModal.on('shown.bs.modal', function (e) {

                $(".sup-btn").off().on("click",function() {
                    prioModal.modal("hide");
                    dyFObj.openForm("organization",function(){ },null,null,
                        {
                            afterSave : function(data) {
                                var sendDataM = {
                                    parentId : cntxtId,
                                    parentType : cntxtType,
                                    listInvite : {
                                        organizations : {}
                                    }
                                };
                                sendDataM.listInvite.organizations[data.id] = {};
                                sendDataM.listInvite.organizations[data.id]["name"] = data.map.name;
                                sendDataM.listInvite.organizations[data.id]["roles"] = ["Financeur"];

                                ajaxPost("",
                                    baseUrl+"/co2/link/multiconnect",
                                    sendDataM,
                                    function(data) {
                                        dyFObj.closeForm();
                                        //reloadInput("<?php //echo $key
                                        ?>//", "<?php //echo @$form["id"]
                                        ?>//");
                                        if (!standedAl){
                                            reloadWizard();
                                        }else {

                                        }
                                    },
                                    null,
                                    "json"
                                );
                                console.log("hey",data);
                            }
                        }
                    );
                });

                $(".switchcombtn").on("click",function() {
                    $(".financerdialog").find("."+$(this).data("switchactbtn")).addClass("active");
                    $(".financerdialog").find("."+$(this).data("removebtn")).removeClass("active");
                    $(".financerdialog").find("."+$(this).data("switchactdiv")).removeClass("hide");
                    $(".financerdialog").find("."+$(this).data("removediv")).addClass("hide");
                });

            });

        });

        var answerid = "<?php echo (!empty($answerid) ? $answerid : $answerid ); ?>" ;


        ajaxPost(
            null,
            baseUrl+'/survey/answer/countvisit/answerid/'+answerid+'/',
            null,
            function(data){
                if (data && data.new == true){
                    dataHelper.path2Value( data.upvalue, function(){
                        $('#countv').html(data.upvalue.value.count);
                    });
                }
            },
            "json"
        );

        $.each($(".sheet-markdown"), function(k,v){
            descHtml = dataHelper.markdownToHtml($(v).html(),{
                parseImgDimensions:true,
                simplifiedAutoLink:true,
                strikethrough:true,
                tables:true,
                openLinksInNewWindow:true
            });
            $(v).html(descHtml);
        });
        $('.like-project').on('click',function(){
            $btn = $(this);
            $val = $btn.data('value').toString();
            var tplCtx ={
                id : $btn.data("ans"),
                collection : "answers",
                format : true,
                path :"publicRating."+userId,
            }
            if($val=="false"){
                $btn.data('value',"true");
                $btn.find('i').removeClass("fa-heart-o").addClass("fa-heart");
                var count = $btn.find('.counter').text();
                $btn.find('.counter').text(parseInt(count)+1);
                tplCtx.value = $btn.data('value').toString();
                ajaxPost(null, baseUrl+'/costum/aap/publicrate',
                    tplCtx,
                    function(res){
                        /*if(res.result)
                            toastr.success(res.msg)
                        else
                            toastr.error(res.msg)*/
                    },null);
            }
            else if($val=="true"){
                $btn.data('value',"false");
                $btn.find('i').removeClass("fa-heart").addClass("fa-heart-o");
                var count = $btn.find('.counter').text();
                $btn.find('.counter').text(parseInt(count)-1);
                tplCtx.value = $btn.data('value').toString();
                ajaxPost(null, baseUrl+'/costum/aap/publicrate',
                    tplCtx,
                    function(res){
                        /*if(res.result)
                            toastr.success(res.msg)
                        else
                            toastr.error(res.msg)*/
                    },null);
            }
        })
        var swiper = new Swiper(".mySwiper", {
            pagination: {
                el: ".swiper-pagination",
                type: "fraction",
            },
            autoplay: {
                delay: 2500,
                disableOnInteraction: true,
            },
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
        });
        setTimeout(() => {
            readMoreLess();
        }, 500); 
    })
</script>
