<style type="text/css">
    @media only screen and (min-width: 992px) {
        .aappageform {

        }
    }
</style>

<?php
$me = isset(Yii::app()->session['userId']) ? Person::getById(Yii::app()->session['userId']) : null;
$canEdit=false;

if(!empty($answerid) && !empty($answers[$answerid])) {
    $answer = $answers[$answerid];
    //$canEdit = Authorisation::isUserSuperAdmin((string) @$me["_id"]) || Form::canAdmin((string)@$elform["_id"]) || (string) @$me["_id"] == $answer["user"];
?>

    <div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
        <ul class="breadcrumb">
            <li ><a><?php if (isset($el["el"]["name"])) { echo $el["el"]["name"]; } ?></a></li>
            <li class="active"><a href="javascript:;" class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.list" ><?php if (isset($elform["name"])) { echo $elform["name"]; } ?> </a></li>
            <!-- <li ><a><?php if (isset($elform["name"])) { echo $elform["name"]; } ?></a></li> -->
            <li class="active"><a href="javascript:;" class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.sheet.answer.<?php echo (string)$answer["_id"] ?>" >edition proposition : <?php echo @$answer["answers"]["aapStep1"]["titre"] ?> </a></li>
        </ul>
    </div>

<?php
if (empty($pageedit) ||  ( !empty($pageedit) && !$pageedit)){
    ?>

    <div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
        <div class="aapconfigdiv">
            <div class="text-center">
                <button type="button"  class="aapgoto resume aap-breadrumbbtn" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.sheet.answer.<?php echo (string)$answer["_id"] ?>"> <i class="fa fa-edit"></i> Résumé de la proposition </button>
                <button type="button" class="aapgoto  observatory aap-breadrumbbtn" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.formdashboard.answer.<?php echo (string)$answer["_id"] ?>"><i class="fa fa-plus-square-o"></i> Observatoire </button>
            </div>
        </div>
    </div>

    <?php
}
    ?>


<!--<div class="col-md-offset-8 col-sm-offset-4">
    <a class="aapgoto aapswitch btn btn-outline-info" data-url="<?php /*echo Yii::app()->createUrl("/costum")*/?>/co/index/slug/<?php /*echo @$slug; */?>#welcome.slug.<?php /*echo $el_slug; */?>.page.sheet.answer.<?php /*echo (string)$answer["_id"]; */?>"><i class="fa fa-plus-square-o"></i> Résumé du proposition </a>
</div>-->

<?php
} else {
?>
    <div class="col-md-offset-1 col-md-<?php echo ((empty($pageedit) || !$pageedit) ? "6" : '10'); ?>">
        <ul class="breadcrumb">
            <li ><a><?php if (isset($el["el"]["name"])) { echo $el["el"]["name"]; } ?></a></li>
            <li ><a><?php if (isset($elform["name"])) { echo $elform["name"]; } ?></a></li>
            <li class="active"><a class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.details "><?php echo ((empty($pageedit) || !$pageedit) ? "nouvelle proposition" : 'parametre pour '.$el["el"]["name"]); ?></a></li>
        </ul>
    </div>

<?php
if (empty($pageedit) ||  ( !empty($pageedit) && !$pageedit)){
    ?>

    <div class="col-md-4">
        <div class="aapconfigdiv">
            <div class="text-center">
                <button type="button"  class="aapgoto resume aap-breadrumbbtn newsheet" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.sheet.answer."> <i class="fa fa-edit"></i> Résumé de la proposition </button>
                <button type="button" class="aapgoto observatory aap-breadrumbbtn newdashboard" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.formdashboard."><i class="fa fa-plus-square-o"></i> Observatoire </button>
            </div>
        </div>
    </div>

    <?php
}
    ?>

<!--    <div class="col-md-offset-8 col-sm-offset-4">
        <a class="aapgoto aapswitch aapaddanswertourl btn btn-outline-info" data-url="<?php /*echo Yii::app()->createUrl("/costum")*/?>/co/index/slug/<?php /*echo @$slug; */?>#welcome.slug.<?php /*echo $el_slug; */?>.page.sheet.answer.<?php /* */?>"><i class="fa fa-file-text"></i> Résumé du proposition </a>
    </div>-->
<?php
}
?>
<div class="aappageform col-md-offset-1 col-md-10 text-center" id="aapformcont">
    <i class="fa fa-spinner fa-3x fa-spin" ></i>
    <!-- <div class="col-xs-12 text-center">
        <h1>Votre proposition n'a pas retenu par l'administrateur. Merci</h1>
    </div> -->
</div>
<script type="text/javascript">
    var newresaap = true;

    var answerid =  "<?php echo (!empty($answerid) ?$answerid : ''); ?>";

    if (answerid != ''){
        newresaap = false;
    }

    var formedit = <?php echo (!empty($pageedit) ? $pageedit : 'false'); ?>;

    var el_form = "<?php echo (isset($elform["_id"]) ? (string)$elform["_id"] : ''); ?>";

    $(document).ready(function() {
        var validation = function(){
            if( typeof $('#titre').val() != "undefined" && typeof $('#description').val() != "undefined" &&  ($('#titre').val().trim()=='' || $("#description").val().trim()=="")){
                $('.aapgoto.resume,.aapgoto.observatory, .aapconfigdiv').hide();
            }
            $('#titre,#description').on('blur',()=>{
                if(typeof $('#titre').val() != "undefined" && typeof $('#description').val() != "undefined" &&  ($('#titre').val().trim()=='' || $("#description").val().trim()==""))
                    $('.aapgoto.resume,.aapgoto.observatory, .aapconfigdiv').hide();
                else
                    $('.aapgoto.resume,.aapgoto.observatory, .aapconfigdiv').show();
            })
        }
        //$canEdit = "<?= $canEdit ?>";
        if(newresaap && el_form != "" && !formedit){
            ajaxPost("#aapformcont", baseUrl+'/survey/answer/index/id/new/form/'+el_form,
                null,
                function(data){
                    validation();
                    if (answerId != '' ){
                        window.location.href = window.location.href + ".answer." + answerId;
                        if (typeof answerId != "undefined"){
                            $('.newsheet').data('url', $('.newsheet').data('url')+answerId);
                            $('.newdashboard').data('url', $('.newdashboard').data('url')+answerId);
                        }
                    }
                },"html");
        }else if (formedit && el_form != ""){
            ajaxPost("#aapformcont", baseUrl+'/survey/form/edit/id/'+el_form,
                null,
                function(){
                    validation();
                },"html");
        }
        else{
            /*if(typeof is_accepted != "undefined" && is_accepted != false)*/
            //if($canEdit)
                ajaxPost("#aapformcont", baseUrl+'/survey/answer/index/id/'+answerid+'/form/'+el_form+'/mode/w',
                    null,
                function(){
                     validation();
                },"html");
            /*else
                $("#aapformcont").html(`<div class="col-xs-12">
                                            <h3 class="text-red text-center"><?= Yii::t("common", "You are not authorized") ?></h3>
                                        </div>`);*/
                
                
        }
    });

</script>
