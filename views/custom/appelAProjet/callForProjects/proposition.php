<style>
    .content-btn-action-aap {
        display: block;
        z-index: 10;
        position: absolute;
        top: 250px;
        right: 20px;
    }

</style>

<?php
HtmlHelper::registerCssAndScriptsFiles(['/plugins/rater/rater-js.js'], Yii::app()->request->baseUrl);
HtmlHelper::registerCssAndScriptsFiles(["/css/aap/proposition.css"], Yii::app()->getModule('costum')->assetsUrl);
HtmlHelper::registerCssAndScriptsFiles(["/css/aap/list.css"], Yii::app()->getModule('costum')->assetsUrl);
HtmlHelper::registerCssAndScriptsFiles(["/css/aap/listitem.css"], Yii::app()->getModule('costum')->assetsUrl);
HtmlHelper::registerCssAndScriptsFiles(array(
    // '/css/dashboard.css',
    '/js/form.js'
), Yii::app()->getModule( Survey::MODULE )->getAssetsUrl() );
$logoOceco = Yii::app()->getModule('co2')->assetsUrl."/images/OCECO.png";

if (empty($modalrender)){
    $modalrender = false;
    $renderUrl = $_SERVER['REQUEST_URI'];
} else {
    $modalrender = true;
    $renderUrl = $urlR;
}

$exp_url = explode("/", $renderUrl);
$el_slug = null;
$elform = null;
//$page = "list";
$elanswers = [];
$el_form_id = "";
//$configform =
if (
        in_array("slug", $exp_url) &&
        isset($exp_url[array_search('slug', $exp_url) + 1]) &&
        is_string($exp_url[array_search('slug', $exp_url) + 1]) &&
        $exp_url[array_search('slug', $exp_url) + 1] != ""
){
    $el_slug = $exp_url[array_search('slug', $exp_url) + 1];
}

if (
    in_array("formid", $exp_url) &&
    isset($exp_url[array_search('formid', $exp_url) + 1]) &&
    is_string($exp_url[array_search('formid', $exp_url) + 1]) &&
    $exp_url[array_search('formid', $exp_url) + 1] != ""
){
    $el_form_id = $exp_url[array_search('formid', $exp_url) + 1];
    $elform = PHDB::findOneById(Form::COLLECTION, $el_form_id);
    if(!empty($elform)){
        $el_configform = PHDB::findOneById(Form::COLLECTION, $elform["config"]);
        $elanswers = PHDB::find(Form::ANSWER_COLLECTION,array( "form" => (string)$elform["_id"]));
    }
}

if (
    in_array("page", $exp_url) &&
    isset($exp_url[array_search('page', $exp_url) + 1]) &&
    is_string($exp_url[array_search('page', $exp_url) + 1]) &&
    $exp_url[array_search('page', $exp_url) + 1] != ""
){
    $page = $exp_url[array_search('page', $exp_url) + 1];
}

if (
    in_array("answer", $exp_url) &&
    isset($exp_url[array_search('answer', $exp_url) + 1]) &&
    is_string($exp_url[array_search('answer', $exp_url) + 1]) &&
    $exp_url[array_search('answer', $exp_url) + 1] != ""
){
    $answerid = $exp_url[array_search('answer', $exp_url) + 1];
}

if (in_array("editform", $exp_url)){
    $pageedit = true;
} else {
    $pageedit = false;
}

if (isset($this->costum["slug"])){
    $slug = $this->costum["slug"];
}
$el = null;
if (!empty($el_slug)){
    $el = Slug::getElementBySlug($el_slug);
    $elconfig = Slug::getElementBySlug($slug);
    /*$el_configform = PHDB::findOne(Form::COLLECTION,array( "parent.".$elconfig["id"] => array('$exists'=>1)));
    if (!empty($el_configform) && !empty($el["id"]) && !empty($elconfig["id"])){
        $elform = PHDB::findOne(Form::COLLECTION,array( "parent.".$el["id"] => array('$exists'=>1), "config" => (string)$el_configform["_id"] ));
        if (!empty($elform["_id"])){
            $elanswers = PHDB::find(Form::ANSWER_COLLECTION,array( "form" => (string)$elform["_id"]));
        }
    }*/
}

//count new proposition
if(isset(Yii::app()->session['userId'])){
    $countNewProposition = PHDB::count(Form::ANSWER_COLLECTION,[
        '$and'=>[
            array("form"=>(string)$elform["_id"]),
            array("answers"=>['$exists'=>true]),
            array("visitor_counter.user" => ['$ne'=>Yii::app()->session['userId']]),
            array("user" =>['$ne'=>Yii::app()->session['userId']])
        ]
    ]);
    //var_dump($countNewProposition);
}

if (empty($elform)){ ?>
<div class="portfolio-modal modal fade" id="modalListAapFromWelcome" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-content padding-top-15">
        <div class="container bg-white">
            <div class="col-md-12">
                <h2 class="margin-bottom-35 padding-bottom-35">Quel appel à projet ?</h2>
                <?php $el_configform = PHDB::findOne(Form::COLLECTION,array("parent.".$this->costum["contextId"] => ['$exists'=> true] ));
                $allForm = PHDB::find(Form::COLLECTION,array("config"=> (string)$el_configform["_id"]));
                $allFormParent = array();
                $i = 1;
                foreach ($allForm as $kff => $vff) {
                    foreach ($vff["parent"] as $kp => $vp) { ?>
                        <div class="col-xs-12 text-left form-chooser margin-bottom-5">
                            <a url="javascript:;" class="btnaapp_link h4 btn-block" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo Slug::getSlugByIdAndCol($kp,$vp['type']) ?>.formid.<?php echo $kff ?>.page.list"> 
                            <?= $i?>- <b> Organisation :</b> <?= $vp['name']?> | <b> Questionnaire : </b><?= $vff['name']?> 
                            </a>
                        </div>
                    <?php 
                    $i++;}
                }?> 
            </div>
		</div>
	</div>
</div>
<?php 
}
    //cms mode

$blockKey = @$blockCms["_id"];
$keyTpl ="callForProjects";

if (!empty($blockCms))
    $kunik = $keyTpl.(string)$blockCms["_id"];
else
    $kunik = "";

$paramsData = [
    "titleDetails" => "Details de l'appel à projet",
    "titleList" => "Liste des projets",
    "titleEvaluate" => "Evaluer",
    "titleObservatory" => "Observatoire global",
    "titleKanban" => "Kanban",
];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (isset($blockCms[$e]) ) {
              $paramsData[$e] = $blockCms[$e];
      }
    }
  }


if ( !$modalrender && (Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) || Form::canAdmin((string)@$elform["_id"]) )){
?>

<div class="text-center content-btn-action-aap btn-edit-delete-<?= @$kunik?>">

    <button class="material-button tooltips btnConfigForm" data-form='<?=  htmlspecialchars(json_encode($elform), ENT_QUOTES, 'UTF-8')?>' type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Configurer appel à projet">
        <i class="fa fa-paste" aria-hidden="true"></i>
    </button>

    <button class="material-button tooltips margin-top-10 aapgoto" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.form.editform" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Configurer questionaire">
        <i class="fa fa-copy" aria-hidden="true"></i>
    </button>

    <button class="material-button tooltips margin-top-10 aapgoto" data-url="/#@<?php echo $el_slug; ?>" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="<?php echo @$el["name"] ?>" >
        <i class="fa fa-home" aria-hidden="true"></i>
    </button>
    <button class="material-button tooltips margin-top-10" onclick="smallMenu.openAjaxHTML(baseUrl+'/costum/aap/getviewbypath/path/costum.views.custom.appelAProjet.templateList');" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Template AAP" >
        <i class="fa fa-window-restore" aria-hidden="true"></i>
    </button>

</div>

<?php } ?>



<!--<button id="btn-gotoco" class="btnaapp_link btn btn-success" data-url="/#@<?php /*echo $el_slug; */?>" >CO</button>
-->
<?php
if (!$modalrender){
?>
<nav class="aapnav">
    <ul>
        <li class="<?php echo ((!empty($page) && $page == "details") ? "active" : ""); ?>"><a class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.details "><?php echo $paramsData["titleDetails"] ?></a></li>
        <!-- <li class="<?php //echo ((!empty($page) && $page == "list") ? "active" : ""); ?>"><a class="btnaapp_link" data-url="<?php // echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php // echo $el_slug; ?>.page.list"> Liste des projets</a></li> -->
        <li class="<?php echo ((!empty($page) && $page == "list") ? "active" : ""); ?>">
        <a class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.list"> <?php echo $paramsData["titleList"] ?> 
            <span class="label label-success tooltips <?= empty(@$countNewProposition) ? "hidden":"" ?>" style="border-radius: 10px;" data-toggle="tooltip" data-placement="right" data-original-title="<?= @$countNewProposition ?> nouvelle(s) proposition(s)">
            <?= @$countNewProposition ?>
            </span>
        </a></li>
        <li class="<?php echo ((!empty($page) && $page == "evaluate") ? "active" : ""); ?>"><a class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.evaluate"><?php echo $paramsData["titleEvaluate"] ?></a></li>
        <li class="<?php echo ((!empty($page) && $page == "dashboard") ? "active" : ""); ?>"><a class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.dashboard"><?php echo $paramsData["titleObservatory"] ?></a></li>
        <?php if(isset(Yii::app()->session["user"]["slug"]) && in_array(Yii::app()->session["user"]["slug"], ["gova","anatolerakotoson","oceatoon"/*,"mirana", "ifals", "devchriston", "nicoss","Jaona","yorre"*/])){ ?>
            <li class="<?php echo ((!empty($page) && $page == "kanban") ? "active" : ""); ?>"><a class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.kanban"><?php echo $paramsData["titleKanban"] ?></a></li>
        <?php } ?>
        <!-- <li class="<?php //echo ((!empty($page) && $page == "admin") ? "active" : ""); ?>"><a class="btnaapp_link" data-url="<?php //echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php //echo @$slug; ?>#welcome.slug.<?php //echo $el_slug; ?>.page.admin">Administrer</a></li> -->
    </ul>
</nav>

<?php
}
?>
<?php
    if (!empty($page) && !empty($el_slug) && !empty($el_configform)){
        $params = [
            "answers" => @$elanswers, // array de tous les answers du form parent
            "slug" => @$slug, // slug de l'element qui abbrite le form config
            "el_slug" => @$el_slug,  // element qui abbrite le form config
            "elform" => @$elform, //le form parent
            "el" => @$el, //element qui abbrite le form parent
            "el_configform" => $el_configform,
            "el_form_id" => $el_form_id,
            "countNewProposition" => @$countNewProposition
        ];
        if (!empty($answerid)){
            $params["answerid"] = $answerid;
        }
        if (!empty($el) && !empty($el["id"])){
            $params["elid"] = $el["id"];
        }

        if (isset($pageedit)){
            $params["pageedit"] = $pageedit;
        }
        $page = explode("?",$page);
        $page = $page[0];
        echo $this->renderPartial("costum.views.custom.appelAProjet.callForProjects.".$page, $params,true );
    }
?>


<div>

</div>

<script>
/*    function sticky_relocate() {
        var window_top = $(window).scrollTop();
        var div_top = $('#sticky-anchor').offset().top;
        if (window_top > div_top) {
            $('#sticky').addClass('stick');
        } else {
            $('#sticky').removeClass('stick');
        }
    }*/
    $(window).scroll(function(){
        console.log("eeee");
    });

    var fObj = formObj.init({});

    fObj.container = "block-container-callForProjects60dd6a3860e5d2695e342d2b";

    fObj.events.form(fObj);

    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        <?php if(isset($elform["name"]) && isset($el["el"]["name"])){  ?>
            var newPropCounter = <?= ((isset($countNewProposition) && !empty($countNewProposition)) ? $countNewProposition : 0 ) ?>;
            newPropCounter = (newPropCounter!=0 ? '('+newPropCounter+') ' : '');
            setTimeout(() => { setTitle(newPropCounter + <?= json_encode(@$el["el"]["name"].' > '.$elform["name"]) ?>) }, 700);
        <?php } ?>

        var ocecoLogo = "<?= $logoOceco ?>";
        var el_slug = "<?= $el_slug ?>";
        if(location.hash =="" || location.hash =="#welcome" || location.hash.indexOf('..') != -1 || location.hash.indexOf('.null.') != -1){
             $('#modalListAapFromWelcome').modal();
        }
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {
                "title" : "Configurer votre section",
                "description" : "Personnaliser votre section",
                "icon" : "fa-cog",

                "properties" : {
                    "titleDetails" : {
                        "inputType" : "text",
                        "label" : "Titre",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.titleDetails
                    },
                    "titleList" : {
                        "inputType" : "text",
                        "label" : "Titre",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.titleDetails
                    },
                    "titleEvaluate" : {
                        "inputType" : "text",
                        "label" : "Titre",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.titleDetails
                    },
                    "titleObservatory" : {
                        "inputType" : "text",
                        "label" : "Titre",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.titleDetails
                    },
                    "titleKanban" : {
                        "inputType" : "text",
                        "label" : "Titre",
                        values :  sectionDyf.<?php echo $kunik ?>ParamsData.titleKanban
                    }
                },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function (data) {
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                        tplCtx.value[k] = $("#"+k).val();
                    });
                    console.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.commonAfterSave(params,function(){
                                toastr.success("Élément bien ajouter");
                                $("#ajax-modal").modal('hide');
                                dyFObj.closeForm();
                                urlCtrl.loadByHash(location.hash);
                            });
                        } );
                    }

                }
            }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
        });

        $('.btnaapp_link').off().on("click", function () {
            mylog.log("azeee", $(this).data("url"));
            var url = $(this).data("url")
            if(location.hash.indexOf('.null.') > 0 || location.hash.indexOf('..') > 0)
                urlCtrl.loadByHash("#welcome");
            else{
                window.location.href = url;
                urlCtrl.loadByHash(location.hash);
            }
        });

        $(".aapgoto").off().on("click", function(){
            if(isUserConnected == "unlogged")
                return $("#modalLogin").modal();
            mylog.log("azeee", $(this).data("url"));
            window.location.href = $(this).data("url");
            urlCtrl.loadByHash(location.hash);
        });
        $('.rater-public').each(function(i, obj) {
            /*if(userId != ""){
                var starRating1 = raterJs( {
                    rating : $(obj).data("rating"),
                    starSize:25,
                    step:0.5,
                    element:obj, 
                    rateCallback:function rateCallback(rating, done) {
                        var $this = this;
                        this.setRating(rating); 
                            var tplCtx ={
                                id : $(obj).data("ans"),
                                collection : "answers",
                                path :"publicRating."+userId,
                                value : rating
                            }
                            dataHelper.path2Value(tplCtx, function (params) {
                                console.log(params,"govalosy")
                                if(params.result){  
                                    var publicRating = 0;
                                    if(typeof params.elt.publicRating != "undefined"){
                                        $.each(params.elt.publicRating,(k,v)=>{
                                            publicRating = publicRating + parseFloat(v)
                                        })
                                    }
                                    publicRating = publicRating/Object.keys(params.elt.publicRating).length;
                                    $this.setRating(publicRating);
                                    done(); 
                                }
                            });
                        done(); 
                    }
                });
            }else{
                var starRating1 = raterJs( {
                    rating : $(obj).data("rating"),
                    starSize:25,
                    showToolTip:true,
                    step:0.5,
                    element:obj,
                    readOnly:true, 
                });
            }*/
        });
        $('.rater-admin').each(function(i, obj) {
            /*var starRatingAdmin = raterJs( {
                rating : $(obj).data("rating"),
                starSize:25,
                showToolTip:true,
                readOnly:true,
                element:obj, 
            });*/
        });
    });
    function readMoreLess(showChar=400,moretext = "Lire la suite",lesstext = "Lire moins") {
        var ellipsestext = "...";
        $('.project-description').each(function() {
        var content = $(this).html();
        var textcontent = $(this).text();

        if (textcontent.length > showChar) {

            var c = textcontent.substr(0, showChar);
            //var h = content.substr(showChar-1, content.length - showChar);

            var html = '<span class="container"><span>' + c + '</span>' + '<span class="moreelipses">' + ellipsestext + '</span></span><span class="morecontent">' + content + '</span>';

            $(this).html(html);
            $(this).after('<a href="" class="moreLinkaap btn-xs btn btn-default">' + moretext + '</a>');
        }

        });

        $(".moreLinkaap").click(function() {
            if ($(this).hasClass("less")) {
                $(this).removeClass("less");
                $(this).html(moretext);
                $(this).prev().children('.morecontent').fadeToggle(500, function(){
                $(this).prev().fadeToggle(500);
                });
            
            } else {
                $(this).addClass("less");
                $(this).html(lesstext);
                $(this).prev().children('.container').fadeToggle(500, function(){
                $(this).next().fadeToggle(500);
                });
            }
            
            return false;
        });
    }
</script>



