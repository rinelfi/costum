<?php
	PHDB::updateWithOptions(Form::ANSWER_COLLECTION, 
		array("form"=>(string)$elform["_id"],"acceptation"=>array('$exists' => false ),"answers.aapStep1"=>['$exists'=>true]),  
		array('$set' => array("acceptation" => "pending")),
		array('multiple' => true));
	$allThematics = PHDB::find(Form::ANSWER_COLLECTION,array("form"=>(string)$elform["_id"],"answers.aapStep1.tags"=>['$exists'=>true]),array("answers.aapStep1.tags"));
	$thematics = [];
	foreach($allThematics as $kthem => $vthem){
		$thematics = array_merge($thematics,strtolower($vthem["answers"]["aapStep1"]["tags"]));
	}
	sort($thematics);
?>

<div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12" style="z-index: 1;">
    <ul class="breadcrumb">
        <li ><a><?php if (isset($el["el"]["name"])) { echo $el["el"]["name"]; } ?></a></li>
        <li ><a><?php if (isset($elform["name"])) { echo $elform["name"]; } ?></a></li>
        <li class="active"><a href="javascript:;" class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.list ">Liste des propositions</a></li>
    </ul>
</div>

<div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12" style="z-index: 1;">
    <div class="aapconfigdiv">
        <div class="">
			<div class="view-mode">
				<a href="javascript:;" class="compact margin-right-5"><i class="fa fa-2x fa-th" aria-hidden="true"></i> <span class="sr-only"></a>
				<a href="javascript:;" class="minimalaap margin-right-5"><i class="fa fa-2x fa-align-justify" aria-hidden="true"></i></a>
				<a href="javascript:;" class="detaaap margin-right-5"><i class="fa fa-2x fa-picture-o" aria-hidden="true"></i></a>
			</div>
            <!-- <div class="dropdown" style="display:inline-flex;">
                <button id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" type="button" class="aap-breadrumbbtn dropdown-toggle" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.form"><i class="fa fa-plus-square-o"></i> Changer de vue <span class="caret"></span></button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                    <li><a href="javascript:;" class="detaaap" >Detailé</a></li>
                    <li><a href="javascript:;" class="minimalaap" >Minimaliste</a></li>
                </ul>
            </div> -->
            <button type="button" class="aapgoto aap-breadrumbbtn" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.form"><i class="fa fa-plus-square-o"></i> Nouvelle proposition</button>
			<?php if(@$el_configform["showMap"]){ ?>
				<button class="btn-show-hide-map margin-bottom-10 aap-breadrumbbtn" data-showmap="<?= @$elform["showMap"]==true ? "true" : "false" ?>"></button>
			<?php } ?>
		</div>
    </div>
</div>
<div class="list-aap-container <?= @$el_configform["showMap"]==true ? "cfm container-fluid" : "container" ?>">
	<div class="row">
		<div class="col-xs-12 text-center">
			<div id='filterContainer' class='searchObjCSS'></div>
		</div>
		<div class="content-answer-search col-xs-12 <?= @$el_configform["showMap"]==true ? "cm8 col-md-8" : "col-md-12" ?>">
			<div class="headerSearchContainer col-xs-12 no-padding"></div>
			<div class="col-xs-12 bodySearchContainer margin-top-10">
				<div class="no-padding col-xs-12" id="dropdown_search">
					<p class="text-center"><i class="fa fa-3x fa-spinner fa-spin"></i></p>
				</div>
				<div class="no-padding col-xs-12 text-left footerSearchContainer"></div>   
			</div>
		</div>
		<div id="mapOfResultsAnsw" class="col-md-4 col-xs-12 pull-right" style="height:500px; <?= @$el_configform["showMap"]==true ? " " : "display:none" ?>"></div>
	</div>
</div>

<script type="text/javascript">
	var isOcecoform = <?= isset($el_configform["subType"]) && $el_configform["subType"] == "ocecoformConfig" ? "true" : "false" ?>;
	var thematicAap = <?= json_encode(array_values(array_unique($thematics))) ?>;
	console.log(thematicAap,"gova");
    var elform = "<?= (string) @$elform["_id"] ?>";
	var actionDomains = <?= isset($elform["actionDomains"]) ? json_encode(@$elform["actionDomains"]): "{}"?>;
	var actionDomainsObj = {};
	$.each(actionDomains,function(k,v){
		actionDomainsObj[v] = v
	});

	var objectives = <?= isset($elform["objectives"]) ? json_encode($elform["objectives"]) : "{}" ?>;
	var objectivesObj = {};
	$.each(objectives,function(k,v){
		objectivesObj[v] = v
	});


	var $slug = <?php echo json_encode($slug)  ?>;
	var $el_slug = <?php echo json_encode($el_slug)  ?>;
	var $el_configform = <?php echo json_encode($el_configform)  ?>;
	var $el_form = <?php echo json_encode($elform)  ?>;
	var $el = <?php echo json_encode($el)  ?>;
    var $el_form_id = <?php echo json_encode($el_form_id)  ?>;
	var $countNewProposition = <?php echo json_encode($countNewProposition)  ?>;

	searchObject.types=["answers"];
	searchObject.indexStep=20;

	var tagsValue = getUrlParameter("tags") != false ? getUrlParameter("tags").split(',') : [];
		
	//creation d'instance de map
	if($el_configform.showMap)
		var mapAnsw = new CoMap({
			container : "#mapOfResultsAnsw",
			activePopUp : true,
			mapCustom:{
				tile : "mapbox"
			},
			elts : []
		});

	var paramsFilter= {
		container : "#filterContainer",
		urlData : baseUrl+"/survey/answer/directory/source/"+costum.slug+"/form/"+elform,
		interface : {
			events : {
				page : true
			}
		},
		filters : {
			//text : true,
			/*address : {
				view: "text",
				event:"text",
				placeholder : "Où (ville, cp) ?"
			},*/
			"answers.aapStep1.titre" : {
				view : "text",
				event : "text",
				placeholder : "Nom"
			},
			"actionPrincipal" : {
				view : "dropdownList",
				type : "filters",
				field : "answers.aapStep2.caracter.actionPrincipal",
				name : "Domaine d'actions primaire",
				event : "filters",
				keyValue : false,
				list : actionDomainsObj
			},
			"actionSecondaire" : {
				view : "dropdownList",
				type : "filters",
				field : "answers.aapStep2.caracter.actionSecondaire",
				name : "Domaine d'actions secondaire",
				event : "filters",
				keyValue : false,
				list : actionDomainsObj
			},
			"cibleDDPrincipal" : {
				view : "dropdownList",
				type : "filters",
				field : "answers.aapStep2.caracter.cibleDDPrincipal",
				name : "Objectif primaire",
				event : "filters",
				keyValue : false,
				list : objectivesObj
			},
			"cibleDDSecondaire" : {
				view : "dropdownList",
				type : "filters",
				field : "answers.aapStep2.caracter.cibleDDSecondaire",
				name : "Objectif secondaire",
				event : "filters",
				keyValue : false,
				list : objectivesObj
			},
			theme : {
	 			view : "megaMenuDropdown",
	 			type : "filters",
				field : "answers.aapStep1.tags",
	 			remove0: false,
	 			name : "<?php echo Yii::t("common", "search by theme")?>",
	 			event : "filters",
	 			list : thematicAap
	 		},
			acceptation : {
				view : "dropdownList",
				type : "filters",
				field : "acceptation",
				name : "Retenu ou Rejeté",
				event : "filters",
				keyValue : false,
				list : {
					"retained" : "Retenu",
					"rejected" : "Rejeté",
					"pending" : "En attente"
				}
			},
			urgency : {
				view : "dropdownList",
				type : "filters",
				field : "answers.aapStep1.multiCheckboxPlusurgency",
				name : "Urgence",
				event : "exists",
				typeList : "object",
				action : "filters",
				list :  {
					"Urgent": {
						"label" : "Urgent",
						"value" : true
					}
				}
			},
			// tags : {
			// 	view : "dropdownList",
			// 	type : "filters",
			// 	field : "answers.aapStep1.tags",
			// 	name : "Urgence",
			// 	event : "filters",
			// 	keyValue : false,
			// 	list : {
			// 		"Urgent" : "true"
			// 	}
			// },
            status : {
                view : "dropdownList",
                type : "filters",
                field : "status",
                name : "status",
                event : "filters",
                keyValue : false,
                list : {
                    "progress" : "En cours",
                    "vote" : "En evaluation",
                    "finance" : "En financement",
                    "call" : "Appel à participation",
                    "newaction" : "nouvelle proposition",
                    "projectstate" : "En projet",
                    "finish" : "Términé",
                    "suspend" : "Suspendu"
                }
            },
			sortBy : {
                view : "dropdownList",
				type : "sortBy",
				name : trad.sortBy,
				event : "sortBy",
                list : {
					// "nom croissant" : {
					// 	"answers.aapStep1.titre" : -1,
					// },
					// "nom décroissant" : {
					// 	"answers.aapStep1.titre" : 1,
					// },
					"Date de création croissant" : {
						"created" : 1,
					},
					"Date de création décroissant" : {
						"created" : -1,
					},
					"Date de mise à jour croissant" : {
						"updated" : 1,
					},
					"Date de mise à jour décroissant" : {
						"updated" : -1,
					}					
                }
            },
		},
		defaults:{
			notSourceKey : true,
			indexStep: "21",
			types:["answers"],
			//forced: {
			filters:{
				'form' : elform,
				'answers' : {'$exists':true},
				'status':{'$not':"finish"},
			},
			//},
			sortBy:{"updated":-1}
			//fields : [],
		},

		header:{
			dom : ".headerSearchContainer",
			options:{
				left:{
					classes :"col-xs-12 no-padding",
					group:{
					count : true,
					map : true
				}
				}
			},
			views : {
				map : function(fObj,v){
				/*return  '<button class="btn-show-map-search pull-right" style="" title="'+trad.showmap+'" alt="'+trad.showmap+'">'+
							'<i class="fa fa-map-marker"></i> '+trad.map+
						'</button>';*/
						return "";
			}
		},
		events : {
			map : function(fObj){
				$(".btn-show-map-search").off().on("click", function(){
					$("#mapOfResultsAnsw").css({"left":"0px", "right":"0px", "bottom":"50px"});
					$("#mapOfResultsAnsw").show(200);
					$(".footerSearchContainer").addClass("affix");
					if($el_configform.showMap)
						mapAnsw.map.invalidateSize();
					$("body").css({"overflow":"hidden"});
					$("#mapOfResultsAnsw .btn-hide-map").off().on("click", function(){
						$("#mapOfResultsAnsw").hide(200);
						$("body").css({"overflow":"inherit"});
						$(".footerSearchContainer").removeClass("affix");	
					});

				});
			}
		}
		},
		results :{
			map : {
				showMapWithDirectory :true
			}
		}
	};
	if(isOcecoform){
		delete paramsFilter.filters.actionPrincipal;
		delete paramsFilter.filters.actionSecondaire;
		delete paramsFilter.filters.cibleDDPrincipal;
		delete paramsFilter.filters.cibleDDSecondaire;
	}
	if(tagsValue.length !=0){
		paramsFilter.defaults.forced.filters["answers.aapStep1.tags"] = {'$in' : tagsValue}
	}
	if($el_configform.showMap)
		paramsFilter["mapCo"]= mapAnsw;

	var filterSearch={};

	$(function(){
		filterSearch = searchObj.init(paramsFilter);
		filterSearch.header.set=function(fObj){
			if(exists($el_form) && $el_form != null && typeof $el_form.endDate != "undefined"){
				var formattedArrDays = $el_form.endDate.split("/");
				var formatData = formattedArrDays[2]+'-'+formattedArrDays[1]+'-'+formattedArrDays[0];
				formatData = new Date(formatData).getTime() / 1000; //secs
				var nowDate = new Date().getTime() / 1000;
				var endDate = '<span class="letter-green">| Fin de la session : '+$el_form.endDate+'</span>';
				if(nowDate > formatData){
					endDate = '<span class="text-red">| Session éxpiré le : '+$el_form.endDate+'</span>';
				}
			}
		
			//var epoch = new Date(formattedDays[2], formattedDays[1] - 1, formattedDays[0]).getSeconds();
			var resultsStr = (fObj.results.numberOfResults > 1) ? " dossiers" : "dossier";
			if(typeof costum.hasRoles != "undefined" && $.inArray("Opérateur", costum.hasRoles)>=0  && !costum.isCostumAdmin)
				resultsStr+= " en attente d'opérateur";
			$(fObj.header.dom+" .countResults").html('<i class=\'fa fa-angle-down\'></i> ' + fObj.results.numberOfResults + ' '+resultsStr+' '+endDate);
		};

		filterSearch.results.render=function(fObj, results, data){
			coInterface.showLoader(fObj.results.dom);
			// resultsArray = [];
			// $.each(results,(k,v)=>{
			// 	resultsArray.push(v)
			// })
			// resultsArray = resultsArray.sort((a,b) => (a.created < b.created) ? 1 : ((b.created < a.created) ? -1 : 0));
			// mylog.log(resultsArray,"govalos");
			if(Object.keys(results).length > 0){
				var params = 
					{	
						form:data.form,
						forms:data.forms,
						el: $el,
						what: data.what,
						canEdit: data.canEdit,
						slug : $slug,
						el_slug : $el_slug,
                        el_configform: $el_configform,
                        elform: $el_form,
                        el_form_id : $el_form_id,
						view: (localStorage.getItem("view") != null ? localStorage.getItem("view") : "detailed"),
						allAnswers:results,
						countNewProposition : $countNewProposition
					};
					ajaxPost(fObj.results.dom, baseUrl+"/survey/answer/views/tpl/costum.views.custom.appelAProjet.callForProjects.listItem", 
					params,
					function(){
						fObj.results.events(fObj);
					},null,null,
					{
						beforeSend : () => $(fObj.results.dom).html('<p class="text-center"><i class="fa fa-3x fa-spinner fa-spin"></i></p>'),
						async:false,
						xhr: function () {
							var xhr = $.ajaxSettings.xhr();
							xhr.onprogress = function (e) {
								// For downloads
								if (e.lengthComputable) {
									console.log("andram",e.loaded / e.total);
								}
							};
							xhr.upload.onprogress = function (e) {
								// For uploads
								if (e.lengthComputable) {
									console.log(e.loaded / e.total);
								}
							};
							return xhr;
						}
					});
			}else 
				$(fObj.results.dom).append(fObj.results.end(fObj));
			fObj.results.events(fObj);
			fObj.results.addToMap(fObj, results);
		};

		filterSearch.results.addToMap = function(fObj, results){
			mylog.log("resultss",results);
			var elts = [];

			$.each(results, function(k, result){
				if(result.answers){
					$.each(result.answers, function(k, answer){
						if(answer.adress && answer.adress.geo)
						elts.push(
							{
								geo: answer.adress.geo,
								name: answer.proposition,
							}
						)
					})
				}
			})
			mylog.log("eltss",elts);
			if($el_configform.showMap)
				mapAnsw.addElts(elts);
		};
		filterSearch.search.init(filterSearch);

		if($el_configform.showMap){
			if(localStorage.getItem("showAapMap")=="true"){
				$(".btn-show-hide-map").text("Cacher la carte");
				$('.cx12').removeClass("container").addClass("col-xs-12");
				$('.cfm').removeClass("container").addClass("container-fluid");
				$('.cm8').removeClass("col-md-12").addClass("col-md-8")
			}else{
				$(".btn-show-hide-map").text("Afficher la carte");
				$('.cx12').removeClass("col-xs-12").addClass("container");
				$('.cfm').removeClass("container-fluid").addClass("container");
				$('.cm8').removeClass("col-md-8").addClass("col-md-12");
				$("#mapOfResultsAnsw").hide();				
			}
		}
		$(".btn-show-hide-map").off().on("click",function(){
			$btn = $(this);
			if(localStorage.getItem("showAapMap")=="true"){
				localStorage.setItem("showAapMap", false);
				$("#mapOfResultsAnsw").hide();
				$(".btn-show-hide-map").text("Afficher la carte");
				$('.cx12').removeClass("col-xs-12").addClass("container");
				$('.cfm').removeClass("container-fluid").addClass("container");
				$('.cm8').removeClass("col-md-8").addClass("col-md-12");
			}else{
				$(".btn-show-hide-map").text("Cacher la carte");
				localStorage.setItem("showAapMap", true);
				$("#mapOfResultsAnsw").show();
				$('.cx12').removeClass("container").addClass("col-xs-12");
				$('.cfm').removeClass("container").addClass("container-fluid");
				$('.cm8').removeClass("col-md-12").addClass("col-md-8");
			}
		})

        $('.deleteanswer').on('click', function() {
            var itemId = $(this).data("ansid");

            id = $(this).data("ansid");
            bootbox.dialog({
                title: "Confirmez la suppression",
                message: "<span class='text-red bold'><i class='fa fa-warning'></i> Cette action sera irréversible</span>",
                buttons: [
                    {
                        label: "Ok",
                        className: "btn btn-primary pull-left",
                        callback: function() {
                            getAjax("", baseUrl+"/survey/co/delete/id/"+itemId , function(){
                                urlCtrl.loadByHash(location.hash);
                                //$('.survey-item[data-answid="'+itemId+'"]').hide();
                            },"html");
                        }
                    },
                    {
                        label: "Annuler",
                        className: "btn btn-default pull-left",
                        callback: function() {}
                    }
                ]
            });
        });
		$('.minimalaap').off().on("click", function () {
			localStorage.setItem("view","minimalist");
			urlCtrl.loadByHash(location.hash);
        });

        $('.detaaap').off().on("click", function () {
			localStorage.setItem("view","detailed");
			urlCtrl.loadByHash(location.hash);
        });
		$('.compact').off().on("click", function () {
			localStorage.setItem("view","compact");
			urlCtrl.loadByHash(location.hash);
        });
		if(localStorage.getItem("view")=="minimalist") $('.minimalaap').css("color","#7da53d");
		else if(localStorage.getItem("view")=="detailed") $('.detaaap').css("color","#7da53d");
		else if(localStorage.getItem("view")=="compact") $('.compact').css("color","#7da53d");

		$('.searchBarInMenu.pull-right').addClass('showHide-filters-xs-aap').html('<h6> Filtres <i class="fa fa-caret-down"></i></h6>');
		$('.showHide-filters-xs-aap').off().on('click', function() {
				if($(".menu-filters-xs").is(":visible"))	
					$(this).find("i").removeClass("fa-times").addClass("fa-filter");
				else
					$(this).find("i").removeClass("fa-filter").addClass("fa-times");
			    $(".menu-filters-xs").slideToggle(500);
			});
		//document.querySelector('.searchBarInMenu.pull-right').appendChild(document.querySelector('.searchBar-filters.pull-left'));
		//<input type="text" class="form-control pull-left text-center main-search-bar central-search-bar" id="main-search-bar" placeholder="Que recherchez-vous ?">
		
	})
</script>