<style type="text/css">
    @media only screen and (min-width: 992px) {
        .aappageform {

        }
    }

    .single_evaluate img {
        min-height: 0;
    }
</style>

<?php
if(!empty($answerid) && !empty($answers[$answerid])){
$answer = $answers[$answerid];

?>

    <div id="sticky-anchor"></div>
    <div id="sticky">
        <div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
            <ul class="breadcrumb">
                <li ><a><?php if (isset($el["el"]["name"])) { echo $el["el"]["name"]; } ?></a></li>
                <li class="active"><a href="javascript:;" class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.list" ><?php if (isset($elform["name"])) { echo $elform["name"]; } ?> </a></li>
                <!-- <li ><a><?php if (isset($elform["name"])) { echo $elform["name"]; } ?></a></li> -->
                <li class="active"><a href="javascript:;" class="btnaapp_link" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.details ">Observatoire : <?php echo @$answer["answers"]["aapStep1"]["titre"] ?></a></li>
            </ul>
        </div>

        <div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
            <div class="aapconfigdiv">
                <div class="text-center">
                    <button type="button" class="aapgoto aap-breadrumbbtn" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.form.answer.<?php echo (string)$answer["_id"]; ?>"><i class="fa fa-plus-square-o"></i> Editer proposition</button>
                    <button type="button"  class="aapgoto aap-breadrumbbtn" data-url="<?php echo Yii::app()->createUrl("/costum")?>/co/index/slug/<?php echo @$slug; ?>#welcome.slug.<?php echo $el_slug; ?>.formid.<?php echo $el_form_id ?>.page.sheet.answer.<?php echo (string)$answer["_id"] ?>"> <i class="fa fa-edit"></i> Résumé de la proposition </button>
                </div>
            </div>
        </div>

    </div>

    <div class="col-xs-12 col-lg-10 col-lg-offset-1">

        <div class="single_evaluate">
        <div class="row no-margin">
            <div class="col-xs-12 col-sm-7 col-md-7 padding-15">
                <div class="evaluate_content">
                    <div class="row">
                        <div class="col-xs-7">
                            <h4> <?php echo @$answer["answers"]["aapStep1"]["titre"] ?> <br>
                                <small>Déposé par <b><i><?= @$user["name"] ?></i></b></small>
                            </h4>
                        </div>
                    </div>
                    <p>
                        <?php echo !empty(@$answer["answers"]["aapStep1"]["opalProcess41"]) ? @$answer["answers"]["aapStep1"]["opalProcess41"] : @$answer["answers"]["aapStep1"]["description"]   ?>
                    </p>
                    <div class="btn-component">
                        <!--<button class="btn btn-default btn-evaluate">
                            Evaluer
                        </button>-->
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5 no-padding">
                <!--                <img class="img-responsive" src="<?php /*echo $this->module->assetsUrl */?>/images/thumbnail-default.jpg">
-->                <!--                <img class="img-responsive" src="http://www.place-to-be.fr/wp-content/uploads/2014/11/meilleur-whisky-du-monde-japonais-1050x656.jpg" alt="burger">-->
                <?php
                $initAnswerFiles=Document::getListDocumentsWhere(array(
                    "id"=>(string)$answer["_id"],
                    "type"=>'answers',
                    "subKey"=>"aapStep1.image"), "image");


                //echo $this->renderPartial("co2.views.pod.docsList",array("edit"=>false, "documents"=>$initAnswerFiles,"docType"=>"image") );
                if(!empty($initAnswerFiles)){
                    foreach ($initAnswerFiles as $key => $d) {

                        ?>

                        <img class="img-responsive" src="<?php echo $d["imageThumbPath"] ?>">

                        <?php
                    }
                } else {
                    ?>
                    <img class="img-responsive" src="<?php echo $this->module->assetsUrl ?>/images/thumbnail-default.jpg">
                    <?php

                } ?>

            </div>

        </div>
    </div>

    </div>

    <div class="aappageform" id="aapformcont"></div>

<script type="text/javascript">

    var answerid =  "<?php echo (!empty($answerid) ?$answerid : ''); ?>";

    var el_form = "<?php echo (isset($elform["_id"]) ? (string)$elform["_id"] : ''); ?>";

    $(document).ready(function() {
        ajaxPost("#aapformcont", baseUrl+'/survey/answer/ocecoformdashboard/answerid/'+answerid,
            null,
            function(data){

            },"html");
    });

</script>

    <?php
}
?>