<?php 
    HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/jkanban.css","/css/aap/kanban.css","/js/blockcms/jkanban.js"], Yii::app()->getModule('costum')->assetsUrl);
?>
<div class="col-xs-12">
    <div class="kanban-container">
        <h1>Projects</h1>
        <div id="myKanban1">
            <div></div>
        </div>
        <div id="myKanban"></div>
        <button id="addDefault">Add "Default" board</button>
        <br />
        <button id="addToDo">Add element in "To Do" Board</button>
        <br />
        <button id="addToDoAtPosition">Add element in "To Do" Board at position 2</button>
        <br />
        <button id="removeBoard">Remove "Done" Board</button>
        <br />
        <button id="removeElement">Remove "My Task Test"</button>
    </div>
</div>
<script>
    $(function(){
        var allAns = <?= json_encode($allAnswers) ?>;
        var elSlug = <?= json_encode($elSlug) ?>;
        var elFormId = <?= json_encode($elFormId) ?>;
        var kanbanProjectObj = {
            proposeItems : [],
            evaluationItems:[],
            financeItems:[],
            progressItems:[],
            finishedItems:[],
            suspendedItems:[],
            init: function(kanObj){
                kanbanProjectObj.initItems(kanbanProjectObj);
                kanbanProjectObj.initKanban(kanbanProjectObj);
                kanbanProjectObj.events(kanbanProjectObj);
            },
            initItems : function(kanObj){
                var proposeItem = [];
                $.each(allAns,(k,v)=>{
                    if(exists(v.answers) ){
                        mylog.log("prepareBoard",v.answers.aapStep1);
                        if(typeof v.status == "undefined" || (exists(v.status) && v.status.length == 0)){
                            kanObj.proposeItems.push(
                                {
                                    id: k,
                                    title: v.answers.aapStep1.titre,
                                    drag: function(el, source) {
                                        console.log("START DRAG: " + el.dataset.eid)
                                    },
                                    dragend: function(el) {
                                        //console.log("rakoto",el.dataset);
                                    },
                                    drop: function(el) {
                                        console.log("DROPPED: " + el.dataset.eid);
                                    },
                                    click: function(el) {
                                       // alert("click"+v.answers.aapStep1.titre);
                                    },
                                    context: function(el, e){
                                        console.log("rakoto",el);
                                        alert("right-click at (" + `${e.pageX}` + "," + `${e.pageX}` + ")")
                                    },
                                    class: ["cursor-pointer","aapgetaapview"],
                                    url: 'slug.'+elSlug+'.formid.'+elFormId+'.page.sheet.answer.'+k
                                }
                            )
                        }else if(v.status.indexOf("suspend") != -1 ){
                            kanObj.suspendedItems.push(
                                {
                                    id: k,
                                    title: v.answers.aapStep1.titre,
                                    drag: function(el, source) {
                                        console.log("START DRAG: " + el.dataset.eid);
                                    },
                                    dragend: function(el) {
                                        console.log("END DRAG: " + el.dataset.eid);
                                    },
                                    drop: function(el) {
                                        console.log("DROPPED: " + el.dataset.eid);
                                    },
                                    click: function(el) {
                                       // alert("click"+v.answers.aapStep1.titre);
                                    },
                                    context: function(el, e){
                                        //alert("right-click at (" + `${e.pageX}` + "," + `${e.pageX}` + ")")
                                    },
                                    class: ["cursor-pointer"],
                                    url: 'slug.'+elSlug+'.formid.'+elFormId+'.page.sheet.answer.'+k
                                }
                            )
                        }
                        else if(v.status.indexOf("finish") != -1 ){
                            kanObj.finishedItems.push(
                                {
                                    id: k,
                                    title: v.answers.aapStep1.titre,
                                    drag: function(el, source) {
                                        console.log("START DRAG: " + el.dataset.eid);
                                    },
                                    dragend: function(el) {
                                        console.log("END DRAG: " + el.dataset.eid);
                                    },
                                    drop: function(el) {
                                        console.log("DROPPED: " + el.dataset.eid);
                                    },
                                    click: function(el) {
                                       // alert("click"+v.answers.aapStep1.titre);
                                    },
                                    context: function(el, e){
                                        //alert("right-click at (" + `${e.pageX}` + "," + `${e.pageX}` + ")")
                                    },
                                    class: ["cursor-pointer"],
                                    url: 'slug.'+elSlug+'.formid.'+elFormId+'.page.sheet.answer.'+k
                                }
                            )
                        }else if(v.status.indexOf("progress") != -1){
                            kanObj.progressItems.push(
                                {
                                    id: k,
                                    title: v.answers.aapStep1.titre,
                                    drag: function(el, source) {
                                        console.log("START DRAG: " + el.dataset.eid);
                                    },
                                    dragend: function(el) {
                                        console.log("END DRAG: " + el.dataset.eid);
                                    },
                                    drop: function(el) {
                                        console.log("DROPPED: " + el.dataset.eid);
                                    },
                                    click: function(el) {
                                       // alert("click"+v.answers.aapStep1.titre);
                                    },
                                    context: function(el, e){
                                        //alert("right-click at (" + `${e.pageX}` + "," + `${e.pageX}` + ")")
                                    },
                                    class: ["cursor-pointer"],
                                    url: 'slug.'+elSlug+'.formid.'+elFormId+'.page.sheet.answer.'+k
                                }
                            )
                        }else if(v.status.indexOf("finance") != -1){
                            kanObj.financeItems.push(
                                {
                                    id: k,
                                    title: v.answers.aapStep1.titre,
                                    drag: function(el, source) {
                                        console.log("START DRAG: " + el.dataset.eid);
                                    },
                                    dragend: function(el) {
                                        console.log("END DRAG: " + el.dataset.eid);
                                    },
                                    drop: function(el) {
                                        console.log("DROPPED: " + el.dataset.eid);
                                    },
                                    click: function(el) {
                                       // alert("click"+v.answers.aapStep1.titre);
                                    },
                                    context: function(el, e){
                                        //alert("right-click at (" + `${e.pageX}` + "," + `${e.pageX}` + ")")
                                    },
                                    class: ["cursor-pointer"],
                                    url: 'slug.'+elSlug+'.formid.'+elFormId+'.page.sheet.answer.'+k
                                }
                            )
                        }else if(v.status.indexOf("vote") != -1){
                            kanObj.evaluationItems.push(
                                {
                                    id: k,
                                    title: v.answers.aapStep1.titre,
                                    drag: function(el, source) {
                                        console.log("START DRAG: " + el.dataset.eid);
                                    },
                                    dragend: function(el) {
                                        console.log("END DRAG: " + el.dataset.eid);
                                    },
                                    drop: function(el) {
                                        console.log("DROPPED: " + el.dataset.eid);
                                    },
                                    click: function(el) {
                                       // alert("click"+v.answers.aapStep1.titre);
                                    },
                                    context: function(el, e){
                                        //alert("right-click at (" + `${e.pageX}` + "," + `${e.pageX}` + ")")
                                    },
                                    class: ["cursor-pointer"],
                                    url: 'slug.'+elSlug+'.formid.'+elFormId+'.page.sheet.answer.'+k
                                }
                            )
                        }
                    } 
                })
            },
            events : function(kanObj){
                var kanbanContainerWidth= $("#myKanban .kanban-container" ).width()
                $("#myKanban1 div").css("width",kanbanContainerWidth);
                $("#myKanban1").scroll(function(){
                    $("#myKanban").scrollLeft($("#myKanban1").scrollLeft());
                });
                $("#myKanban").scroll(function(){
                    $("#myKanban1").scrollLeft($("#myKanban").scrollLeft());
                });

                $('.aapgetaapview').off().on('click',function(e){
                    e.stopPropagation();
                    coInterface.showLoader(".aapinputcontent");
                    var ansurl = $(this).data('url');
                    smallMenu.openAjaxHTML(baseUrl+'/survey/form/getaapview/urll/'+ansurl);
                });
                // $.getScript( "https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.0/slick.js", function( data, textStatus, jqxhr ) {
                    
                // })
            },
            initKanban: function(kanbanProjectObj){
                var KanbanTest = new jKanban({
                    element: "#myKanban",
                    gutter: "10px",
                    widthBoard: "300px",
                    itemHandleOptions:{
                        enabled: true,
                    },
                    click: function(el) {
                        console.log("Trigger on all items click!");
                    },
                    context: function(el, e) {
                        console.log("Trigger on all items right-click!");
                    },
                    dropEl: function(el, target, source, sibling){
                        console.log(target.parentElement.getAttribute('data-id'));
                        console.log(el, target, source, sibling)
                    },
                    buttonClick: function(el, boardId) {
                        console.log(el);
                        console.log(boardId);
                        // create a form to enter element
                        var formItem = document.createElement("form");
                        formItem.setAttribute("class", "itemform");
                        formItem.innerHTML =
                        '<div class="form-group"><textarea class="form-control" rows="2" autofocus></textarea></div><div class="form-group"><button type="submit" class="btn btn-primary btn-xs pull-right">Submit</button><button type="button" id="CancelBtn" class="btn btn-default btn-xs pull-right">Cancel</button></div>';

                        KanbanTest.addForm(boardId, formItem);
                        formItem.addEventListener("submit", function(e) {
                        e.preventDefault();
                        var text = e.target[0].value;
                        KanbanTest.addElement(boardId, {
                            title: text
                        });
                        formItem.parentNode.removeChild(formItem);
                        });
                        document.getElementById("CancelBtn").onclick = function() {
                        formItem.parentNode.removeChild(formItem);
                        };
                    },
                    itemAddOptions: {
                        enabled: true,
                        content: '+ Add New Card',
                        class: 'custom-button',
                        footer: true
                    },
                    boards: [
                        {
                            id: "_propose",
                            title: "Proposer",
                            class: "info,good",
                            dragTo: ["_decide"],
                            item: kanbanProjectObj.proposeItems
                        },
                        {
                            id: "_decide",
                            title: "Decider",
                            class: "warning",
                            dragTo: ["_funding","_propose"],
                            item: kanbanProjectObj.evaluationItems
                        },
                        {
                            id: "_funding",
                            title: "Financement",
                            class: "dark",
                            dragTo: ["_decide","_running"],
                            item: kanbanProjectObj.financeItems
                        },
                        {
                            id: "_running",
                            title: "En cours",
                            class: "info",
                            dragTo: ["_finished","_funding"],
                            item: kanbanProjectObj.progressItems
                        },
                        {
                            id: "_finished",
                            title: "Terminé",
                            class: "success",
                            dragTo: ["_running"],
                            item: kanbanProjectObj.finishedItems
                        },
                        {
                            id: "_suspended",
                            title: "Suspendu",  
                            class: "danger",
                            dragTo: [],
                            item: kanbanProjectObj.suspendedItems
                        }
                    ]
                });

                var toDoButton = document.getElementById("addToDo");
                toDoButton.addEventListener("click", function() {
                    KanbanTest.addElement("_propose", {
                        title: "Test Add"
                    });
                });

                var toDoButtonAtPosition = document.getElementById("addToDoAtPosition");
                toDoButtonAtPosition.addEventListener("click", function() {
                    KanbanTest.addElement("_propose", {
                        title: "Test Add at Pos"
                    }, 1);
                });

                var addBoardDefault = document.getElementById("addDefault");
                addBoardDefault.addEventListener("click", function() {
                    KanbanTest.addBoards([
                        {
                        id: "_default",
                        title: "Kanban Default",
                        item: [
                            {
                            title: "Default Item"
                            },
                            {
                            title: "Default Item 2"
                            },
                            {
                            title: "Default Item 3"
                            }
                        ]
                        }
                    ]);
                });

                var removeBoard = document.getElementById("removeBoard");
                removeBoard.addEventListener("click", function() {
                    KanbanTest.removeBoard("_funding");
                });

                var removeElement = document.getElementById("removeElement");
                removeElement.addEventListener("click", function() {
                    KanbanTest.removeElement("_test_delete");
                });

                var allEle = KanbanTest.getBoardElements("_propose");
                allEle.forEach(function(item, index) {
                //console.log(item);
                });
            }
        }

        //************************************************************************* */
        kanbanProjectObj.init(kanbanProjectObj);
    })
</script>