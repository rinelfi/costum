<?php  
    $cssAnsScriptFilesTheme = array(
        // SHOWDOWN
        '/plugins/showdown/showdown.min.js',
        // MARKDOWN
        '/plugins/to-markdown/to-markdown.js'            
    );

    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

    $cssAndScriptFilesModule = array(
        '/js/default/profilSocial.js',
    );

    HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());


    $colorSectionAgenda = @$this->costum["cms"]["colorSectionAgenda"];
    $colorSectionActu = @$this->costum["cms"]["colorSectionActu"];
    $colorCardEvent = @$this->costum["cms"]["colorCardEvent"];
    $colorPathPlus = @$this->costum["cms"]["colorPathPlus"];
    $colorTitleAgenda = @$this->costum["cms"]["colorTitleAgenda"];
    $colorTitleActu = @$this->costum["cms"]["colorTitleActu"];
    $colorSearchicon = @$this->costum["cms"]["colorSearchicon"];
    $bannerImg = @$this->costum["banner"] ? Yii::app()->baseUrl.$this->costum["banner"] : Yii::app()->getModule("costum")->assetsUrl."/images/hubterritoires/header.jpg";
?>

<style type="text/css">
    .main-container {
        padding-top: 207px !important;
    }

    #newsstream {
        min-height: auto !important;
    }

    .hexagon {
        overflow: hidden;
        visibility: hidden;
       /* -webkit-transform: rotate(120deg);
           -moz-transform: rotate(120deg);
            -ms-transform: rotate(120deg);
             -o-transform: rotate(120deg);
                transform: rotate(120deg);*/
        cursor: pointer;
    }


    .hexagon1 {
        max-height: 260px;
    }

    .hexagon-in1 {
        overflow: hidden;
        width: 100%;
        height: 100%;
        /*-webkit-transform: rotate(-60deg);
           -moz-transform: rotate(-60deg);
            -ms-transform: rotate(-60deg);
             -o-transform: rotate(-60deg);
                transform: rotate(-60deg);*/
    }

    .hexagon-in2 {
        width: 100%;
        height: 100%;
        background-repeat: no-repeat;
        visibility: visible;
        -/*webkit-transform: rotate(-60deg);
           -moz-transform: rotate(-60deg);
            -ms-transform: rotate(-60deg);
             -o-transform: rotate(-60deg);
                transform: rotate(-60deg);*/
    }

    .img-hexa{
        /*margin-top: -25%;*/
    }

    .info-card{
        border-radius: 15px;
        background: <?php echo $colorCardEvent ?> ;
        padding: 3%;
        font-size: 25px;
        color : white;
        box-shadow: 4px 3px 7px 3px #dadada;
        margin-left: 13%;
        margin-right: 13%;
    }

    #dropdown #footerDropdownGS #btnShowMoreResultGS {
        display: none;
    }
</style>

<center>
	<img class="img-responsive ultra" src=" <?= @$this->costum["logo"]; ?>">
    
    <div class="col-lg-12" id="searchBar" style="margin-left: 18vw;">
        <a data-type="filters" href="javascript:;">
                <span id="second-search-bar-addon-mednum" class="text-white input-group-addon pull-left main-search-bar-addon-mednum">
                    <i style="color:<?php echo $colorSearchicon ?>" class="fa fa-search searchIcone"></i>
                </span>
            </a>
        <input type="text" class="form-control pull-left text-center main-search-bars" id="second-search-bar" placeholder="Taper et valider la recherche">
    </div> 

    <div id="dropdown" class="dropdown-result-global-searchbar content-result col-sm-5 col-md-5 col-lg-5 no-padding">
    </div>

    <p class="txtHeader text-center col-lg-12 col-xs-12" style="text-shadow: black 0px 0px 4px;">Le réseau des acteurs ultramarins de la </br> MÉDIATION NUMÉRIQUE </p>
	<img style="margin-top: -3.3vw;width: 100%" class="img-responsive" src='<?php echo $bannerImg ?>'>
</center>

<div style="margin-top: 2.4vw;background-color: white" class="col-xs-12 no-padding" >
    <div style="margin-top: 7vw;">
        <div class="contain">
            <div id="containerCard" class="col-md-4 col-sm-4 col-xs-12">
                <center>
                    <h2 class="title_card" style="margin-top: 1vw;color:#172881">Projets</h2>
                    <img class="img-responsive logoApp" src="<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>/images/hubterritoires/logo-fusee.svg"><br>
                    <a href="javascript:;" data-hash="#projects" class="lbh-menu-app">
                        <svg class="plus" style="border-radius: 38px;box-shadow: 0px 0px 8px gray;margin-top: 1.5vw;" fill="#000000" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 128" width="64px" height="64px">
                            <path fill="white" d="M105,23C105,23,105,23,105,23C82.4,0.4,45.6,0.4,23,23C0.4,45.6,0.4,82.4,23,105c11.3,11.3,26.2,17,41,17s29.7-5.7,41-17C127.6,82.4,127.6,45.6,105,23z M100.8,100.8c-20.3,20.3-53.3,20.3-73.5,0C7,80.5,7,47.5,27.2,27.2C37.4,17.1,50.7,12,64,12s26.6,5.1,36.8,15.2C121,47.5,121,80.5,100.8,100.8z"/>
                            <path fill="<?php echo $colorPathPlus ?>" d="M83,61H67V45c0-1.7-1.3-3-3-3s-3,1.3-3,3v16H45c-1.7,0-3,1.3-3,3s1.3,3,3,3h16v16c0,1.7,1.3,3,3,3s3-1.3,3-3V67h16c1.7,0,3-1.3,3-3S84.7,61,83,61z"/>
                        </svg>
                    </a>
                </center>
            </div>
            <div id="containerCard" class="col-md-4 col-sm-4 col-xs-12">
                <center>
                    <h2 class="title_card" style="margin-top: 1vw;color:#172881">Participants</h2>
                    <img class="img-responsive logoApp" src="<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>/images/hubterritoires/logo-participants.svg"><br>
                    <a href="javascript:;" data-hash="#annuaire" class="lbh-menu-app">
                        <svg class="plus" style="border-radius: 38px;box-shadow: 0px 0px 8px gray;margin-top: 3.5vw;" fill="#000000" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 128" width="64px" height="64px">
                            <path fill="white" d="M105,23C105,23,105,23,105,23C82.4,0.4,45.6,0.4,23,23C0.4,45.6,0.4,82.4,23,105c11.3,11.3,26.2,17,41,17s29.7-5.7,41-17C127.6,82.4,127.6,45.6,105,23z M100.8,100.8c-20.3,20.3-53.3,20.3-73.5,0C7,80.5,7,47.5,27.2,27.2C37.4,17.1,50.7,12,64,12s26.6,5.1,36.8,15.2C121,47.5,121,80.5,100.8,100.8z"/>
                            <path fill="<?php echo $colorPathPlus ?>" d="M83,61H67V45c0-1.7-1.3-3-3-3s-3,1.3-3,3v16H45c-1.7,0-3,1.3-3,3s1.3,3,3,3h16v16c0,1.7,1.3,3,3,3s3-1.3,3-3V67h16c1.7,0,3-1.3,3-3S84.7,61,83,61z"/>
                        </svg>
                    </a>
                </center>
            </div>
            <div id="containerCard" class="col-md-4 col-sm-4 col-xs-12">
                <center>
                    <h2 class="title_card" style="margin-top: 1vw;color:#172881">Formations</h2>
                    <img class="img-responsive logoApp" src="<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>/images/hubterritoires/logo-formation.svg"><br>
                    <a href="javascript:;" data-hash="#jobs" class="lbh-menu-app">
                        <svg class="plus" style="border-radius: 38px;box-shadow: 0px 0px 8px gray;margin-top: 2.9vw;" fill="#000000" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 128" width="64px" height="64px">
                            <path fill="white" d="M105,23C105,23,105,23,105,23C82.4,0.4,45.6,0.4,23,23C0.4,45.6,0.4,82.4,23,105c11.3,11.3,26.2,17,41,17s29.7-5.7,41-17C127.6,82.4,127.6,45.6,105,23z M100.8,100.8c-20.3,20.3-53.3,20.3-73.5,0C7,80.5,7,47.5,27.2,27.2C37.4,17.1,50.7,12,64,12s26.6,5.1,36.8,15.2C121,47.5,121,80.5,100.8,100.8z"/>
                            <path fill="<?php echo $colorPathPlus ?>" d="M83,61H67V45c0-1.7-1.3-3-3-3s-3,1.3-3,3v16H45c-1.7,0-3,1.3-3,3s1.3,3,3,3h16v16c0,1.7,1.3,3,3,3s3-1.3,3-3V67h16c1.7,0,3-1.3,3-3S84.7,61,83,61z"/>
                        </svg>
                    </a>
                </center>
            </div>
        </div>
    </div>
</div>

<div class="col-xs-12 no-padding" style="margin-top: 30px;background-color: #fafafa;">
	<div class="">
	    <div style="background-color:<?php echo $colorSectionActu ?>;" class="explication-title col-xs-12 col-sm-12 col-lg-12">
	        <div>
	            <h1 style="color: <?php echo $colorTitleActu ?>" class="titleBandeau"><i class="fa fa-newspaper-o" aria-hidden="true"></i>  Actualité</h1>
	        </div>
	    </div>
	    <!-- NEWS -->
	    <div style="margin-top: 5vw;background-color: white" id="newsstream" class="col-xs-offset-1 col-xs-10">
	        <div style="background-color: white;">
	        </div>
	    </div>

	    <div style="margin-top: 3vw;margin-bottom: 2vw;" class="text-center container col-lg-12 col-sm-12 col-xs-12">
	        <a href="javascript:;" data-hash="#live" class="lbh-menu-app btn btn-redirect-home btn-small-orange">
                <svg style="border-radius: 38px;box-shadow: 0px 0px 8px gray;" fill="#000000" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 128" width="64px" height="64px">
                    <path fill="transparent" d="M105,23C105,23,105,23,105,23C82.4,0.4,45.6,0.4,23,23C0.4,45.6,0.4,82.4,23,105c11.3,11.3,26.2,17,41,17s29.7-5.7,41-17C127.6,82.4,127.6,45.6,105,23z M100.8,100.8c-20.3,20.3-53.3,20.3-73.5,0C7,80.5,7,47.5,27.2,27.2C37.4,17.1,50.7,12,64,12s26.6,5.1,36.8,15.2C121,47.5,121,80.5,100.8,100.8z"/>
                    <path fill="<?php echo $colorPathPlus ?>" d="M83,61H67V45c0-1.7-1.3-3-3-3s-3,1.3-3,3v16H45c-1.7,0-3,1.3-3,3s1.3,3,3,3h16v16c0,1.7,1.3,3,3,3s3-1.3,3-3V67h16c1.7,0,3-1.3,3-3S84.7,61,83,61z"/>
                </svg>
            </a>
	    </div>
	</div>
</div>

<div class="col-xs-12 no-padding"  style="background-color: white;">
	<div class="explicationAgenda row">
	    <div style="background-color:<?php echo $colorSectionAgenda ?>" class="explication-title">
	        <div>
	            <h1 style="color:<?php echo $colorTitleAgenda ?>" class="titleBandeau"><i class="fa fa-calendar" aria-hidden="true"></i>  Agenda</h1>
	        </div>
	    </div>

        <div style="margin-top: 7vw;" id="containEvent" class="col-lg-12 col-xs-12">
            
        </div>

	    <div style="margin-top: 3vw;" class="text-center container col-lg-12 col-sm-12 col-xs-12">
	        <a href="javascript:;" data-hash="#agenda" class="lbh-menu-app btn btn-redirect-home btn-small-orange">
                <svg style="border-radius: 38px;box-shadow: 0px 0px 8px gray;" fill="#000000" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 128 128" width="64px" height="64px">
                    <path fill="transparent" d="M105,23C105,23,105,23,105,23C82.4,0.4,45.6,0.4,23,23C0.4,45.6,0.4,82.4,23,105c11.3,11.3,26.2,17,41,17s29.7-5.7,41-17C127.6,82.4,127.6,45.6,105,23z M100.8,100.8c-20.3,20.3-53.3,20.3-73.5,0C7,80.5,7,47.5,27.2,27.2C37.4,17.1,50.7,12,64,12s26.6,5.1,36.8,15.2C121,47.5,121,80.5,100.8,100.8z"/>
                    <path fill="<?php echo $colorPathPlus ?>" d="M83,61H67V45c0-1.7-1.3-3-3-3s-3,1.3-3,3v16H45c-1.7,0-3,1.3-3,3s1.3,3,3,3h16v16c0,1.7,1.3,3,3,3s3-1.3,3-3V67h16c1.7,0,3-1.3,3-3S84.7,61,83,61z"/>
                </svg>
            </a> 
	    </div>
	</div>
</div>

<script type="text/javascript">
jQuery(document).ready(function() {
    contextData = {
        id : costum.contextId,
        type : costum.contextType,
        name : costum.title,
        profilThumbImageUrl : "http://127.0.0.1/ph/themes/CO2/assets/img/LOGOS/CO2/logo-min.png"
    };

    news();
    afficheEvent();
});

function news(){

    var urlNews = "news/co/index/type/"+costum.contextType+"/id/"+costum.contextId+"/source/"+costum.contextSlug+"/nbCol/2";

    var paramsLive = {
        indexStep : 10,
        search : true,
        formCreate : true,
        members : false,
        scroll : false
    };

    ajaxPost( "#newsstream",baseUrl+"/"+urlNews,paramsLive, 
    function(news){ 
        mylog.log("success news : ",news);
    }, "html" );
}

function afficheEvent(){
    mylog.log("----------------- Affichage évènement");

    var params = {
       source : costum.contextSlug
    }

    ajaxPost(
        null,
        baseUrl + "/costum/hubterritoires/geteventaction",
        params,
        function (data) {
            mylog.log("success : ",data);
            var str = "";
            var ctr = "";
            var itr = "";
            var url = "<?php echo Yii::app()->getModule('costum')->assetsUrl; ?>" + directory.costum.results.events.defaultImg;
            var ph = "<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>";
            
            var i = 1;
            
            if(data.result == true){
                $(data.element).each(function(key,value){
                    mylog.log("data.element",data.element);
                    
                    var startDate = (typeof value.startDate != "undefined") ? "Du "+value.startDate : null;
                    var hour = moment(value.startHour).format('H');  
                    var minutes = moment(value.startHour).format('mm'); 
                    var startHour = 'À partir de '+hour+'h'+minutes;

                    var imgMedium = (value.imgMedium != "none") ? baseUrl + value.imgMedium : url;
                    var img = (value.img != "none") ? baseUrl + value.img : url;
                    
                    str += '<div style="color:white" class="card">'+
                                '<div id="event-affiche" class="card-color col-md-4 col-sm-9 col-lg-4">'+
                                    '<div id="affichedate" class="info-card text-center">'+
                                        '<div id="afficheImg" class="img-hexa">';
                                    str += '<a class="entityName bold text-dark add2fav  lbh-preview-element" href="#page.type.events.id.'+value.id+'">';
                                    str += '<div class="hexagon hexagon1"><div class="hexagon-in1"><img class="hexagon-in2" src='+imgMedium+'></div></div>';
                                str += '</div>';
                            str += '<span style="color:white;">'+value.name+'</span><br>';
                            str += '</a>';
                            str += startDate+'</br>'+startHour+'</br>'+value.type;
                            str += '</div>'+
                                '</div>'+
                            '</div>';
                });
            }
            else{
                str += "<div class='col-xs-12 col-sm-12 col-md-12'><b class='p-mobile-description' >Aucun évènement n'est prévu</b></div>";
            }
            $("#containEvent").html(str);
            mylog.log(str);
        },
        function (){
            mylog.log("error >>>>>>>");
        }
    );
}
</script>