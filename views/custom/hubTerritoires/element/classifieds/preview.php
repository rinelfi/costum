
<style type="text/css">
	.headerTitleStanalone{
		left:-25px;
		right:-25px;
	}
	.contentOnePage{
		border: 1px solid lightgray;
		margin-top: 25px;
	}

	.textarea-new-comment {
		height: 3em !important;
	}

	.ressourceStandalone h2,
	.ressourceStandalone h4,
	.ressourceStandalone h5,
	.ressourceStandalone h6{
	    text-transform: none;
	}
	.carousel-media > ol > li.active{
	   margin:1px;
	   border-top: 5px solid #ea4335 !important;
	}
	.carousel-media > ol > li{
		width: 60px !important;
	    background-color: inherit;
	    border: inherit !important;
	    height: 65px !important;
	    border-radius: inherit;
	    border-top: 5px solid #666F78 !important;
	}
	
	.carousel-media > ol > li > img{
	   float:left;
	   width:60px;
	   height:60px;
	}
	.carousel-media > ol{
		bottom: -85px;
		left: inherit !important;
	}
	.carousel-media{
		margin-bottom: 100px;
	}
	.informations .btn-social{
		padding: 0px;
	    height: inherit;
	    width: 50px;
	}
	.informations .btn-social > span{
		position: absolute;
    	font-size: 20px;
	}
	.container-txtarea{
		padding-left:5px!important;
	}
	.ctnr-txtarea {
	    left: 45px !important;
	}
	.carousel-indicators{
		bottom: -85px;
		position: absolute;
		left: 0%;
		width: 100%;
		margin-left: 0px;
	}
	hr.hr10{
		margin-bottom: 10px;
		margin-top: 10px;
	}
	hr.top-price {
		border-top: 3px solid #247a8a;
	}

	.portfolio-modal .modal-content{
		text-align: left;
	}


	#modal-preview-coop{
		overflow: auto;
	}
	.contentOnePage div h4 {
	    position: relative;
	    padding: 10px 20px;
	    margin: 1px 0px;
	    margin-left: -15px;
	    margin-right: -15px;
	    line-height: 15px;
	    font-weight: bold;
	    color: #fff;
	    background: #4a667f;
	    text-shadow: 0 1px 1px rgba(0,0,0,0.2);
	    -webkit-box-shadow: 0 1px 1px rgba(0,0,0,0.2);
	    -moz-box-shadow: 0 1px 1px rgba(0,0,0,0.2);
	    box-shadow: 0 1px 1px rgba(0,0,0,0.2);
	    zoom: 1;
	    width: -webkit-fill-available;
	    /*margin-bottom: 10px;*/
	}
	.contentOnePage div h4:after, .contentOnePage div h4:before {
	    content: "";
	    position: absolute;
	    /* z-index: -1; */
	    top: 100%;
	    left: 0;
	    border-width: 0 14px 12px 0;
	    border-style: solid;
	    border-color: transparent #115052;
	}
	.contentOnePage div h4:after {
	    right: 0!important;
	    border-width: 0 0 12px 14px!important;
	    left: auto;
	}
	.content-tags {
		width: 100%;
    	display: inline-flex;
    	justify-content: center;
	}
	.content-tags .label{
		margin: 2px;
	}
	.product-title, .rating, .product-description, .price, .vote, .sizes {
	    margin-bottom: 15px;
	    font-size: 18px;
	}
	.desc-eco {
		font-size: 15px;
		line-height: 25px;
	}
	.pricingTable-header {
	    color: #fff;
	    background: #4a667f;
	    text-align: center;
	    padding: 13px 10px 30px;
	    margin: -20px auto 20px;
	    border-left: 7px solid rgba(0,0,0,0.3);
	    border-right: 7px solid rgba(0,0,0,0.3);
	    /*border-radius: 5px;*/
	    clip-path: polygon(0 0, 100% 0, 100% 75%, 50% 100%, 0 75%);
	    min-width: 30%;
    	width: fit-content;
	}
	.pricingTable-header .title {
	    font-size: 23px;
	    font-weight: 700;
	    text-transform: uppercase;
	    padding: 0 0 10px;
	    margin: 0 0 13px;
	    border-bottom: 2px solid rgba(0,0,0,0.15);
	    color: #b4d7ef;
	}
	.pricingTable-header .price-value {
	    font-size: 35px;
	    font-weight: 700;
	    line-height: 35px;
	    letter-spacing: 2px;
	}
	.addres-prod {
		color: #4a667f;
	}
	@media (max-width: 768px) {
		.pricingTable-header {
			min-width: 70%;
		}
	}
</style>

<div class="ressourceStandalone">
	<div class="headerTitleStanalone col-xs-12 hidden"></div>

	<div class="col-xs-12 col-lg-12">

		<div class="col-xs-12 margin-top-50">
			<?php if(@$element["parent"]["name"]){ ?>
				<span class="letter-<?php echo Element::getColorIcon(@$element["typeClassified"]) ?> font-montserrat">
					<i class="fa fa-angle-down"></i> <i class="fa fa-<?php echo Element::getFaIcon(@$element["typeClassified"]) ?>"></i> 
					<?php echo Yii::t("classified", "{what} published by {who}", 
						array("{what}"=> "Formations",
							"{who}"=>"<a href='#page.type.".@$v["type"].".id.".@$key."' class='lbh'>".
										@$v["name"].
									"</a>")
						);
					?> 
				</span>
			<?php }else if(@$element["parent"]){ 
				foreach($element["parent"] as $key => $v){ ?>
					<span class="letter-<?php echo Element::getColorIcon(@$element["typeClassified"]) ?> font-montserrat">
					<i class="fa fa-angle-down"></i> <i class="fa fa-<?php echo Element::getFaIcon(@$element["typeClassified"]) ?>"></i> 
					<?php echo Yii::t("classified", "{what} published by {who}", 
						array("{what}"=> "Formations",
							"{who}"=>"<a href='#page.type.".@$v["type"].".id.".@$key."' class='lbh'>".
										@$v["name"].
									"</a>")
						);
					?> 
				</span>		
			<?php	}
			} ?>
			<button class="btn btn-default pull-right btn-close-preview" style="margin-top:-15px;">
					<i class="fa fa-times"></i>
			</button>
			<?php 
			if( $element["creator"] == Yii::app()->session["userId"] || Authorisation::canEditItem( Yii::app()->session["userId"], Classified::COLLECTION, $id, @$element["parentType"], @$element["parentId"] ) ){?>
			<button class="btn btn-default pull-right text-red deleteThisBtn" data-type="classifieds" data-id="<?php echo $id ?>" style="margin-top:-15px;">
				<i class=" fa fa-trash"></i>
			</button>
			<button class="btn btn-default pull-right btn-edit-preview" style="margin-top:-15px;">
				<i class="fa fa-pencil"></i>
			</button>
			<?php } ?>
			<br>
			<a href="#page.type.<?php echo @$type; ?>.id.<?php echo (string)@$element["_id"]; ?>" 
				class="letter-green lbh">
				#page.type.<?php echo Classified::COLLECTION ?>.id.<?php echo (string)@$element["_id"]; ?>
			</a>

			<?php if(@$element["section"]){ ?>
				<hr class="hr10">
					<span class="sectionClassified"><?php echo Yii::t("category", @$element["section"]); ?></span> >
				<small> 
					<span class="categoryClassified"><?php echo Yii::t("category", @$element["category"]); ?></span> >
					<span class="subtypeClassified"><?php echo Yii::t("category", @$element["subtype"]); ?></span>
				</small>
			<?php } ?>
			<?php if(@$element["preferences"] && @$element["preferences"]["private"]){ ?> 
				<br>
				<span class="text-red"><i class="fa fa-lock"></i> <?php echo Yii::t("common","private") ?></span>
			<?php } ?>
		</div>

		<div class="col-md-12 col-sm-12 col-xs-12 contentOnePage">
			<div class="col-md-12 no-padding title text-left">
				<h4 class="pull-left"><?php echo ucfirst($element["name"]) ?>
				<?php if(@$element["price"] && @$element["devise"]){ ?>
					<!--<span class="pull-right letter-red">
						<?php echo @$element["price"]." ".@$element["devise"]; ?>
					</span> -->
				<?php } ?>
				</h4>
			</div>
			<?php 
					echo $this->renderPartial('../pod/sliderMedia', 
								array(
									  "medias"=>@$element["medias"],
									  "images" => @$element["images"],
									  ) ); 
									  ?>


			<div class="col-md-12 margin-bottom-20 no-padding ">
				<p class="text-center desc-eco"><?php echo @$element["description"]; ?></p>
				<hr class="top-price">
				<?php if (isset($element["price"])) { ?>
					<div class="pricingTable-header">
	                    <h3 class="title">Prix</h3>
	                    <div class="price-value">
	                        <span><?php echo $element["price"]." ".@$element["devise"]; ?></span>
	                    </div>
	                </div>
				<?php } ?>
				

					<?php if(!empty($element["address"]["addressLocality"])){ ?>
					<h5 class="price text-center"> <i class='fa fa-map-marker'></i> Lieux : <span class="addres-prod">
	        		<?php
						echo !empty($element["address"]["streetAddress"]) ? $element["address"]["streetAddress"].", " : "";
						echo !empty($element["address"]["postalCode"]) ? $element["address"]["postalCode"].", " : "";
						echo $element["address"]["addressLocality"]."<b>";
					?>
					</span></h5>	
					<?php } ?>
				

				<?php if(@$element["contactInfo"]){ ?>
					<h5 class="text-center"><i class="fa fa-user-circle"></i> <b>Contact : </b><?php echo @$element["contactInfo"]; ?></h5>
				<?php } ?>


				<div class="col-xs-12 content-tags">
				<?php if(@$element["tags"]){
					foreach(@$element["tags"] as $tag){ ?>
					<label class="label bg-red">#<?php echo Yii::t("category", @$tag); ?></label>
				<?php } } ?>
				</div>
			</div>
		</div>



		<div class="col-md-12 col-sm-12 col-xs-12 padding-25">
			<?php if(@Yii::app()->session["userId"] && Yii::app()->params['rocketchatEnabled'] )
	  			{
	  				$creator = Person::getById($element["creator"]);
	  	  	?>
			<button class="btn btn-link bg-azure margin-bottom-15" id="btn-private-contact" data-name-el="<?php echo $creator['name']; ?>" data-username="<?php echo @$creator['username']; ?>" data-id="<?php echo $element['creator']; ?>" >
				<i class="fa fa-comments"></i> <?php echo Yii::t("ressources", "Send a private message to the author");  ?>
			</button>
		<?php } ?>
				<?php if(@$element["parent"]["preferences"]["publicFields"]["email"]==true &&
						 @$element["parent"]["email"]!=""){ ?>
					<br>
					<b><i class="fa fa-envelope"></i> Contacter par e-mail : </b><?php echo @$element["parent"]["email"]; ?>
				<?php } ?>

				<?php if(@$element["parent"]["preferences"]["publicFields"]["phone"]==true){ ?><br>
					<b><i class="fa fa-phone"></i> Contacter par téléphone : </b>
					<?php if(@$element["parent"]["telephone"]["fixe"]){ ?>
						<?php echo @$element["parent"]["telephone"]["fixe"][0]; ?>
					<?php } ?>
					<?php if(@$element["parent"]["telephone"]["mobile"] && 
							@$element["parent"]["telephone"]["fixe"]){ ?>
						- 
					<?php } ?>
					<?php if(@$element["parent"]["telephone"]["mobile"]){ ?>
						<?php echo @$element["parent"]["telephone"]["mobile"][0]; ?>
					<?php } ?>
				<?php } ?>
			<hr>
			<h5>
				<i class="fa fa-angle-down"></i> <i class="fa fa-comment"></i> 
				<?php echo Yii::t("comment", "Comments");  ?>
			</h5>

			<div id="commentElement" class="col-xs-12 no-padding margin-top-20"></div>
		</div>
	</div>
</div>

<script type="text/javascript">

	var element= <?php echo json_encode($element); ?>;
	var type = "<?php echo $type; ?>";
	var typeClassified = "<?php echo @$element["typeClassified"]; ?>";

	getAjax("#commentElement",
			baseUrl+"/"+moduleId+"/comment/index/type/classifieds/id/"+element['_id']['$id'],
			function(){}
			,"html");
	
	jQuery(document).ready(function() {	
		if(element.section != "undefined") $(".sectionClassified").text(tradCategory[element.section]);
		if(element.category != "undefined") $(".categoryClassified").text(tradCategory[element.category]);
		if(element.subtype != "undefined") $(".subtypeClassified").text(tradCategory[element.subtype]);			
		var nav = directory.findNextPrev("#page.type."+type+".id."+element['_id']['$id']);

        str =  "<div class='col-md-6 no-padding'>"+ 
			        nav.prev+
			        "<span>"+element.name+"</span>"+
			        nav.next+
			    "</div>";

	    $(".headerTitleStanalone").html(str);

	    directory.bindBtnElement();

	    
		$("#btn-private-contact").click(function(){

	    	var nameElo = $(this).data("name-el");
	    	var idEl = $(this).data("id");
	    	var usernameEl = $(this).data("username");
	    	//alert(nameElo  +" | "+idEl +" | "+usernameEl);
	    	var ctxData = {
	    		name 	 : nameElo,
	    		type  	 : "citoyens",
	    		username : usernameEl,
	    		id 		 : idEl
	    	};
	    	
	    	rcObj.loadChat(nameElo ,"citoyens" ,true ,true, ctxData );
			
		});
		
		element["id"] = element['_id']['$id'];
	});

	$(".btn-edit-preview").click(function(){
		$("#modal-preview-coop").hide(300);
		$("#modal-preview-coop").html("");

		dynCustom = {
			"properties" : {
				"subtype" : {
					"inputType" : "hidden",
					"value" : "<?php echo @$element['subtype'] ?>"
				}
			}
		};

		dyFObj.editElement('classifieds', '<?php echo (string) $id ?>', typeClassified , dynCustom);
	});
</script>