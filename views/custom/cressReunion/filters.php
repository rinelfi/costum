<?php 
	// $themeFilters = $this->renderPartial('costum.views.custom.cressReunion.themeFilter', array());
	// echo $themeFilters;
	$filliaireCategories = CO2::getContextList("filliaireCategories"); 
?>

<style type="text/css">
	#filters-nav{
		padding-top: .6em !important;
	}

	.label{
		border-radius: 10px;
	}
</style>

<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;

	var appConfig=<?php echo json_encode(@$appConfig); ?>;
	var filliaireCategories=<?php echo json_encode(@$filliaireCategories); ?>;
	// var paramsFilter= {
	//  	container : "#filters-nav",
	//  	//filliaireCategories : filliaireCategories,
	//  	filters : {
	//  		themes : {
	//  			view : "themes",
	//  			type : "themes",
	//  			name : "Thématique",
	//  			action : "themes",
	//  			filliaireCategories : filliaireCategories
	//  		},
	// 	 	secteur : {
	//  			view : "dropdownList",
	//  			type : "filters",
	//  			field : "secteurEtablissement",
	//  			name : "Secteur d'activité",
	//  			action : "filters",
	//  			list : costum.lists.secteurEtablissement
	// 	 	},
	// 	 	arrondissement : {
	//  			view : "dropdownList",
	//  			type : "filters",
	//  			field : "arrondissement",
	//  			name : "Arrondissement",
	//  			action : "filters",
	//  			list : costum.lists.arrondissement
	// 	 	},
	// 	 	famille : {
	//  			view : "dropdownList",
	//  			type : "filters",
	//  			field : "famille",
	//  			name : "Famille",
	//  			action : "filters",
	// 			list : costum.lists.famille
	// 	 	}
	//  	}
	//  

	


	 var paramsFilter= {
	 	container : "#filters-nav",
	 	defaults : {
	 		indexStep:30,
	 		sortBy : "name"
	 	},
		loadEvent : {
	 		default : "scroll"
	 	},
	 	results :{
	 		renderView : "directory.elementPanelHtml",
	 		smartGrid : true
		},
	 	filters : {
	 		/*themes : {
	 			view : "themes",
	 			type : "themes",
	 			name : "Thématique",
	 			action : "themes",
	 			event : "themes",
	 			filliaireCategories : filliaireCategories
	 		},*/
	 		themes : {
	 			view : "megaMenuDropdown",
	 			type : "tags",
	 			name : "<?php echo Yii::t("common", "search by theme")?>",
	 			event : "tags",
	 			keyValue: true,
	 			list : (costum.lists.theme)?costum.lists.theme:filliaireCategories
	 		},
		 	secteur : {
	 			view : "dropdownList",
	 			type : "filters",
	 			field : "secteurEtablissement",
	 			name : "Secteur d'activité",
	 			action : "filters",
	 			event : "selectList",
	 			keyValue: true,
	 			list : costum.lists.secteurEtablissement
		 	},
		 	arrondissement : {
	 			view : "dropdownList",
	 			type : "filters",
	 			field : "arrondissement",
	 			name : "Localisation",
	 			action : "filters",
	 			event : "selectList",
	 			keyValue: true,
	 			list : costum.lists.arrondissement
		 	},
		 	famille : {
	 			view : "dropdownList",
	 			type : "filters",
	 			field : "famille",
	 			name : "Famille",
	 			action : "filters",
	 			event : "selectList",
	 			keyValue: true,
				list : costum.lists.famille
		 	},
		 	monthEss : {
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Mois de l'ESS",
	 			event : "tags",
	 			list : [
	 				"MoisESS2021"
	 			]	 				
	 		}	
	 	}

	};
	if(typeof appConfig.loadEvent != "undefined" && !appConfig.loadEvent)
		paramsFilter.loadEvent=appConfig.loadEvent;
	if(typeof appConfig.results != "undefined")
		paramsFilter.results=$.extend({},paramsFilter.results,appConfig.results);
	
	if(pageApp=="moisessmap" || pageApp=="cressmap" || pageApp=="mapmoisess" || pageApp=="mapcress"){
		paramsFilter.results.map = {
			active:true
		}
	}

	var filterSearch={};
	
	// function lazyFilters(time){
	// 	mylog.log("lazyFilters TESTHERE paramsFilter", time);
	// 	if(typeof filterObj != "undefined" ){
	// 		mylog.log("lazyFilters TESTHERE paramsFilter", paramsFilter);
	// 		filterGroup = filterObj.init(paramsFilter);
	// 	} else if(time < 5000){
	// 		setTimeout(function(){
	// 		  lazyFilters(time+200)
	// 		}, time);
	// 	}
	// }

	jQuery(document).ready(function() {
		//alert("HERE");
		//lazyFilters(0);
		/* if (pageApp == "mapmoisess") {
    	 	$(".btn-hide-map").css("display","none");
     		$("#btn-filtersmapContent").css("display","none");
     		$("#btn-panelmapContent").css("display","none");
     	}
     	if (pageApp != "mapmoisess") {
    	 	$(".btn-hide-map").css("display","block");
     		$("#btn-filtersmapContent").css("display","block");
     		$("#btn-panelmapContent").css("display","block");
     	} */

		mylog.log("CRESSREUNION filters.php");
		if(typeof appConfig.filters != "undefined"){
			if(typeof appConfig.filters.types != "undefined"){
				paramsFilter.defaults.types=appConfig.filters.types;
				if(typeof paramsFilter.filters.types != "undefined")
					paramsFilter.filters.types.lists=appConfig.filters.types;
			}
			if(typeof appConfig.filters.tags != "undefined")
				paramsFilter.defaults.tags=appConfig.filters.tags;
		}
		filterSearch = searchObj.init(paramsFilter);
	});
	
</script>