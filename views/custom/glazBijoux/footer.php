<!------------------ begin footer ------------------------------>
<footer>
	<div class="col-md-12 col-xs-12 col-sm-12 col-lg-12" style="background-image: url('<?php  echo Yii::app()->getModule('costum')->assetsUrl?>/images/glazBijoux/footer_bg-02.jpg');padding:50px">
		<!-- <div class="col-md-12"> -->
		<img class="img-responsive" src="<?php  echo Yii::app()->getModule('costum')->assetsUrl?>/images/glazBijoux/ligne_footer-02.png" alt="">
	<!-- </div> -->
		<div class="col-md-4 item Oswald">
			<h5><i class="fa fa-envelope"></i> Email</h5>
			<span class="JosefinSansRegular" >glazbijoux974@gmail.com</span>
		</div>
		<div class="col-md-4 item">
			<h5><i class="fa fa-map-marker"></i> Adresse</h5>
			<span class="JosefinSansRegular">30 impasse de la Ravine 97434 La Saline Les bains</span>
		</div>
		<div class="col-md-4 item">
			<h5><i class="fa fa-phone"></i> Téléphone</h5>
			<span class="JosefinSansRegular">06.92.01.42.11</span>
		</div>
	</div>
</footer>
<!------------------ end footer -------------------------------->
<style>
	footer,footer .item,footer img{
		color: white;
		/*position: relative;
		background-image: url("<?php  echo Yii::app()->getModule('costum')->assetsUrl?>/images/glazBijoux/footer_bg-02.jpg");
		height: 400px;
		width: 100%*/
	}
	footer .item{
		padding-right: 60px;
		padding-left: 60px;
		text-align: center;
	}
	footer h5 .fa{color: #bca87d}
	@media (max-width: 567px){
		footer .item{
			padding-right: 15px;
			padding-left: 15px;
			padding-bottom: 20px
		}
	}
</style>