<?php
HtmlHelper::registerCssAndScriptsFiles( 
	array( 
		'/vendor/colorpicker/js/colorpicker.js',
		'/vendor/colorpicker/css/colorpicker.css',
		'/css/default/directory.css',	
		'/css/profilSocial.css',
		'/css/calendar.css',
	), Yii::app()->theme->baseUrl. '/assets'
);

$cssAnsScriptFilesModule = array(
	'/js/default/calendar.js',
    '/js/default/profilSocial.js',
    '/js/default/editInPlace.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->assetsUrl);

$cssAnsScriptFilesTheme = array(
	"/plugins/jquery-cropbox/jquery.cropbox.css",
	"/plugins/jquery-cropbox/jquery.cropbox.js",
	// SHOWDOWN
	'/plugins/showdown/showdown.min.js',
	//MARKDOWN
	'/plugins/to-markdown/to-markdown.js',
	'/plugins/jquery.qrcode/jquery-qrcode.min.js',
	'/plugins/fullcalendar/fullcalendar/fullcalendar.min.js',
    '/plugins/fullcalendar/fullcalendar/fullcalendar.css', 
    '/plugins/fullcalendar/fullcalendar/locale/'.Yii::app()->language.'.js',
    "/plugins/d3/d3.js",
    "/plugins/d3/d3-flextree.js",
    "/plugins/d3/view.mindmap.js",
    "/plugins/d3/view.mindmap.css",
    
);
HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
?>

<style>
	#onepage {
		background-color: white;
	}

	.acceptBtn{
		border-radius:3px !important;
		color: white;
		background-color: #71CE4E;
		padding: 5px 10px;
		margin-top: 5px;
	}
	.acceptBtn:hover{
		color: #71CE4E !important;
		background-color: white;
		border: 1px solid #71CE4E;
	}
	.acceptBtn i{
		font-size:12px;
	}
	.refuseBtn{
		border-radius:3px !important;
		color: white;
		background-color: #E33551;
		padding: 5px 10px;
		margin-top: 5px;

	}
	.refuseBtn:hover{
		color: #E33551 !important;
		background-color: white;
		border: 1px solid #E33551;
	}
	.refuseBtn i{
		font-size:12px;
	}
	.waitAnswer{
		position:absolute;
		left:38px;
	}
	.col-members{
		background-color: #fff !important;
	    min-height: 100%;
	    position: absolute;
	    right: 0px;
	    -webkit-box-shadow: 0px 5px 5px -2px #656565 !important;
	    -o-box-shadow: 0px 5px 5px -2px #656565 !important;
	    /* box-shadow: 0px -5px 5px -2px #656565 !important; */
	    filter: progid:DXImageTransform.Microsoft.Shadow(color=#656565, Direction=NaN, Strength=5) !important;
	}
	.img-header{
		max-height: 350px;
		width:100%;
		overflow: hidden;
		/*background-image: url("<?php echo Yii::app()->theme->baseUrl; ?>/assets/images/tropic.jpg");
		background-size: 100%;*/
	}
	.main-col-search{
		padding:0px;
	}
	.mix{
		min-height: 100px;
		/*width: 31.5%;*/
		background-color: white;
		display: inline-block;
		border:1px solid #bbb;
		margin-right : 1.5%;
		border-radius: 10px;
		padding:1%;
		margin:-1px;
		-webkit-box-shadow: 5px 5px 5px 0 rgba(0, 0, 0, 0.55);
		-moz-box-shadow: 5px 5px 5px 0 rgba(0, 0, 0, 0.55);
		box-shadow: 5px 5px 5px 0 rgba(0, 0, 0, 0.55);
	}
	#grid .followers{
	display: none;
}
	.mix a{
		color:black;
		/*font-weight: bold;*/
	}
	.mix .imgDiv{
		float:left;
		width:30%;
		background: ;
		margin-top:0px;
	}
	.mix .detailDiv{
		float:right;
		width:70%;
		margin-top:0px;
		padding-left:10px;
		text-align: left;
		text-overflow: ellipsis;
		white-space: nowrap;
		overflow: hidden;
	}

	.mix .toolsDiv{
		float:right;
		width:20%;
		margin-top:0px;
		padding-left:10px;
		text-align: left;
		text-overflow: ellipsis;
		white-space: nowrap;
		overflow: hidden;
		color:white;
	}

	.mix .text-xss{ font-size: 11px; }

	#Grid{
		margin-top: 20px;
		background-color: transparent;
		padding: 15px;
		border-radius: 4px;
		/*border-right: 1px solid #474747;*/
		padding: 0px;
		width:100%;
	}
	#Grid .mix{
		margin-bottom: -1px !important;
	}
	#Grid .item_map_list{
		padding:10px 10px 10px 0px;
		margin-top:0px;
		text-decoration:none;
		background-color:white;
		border: 1px solid rgba(0, 0, 0, 0.08); /*rgba(93, 93, 93, 0.15);*/
	}
	#Grid .item_map_list .left-col .thumbnail-profil{
		width: 75px;
		height: 75px;
	}
	#Grid .ico-type-account i.fa{
		margin-left:11px !important;
	}
	#Grid .thumbnail-profil{
		margin-left:10px;
	}
	#Grid .detailDiv a.text-xss{
		font-size: 12px;
		font-weight: 300;
	}

	.label.address.text-dark{
		padding:0.4em 0.1em 0.4em 0em !important;
	}
	.detailDiv a.thumb-info.item_map_list_panel{
		font-weight:500 !important;
	}

	.shadow {
	    -webkit-box-shadow: none;
	    -moz-box-shadow: none;
	    box-shadow: none;
	}
	#description .container{
		width: 60%;
		margin-left: 20%;
		font-size: 15px;
	}
	.section-title{
		text-transform: uppercase;
		font-weight: 700;
		color: rgb(92,75,62) !important;
	}

	#description .btn-edit-section{
		display: none;

	}

	.col-members h3{
		text-transform: uppercase;
		color:rgb(92,75,62);
		font-size: 0.7em;
		font-weight: 700;
	}
	.col-members h4{
		color:rgb(92,75,62);
		font-size: 0.9em;
		font-weight: 700;

	}
	.col-members .username-min{
		font-weight: 700;
		color:grey;
	}

	.elipsis{
		display: block;
	}

	.element-name{
		font-size:18px;
		padding:10px 20px;
		font-weight: 700;
		height:50px;
		margin-top:0px;
		background-color: rgba(255, 255, 255, 0.8);
	}
	.btn-follow{
		font-weight: 700;
		font-size:13px;
		border-radius:40px;
		border:none;
	}
	.menubar{
		-webkit-box-shadow: 0px 5px -5px rgba(50, 50, 50, 0.75);
		-moz-box-shadow: 0px 5px -5px rgba(50, 50, 50, 0.75);
		box-shadow: 0px 5px 5px -5px rgba(50, 50, 50, 0.75);
		margin-bottom: 40px;
	}
	.btn-menubar{
		font-weight: 700;
		font-size: 12px;
		border-radius: 40px;
		border: none;
		background-color: white;
		padding: 13px 20px;
	}

	.btn-menubar:hover{
		background-color: #4a4a4a;
		color:white;
		-webkit-box-shadow: 0px 0px 5px -1px rgba(50, 50, 50, 0.75);
		-moz-box-shadow: 0px 0px 5px -1px rgba(50, 50, 50, 0.75);
		box-shadow: 0px 0px 5px -1px rgba(50, 50, 50, 0.75);
	}
	iframe.fullScreen {
	    width: 100%;
	    height: 100%;
	    position: absolute;
	    top: 0;
	    left: 0;
	}
	.contentEntity{
		padding: 0px !important;
		margin: 0px !important;
		border-top: solid rgba(128, 128, 128, 0.2) 1px;
		margin-left: 0% !important;
		width: 100%;
		box-shadow: 0px 0px 5px -1px #d3d3d3;
	}
	.contentEntity:hover {
   	 background-color: rgba(211, 211, 211, 0.2);
	}
	.container-img-parent {
	    display: block;
	    width: 100%;
	    max-width: 100%;
	    /*min-height: 90px;*/
	    max-height: 90px;
	    overflow: hidden;
	    background-color: #d3d3d3;
	    text-align: center;
    }
    .container-img-parent i.fa {
	    margin-top: 20px;
	    font-size: 50px;
	    color: rgba(255, 255,255, 0.8);
	}

	.fileupload, .fileupload-preview.thumbnail, .fileupload-new .thumbnail,
	.fileupload-new .thumbnail img, .fileupload-preview.thumbnail img {
	    width: auto !important;
	}
	.user-image{
		background-color: white;
	}
	#fileuploadContainer{
		margin:-1px!important;
	}
	#fileuploadContainer .thumbnail{
		border-radius: 0px!important
	}
	#profil_imgPreview{}
	.removeLink{
		    display: none;
    position: absolute;
    right: -5px;
    top: -5px;
    border-radius: 12px;
    width: 25px;
    background-color: black;
    height: 25px;
    color: white;
    border: inherit;
	}
	.removeLink:hover{
		font-size: 15px;
		border: 1px solid white;
	}
	.contentItem{
		float:left;
		position: relative;
	}
	#divTags .tag {
	    background-color: #3A87AD !important;
	    border-color: #3A87AD !important;
	}
</style>
<div class="col-xs-12 no-padding" id="onepage">
	<div class="col-md-12 col-sm-12 col-xs-12 text-dark text-center">
		<h1 class="">
			<?php echo $element['name']; ?>
		</h1>
	</div>
	<br/>
	<div class="col-xs-12 margin-top-20">
		<?php 
			$description = (empty($element["description"]) ? "" : $element["description"]);
		?>
		<div id="descriptionMarkdown" class="hidden"><?php echo (empty($description) ? "" : $description ); ?></div>
		<div class="col-xs-10 col-xs-offset-1 no-padding item-desc" id="descriptionAbout"><?php echo $description ; ?></div>
	</div>
	<div class="col-xs-12 margin-top-20">
		<div class="col-xs-12 text-center">
		    <h4 class="section-title">
		        <span class="sec-title">Productions</span><br>
		        <i class="fa fa-angle-down"></i>
		    </h4>
		</div>
		<div class="col-xs-10 col-xs-offset-1 item-desc margin-top-20" id="listPoi"></div>
	</div>
</div>
<?php
$slugVal = InflectorHelper::slugify2($element['name']) ;


?>
<script type="text/javascript">
var contextData = <?php echo json_encode( Element::getElementForJS(@$element, @$type) ); ?>;
var canEdit = <?php echo json_encode($canEdit) ?>;
var slugVal = "<?php echo $slugVal ?>";
var hashUrlPage= ( (typeof contextData.slug != "undefined") ? 
						"#@"+contextData.slug : 
						"#page.type."+contextData.type+".id."+contextData.id);
    
if(location.hash.indexOf("#page")>=0){
	strHash="";
	if(location.hash.indexOf(".view")>0){
		hashPage=location.hash.split(".view");
		strHash=".view"+hashPage[1];
	}
	replaceSlug=true;
	history.replaceState("#page.type."+contextData.type+".id."+contextData.id, "", "#@poi."+contextData.name);
}
	mylog.log("contexteBadge",contextData);
	var data = {
		searchType : ["poi"], 
		filters : {
			tags : [$.trim(contextData.name)]	
		}	
	};

jQuery(document).ready(function() {
	addItemsToSly(slugVal);
	initDescs();
	// getAjax('', baseUrl+'/api/poi/get/limit/500/tags/'+$.trim(contextData.name),
	// 			function(data){ 
	// 				var type = "poi";
	// 				// mylog.log("pageProfil.views.directory canEdit" , canEdit);
	// 				if(typeof canEdit != "undefined" && canEdit)
	// 					canEdit="poi";
	// 				// mylog.log("pageProfil.views.directory edit" , canEdit);
	// 				$("#listPoi").html( directory.showResultsDirectoryHtml(data.entities, type, null, canEdit) );
					
	// 				directory.bindBtnElement();
	// 				initBtnAdmin();
	// 				coInterface.bindButtonOpenForm();
	// 			}
	// ,"html");
	ajaxPost(null,
			baseUrl + "/" + moduleId + "/search/globalautocomplete",
			data,
		function(data){ 
					var type = "poi";
					// mylog.log("pageProfil.views.directory canEdit" , canEdit);
					if(typeof canEdit != "undefined" && canEdit)
						canEdit="poi";
					// mylog.log("pageProfil.views.directory edit" , canEdit);
					$("#listPoi").html( directory.showResultsDirectoryHtml(data.results, "directory.elementPanelHtml", true, null) );
					
					coInterface.bindLBHLinks();
			
					directory.bindBtnElement();
					//initBtnAdmin();
					coInterface.bindButtonOpenForm();

					const arr = document.querySelectorAll('img.lzy_img')
					arr.forEach((v) => {
						v.dom = "#content-results-profil";
						imageObserver.observe(v);
					});
				}
	,"html");
});


function initDescs() {
	mylog.log("inintDescs");
	var descHtml = "<i>"+trad.notSpecified+"</i>";
	if($("#descriptionMarkdown").html().length > 0){
		descHtml = dataHelper.markdownToHtml($("#descriptionMarkdown").html()) ;
	}
	$("#descriptionAbout").html(descHtml);
	mylog.log("descHtml", descHtml);
}

</script>
