<?php 

	$cssAnsScriptFilesTheme = array(
		"/plugins/Chart-2.8.0/Chart.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);

?>

<script type="text/javascript">

	var COLORS = [
	"#0A2F62",
	"#0B2D66",
	"#064191",
	"#2C97D0",
	"#16A9B1",
	"#0AA178",
	"#74B976",
	"#0AA178",
	"#16A9B1",
	"#2A99D1",
	"#064191",
	"#0B2E68"
	];
</script>


<div id="canvas-holder" style="margin:0px auto;width:65%">
		<canvas id="chart-area"></canvas>
</div>

<script>
jQuery(document).ready(function() {
	mylog.log("render","/modules/costum/views/custom/ctenat/graph/pieMillion.php");
	var config = {
		type: 'pie',
		data: {
			datasets: [{
				data: [328,333],
				backgroundColor: [COLORS[0],"#38A1E0"],
			}],
			labels: [ 'Public','Privé']
		},
		options: {
			responsive: true
		}
	};
	var ctx = document.getElementById('chart-area').getContext('2d');
	window.myPie = new Chart(ctx, config);
});
</script>