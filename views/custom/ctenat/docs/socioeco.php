<div class="col-xs-12 margin-top-20">
    <h1 class="text-center">Acteurs socio-économiques</h1>

    <div class="col-xs-12 margin-top-20">
        <?php 
        $params = array(
            "canEdit" => Authorisation::isCostumAdmin(),
            "cmsList" => PHDB::find(Cms::COLLECTION, 
                        array( "parent.".$this->costum["contextId"] => array('$exists'=>1), 
                               "parent.".$this->costum["contextId"].".type"=>$this->costum["contextType"],
                               "page"=>"socioeco",
                                "structags" => array('$in'=>array("stepsocio1","stepsocio2","stepsocio3","stepsocio4"))
                                )),
            "listSteps" => array("socio1","socio2","socio3","socio4")
        );
        //var_dump($params['cmsList']);
        foreach ($params['cmsList'] as $id => $value) 
        {
            $documents  = PHDB::find(Document::COLLECTION, array('id' => $id, "type" => Cms::COLLECTION ));
            if( count($documents) )
            {
                $params['cmsList'][$id]["documents"] = array();
                foreach ( $documents as $key => $doc ) {
                  $params['cmsList'][$id]["documents"][] = array (
                    "name" => $doc['name'], 
                    "doctype" => $doc['doctype'],  
                    "path" => Yii::app()->baseUrl."/".Yii::app()->params['uploadUrl'].$doc["moduleId"]."/".$doc["folder"]."/".$doc['name']
                  );
                }
                //var_dump($params['cmsList'][$id]["documents"]);
            }
        }

        //var_dump($params);
        echo $this->renderPartial("costum.views.tpls.blockCms.wizard",$params,true);
        ?>
    </div>
</div>

