<style type="text/css">
.img-bd-round{border: 1px solid #ccc; border-radius: 30px;padding : 20px;}


#startSection { background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABZ0RVh0Q3JlYXRpb24gVGltZQAxMC8yOS8xMiKqq3kAAAAcdEVYdFNvZnR3YXJlAEFkb2JlIEZpcmV3b3JrcyBDUzVxteM2AAABHklEQVRIib2Vyw6EIAxFW5idr///Qx9sfG3pLEyJ3tAwi5EmBqRo7vHawiEEERHS6x7MTMxMVv6+z3tPMUYSkfTM/R0fEaG2bbMv+Gc4nZzn+dN4HAcREa3r+hi3bcuu68jLskhVIlW073tWaYlQ9+F9IpqmSfq+fwskhdO/AwmUTJXrOuaRQNeRkOd5lq7rXmS5InmERKoER/QMvUAPlZDHcZRhGN4CSeGY+aHMqgcks5RrHv/eeh455x5KrMq2yHQdibDO6ncG/KZWL7M8xDyS1/MIO0NJqdULLS81X6/X6aR0nqBSJcPeZnlZrzN477NKURn2Nus8sjzmEII0TfMiyxUuxphVWjpJkbx0btUnshRihVv70Bv8ItXq6Asoi/ZiCbU6YgAAAABJRU5ErkJggg==);
		min-height: 400px;}
	#startSection .error-template {padding: 40px 15px;text-align: center;}
	#startSection .error-template h1 {margin-top: 120px;}
	#container-docs.side-body{margin-top: 0px;}
</style>

<div class="col-xs-12 " id="startSection">
	<!-- <h1 class="text-center">Découvrir les dispositifs</h1>
	<div class="col-sm-6 text-center">
		<a href="javascript:;" id="startSectionCTEbtn" >
			<img class="img-responsive img-bd-round" width="150" style="margin: auto;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/PictoCTEFlower.jpg">
			<h3>CTE</h3>
		</a>
	</div>
	<div class="col-sm-6 text-center">
		<a href="https://agence-cohesion-territoires.gouv.fr/crte" target="_blank"  >
			<img class="img-responsive img-bd-round" width="150" style="margin: auto;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/PictoCTE.png">
			<h3>CRTE</h3>
		</a>
	</div> -->



	<?php if(@Yii::app()->session["userId"]){?>
		<div class="error-template">
	        <h1>
	            Page en cours de construction
	        </h1>
	    </div>
    <?php }  ?>
</div>

<!-- <div class="col-xs-12 margin-top-20 hide " id="startSectionCTE">
	<h1 class="text-center">Découvrir CTE</h1>
	<div class="col-sm-6 text-center">
		<a href="javascript:;"  id="userDoc" >
			<img class="img-responsive img-bd-round" width="150" style="margin: auto;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/parcours.png">
			<h3>Suivez votre parcours utilisateur</h3>
		</a>
	</div>
	<div class="col-sm-6 text-center">
		<a href="javascript:;"  id="detailDoc"  >
			<img class="img-responsive img-bd-round" width="150" style="margin: auto;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/docDétaillé.png">
			<h3>Accédez à toute la documentation détaillée</h3>
		</a>
	</div>
</div>

<div id="acteurKick" class="col-xs-12 margin-top-20 hide">
	<h2 class="text-center text-grey">Quel utilisateur êtes vous ?</h2>
	<div class="col-sm-6 text-center">
		<a href="javascript:;"  id="startDoc"  class="link-docs-menu" data-type="public" data-dir="costum.views.custom.ctenat.docs">
			<img class="img-responsive img-bd-round" width="150" style="margin: auto;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/public.png">	
			<h3>Acteur Public</h3>
		</a>
	</div>
	<div class="col-sm-6 text-center">
		<a href="javascript:;"  id="startDoc"  class="link-docs-menu" data-type="socioeco" data-dir="costum.views.custom.ctenat.docs">
			<img class="img-responsive img-bd-round" width="170" style="margin: auto;" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/ctenat/socioeco.png">
			<h3>Acteur socio-éco</h3>
		</a>
	</div>
	
</div> -->
<script type="text/javascript">
jQuery(document).ready(function() {
	bindLinkDocs();
	$("#userDoc").click(function(e) { 
		$('#acteurKick').removeClass("hide");
		$('#startSection').addClass("hide");
	 })
	$("#startSectionCTEbtn").click(function(e) { 
		$('#startSectionCTE').removeClass("hide");
		$('#startSection').addClass("hide");
	 })
	
	
	$("#detailDoc").click(function(e) { 
		//$('#container-docs').removeClass('col-xs-12').addClass('col-xs-9')
		$('.section-desc').addClass('ml-310');
		$('.blockcms-section').addClass('ml-310');
		$('#container-docs').removeClass('col-xs-12').addClass('ml-310');
		$('#header-doc').addClass('pl-310');
		$('#menu-left').removeClass("hide");
		
		navInDocs("docDetail", "costum.views.custom.ctenat.docs");
	 })
});
</script>