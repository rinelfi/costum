<style type="text/css">
	.btn-filters-select.active{
    border: 4px solid #1E86C8;
    background-color: #9FBD38;
}
.btn-filters-select{
    background-color: white;
}
.btn-filters-select:hover{
    background-color: white;
}
</style>
<div id="iconNavFilters" class="col-xs-offset-2 col-xs-8"></div>
<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	var paramsFilter= {
	 	container : "#filters-nav",
	 	results : {
	 		smartGrid : true,
	 		renderView : "directory.elementPanelHtml"
	 	},
	 	loadEvent : {
	 		default : "scroll"
	 	},
	 	filters : {
	 		/*iconNav : {
				dom : "#iconNavFilters",
				view : "horizontalList",
				active : true,
				classList : "pull-left tooltips margin-10",
				type : "filters",
				field : "tags",
				event : "selectList",
				typeList : "object",  
				multiple : true,
				noLabel : true,
				tooltip : "bottom",
				seeall : false, //"after",
				imgConfig : { h : "80px"}, 
				list : {
					water : {
						label : Object.keys(costum.lists.domainAction)[0],
						value : Object.keys(costum.lists.domainAction)[0],
						img : modules.costum.url+"/images/ctenat/water.png"
					},
					mobility : {
						label : Object.keys(costum.lists.domainAction)[1],
						value : Object.keys(costum.lists.domainAction)[1],
						img : modules.costum.url+"/images/ctenat/cyclo.png"
					},
					agriculture : {
						label : Object.keys(costum.lists.domainAction)[2],
						value : Object.keys(costum.lists.domainAction)[2],
						img : modules.costum.url+"/images/ctenat/trees.png"
					},
					nrj : {
						label : Object.keys(costum.lists.domainAction)[3],
						value : Object.keys(costum.lists.domainAction)[3],
						img : modules.costum.url+"/images/ctenat/kwInstalled.png"
					},
					recycle : {
						label : Object.keys(costum.lists.domainAction)[4],
						value : Object.keys(costum.lists.domainAction)[4],
						img : modules.costum.url+"/images/ctenat/tonnes.png"
					},
					urbanism : {
						label : Object.keys(costum.lists.domainAction)[5],
						value : Object.keys(costum.lists.domainAction)[5],
						img : modules.costum.url+"/images/ctenat/building.png"
					},
					citizen : {
						label : Object.keys(costum.lists.domainAction)[6],
						value : Object.keys(costum.lists.domainAction)[6],
						img : modules.costum.url+"/images/ctenat/person.png"
					}
				}
			},*/
	 		domainAction : {
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Domaine d'action",
	 			event : "tags",
	 			list : costum.lists.domainAction
	 		},
	 		cibleDD:{
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Objectif",
	 			event : "tags",
	 			list : costum.lists.cibleDD
	 		},
	 		scope : true,
	 		scopeList : {
	 			name : "Région",
	 			params : {
	 				countryCode : ["FR","RE","MQ","GP","GF","YT"], 
	 				level : ["3"],
	 				sortBy:"name"
	 			}
	 		},
	 		dispositif : {
	 			view : "dropdownList",
	 			type : "filters",
	 			field : "dispositif",
	 			name : "Dispositif",
	 			event : "selectList",
	 			keyValue : true,
	 			list : costum.lists.dispositif
	 		}

	 	}
	 };
	if(pageApp=="territoires"){
		
	 	/*paramsFilter.filters.status={
	 		view : "dropdownList",
 			type : "filters",
 			field : "source.status.ctenat",
 			name : "Statuts",
 			event : "selectList",
 			keyValue : true,
 			list : [ 
                "<?php //echo Ctenat::STATUT_CTER_LAUREAT; ?>", 
                "<?php //echo Ctenat::STATUT_CTER_SIGNE; ?>"
            ]
	 	};*/
	 	paramsFilter.results.renderView = "directory.elementPanelHtmlFullWidth";
	 	paramsFilter.results.smartGrid = false;
	 	paramsFilter.defaults = {
	 		types : [ "projects" ],
	 		filters : {
		 		"category" : "cteR"
			}
		};
	}else if(pageApp=="projects"){
	 	paramsFilter.defaults = {
	 		types : [ "projects" ],
	 		filters : {
		 		"category" : "ficheAction"
			}
		};
		paramsFilter.results.renderView = "directory.elementPanelHtmlFullWidth";
		paramsFilter.results.smartGrid = false;

		if(typeof contextData != "undefined" && contextData != null &&
			contextData.id != costum.contextId ){
			paramsFilter.defaults.filters["links.projects."+contextData.id] = {
				'$exists' : 1 
			};
		}
	}


	filterSearch = undefined
	jQuery(document).ready(function() {
		if(location.hash.indexOf("@") == -1)
			filterSearch = searchObj.init(paramsFilter);
	});
</script>