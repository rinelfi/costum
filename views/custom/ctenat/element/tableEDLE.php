<?php
$editBtnL = ($canEdit === true && $editable) ? " <a href='javascript:;' data-id='".$id."' data-collection='".Project::COLLECTION."' data-path='".$answerPath.".' class='addIndecolo btn btn-default'><i class='fa fa-plus'></i> Ajouter une ligne </a>" : "";

$paramsData = [ "titles" => [ "Indicateur",
    "Année Réf1 / Valeur",
    "Année Réf2 / Valeur",
    "Objectif / Réalisé",

    "2026"],
    "keys" => [ "res2026"] ];

?>
<style type="text/css">
    .bgGrey{background-color: grey;}

    .ambitionEDLEtitle{
        text-align: center;
        color: black;
    }

    .ambitionEDLEbtn {
        margin-top: 10px;
        margin-bottom: 10px;
        color: #b94a48;
        border: solid 2px #b94a48;
        border-radius: 3px;
        letter-spacing: 1px;
    }

    .ambitionEDLEcsvbtn {
        margin-top: 10px;
        margin-bottom: 10px;
        color: #4b9a20;
        border: solid 2px #4b9a20;
        border-radius: 3px;
        letter-spacing: 1px;
    }
</style>

<?php if (!isset($mode) || (isset($mode) && $mode != "pdf") ){ ?>
<div class="form-group">
    <div class="row">
        <h4 class="col-md-6 col-md-offset-3 ambitionEDLEtitle" >Liste d'indicateurs<?php echo $editBtnL?></h4>
        <div class="col-md-3 ambitionEDLEtitle">
            <a href="javascript::" onclick="document.location.href='<?php echo Yii::app()->createUrl("/costum").'/ctenat/edlepdf/id/'.$id.'/type/'.$type ?>'" data-url="<?php echo Yii::app()->createUrl("/costum").'/ctenat/edlepdf/id/'.$id.'/type/'.$type ?>" class="btn btnedlePdf ambitionEDLEbtn" ><i class="fa fa-file-pdf-o"></i> PDF </a>
            <a href="javascript::" data-url="<?php echo Yii::app()->createUrl("/costum").'/ctenat/edlecsv/id/'.$id.'/type/'.$type ?>" class="btn btnCsv btnedleCsv ambitionEDLEcsvbtn" ><i class="fa fa-table"></i> CSV </a>
        </div>

        <table class="table table-bordered table-hover  directoryTable" id="panelAdminIndecolo">
            <thead>

            <tr>
                <th></th>
                <?php foreach ($paramsData["titles"] as $key => $t) {
                    echo "<th>".$t."</th>";
                } ?>
            </tr>
            </thead>
            <tbody class="directoryLines">
            <?php
            $ct = 0;
            if(isset($indecolosActifs)){

                $editableAttr = ($editable) ? "contenteditable='true'" :"";
                foreach ($indecolosActifs as $indid => $a) {
                    echo "<tr class='line".$indid."'>".
                        (($editable) ? "<td rowspan='2'><i  data-indid='".$indid."' class='delIndecolo fa fa-trash text-red'></i></td>" : "<td rowspan='2'></td>").
                        "<td id='indic".$ct."' rowspan='2' style='vertical-align : middle;text-align:center;'>".( !empty($indecolos[$indid]) ?  $indecolos[$indid]["name"] : "" )."</td>".
                        "<td rowspan=2>".
                        "<table class='table table-bordered'><tr>".

                        "<td ".$editableAttr." class='editable' id='input".$indid."ref1year' data-indid='".$indid."' data-key='ref1year'  data-type='year'>".((isset($indecolosActifs[$indid]["ref1year"])) ? $indecolosActifs[$indid]["ref1year"] : "")."</td>".
                        "<td  ".$editableAttr." class='editable' id='input".$indid."ref1' data-indid='".$indid."' data-key='ref1'>".((isset($indecolosActifs[$indid]["ref1"])) ? $indecolosActifs[$indid]["ref1"] : "")."</td></tr></table>".
                        "</td>".
                        "<td rowspan=2>".
                        "<table class='table table-bordered'><tr>".
                        "<td ".$editableAttr." class='editable' id='input".$indid."ref2year' data-indid='".$indid."' data-key='ref2year' data-type='year'>".((isset($indecolosActifs[$indid]["ref2year"])) ? $indecolosActifs[$indid]["ref2year"] : "")."</td>".
                        "<td ".$editableAttr." class='editable' id='input".$indid."ref2' data-indid='".$indid."' data-key='ref2'>".((isset($indecolosActifs[$indid]["ref2"])) ? $indecolosActifs[$indid]["ref2"] : "")."</td></tr></table>".
                        "</td>".
                        "<td style='vertical-align : middle;text-align:center;'>Objectif</td>";
                    foreach ($paramsData["keys"] as $i => $k) {
                        $val = (isset($indecolosActifs[$indid]["obj".$k])) ? $indecolosActifs[$indid]["obj".$k] : "";
                        echo "<td ".$editableAttr." class='editable' id='input".$indid."obj".$k."' data-indid='".$indid."' data-key='obj".$k."' >".$val."</td>";
                    }
                    echo "</tr>";

                    $editableClass =  "editContent";
                    echo "<tr class='line".$indid."'>".

                        "<td>Réalisé</td>";
                    foreach ($paramsData["keys"] as $i => $k) {
                        $val = (isset($indecolosActifs[$indid]["real".$k])) ? $indecolosActifs[$indid]["real".$k] : "";
                        echo "<td ".$editableAttr." class='editable' id='input".$indid."real".$k."' data-indid='".$indid."' data-key='real".$k."'>".$val."</td>";
                    }
                    echo "</tr>";

                    $ct++;
                }
            }

            ?>

            </tbody>
        </table>
    </div>
    
    <script type="application/javascript">
        $( ".btnefgfdlePdf").off().on("click", function(){
            ajaxPost(
                null,
                $(this).data("url"),
                null,
                function(data){
                    mylog.log("pdf data", typeof data);
                    try {
                        const { jsPDF } = window.jspdf;
                        var div = document.createElement('div');
                        div.innerHTML = data.trim();
                        // Change this to div.childNodes to support multiple top-level nodes
                        //div.firstChild;
                        console.log("html2canvas div.firstChild", div);
                        var doc4 = new jsPDF();
                        doc4.html(div, {
                            callback: function (doc) {
                                doc4.save("a4.pdf");
                            }
                        });
                    } catch(e) {
                        console.log("html2canvas catch");
                        console.log(e);
                    }
                },
                function(data){
                    $("#searchResults").html("erreur");
                }
            );
        });

        $(".btnCsv").off().on("click", function(){

            ajaxPost(
                null,
                $(this).data("url"),
                null,
                function(data){
                    mylog.log("csv data", data);
                    const { Parser } = json2csv;
                    const fields = data.fields;
                    const json2csvParser = new Parser({ fields });
                    const csv = json2csvParser.parse(data.results);
                    var exportedFilenmae = 'export.csv';
                    var blob = new Blob([csv], { type: 'text/csv;charset=utf-8;' });
                    if (navigator.msSaveBlob) { // IE 10+
                        navigator.msSaveBlob(blob, exportedFilenmae);
                    } else {
                        var link = document.createElement("a");
                        if (link.download !== undefined) { // feature detection
                            // Browsers that support HTML5 download attribute
                            var url = URL.createObjectURL(blob);
                            link.setAttribute("href", url);
                            link.setAttribute("download", exportedFilenmae);
                            link.style.visibility = 'hidden';
                            document.body.appendChild(link);
                            link.click();
                            document.body.removeChild(link);
                        }
                    }

                },
                function(data){
                    $("#searchResults").html("erreur");
                }
            );
        });
    </script>

    <?php } elseif (isset($mode) && $mode == "pdf"){ ?>
        <style type="text/css">
            table.edltetable{
                width: 100%;
                border-collapse: collapse;
                padding: 0px;
                margin: 0px;
                font-family: Arial;
                font-size: 13px;
                line-height: 1.4;
                border: solid 1px #000000;
            }

            .edltetable th
            {
                border: solid 1px #000000;
                text-align: center;
            }

            .edltetable td
            {
                border: solid 1px #000000;
            }

            .edltetable .row
            {
                border-top: solid 1px #000000;
            }
        </style>
        <h1>Etat des lieux écologique du <?php echo @$name ?></h1>

        <table class="edltetable">
            <thead>
            <tr>
                <?php foreach ($paramsData["titles"] as $key => $t) {
                    echo "<th>".$t."</th>";
                } ?>
            </tr>
            </thead>
            <tbody class="directoryLines">
            <?php
            $ct = 0;
            if(isset($indecolosActifs)){
                foreach ($indecolosActifs as $indid => $a) {
                    echo "<tr class='line".$indid."'>".
                        '<td id="indic'.$ct.'" rowspan="2" style="vertical-align : middle;text-align:center;">'.( !empty($indecolos[$indid]) ?  $indecolos[$indid]["name"] : "" ).'</td>'.
                        '<td rowspan="2">'.
                        "<table class='edltetable'>".
                        "<tr>".
                        "<td class='editable' id='input".$indid."ref1year' data-indid='".$indid."' data-key='ref1year'  data-type='year'>".((isset($indecolosActifs[$indid]['ref1year'])) ? $indecolosActifs[$indid]['ref1year'] : '')."</td>".
                        //  "<td class='editable' id='input".$indid."ref1' data-indid='".$indid."' data-key='ref1'>".((isset($indecolosActifs[$indid]["ref1"])) ? $indecolosActifs[$indid]["ref1"] : "")."</td>".
                        "</tr>".
                        "<tr>".
                        // "<td class='editable' id='input".$indid."ref1year' data-indid='".$indid."' data-key='ref1year'  data-type='year'>".((isset($indecolosActifs[$indid]['ref1year'])) ? $indecolosActifs[$indid]['ref1year'] : '')."</td>".
                        "<td class='editable' id='input".$indid."ref1' data-indid='".$indid."' data-key='ref1'>".((isset($indecolosActifs[$indid]["ref1"])) ? $indecolosActifs[$indid]["ref1"] : "")."</td>".
                        "</tr>".
                        "</table>".
                        "</td>".
                        '<td rowspan="2">'.
                        "<table class='edltetable'>".
                        "<tr>".
                        "<td style='border-right: solid 1px #000000;' >".((isset($indecolosActifs[$indid]["ref2year"])) ? $indecolosActifs[$indid]["ref2year"] : "")."</td>".
                        "</tr>".
                        "<tr>".
                        "<td class='editable' id='input".$indid."ref2' data-indid='".$indid."' data-key='ref2'>".((isset($indecolosActifs[$indid]["ref2"])) ? $indecolosActifs[$indid]["ref2"] : "")."</td></tr></table>"."</td>".
                        "<td style='vertical-align : middle;text-align:center;'>Objectif</td>";
                    foreach ($paramsData["keys"] as $i => $k) {
                        $val = (isset($indecolosActifs[$indid]["obj".$k])) ? $indecolosActifs[$indid]["obj".$k] : "";
                        echo "<td class='editable' id='input".$indid."obj".$k."' data-indid='".$indid."' data-key='obj".$k."' >".$val."</td>";
                    }
                    echo "</tr>";


                    echo "<tr class='line".$indid."'>".

                        "<td>Réalisé</td>";
                    foreach ($paramsData["keys"] as $i => $k) {
                        $val = (isset($indecolosActifs[$indid]["real".$k])) ? $indecolosActifs[$indid]["real".$k] : "";
                        echo "<td class='editable' id='input".$indid."real".$k."' data-indid='".$indid."' data-key='real".$k."'>".$val."</td>";
                    }
                    echo "</tr>";

                    $ct++;
                }
            }

            ?>

            </tbody>
        </table>
    <?php  } ?>
