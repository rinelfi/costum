<style type="text/css">
.dashboard-cte .close-modal .lr,
.dashboard-cte .close-modal .lr .rl{
	height:50px;
	width: 1px;
    background: white;
}
.dashboard-cte .close-modal:hover .lr,
.dashboard-cte .close-modal:hover .lr .rl{
	width: 2px;
}
.dashboard-cte .close-modal .lr{
	margin-left: 28px;
    transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    -webkit-transform: rotate(45deg);
}
.dashboard-cte .close-modal .lr .rl{
    transform: rotate(90deg);
    -ms-transform: rotate(90deg);
    -webkit-transform: rotate(90deg);

}
.dashboard-cte .close-modal {
	position: absolute;
	top: 5px;
	right: 5px;
    width: 55px;
    height: 50px;
    z-index: 1;
    cursor: pointer;
}
.dashboard-cte .profil-img{
	border-radius: 5px;
	border:2px solid white;
}
.dashboard-cte .btn-dash-link{
	color: white;
	font-size: 18px;
	padding: 5px 10px;
	border-bottom: 1px solid white;
}
.dashboard-cte .btn-dash-link:hover{
	color:black;
	background-color: white;
}
.tilsCTENAT{
	border-radius: 5px;
	padding: 10px;
	color: white;
}
.tilsCTENAT:hover{
	background-color: rgba(255, 255, 255, 0.88);
	color:#22252A;
}
.tilsCTENAT .img-tils{
	border-radius: 5px;
	border:2px solid white;  
	width: 90px;
	height: 90px;
}
.tilsCTENAT .title-tils{
	font-size: 18px;
	color: white;
	font-weight: 800;
}
.tilsCTENAT:hover .title-tils{
	color:#22252A;
}
.showAllList, .reduceList, .linkTODoc{
	color: #45c7dd;
	cursor: pointer;
}
.linkTODoc:hover, .tilsCTENAT .refLink{
	color: #45c7dd;
}
a.btn-logout-Dash {
    background-color: white;
    color: red;
    border: 1px solid red;
    text-transform: none;
    font-size: 16px;
    font-weight: 700;
}
a.btn-logout-Dash:hover{
	background-color: red;
	color: white;
	border : 1px solid white;
}
</style>
<div class="col-xs-12 padding-top-50">
	<div class="close-modal" data-dismiss="modal">
        <div class="lr">
            <div class="rl">
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-4 col-xs-12">
    	<?php $imgPath=(isset(Yii::app()->session["user"]["profilMediumImageUrl"]) && !empty(Yii::app()->session["user"]["profilMediumImageUrl"])) ? Yii::app()->createUrl(Yii::app()->session["user"]["profilMediumImageUrl"]): $this->module->getParentAssetsUrl()."/images/thumbnail-default.jpg"; ?>  
    	<img class="img-responsive profil-img" src="<?php echo $imgPath ?>">
    	<a href="#@<?php echo Yii::app()->session["user"]["slug"] ?>" class="lbh btn-dash-link col-xs-12">Mon profil</a>
    	<a href="#@<?php echo Yii::app()->session["user"]["slug"] ?>.view.settings" class="lbh btn-dash-link col-xs-12">Mes paramètres</a>
    	<a href="javascript:;" class="btn btn-logout-Dash col-xs-12 logoutBtn" style="margin-top: 10px">
			<i class="fa fa-sign-out"></i> Déconnexion								
		</a>
    </div>
    <div class="col-md-9 col-sm-8 col-xs-12">
    	<h1 class="text-white col-xs-12" style="margin-top:0px;">Bonjour <?php echo Yii::app()->session["user"]["name"]; ?></h1>
    	<?php if(isset($roles["adminCTENat"]) && !empty($roles["adminCTENat"]) && $roles["adminCTENat"] != "false"){ ?>
			<div class="col-xs-12 margin-bottom-20 no-padding">
				<h4 class='col-xs-12 text-white'>Vous êtes administrateur</h4>
				<span class="col-xs-12 text-white">Ce rôle vous offre un accès intégral, y compris en édition, à la plateforme<!--<a href="javascript:;" class="linkTODoc"> ... En savoir plus</a>--></span>
			</div>
		<?php } 
		if(isset($roles["partnerCTENat"]) && !empty($roles["partnerCTENat"]) && $roles["partnerCTENat"] != "false"){ ?>
			<div class="col-xs-12 margin-bottom-20 no-padding">
				<h4 class='col-xs-12 text-white'>Vous êtes partenaire</h4>
				<span class="col-xs-12 text-white">Ce rôle vous permet de visualiser l’ensemble de la plateforme et son contenu. Il permet notamment de commenter tous les dispositifs et leurs actions en construction<!--<a href="javascript:;" class="linkTODoc"> ... En savoir plus</a>--></span>
			</div>
		<?php }
		if(isset($roles["referentCTENat"]) && !empty($roles["referentCTENat"])){ ?>
			<div class="col-xs-12 margin-bottom-20 no-padding">
				<h4 class='col-xs-12 text-white'>Vous êtes référent-e d'une organisation partenaire</h4>
				<span class="col-xs-12 text-white">Ce rôle vous permet de visualiser l’ensemble de la plateforme et son contenu. Il permet notamment de commenter tous les dispositifs et leurs actions en construction<!--<a href="javascript:;" class="linkTODoc"> ... En savoir plus</a>--></span>
			</div>
		<?php }
		if(isset($roles["adminCTER"]) && !empty($roles["adminCTER"])){
			echo '<div class="col-xs-12 margin-bottom-20 no-padding">';
				echo "<h4 class='col-xs-12 text-white'>Vous êtes administrateur</h4>";
				echo "<span class='col-xs-12 text-white'>Ce rôle vous offre un accès intégral, y compris en édition, aux disposiitfs et aux actions rattachées</span>";
					displayTilsContent($roles["adminCTER"]);
			echo "</div>";
		}
		if(isset($roles["partnerCTER"]) && !empty($roles["partnerCTER"])){
			echo '<div class="col-xs-12 margin-bottom-20 no-padding">';
				echo "<h4 class='col-xs-12 text-white'>Vous êtes partenaire de CTE</h4>";
				echo "<span class='col-xs-12 text-white'>Ce rôle vous permet de visualiser l’ensemble des disposiitfs et leurs contenus. Il permet notamment de commenter toutes les actions en construction</span>";
					displayTilsContent($roles["partnerCTER"]);
			echo "</div>";	
		}
		if(isset($roles["referentCTER"]) && !empty($roles["referentCTER"])){
			echo '<div class="col-xs-12 margin-bottom-20 no-padding">';
				echo "<h4 class='col-xs-12 text-white'>Vous êtes référent-e de disposiitf</h4>";
				echo "<span class='col-xs-12 text-white'>Ce rôle vous permet de visualiser l’ensemble des disposiitfs et leurs contenus. Il permet notamment de commenter toutes les actions en construction</span>";
					displayTilsContent($roles["referentCTER"]);
			echo "</div>";
		}
		if(isset($roles["adminAction"]) && !empty($roles["adminAction"])){
			echo '<div class="col-xs-12 margin-bottom-20 no-padding">';
				echo "<h4 class='col-xs-12 text-white'>Vous êtes administrateur d'actions</h4>";
				echo "<span class='col-xs-12 text-white'>Ce rôle vous offre un accès intégral, y compris en édition, à l’action</span>";
					displayTilsContent($roles["adminAction"]);
			echo "</div>";
		}
		if(isset($roles["partnerAction"]) && !empty($roles["partnerAction"])){
			echo '<div class="col-xs-12 margin-bottom-20 no-padding">';
				echo "<h4 class='col-xs-12 text-white'>Vous êtes partenaire des actions</h4>";
				echo "<span class='col-xs-12 text-white'>Ce rôle vous permet de visualiser l’ensemble de l’action et son contenu. Il permet notamment de commenter ses éléments constitutifs</span>";
				displayTilsContent($roles["partnerAction"]);
			echo "</div>";
		}
		if(isset($roles["referentAction"]) && !empty($roles["referentAction"])){

			echo '<div class="col-xs-12 margin-bottom-20 no-padding">';
				echo "<h4 class='col-xs-12 text-white'>vous êtes referent-e sur les actions</h4>";
				echo "<span class='col-xs-12 text-white'>Ce rôle vous permet de visualiser l’ensemble de l’action et son contenu. Il permet notamment de commenter ses éléments constitutifs</span>";
				displayTilsContent($roles["referentAction"]);
			echo "</div>";	
		}
		?>
    	
    </div>
</div>
<?php 
	function displayTilsContent($roles){
		$incShow=3;
		$count=count($roles);
		$i=1;
		echo "<div class='col-xs-12 no-padding'>";
		foreach($roles as $e => $v){
			if($i<=$incShow){
				drawTilsCTE($e, $v);
				$i++;
			}else{
				drawTilsCTE($e, $v, " style='display:none;'");
			}
		}
		if($count>$incShow){
			echo "<span class='showAllList col-xs-12'><i class='fa fa-caret-down'></i> Tout afficher</span>";
			echo "<span class='reduceList col-xs-12' style='display:none'><i class='fa fa-caret-up'></i> Réduire</span>";
		}	
		echo "</div>";
	}
	function drawTilsCTE($key, $v, $styleNone=""){
		$descr=(@$v["why"]) ? $v["why"] : @$v["description"];
		if(!empty($descr)) $descr="<span class='small-descr col-xs-12 no-padding'>".substr($descr,0, 160)."...</span>";
		$orgaPartner="";
		if(@$v["orgaPartner"]){
			$orgaPartner="<span class='col-xs-12 no-padding'>Vous êtes référent du partenaire <a href='#page.type.".Organization::COLLECTION.".id.".$v["orgaPartner"]["id"]."' class='lbh refLink'>".$v["orgaPartner"]["name"]."</a><span>";
		}
		$orgaPorter="";
		if(@$v["orgaPorter"]){
			$orgaPorter="<span class='col-xs-12 no-padding'>Vous êtes référent de l'organisation porteuse <a href='#page.type.".Organization::COLLECTION.".id.".$v["orgaPorter"]["id"]."' class='lbh refLink'>".$v["orgaPorter"]["name"]."</a><span>";
		}
		$imgPath=(isset($v["profilMediumImageUrl"]) && !empty($v["profilMediumImageUrl"])) ? Yii::app()->createUrl($v["profilMediumImageUrl"]) :  Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl()."/images/thumb/default_".Project::COLLECTION.".png"; 
		echo '<div class="col-xs-12 tilsCTENAT shadow2" '.$styleNone.'>'.
				'<div class="col-xs-12 col-sm-3 col-md-2">'.
					"<img class='img-responsive img-tils pull-right' src='".$imgPath."'>".
				'</div>'.
				'<div class="col-xs-12 col-sm-9 col-md-10 padding-5">'.
					"<a href='#page.type.".Project::COLLECTION.".id.".$key."' class='lbh col-xs-12 title-tils no-padding elipsis'>".$v["name"]."</a>".
					$descr.
					$orgaPartner.
					$orgaPorter.
				'</div>'.
			 '</div>';
	}
?>
<script type="text/javascript">
	jQuery(document).ready(function() {
		coInterface.bindEvents();
		coInterface.bindLBHLinks();
		var $win = $(document); // or $box parent container
        var $box = $(".dashboard-cte");
        var $lbhBtn=$(".lbh, .lbhp, .close-modal");
        //var $log = $(".log");
        
        $win.on("click.Bst", function(event){
        	if ($box.has(event.target).length == 0 //checks if descendants of $box was clicked
                    &&
                    !$box.is(event.target) //checks if the $box itself was clicked
                  ){
                $(".dashboard-cte").hide(200);
            } else {
                if($lbhBtn.has(event.target).length != 0 //checks if descendants of $box was clicked
                    ||
                    $lbhBtn.is(event.target)){
                        $(".dashboard-cte").hide(200);
                }
                //alert("you clicked inside the box");
            }
        });
		$(".showAllList").click(function(){
			$(this).parent().find(".reduceList").show(200);
			$(this).hide();
			$(this).parent().find(".tilsCTENAT").each(function(){
				if(!$(this).is(":visible")) $(this).show(200);
			});
			
		});
		$(".reduceList").click(function(){
			$(this).parent().find(".showAllList").show(200);
			$(this).hide();
			cntInc=0;
			$(this).parent().find(".tilsCTENAT").each(function(){
				if(cntInc>=2)
					$(this).hide(200);
				cntInc++;
			});
		});
	});
</script>