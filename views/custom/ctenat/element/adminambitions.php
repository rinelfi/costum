<style type="text/css">
	ul.listSelect{
		list-style: none
	}
	.indecoloLi.active{
		background-color : #5FAD88;
		color
	}
	.indecoloLi,.delIndecolo{
		cursor: pointer;
		padding:5px;
	}
	.indecoloLi:hover{
		border : 1px solid grey;
	}
	.modal-body {
		height: 500px;
	    overflow-y: scroll;
	}
</style>

<h2 class="text-center">État des lieux écologique</h2>
<?php 
	$keyTpl = "indecolo";
	$kunik = $keyTpl;
	$answerPath = "indecolo";

echo $this->renderPartial("costum.views.custom.ctenat.element.tableEDLE",[
"keyTpl"=>$keyTpl,
"kunik" => $kunik,
"answerPath" => $answerPath,
"id" => $id,
"type" => $type,
"indecolos" => $indecolos,
"indecolosActifs" => $indecolosActifs,
"canEdit" => $canEdit,
"editable" => true
],true);
?>

<script type="text/javascript">
if(typeof dyFObj.elementObjParams == "undefined")
	dyFObj.elementObjParams = {};
dyFObj.elementObjParams.indicateurs = <?php echo json_encode($indecolos); ?>;

var <?php echo $kunik
 ?>Data = <?php echo json_encode( (isset($indecolos)) ? $indecolos : null ); ?>;

var bbindecolo = null;
$(document).ready(function() { 
	mylog.log("render","/var/www/dev/modules/costum/views/tpls/forms/<?php echo $kunik
	 ?>.php");
	
	//adds a line into answer
    $(".add<?php echo $kunik ?>").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");            
        tplCtx.path = $(this).data("path")+((notNull(<?php echo $kunik ?>Data) ? <?php echo $kunik ?>Data.length : "0"));
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?> );
    });

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  	
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });


$(".addIndecolo").click(function(){
	var listHTML = "";
	var filterOptions = "<option value='indecoloLi'>Tous</option>";;
	var filterObjEnv = {};
	$.each(dyFObj.elementObjParams.indicateurs,function(i,ind){
		var activeClass = ( elementData.indecolo && typeof elementData.indecolo[i] != "undefined" ) ? "active"  : "";
		var filterKey = "";
		if(typeof ind.obj_environnemental != "undefined"  ){
			//console.log(ind.obj_environnemental)
			filterKey = ind.obj_environnemental.replace( /[, ]+/g , '', "" );
			if( typeof filterObjEnv[filterKey] == "undefined" ){
				filterObjEnv[filterKey] = ind.obj_environnemental;
				//console.log(filterKey);
				filterOptions += "<option value='"+filterKey+"'>"+ind.obj_environnemental+"</option>";
			}
		}
		if(activeClass == "" && ( typeof ind.hideInList == "undefined" || ind.hideInList == false )  )
			listHTML += "<li class='indecoloLi ind"+i+" "+activeClass+" "+filterKey+" ' data-id='"+i+"' >"+ind.name+"</li>";
	})


	bbindecolo = bootbox.dialog({
      title: "Sélectionnez vos indicateurs",
      message: '<div class="row">' +
            '<div class="col-md-12 text-center"> ' +
            '<div id="indecoloDesc" class="hidden"></div>'+
            '<div id="indecoloListUL" >'+
            	'<select id="filterByObjEnv" class="margin-20" style="width:80%">'+filterOptions+'</select>'+
              '<ul class="listSelect">'+listHTML+
              "<li><br/><a href='javascript:;' class='btn btn-default addNewIndecolo'>Créer un nouvel indicateur</a></li>"+
              '</ul>'+
             '</div>'+
              
          '</div></div>',
      buttons: {
        cancel: {
					label: "Terminer",
					className: "btn-secondary",
					callback: function() {
						urlCtrl.loadByHash( location.hash );
					}
        }
  	}
	}).init(function() { 
				
				$("#filterByObjEnv").change(function(){
					$(".indecoloLi").hide();
					$("."+$(this).val()).show();
				});

        $(".indecoloLi").click(function(){
        	//$(this).toggleClass("active");
        	var lId = $(this).data("id");
        	$("#indecoloListUL").toggleClass("hidden");
        	var htmlrender = '<div class="form-horizontal" style ="border:solid 1px;" >'+
								"<a href='javascript:;' class='pull-right' onclick='emptyFiche()'><i class='fa fa-times fa-3x text-red padding-10'></i> </a>"+
								'<h3>'+dyFObj.elementObjParams.indicateurs[lId].name+'</h3>'+
								"<a href='javascript:addIndicatorEcolo(\""+lId+"\");' class=' btn btn-primary margin-20' onclick='emptyFiche()'><i class='fa fa-plus'></i> Ajouter cet indicateur </a>";
					
					var titlelabel = {
						"definition" : "Définition",
						"obj_environnemental" :  "Objectif Environnemental",
						"pol_pub" : "Politique Publique",
						"obj_strategique" : "Objectif stratégique",
						"obj_operationnel_national" : "Objectif Opérationnel National",
						"methodeCalcul" : "Méthode de calcul",
						"unity1" : "Unité",
						"moyenne_nationale" : "Moyenne Nationale"
					}

					$.each(titlelabel, function( index, value ) {
					  	htmlrender += '<div class="form-group"><label for="" class="col-xs-3 control-label " >'+titlelabel[index]+'</label><div class="col-xs-9"><label for="" class="control-label" style = "text-align: left;font-weight: 100;">'+dyFObj.elementObjParams.indicateurs[lId][index].replace("\n","<br/>")+'</label></div></div>';
					  		
					});
					htmlrender += "<a href='javascript:addIndicatorEcolo(\""+lId+"\");' class=' btn btn-primary margin-20'><i class='fa fa-plus'></i> Ajouter cet indicateur </a>";
					htmlrender += '</div>';
        	$("#indecoloDesc").html(htmlrender).toggleClass("hidden");
	        	
				})

				$(".addNewIndecolo").click(function(){
					dyFObj.openForm("indecolo");
					$(".bootbox").remove();
				})
	    });
	})
	function delIndecolo(lId){
		bootbox.confirm( "Etes vous sur de vouloir supprimer cet indicateur ?",
	    function(result)
	    {
	      	if(result==true){
		      	var indecolItem = {
                collection : contextType,
                id : contextId,
            };
            indecolItem.value = null;

            indecolItem.path = "indecolo."+lId;  
            dataHelper.path2Value( indecolItem , function(params) {
            	delete elementData.indecolo[lId];
            	$('.line'+lId).remove()
          	} );
	      	} 
	    });
	}
	$(".delIndecolo").click(function(){
		var lId = $(this).data("indid");
		delIndecolo(lId);
	});

	$(".editable").blur(function(){
		//alert($(this).data("indid")+" : "+$(this).data("key")+" : "+$(this).text());
		var lId = $(this).data("indid");
		var lkey = $(this).data("key");
		var val = parseFloat($(this).text().replace(",","."));
		var inputId = "#input"+lId+lkey;
		//alert(inputId);
		var go = true;
		if( $(this).data("type") ) 
		{
		//alert($(this).data("type")+" : "+val);
	    	if( $(this).data("type") == "year" && (val < 1990 || val > 2030) )
	    	{
	    		go = false;
	    		toastr.error("Tapez une année valide , svp");
	    		$(this).html("2021");
	    	}
	    }
		if( go && elementData.indecolo && elementData.indecolo[lId][lkey] != val ){
			var indecolItem = {
        collection : contextType,
        id : contextId,
	    };
	    indecolItem.value = val;
			$(this).html( val );
			
		    indecolItem.path = "indecolo."+lId+"."+lkey;  
		    //console.log(".editable",inputId,indecolItem);
		    dataHelper.path2Value( indecolItem , function(params) {
		    	elementData.indecolo[lId][lkey] = indecolItem.value;
		    	
		    	$(inputId).animate({backgroundColor: "#5DAA85"}, "fast").animate({backgroundColor: "#fff"}, "fast");

		    } );
		} 
	});

});
function emptyFiche(){
	$("#ficheIndic").html("");
	$("#indecoloListUL").toggleClass("hidden");
	$("#indecoloDesc").toggleClass("hidden");
}
function addIndicatorEcolo(lId){
	var indecolItem = {
      collection : contextType,
      id : contextId,
  };
  indecolItem.value = {v:""};

  indecolItem.path = "indecolo."+lId;  
  mylog.log(indecolItem)
  dataHelper.path2Value( indecolItem , function(params) {
  	if(typeof elementData.indecolo =="undefined")
  		elementData.indecolo = {};

  	elementData.indecolo[lId] = true;
  	//$(".bootbox").remove();
  	$("#indecoloListUL").removeClass("hidden");
		$("#indecoloDesc").addClass("hidden");
		$('.ind'+lId).addClass("active");
  	//urlCtrl.loadByHash( location.hash );
  } );
}
  	</script>

	
