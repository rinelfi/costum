<?php 

	$cssAnsScriptFilesTheme = array(
		"/plugins/Chart-2.8.0/Chart.min.js",
	); HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
?>



<div id="container" style="margin:20px auto;width:100%">
	<canvas id="<?php echo $id?>-canva"></canvas>
</div>


<script>

	jQuery(document).ready(function() {
		mylog.log("render","/dev/modules/costum/views/custom/franceTierslieux/graph/doughnutCommon.php");
		
			var config = {
					type: 'doughnut',
					data: <?php echo $id ?>Data,
					options: {
						responsive: true,
						legend : {display:false}
					}
				}
					
			var canvas = document.getElementById('<?php echo $id?>-canva');
			var ctx = canvas.getContext('2d');
			window.myDoughnut = new Chart(ctx, config);
				
			
		

			
});
	</script>
