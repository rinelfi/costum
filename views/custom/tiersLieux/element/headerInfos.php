<div class="col-xs-12 col-sm-12 col-md-12 contentHeaderInformation <?php if(@$element["profilBannerUrl"] && !empty($element["profilBannerUrl"])) echo "backgroundHeaderInformation" ?>">	
	<div class="col-xs-12 col-sm-9 col-md-9 col-lg-10 text-white pull-right">
		<?php if (@$element["status"] == "deletePending") { ?> 
			<h4 class="text-left padding-left-15 pull-left no-margin letter-red"><?php echo Yii::t("common","Being suppressed") ?></h4><br>
		<?php } ?>
		<h4 class="text-left padding-left-15 pull-left no-margin">
			<span id="nameHeader">
				<div class="pastille-type-element bg-<?php echo $iconColor; ?> pull-left">
					
				</div>
				<i class="fa fa-<?php echo $icon; ?> pull-left margin-top-5"></i> 
				<div class="name-header pull-left"><?php echo @$element["name"]; ?></div>
			</span>
			<?php if($element["collection"]==Event::COLLECTION && isset($element["type"])){ 
					$typesList=Event::$types;
			?>
				<span id="typeHeader" class="margin-left-10 pull-left">
					<i class="fa fa-x fa-angle-right pull-left"></i>
					<div class="type-header pull-left">
				 		<?php echo Yii::t("category", $typesList[$element["type"]]) ?>
				 	</div>
				</span>
			<?php } ?>
		</h4>	

		<!-- <?php if($edit){ ?>
			<button class="btn-danger text-white pull-right" style="border: none;padding: 5px 10px;border-radius: 5px;" onclick="directory.deleteElement('<?php echo $element["collection"] ?>', '<?php echo (string)$element["_id"] ?>');"><i class="fa fa-trash"></i> Supprimer</button>
		<?php } ?>	 -->			
	</div>

<?php 
	$classAddress = ( (@$element["address"]["postalCode"] || @$element["address"]["addressLocality"] || @$element["tags"]) ? "" : "hidden" );

	if(!empty($cteRParent))
		$classAddress = "";
//if(@$element["address"]["postalCode"] || @$element["address"]["addressLocality"] || @$element["tags"]){ ?>
	<div class="header-address-tags col-xs-12 col-sm-9 col-md-9 col-lg-10 pull-right margin-bottom-5 <?php echo $classAddress ; ?>">
		<?php
		 if(!empty($element["address"]["addressLocality"])){ ?>
			<div class="header-address badge letter-white bg-red margin-left-5 pull-left">
				<?php
					echo !empty($element["address"]["streetAddress"]) ? "<i class='fa fa-map-marker'></i> ".$element["address"]["streetAddress"].", " : "";
					echo !empty($element["address"]["postalCode"]) ? 
							$element["address"]["postalCode"].", " : "";
					echo $element["address"]["addressLocality"];
				?>
			</div>
		<?php
		} ?>	

		<div class="header-tags pull-left" style="margin-top: 5px;">
		<?php 
		if(@$element["tags"]){ 
			foreach ($element["tags"] as $key => $tag) { ?>
				<a  href="#search?text=#<?php echo $tag; ?>"  class="badge letter-red bg-white lbh" style="vertical-align: top;">#<?php echo $tag; ?></a>
			<?php } 
			
		} ?>
		</div>
	</div>

<?php 
	$this->renderPartial('co2.views.element.menus.answerInvite', 
    			array(  "invitedMe"      => $invitedMe,
    					"element"   => $element
    					) 
    			); 
 	?>
</div>

