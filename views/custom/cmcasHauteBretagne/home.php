<?php
	$cssAnsScriptFilesTheme = array(
      // SHOWDOWN
      '/plugins/showdown/showdown.min.js',
      // MARKDOWN
      '/plugins/to-markdown/to-markdown.js'            
    );

	$cssAndScriptFilesModule = array(
		'/js/default/profilSocial.js',
	);

	$cssAnsScriptFilesModuleMap = array( 
		'/leaflet/leaflet.css',
		'/leaflet/leaflet.js',
		'/css/map.css',
		'/markercluster/MarkerCluster.css',
		'/markercluster/MarkerCluster.Default.css',
		'/markercluster/leaflet.markercluster.js',
		'/js/map.js',
	);

	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModuleMap, Yii::app()->getModule( Map::MODULE )->getAssetsUrl() );

    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
     
	HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());

	if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
		$el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );

		$blockCmss = PHDB::find(Cms::COLLECTION, 
                  array( '$or' => 	array(
                  						array("source.key" => $this->costum["slug"]), 
                  					array("parent.".$this->costum["contextId"] => array('$exists'=>1), 
			                         "parent.".$this->costum["contextId"].".type"=>$this->costum["contextType"],
			                         "page" => "welcome") ) ));

  		$blockCmsById = [];

  		foreach ($blockCmss as $key => $value) {
  			if (isset( $value["id"] )) {
  				$blockCmsById[$value["id"]] = $value;
  			}
  		}
  		$page = "welcome";
	}

	$bannerImg = @$el["profilBannerUrl"] ? Yii::app()->baseUrl.$el["profilBannerUrl"] : Yii::app()->getModule("costum")->assetsUrl."/images/cmcasHauteBretagne/header-bg.png";

	$params = [  "tpl" => "cmcasHauteBretagne","slug"=>$this->costum["slug"],"canEdit"=>false,"el"=>$el ];
    echo $this->renderPartial("costum.views.tpls.acceptAndAdmin", $params,true );
?>

<style type="text/css">
	.showSlide {
		border-color: #2c407a !important;
		width: 15px !important;
		height: 15px !important;
		border-radius: 100%;
		position: relative;
		bottom: -60px;
	}

	.button-rejoin{
		background-color: #2c407a ;
		color : white ;
		position: relative;
		top: 12px;
		padding:6px;
		border-color: transparent;
		border-radius: 10px;
	}

	.button-rejoin:hover{
		color : white ;
	}

	.editThisBtn{
		position: relative;
		z-index: 100;
	}
	.deleteThisBtn{
		position: relative;
		z-index: 100;
	}

	#about-cmcas .members{
		color: #2c407a;
		margin-top: 5px;
	}
</style>

<div id="header-wrapper" style="background: #fff url(<?= $bannerImg; ?>) no-repeat center;background-size: cover;">
	<div id="header" class="container-cmcas" >
		<div id="signe-cmcas" class="pull-right">
			<img style="position: relative;top: 80px;" class="img-responsive" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/cmcasHauteBretagne/png_logo_AS_R.png">
		</div>
		<div id="about-cmcas" class="well col-md-6 col-sm-8 col-xs-12 col-md-offset-3 col-sm-offset-2">
			<?php 
			$params = array( 
              "cmsList" => $blockCmss,
              "tag"=> "whoCmcas",
              "color" => "black"
          	);	
			echo $this->renderPartial("costum.views.tpls.blockCms.textImg.text",$params,true); ?>
			<?php 
			if (!empty(Yii::app()->session["userId"])) {
				if (Authorisation::isElementMember($this->costum["contextId"],$this->costum["contextType"],Yii::app()->session["userId"]) != true) {
					echo '<a href="javascript:links.connect(\''.$this->costum["contextType"].'\',\''.$this->costum["contextId"].'\',\''.Yii::app()->session["userId"].'\',\''.Person  ::COLLECTION.'\',\'member\')" class="button-rejoin">';
						echo "Rejoindre ".$this->costum["slug"];
					echo "</a>";
				}else{
					echo '<p style="font-size:14px !important;" class="members margin-left-10 margin-right-10">';
						echo "<i style='font-size:19px;color:#2c407a;' class='fa fa-check-circle' aria-hidden='true'></i> Déja membre de ".$this->costum["slug"];
					echo "</p>";
				}
    		} 
    		else{
    			echo '<a style="margin-top:10px;" href="javascript:;" id="" class="menu-button btn-menu btn-menu-tooltips menu-btn-top btn btn-menu-connect margin-left-10 margin-right-10 menu-btn-top" data-toggle="modal" data-target="#modalLogin"><i class="fa fa-sign-in"></i> Veuillez vous connecté afin rejoindre cette organisations </a>';
    		}
			?>
		</div>	
	</div>
</div>

<!-- <video controls id="about-video" class="col-md-4 col-sm-6 col-xs-12 col-md-offset-4 col-sm-offset-3">
 --> <!--  <source src="https://vimeo.com/331380215" type="video/mp4">
  <source src="https://vimeo.com/331380215" type="video/ogg"> -->
  	<iframe style="margin-top: -80px;" id="about-video" class="col-md-4 col-sm-6 col-xs-12 col-md-offset-4 col-sm-offset-3" src="https://player.vimeo.com/video/485496297?loop=1" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
	<!-- <p>
		<a href="https://vimeo.com/331380215">C&#039;est quoi les Activit&eacute;s Sociales ?</a> from <a href="https://vimeo.com/ccas">CCAS</a> on <a href="https://vimeo.com">Vimeo</a>.
	</p> -->
  <!-- Your browser does not support HTML video. -->
<!-- </video>
 --><!--------------------- End About ----------------------------->


<!--------------------- Agenda ---------------------------------->
<div id="agenda-cmcas" class="featured-agenda col-xs-12">
	<div class="container text-center col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
		<h2>L'agenda</h2>
		<div class="featured-services-grids">
			<div class="col-md-4 col-sm-4 col-xs-12 featured-services-grid space-top-bottom">
				<div class="featured-services-grd" style="background: url(<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/cmcasHauteBretagne/featured-1.jpg) center;">
					
					<div class="more">
						<a href="#cafelitteraire" class="lbh hvr-curl-bottom-right"  style="background-color: green">Café littéraire</a>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-sm-4 col-xs-12 featured-services-grid space-top-bottom">
				<div class="featured-services-grd" style="background-size: cover !important;background: url(<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/cmcasHauteBretagne/featured-2.jpg) center;">
					
					<div class="more">
						<a href="#faisdesbulles" class="lbh hvr-curl-bottom-right"   style="background-color: blue">Fais des bulles</a>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-sm-4 col-xs-12 featured-services-grid space-top-bottom">
				<div class="featured-services-grd" style="background: url(<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/cmcasHauteBretagne/featured-3.jpg) center;">
					
					<div class="more">
						<a href="#blablabla" class="lbh hvr-curl-bottom-right" style="background-color: red">Blablabla</a>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-sm-4 col-xs-12 featured-services-grid space-top-bottom">
				<div class="featured-services-grd" style="background: url(<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/cmcasHauteBretagne/featured-5.jpg) center;background-size: cover !important;">
					
					<div class="more">
						<a href="#bienetreenfamille" class="lbh hvr-curl-bottom-right" style="background-color: #E91E63;">Bien-être en famille</a>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-sm-4 col-xs-12 featured-services-grid space-top-bottom">
				<div class="featured-services-grd" style="background: url(<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/cmcasHauteBretagne/featured-4.jpg) center;">
					
					<div class="more">
						<a href="#carteblanche" class="lbh hvr-curl-bottom-right" style="background-color: purple">Carte Blanche</a>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-sm-4 col-xs-12 featured-services-grid space-top-bottom">
				<div class="featured-services-grd" style="background: url(<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/cmcasHauteBretagne/featured-9.jpg) center;">
					
					<div class="more">
						<a href="#quoideneuf" class="lbh hvr-curl-bottom-right" style="background-color: #FF9800">Alors, quoi de neuf</a>
					</div>
				</div>
			</div>

			<div class="clearfix"> </div>
		</div>
	</div>
</div>

<div id="wave"></div>

<div class="bg-sobre no-padding item-ctr">
		    	<!-- Contain the slide agenda the code is a the end off the file  -->
</div>
<!--------------------- End Agenda ---------------------------------->

<?= $this->renderPartial("costum.views.tpls.blockCms.article.blockarticlescmcas",
										array(
											"canEdit"	=> true,
											"page"	=> @$page,
											"idToPath" => "cmcasHauteBretagne.welcome.blockarticlescmcas",		
											"blockCms"	=> @$blockCmsById["cmcasHauteBretagne.welcome.blockarticlescmcas"]
										)
								); ?>
<!--------------------- End Actualité ---------------------------------->


<div id="autre-cmcas" class="bg-sobre-autre">
<?php echo $this->renderPartial("costum.views.tpls.multiblocksCaroussel",
										array(
											"canEdit"=> true,
											"blockName" => "cmcas",
											"blockCt"   => 1,
											"titlecolor" => "black",
											"cmsList"=> $blockCmss,
											"eltcolor" => "black",
											"blockTpl" => "blockCms.textImg.CmcasTxtImg"
										)
								); 
?>
</div>

<!-- <div style="z-index: 1;height: 500px;" class="col-md-12 mapBackground no-padding" id="mapCmcas">
</div> -->
<!-- <div class="col-md-12 col-xs-12 col-sm-12 text-center">
	<h1 style="color: #2c407a;text-decoration: none;" class="title-map">Notre communautée</h1> -->
	<!-- <?php //echo $this->renderPartial("costum.views.tpls.map.mapCommunity",
										//array(
	 										//"canEdit"=> false
										//)
								//); 
	?>
 --><!-- </div> -->


<script type="text/javascript">
	jQuery(document).ready(function() {

		contextData = <?php echo json_encode($el); ?>;
	    var contextId=<?php echo json_encode((string) $el["_id"]); ?>;
	    var contextType=<?php echo json_encode($this->costum["contextType"]); ?>;
	    var contextName=<?php echo json_encode($el["name"]); ?>;
	    contextData.id=<?php echo json_encode((string) $el["_id"]); ?>;

		params = {
			"sourceKey" : costum.contextSlug
		};

		ajaxPost(
			null, 
			baseUrl+"/costum/cmcashautebretagne/getevents", 
			params,
			function(events){ 
				mylog.log("events : ",events);
				if (events.result == true) {
					str = "";
					i = 0 ;
					slide = 0;

					str += '<div class="container-slide-agenda text-white col-xs-12">';
						str += '<div class="carousel slide row" id="myCarousel">';
							str += "<div class='carousel-inner'>";
								str += "<div class='item active'>";

					$.each(events.element,function(key,value){
						var startHour = moment(value.startHour).format('H:mm');

						if (i >= 4) {
							str += "</div>";
							str += "<div class='item'>";
							i = 0;
							slide++;
						}
						

			            str += '<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">';
			            	str += '<div class="well">';
			            		str += '<a style="border-color: transparent ;background-color: #fcc845 ; color: #2c407a ;position:relative;z-index:10;" class="lbh-preview-element btn btn-success pull-right" title="En savoir plus" href="#page.type.'+value.collection+'.id.'+value.id+'"> <i class="fa fa-chevron-right" aria-hidden="true"></i> </a>';
			            		str += '';
		            			str += '<h1 class="agenda-date-day">'+value.startDate+'</h5>'
						    	str += '<h4 class="agenda-date-month">'+value.startMonth+'</h6>';
						    	str += '<p>'+value.name+'</p>';
							    str += '<div class="agenda-body">';
							    	str += '<p class="agenda-text">';
							    		str += value.address+'<br>';
							    		str += value.startDay +' à partir de '+ startHour +' <br>';
							    		// salle Parcel Paul
							    	str += '</p>';
							    	str += '<p class="agenda-text">';
							    		str += '<span class="agenda-text-contact">contact:<br>'; 
								    		str += '<span class="name">';
								    			str += value.email+'<br>'
								    			str += value.nbrattendees+" Participants";
								    		str += '</span><br>';
								    		str += value.phone;
							    		str += '</span>';
							    			<?php if (!empty(Yii::app()->session["userId"])) { ?>
							    			if (typeof value["attendees"] != "undefined" &&  value["attendees"] != null && value["attendees"][userId]) {
							    				str += "Déjà participant";
							    			}else{
							    				str += '<a href="javascript:links.connect(\''+value.collection+'\',\''+value.id+'\',\''+userId+'\',\'citoyens\',\'attendee\')" class="btn btn-default agenda-link">Participer</a>';
							    			}
							    		<?php }else{ ?>
							    			str += '<a style="margin-top:10px;" href="javascript:;" id="" class="text-white menu-button btn-menu btn-menu-tooltips menu-btn-top btn btn-menu-connect margin-left-10 margin-right-10 menu-btn-top" data-toggle="modal" data-target="#modalLogin"><i class="fa fa-sign-in"></i> Se connecter pour participer </a>';
							    		<?php } ?>
							    	str += '</p>';
							    str += '</div>';
			            	str += '</div>';
			            str += '</div>';
			            i++;
					});

					str += "</div>";
						str += "</div>";

					if (slide >= 1) {	
						str += '<a class="left carousel-control" href="#myCarousel" data-slide="prev"><i class="glyphicon glyphicon-chevron-left hidden-lg hidden-md"></i></a>';
		    			str += '<a class="right carousel-control" href="#myCarousel" data-slide="next"><i class="glyphicon glyphicon-chevron-right hidden-lg hidden-md"></i></a>';
					}

							str += "</div>";
								str += "</div>";

				}else{
					str = "Il n'éxiste aucun événement(s)";
				} 

				$(".item-ctr").html(str);
				coInterface.bindLBHLinks(); 
			},
			function(){
				mylog.log("error events");
			}
		);
        
		jQuery('.carousel[data-type="multi"] .item').each(function(){
			var next = jQuery(this).next();
			if (!next.length) {
				next = jQuery(this).siblings(':first');
			}
			next.children(':first-child').clone().appendTo(jQuery(this));
		  
			for (var i=0;i<2;i++) {
				next=next.next();
				if (!next.length) {
					next = jQuery(this).siblings(':first');
				}
				next.children(':first-child').clone().appendTo($(this));
			}
		});

		ajaxPost(
			null, 
			baseUrl+"/costum/cmcashautebretagne/getarticles", 
			params,
			function(results){ 
				mylog.log("articles :",results);

				if (results.dataArticles = true) {

					var ctr = "";
					var ctCol = 0;
					var color = [
						"#e36f41",
						"#791880",
						"#00a890",
						"#dc094b"
					];
					if(typeof results.article != "undefined" && results.article != null){
						$.each(results.article,function(key,value){

							var imgMedium = (value.imgMedium != "none") ? value.imgMedium : url;

							ctr += '<div class="col-md-6 col-sm-6 col-xs-12 grid-actu">';
								ctr += '<div class="well">';
									ctr += '<div class="contain">';
										ctr += '<div class="date-actu">';
											ctr += '<a href="#" class="actu-link"  style="background-color: '+color[ctCol]+'">';
												ctr += value.dCreate+' '+value.mCreate+' '+value.yCreate;
											ctr += '</a>';
										ctr += '</div>';
										ctr += '<div class="actu-name">';
											ctr += value.name;
										ctr += '</div>';
										ctr += '<a href="#page.type.'+value.collection+'.id.'+value.id+'" class="lbh-preview-element btn btn-default btn-more-actu">En savoir plus</a>';
									ctr += '</div>';
									ctr += '<div class="featured-services-grid pull-right">';
										ctr += '<div class="featured-services-grd" style="background: url('+baseUrl+value.imgMedium+') center;">';
										ctr += '</div>';
									ctr += '</div>';
								ctr += '</div>';
							ctr += '</div>';
							ctCol++;
						});
					}
					
					
				}else{
					ctr += "Aucune actualité pour le moment";
				}

				$(".result-actus").html(ctr);
				coInterface.bindLBHLinks();
			},
			function(){
				mylog.log("error articles");
			}
		);
	});
	
</script>