<style type="text/css">
	.createBlockBtn{
		margin-top: 10px;
	}

	#acceptAndAdmin{
		display: none;
	}

	#simple-img #carousel-autre{
		background-color: transparent;
	}

    #simple-img{
        margin-top: 2%;
    }

    /*.simpleTextAndImg .title {
        color: blue;
    }*/
</style>

<?php
    $page = "Faitdesbulles";

	if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){

		$el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );
        $cmsList = PHDB::find( Cms::COLLECTION,array( '$or' => array(   array("page" => $page),
                                                                        array("source.key" => $this->costum["slug"])) 
                                                                    ));
	}

	if (Authorisation::isElementAdmin($this->costum["contextId"],$this->costum["contextType"],Yii::app()->session["userId"])) 
		$canEdit = true;
	else 
		$canEdit = false;

	$structField = "structags";

	$params = [  "tpl" => "cmcasHauteBretagne","slug"=>$this->costum["slug"],"canEdit"=>$canEdit,"el"=>$el,"page" => $page ];
	echo $this->renderPartial("costum.views.tpls.acceptAndAdmin",$params,true);

	echo '<div class="no-padding col-xs-12 col-sm-12 col-lg-12">';
		$params = array( 
          	"cmsList" => $cmsList,
          	"canEdit" => $canEdit,
          	"eltColor" => "blue",
            "page"  =>  $page,
          	"tag"=> "faitdesbulles");

        echo $this->renderPartial("costum.views.tpls.blockCms.textImg.simpleTextAndImg" ,$params,true);
	echo '</div>';

	echo $this->renderPartial("costum.views.custom.cmcasHauteBretagne.tpls.textRight", 
										[ 
											"cmsList" => $cmsList, 
											"canEdit" => $canEdit, 
                                            "page"  =>  $page,
											"tag" => "faitdesbulles2"
										],true);


	echo $this->renderPartial("costum.views.custom.cmcasHauteBretagne.tpls.textLeft", 
										[ 
											"cmsList" => $cmsList, 
											"canEdit" => $canEdit, 
                                            "page"  =>  $page,
											"tag" => "faitdesbulles3"
										],true);
?>

<?php
foreach ($cmsList as $e => $v) {
    if (!empty($v["type"]) && isset($v["type"])) {
        $params = [
            "cmsList"   =>  $cmsList,
            "blockCms"  =>  $v,
            "page"      =>  $page,
            "thematique"    =>  "Fait des bulles",
            "canEdit"   =>  $canEdit,
            "type"      =>  $v["type"]
        ];
        echo $this->renderPartial("costum.views.".$v["type"],$params);
    }
}
?>


<?php if(Authorisation::isInterfaceAdmin()){ ?>
    <div class="col-xs-12">
        <center>
            <a href="javascript:;" class="addTpl btn btn-primary" style="background-color: blue;margin-top: 2%;color: white;"  data-key="blockevent"  data-collection="cms"><i class="fa fa-plus"></i> Ajouter une section</a>
        </center>
    </div>
<?php } ?>

<?php 
    echo '<div id="simple-img" class="bg-sobre-autre">';
        echo $this->renderPartial("costum.views.tpls.multiblocksCaroussel",
                                        array(
                                            "canEdit"=> false,
                                            "blockName" => "imgFaitdesbulles",
                                            "blockCt"   => 1,
                                            "titlecolor" => "black",
                                            "cmsList"=> $cmsList,
                                            "eltcolor" => "black",
                                            "blockTpl" => "blockCms.textImg.textImg"
                                        )
                                );
    echo '</div>';
?>

<script type="text/javascript">
	var dynFormCostumIframe = {
	    "beforeBuild":{
	        "properties" : {
                "media" : {
                    "inputType" : "text",
                    "label" : "Liens iFrame",
                    "placeholder" : "http://exemple.com",
                    "groupOptions" : false,
                    "groupSelected" : false,
                    "optionsValueAsKey":true
                }
	        }
	    }
	};

	$(".editThisBtn").off().on("click",function (){
        mylog.log("editThisBtn");
        var id = $(this).data("id");
        var type = $(this).data("type");
        dataObj = { parent : {} };
        dataObj.parent[userConnected._id.$id] = { collection : userConnected.type, name : userConnected.name };

        dyFObj.editElement(type,id,dataObj,dynFormCostumIframe);
    });

    $(".createBlockBtn").off().on("click",function (){
        mylog.log("createBlockBtn");
        var tag = $(this).data("tag");
        dynFormCostumIframe.afterSave = function() { 
        	window.location.reload();
        };

        dataObj = { parent : {} };
        dataObj.parent[userConnected._id.$id] = { collection : userConnected.type, name : userConnected.name };
        dataObj.structags = tag;
        dyFObj.openForm( "cms" , null , dataObj , null , dynFormCostumIframe );
    });
</script>