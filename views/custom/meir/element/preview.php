<style>
    #modal-preview-coop{
        overflow: auto;
    }
    .short-description{
        font-size:20px;
        text-align: justify;
    }
    .description-preview{
        text-align: justify;
    }
    .description-preview.activeMarkdown p, .description-preview.activeMarkdown li{
        font-size: 14px !important;
    }
    .link-files{
        border-radius: 3px;
        padding: 5px 10px;
    }




    .heading img{
        width: 10%;
    }

    .preview-info{
        padding: 5%;
        background:#fff;
        box-shadow: 0px 0px 4px 0px #b0b3b7;
        margin-top: 10px;
        margin-bottom: 20px;
    }
    .preview-image{
        text-align:center;
    }
    .preview-image img{
        border-radius:10%;
    }
    .preview-content{
        text-align:left;
    }
    .preview-content .title-preview {
        font-weight:600;
        text-transform: none;
    }
    .preview-content .preview-shortDescription {
        font-size: 18px;
    }
    .preview-content .header-address {
        font-size: 18px;
        font-weight: 500;
        color: #428bca;
        padding: 5px;
    }
    .description-preview {
        margin-left: 5%;
        margin-right: 5%;
    }
    .preview-content .item-content-block{padding:20px; border-top:2px solid #f6f6f2; background-color:transparent; display:block;}
    .preview-content .tags a{background-color:#756f5d; padding:10px; color:#fff; display:inline-block; font-size:11px; line-height:11px; border-radius:2px; margin-bottom:5px; margin-right:2px; text-decoration:none;}
    .preview-content .tags a:hover{background-color:#a38018;}
    .description-preview {

    }
    @media (max-width: 1199px) {
        .preview-content{
            text-align:center;
        }
        .preview-content .title-preview {
            text-align: center;
            font-size: 24px;
        }
        .preview-content .header-address {
            text-align: center;
        }
        .preview-content .preview-shortDescription {
            text-align: center;
        }
    }
</style>
<?php


$element = [];
    $element=PHDB::findOne($collection,array("_id"=> new MongoId($id)));

//var_dump($element);
?>
<div class="margin-top-25 margin-bottom-50 col-xs-12">
    <div class="col-xs-12 no-padding">
        <button class="btn btn-default pull-right btn-close-preview" style="margin-top:-15px;">
            <i class="fa fa-times"></i>
        </button>


        <!--<a href="javascript:;" onclick="dySObj.openSurvey('octosource','json')" class="btn btn-primary col-xs-12"  style="width:100%">C'est parti <i class="fa fa-arrow-circle-right fa-2x "></i></a>-->
    </div>

    <div class="col-xs-12 preview-info">
        <div class="row">
            <div class="col-lg-4 col-md-12 col-sm-12 col-xl-12">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="preview-image">
                            <!--<img src="<?php /*echo Yii::app()->createUrl('/'.$element["profilMediumImageUrl"]) */?>" alt="image" />-->
                            <?php if(isset($element["profilMediumImageUrl"]) && !empty($element["profilMediumImageUrl"])){ ?>
                                <a href="<?php echo Yii::app()->createUrl('/'.$element["profilImageUrl"]) ?>"
                                   class="thumb-info"
                                   data-title="<?php echo Yii::t("common","Profil image of")." ".$element["name"] ?>"
                                   data-lightbox="all">
                                    <img class="img-responsive" onload='eltImgPreview(this)' style="box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.5);" src="<?php echo Yii::app()->createUrl('/'.$element["profilMediumImageUrl"]) ?>" />
                                </a>
                            <?php }else{ ?>
                                <img class="img-responsive shadow2 thumbnail" onload='eltImgPreview(this)' style="box-shadow: 0px 0px 8px 2px rgba(0,0,0,0.5);" src="<?php echo $this->module->assetsUrl ?>/images/thumbnail-default.jpg"/>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-12 col-sm-12 col-xl-12">
                <div class="preview-content">
                    <h1 class="title-preview"><?php echo $element["name"]; ?></h1>
                    <?php if(isset($element["shortDescription"]) && !empty($element["shortDescription"]) ) echo "<div class='col-xs-12 preview-shortDescription'>".$element["shortDescription"]."</div>"; ?>
                    <?php
                    if(!empty($element["address"]["addressLocality"])){ ?>
                        <div class="header-address col-xs-12 blockFontPreview">
                            <?php
                            echo !empty($element["address"]["streetAddress"]) ? "<i class='fa fa-map-marker'></i> ".$element["address"]["streetAddress"].", " : "";
                            echo !empty($element["address"]["postalCode"]) ?
                                $element["address"]["postalCode"].", " : "";
                            echo $element["address"]["addressLocality"];
                            ?>
                        </div>
                    <?php } ?>
                    <div class=" col-xs-12 item-content-block tags">
                        <?php
                        if(isset($element["tags"]) && $element["collection"]!=Project::COLLECTION){
                            foreach ($element["tags"] as $key => $tag) { ?>
                                <a  href="javascript:;"  class="" style="vertical-align: top;">#<?php echo $tag; ?></a>
                            <?php }
                        } ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="col-xl-12 description-preview markdown-txt"><?php echo @$element["description"]; ?></div>
</div>
<script>
    jQuery(document).ready(function() {

        if($(".description-preview").hasClass("markdown-txt")){
            descHtml = dataHelper.markdownToHtml($(".description-preview").html());
            $(".description-preview").html(descHtml);
        }

    });


</script>


