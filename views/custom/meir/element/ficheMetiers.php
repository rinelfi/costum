
<style type="text/css">
    body {
        background-color: #f5f7fb;
    }
    .title-subtitle {
        text-align: center;
    }
    .card-title {
        margin-top: 5px;
        margin-bottom: 5px;
        font-size: 30px;
        font-weight: 700;
        text-decoration: underline;
    }
    .card-subtitle {
        margin-top: 5px;
        margin-bottom: 10px;
        font-size: 40px;
        font-weight: 700;
    }
    div[class^="illustration-border"] {
        position: absolute;
        background-color: #c5d117;
    }
    .illustration-border-left-1 {
        width: 4px;
        height: 60px;
        top: 0;
        left: 0;
    }
    .illustration-border-left-2 {
        width: 10%;
        height: 4px;
        top: 0;
        left: 0;
    }
    .illustration-border-right-1 {
        width: 4px;
        height: 60px;
        bottom: 0;
        right: 0;
    }
    .illustration-border-right-2 {
        width: 10%;
        height: 4px;
        bottom: 0;
        right: 0;
    }
	.fiche-img {
		width: 100%;
		height: 300px;
		object-fit: none;
	}
	.well-fiche.text-blue-fiche p {
		font-size: 20px !important;
	}
    .fiche-parag {
        color: #093081;
        font-size: 25px;
        margin: 15px 0px;
        text-align: center;
    }
    .bg-blue-fiche {
        background-color: #90b2f8;
    }
    .bg-white {
        background-color: #ffffff;
    }
    .text-blue-fiche {
        color: #093081;
    }
    .well-fiche {
        margin: 15px 0px;
        font-size: 20px;
        border-radius: 15px;
        word-break: break-word;
    }
    .label-fiche {
        border-radius: 0px;
        padding: 5px 60px;
        position: relative;
        min-width: 45%;
        text-align: center;
        display: inline-block;
        margin-bottom: 25px;
        font-size: 20px;
        margin-top:5px;
    }
    .well-fiche .read-more {
        display: block;
        width: 45px;
        height: 45px;
        line-height: 45px;
        border-radius: 50%;
        background: #fff;
        color: #90b2f8;
        border: 1px solid #e7e7e7;
        font-size: 18px;
        margin: 0 auto;
        position: absolute;
        top: -9px;
        left: 0;
        right: 0;
        transition: all 0.3s ease-out 0s;
        text-align: center;
        font-weight: 700;
        text-decoration: none;
    }
    .fiche-logo .logo {
        height: 100px;
        margin: auto;
		margin-bottom: 30px;
    }
    .fiche-logo {
        text-align: center;
    }
    .fiche-logo .description {
        font-weight: 700;
        margin-top: 20px;
    }

    @media (max-width:767px)  {
        .card-title {
            font-size: 22px;
        }
        .card-subtitle {
            font-size: 35px;
        }
        .fiche-parag {
            font-size: 20px;
        }
        .well-fiche {
            font-size: 15px;
        }
        .label-fiche {
            padding: 5px 10px;
            margin-bottom: 20px;
            font-size: 14px;
        }
        .well-fiche .read-more {
            top: -23px;
        }
        .fiche-themetique {
            margin-top: 30px;
        }
        .fiche-logo .logo {
            height: 40px;
        }
    }

	 #social-header{
		display: none;
	}
</style>
<div class="col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12 ">
	<div class="row">

		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<div class="title-subtitle col-xs-12">
				<div class="illustration-border-left-1"></div>
				<div class="illustration-border-left-2"></div>
				<div class="card-title text-blue-fiche">
					Les fiches métiers des acteurs du MEIR
				</div>
				<div class="card-subtitle text-blue-fiche">
				<?php if (isset($element["jobFamily"])) {
					if(is_array($element["jobFamily"])){
						foreach($element["jobFamily"] as $kjobf => $vjobf){
							echo $vjobf."</br>";
						}
					}else {
						echo $element["jobFamily"];
					}
				}
						 
					?>
				</div>
				
				<div class="illustration-border-right-1"></div>
				<div class="illustration-border-right-2"></div>

				
			</div>
			<div class="fiche-parag text-blue-fiche col-xs-12">
				<?= $element["name"]?>
				<?php if(Authorisation::isInterfaceAdmin()){ ?>
					<div>
						<a href="javascripr:;" class="editFiche pull-left btn letter-blue" data-id="<?= (String)$element["_id"]?>" > <i class="fa fa-pencil"></i> Éditer les informations</a>
					</div>
				<?php } ?>
			</div>
			<div class="well well-fiche col-xs-12 text-blue-fiche bg-blue-fiche" >
				<b>Adresse:</b> 
				<?php if (isset($element["address"])){
					if (isset($element["address"]["streetAddress"] ))	
						echo $element["address"]["streetAddress"];
					echo $element["address"]["postalCode"].$element["address"]["addressLocality"];	
					?>
				<?php }?>
				<br><br>
				<b>Téléphone: </b> <?php if (isset($element["telephone"])) 
						echo $element["telephone"]["mobile"][0]; 
					?>
				<br>
				<b>Mail: </b><?php if (isset($element["email"])) 
				echo $element["email"]; ?>
				<br><br>
				<b>Lien: </b> <span class="markdown"><?php if (isset($element["link"])) 
						echo $element["link"]; 
					?>
				</span>
			</div>
		</div>
		<?php 
		if(isset($element["profilImageUrl"])) 
			$src = $element["profilImageUrl"];
		else 
			$src =  Yii::app()->getModule("costum")->assetsUrl."/images/meir/TIERS-LIEUX.png";
		?>
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
			<img  class="img-responsive fiche-img" src = "<?= $src?>">
			<div class="well well-fiche text-blue-fiche bg-white" >
				<b>Statut Juriqique:
					<br>
					<?php if (isset($element["legalStatus"])) 
						echo $element["legalStatus"]; 
					?>
				</b>
				<br><br>
				<b>Responsable: <br>
					<?php if (isset($element["responsable"])) 
						echo $element["responsable"]; 
					?>
				</b>

			</div>
		</div>

	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="well well-fiche text-blue-fiche bg-white" >
				<div class="text-center">
					<span class="label label-fiche bg-blue-fiche">
						OBJECTIF(S)
					</span>
				</div>
				<p>
				<span class="markdown"><?php if (isset($element["objective"])) 
						echo $element["objective"]; 
					?>
				</span>
				</p>
			</div>
		</div>
	</div>
	<div class="row">
	<?php if (isset($element["linkFinancialDevice"])) {
		foreach($element["linkFinancialDevice"] as $kLink => $vLink){
		?>
				
			<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
				<div class="well well-fiche text-blue-fiche bg-white fiche-themetique" >
					<a  class="read-more" title="1"><?= $kLink+1 ?></a>
					<p>
						<b><?= @$vLink["financialDevice"]?></b><br>
						<a href="<?= $vLink["link"]?>" target="_blank"><?= $vLink["link"]?></a>
					</p>
				</div>
			</div>
		<?php }
	} ?>
	</div>
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class="well well-fiche text-blue-fiche bg-white" >
				<div class="text-center">
					<span class="label label-fiche bg-blue-fiche">
						PARTENAIRE(S) FINANCIER(S)
					</span>
					<div class="row">
						<?php if (isset($element["financialPartners"])) {
							foreach($element["financialPartners"] as $kFin => $vFin){
							?>
							<div class=" col-lg-4 col-md-4 col-sm-4 col-xs-4 fiche-logo">
								<!-- <img src="logo-fiche/region.png" class="img-responsive logo"> -->
								<div class="description">
									<?= $vFin["financialPartner"]?>
								</div>
							</div>
							<?php } 
						}?>
					</div>

				</div>

			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class="well well-fiche text-blue-fiche bg-white" >
				<p>
					<b>Montant maximal :</b><br>
					<?php if (isset($element["maximumAmount"])) 
						echo $element["maximumAmount"]; 
					?><br>
					<b>Public cible</b><br>
					<?php if (isset($element["publicCible"])) 
						echo $element["publicCible"]; 
					?><br>
					<b>Condition d'éligibilité :</b><br>
					<?php if (isset($element["conditionsEligibility"])) 
						echo $element["conditionsEligibility"]; 
					?><br>
					<b>Référencements :</b><br>
					<?php if (isset($element["referencing"])) 
						echo $element["referencing"]; 
					?>
				</p>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class="well well-fiche text-blue-fiche bg-white" >
				<a class="read-more" title="+"><i class="fa fa-plus"></i></a>
				<div class="text-center">
					<span class="label label-fiche bg-blue-fiche">
						POINTS FORTS :
					</span>
				</div>
				<p class="markdown">
					<?php if (isset($element["strongPoints"])) 
						echo $element["strongPoints"]; 
					?>
				</p>
			</div>
		</div>
		<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<div class="well well-fiche text-blue-fiche bg-white" >
				<a class="read-more" title="--"><i class="fa fa-minus"></i></a>
				<div class="text-center">
					<span class="label label-fiche bg-blue-fiche">
						POINTS FAIBLE :
					</span>
				</div>
				<p class="markdown">
					<?php if (isset($element["weaknesses"])) 
						echo $element["weaknesses"]; 
					?>
				</p>
			</div>
			<div class="row">
				<div class=" col-lg-4 col-md-4 col-sm-4 col-xs-4 fiche-logo">
					<img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/meir/technopole.jpg" class="img-responsive logo">
				</div>
				<div class=" col-lg-4 col-md-4 col-sm-4 col-xs-4 fiche-logo">
					<img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/meir/region.jpg" class="img-responsive logo">
				</div>
				<div class=" col-lg-4 col-md-4 col-sm-4 col-xs-4 fiche-logo">
					<img src="<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/meir/republique.jpg" class="img-responsive logo">
				</div>
			</div>
		</div>

	</div>

</div>
<script>
	jQuery(document).ready(function() {
		
		$(".editFiche").off().on('click',function(){
			var id = $(this).data("id");
			var dyfOrga={
              "beforeBuild":{
                "properties" : {
					"name" : {
						"label" : "Dénomination ", 
						"inputType" : "text",
						"placeholder" :"",
						
					},
					"jobFamily" : {
						"label" : "Famille de métier(s) ", 
						"inputType" : "selectMultiple",	
						"placeholder" :"",
						"isSelect2" : true,
						"list" : "family",
					},
					"thematic" : {
						"label" : "Secteur ", 
						"inputType" : "select",
						"list" : "secteur",
					},
					"typeFinancing" : {
						"label" : "Type(s) de financement(s)", 
						"inputType" : "textarea",
						"placeholder" :"",
					},
					"modality" : {
						"label" : "Modalités ", 
						"inputType" : "textarea",
						"placeholder" :"",
						"order" : 4
					},
					"legalStatus" : {
						"label" : "Statut juridique ", 
						"inputType" : "textarea",
						"placeholder" :"",
						"rules" :{
						"required" : true
						},
					},
					"formLocality" : {
						"label" : "Adresse"
					},
					"responsable" : {
						"label" : "Responsable", 
						"inputType" : "textarea",
						"placeholder" :"",
					},
					"link" : {
						"label" : "Lien", 
						"inputType" : "textarea",
						"placeholder" :"",
						
					},	
					"objective" : {
						"label" : "Objectif(s) ", 
						"inputType" : "textarea",
						"placeholder" :"",
					},
					"linkFinancialDevice" : {
						"label" : "Lien(s) dispositif(s) financier(s) ", 
						"inputType" : "lists",
						"entries":{
							"key":{
								"type":"hidden",
								"class":""
							},
							"financialDevice":{
								"label":"Dispositif financier",
								"type":"textarea",
								"class":"col-md-6 col-sm-6 col-xs-12"
							},
							"link":{
								"label":"Lien",
								"type":"text",
								"class":"col-md-5 col-sm-5 col-xs-12"
							}
						},
					},
					"mobile" : {
						"inputType" : "text",
						"label" : "Téléphone",
						"placeholder" : "Téléphone",
					},				   
					"email" : {
						"label" : "Mèl dispositif financier ",
					},
					"financialPartners" : {
						"label" : "Partenaire(s) financier(s) ", 
						"inputType" : "lists",
						"entries":{
							"key":{
								"type":"hidden",
								"class":""
							},
							"financialPartner":{
								"label":"Partenaire financier",
								"type":"textarea",
								"class":"col-md-11 col-sm-11 col-xs-12"
							}
						},
					},	
					"maximumAmount" : {
						"label" : "Montant maximal", 
						"inputType" : "text",
						"placeholder" :"",
					},	
					"publicCible" : {
						"label" : "Public cible", 
						"inputType" : "text",
						"placeholder" :"",
					},	
					"strongPoints" : {
						"label" : "Points forts", 
						"inputType" : "textarea",
						"placeholder" :"",
					}, 	
					"weaknesses" : {
						"label" : "Points faibles", 
						"inputType" : "textarea",
						"placeholder" :"",
					}, 	
					"conditionsEligibility" : {
						"label" : "Conditions d’éligibilité", 
						"inputType" : "textarea",
						"placeholder" :"",
					}, 
					"referencing" : {
						"label" : "Référencements", 
						"inputType" : "textarea",
						"placeholder" :"",
					},
					"image" :{
						"order" : 49
					},
					"tags" :{
						"order" : 50
					},
					"category" : {
						"inputType" : "hidden",
						"value" : "acteurMeir",
					},  	
				  
                }
              },
              "onload" : {
                "actions" : {
					"setTitle" : "Ajouter un acteur",
					"src" : {
						"infocustom" : "<br/>Remplir le champ"
					},
				    "hide" : {
	                    "shortDescriptiontextarea" : 1,
	                    "urltext" : 1,
						"typeselect" :1,
						"roleselect" :1,
	                }          
                } 
              }   
            };
			dyfOrga.afterSave = function(data){
              	dyFObj.commonAfterSave(data, function(){
					  console.log("kolyl",data)
					var tplCtx = {};
					var phone = data.map.mobile;
					tplCtx.id = id;
					tplCtx.path = "telephone.mobile";
					tplCtx.value = [phone];
					tplCtx.collection = "organizations";
					dataHelper.path2Value( tplCtx, function(params) {	
						urlCtrl.loadByHash("#mapping");
					});
				});
            }
			dyFObj.editElement('organizations',id,null,dyfOrga);
		});
	})
</script>