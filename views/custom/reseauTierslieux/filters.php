<style type="text/css">
.leaflet-bar a:hover {
	border-color: #FF286B;
    background-color: #0044cc;
    color: #fff;
}
.leaflet-bar a:hover {
    background-color: #fff;
    color: #0044cc;
}
</style>

<script type="text/javascript">
	var pageApp=<?php echo json_encode(@$page); ?>;
	var paramsFilter= {
	 	container : "#filters-nav",
	 	options : {
	 		tags : {
	 			verb : '$and'
	 		}
	 	},
	 	loadEvent : {
	 		default : "scroll"
	 	},
	 	results :{
            renderView : "directory.elementPanelHtml",
            smartGrid : true,
            map : {
            	active : true
            }
        },
        header:{
	 		options:{
	 			left:{
	 				classes :"col-xs-6 no-padding",
	 				group:{
						count : false
					}
	 			},
	 			right:{
	 				classes :"col-xs-5 no-padding",
	 				group:{
						map : true
					}
	 			}
	 		}
	 	},	
	 // 		views : {
	 // 			map : function(fObj,v){
		// 			return  '<button class="btn-show-map-search pull-right bg-main1" style="" title="yoyo" alt="yoyoyo">'+
		// 						'<i class="fa fa-map-marker"></i> heyyou</button>';
		// 		}
		// 	}
		// },	
	 	defaults : {
	 		indexStep : 0,
	 		types : ["organizations"]
	 	},
	 	filters : {
	 		scope :true,
	 		scopeList : {
	 			params : {
	 				countryCode : scopeCountryParam, 
	 				level : scopeLevelParam,
	 				upperLevelId : upperLevelId
	 			}
	 		},
	 		typePlace : {
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Type",
	 			event : "tags",
	 			list : costum.lists.typePlace
	 		},
	 		services:{
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Services",
	 			event : "tags",
	 			list : costum.lists.services
	 		},
	 		// greeting : {
	 		// 	view : "selectList",
	 		// 	type : "tags",
	 		// 	name : "Accueil",
	 		// 	event : "tags",
	 		// 	list : costum.lists.greeting
	 		// },
	 		manageModel : {
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Portage",
	 			event : "tags",
	 			list : costum.lists.manageModel
	 		},
	 		// state : {
	 		// 	view : "selectList",
	 		// 	type : "tags",
	 		// 	name : "Etat",
	 		// 	event : "tags",
	 		// 	list : costum.lists.state
	 		// },
	 		spaceSize : {
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Taille",
	 			event : "tags",
	 			list : costum.lists.spaceSize
	 		},
	 		
	 		compagnon : {
	 			view : "tags",
	 			type : "tags",
	 			name : "Compagnons",
	 			event : "tags",
	 			list : costum.lists.compagnon
	 		},
	 		certification : {
	 			view : "dropdownList",
	 			type : "tags",
	 			name : "Lauréats Fabriques",
	 			event : "tags",
	 			list : costum.lists.certification
	 		},
	 		network : {
	 			view : "tags",
	 			type : "category",
	 			name : "Réseaux",
	 			event : "selectList",
	 			field : "category"
	 		}
	 	}
	};
	 
	//function lazyFilters(time){
	  //if(typeof searchObj != "undefined" )
	    //filterGroup = searchObj.init(paramsFilter);
	  //else
	    //setTimeout(function(){
	      //lazyFilters(time+200)
	    //}, time);
	//}
var filterSearch={};
	jQuery(document).ready(function() {
		  filterSearch = searchObj.init(paramsFilter);
		$("#menuRight").find("a").empty().html("<i class='fa fa-list'></i> Afficher l’annuaire");
		if(!($("#menuRight").find("a").hasClass("changelabel"))){
			$("#menuRight").find("a").addClass("changelabel")
		}
  
		$("#menuRightmapContent").hide();	
		$(".BtnFiltersLieux").show();

		// var typeObjOrga=costum.typeObj;
		// delete typeObjOrga.organization;
		// delete typeObjOrga.projects;
		// delete typeObjOrga.events;
		// costum.init(typeObjOrga);

		
	//	lazyFilters(0);
		
	});

</script>



