<?php 
$cssAnsScriptFilesTheme = array(
      // SHOWDOWN
      '/plugins/showdown/showdown.min.js',
      // MARKDOWN
      '/plugins/to-markdown/to-markdown.js'            
    );

	$cssAndScriptFilesModule = array(
		'/js/default/profilSocial.js',
	);

	$cssAnsScriptFilesModuleMap = array( 
		'/leaflet/leaflet.css',
		'/leaflet/leaflet.js',
		'/css/map.css',
		'/markercluster/MarkerCluster.css',
		'/markercluster/MarkerCluster.Default.css',
		'/markercluster/leaflet.markercluster.js',
		'/js/map.js',
	);

	HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModuleMap, Yii::app()->getModule( Map::MODULE )->getAssetsUrl() );

    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesTheme, Yii::app()->request->baseUrl);
     
	HtmlHelper::registerCssAndScriptsFiles($cssAndScriptFilesModule, Yii::app()->getModule( "co2" )->getAssetsUrl());

	if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
  		$el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );

  		$blockCmss = PHDB::find(Cms::COLLECTION, 
                  array( "parent.".$this->costum["contextId"] => array('$exists'=>1), 
                         "parent.".$this->costum["contextId"].".type"=>$this->costum["contextType"],
                         "page" => "welcome") );
  		// var_dump($blockCmss);exit;
  		$blockCmsById = [];

  		foreach ($blockCmss as $key => $value) {
  			if (isset( $value["id"] )) {
  				$blockCmsById[$value["id"]] = $value;
  			}
  		}
  	}
  	$page = "welcome";

    $params = [  "tpl" => "alternatiba","slug"=>$this->costum["slug"],"canEdit"=>false,"el"=>$el ];
    echo $this->renderPartial("costum.views.tpls.acceptAndAdmin", $params,true );
?>

<style type="text/css">
	@font-face{
	    font-family: "Helvetica";
	    src: url("<?php echo Yii::app()->getModule("costum")->getAssetsUrl() ?>/font/alternatiba/Helvetica.otf")
	}
	.cls-1{
		fill: #098048 !important;
	}
</style>

<div class="text-center row">
	<!-- Header -->
	<div class="header-alternatiba">
		<div class="one col-xs-12">
			<center>
				<img class="img-responsive img-one" src="<?php echo Yii::app()->getModule('costum')->assetsUrl;?>/images/alternatibaRe/logo1.svg">
				<div style="margin-left: 18vw;" class="hidden-xs col-xs-8" id="searchBar">
					<a data-type="filters" href="javascript:;">
            			<span id="second-search-bar-addon-alternatiba" class="text-white input-group-addon pull-left main-search-bar-addon-mednum">
                			<i id="fa-search" class="fa fa-search"></i>
            			</span>
        			</a>
        			<input type="text" class="form-control pull-left text-center main-search-bars" id="second-search-bar" placeholder="Une recherche">
    			</div>

    			<!-- Dropdown -->
			    <div style="margin-left: 22.7vw;margin-top: -0.1%;" id="dropdown" class="hidden-xs dropdown-result-global-search hidden-xs col-sm-5 col-md-5 col-lg-5 no-padding">
			    </div>
			</center>
		</div>
		<img class="img-responsive seconde-img-one" src="<?php echo Yii::app()->getModule('costum')->assetsUrl;?>/images/alternatiba/bannerAlternatiba.png">
	</div>

	<!-- Actualités -->
	<div style="background-color: #ededed;" class="col-xs-12 text-center">
		<div>
			<!-- <h1 style="font-family:'Helvetica';color: #098048;">
				<i class="fa fa-newspaper-o" aria-hidden="true"></i>  L'actualités de nos membres
			</h1> -->
			<?php 
			$paramsArticles = [
				"canEdit"=>true, 
				"blockCms" => @$blockCmsById["alternatiba.welcome.blockarticlescommunity.0"],
				"idToPath" => "alternatiba.welcome.blockarticlescommunity.0",
				"page" => $page, 
				"limit" => 4,
				"el"=>$el
			];

			echo $this->renderPartial("costum.views.tpls.blockCms.article.blockarticlescommunity", $paramsArticles); ?>
		</div>
	</div>

	<!-- Agenda -->
	<div style="background-color: white;margin-top: 1%;" class="col-xs-12 text-center">
		<!-- <div>
			<h1 style="font-family:'Helvetica';color: #098048;margin-bottom: 37px;">
				<i class="fa fa-calendar" aria-hidden="true"></i>  Agenda
			</h1>
		</div> -->
		<?php 
		$paramsEventCommunity = [
			"canEdit"=>true, 
			"blockCms" => @$blockCmsById["alternatiba.welcome.blockeventcommunity.0"],
			"idToPath" => "alternatiba.welcome.blockeventcommunity.0",
			"page" => $page, 
			"limit" => 4,
			"el"=>$el
		];

		echo $this->renderPartial("costum.views.tpls.blockCms.events.blockeventcommunity", $paramsEventCommunity); ?>
	</div>

	<div style="background-color: #ededed;margin-top: 1%;" class="col-xs-12 text-center">
		<div>
			<!-- <h1 style="font-family:'Helvetica';color: #098048;">
				<i class="fa fa-newspaper-o" aria-hidden="true"></i>  Nos membres
			</h1> -->
			<?php 
			$paramsCommunityBlock = [
				"canEdit"=>true, 
				"blockCms" => @$blockCmsById["alternatiba.welcome.communityblock.0"],
				"idToPath" => "alternatiba.welcome.communityblock.0",
				"page" => $page, 
				"limit" => 4,
				"el"=>$el
			];

			echo $this->renderPartial("costum.views.tpls.blockCms.community.communityblock", $paramsCommunityBlock); 
			?>
		</div>
	</div>

	<!-- Cartographie -->
	<div style="background-color:#098048;" class="col-xs-12 text-center">
		<!-- <h1 style="font-family:'Helvetica';color: white;">
			<i class="fa fa-map-marker" aria-hidden="true"></i>  Cartographie
		</h1> -->
		<?php 
		$paramsCommunity = [
			"canEdit"=>true, 
			"blockCms" => @$blockCmsById["alternatiba.welcome.communitymaps.0"],
			"idToPath" => "alternatiba.welcome.communitymaps.0",
			"page" => $page, 
			"limit" => 4,
			"el"=>$el
		];

		echo $this->renderPartial("costum.views.tpls.blockCms.map.communitymaps", $paramsCommunity); 
		?>
	</div>
</div>

<script type="text/javascript">
	
	jQuery(document).ready(function(){
		setTitle("Alternatiba");
		contextData = <?php echo json_encode($el); ?>;
	    var contextId=<?php echo json_encode((string) $el["_id"]); ?>;
	    var contextType=<?php echo json_encode($this->costum["contextType"]); ?>;
	    var contextName=<?php echo json_encode($el["name"]); ?>;
	    contextData.id=<?php echo json_encode((string) $el["_id"]); ?>;
	});

	$("#second-search-bar").off().on("keyup",function(e){ 
        $("#input-search-map").val($("#second-search-bar").val());
        $("#second-search-xs-bar").val($("#second-search-bar").val());
        if(e.keyCode == 13){
                mylog.log("searchObject.text",searchObject.text);
                searchObject.text=$(this).val();
                myScopes.type="open";
                myScopes.open={};
                startGlobalSearch(0, indexStepGS);
                $("#dropdown").css('display','block');
         }  
	});

	$("#second-search-xs-bar").off().on("keyup",function(e){ 
	    $("#input-search-map").val($("#second-search-xs-bar").val());
	    $("#second-search-bar").val($("#second-search-xs-bar").val());
	    if(e.keyCode == 13){
	            mylog.log("searchObject.text",searchObject.text);
	            searchObject.text=$(this).val();
	            myScopes.type="open";
	            myScopes.open={};
	            startGlobalSearch(0, indexStepGS);
	            $("#dropdown").css('display','block');            
	    }
	});

	$("#second-search-bar-addon-alternatiba, #second-search-xs-bar-addon").off().on("click", function(){
	    $("#input-search-map").val($("#second-search-bar").val());
	    mylog.log("searchObject.text",searchObject.text);
	    searchObject.text=$("#second-search-bar").val();
	    myScopes.type="open";
	    myScopes.open={};
	    startGlobalSearch(0, indexStepGS);
	    $("#dropdown").css('display','block');
	});

</script>