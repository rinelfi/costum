
<div class="">


<style type="text/css">
  #customHeader{
    margin-top: 0px;
  }
  #costumBanner{
   /* max-height: 375px; */
   margin-top:-70px;
  }
  #costumBanner h1{
    position: absolute;
    color: white;
    background-color: rgba(0,0,0,0.4);
    font-size: 29px;
    bottom: 0px;
    padding: 20px;
  }
  #costumBanner h1 span{
    color: #eeeeee;
    font-style: italic;
  }
  #costumBanner img{
    min-width: 100%;
  }
  .btn-main-menu{
    background: #e6344d !important;
    border-radius: 20px;
    padding: 20px !important;
    color: white !important;
    border:2px solid transparent;
    /*min-height:100px;*/
  }
  .btn-main-menu:hover{
    border:2px solid #ccc;
  }
  .ourvalues img{
    height:70px;
  }

  .box-register label.letter-black{
    margin-bottom:3px;
    font-size: 13px;
  }
  @media screen and (min-width: 450px) and (max-width: 1024px) {
    .logoDescription{
      width: 60%;
      margin:auto;
    }
  }

  @media (max-width: 1024px){
    #customHeader{
      margin-top: -1px;
    }
  }
  @media (max-width: 768px){

  }
</style>

<div class="col-xs-12 no-padding" id="customHeader" style="background-color: white">
  <div id="costumBanner" class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2 no-padding">
 <!--  <h1>L'entraide<br/><span class="small">Une interface numérique pour échanger</span></h1>-->
    <img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/coMains/Visuel_co-main_HD-01.jpg'> 
  </div>
  <!--<div class="col-md-12 col-lg-12 col-sm-12 imageSection no-padding" 
     style=" position:relative;">-->
    <div class="col-sm-12 col-md-12 col-xs-12 no-padding" style="background-color:#fff; max-width:100%; float:left;">
      <div class="col-xs-12 no-padding" style="text-align:center;margin-top:100px;"> 
        <div class="col-xs-12 no-padding">
          <div class="col-md-12 col-sm-12 col-xs-12 padding-20" style="padding-left:100px;background-color: #f6f6f6;">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 shadow2" style="margin-top:-160px;margin-bottom:5px;background-color: #fff;font-size: 14px;">
              <div class="col-xs-12 font-montserrat ourvalues" style="text-align:center;">
                        <h3 class="col-xs-12 text-center">
              <i class="fa fa-th"></i> <?php echo Yii::t("home", "Un outil pour s'entraider") ?><br>
              <!-- <small>
                <b>Centres sociaux connectés :</b> <?php echo Yii::t("home", "L'innovation au service de la collaboration") ?><br>
                <?php //echo Yii::t("home", "created for citizens actors of change") ?>
              </small> -->
              <hr style="width:40%; margin:20px auto; border: 4px solid #cecece;">
            </h3>
            <div class="col-xs-12">
              <a href="javascript:;" data-hash="#annonces?section=offer" class=" btn-main-menu lbh-menu-app col-xs-12 col-sm-4 col-md-4 col-md-offset-2 col-sm-offset-2 padding-20 margin-top-5 margin-right-5" data-type="classifieds" >
                  <div class="text-center">
                      <div class="col-md-12 no-padding text-center">
                          <h4 class="no-margin text-white">
                            <i class="fa fa-search"></i>
                            <?php echo Yii::t("home","Les offres") ?>
                            <!--  <br><small class="text-white">
                                  <?php echo Yii::t("home","Retrouver toutes les annonces à disposition")?>
                              </small>-->
                          </h4>
                      </div>
                  </div>
              </a>
              <a href="javascript:;" data-hash="#annonces?section=need" class="btn-main-menu lbh-menu-app col-xs-12 col-sm-4 col-md-4 padding-20 margin-top-5" data-type="search" >    
                  <div class="text-center">
                      <!-- <h4 class="text-red no-margin "><i class="fa fa-search"></i>
                          <span class="homestead"> <?php //echo Yii::t("home","SEARCH") ?></span>
                      </h4><br/> -->
                      <div class="col-md-12 no-padding text-center">
                          <h4 class="no-margin text-white">
                            <i class="fa fa-bullhorn"></i>
                            <?php echo Yii::t("home","Les besoins") ?>
                              <!--<br>
                              <small class="text-white">
                                  <?php echo Yii::t("home","Aider, soutenir, venir en aide") ?>
                              </small>-->
                          </h4>
                      </div>
                  </div>
              </a>
            </div>
         <!--    <div class="col-xs-12 no-padding margin-top-20">
                         <div class="col-md-1 col-sm-1 hidden-xs"></div> 
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 margin-top-15" style="text-align:center;">
                <a href="https://larbrisseau.com/" target="_blank">
                  <img class="img-responsive" style="margin:0 auto;" 
                   src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/csc/logo-arbrisseau.jpg"/>
                   Centre social et culturel de l'Arbrisseau
                </a>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 margin-top-15">
                <a href="http://www.centre-social-lazare-garreau-lille.fr/" target="_blank"><img class="img-responsive" style="margin:0 auto;" 
                   src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/csc/Logo-header-CSCLG.png"/>
                   Centre social et culturel Lazare Garreau</a>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 margin-top-15" style="text-align:center;">
                <a href="https://www.cheminrouge.fr/" target="_blank">
                  <img class="img-responsive" style="margin:0 auto;" 
                   src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/csc/logo-mcr.png"/>
                   Centre social intercommunal La Maison du Chemin Rouge
                </a>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 margin-top-15">
                <a href="http://www.association-projet.org/" target="_blank">
                <img class="img-responsive" style="margin:0 auto;" 
                   src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/csc/logo-projet.png"/>
                   Centre social Projet
                </a>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 margin-top-15">
                <a href="http://www.lsi-asso.fr/" target="_blank">
                  <img class="img-responsive" style="margin:0 auto;" 
                   src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/csc/lsi.png"/>
                   Lille Sud Insertion 
                </a>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 margin-top-15">
                <a href="#@laFabriqueDuSud" class="lbh" target="_blank">
                  <img class="img-responsive" style="margin:0 auto;" 
                     src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/csc/logo-fabrique-du-sud.png"/>
                     La Fabrique du Sud
                </a>
              </div> 
            </div> -->
        </div>
      </div>
    </div>

       
      
    </div>
    <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 contact-map" style="color:#293A46;float:left;" id="contactSection">
    <center>
      <h4 style="text-transform: inherit;line-height: 19px;"><i class="fa fa-at headerIcon"></i> <span style="vertical-align: middle;line-height: 30px;">contact@csconnectes.eu</span></h4>
      <h4 class="" style="text-transform: inherit;line-height: 19px;"><i class="fa fa-globe headerIcon"></i>  <a href="http://csconnectes.eu" target="_blank" style="vertical-align: middle;line-height: 30px;">www.csconnectes.eu</a></h4>
     
    <center>
  </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8 col-lg-offset-2 no-padding">
 <!--  <h1>L'entraide<br/><span class="small">Une interface numérique pour échanger</span></h1>-->
    <img class="img-responsive" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/csc/logo-feder.jpg'> 
  </div>

<script type="text/javascript">

  setTitle("CO-mains, les centres sociaux connectés");



</script>





