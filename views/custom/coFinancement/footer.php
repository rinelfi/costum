<footer style="min-height:50px;" class="text-center col-xs-12 pull-left no-padding bg-yellow">
     <div class="">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 text-center col-footer col-footer-step">
                    <a href="#mentions" target="_blank"><?php echo Yii::t("home","Mentions légales"); ?></a></br><br>
                  <!--   <a href="https://www.helloasso.com/" target="_blank" class="text-white"><?php echo Yii::t("home","Faire un don") ?></a> -->
                    <span>Site web associé :</span></br>
                    <a href="https://movilab.org">Movilab</a></br>
                    <a href="https://cartographie.francetierslieux.fr" target="_blank" class="">Cartographie des Tiers-lieux</a>            
                </div>
                <div class="col-xs-12 col-sm-6 col-footer">
                    <span style="font-size:15px;"><a href="mailto:mail@mymail.fr">mail@mymail.fr</a></span></br>

                    <!-- <ul class="list-inline">
                        <li>
                            <a href="https://www.facebook.com/" target="_blank" 
                               class="btn-social btn-outline">
                                <i class="fa fa-fw fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/" target="_blank" 
                               class="btn-social btn-outline">
                                <i class="fa fa-fw fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://instagram.com/" target="_blank" 
                               class="btn-social btn-outline">
                                <i class="fa fa-fw fa-instagram"></i>
                            </a>
                        </li>
                     </ul> -->
                    <span class="">Powered by <a href="https://gitlab.adullact.net/pixelhumain" target="_blank"><img src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/img/LOGO_PIXEL_HUMAIN.png" height=50></a></span>
                </div>
            </div>
        </div>
    </div>
</footer>
