<div id="adminDirectory" class="col-xs-12 no-padding"></div>
<script type="text/javascript">
var contextElt = <?php echo json_encode(@$context)  ?>;
var panelAdmin = <?php echo json_encode($panelAdmin) ?>;
var adminGroup = {};
var filterGroup = {};
var filterAdmin = {};
var paramsFilterAdmin= {};
jQuery(document).ready(function() {
	var paramsFilterAdmin= {
	 	container : "#filters-nav-admin",
	 	loadEvent: {
	 		default : "admin",
	 		options : {
	 			results : {},
				initType : panelAdmin.types,
				panelAdmin : panelAdmin
	 		}
	 	},
	 	results : {
	 		multiCols : false
	 	},
		/*admin : {
			paramsAdmin : {
				
			}
		},*/
		header : {
			dom : ".headerSearchContainerAdmin",
			options : {
				left : {
					classes : 'col-xs-8 elipsis no-padding',
					group:{
						count : true
					}
				}
			}
		},
		urlData : baseUrl+"/co2/search/globalautocompleteadmin"
	};
	if(notNull(contextElt) && notEmpty(contextElt)){
		paramsFilterAdmin.urlData+="/type/"+contextElt.type+"/id/"+contextElt.id;
	}
	if(typeof panelAdmin != "undefined" && typeof panelAdmin.paramsFilter != "undefined"){
		if(typeof panelAdmin.paramsFilter.filters != "undefined")
			paramsFilterAdmin.filters = panelAdmin.paramsFilter.filters;
		if(typeof panelAdmin.paramsFilter.defaults != "undefined")
			paramsFilterAdmin.defaults = panelAdmin.paramsFilter.defaults;
	}

	if(typeof panelAdmin != "undefined" && typeof panelAdmin.context != "undefined"){
		paramsFilterAdmin.loadEvent.options["context"] = panelAdmin.context;
	}
	// Admin structure simple d'admin pour qu'elle s'adapte au header du searchObj	
	if( typeof panelAdmin.csv != "undefined" || 
		typeof panelAdmin.pdf != "undefined"|| 
		typeof panelAdmin.invite != "undefined"){
		paramsFilterAdmin.header.options.right = {
			classes : 'col-xs-4 text-right no-padding',
			group : { }
		}

		if(typeof panelAdmin.csv != "undefined")
			paramsFilterAdmin.header.options.right.group.csv = panelAdmin.csv;
		if(typeof panelAdmin.pdf != "undefined")
			paramsFilterAdmin.header.options.right.group.pdf = panelAdmin.pdf;
		if(typeof panelAdmin.invite != "undefined")
			paramsFilterAdmin.header.options.right.group.invite = panelAdmin.invite;
	}

	/*adminDirectory.initViewTable = function(){
		var aObj = this;
		mylog.log("adminDirectory.initViewTable", aObj.container);
		$("#"+aObj.container+" #panelAdmin .directoryLines").html("");
		mylog.log("adminDirectory.initViewTable !", aObj.results);
		if(Object.keys(aObj.results).length > 0){
			$.each(aObj.results ,function(key, values){
				mylog.log("adminDirectory.initViewTable !!! ", key, values);
				var entry = aObj.buildDirectoryLine( values, values.collection);
				$("#"+aObj.container+" #panelAdmin .directoryLines").append(entry);
			});
		}
		aObj.bindAdminBtnEvents(aObj);
	};*/

	/*adminDirectory.getElt = function(id, type){
		mylog.log("adminDirectory.getElt", this, id, type);
		return this.results[id] ;
	};
	adminDirectory.setElt = function(elt, id, type, attr){
		mylog.log("adminDirectory.setElt", id, type);
			if(notNull(attr) && typeof this.results[id][attr] !="undefined"){
			this.results[id][attr]=elt;
		}else
			this.results[id] = elt;
	};*/

	

	//aObj.search(0);
	//mylog.log("ixi admin params", paramsFilterAdmin);
	filterAdmin = searchObj.init(paramsFilterAdmin);

/*	filterAdmin.results.render = function(fObj, results, data){ 
      	mylog.log("filterAdmin.results.render adminDirectory", fObj.admin.adminDirectory, results, data);
		fObj.admin.adminDirectory.results = results;
		fObj.admin.adminDirectory.initViewTable(fObj.admin.adminDirectory);
		fObj.admin.adminDirectory.bindAdminBtnEvents(fObj.admin.adminDirectory);
		if(typeof fObj.admin.adminDirectory.bindCostum == "function")
			fObj.admin.adminDirectory.bindCostum(fObj.admin.adminDirectory);

		// fObj.admin.adminDirectory.countTotal = 0 ;
		// $.each(fObj.admin.adminDirectory.results.count, function(key, values){
		// 	fObj.admin.adminDirectory.countTotal += values;
		// });
		// $("#"+fObj.admin.adminDirectory.container+" #countTotal").html(fObj.admin.adminDirectory.countTotal);
		//adminDirectory.initPageTable();
    }*/

    

    
	filterAdmin.search.init(filterAdmin);

	
});
</script>