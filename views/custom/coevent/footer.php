<?php
$eventOrga = PHDB::findOne(Event::COLLECTION,array('_id' => new MongoId($this->costum["contextId"])));
$postalCode = (!empty($eventOrga["address"]["postalCode"]) ? $eventOrga["address"]["postalCode"] : "Aucune adresse pour cette événement" );
$addressLocality = (!empty($eventOrga["address"]["addressLocality"]) ? $eventOrga["address"]["addressLocality"] : "Aucune adresse pour cette événement" );
$email = (!empty($eventOrga["email"]) ? $eventOrga["email"] : "Aucune email pour cette événement" );
?>
<footer class="col-md-12">
	<div class="col-md-6 pull-left">
	</div>
	<div style="position: relative;margin-top: 2vw;z-index:10;" class="col-md-6 pull-right text-right">
		<!-- <div class="col-md-12 text-center"> -->
			<p style="color: white;font-size: 1.5vw;"><?php echo $postalCode ." ". $addressLocality; ?></p>
			<p style="color: white;font-size: 1.5vw;"><?php echo $email; ?></p>
		<!-- </div> -->
	</div>
	<img style="position: absolute;" class="img-responsive" src="<?= Yii::app()->getModule("costum")->assetsUrl; ?>/images/coevent/footer.png">
</footer>