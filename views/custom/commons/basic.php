<?php 
$cssJS = array(
    '/plugins/jQuery-Knob/js/jquery.knob.js',
    '/plugins/jQuery-Smart-Wizard/js/jquery.smartWizard.js',
    //'/plugins/jQuery-Smart-Wizard/styles/smart_wizard.css',
  // SHOWDOWN
  '/plugins/showdown/showdown.min.js',
  // MARKDOWN
  '/plugins/to-markdown/to-markdown.js'
);
HtmlHelper::registerCssAndScriptsFiles($cssJS, Yii::app()->request->baseUrl); 
$poiList = array();

// echo "<script type='text/javascript'> alert('commons home :".@$this->costum["slug"]." : ".@$this->costum["contextType"]." : ".@$this->costum["contextId"]."'); </script>";

if(isset($this->costum["contextType"]) && isset($this->costum["contextId"])){
    $el = Element::getByTypeAndId($this->costum["contextType"], $this->costum["contextId"] );

    $poiList = PHDB::find(Poi::COLLECTION, 
                    array( "parent.".$this->costum["contextId"] => array('$exists'=>1), 
                           "parent.".$this->costum["contextId"].".type"=>$this->costum["contextType"],
                           "type"=>"cms") );
} else {?>
  
  <div class="col-xs-12 text-center margin-top-50">
    <h2>Ce COstum est un template <br/>doit etre associé à un élément pour avoir un context.</h2>
  </div>

<?php exit;}?>


<div class="">


<div class="col-xs-12 no-padding" id="customHeader" style="background-color: white">
    
<div id="costumBanner" class="col-xs-12 col-sm-12 col-md-12 no-padding">
 <!--  <h1>L'entraide<br/><span class="small">Une interface numérique pour échanger</span></h1>-->
    
    <?php 
    $banner = Yii::app()->getModule("co2")->assetsUrl."/images/banniere-Campagne-Acoeur.jpg";
    if(@$this->costum["metaImg"]){
      //ex this.profilBannerUrl
    if(substr_count($this->costum["metaImg"], 'this.') > 0 && isset($el)){
      $field = explode(".", $this->costum["metaImg"]);
      if( isset( $el[ $field[1] ] ) )
          $banner = Yii::app()->getRequest()->getBaseUrl(true).$el[$field[1]] ;
    }
    else if(strrpos($this->costum["metaImg"], "http" ) === false && strrpos($this->costum["metaImg"], "/upload/" ) === false ) {
      $banner = Yii::app()->getModule("co2")->getAssetsUrl().$this->costum["metaImg"] ;
        }
    else 
      $banner = $this->costum["metaImg"];
    }
    ?>
    <img class="img-responsive"  style="margin:auto;background-color: black;" src='<?php echo $banner ?>'/> 

  </div>
  
  <div class="col-sm-12 col-md-12 col-xs-12 no-padding" style="background-color:#1F2532; max-width:100%; float:left;">
    <div class="col-xs-12 no-padding" style="margin-top:100px;"> 
      <div class="col-xs-12 no-padding">
        <div class="col-md-12 col-sm-12 col-xs-12 padding-20" style="padding-left:100px;background-color: #f6f6f6; min-height:400px;">
          <div class="col-xs-12 col-sm-10 col-sm-offset-1 pull-left padding-20 shadow2" style="margin-top:-80px;margin-bottom:-80px;background-color: #fff;font-size: 14px;z-index: 5;">
            <div class="col-xs-12 font-montserrat ourvalues" style="text-align:center;">
              <h3 class="col-xs-12 text-center">
                
                <small style="text-align: left">
                <link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
                  <bloquote style="font: 10 25px/1 'Pacifico', Helvetica, sans-serif;
  color: #2b2b2b;
  text-shadow: 4px 4px 0px rgba(0,0,0,0.1);">



  « <?php if(isset($el['description']))echo $el['description'] ?>   »</bloquote>

              </h3>
              <div  style="text-align: right; font-size: 1.4em;padding-right:30px;">
                <b>Mr. X</b>,<br/>
                Acteur engagé dans le changement positif
                  </small>
              </div>
              <br/>



                <hr style="width:40%; margin:20px auto; border: 4px solid #6BB3C1;">
              
              <div class="col-md-10 col-md-offset-1 col-xs-12">
                
                <span class="text-explain">
                  
                    <?php 
                    echo $this->renderPartial("costum.views.tpls.multiblocks",array(
                        "poiList"   => $poiList,
                        "blockName" => "cc",
                        "titlecolor"=> "#e6344d",
                        "blockCt"   => 1,
                        "endHTML"   => '<br/><span class="bullet-point"></span><br/>'
                      ),true);
                     ?>
                </span>

                <hr style="width:40%; margin:20px auto; border: 4px solid #6BB3C1;">

            </a>
              </div>
            </div>

        </div>
          </div>

        </div>

      </div>
      







    </div>
  </div>
</div>

<div class="col-xs-12 text-center">
  <a href="javascript:;" id="changeTemplate">Change Template</a>
</div>


<?php 
/*
- search filter by src key 
- admin candidat privé only
- app carte 
*/
 ?>


<script type="text/javascript">
var changeTemplateForm = {
    jsonSchema : {
      title : "Change Template",
      icon : "paint-brush",
      type : "object",
      onLoads : {
        onload : function(){
          dyFInputs.setSub("bg-azure");
        }
      },
      afterSave : function(data,callB){
            dyFObj.commonAfterSave();
            //window.location.reload();
           /* if( $(uploadObj.domTarget).fineUploader('getUploads').length > 0 ){
                $(uploadObj.domTarget).fineUploader('uploadStoredFiles');   
            }
            */
            
      },
      properties : {
        parent : {
          inputType : "finder",
          label : "Ajouter un badge parent",
          //multiple : true,
                  initMe:false,
                  placeholder:"Rechercher un badge existant",
          rules : { required : false, lengthMin:[1, "parent"]}, 
          initType: ["badges"],
          openSearch :true
        }
      }
  }
};

  jQuery(document).ready(function() {
    
    setTitle("<?php echo $el['name'] ?>");
    contextData = {
        id : "<?php echo $this->costum["contextId"] ?>",
        type : "<?php echo $this->costum["contextType"] ?>",
        name : "<?php echo $el['name'] ?>",
        slug : "<?php echo $el['slug'] ?>",
        profilThumbImageUrl : "<?php echo $el['profilThumbImageUrl'] ?>"
    };
    if(contextData.id == "")
      alert("Veuillez connecter ce costum à un élément!!")
    
    $.each($(".markdown"), function(k,v){
        descHtml = dataHelper.markdownToHtml($(v).html()); 
        $(v).html(descHtml);
    });

    $("#changeTemplate").click(function() {  
      openForm();
    })
}
    
});

</script>

