<style type="text/css">
        .text-explain{
            font-size: 22px;
        }
    </style>

<?php
$cssAnsScriptFilesModule = array(
      '/js/default/profilSocial.js'
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->assetsUrl);
?>


<div id="sub-doc-page">
    <div class="col-xs-12 support-section section-home col-md-10 col-md-offset-1">
        <div class="col-xs-12 header-section">
            <h3 class="title-section col-sm-8">Le collectif éco-citoyen</h3>
            <hr>
        </div>
        <div class="col-xs-12">
        
            <span class="col-xs-12 text-left text-explain">
              Crée en novembre 2019, le collectif éco-citoyen est actuellement composé de <strong>11 membres actifs</strong>. Il organisera bientôt des temps pour accueillir pas à pas les 60 personnes ayant souhaité le rejoindre lors de la rencontre du 20 janvier. A cette occassion, les membres du collectif animés par la volonté de donner la paroles aux citoyens, ont moblisé près de <strong>140 personnes</strong> afin de travailler sur des actions en faveur d'une transition écologique nécessaire à Montreuil.
              <br>
              N'hésitez pas à rejoindre nos groupes de travail pour réfléchir et élaborer des actions concrètes et prioritaires à mener sur Montreuil.
            <br>
            Pour être visible ci dessous, rejoignez le groupe directement à partir du site communecter.org, en <a href="https://www.communecter.org/#@CollectifEcocitoyenDeMontreuilSurMer">cliquant ici</a>
            </span>
        </div> 
        <div id="community" class="col-sm-12 col-md-12 col-xs-12 no-padding" style="max-width:100%; float:left;">
            <div id="central-container"></div> 
        </div>
     
 
        
    </div>
</div>




<script type="text/javascript">

  var hashUrlPage="#community";
  var connectTypeElement="<?php echo Element::$connectTypes[Organization::COLLECTION] ?>";
  var contextData = {type:costum.contextType,id:costum.contextId,name:"Le collectif éco-citoyen"};

  var openEdition = false;
  var canEdit = false; 

  jQuery(document).ready(function() {
        setTitle("Le collectif");
        getAjax("", baseUrl+"/"+moduleId+"/element/getdatadetail/type/"+costum.contextType+"/id/"+costum.contextId+"/dataName/members",
        function(data){displayInTheContainer(data,"members","group",null,true);
        },
        "html");
        

});

</script>