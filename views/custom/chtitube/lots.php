<style type="text/css">
	table td{
		border-top: 1px solid white;
		text-align: center;
		padding: 5px 20px;
	}
	@media (max-width: 742px){
		table td{
			padding:5px;
		}
	}
</style>
<h2 class="col-xs-12 text-yellow text-center">5 lots de bières artisanales à gagner du Pas-de-calais</h2>
<table class="col-xs-12">
	<thead>
		<tr>
			<td>Rang</td>
			<td>Mai</td>
			<td>Epinette</td>
			<td>Arras In</td>
			<td><span class="hidden-xs">Sept Bonnettes</td>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>1er<span class="hidden-xs"> chtitubeur</span></td>
			<td>3</td>
			<td>3</td>
			<td>3</td>
			<td>3</td>
		</tr>
		<tr>
			<td>2e <span class="hidden-xs"> chtitubeur</span></td>
			<td>2</td>
			<td>2</td>
			<td>2</td>
			<td>2</td>
		</tr>
		<tr>
			<td>3e <span class="hidden-xs"> chtitubeur</span></td>
			<td>2</td>
			<td>2</td>
			<td>1</td>
			<td>1</td>
		</tr>
		<tr>
			<td>4e <span class="hidden-xs"> chtitubeur</span></td>
			<td>1</td>
			<td>1</td>
			<td>1</td>
			<td>1</td>
		</tr>
		<tr>
			<td>5e <span class="hidden-xs"> chtitubeur</span></td>
			<td>X</td>
			<td>X</td>
			<td>1</td>
			<td>1</td>
		</tr>
	</tbody>
</table>
<h3 class="col-xs-12 no-padding margin-top-10 margin-bottom-10 bg-yellow text-center" style="padding-top: 20px !important;padding-bottom: 15px !important;color: black !important;">La sélection des brasseurs</h3>
<div class="col-xs-12 margin-top-20">
	<div class="col-xs-12 no-padding">
		<h4 class="col-xs-12 text-center text-yellow" style="font-size: xx-large;">Brasserie des Sept Bonnettes</h4>
		<div class="col-xs-offset-2 col-xs-8">
			<img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/chtitube/bieres-7-bonnettes.jpg" class="img-responsive" style="margin:auto;">
		</div>
		<div class="col-xs-10 col-xs-offset-1">
			<h5 class="col-xs-12 text-yellow text-center"><i class="fa fa-map-marker"></i> 8 rue de la Brasserie 62156 Etaing</h5>
			<span class="col-xs-12 text-center">Notre brasserie vous propose des bières artisanales, brassées dans le respect de la tradition. Elles sont refermentées en bouteille et non filtrées, elles gardent ainsi tout leur caractère.<br/><br/>Du concassage du malt à l’étiquetage des bouteilles, toutes les étapes de la production sont réalisées au sein de la Brasserie des 7 Bonnettes, par le brasseur lui-même.<br/><br/> Les matières premières utilisées (malts d’orge, houblons), sont issues autant que possible de productions locales.</span>
			<a class="col-xs-12 text-yellow text-center" href="https://www.brasserie-7bonnettes.com" target="_blank">https://www.brasserie-7bonnettes.com</a>
		</div>
	</div>
</div>
<hr class="col-xs-12">
<div class="col-xs-12 margin-top-20">
	<div class="col-xs-12 no-padding">
		<h4 class="col-xs-12 text-center text-yellow margin-bottom-20" style="font-size: xx-large;">Brasserie de Mai</h4>
		<div class="col-xs-offset-2 col-xs-8">
			<img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/chtitube/mai.jpg" class="img-responsive" style="margin:auto;">
		</div>
		<div class="col-xs-10 col-xs-offset-1">
			<h5 class="col-xs-12 text-yellow text-center"><i class="fa fa-map-marker"></i> 50 rue Edouard Plachez 62220 Carvin</h5>
			<span class="col-xs-12 text-center">Une fabrique de bières authentiques, éco-responsables et locales !<br/><br/>La Brasserie de Mai propose 4 bières permanentes et une bière éphémère à chaque nouvelle saison.<br/><br/>La Brasserie de Mai, c’est aussi le Bistrot de Mai, un lieu de vente et de dégustation de nos bières où vous pourrez goûter nos tartines aux drêches et participer à des ateliers de brassage collectif !</span>
			<a class="col-xs-12 text-yellow text-center" href="https://brasseriedemai.fr/" target="_blank">https://brasseriedemai.fr/</a>
		</div>
	</div>
</div>
<hr class="col-xs-12">
<div class="col-xs-12 margin-top-20">
	<div class="col-xs-12 no-padding">
		<h4 class="col-xs-12 text-center text-yellow margin-bottom-20" style="font-size: xx-large;">Brasserie de L'Arras'In</h4>
		<div class="col-xs-offset-2 col-xs-8">
			<img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/chtitube/arrasin.jpg" class="img-responsive" style="margin:auto;">
		</div>
		<div class="col-xs-10 col-xs-offset-1">
			<h5 class="col-xs-12 text-yellow text-center"><i class="fa fa-map-marker"></i> Place de Vacquerie 62000 Arras</h5>
			<span class="col-xs-12 text-center">L’ARRAS’IN est une Micro Brasserie urbaine enracinée au cœur d’Arras dans le Pas-de-Calais.<br/><br/>Nous souhaitons, réintroduire la fabrication de la bière au plus près des consommateurs, proposer une brasserie ouverte au public qui produit une bière locale, artisanale et de qualité.<br/><br/>La Micro Brasserie L’ARRAS’IN brasse, des bières traditionnelles & originales, déclinées en différents styles (blonde, triple, bière de mars...) ; des bières éphémères, en édition limitée, des bières brassées selon l’inspiration du moment, mettant en avant des collaborations locales ou régionales.</span>
			<a class="col-xs-12 text-yellow text-center" href="https://www.facebook.com/MicroBrasserie.Arras/" target="_blank">https://www.facebook.com/MicroBrasserie.Arras/</a>
		</div>
	</div>
</div>
<hr class="col-xs-12">
<div class="col-xs-12 margin-top-20">
	<div class="col-xs-12 no-padding">
		<h4 class="col-xs-12 text-center text-yellow margin-bottom-20" style="font-size: xx-large;">Brasserie Paysanne de l'Artois</h4>
		<div class="col-xs-offset-2 col-xs-8">
			<img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/chtitube/epinettes.jpg" class="img-responsive" style="margin:auto;">
		</div>
		<div class="col-xs-10 col-xs-offset-1">
			<h5 class="col-xs-12 text-yellow text-center"><i class="fa fa-map-marker"></i> 5 ter Rue de Rœux, 62580 Gavrelle</h5>
			<span class="col-xs-12 text-center">Implantée à Gavrelle près d'Arras, notre brasserie a vu le jour en 2014 sur une ferme d'une cinquantaine d'hectares où sont cultivés blé, orge, avoine, pommes de terre, chicorée et luzerne, en bio depuis 2000.<br/><br/>Notre production est le fruit du travail de Mathieu Glorian et de François Théry qui l'a accueilli sur sa ferme. De nombreux et inestimables soutiens nous ont également aidé pour recréer localement d'authentiques bières du terroir Artésien.<br/><br/>Notre ambition : vous proposer des bières d'Appellation d'Origine Contrôlable ;-)</span>
			<a class="col-xs-12 text-yellow text-center" href="https://www.facebook.com/brasseriepaysannedelartois/" target="_blank">https://www.facebook.com/brasseriepaysannedelartois/</a>
		</div>
	</div>
</div>