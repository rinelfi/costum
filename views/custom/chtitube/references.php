<style type="text/css">
.tilsContent{
	border-radius: 5px;
	padding: 10px;
	color: white;
	box-shadow: 0px 0px 2px 1px rgba(250,250,250,0.5);
    margin-bottom: 5px;
}
.tilsContent:hover{
	background-color: rgba(255, 255, 255, 0.3);
	color:#22252A;
}
.tilsContent .img-tils{
	border-radius: 5px;
	border:2px solid white;  
	width: 90px;
	height: 90px;
}
.tilsContent .title-tils{
	font-size: 18px;
	color: white;
	font-weight: 800;
}
.tilsContent:hover .title-tils{
	color:#22252A;
}
.linkTODoc:hover, .tilsContent .refLink{
	color: #45c7dd;
}
</style>
<div class="col-xs-12">
<div class="col-xs-12 margin-top-20 margin-bottom-20">
	<span class="col-xs-12 text-center uppercase" style="font-size: 30px;">Vous avez ajouté<br/><span class="text-yellow"><?php echo $count["total"] ?></span> élément<?php if ($count["total"]>1) echo "s" ?><br/>dont <span class="text-yellow"><?php echo ($count["total"]-$count["toBeValidated"]) ?></span> validé<?php if( ($count["total"]-$count["toBeValidated"]) >1 ) echo "s"; ?></span> 
	<div class="filters"></div>
</div>
<div class="col-xs-12 results-container-chtitube">
	<?php foreach($results as $k => $v){
		drawTils($k, $v);
	} ?>
</div>
<?php function drawTils($key, $v){
		$descr="Aucune description";
		if(isset($v["shortDescription"])){
			$descr=$v["shortDescription"];
		}else{
			$descr=@$v["description"];
			if(!empty($descr)) 
				$descr=substr($descr,0, 160);
		}
		$name=(isset($v["title"])) ? $v["title"] : $v["name"];
		$imgPath=(isset($v["profilMediumImageUrl"]) && !empty($v["profilMediumImageUrl"])) ? Yii::app()->createUrl($v["profilMediumImageUrl"]) :  Yii::app()->getModule( Yii::app()->params["module"]["parent"] )->getAssetsUrl()."/images/thumb/default_".$v["type"].".png"; 
		$addressStr="";
		if(!empty($v["address"]["addressLocality"])){
			$addressStr.= !empty($v["address"]["streetAddress"]) ? $v["address"]["streetAddress"].", " : "";
			$addressStr.= !empty($v["address"]["postalCode"]) ? 
					$v["address"]["postalCode"].", " : "";
			$addressStr.= $v["address"]["addressLocality"];
					
		} 
		if(isset($v["preferences"]) && isset($v["preferences"]["private"]) && !empty($v["preferences"]["private"])){
			$class="private";
			$valid= "<span class='col-xs-12 no-padding'>STATUT : <span class='text-orange'><i class='fa fa-time'></i> En attente de validation</span></span>";
		}
		else{
			$class="public";
			$valid= "<span class='col-xs-12 no-padding'>STATUT : <span class='text-green-k'><i class='fa fa-check'></i> Validé</span></span>";
		}
		echo '<div class="col-xs-12 tilsContent shadow2 '.$class.'">'.
				'<div class="col-xs-12 col-sm-3 col-md-2">'.
					"<img class='img-responsive img-tils pull-right' src='".$imgPath."'>".
				'</div>'.
				'<div class="col-xs-12 col-sm-9 col-md-10 padding-5">'.
					"<a href='#page.type.".$v["type"].".id.".$v["_id"]."' class='lbh-preview-element col-xs-12 title-tils no-padding elipsis text-yellow'>".$name."</a>".
					"<span class='col-xs-12 no-padding'>".
						"TYPE : <i class='fa fa-".Element::getFaIcon($v["type"])."'></i> ".Yii::t("common", Element::getControlerByCollection($v["type"])).
					"</span>".
					$valid.
					"<span class='small-descr col-xs-12 no-padding'>".$descr."</span>";
					if(!empty($addressStr)){
						echo "<span class='col-xs-12 no-padding'><i class='fa fa-map-marker'></i> ".$addressStr."</span>"; 
					}
					//$orgaPartner.
					//$orgaPorter.
			echo '</div>'.
			 '</div>';
	}
?>
<script type="text/javascript">
	resultData=<?php echo json_encode($results); ?>;
	jQuery(document).ready(function(){
		//html="";
		//$.each(resultData, function(e,data){
		//html = 	directory.showResultsDirectoryHtml(resultData);
		//});
		//$(".results-container-chtitube").html(html);
		directory.bindBtnElement();
	});
</script>