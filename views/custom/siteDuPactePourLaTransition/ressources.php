<style type="text/css">
  .section-home.elt-section{
    padding: 15px 0px;
  }
  .section-home.elt-section .content-img{
    height:auto;
    overflow-y: hidden;
  }
  .section-home.elt-section .header-section h3{
    font-size: 20px;
    margin-bottom: 0px;
  }
  .section-home.elt-section .header-section hr{
    margin-left: 0px;
    margin-top: 0px;
    bottom: 5px;
    margin-bottom: 7px;
  }
  
  #sub-doc-page .section-home .header-section{
    line-height: inherit !important;
    margin-bottom: 0px;
  }

/*  .section-home.elt-section .header-section h3{
    font-size: 22px;
  }*/
  .section-home.elt-section .textExplain{
    font-size: 18px;
  }
  .support-section .text-explain {
    font-size: 22px;
  }
  .text-explain, .text-partner {
    color: #555;
  }

  #sub-doc-page{
    margin-top: 0px;
  }
  @media (min-width: 768px) {
    .cont-desc{
      height: 100px;
    }
    .section-home.elt-section .textExplain{
      font-size: 18px;
      display: -webkit-box;
      -webkit-line-clamp: 4;
      -webkit-box-orient: vertical;
      overflow: hidden;
      text-overflow: ellipsis;
      padding-top: 10px;
    }
  }
</style>
<div id="sub-doc-page">
    <!--<div id="start" class="section-home section-home-video">
        <div class="col-xs-12 content-video-home no-padding">
          <div class="col-xs-12 no-padding container-video text-center" style="max-height: 450px;overflow-y: hidden;">
              <img class="img-responsive start-img" src='<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/siteDuPactePourLaTransition/sengaer_bandeau.jpg' style="margin:auto;">
          </div>
        </div>
    </div>
    <div class="col-xs-12 section-separtor no-padding">
      <div class="col-xs-4 bg-orange"></div>
      <div class="col-xs-4 bg-blue"></div>
      <div class="col-xs-4 bg-orange"></div>
    </div>-->
    <div class=" col-lg-12 col-md-12 col-xs-12 col-sm-12">
      <!--<div class="col-xs-12 support-section section-home margin-bottom-20 no-padding">
          <div class="col-xs-12 header-section">
            <h3 class="title-section col-sm-8"> Ressources</h3>
            <hr>
          </div>
          <div class="col-xs-12">
            <span class="col-xs-12 text-left text-explain">
              Texte ressources ???????
          </span>
        </div>
      </div>-->
      <div class="col-xs-12">
          <div class="col-md-6 col-lg-3 col-sm-6 col-xs-12">
            <div class="col-xs-12 shadow2 section-home elt-section">
                <a href="#guide" class="lbh" title="Le guide des collectifs locaux">
                  <div class="content-img">
                      <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/siteDuPactePourLaTransition/r_organiser.png" class="img-responsive"/>
                  </div>
          <!--
                  <div class="col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1 col-lg-12 col-lg-offset-0 header-section">
                      <h3 class="text-purple">
                        Le guide des collectifs locaux
                      </h3>
                  </div> 
          -->
                  <div class="col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1 col-lg-12 col-lg-offset-0 cont-desc">
                      <span class="textExplain">
                        Des conseils et ressources pour créer ou structurer votre collectif, développer votre projet et échanger avec vos élu·es.
                        <!--Faites bouger votre commune en créant un collectif local ou en organisant un évènement.-->
                      </span>
                  </div>
                </a>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 col-sm-6 col-xs-12">
            <div class="col-xs-12 shadow2 section-home elt-section">
                <a href="#communication" class="lbh" title="Communication / Presse">
                  <div class="content-img">
                      <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/siteDuPactePourLaTransition/r_communiquer.png" class="img-responsive"/>
                  </div>
        <!--  
                  <div class="col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1 col-lg-12 col-lg-offset-0 header-section">
                      <h3 class="text-purple">
                        Communication / Presse
                      </h3>
                  </div>
        -->  
                  <div class="col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1 col-lg-12 col-lg-offset-0 cont-desc">
                      <span class="textExplain">
                        Des ressources pour communiquer sur l’action de votre collectif et
                        présenter le Pacte pour la Transition
                        <!--Echangez avec les habitant⋅es, parlez à vos candidat⋅es et aux médias locaux.-->
                      </span>
                  </div>
                 </a>
            </div>
          </div>
          <div class="col-md-6 col-lg-3 col-sm-6 col-xs-12">
            <div class="col-xs-12 shadow2 section-home elt-section">
                <a href="#thematiques" class="lbh" title="Approfondir les thématiques du Pacte">
                  <div class="content-img">
                      <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/siteDuPactePourLaTransition/r_documenter.png" class="img-responsive" />
                  </div>
          <!--
                  <div class="col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1 col-lg-12 col-lg-offset-0  header-section">
                      <h3 class="text-purple">
                        Approfondir les thématiques du Pacte
                      </h3>
                  </div>
          -->
                  <div class="col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1 col-lg-12 col-lg-offset-0 cont-desc">
                      <span class="textExplain">
                        Des documents, formations et liens pour aller plus loin sur les mesures du Pacte.
                        <!--Etudiez les mesures du Pacte pour la transition et appliquez-les concrètement.-->
                      </span>
                  </div>
                </a>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 col-sm-6 col-xs-12">
            <div class="col-xs-12 shadow2 section-home elt-section">
                <a href="#sorganiser" class="lbh" title="S’organiser, co-construire et mobiliser">
                  <div class="content-img">

                      <img src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/siteDuPactePourLaTransition/r_elues.png" class="img-responsive"/>
                  </div>
        <!--
                  <div class="col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1 col-lg-12 col-lg-offset-0 header-section">
                      <h3 class="text-purple">S’organiser, co-construire et mobiliser</h3>
                  </div>
        -->
                  <div class="col-sm-12 col-sm-offset-0 col-xs-10 col-xs-offset-1 col-lg-12 col-lg-offset-0 cont-desc">
                      <span class="textExplain">Des formations et ressources pour monter en compétence sur le fonctionnement de votre collectif.
                      </span>
                  </div>
                </a>
            </div>
          </div>

      </div>
    </div>
    <div class="col-xs-12 text-center margin-top-50">
        <a href="javascript:;" data-hash="#faq" class="lbh-menu-app btn-redirect-home" style="display: inline-block;padding: 10px 25px;border-radius: 4px;">
            <div class="text-center">
                <div class="col-md-12 no-padding text-center">
                    <h4 class="no-margin">
                      Questions fréquentes
                    </h4>
                </div>
            </div>
          </a>
        </div>
</div>
<!--<div id="sub-doc-page">
    <div class="col-xs-12 support-section section-home col-md-10 col-md-offset-1">
        <div class="col-xs-12 header-section">
        	<h3 class="title-section col-sm-8"><i class="fa fa-connectdevelop"></i> Mobilisation</h3>
        	<hr>
        </div>
        <div class="col-xs-12">
        	<h3 class="sub-header-section col-sm-12">Organiser une réunion pacte dans ma commune</h3>
        	<span class="col-xs-12 text-left text-explain">
        	Organiser une réunion pacte dans ma communeOrganiser une réunion pacte dans ma communeOrganiser une réunion pacte dans ma communeOrganiser une réunion pacte dans ma communeOrganiser une réunion pacte dans ma communeOrganiser une réunion pacte dans ma communeOrganiser une réunion pacte dans ma communeOrganiser une réunion pacte dans ma communeOrganiser une réunion pacte dans ma communeOrganiser une réunion pacte dans ma communeOrganiser une réunion pacte dans ma communeOrganiser une réunion pacte dans ma communeOrganiser une réunion pacte dans ma commune
    	</span>
        <div class="col-xs-12 text-right margin-top-50">
          	<a href="mailto:pacte@transitioncitoyenne.org" target="_blank" class="btn-main-menu btn-redirect-home">
          		<i class="fa fa-download"></i> Télécharger la ressource
          	</a>
        </div>
        </div>
        
        <div class="col-xs-12">
        	<h3 class="sub-header-section col-sm-12">Créer un collectif local</h3>
        	<span class="col-xs-12 text-left text-explain">
        	Créer un collectif localCréer un collectif localCréer un collectif localCréer un collectif localCréer un collectif localCréer un collectif localCréer un collectif localCréer un collectif localCréer un collectif localCréer un collectif localCréer un collectif localCréer un collectif localCréer un collectif localCréer un collectif localCréer un collectif localCréer un collectif localCréer un collectif local
    	</span>
        <div class="col-xs-12 text-right margin-top-50">
          	<a href="mailto:pacte@transitioncitoyenne.org" target="_blank" class="btn-main-menu btn-redirect-home">
          		<i class="fa fa-download"></i> Télécharger la ressource
          	</a>
        </div>
        </div>
        
        <div class="col-xs-12">
        	<h3 class="sub-header-section col-sm-12">charte des collectifs locaux</h3>
        	<span class="col-xs-12 text-left text-explain">
        	charte des collectifs locaux charte des collectifs locaux charte des collectifs locaux charte des collectifs locaux charte des collectifs locaux charte des collectifs locaux charte des collectifs locaux charte des collectifs locaux charte des collectifs locaux charte des collectifs locaux charte des collectifs locaux charte des collectifs locaux charte des collectifs locaux charte des collectifs locaux charte des collectifs locaux charte des collectifs locaux charte des collectifs locaux charte des collectifs locaux charte des collectifs locaux charte des collectifs locaux charte des collectifs locaux charte des collectifs locaux 
    	</span>
        <div class="col-xs-12 text-right margin-top-50">
          	<a href="mailto:pacte@transitioncitoyenne.org" target="_blank" class="btn-main-menu btn-redirect-home">
          		<i class="fa fa-download"></i> Télécharger la ressource
          	</a>
        </div>
        </div>
    </div>
</div>-->
