<table class="row masthead" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;background: white;width: 100%;position: relative;display: table;">
	<tbody>
		<tr style="padding: 0;vertical-align: top;text-align: left;"> <!-- Masthead -->
			<th class="small-12 large-12 columns first last" style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0 auto;text-align: left;line-height: 19px;font-size: 15px;padding-left: 16px;padding-bottom: 16px;width: 564px;padding-right: 16px;">
				<table style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;">
					<tr style="padding: 0;vertical-align: top;text-align: left;">
						<th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: center;line-height: 19px;font-size: 15px;">
							<center style="width: 100%;min-width: 532px;">
							<!--http://localhost:8888/ph/images/logoLTxt.jpg-->
							<?php if(!empty($logo2)){ ?> 
								<img src="<?php echo Yii::app()->getRequest()->getBaseUrl(true).$logo2 ?>" valign="bottom" alt="Logo Communecter" align="center" class="text-center" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;width: auto;max-width: 100%;clear: both;display: block;margin: 0 auto;float: none;text-align: center;margin:auto;">
							<?php } ?>
							</center>
						</th>
						<th class="expander" style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0 !important;margin: 0;text-align: left;line-height: 19px;font-size: 15px;visibility: hidden;width: 0;"></th>
					</tr>
				</table>
			</th>
		</tr>
	</tbody>
</table>
<table class="row" style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;position: relative;display: table;">
	<tbody>
		<tr style="padding: 0;vertical-align: top;text-align: left;"> <!-- Horizontal Digest Content -->
			<th class="small-12 large-12 columns first" style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0 auto;text-align: left;line-height: 19px;font-size: 15px;padding-left: 16px;padding-bottom: 16px;width: 564px;padding-right: 8px;">
				<table style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;">
					<tr style="padding: 0;vertical-align: top;text-align: left;">
						<th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: left;line-height: 19px;font-size: 15px;">
							<!--http://localhost:8888/ph/images/betatest.png-->
							<!-- <a href="http://www.communecter.org" style="color: #e33551;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: left;line-height: 1.3;text-decoration: none;"><img align="right" width="200" src="<?php echo Yii::app()->getRequest()->getBaseUrl(true)."/images/bdb.png"?>" style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;width: auto;max-width: 100%;clear: both;display: block;border: none;" alt="Intelligence collective"></a> -->
							<h3>Bienvenu.e sur le portail réunionnais de l'aide à l'amélioration de l'habitat</h3><br/><br/>
							<?php if(!empty($target) && !empty($target["type"]) && $target["type"] != Person::COLLECTION ){ ?>
								<?php echo $invitorName ?> vous invite à rejoindre <a href="<?php echo $urlRedirect ?>#page.type.<?php echo $target["type"] ?>.id.<?php echo $target["id"] ?>"><?php echo $target["name"] ?></a>. <br><br>
								Vous pouvez accepter ou non l'invitation sur <a href="<?php echo $urlRedirect ?>#page.type.<?php echo $target["type"] ?>.id.<?php echo $target["id"] ?>"><?php echo $target["name"] ?></a> puis finaliser votre inscription sur le portail Améliore Out Kaz.<br><br><br>
							
									 <table style="border-spacing: 0;border-collapse: collapse;padding: 0;vertical-align: top;text-align: left;width: 100%;">
            		
						              <tr style="padding: 0;vertical-align: top;text-align: left;">
						               <th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: right;width:50%;line-height: 19px;font-size: 15px;">
						                <a href="<?php echo $urlActions ."/answer/true" ?>" target="_blank" style="color: white;
						                    font-family: Helvetica, Arial, sans-serif;
						                    font-weight: normal;
						                    padding: 10px 40px;
						                    margin: 0;
						                    text-align: left;
						                    line-height: 1.3;
						                    text-decoration: none;
						                    width: 40%;
						                    margin-right: 2%;
						                    border-radius: 3px;
						                    background-color: #84d802;
						                    font-size: 15px;
						                    font-weight: 800;"><?php echo Yii::t("common", "Accept") ?></a>
						                </th>
						                <th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: left;line-height: 19px;font-size: 15px;width:50%;">
						               
						                <a href="<?php echo $urlActions."/answer/false" ?>" target="_blank" style="color: white;
						                  font-family: Helvetica, Arial, sans-serif;
						                  font-weight: normal;
						                  padding: 0;
						                  margin: 0;
						                  text-align: left;
						                  line-height: 1.3;
						                  text-decoration: none;
						                  width: 40%;
						                  margin-left: 2%;
						                  border-radius: 3px;
						                  background-color: #e33551;
						                  font-size: 15px;
						                  font-weight: 800;
						                  padding: 10px 40px;"><?php echo Yii::t("common", "Refuse") ?></a>
						                </th>
						                
						              </tr>
						              </table>
									<br/><br/>	
								<?php 
							 }else{ ?> 
								Vous avez été invité.e par l’équipe de coordination du portail améliore Out Kaz à rejoindre la plateforme. <br/><br/>
							<?php } ?>

								En quelques clics, venez découvrir le portail réunionnais de l'aide à l'amélioration de l'habitat qui facilite le dépôt et le suivi des dossiers des personnes qui en font la demande ! 
							<br/><br/>
								Rejoignez-nous en suivant simplement ce lien afin de <a href="<?php echo $urlValidation ?>" style="color: #e33b6c;font-family: Helvetica, Arial, sans-serif;font-weight: bold;padding: 0;margin: 0;text-align: center;line-height: 1.3;text-decoration: none;">
										finaliser votre compte
									</a> <br/>	
						</th>
					</tr>
					<tr style="padding: 0;vertical-align: top;text-align: left;">
						<th style="color: #3c5665;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;margin: 0;text-align: left;line-height: 19px;">
							<br/><br/>
							<p style="margin: 0;margin-bottom: 10px;color: #3c5665 !important;font-family: Helvetica, Arial, sans-serif;font-weight: normal;padding: 0;text-align: left;line-height: 19px;font-size: 12px;">
								Si le lien ne fonctionne pas vous pouvez le copier dans l'adresse de votre navigateur
							<div style="word-break: break-all;font-size: 12px;"><?php echo $urlValidation ?></div></p>
						</th>
					</tr>					
				</table>
	        </th>
		</tr>