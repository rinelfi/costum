    <?php
    $cssAnsScriptFilesModule = array(
        //Data helper
        '/js/dataHelpers.js',
        '/js/default/editInPlace.js',
        '/css/element/about.css'
    );
    HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, $this->module->getParentAssetsUrl());
    $elementParams=@$this->appConfig["element"];
    if(isset($this->costum)){
        $cssJsCostum=array();
        if(isset($elementParams["js"]) && $elementParams["js"] == "about.js")
            array_push($cssJsCostum, '/js/'.$this->costum["slug"].'/about.js');
        if(isset($elementParams["css"]) && $elementParams["js"] == "about.css")
            array_push($cssJsCostum, '/css/'.$this->costum["slug"].'/about.css');
        if(!empty($cssJsCostum))
            HtmlHelper::registerCssAndScriptsFiles($cssJsCostum, Yii::app()->getModule( "costum" )->getAssetsUrl());
    }

    if (Authorisation::isCostumAdmin()) 
        $edit =true;
    else
        $edit =false;

    if (@$element["collection"]) {
        $type = $element["collection"];
    }
    
    $openEdition = $element["preferences"]["isOpenEdition"];
?>

<style type="text/css">
    .profil-title-informations .btn-update-info{
        background-color: white;
        color: #172881;
        border-color: #172881;
    }

    .pageElement {
        margin-top: -84px;
    }

    .profil-title-informations .btn-update-info:hover {
        background-color: #172881 !important;
        color: white !important;
    }

    #btn-descriptions {
        background-color: white;
    }

    #btn-descriptions:hover {
        background-color: #4285f4 !important;
        color: white !important;
    }
</style>

<div class='col-md-12 margin-bottom-15'>
    <i class="fa fa-info-circle fa-2x"></i><span class='Montserrat' id='name-lbl-title'> <?php echo Yii::t("common","About") ?></span>
</div>
<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
    
<div class="pod-info-Description"  id="pod-info-Description">
 <?php if  (Yii::app()->session["userId"] != null)  {?> 
    <span class="text-nv-3 letter-blue">
        <i class="fa fa-file-text-o"></i>&nbsp;<?php echo Yii::t("common","Descriptions") ?>
    </span>
    
    <?php if($edit==true ||( $openEdition==true && Yii::app()->session["userId"] != null ) ){?>
        <button style="color:#4285f4;border-color: #4285f4;" class="btn-update-descriptions btn btn-update-blue pad-2 pull-right tooltips"
                    data-toggle="tooltip" data-placement="top" title="" alt="" data-original-title="<?php echo Yii::t("common","Update description") ?>" id="btn-descriptions">
        <?php if((!empty($element["shortDescription"])) || (!empty($element["description"])) ) {?>
            <i class="fa fa-edit"></i>&nbsp;<?php echo Yii::t("common", "Edit") ?>
        <?php }
                else{?>
            <i class="fa fa-plus"></i>&nbsp;<?php echo Yii::t("common", "Add") ?>
            <?php } ?>
        </button>
   
    <?php } ?>

 <hr class="line-hr letter-blue" >
  <?php } 

  else if((!empty($element["shortDescription"])) || (!empty($element["description"])) && (Yii::app()->session["userId"] == null )) { ?>  
        <span class="text-nv-3 letter-blue">
            <i class="fa fa-file-text-o"></i>&nbsp;<?php echo Yii::t("common","Descriptions") ?>
        </span> 
    <hr class="line-hr letter-blue">
   <?php } ?> 

    <div id="contenuDesc">  
    <?php
     if  (!( (empty($element["shortDescription"])) && Yii::app()->session["userId"] == null)) {?> 
        <div class="contentInformation margin-10">
            <span id="shortDescriptionAbout" name="shortDescriptionAbout" style="font-size: 18px;white-space: pre-line !important;" class="bold"><?php echo (@$element["shortDescription"]) ? $element["shortDescription"] : '<i>'.Yii::t("common","Not specified").'</i>'; ?></span>
            <span id="shortDescriptionAboutEdit" name="shortDescriptionAboutEdit"  class="hidden" ><?php echo (!empty($element["shortDescription"])) ? $element["shortDescription"] : ""; ?></span>
        </div>
    <?php }
    if  (!( (empty($element["description"])) && Yii::app()->session["userId"] == null)) {?>
        
        <div class="contentInformation margin-10">
            <div style="white-space: pre-line;" class="more no-padding" id="descriptionAbout"><?php echo (@$element["description"]) ? $element["description"] : '<i>'.Yii::t("common","Not specified").'</i>'; ?>
            </div>
        </div>   
    <?php }?>  

    </div>
</div>


    <div class="section light-bg pod-infoGeneral" id="pod-infoGeneral">
        <div class="row profil-title-informations">
            <div class="col-md-8 col-sm-8 col-xs-12">
                <span style="color: #172881;" class="text-nv-3">
                    <i class="fa fa-address-card-o"></i> <?php echo Yii::t("common","General information") ?>
                </span>
                <?php $class = ( !empty($element["collection"]) && $element["collection"] == "organizations") ? "btn-update-orga" : "btn-update-info" ?>
                <?php if($edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) ){?>
                <button class="<?= $class; ?> btn btn-update-red pad-2 pull-right  visible-xs tooltips"
                        data-toggle="tooltip" data-placement="top" title="" alt="" data-original-title="<?php echo Yii::t("common","Update general information") ?>">
                    <i class="fa fa-edit"></i>
                </button>
                <?php } ?>
            </div>
            <?php if($edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) ){?>
            <div class=" col-md-4 col-sm-4 col-xs-12 visible-sm visible-md visible-lg">
                <button class="btn-update-info btn btn-update-red pad-2 pull-right tooltips"
                        data-toggle="tooltip" data-placement="top" title="" alt="" data-original-title="<?php echo Yii::t("common","Update general information") ?>">
                    <i class="fa fa-edit"></i>&nbsp;<?php echo Yii::t("common", "Edit") ?>
                </button>
            </div>
            <?php } ?>

        </div>
        <hr style="color: #172881;" class="line-hr">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 margin-bottom-20">
                <div class="col-md-6 col-sm-6 col-xs-12" >
                    <div class="card-terr">
                        <div class="card-body">

                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-font" style="color:#172881;"></i></span>
                                <div class="media-body">
                                    <p class="padding-top-10" id="nameAbout"> <?php echo $element["name"]; ?> </p>
                                </div>
                            </div>

                            <?php if($type==Project::COLLECTION){ ?>
                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-spinner" style="color:#172881;"></i></span>
                                <div class="media-body">
                                    <p class="padding-top-10" id="avancementAbout"> <?php echo (@$element["properties"]["avancement"]) ? Yii::t("project",$element["properties"]["avancement"]) : '<i>'.Yii::t("common","Not specified").'</i>' ?> </p>
                                </div>
                            </div>
                            <?php } ?>

                            <?php if($type==Person::COLLECTION) { ?>
                                <div class="media contentInformation">
                                    <span class="ti-2x mr-3"><i class="fa fa-user-secret" style="color:#172881;"></i></span>
                                    <div class="media-body">
                                        <p class="padding-top-10"
                                           id="usernameAbout"><?php echo (@$element["username"]) ? $element["username"] : '<i>' . Yii::t("common", "Not specified") . '</i>' ?></p>
                                    </div>
                                </div>
                                <?php if (Preference::showPreference($element, $type, "birthDate", Yii::app()->session["userId"])) { ?>
                                    <div class="media contentInformation" >
                                        <span class="ti-2x mr-3"><i class="fa fa-birthday-cake" style="color:#172881;"></i></span>
                                        <div class="media-body">
                                            <p class="padding-top-10" id="birthDateAbout"><?php echo (@$element["birthDate"]) ? date("d/m/Y", strtotime($element["birthDate"]))  : '<i>'.Yii::t("common","Not specified").'</i>'; ?></p>
                                        </div>
                                    </div>

                                <?php }
                            }

                            if($type==Organization::COLLECTION || $type==Event::COLLECTION){ ?>
                            <div class="media contentInformation" id="divTypeAbout">
                                <span class="ti-2x mr-3"><i class="fa fa-list-ul" style="color:#172881;"></i></span>
                                <div class="media-body">
                                    <p class="padding-top-10"  id="typeAbout">
                                        <?php
                                        if(@$typesList && @$element["type"] && !empty($typesList[$element["type"]]))
                                            $showType=Yii::t( "category",$typesList[$element["type"]]);
                                        else if (@$element["type"])
                                            $showType=Yii::t( "category",$element["type"]);
                                        else
                                            $showType='<i>'.Yii::t("common","Not specified").'</i>';
                                        echo $showType; ?>
                                    </p>
                                </div>
                            </div>
                            <?php }

                            if( (   $type==Person::COLLECTION &&
                                Preference::showPreference($element, $type, "email", Yii::app()->session["userId"]) ) ||
                            in_array($type, [Organization::COLLECTION, Project::COLLECTION, Event::COLLECTION])) { ?>

                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-at" style="color:#172881;"></i></span>
                                <div class="media-body">
                                    <p class="padding-top-10" id="emailAbout"><?php echo (@$element["email"]) ? $element["email"]  : '<i>'.Yii::t("common","Not specified").'</i>'; ?></p>
                                </div>
                            </div>
                            <?php } ?>

                            <?php if ($type == Organization::COLLECTION) { ?>
                            <div class="media contentInformation">
                                <div class="media-body">
                                    <?php if ( is_array(@$element["typePlace"]) ) {?>
                                        <p class="padding-top-10" id="typePlaceAbout"> <span style="color: #172881">Type de lieu </span>
                                        <?php foreach ($element["typePlace"] as $key => $value) { ?>
                                            <?php echo (@$value) ? "<p>".$value."</p>"  : '<i>'.Yii::t("common","Not specified").'</i>'; ?>
                                    <?php } ?>
                                    </p>
                                    <?php }else{ ?>
                                    <p class="padding-top-10" id="typePlaceAbout"> <span style="color: #172881">Type de lieu : </span><?php echo (@$element["typePlace"]) ? $element["typePlace"]  : '<i>'.Yii::t("common","Not specified").'</i>'; ?></p>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php } ?>

                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <div class="card-terr">
                        <div class="card-body">
                            <?php if( $type != Person::COLLECTION /*&& $type != Organization::COLLECTION */ ){ ?>
                            <div class="media contentInformation" id="divParentAbout">
                                <span class="ti-2x mr-3"><i class="fa fa-link" style="color:#172881;"></i></span>
                                <div class="media-body">
                                    <span style="color: #172881;" class="card-title"><?php echo Yii::t("common","Carried by"); ?></span>
                                    <p class="padding-top-10" id="parentAbout">
                                        <?php
                                        if(!empty($element["parent"])){
                                            $count=count($element["parent"]);
                                            foreach($element['parent'] as $key =>$v){
                                                $heightImg=($count>1) ? 35 : 25;
                                                $imgPath = (@$v["profilThumbImageUrl"] && !empty($v["profilThumbImageUrl"])) ? Yii::app()->createUrl('/'.$v["profilThumbImageUrl"]) : $this->module->assetsUrl.'/images/thumb/default_'.$v["type"].'.png' ?>
                                                <a href="#page.type.<?php  echo $v['type']; ?>.id.<?php  echo $key; ?>"
                                                   class="lbh tooltips"
                                                    <?php if($count>1) echo 'data-toggle="tooltip" data-placement="left" title="'.$v["name"].'"' ?>>
                                                    <img src="<?php echo $imgPath ?>" class="img-circle" width='<?php echo $heightImg ?>' height='<?php echo $heightImg ?>' />
                                                    <?php  if ($count==1) echo $v['name']; ?>
                                                </a>
                                            <?php   }
                                        }else
                                            echo '<i>'.Yii::t("common","Not specified").'</i>';
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <?php } ?>

                            <?php if($type == Event::COLLECTION){ ?>
                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-link" style="color:#172881;"></i></span>
                                <div class="media-body">
                                    <span class="card-title ttr-4"><?php echo Yii::t("common","Organized by"); ?></span>
                                    <p class="padding-top-10" id="organizerAbout">
                                        <?php
                                        if(!empty($element["organizer"])){
                                            $count=count($element["organizer"]);
                                            foreach($element['organizer'] as $key =>$v){
                                                $heightImg=($count>1) ? 35 : 25;
                                                $imgPath = (@$v["profilThumbImageUrl"] && !empty($v["profilThumbImageUrl"])) ? Yii::app()->createUrl('/'.$v["profilThumbImageUrl"]) : $this->module->assetsUrl.'/images/thumb/default_'.$v["type"].'.png' ?>
                                                <a href="#page.type.<?php  echo $v['type']; ?>.id.<?php  echo $key; ?>"
                                                   class="lbh tooltips"
                                                    <?php if($count>1) echo 'data-toggle="tooltip" data-placement="left" title="'.$v["name"].'"' ?>>
                                                    <img src="<?php echo $imgPath ?>" class="img-circle" width='<?php echo $heightImg ?>' height='<?php echo $heightImg ?>' />
                                                    <?php  if ($count==1) echo $v['name']; ?>
                                                </a>
                                            <?php   }
                                        }else
                                            echo '<i>'.Yii::t("common","Not specified").'</i>';
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <?php } ?>

                            <?php if($type!=Poi::COLLECTION){ ?>
                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-desktop" style="color:#172881;"></i></span>
                                <div class="media-body">
                                    <p class="padding-top-10" id="webAbout">
                                        <?php
                                        if(@$element["url"]){
                                            //If there is no http:// in the url
                                            $scheme = ( (!preg_match("~^(?:f|ht)tps?://~i", $element["url"]) ) ? 'http://' : "" ) ;
                                            echo '<a href="'.$scheme.$element['url'].'" target="_blank" id="urlWebAbout" style="cursor:pointer;">'.$element["url"].'</a>';
                                        }else
                                            echo '<i>'.Yii::t("common","Not specified").'</i>'; ?>
                                    </p>
                                </div>
                            </div>
                            <?php } ?>

                            <?php  if($type==Organization::COLLECTION || $type==Person::COLLECTION){ ?>
                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-phone" style="color:#172881;"></i></span>
                                <div class="media-body">
                                    <p class="padding-top-10" id="fixeAbout">
                                        <?php
                                        $fixe = '<i>'.Yii::t("common","Not specified").'</i>';
                                        if( !empty($element["telephone"]["fixe"]))
                                            $fixe = ArrayHelper::arrayToString($element["telephone"]["fixe"]);

                                        echo $fixe;
                                        ?>
                                    </p>
                                </div>
                            </div>


                            <div class="media contentInformation">
                                <span class="ti-2x mr-3"><i class="fa fa-mobile" style="color:#172881;"></i></span>
                                <div class="media-body">
                                    <p class="padding-top-10" id="mobileAbout">
                                        <?php
                                        $mobile = '<i>'.Yii::t("common","Not specified").'</i>';
                                        if( !empty($element["telephone"]["mobile"]))
                                            $mobile = ArrayHelper::arrayToString($element["telephone"]["mobile"]);
                                        echo $mobile;
                                        ?>
                                    </p>
                                </div>
                            </div>

                            <?php } ?>

                        </div>
                    </div>
                </div>

                     <div class="col-md-12 col-sm-12 col-xs-12">
                         <div class="card features visible-lg visible-md visible-sm margin-bottom-20">

                         </div>

                        <?php if(!empty($element["tags"])) { ?>
                        <div class="card features margin-bottom-20">
                            <div class="card-body">

                                <div class="media contentInformation">
                                    <span class="ti-2x mr-3"><i class="fa fa-tags" style="color:#172881;"></i></span>
                                    <div class="media-body">
                                        <p class="padding-top-10">
                                        <ul class="tag-list no-padding">
                                            <?php
                                            if(!empty($element["tags"])){
                                                foreach ($element["tags"]  as $key => $tag) {
                                                    echo '<li class="tag"><a href="#search?text=#'.$tag.'"> <i class="fa fa-tag text-red"></i>&nbsp;'.$tag.'</a></li>';
                                                }
                                            }else{
                                                echo '<i>'.Yii::t("common","Not specified").'</i>';
                                            } ?>
                                        </ul>
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div >
                        <?php } ?>
                    </div>

                    <?php 
                        $allCateg = array();
                        if (!empty($element["categ"])) {
                            $allCateg["Catégorie"] = $element["categ"]; 
                        }
                        if (!empty($element["subcateg"])) {
                            $allCateg["Sous-catégorie"] = $element["subcateg"]; 
                        }
                        if (!empty($element["subsubcateg"])) {
                            $allCateg["Sous-sous-catégorie"] = $element["subsubcateg"]; 
                        }
                    ?>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                    <?php if(!empty($element["categ"])) { ?>

                        <span class="col-lg-10 text-nv-3" style="color: #336699">
                        Catégorie du dispositifs
                        </span>

                        <?php if($edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) ){?>
                        <button class="btn-update-info btn btn-update-red pad-2 pull-right  visible-xs tooltips"
                                data-toggle="tooltip" data-placement="top" title="" alt="" data-original-title="<?= Yii::t("common","Update general information") ?>">
                            <i class="fa fa-edit"></i>
                        </button>
                        <?php } ?>
                        <?php if($edit==true || ( $openEdition==true && Yii::app()->session["userId"] != null ) && $type == Project::COLLECTION ){?>
                            <div class=" col-lg-2 visible-sm visible-md visible-lg">
                                    <button class="btn-update-categ btn btn-update-red pad-2 pull-right tooltips"
                                        data-toggle="tooltip" data-placement="top" title="" alt="" data-original-title="Mettre à jour les catégorie">
                                    <i class="fa fa-edit"></i>&nbsp;<?= Yii::t("common", "Edit") ?>
                                </button>
                            </div>
                        <?php } ?>
                        <div class="card features margin-bottom-20 col-xs-12">
                            <div class="card-body">
                                <div class="media contentInformation">
                                    <span class="ti-2x mr-3"><i class="fa fa-tags gradient-fill"></i></span>
                                    <div class="media-body">
                                        <p class="padding-top-10">
                                        <ul style="display:grid;" class="categ-list no-padding">
                                            <?php
                                            if(!empty($allCateg)){
                                                foreach ($allCateg  as $key => $value) {
                                                    echo '<li style="font-size:16px !important;" class="'.$key.'">';
                                                    echo '<span style="color:#336699">'.$key.' : </span>'; 
                                                    echo $value.'</li>';
                                                }
                                            }else{
                                                echo '<i>'.Yii::t("common","Not specified").'</i>';
                                            } ?>

                                            <!--<li><a href=""><i class="fa fa-tag"></i> tag</a></li>-->
                                        </ul>
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div >
                    <?php } ?>
                    </div>
                </div>
            </div>
    </div>
</div>

<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
    <?php if(in_array($type, [Event::COLLECTION ,Project::COLLECTION ,Organization::COLLECTION])){ 
        $modeDate="recurrency";
        $title = Yii::t("common","When");
        $emptyval = ' Pas de date';
        if($type == Organization::COLLECTION) {
            $title = 'Heure d\'ouverture';
            $emptyval = ' Pas d\'horaire d\'ouverture';
            $modeDate="openingHours";

        } else if($type==Project::COLLECTION){
                    $modeDate="date";
            }
                echo $this->renderPartial('co2.views.pod.dateOH', array("element" => $element, "title" => $title, "emptyval" => $emptyval, "edit" => $edit , "openEdition" => $openEdition));
        };?>
    
    <!-- Recuperer l'adresse avec la carte dans co2.views.pod.address -->
    <?php echo $this->renderPartial('co2.views.pod.address', array("element" => $element, "type" => $type, "edit" => $edit , "openEdition" => $openEdition)); ?>
    <!-- Recuperer les reseau sociaux dans co2.views.pod.network -->
    <?php echo $this->renderPartial('co2.views.pod.socialNetwork', array("element" => $element, "type" => $type, "edit" => $edit , "openEdition" => $openEdition)); ?>
    
</div>
    
</div>

<script type="text/javascript">
    //Affichage de la map
    $("#divMapContent").show(function () {
        afficheMap();

    });
    var mapAbout = {};

    $("#InfoDescription").show(function () {
        controleAffiche();
    });

    function controleAffiche() {
        mylog.log(contextData.shortDescription, contextData.description);
        if((typeof contextData.shortDescription == "undefined") && (typeof contextData.description == "undefined")){
            $("#contenuDesc").addClass("hidden");
            //$("#btn-descriptions").html('<i class="fa fa-plus"> Ajouter');

            };
    };


    function afficheMap(){
            mylog.log("afficheMap");

            //Si contextData.geo est undefined caché la carte
            if(typeof contextData.geo == "undefined"){
            $("#divMapContent").addClass("hidden");
            };

            var paramsMapContent = {
                container : "divMapContent",
                latLon : contextData.geo,
                activeCluster : false,
                zoom : 16,
                activePopUp : false
            };


            mapAbout = mapObj.init(paramsMapContent);

            var paramsPointeur = {
                elt : {
                    id : contextData.id,
                    type : contextData.type,
                    geo : contextData.geo
                },
                center : true
            };
            mapAbout.addMarker(paramsPointeur);
            mapAbout.hideLoader();


    };


    //var paramsPointeur[contextId] = contextData;
    var formatDateView = "DD MMMM YYYY à HH:mm" ;
    var formatDatedynForm = "DD/MM/YYYY HH:mm" ;

    jQuery(document).ready(function() {
        bindDynFormEditable();
        initDate();
        inintDescs();
        //changeHiddenFields();
        bindAboutPodElement();
        bindExplainLinks();

        $("#small_profil").html($("#menu-name").html());
        $("#menu-name").html("");

        $(".cobtn").click(function () {
            communecterUser();
        });

        $(".btn-update-geopos").click(function(){
            updateLocalityEntities();
        });

        $("#btn-add-geopos").click(function(){
            updateLocalityEntities();
        });

        $("#btn-update-organizer").click(function(){
            updateOrganizer();
        });
        $("#btn-add-organizer").click(function(){
            updateOrganizer();
        });

        $("#btn-remove-geopos").click(function(){
            removeAddress();
        });

        $("#btn-update-geopos-admin").click(function(){
            findGeoPosByAddress();
        });


    });

    $(".btn-update-orga").off().on( "click", function(){
        var form = {
            saveUrl : baseUrl+"/costum/hubterritoires/updateblock/",
            dynForm : {
                jsonSchema : {
                    title : trad["Update description"],
                    icon : "fa-key",
                    onLoads : {
                        markdown : function(){
                            dataHelper.activateMarkdown("#ajaxFormModal #description");
                            $("#ajax-modal .modal-header").removeClass("bg-dark bg-purple bg-red bg-azure bg-green bg-green-poi bg-orange bg-yellow bg-blue bg-turq bg-url")
                                                              .addClass("bg-dark");
                        }
                    },
                    afterSave : function(data){
                        mylog.dir(data);
                        if(data.result&& data.resultGoods && data.resultGoods.result){
                            if(data.resultGoods.values.shortDescription=="")
                                $(".contentInformation #shortDescriptionAbout").html('<i>'+trad["notSpecified"]+'</i>');
                            else
                                $(".contentInformation #shortDescriptionAbout").html(data.resultGoods.values.shortDescription);
                            $(".contentInformation #shortDescriptionAboutEdit").html(data.resultGoods.values.shortDescription);
                            $("#shortDescriptionHeader").html(data.resultGoods.values.shortDescription);
                            if(data.resultGoods.values.description=="")
                                $(".contentInformation #descriptionAbout").html(dataHelper.markdownToHtml('<i>'+trad["notSpecified"]+'</i>'));
                            else
                                $(".contentInformation #descriptionAbout").html(dataHelper.markdownToHtml(data.resultGoods.values.description));
                            $("#descriptionMarkdown").html(data.resultGoods.values.description);

                            if(data.resultGoods.values.category=="")
                                $(".contentInformation #categoryAbout").html('<i>'+trad["notSpecified"]+'</i>');
                            else
                                $(".contentInformation #categoryAbout").html(data.resultGoods.values.category);
                            $(".contentInformation #categoryAboutEdit").html(data.resultGoods.values.category);
                        
                        }
                        dyFObj.closeForm();
                        urlCtrl.loadByHash(location.hash);
                        // location.reload();
                    },
                    properties : {
                        block : dyFInputs.inputHidden(),
                        typeElement : dyFInputs.inputHidden(),
                        descMentions : dyFInputs.inputHidden(),
                        isUpdate : dyFInputs.inputHidden(true),
                        name : dyFInputs.name("organization"),
                        type : dyFInputs.inputSelect(tradDynForm["organizationType"], tradDynForm["organizationType"], organizationTypes, { required : true }),
                        tags : dyFInputs.tags(),
                        email : dyFInputs.text(),
                        fixe : {
                            inputType : "text",
                            label : "Fixe",
                            placeholder : "Téléphone fixe",
                        },
                        mobile : {
                            inputType : "text",
                            label : "Mobile",
                            placeholder : "Téléphone mobile",
                        },
                        typePlace : {
                            inputType : "select",
                            label : "Type de lieu",
                            placeholder : "Choisir un type de lieu",
                            options : {
                                "Amicale Laïque" : "Amicale Laïque",
                                "BIJ/PIJ" : "BIJ/PIJ",
                                "Centre Social" : "Centre Social",
                                "Cyberbase / Cybercentre" : "Cyberbase / Cybercentre",
                                "Fablab / Hackerspace" :"Fablab / Hackerspace",
                                "Maison de Quartier" : "Maison de Quartier",
                                "Maison des Services" : "Maison des Services",
                                "Médiathèque" : "Médiathèque"
                            },
                            groupOptions : false,
                            groupSelected : false,
                            optionsValueAsKey :true,
                            select2 : {
                                multiple : true
                            }
                        },
                        url : dyFInputs.inputUrl(),
                        parent : {
                            inputType : "finder",
                            label : tradDynForm.ispartofevent,
                            multiple : false,
                            initType: ["events"],
                            openSearch :true,
                            init: function(){
                                mylog.log("finder init2");
                                var finderParams = {
                                    id : "parent",
                                    multiple : false,
                                    initType : ["events"],
                                    values : null,
                                    update : null
                                };

                                finder.init(finderParams, 
                                    function(){
                                        $.each(finder.selectedItems, function(e, v){
                                            startDateParent=v.startDate;
                                            endDateParent=v.endDate;
                                        });
                                        $("#startDateParent").val(startDateParent);
                                        $("#endDateParent").val(endDateParent);
                                        if($("#parentstartDate").length <= 0){
                                            $("#ajaxFormModal #startDate").after("<span id='parentstartDate'><i class='fa fa-warning'></i> "+tradDynForm.eventparentStartDate+" : "+ moment( startDateParent /*,"YYYY-MM-DD HH:mm"*/).format('DD/MM/YYYY HH:mm')+"</span>");
                                            $("#ajaxFormModal #endDate").after("<span id='parentendDate'><i class='fa fa-warning'></i> "+tradDynForm.eventparentEndDate+" : "+ moment( endDateParent /*,"YYYY-MM-DD HH:mm"*/).format('DD/MM/YYYY HH:mm')+"</span>");
                                        }
                                        $("#parentstartDate").html("<i class='fa fa-warning'></i> "+tradDynForm.eventparentStartDate+" : "+moment( startDateParent ).format('DD/MM/YYYY HH:mm'));
                                        $("#parentendDate").html("<i class='fa fa-warning'></i> "+tradDynForm.eventparentEndDate+" : "+moment( endDateParent ).format('DD/MM/YYYY HH:mm')); 
                                    }
                                );
                            }
                        }
                    }
                }
            }
        };

        var dataUpdate = {
            block : "info",
            id : contextData.id,
            typeElement : contextData.type,
            name : contextData.name,
            type : contextData.typeOrga,
            tags : contextData.tags,
            email : contextData.email,
            fixe : contextData.fixe,
            mobile : contextData.mobile,
            url : contextData.url,
            parent : contextData.parent,
            typePlace : contextData.typePlace
        };

        dyFObj.openForm(form, "markdown", dataUpdate);
    });

    $(".btn-update-info").off().on( "click", function(){
        var form = {
            saveUrl : baseUrl+"/costum/hubterritoires/updateblock/",
            dynForm : {
                jsonSchema : {
                    title : trad["Update description"],
                    icon : "fa-key",
                    onLoads : {
                        markdown : function(){
                            dataHelper.activateMarkdown("#ajaxFormModal #description");
                            $("#ajax-modal .modal-header").removeClass("bg-dark bg-purple bg-red bg-azure bg-green bg-green-poi bg-orange bg-yellow bg-blue bg-turq bg-url")
                                                              .addClass("bg-dark");
                        }
                    },
                    afterSave : function(data){
                        mylog.dir(data);
                        if(data.result&& data.resultGoods && data.resultGoods.result){
                            if(data.resultGoods.values.shortDescription=="")
                                $(".contentInformation #shortDescriptionAbout").html('<i>'+trad["notSpecified"]+'</i>');
                            else
                                $(".contentInformation #shortDescriptionAbout").html(data.resultGoods.values.shortDescription);
                            $(".contentInformation #shortDescriptionAboutEdit").html(data.resultGoods.values.shortDescription);
                            $("#shortDescriptionHeader").html(data.resultGoods.values.shortDescription);
                            if(data.resultGoods.values.description=="")
                                $(".contentInformation #descriptionAbout").html(dataHelper.markdownToHtml('<i>'+trad["notSpecified"]+'</i>'));
                            else
                                $(".contentInformation #descriptionAbout").html(dataHelper.markdownToHtml(data.resultGoods.values.description));
                            $("#descriptionMarkdown").html(data.resultGoods.values.description);

                            if(data.resultGoods.values.category=="")
                                $(".contentInformation #categoryAbout").html('<i>'+trad["notSpecified"]+'</i>');
                            else
                                $(".contentInformation #categoryAbout").html(data.resultGoods.values.category);
                            $(".contentInformation #categoryAboutEdit").html(data.resultGoods.values.category);
                        
                        }
                        dyFObj.closeForm();
                        urlCtrl.loadByHash(location.hash);
                        // location.reload();
                    },
                    properties : {
                        block : dyFInputs.inputHidden(),
                        typeElement : dyFInputs.inputHidden(),
                        descMentions : dyFInputs.inputHidden(),
                        isUpdate : dyFInputs.inputHidden(true),
                        name : dyFInputs.name("organization"),
                        type : dyFInputs.inputSelect(tradDynForm["organizationType"], tradDynForm["organizationType"], organizationTypes, { required : true }),
                        tags : dyFInputs.tags(),
                        email : dyFInputs.text(),
                        url : dyFInputs.inputUrl(),
                        parent : {
                            inputType : "finder",
                            label : tradDynForm.ispartofevent,
                            multiple : false,
                            initType: ["events"],
                            openSearch :true,
                            init: function(){
                                mylog.log("finder init2");
                                var finderParams = {
                                    id : "parent",
                                    multiple : false,
                                    initType : ["events"],
                                    values : null,
                                    update : null
                                };

                                finder.init(finderParams, 
                                    function(){
                                        $.each(finder.selectedItems, function(e, v){
                                            startDateParent=v.startDate;
                                            endDateParent=v.endDate;
                                        });
                                        $("#startDateParent").val(startDateParent);
                                        $("#endDateParent").val(endDateParent);
                                        if($("#parentstartDate").length <= 0){
                                            $("#ajaxFormModal #startDate").after("<span id='parentstartDate'><i class='fa fa-warning'></i> "+tradDynForm.eventparentStartDate+" : "+ moment( startDateParent /*,"YYYY-MM-DD HH:mm"*/).format('DD/MM/YYYY HH:mm')+"</span>");
                                            $("#ajaxFormModal #endDate").after("<span id='parentendDate'><i class='fa fa-warning'></i> "+tradDynForm.eventparentEndDate+" : "+ moment( endDateParent /*,"YYYY-MM-DD HH:mm"*/).format('DD/MM/YYYY HH:mm')+"</span>");
                                        }
                                        $("#parentstartDate").html("<i class='fa fa-warning'></i> "+tradDynForm.eventparentStartDate+" : "+moment( startDateParent ).format('DD/MM/YYYY HH:mm'));
                                        $("#parentendDate").html("<i class='fa fa-warning'></i> "+tradDynForm.eventparentEndDate+" : "+moment( endDateParent ).format('DD/MM/YYYY HH:mm')); 
                                    }
                                );
                            }
                        }
                    }
                }
            }
        };

        var dataUpdate = {
            block : "info",
            id : contextData.id,
            typeElement : contextData.type,
            name : contextData.name,
            type : contextData.typeOrga,
            tags : contextData.tags,
            email : contextData.email,
            url : contextData.url,
            parent : contextData.parent
        };

        dyFObj.openForm(form, "markdown", dataUpdate);
    });

     $(".btn-update-categ").off().on( "click", function(){
        $('#exampleModalLong').modal('show');
        costum.mednumRhoneAlpe.init();
        categ = "";
        subcateg = "";
        subsubcateg = "";
        
        $('.validate-form-ressources').click(function(){
            tplCtx = {};

            tplCtx.collection = "projects";
            tplCtx.id = contextData._id.$id;
            tplCtx.path = "allToRoot";

            tplCtx.value = {
                categ : categ,
                subcateg : subcateg
            };

            tplCtx.value["subsubcateg"] = (typeof subsubcateg != "undefined" && subsubcateg != "") ? subsubcateg : "";

            if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
            else {
                dataHelper.path2Value( tplCtx, function(params) { 
                    $("#exampleModalLong").modal('hide');
                    toastr.success("Élément mis à jour");
                    urlCtrl.loadByHash(location.hash);
                } );
            }
        });
    });

    function inintDescs() {
        mylog.log("inintDescs");
        if($("#descriptionAbout").length > 0){
            if(canEdit == true || openEdition== true)
                descHtmlToMarkdown();
            mylog.log("after");
            mylog.log("inintDescs", $("#descriptionAbout").html());
            var descHtml = "<i>"+trad.notSpecified+"</i>";
            if($("#descriptionAbout").html().length > 0){
                descHtml = dataHelper.markdownToHtml($("#descriptionAbout").html()) ;
            }
            $("#descriptionAbout").html(descHtml);
            //$("#descProfilsocial").html(descHtml);
            mylog.log("descHtml", descHtml);
        }
    }

    function initDate() {//DD/mm/YYYY hh:mm

        formatDateView = "DD MMMM YYYY à HH:mm" ;
        formatDatedynForm = "DD/MM/YYYY HH:mm" ;
        
        mylog.log("formatDateView", formatDateView);
        //if($("#startDateAbout").html() != "")

            $("#startDateAbout").html(moment(contextData.startDateDB).local().locale(mainLanguage).format(formatDateView));

            $("#endDateAbout").html(moment(contextData.endDateDB).local().locale(mainLanguage).format(formatDateView));

        if($("#birthDate").html() != "")
            $("#birthDate").html(moment($("#birthDate").html()).local().locale(mainLanguage).format("DD/MM/YYYY"));
        $('#dateTimezone').attr('data-original-title', "Fuseau horaire : GMT " + moment().local().format("Z"));
    }

    function AddReadMore() {
    var showChar = 300;
    var ellipsestext = "...";
    var moretext = "Lire la suite";
    var lesstext = "Lire moins";
    $('.more').each(function() {
      var content = $(this).html();
      var textcontent = $(this).text();

      if (textcontent.length > showChar) {

        var c = textcontent.substr(0, showChar);
        //var h = content.substr(showChar-1, content.length - showChar);

        var html = '<span class="container"><span>' + c + '</span>' + '<span class="moreelipses">' + ellipsestext + '</span></span><span style="white-space:pre-line;" class="morecontent">' + content + '</span>';

        $(this).html(html);
        $(this).after('<a href="" class="morelink">' + moretext + '</a>');
      }

    });

    $(".morelink").click(function() {   
      if ($(this).hasClass("less")) {
        $(this).removeClass("less");
        $(this).html(moretext);
        $(this).prev().children('.morecontent').fadeToggle(500, function(){
          $(this).prev().fadeToggle(500);
          $(this).prev().css("display","pre-line");
        });
       
      } else {
        $(this).addClass("less");
        $(this).html(lesstext);
        $(this).prev().children('.container').fadeToggle(500, function(){
          $(this).next().fadeToggle(500);
          $(this).prev().css("display","pre-line");
        });
      }
      //$(this).prev().children().fadeToggle();
      //$(this).parent().prev().prev().fadeToggle(500);
      //$(this).parent().prev().delay(600).fadeToggle(500);
      
      return false;
    });
}
$(function() {
    //Calling function after Page Load
    AddReadMore();
});



</script>

