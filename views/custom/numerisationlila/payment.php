<style type="text/css">
    select option, select optgroup{
        font-size: 12pt !important;
        padding-top: 3em !important;
    }

    .form-control{
        padding: .5em !important;
    }
</style>

<?php 

$answer = PHDB::findAndSort("answers", array("source.key"=>$this->costum['slug'], "draft"=>['$exists' => false ]), array( 'updated' => -1 ));
$kunik = "5611212265512151514226";
if($answer){ ?>

    <div class="col-md-6">
        <?php
            $types = array();
            if(!function_exists("get_types")){
                function get_types(&$types, $label){
                    $exist = false;
                    foreach ($types as $k => &$v) {
                        if($v==$label){
                            $exist = true;
                        }
                    }

                    if(!$exist){
                        array_push($types, "$label");
                    }
                }
            }

            # Initialize parameters data
            $paramsData = [
                "types" => [
                    "type0" => [
                        "type" => "Acteurs producteurs",
                        "label" => "",
                        "droit" => 0
                                
                    ],
                    "type1" => [
                        "type" => "Bénéficiaires",
                        "label" => "",
                        "droit" => 0
                    ]
                ]
            ];

            // # Set parameters data
            // if( isset($form["params"][$kunik]["types"]) )
            //     $paramsData["types"] = $form["params"][$kunik]["types"];


            // foreach ($paramsData["types"] as $tk => $tv){
            //     get_types($types, $tv["type"]);
            // }
            
            
            // remove the to test directily (dev) "draft" => ["$exists" => false ]]
            $theAnswers = PHDB::findAndSort("answers", array("source.key"=>$this->costum['slug'], "draft"=>['$exists' => false ]), array( 'updated' => -1 ));
            
        ?>



        <?php 
            //$p = PHDB::findOne("payments", array('user' => $_SESSION["userId"], 'source.form' => $form["id"]));
        ?>

        <br>
        <div class="form-group">
            <select id="" data-form="" class="form-control saveOneByOne">
                <option></option>
                <?php foreach ($types as $gkey => $gvalue) { ?>
                    <optgroup label="<?php echo $gvalue ?>">
                        <?php foreach ($paramsData["types"] as $tkey => $tvalue) {
                            if($tvalue["type"]==$gvalue){
                                echo '<option value="'.base64_encode(json_encode($tvalue)).'">'.$tvalue["label"].' ( <b> '.$tvalue["droit"].' € </b> )</option>';
                            }
                        } ?>
                    </optgroup>
                <?php } ?>
            </select>
            <?php 

            // echo ($canEditForm)?"<div class='input-group-btn'>".$editParamsCheckbox."</div>":""; ?>
        </div>

        <div class="form-group">
            <label>Nombre de part</label>
            <input type="number" id="nbPart" data-form="" name="nbPart" min="1" value="1" class="col-sm-10 col-md-6 form-control saveOneByOne">
        </div>
        <br>
        <hr>
        <div class="form-group">
            <b>Montant Total : </b> 
            <span id="total">0 €</span>
        </div>
        <hr>
        <?php if(isset($answer["answers"]["isPaid"])){ ?>
        <div class="form-group">
            <b>Statut : </b>
            <span class="label label-success"> PAYE </span>
        </div>
        <?php } ?>
        <div class="form-group">
            <a id="btn-payement" disabled class="btn btn-success btn-lg" style="text-decoration : none;"> 
                Proceder au Payement
            </a>
            <?php if(isset($answer["answers"]["isPaid"])){ ?>
                <a id="btn-facturation" class="btn btn-success btn-lg" href="<?= Yii::app()->baseUrl; ?>/survey/payment/invoice?id=<?= $this->costum['contextId'] ?>&slug=<?= $this->costum['slug']; ?>&user=<?= $_SESSION['userId']; ?>&form=<?= $answer["form"]; ?>" target="_blank" style="text-decoration : none;"> 
                    Télécharger Facture
                </a>
            <?php } ?>
            <span id="load-payment"></span>
        </div>
    </div>
    
    <script type="text/javascript">
 
    </script>
    <?php } else {
        //echo "<h4 class='text-red'>evaluation works with existing answers</h4>";
    } ?>