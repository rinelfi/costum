<!-- <footer class="col-xs-12 col-lg-12 no-padding" style="background-color:rgba(0,0,0,.6)">
    <div class="no-padding col-lg-12 col-xs-12 col-footer col-footer-step dfooter">
        <div class="col-lg-3 col-xs-3">
        Propulsé par Communecter
        </div>
        
        <div class="col-lg-455 col-xs-9 no-padding" style="font-size: 13px;margin-top: 1%;">
            <a href="javascript:;" data-hash="#mentions" class="lbh-menu-app col-lg-3 col-xs-4" style="color:white;">Mentions légales</a>
            <a style="color:white;" href="javascript:;" data-hash="#politiqueConf" class="lbh-menu-app col-lg-3 col-xs-4">Politique de confidentialité</a>
            <a href="http://www.lapossession.re" style="color:white;" target="_blank" class="col-lg-3 col-xs-4">Site de la mairie</a>
        </div>
        <img class="no-padding hidden-xs hidden-sm img-responsive pull-right" id="footerPossession" src="<?php //echo Yii::app()->getModule("costum")->assetsUrl ?>/images/lapossession/footer.png">
    </div>
</footer> -->

<footer class="col-lg-12 col-xs-12 col-sm-12 no-padding text-center" style="background-color:rgba(0,0,0,.6)">
    <!-- <div class="col-lg-12 col-sm-12 col-xs-12 col-footer col-footer-step dfooter"> -->
        <div class="col-lg-9 col-xs-12 col-sm-9 no-padding contain-footer">
            <a style="color:white;" href="www.communecter.org" class="col-lg-3 col-xs-12 col-sm-3">Propulsé par Communecter</a>
            <a href="javascript:;" data-hash="#mentions" class="lbh-menu-app col-lg-3 col-xs-12 col-sm-3" style="color:white;">Mentions légales</a>
            <a style="color:white;" href="javascript:;" data-hash="#politiqueConf" class="lbh-menu-app col-lg-3 col-xs-12 col-sm-3">Politique de confidentialité</a>
            <a href="http://www.lapossession.re" style="color:white;" target="_blank" class="col-lg-3 col-xs-12 col-sm-3">Site de la mairie</a>

        </div>

        <div class="no-padding col-lg-3 col-sm-3 hidden-xs">
            <img class="no-padding img-responsive pull-right" src="<?php echo Yii::app()->getModule("costum")->assetsUrl ?>/images/lapossession/footer.png">
        </div>
    <!-- </div> -->
</footer>