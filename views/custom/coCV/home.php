
<?php          
$form = PHDB::findOne(Form::COLLECTION, array("id"=>$this->costum["formId"]));
$cvID = $_GET['cv'];
$userID = Yii::app()->session["userId"];
$myresume = PHDB::findOne(Form::ANSWER_COLLECTION, array("user"=> $cvID,"form"=>(string)$form["_id"]));
$reponse = $myresume["answers"];
$profil = $reponse["form2072020_1115_0"];

$profil_picture = (isset(PHDB::find(Person::COLLECTION)[$myresume['user']]['profilMediumImageUrl'])) ?
(PHDB::find(Person::COLLECTION)[$myresume['user']]['profilMediumImageUrl']) : '';
$design = (isset($myresume['design'])) ? $myresume['design'] : 'undefined';
$formations = (isset($reponse["form2772020_98_1"]['form2772020_98_11'])) ? $reponse["form2772020_98_1"]['form2772020_98_11'] : 'undefined';
$stages = (isset($reponse["form2772020_918_2"]['form2772020_918_21'])) ? $reponse["form2772020_918_2"]['form2772020_918_21'] : 'undefined';
$skills = (isset($reponse["form2772020_923_3"]['form2772020_923_31'])) ? $reponse["form2772020_923_3"]['form2772020_923_31'] : 'undefined';
$langues = (isset($reponse["form2772020_946_4"]['form2772020_946_41'])) ? $reponse["form2772020_946_4"]['form2772020_946_41'] : 'undefined';
$sports = (isset($reponse["form2972020_1645_5"]["form2972020_1645_51"])) ? $reponse["form2972020_1645_5"]["form2972020_1645_51"] : 'undefined';
$loisirs = (isset($reponse["form2972020_1645_5"]["form2972020_1645_52"])) ? $reponse["form2972020_1645_5"]["form2972020_1645_52"] : 'undefined';

$divers = (isset($reponse["form2972020_1645_5"]["form2972020_1645_53"])) ? $reponse["form2972020_1645_5"]["form2972020_1645_53"] : 'undefined';

 $desinConfig = (Yii::app()->session["userId"] == $cvID) ? '
 <button data-toggle="modal" id="settingsBtn" class="pull-right bg-turq" data-target="#modalDesign"><a href="#" class="Button"><i class="fa fa-cogs"></i></a></button>
 <div class="statistics-btn" style="position: fixed; right: 5px; top: 70px; z-index: 10">
 <a href="#answer.index.id.'.($myresume['_id']).'.mode.w" class="btn btn-danger lbh"><i class="fa fa-edit"></i></a>
 </div>' : "";	
?>
<div class="settings">
	<div class="modal right fade" id="modalDesign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel2">Right Sidebar</h4>
				</div>
				<div class="modal-body">
						<p class="text-center">Choisissez votre model du CV</p>
					<div class="text-center"><hr>
						<div id="design1">
							<div class="design-image" style="background-image: url(<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coCV/1.png);"><br>
							</div>
						</div><hr>
						<div id="design2">
							<div style="background-image: url(<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coCV/2.png)" class="design-image" ><br>
							</div>
						</div><hr>
						<div id="design3">
							<div style="background-image: url(<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coCV/3.png)" class="design-image" ><br>
							</div>
						</div><hr>
						<div id="design4">
							<div style="background-image: url(<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/coCV/4.png)" class="design-image" ><br>
							</div>
						</div><hr>
						<div>
							<button id="btnSave" class="btn btn-success">Enregistrer</button>
						</div>
					</div>
				</div>
			</div><!-- modal-content -->
		</div><!-- modal-dialog -->
	</div><!-- modal -->
</div><!-- container -->
<?php echo $desinConfig; ?>
<div id="design"></div>
<script type="text/javascript">
numDesign = "0";
function design1(){
	$("#design").html(`
	<div class="container"><div class="panel"><div class="container"><div id="container_etat-civile" class="tab-pane fade in active"><img class="img-thumbnail profil" src="<?php echo $profil_picture ?>"><div class="col-md-8 header-right" id="profil"><h3><?php echo $profil["multitextvalidationform2072020_1015_01"] ?></h3><div id="ecole"></div><ul class="list-inline"><li style="padding: 2%"><ul class="address-text"><li><b>Né le: </b><?php echo $profil["multitextvalidationform2072020_1015_03"] ?></li></ul></li><li style="padding: 2%"><ul class="address-text"><li><b>Sexe: </b><?php echo $profil["multiRadioform2072020_1015_04"]["value"] ?></li></ul></li><li style="padding: 2%"><ul class="address-text"><li><b>TEL: </b><?php echo $profil["multitextvalidationform2072020_1015_05"] ?></li></ul></li><li style="padding: 2%"><ul class="address-text"><li><b>E-MAIL: </b> <a href="mailto:<?php echo $profil['multitextvalidationform2072020_1015_08'] ?>"><?php echo $profil["multitextvalidationform2072020_1015_08"] ?></a></li></ul></li></ul></div></div></div></div><br><div class="jumbotron"><div class="tab-content"><div id="container_form-dipl" class="container" style="background-color: white; width: 100%; padding-top: 20px"></div></div><div id="stg-exp" class="container" style="width: 100%; padding-top: 20px;"></div><div id="cpt-info" class="container" style="background-color: white; width: 100%; padding-top: 20px"></div><div id="langue" class="container" style="width: 100%; padding-top: 20px"></div><div id="container_divers" class="container" style="background-color: white; width: 100%; padding-top: 20px"><div class="page-header text-center"><h3>Centre d’intérêt et divers</h3></div><div class="container"><div class="skills-info"><div class="col-md-6 bar-grids"><div id="sports" class="col-md-8 about-left s19 li:before"></div></div><div class="col-md-6 bar-grids"><div id="loisirs" class="col-md-8 about-left"></div></div><div class="col-md-6 bar-grids"><div id="dvrs" class="col-md-8 about-left"></div></div><div class="clearfix"> </div></div></div></div></div></div></div>`);
		$('#design1').css('background-color','#eee');$('#design3').css('background-color','#fff');$('#design2').css('background-color','#fff');$('#design4').css('background-color','#fff');
		btnFormation();
		btnStages();
		btnCompetence();
		btnLangue();
		btnOthers();
		numDesign = "0";
	}
function design2(){
	$("#design").html(`
	<div class="container"><div class="jumbotron"><div class="tab-content"><div class="col-md-4" style="background-color: #d9decb;"><div class="container-fluid bg-1 text-center"><img class="img-circle profil" src="<?php echo $profil_picture ?>"><div class="page-header text-center"><h2><?php echo $profil["multitextvalidationform2072020_1015_01"] ?></h2></div></div><div id="container_cpt-info" class="container" style="width: 100%;"><div class=""><ul class="list-inline"><li style="padding: 2%"><ul class="address-text"><li><b>Né le: </b><?php echo $profil["multitextvalidationform2072020_1015_03"] ?></li></ul></li><li style="padding: 2%"><ul class="address-text"><li><b>Sexe: </b><?php echo $profil["multiRadioform2072020_1015_04"]["value"] ?></li></ul></li><li style="padding: 2%"><ul class="address-text"><li><b>TEL: </b><?php echo $profil["multitextvalidationform2072020_1015_05"] ?></li></ul></li><li style="padding: 2%"><ul class="address-text"><li><b>E-MAIL: </b> <a href="mailto:<?php echo $profil['multitextvalidationform2072020_1015_08'] ?>"><?php echo $profil["multitextvalidationform2072020_1015_08"] ?></a></li></ul></li></ul></div></div><div id="cpt-info" class="container" style="width: 100%; padding-top: 20px"></div><div id="langue" class="container" style="width: 100%; padding-top: 20px"></div><div id="container_divers" class="container" style="width: 100%;"><div class="page-header text-center"><h3>Centre d’intérêt et divers</h3></div><div class="container"><div class="skills-info"><div class="bar-grids"><div id="sports" class="about-left s19 li:before"></div></div><div class="bar-grids"><div id="loisirs" class="about-left"></div></div><div class="bar-grids"><div id="dvrs" class="about-left"></div></div><div class="clearfix"> </div></div></div></div></div><div class="col-md-8" style="background-color: white;"><div id="stg-exp" class="container" style="width: 100%; padding-top: 20px;"></div><div id="container_form-dipl" class="container" style="background-color: white; width: 100%; padding-top: 20px"></div></div></div></div></div></div>`);
	$('#design2').css('background-color','#eee');$('#design3').css('background-color','#fff');$('#design4').css('background-color','#fff');$('#design1').css('background-color','#fff');
	btnFormation();
	btnStages();
	btnCompetence();
	btnLangue();
	btnOthers();
	numDesign = "1";
}

function design3(){

	$("#design").html(`
	<div class="container"><div class="jumbotron"><div class="tab-content"><div class="col-md-4 text-center"><img class="img-thumbnail profil" src="<?php echo $profil_picture ?>"></div><div class="col-md-8"><div><h2><?php echo $profil["multitextvalidationform2072020_1015_01"] ?></h2></div></div><div id="container_cpt-info" class="container" style="width: 100%;"><div class=""><ul class="list-inline"><li style="padding: 2%"><ul class="address-text"><li><b>Né le: </b><?php echo $profil["multitextvalidationform2072020_1015_03"] ?></li></ul></li><li style="padding: 2%"><ul class="address-text"><li><b>Sexe: </b><?php echo $profil["multiRadioform2072020_1015_04"]["value"] ?></li></ul></li><li style="padding: 2%"><ul class="address-text"><li><b>TEL: </b><?php echo $profil["multitextvalidationform2072020_1015_05"] ?></li></ul></li><li style="padding: 2%"><ul class="address-text"><li><b>E-MAIL: </b> <a href="mailto:<?php echo $profil['multitextvalidationform2072020_1015_08'] ?>"><?php echo $profil["multitextvalidationform2072020_1015_08"] ?></a></li></ul></li></ul></div></div><div class="col-md-4" style="background-color: #fff;"><div id="langue" class="container" style="width: 100%; padding-top: 20px"></div><div id="container_divers" class="container" style="width: 100%;"><div style="border-bottom: 3px solid #088ea7"><h3 class="text-blue">Centre d’intérêt</h3></div><div class="container"><div class="skills-info"><div class="bar-grids"><div id="sports" class="about-left s19 li:before"></div></div><div class="bar-grids"><div id="loisirs" class="about-left"></div></div><div class="bar-grids"><div id="dvrs" class="about-left"></div></div><div class="clearfix"> </div></div></div></div></div><div class="col-md-8" style="background-color: white;"><div id="stg-exp" class="container"></div><br><br><div id="container_cpt-info"><div style="border-bottom: 3px solid #088ea7"><h3 class="text-blue">Compétences</h3></div><div id="cpt-info" class="clearfix" style="margin-top: 40px ;padding-top: 25px"></div></div><br><div id="container_form-dipl" style="background-color: white;padding-top: 20px"></div></div></div></div></div>`);
		$('#design3').css('background-color','#eee');$('#design4').css('background-color','#fff');$('#design2').css('background-color','#fff');$('#design1').css('background-color','#fff');
		design_elt();
		btnLangue();
		btnOthers();
		numDesign = "2";
	}

function design4(){
	$("#design").html(`
	<div class="container" style="background-color: whitesmoke"><div class="col-md-8" style="padding: 50px;"><div class="container-fluid" id="profil"><h1><?php echo $profil["multitextvalidationform2072020_1015_01"] ?></h1><div id="ecole"></div><ul class="list-inline"><li style="padding: 2%"><ul class="address-text"><li><b>Né le: </b><?php echo $profil["multitextvalidationform2072020_1015_03"] ?></li></ul></li><li style="padding: 2%"><ul class="address-text"><li><b>Sexe: </b><?php echo $profil["multiRadioform2072020_1015_04"]["value"] ?></li></ul></li><li style="padding: 2%"><ul class="address-text"><li><b>TEL: </b><?php echo $profil["multitextvalidationform2072020_1015_05"] ?></li></ul></li><li style="padding: 2%"><ul class="address-text"><li><b>E-MAIL: </b> <a href="mailto:<?php echo $profil['multitextvalidationform2072020_1015_08'] ?>"><?php echo $profil["multitextvalidationform2072020_1015_08"] ?></a></li></ul></li></ul></div></div><div class="col-md-4 bg-turq"><div class="text-center" style="padding: 30px;"><img class="img-thumbnail profil" src="<?php echo $profil_picture ?>"></div></div><div class="col-md-8" style="background-color: whitesmoke"><div class="" style="padding-top: 20px;"><div id="experience" style="border-bottom: 3px solid #088ea7;background-color: white; padding: 15px" ></div><div><ul id="stg-exp"></ul></div></div><br><br><div id="container_form-dipl"></div></div><div class="col-md-4 bg-turq" style="padding: 30px; height:100%"><div id="container_cpt-info"><div class="text-center" style="border-bottom: 3px solid #088ea7;"><h3 class="text-blue">Compétences</h3></div><div style="margin-top: 10px" id="cpt-info" class="clearfix"></div></div><br><div id="langue" class="container" style="width: 100%; padding-top: 20px"></div><div id="container_divers" class=""><div class="text-center" style="border-bottom: 3px solid #088ea7"><h3 class="text-blue">Centre d’intérêt</h3></div><div class=""><div class="skills-info"><div class="bar-grids"><div id="sports" class="about-left s19 li:before"></div></div><div class="bar-grids"><div id="loisirs" class="about-left"></div></div><div class="bar-grids"><div id="dvrs" class="about-left"></div></div><div class="clearfix"> </div></div></div></div></div></div>`);
		$('#design4').css('background-color','#eee');$('#design3').css('background-color','#fff');$('#design2').css('background-color','#fff');$('#design1').css('background-color','#fff');
		design_elt();
		btnStages();
		btnLangue();
		btnOthers();
		numDesign = "3";
}

function btnFormation(){
	var formations = <?php echo json_encode($formations, JSON_PRETTY_PRINT) ?>;
	if (formations == 'undefined') {
			$("#container_form-dipl").hide();
	}else{
		var elements_formation = `
		<div class="page-header text-center"><h3>Formations et Diplomes</h3></div><div class="container"><ul class="timeline">`
		for (var i = 0; i <formations.length; i++) {
			elements_formation +=`
			<li  class="timeline-inverted">
			<div class="timeline-badge">
			<a><i class="fa fa-graduation-cap" id="" style="color: #3c948b"></i></a>
			</div>
			<div class="timeline-panel">
			<div class="timeline-heading">
			<h4>`+formations[i].liste_row1+` - `+formations[i].liste_row2 +` </h4>
			</div>
			<div class="timeline-body">
			<p>` +formations[i].liste_row+ `.</p>
			</div>
			</div>
			</li>`
			;
		}
		$("#container_form-dipl").html(elements_formation + '<li class="clearfix" style="float: none;"></li></ul></div>');
	}
}

function btnStages(){
	var stages = <?php echo json_encode($stages, JSON_PRETTY_PRINT) ?>;
	if (stages == 'undefined') {
		$("#stg-exp").hide();
	}else{
		var elements_stage = `
		<div class="page-header text-center"><h3>Expériences professionnels</h3></div><div><ul class="timeline">`
		for (var i = 0; i <stages.length; i++) {
			elements_stage +=`
			<li  class="timeline-inverted">
			<div class="timeline-badge">
			<a><i class="fa fa-archive" id="" style="color: #3c948b"></i></a>
			</div>
			<div class="timeline-panel">
			<div class="timeline-heading">
			<h4>`+stages[i].debut+` - `+stages[i].fin +` </h4>
			</div>
			<div class="timeline-body">
			<p>` +stages[i].quoi+ `</p>
			<p>` +stages[i].theme+ `</p>
			<p><u>Technologie</u>: ` +stages[i].technologie+ `</p>
			</div>
			</div>
			</li>`;
		}
		$("#stg-exp").html(elements_stage + '<li class="clearfix" style="float: none;"></li></ul></div>');
	}
}

function btnCompetence(){
	var skills = <?php echo json_encode($skills, JSON_PRETTY_PRINT) ?>;
	if (skills == 'undefined') {
		$("#cpt-info").hide();
	}else{
		var elements_skills = `
		<div class="page-header text-center"><h3>Compétences</h3></div><div class="container">`
		for (var i = 0; i <skills.length; i++) {
			elements_skills +=`
			<div class="skills-info">
			<div class="col-md-6 bar-grids">
			<b><p>`+skills[i].typeCompetence+`</p></b>
			<p>`+skills[i].nomCompetence+`</p>
			<div class="progress">
			<div class="progress-bar text-dark active" style="width: `+skills[i].pourcentage+`% ;background-color: #9fbd38;">
			<span style="color: black">`+skills[i].pourcentage+`%</span>
			</div>
			</div><hr>
			</div>`;
		}
		$("#cpt-info").html(elements_skills + "</div></div>");
	}
}

function btnLangue(){
	var langue = <?php echo json_encode($langues, JSON_PRETTY_PRINT) ?>;
	if (langue == 'undefined') {
		$("#langue").hide();
	}else{
		var elements_langue = `<div class="page-header text-center">
				<h3>Langue(s)</h3>
			</div>
			<div>`
		for (var i = 0; i <langue.length; i++) {
			elements_langue +=`
			<div class="bar-grids">
			<div class="about-left">
			<p style="line-height:1.5em"><b>`+langue[i].langue+`</b></p>
			<p style="line-height: 1.5em;"><i class="fa fa-arrow-circle-right text-turq"></i> `+langue[i].description+`.</p><hr>
			</div>
			</div>`;
		}
		$("#langue").html(elements_langue + "</div>");
	}
}

function btnOthers(){
	var loisirs = <?php echo json_encode($loisirs, JSON_PRETTY_PRINT) ?>;
	var sports = <?php echo json_encode($sports, JSON_PRETTY_PRINT) ?>;
	var divers = <?php echo json_encode($divers, JSON_PRETTY_PRINT) ?>;
	$("#container_divers").hide();
	if (loisirs == 'undefined') {
		$("#loisirs").html("<h3></h3>");
	}else{
		var elements_loisirs = '<h3><i class="fa fa-gamepad"> </i> Loisir(s)</h3>\
			<div style="padding-left: 25px">'
		for (var i = 0; i <loisirs.length; i++) {
			elements_loisirs +='<p style="line-height: 1.5em;"><i class="fa fa-arrow-circle-right text-turq"> </i> <i>'+loisirs[i].loisir+'</i></p>';
		}
		$("#loisirs").html(elements_loisirs + "</div>");
		$("#container_divers").show();
	}

	if (sports == 'undefined') {
		$("#sports").html("");
	}else{
		var elements_sports = '<h3><i class="fa fa-futbol-o"> </i> Sport(s)</h3>\
			<div style="padding-left: 25px">'
		for (var i = 0; i <sports.length; i++) {
			elements_sports +='<p style="line-height: 1.5em;"><i class="fa fa-arrow-circle-right text-turq"> </i> <i>'+sports[i].sport+'</i></p>';
		}
		$("#sports").html(elements_sports +"</div>");
		$("#container_divers").show();
	}
	if (divers == 'undefined') {
		$("#dvrs").html("");
	}else{
		var elements_divers = '<h3><i class="fa fa-puzzle-piece"> </i> Divers</h3>\
			<div style="padding-left: 25px">'
		for (var i = 0; i <divers.length; i++) {
			elements_divers +='<p style="line-height: 1.5em;"><i class="fa fa-arrow-circle-right text-turq"> </i> <i>'+divers[i].divers+'</i></p>';
		}
		$("#dvrs").html(elements_divers + "</div><br>");
		$("#container_divers").show();
	}
}
function design_elt(){
	var stages = <?php echo json_encode($stages, JSON_PRETTY_PRINT) ?>;
	if (stages == 'undefined') {
		$("#stg-exp").hide();
		$("#container_cpt-info").hide();
	}else{
		var elements_stage = `
		<div style="border-bottom: 3px solid #088ea7"><h3 class="text-blue">Expériences professionnels</h3></div><div class=""><div>`
		for (var i = 0; i <stages.length; i++) {
			elements_stage +=`
			<div class="col-md-8">
				<div class="col-md-4" style="padding-top:15px">
					<h5>`+stages[i].debut+` - `+stages[i].fin +`</h5> 
				</div>
                <div class="col-md-8">
                <h5>` +stages[i].quoi+ `</h5>
                    <span>` +stages[i].technologie+ `</span>
                    <span style="line-height:0px;">` +stages[i].theme+ `</span>
                </div>
            </div></br>`;
		}
		$("#stg-exp").html(elements_stage+`	</div>
					</div>
				</div>`);
	}

	var formations = <?php echo json_encode($formations, JSON_PRETTY_PRINT) ?>;
	if (formations == 'undefined') {
		$("#container_form-dipl").hide();
	}else{
		var elements_formation = `<div class="bg-white" style="border-bottom: 3px solid #088ea7;padding:12px" >
		<h3 class="text-blue">Formations et Diplomes</h3></div><div><div>`
		for (var i = 0; i <formations.length; i++) {
			elements_formation +=`
			<div class="col-md-4" style="padding-top:15px">
			<h3>`+formations[i].liste_row1+' - '+formations[i].liste_row2 +`</h3>
			</div>
            <div class="col-md-8" style="padding-top:30px">
			<p>`+formations[i].liste_row+`</p>
			</div>
			`;
		}
		$("#container_form-dipl").html(elements_formation+`</div></div>`);
	}


		var skills = <?php echo json_encode($skills, JSON_PRETTY_PRINT) ?>;
	if (skills == 'undefined') {
		$("#cpt-info").html("<p>Vide!</p>");
	}else{
		var elements_skills = "<div>"
		for (var i = 0; i <skills.length; i++) {
			elements_skills +=`
			<div class="col-xs-4">
			<div class="c100 p`+skills[i].pourcentage+` small dark green">
			<span aling="center">`+skills[i].pourcentage+`% </span>
			<div class="slice">
			<div class="bar"></div>
			<div class="fill"></div>
			</div>
			</div>
			<div>
			<b><p style="font-size:14px;line-height:10px">`+skills[i].typeCompetence+`</p></b>
			<p style="font-size:14px;line-height:10px">`+skills[i].nomCompetence+`</p>
			</div>
			</div>`;
		}
		$("#cpt-info").html(elements_skills + "</div>");
	}
}


$(document).ready(function(){
	function stickNavbar(){
		$('#nav').affix({
			offset: {
				top: $('header').height()
			}
		});	
	}
	stickNavbar();
	setTitle('CV de <?php echo $profil["multitextvalidationform2072020_1015_01"] ?>');
	switch (<?php echo $design; ?>) {
		case 3:
		design4();
		break;
		case 2:
		design3();
		break;
		case 1:
		design2();
		break;
		default:
		design1();
	}

	$("#design1").click(function(){
		design1();
	});
	$("#design2").click(function(){
		design2();
	});
	$("#design3").click(function(){
		design3();
	});
	$("#design4").click(function(){
		design4();
	});

	$("#btnSave").click(function(){
		$("body").find("#dropdown-settings").hide();
		tplCtx = {};
		tplCtx.id = "<?php echo (string)$myresume['_id'] ?>";
		tplCtx.path = "design";
		tplCtx.collection = "<?php echo Form::ANSWER_COLLECTION ?>"; 
		tplCtx.value = numDesign;
		dataHelper.path2Value( tplCtx, function(params) {
			//urlCtrl.loadByHash(location.hash);
			//location.reload();
			toastr.success("Changement enregistré!");
		});

	});

})
</script>