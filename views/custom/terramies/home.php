


    <style type="text/css">
      @import url("https://fonts.googleapis.com/css2?family=Ubuntu:wght@400;500;600;700&display=swap");

html,
body,
div,
span,
applet,
object,
iframe,
h1,
h2,
h3,
h4,
h5,
h6,
p,
blockquote,
pre,
a,
abbr,
acronym,
address,
big,
cite,
code,
del,
dfn,
em,
font,
ins,
kbd,
q,
s,
samp,
small,
strike,
strong,
sub,
sup,
tt,
var,
dl,
dt,
dd,
ol,
ul,
li,
fieldset,
form,
label,
legend,
table,
caption,
tbody,
tfoot,
thead,
tr,
th,
td {
    margin: 0;
    padding: 0;
    border: 0;
    font-family: inherit;
    font-size: 100%;
    font-style: inherit;
    font-weight: inherit;
    vertical-align: baseline;
    outline: 0;
}

/*html {
    overflow-y: scroll;
    font-size: 62.5%;
    -webkit-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%;
}*/

body {
    background: #fff;
}

article,
aside,
details,
figcaption,
figure,
footer,
header,
hgroup,
nav,
section {
    display: block;
}

ol,
ul {
    list-style: none;
}

table {
    border-collapse: separate;
    border-spacing: 0;
}

caption,
th,
td {
    text-align: left;
    font-weight: normal;
}

blockquote:before,
blockquote:after,
q:before,
q:after {
    content: "";
}

blockquote,
q {
    quotes: """";
}

a:focus {
    outline: none;
}

a:hover,
a:active {
    outline: 0;
}

a img {
    border: 0;
}

body,
select,
input,
textarea {
    color: #666;
}

::-moz-selection {
    text-shadow: none;
    color: #fff;
    background: #f4a82c;
}

::selection {
    text-shadow: none;
    color: #fff;
    background: #f4a82c;
}

a:link {
    -webkit-tap-highlight-color: rgba(255,255,255,0);
}

ins {
    text-decoration: none;
    color: #000;
    background-color: #fcd700;
}

mark {
    font-style: italic;
    font-weight: bold;
    color: #000;
    background-color: #fcd700;
}

input:-moz-placeholder {
    color: #a9a9a9;
}

textarea:-moz-placeholder {
    color: #a9a9a9;
}

a {
    text-decoration: none;
}

* {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

body {
    font-family: "Ubuntu",sans-serif !important;
    font-size: 12px;
    font-weight: 400;
    color: #144894;
}

@media screen and (min-width:1280px) {
    body {
        font-size: 16px;
    }
}

h1 {
    margin-bottom: 16px;
    text-align: center;
    font-family: "Ubuntu",sans-serif;
    font-size: 2.5em;
    font-weight: 700;
    line-height: 1.33;
    letter-spacing: 1.22px;
}

h1 span {
    color: #f3d21a;
}

@media screen and (min-width:1280px) {
    h1 {
        font-size: 2.625em;
        line-height: 0.95;
        letter-spacing: 1.7px;
    }
}

h2 {
    position: relative;
    margin-bottom: 24px;
    padding-bottom: 15px;
    text-align: center;
    font-family: "Ubuntu",sans-serif;
    font-size: 1.6666666666666667em;
    font-weight: 700;
    letter-spacing: 0.24px;
}

h2:before {
    content: "";
    display: block;
    position: absolute;
    bottom: 0;
    left: 50%;
    width: 22px;
    height: 5px;
    background: #41d4c3;
    -webkit-transform: translateX(-50%);
    transform: translateX(-50%);
}

@media screen and (min-width:768px) {
    h2 {
        text-align: left;
    }

    h2:before {
        content: "";
        left: 0;
        transform: none;
    }
}

@media screen and (min-width:1280px) {
    h2 {
        padding-bottom: 22px;
        font-size: 2em;
        letter-spacing: 0.38px;
    }

    h2:before {
        width: 46px;
        height: 7px;
    }
}

h3 {
    margin-bottom: 7px;
    font-family: "Ubuntu",sans-serif;
    font-size: 1.333333333333333em;
    font-weight: 700;
    letter-spacing: 0.19px;
    color: #41d4c3;
}

@media screen and (min-width:1280px) {
    h3 {
        font-size: 1.5em;
        letter-spacing: 0.29px;
    }
}

a {
    text-decoration: none;
    color: #144894;
}

p {
    margin-bottom: 10px;
    line-height: 1.67;
    letter-spacing: 0.14px;
}

strong {
    font-weight: 700;
}

ul {
    margin-bottom: 10px;
}

ul li {
    margin-bottom: 5px;
}

img {
    max-width: 100%;
}

.o-wrapper {
    padding-right: 15px;
    padding-left: 15px;
}

@media screen and (min-width:768px) {
    .o-wrapper {
        padding-right: 6vw;
        padding-left: 6vw;
    }
}

@media screen and (min-width:1024px) {
    .o-wrapper {
        padding-right: 10vw;
        padding-left: 10vw;
    }
}

@media screen and (min-width:1280px) {
    .o-wrapper {
        padding-right: 6vw;
        padding-left: 6vw;
    }
}

@media screen and (min-width:1440px) {
    .o-wrapper {
        padding-right: calc(50vw - 600px);
        padding-left: calc(50vw - 600px);
    }
}

/*.o-landing {
    margin-top: 70px;
}
*/
.o-boxer {
    display: grid;
}

@media screen and (min-width:768px) {
    .o-boxer--col2 {
        grid-template-columns: 1fr 1fr;
    }
}

@media screen and (min-width:768px) {
    .o-mob {
        display: none !important;
    }
}

.o-tab {
    display: none !important;
}

@media screen and (min-width:768px) {
    .o-tab {
        display: block !important;
    }
}

.c-linklist {
    margin: 15px 0;
}

.c-linklist--item {
    display: block;
    position: relative;
    padding: 20px 0;
    padding-right: 23px;
    min-height: 70px;
    border-top: 1px solid #144894;
}

.c-linklist--item:after {
    content: "";
    display: block;
    position: absolute;
    top: 50%;
    right: 0;
    width: 19px;
    height: 19px;
    background: url('<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/icons/fleche-droite-blue.svg');
    background-size: contain;
    -webkit-transform: translateY(-50%);
    transform: translateY(-50%);
}

.c-linklist--item:last-of-type {
    border-bottom: 1px solid #144894;
}

.c-findmenu {
    display: grid;
    margin: auto;
    padding-top: 10px;
    max-width: 290px;
    text-align: center;
    font-weight: 700;
    background: #ff5673;
    grid-template-columns: 1fr 1fr;
}

@media screen and (min-width:768px) {
    .c-findmenu {
        padding-top: 0;
        max-width: 620px;
        grid-template-columns: 1fr 1fr 1fr 1fr;
    }
}

@media screen and (min-width:1280px) {
    .c-findmenu {
        height: 250px;
        max-width: 1000px;
    }
}

.c-findmenu--item {
    position: relative;
    width: 145px;
    height: 145px;
    padding-top: 108px;
    font-size: 1.166666666666667em;
    font-weight: 500;
    line-height: 1.14;
    letter-spacing: 0.17px;
    color: white;
}

.c-findmenu--item:last-of-type {
    padding-top: 100px;
    background: #144894;
}

@media screen and (min-width:768px) {
    .c-findmenu--item {
        width: 155px;
        height: 155px;
        padding-top: 114px;
    }

    .c-findmenu--item:last-of-type {
        padding-top: 106px;
    }
}

@media screen and (min-width:1280px) {
    .c-findmenu--item {
        width: 250px;
        height: 250px;
        padding-top: 174px;
        font-size: 1.125em;
        letter-spacing: 0.22px;
    }

    .c-findmenu--item:last-of-type {
        padding-top: 166px;
    }
}

.c-findmenu--item:before {
    content: "";
    display: block;
    position: absolute;
    top: 13px;
    left: 50%;
    width: 80px;
    height: 80px;
    background-size: contain;
    -webkit-transform: translateX(-50%);
    transform: translateX(-50%);
}

.c-findmenu--item:nth-child(1):before {
    background-image: url('<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/icons/entreprises.svg');
}

.c-findmenu--item:nth-child(2):before {
    background-image: url('<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/icons/activit-s.svg');
}

.c-findmenu--item:nth-child(3):before {
    background-image: url('<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/icons/medical.svg');
}

.c-findmenu--item:last-of-type:before {
    background-image: url('<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/icons/events.svg');
}

@media screen and (min-width:768px) {
    .c-findmenu--item:before {
        top: 25px;
    }
}

@media screen and (min-width:1280px) {
    .c-findmenu--item:before {
        top: 45px;
        width: 120px;
        height: 120px;
        background-size: 120px 120px;
    }
}

@media screen and (min-width:1024px) {
    .c-findmenu--item:after {
        content: '';
        display: block;
        position: absolute;
        top: 50%;
        left: 50%;
        width: 128px;
        height: 128px;
        border-radius: 50%;
        background-color: rgba(0,0,0,0.05);
        transition: transform 0.4s ease-in-out;
        transform: translate(-50%, -50%) scale(0);
    }

    .c-findmenu--item:hover:after {
        transform: translate(-50%, -50%) scale(1);
    }
}

@media screen and (min-width:1280px) {
    .c-findmenu--item:after {
        width: 178px;
        height: 178px;
    }
}

.c-soutenez {
    padding-top: 13px;
    padding-bottom: 54px;
    color: white;
    background: url('<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/backgrounds/soutenez-nous.jpg');
    background-position: center;
    background-size: cover;
}

@media screen and (min-width:768px) {
    .c-soutenez {
        padding-top: 20px;
    }
}

@media screen and (min-width:1280px) {
    .c-soutenez {
        padding-top: 24px;
        padding-bottom: 79px;
    }
}

.c-soutenez--title {
    position: relative;
    left: 50%;
    width: 90px;
    height: 90px;
    margin-bottom: 0;
    padding-top: 32px;
    border-radius: 50%;
    text-align: center;
    font-size: 1.166666666666667em;
    font-weight: 500;
    line-height: 1.14;
    letter-spacing: 0.17px;
    color: white;
    background: #ff5673;
    -webkit-transform: translateX(-50%);
    transform: translateX(-50%);
}

@media screen and (min-width:1280px) {
    .c-soutenez--title {
        width: 152px;
        height: 152px;
        padding-top: 51px;
        font-size: 1.5625em;
        font-weight: 400;
        letter-spacing: 0.3px;
    }
}

.c-soutenez--text {
    width: 290px;
    margin: 32px auto 28px auto;
    padding: 0 20px;
    text-align: center;
}

@media screen and (min-width:768px) {
    .c-soutenez--text {
        width: 331px;
        margin: 17px auto 28px auto;
    }
}

@media screen and (min-width:1280px) {
    .c-soutenez--text {
        width: 558px;
        margin: 22px auto 50px auto;
        font-size: 0.9em;
        line-height: 1.75;
        letter-spacing: 0.24px;
    }
}

.c-soutenez .c-button {
    display: block;
}

.c-soutenez .c-button--alt {
    margin-bottom: 18px;
}

@media screen and (min-width:768px) {
    .c-soutenez .c-button--alt {
        margin-bottom: 0;
    }
}

@media screen and (min-width:768px) {
    .c-soutenez .o-boxer {
        width: 448px;
        margin: auto;
        grid-gap: 20px;
    }
}

@media screen and (min-width:1280px) {
    .c-soutenez .o-boxer {
        width: 512px;
    }
}

.c-soutenez {
    position: relative;
}

.c-soutenez:before,
.c-soutenez:after {
    content: '';
    display: block;
    position: absolute;
    top: calc(0% - 15px);
    left: calc(50% - 80px - 45px - 20px);
    width: 80px;
    height: 140px;
    background: url('<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/backgrounds/courbe-1-yellow.svg') no-repeat;
    background-position: 0% 0%;
    background-size: 100%;
}

@media screen and (min-width:1280px) {
    .c-soutenez:before,
    .c-soutenez:after {
        top: calc(0% - 48px);
        left: calc(50% - 308px - (558px / 2) );
        width: 308px;
        height: 540px;
    }
}

.c-soutenez:after {
    left: calc(50% + 45px + 20px);
    background-image: url('<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/backgrounds/courbe-1-purple.svg');
}

@media screen and (min-width:1280px) {
    .c-soutenez:after {
        left: calc(50% + (558px / 2) );
    }
}

.c-datakey {
    width: 100%;
    height: 75px;
    margin-bottom: 0;
    padding-top: 28px;
    overflow: hidden;
    white-space: nowrap;
    background: #f3d21a url('<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/backgrounds/bg-icons.svg') no-repeat center left;
    background-size: contain;
}

@media screen and (min-width:768px) {
    .c-datakey {
        height: 80px;
    }
}

@media screen and (min-width:1280px) {
    .c-datakey {
        height: 167px;
        padding-top: 57px;
    }
}

.c-datakey--item {
    display: inline-block;
    padding-left: 45px;
    font-size: 1.33333333333em;
    font-weight: 700;
    letter-spacing: 0.2256px;
}

.c-datakey--item:first-child {
    position: relative;
}

.c-datakey--item:first-child:before,
.c-datakey--item:first-child:after {
    content: '';
    display: inline-block;
    position: absolute;
    top: calc(50% - 2px);
    right: -37px;
    width: 26px;
    height: 7px;
    border-radius: 3px;
    background: #144894;
}

@media screen and (min-width:1280px) {
    .c-datakey--item:first-child:before,
    .c-datakey--item:first-child:after {
        top: calc(50% - 8px);
        right: -117px;
        width: 71px;
        height: 18px;
        border-radius: 9px;
    }
}

.c-datakey--item:first-child:after {
    right: -17px;
    width: 7px;
    background: #ff5673;
}

@media screen and (min-width:1280px) {
    .c-datakey--item:first-child:after {
        right: -63px;
        width: 18px;
        height: 18px;
        border-radius: 9px;
    }
}

@media screen and (min-width:768px) {
    .c-datakey--item {
        font-size: 1.583333333333em;
        letter-spacing: 0.19px;
    }
}

@media screen and (min-width:1280px) {
    .c-datakey--item {
        padding-left: 155px;
        font-size: 2.5em;
        letter-spacing: 0.48px;
    }
}

.c-datakey--item span {
    color: #ff5673;
}

.c-datakey {
    display: flex;
}

.c-datakey--list {
    animation: title_loop 15s linear infinite;
}

@keyframes title_loop {
    from {
        transform: translate3d(0,0,0);
    }

    to {
        transform: translate3d(-100%,0,0);
    }
}

.c-button {
    display: inline-block;
    position: relative;
    left: 50%;
    width: 214px;
    height: 50px;
    padding: 17px 41px 0 12px;
    border-radius: 25px;
    text-align: center;
    font-size: 1.166666666667em;
    font-weight: 700;
    line-height: 1.14;
    letter-spacing: 0.17px;
    background: #f5f9fc;
    -webkit-box-shadow: -1px 6px 20px -6px rgba(0,0,0,0.15);
    -moz-box-shadow: -1px 6px 20px -6px rgba(0,0,0,0.15);
    box-shadow: -1px 6px 20px -6px rgba(0,0,0,0.15);
    -webkit-transform: translateX(-50%);
    transform: translateX(-50%);
}

@media screen and (min-width:768px) {
    .c-button {
        left: initial;
        transform: none;
    }
}

@media screen and (min-width:1280px) {
    .c-button {
        width: 246px;
        height: 60px;
        padding: 16px 41px 0 12px;
        border-radius: 30px;
        font-size: 1em;
        line-height: 1.75;
        letter-spacing: 0.19px;
        transform: none;
    }
}

.c-button:after {
    content: "";
    display: block;
    position: absolute;
    top: 15px;
    right: 23px;
    width: 19px;
    height: 19px;
    background: url('<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/icons/fleche-droite-blue.svg');
    background-size: contain;
}

@media screen and (min-width:1280px) {
    .c-button:after {
        top: 21.5px;
    }
}

.c-button:hover {
    text-decoration: none;
    color: white;
    background: #dd328d;
}

.c-button:hover:after {
    background: url('<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/icons/fleche-droite-white.svg');
}

.c-button--alt {
    font-weight: 500;
    color: white;
    background: #ff5673;
    box-shadow: none;
}

.c-button--alt:after {
    background: url('<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/icons/fleche-droite-white.svg');
}

.c-button--alt2 {
    font-weight: 500;
    color: white;
    background: #f3d21a;
    box-shadow: none;
}

.c-button--alt2:after {
    background: url('<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/icons/fleche-droite-white.svg');
}

.c-header #mainNav{
    z-index: 2;
    position: fixed;
    top: 0;
    width: 100%;
    height: 70px;
    background: white;
}

.c-header--burger {
    display: block;
    position: absolute;
    width: 20px;
    height: 19px;
    margin: 16px 15px 15px 15px;
    border: none;
    outline: none;
    background: url('<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/burger.svg');
}

@media screen and (min-width:768px) {
    .c-header--burger {
        display: none;
    }
}

.c-header--logo .logo-menutop{
    display: block;
    width: 188px;
    margin: 11px auto;
}

@media screen and (min-width:768px) {
    .c-header--logo .logo-menutop{
        position: absolute;
        top: 4px;
        left: 35px;
    }
}

.c-landing img {
    display: block;
    width: 290px;
    margin: -35px auto 0 auto;
}

@media screen and (min-width:768px) {
    .c-landing img {
        width: auto;
        margin: 0 auto 0 auto;
        margin: -2.5vw auto 0 auto;
        text-align: left;
    }
}

.c-landing--text {
    width: 290px;
    margin: 0 auto 30px auto;
    text-align: center;
    font-size: 1.166666666666667em;
    line-height: 1.71;
    letter-spacing: 0.17px;
}

@media screen and (min-width:768px) {
    .c-landing--text {
        width: 100%;
        margin: 0 0 20px 0;
        max-width: 294px;
        text-align: left;
    }
}

@media screen and (min-width:1280px) {
    .c-landing--text {
        margin: 0 0 40px 0;
        max-width: 386px;
        font-size: 1.25em;
        line-height: 1.75;
        letter-spacing: 0.24px;
    }
}

.c-landing--title {
    margin-top: 30px;
}

@media screen and (min-width:768px) {
    .c-landing--title {
        margin-top: calc(4vw + 15px);
        text-align: left;
    }
}

@media screen and (min-width:1280px) {
    .c-landing--title {
        margin-top: calc(5.5vw + 15px);
        margin-bottom: 32px;
    }
}

@media screen and (min-width:1440px) {
    .c-landing--title {
        margin-top: 91px;
    }
}

.c-landing .c-button {
    margin-bottom: 62px;
}

@media screen and (min-width:768px) {
    .c-landing .o-boxer {
        grid-template-columns: 1fr 1fr;
    }

    .c-landing .o-boxer>div:nth-child(1) {
        grid-column: 2;
    }

    .c-landing .o-boxer>div:nth-child(2) {
        grid-row: 1;
    }
}

.c-landing--image {
    display: block;
    position: relative;
}

.c-landing--image:after {
    content: '';
    display: block;
    z-index: -1;
    position: absolute;
    top: 100%;
    left: calc(50% - 145px);
    width: 290px;
    height: 120%;
    background: url('<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/backgrounds/courbe-1.svg') no-repeat;
    background-position: 100% 30%;
    background-size: 100%;
}

@media screen and (min-width:768px) {
    .c-landing--image:after {
        top: 0;
        left: calc(-100% - 4.2vw);
        width: 100%;
        background-position: 100% 80%;
        background-size: 77%;
    }
}

@media screen and (min-width:1440px) {
    .c-landing--image:after {
        left: calc(-100% - 60px);
    }
}

.c-jeune {
    margin-bottom: 52px;
}

@media screen and (min-width:768px) {
    .c-jeune {
        position: relative;
        margin-top: -14vw;
    }
}

@media screen and (min-width:1280px) {
    .c-jeune {
        margin-top: -18vw;
        margin-bottom: 80px;
    }
}

@media screen and (min-width:1440px) {
    .c-jeune {
        margin-top: -233px;
    }
}

    .c-jeune h2 {
        margin-bottom: 0;
    }

@media screen and (min-width:768px) {
    .c-jeune h2 {
        margin-bottom: -15px;
    }
}

@media screen and (min-width:1280px) {
.c-jeune h2 {
    margin-bottom: 0;
}
}

.c-jeune--title {
    position: relative;
    bottom: -25px;
    left: 50%;
    width: 192px;
    height: 36px;
    padding-top: 10.5px;
    border-radius: 18px;
    text-align: center;
    font-size: 12px;
    font-weight: 500;
    letter-spacing: 0.14px;
    color: white;
    background: #f3d21a;
    -webkit-transform: translateX(-50%);
    transform: translateX(-50%);
}

@media screen and (min-width:768px) {
    .c-jeune--title {
        left: calc(50% - 294px);
        transform: none;
    }
}

@media screen and (min-width:1280px) {
    .c-jeune--title {
        bottom: -36px;
        left: calc(50% - 470px);
        width: 284px;
        height: 60px;
        padding-top: 19.5px;
        border-radius: 30px;
        font-size: 1.125em;
        letter-spacing: 0.22px;
    }
}

.c-travail {
    margin-bottom: 81px;
}

@media screen and (min-width:1280px) {
    .c-travail {
        margin-bottom: 116px;
    }
}

.c-travail .c-linklist {
    width: 290px;
    margin: auto;
}

@media screen and (min-width:768px) {
    .c-travail .c-linklist {
        position: relative;
        left: 50%;
        width: 310px;
        margin: 0;
        margin-top: -87px;
    }
}

@media screen and (min-width:1280px) {
    .c-travail .c-linklist {
        width: 500px;
        margin-top: -124px;
    }
}

.c-travail .c-linklist--item {
    display: flex;
    height: 70px;
    padding: 20px 42px 0 80px;
}

.c-travail .c-linklist--item:before {
    content: "";
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    width: 70px;
    height: 70px;
    background-size: contain;
}

.c-travail .c-linklist--item > p {
    margin: auto 0;
    font-size: 1.166666666666667em;
    font-weight: 700;
    line-height: 1.14;
    letter-spacing: 0.17px;
}

.c-travail .c-linklist--item:nth-child(1):before {
    background-image: url('<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/icons/juridique.svg');
}

.c-travail .c-linklist--item:nth-child(2):before {
    background-image: url('<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/icons/link.svg');
}

.c-travail .c-linklist--item:nth-child(3):before {
    background-image: url('<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/icons/recherche.svg');
}

.c-travail .c-linklist--item:nth-child(4):before {
    background-image: url('<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/icons/workshop.svg');
}

@media screen and (min-width:1280px) {
    .c-travail .c-linklist--item {
        height: 98px;
        padding: 30px 42px 0 135px;
    }

    .c-travail .c-linklist--item:before {
        top: 3px;
        width: 90px;
        height: 90px;
        background-size: 90px 90px;
    }
}

.c-travail h2 {
    margin-bottom: 31px;
}

.c-travail {
    position: relative;
}

.c-travail:before {
    content: '';
    display: block;
    z-index: -1;
    position: absolute;
    top: 20px;
    left: calc(50% - 145px);
    width: 290px;
    height: 100%;
    background: url('<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/backgrounds/courbe-2.svg') no-repeat;
    background-position: 100% 30%;
    background-size: 100%;
}

@media screen and (min-width:768px) {
    .c-travail:before {
        top: -4.2vw;
        left: calc(6vw - 4.2vw);
        width: calc(50% - 6vw);
        height: 481px;
        background-position: 100% 0%;
        background-size: 77%;
    }
}

@media screen and (min-width:1024px) {
    .c-travail:before {
        top: -2.1vw;
        left: calc(10vw - 4.2vw);
        width: calc(50% - 10vw);
    }
}

@media screen and (min-width:1280px) {
    .c-travail:before {
        top: -4.2vw;
        left: calc(6vw - 4.2vw);
        width: calc(50% - 6vw);
        height: calc(481px + (481px * 77/100) );
    }
}

@media screen and (min-width:1440px) {
    .c-travail:before {
        left: calc(50vw - 600px - 60px);
        width: calc(50% - (50vw - 600px) );
        height: calc(481px + (481px * 77/100) );
    }
}

.c-travail:after {
    content: '';
    display: block;
    z-index: -1;
    position: absolute;
    top: 40%;
    left: calc(50% - 145px);
    width: 290px;
    height: calc(481px + (481px * 77/100) );
    background: url('<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/backgrounds/courbe-3b.svg') no-repeat;
    background-position: 0% 30%;
    background-size: 100%;
}

@media screen and (min-width:768px) {
    .c-travail:after {
        top: -4.2vw;
        left: calc(50% + 4.2vw);
        width: calc(50% - 6vw);
        background-position: 0% 0%;
        background-size: 77%;
    }
}

@media screen and (min-width:1024px) {
    .c-travail:after {
        top: -2.1vw;
        width: calc(50% - 10vw);
    }
}

@media screen and (min-width:1280px) {
    .c-travail:after {
        top: -4.2vw;
        width: calc(50% - 6vw);
    }
}

@media screen and (min-width:1440px) {
    .c-travail:after {
        left: calc(50% + 60px);
        width: calc(50% - (50vw - 600px) );
    }
}

.c-actu-banner {
    margin-top: 80px;
    margin-bottom: 85px;
}

@media screen and (min-width:1280px) {
    .c-actu-banner {
        margin-top: 126px;
        margin-bottom: 169px;
    }
}

.c-actu-banner h2 {
    margin-bottom: 31px;
}

@media screen and (min-width:768px) {
    .c-actu-banner h2 {
        text-align: center;
    }

    .c-actu-banner h2:before {
        left: 50%;
        -webkit-transform: translateX(-50%);
        transform: translateX(-50%);
    }
}

.c-actu-banner--head {
    display: block;
    position: relative;
    width: 290px;
    height: 110px;
    margin: auto;
    background: #ff5673;
}

@media screen and (min-width:768px) {
    .c-actu-banner--head {
        width: 180px;
        height: 180px;
    }
}

@media screen and (min-width:1280px) {
    .c-actu-banner--head {
        width: 290px;
        height: 290px;
    }
}

.c-actu-banner--head:before {
    content: "";
    display: block;
    position: absolute;
    top: 25px;
    left: 50%;
    width: 67px;
    height: 56px;
    background: url('<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/icons/actu-banner.svg');
    background-size: contain;
    -webkit-transform: translateX(-50%);
    transform: translateX(-50%);
}

@media screen and (min-width:768px) {
    .c-actu-banner--head:before {
        top: 52px;
        width: 86.89px;
        height: 72.6px;
        background-size: 86.89px 72.6px;
    }
}

@media screen and (min-width:1280px) {
    .c-actu-banner--head:before {
        top: 74px;
        width: 151px;
        height: 126px;
        background-size: 151px 126px;
    }
}

.c-actu-banner--main {
    width: 290px;
    margin: auto;
    padding: 44px 0 48px 0;
    color: white;
    background: #144894;
}

@media screen and (min-width:768px) {
    .c-actu-banner--main {
        width: 320px;
        height: 180px;
        padding: 36px 0;
    }
}

@media screen and (min-width:1280px) {
    .c-actu-banner--main {
        width: 510px;
        height: 290px;
        padding: 75px 0;
    }
}

.c-actu-banner--main p {
    width: 193px;
    margin: 0 auto 25px auto;
    text-align: center;
    font-size: 1.33333333333em;
    font-weight: 500;
}

@media screen and (min-width:768px) {
    .c-actu-banner--main p {
        margin: 0 0 18px 33px;
        text-align: left;
        line-height: 1.3;
    }
}

@media screen and (min-width:1280px) {
    .c-actu-banner--main p {
        width: 312px;
        margin: 0 0 25px 62px;
        font-size: 1.625em;
        font-weight: 400;
        letter-spacing: 0.31px;
    }
}

.c-actu-banner--main .c-button {
    margin-bottom: 0;
}

@media screen and (min-width:768px) {
    .c-actu-banner--main .c-button {
        margin-left: 33px;
    }
}

@media screen and (min-width:1280px) {
    .c-actu-banner--main .c-button {
        margin-left: 62px;
    }
}

@media screen and (min-width:768px) {
    .c-actu-banner .o-boxer {
        width: 500px;
        margin: auto;
        grid-template-columns: 180px 320px;
    }
}

@media screen and (min-width:1280px) {
    .c-actu-banner .o-boxer {
        width: 800px;
        grid-template-columns: 290px 510px;
    }
}

.c-actu-banner {
    position: relative;
}

.c-actu-banner:before,
.c-actu-banner:after {
    content: '';
    display: block;
    z-index: -1;
    position: absolute;
    top: -16px;
    right: calc(50% - 125px);
    width: 96px;
    height: 56px;
    background: url('<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/backgrounds/bulle-1.svg') no-repeat top left;
    background-size: contain;
}

@media screen and (min-width:768px) {
    .c-actu-banner:before,
    .c-actu-banner:after {
        top: -62px;
        right: auto;
        left: calc(50% + 250px - 104px);
        width: 209px;
        height: 121px;
    }
}

@media screen and (min-width:1280px) {
    .c-actu-banner:before,
    .c-actu-banner:after {
        top: -50px;
        left: calc(50% + 400px - 104px);
    }
}

.c-actu-banner:after {
    top: -38px;
    left: calc(50% - 125px);
    width: 72px;
    height: 42px;
    transform: scaleX(-1);
}

@media screen and (min-width:768px) {
    .c-actu-banner:after {
        top: 30px;
        left: calc(50% - 250px - 142px - 27px);
        width: 142px;
        height: 82px;
    }
}

@media screen and (min-width:1280px) {
    .c-actu-banner:after {
        left: calc(50% - 400px - 142px - 27px);
    }
}

@media screen and (min-width:1280px) {
    .c-actu-banner--main {
        position: relative;
    }
}

.c-actu-banner--main:after {
    content: '';
    display: block;
    z-index: -1;
    position: absolute;
    bottom: -40px;
    left: calc(50% + 145px - 106px - 20px);
    width: 106px;
    height: 61px;
    background: url('<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/backgrounds/bulle-1.svg') no-repeat top left;
    background-size: contain;
}

@media screen and (min-width:768px) {
    .c-actu-banner--main:after {
        right: auto;
        bottom: -75px;
        left: calc(50% + 250px - 209px - 20px);
        width: 209px;
        height: 121px;
    }
}

@media screen and (min-width:1280px) {
    .c-actu-banner--main:after {
        right: 77px;
        left: auto;
    }
}

.c-actu-list {
    margin-bottom: 100px;
}

@media screen and (min-width:1280px) {
    .c-actu-list {
        margin-bottom: 126px;
    }
}

.c-actu-list h2 {
    margin-bottom: 43px;
}

.c-actu-list .c-linklist {
    width: 290px;
    margin: 0 auto 37px auto;
}

@media screen and (min-width:768px) {
    .c-actu-list .c-linklist {
        width: 100%;
        padding-top: 32px;
    }
}

@media screen and (min-width:1280px) {
    .c-actu-list .c-linklist {
        padding-top: 45px;
    }
}

.c-actu-list .c-linklist--item {
    padding: 26px 20px 20px 96px;
}

@media screen and (min-width:768px) {
    .c-actu-list .c-linklist--item {
        padding: 26px 90px 20px 96px;
    }
}

@media screen and (min-width:1280px) {
    .c-actu-list .c-linklist--item {
        height: 220px;
        padding: 49px 166px 49px 200px;
    }
}

.c-actu-list .c-linklist--item time {
    font-size: 0.75em;
    line-height: 2.8;
    letter-spacing: 0.11px;
    color: #57667b;
}

@media screen and (min-width:1280px) {
    .c-actu-list .c-linklist--item time {
        font-size: 0.6875em;
        letter-spacing: 0.13px;
    }
}

.c-actu-list .c-linklist--item:before {
    content: "";
    display: block;
    position: absolute;
    top: 29px;
    left: 0;
    width: 80px;
    height: 80px;
    background-image: url('<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/backgrounds/thumb.jpg');
    background-size: contain;
}

@media screen and (min-width:1280px) {
    .c-actu-list .c-linklist--item:before {
        top: 30px;
        width: 160px;
        height: 160px;
    }
}

.c-actu-list .c-linklist--item:after {
    top: initial;
    bottom: 23px;
    transform: none;
}

@media screen and (min-width:768px) {
    .c-actu-list .o-boxer {
        width: 100%;
        grid-template-columns: 136px 1fr;
        grid-gap: 10vw;
    }
}

@media screen and (min-width:1280px) {
    .c-actu-list .o-boxer {
        grid-template-columns: 222px 1fr;
        grid-gap: 90px;
    }
}

.c-accompagnement {
    position: relative;
    padding-top: 44px;
    background: #f5f9fc;
}

@media screen and (min-width:1280px) {
    .c-accompagnement {
        padding-top: 135px;
        padding-bottom: 50px;
    }
}

.c-accompagnement img.o-mob {
    display: block;
    width: 284px;
    margin: 0 auto 24px auto;
}

@media screen and (min-width:768px) {
    .c-accompagnement img.o-tab {
        position: absolute;
        top: -8px;
        right: 9vw;
        width: 263px;
    }
}

@media screen and (min-width:1280px) {
    .c-accompagnement img.o-tab {
        top: -65px;
        right: 9vw;
        width: 419px;
    }
}

.c-accompagnement h2 {
    margin-bottom: 35px;
}

.c-accompagnement h2 > span {
    display: block;
    margin-top: 5px;
    font-size: 0.6em;
    font-weight: 400;
}

@media screen and (min-width:768px) {
    .c-accompagnement h2 {
        padding-top: 32px;
    }
}

@media screen and (min-width:1280px) {
    .c-accompagnement h2 {
        margin-bottom: 58px;
        padding-top: 0;
    }
}

.c-accompagnement ul {
    width: 290px;
    margin: 0 auto;
    padding-bottom: 90px;
}

@media screen and (min-width:768px) {
    .c-accompagnement ul {
        width: auto;
        margin: 0;
    }
}

@media screen and (min-width:1280px) {
    .c-accompagnement ul {
        padding-left: 21px;
    }
}

.c-accompagnement ul li {
    position: relative;
    margin-bottom: 18px;
    padding-left: 41px;
    letter-spacing: 0.14px;
}

.c-accompagnement ul li:last-of-type {
    maargin-bottom: 0;
}

.c-accompagnement ul li:before {
    content: "";
    position: absolute;
    top: 2.5px;
    left: 10px;
    width: 7px;
    height: 7px;
    background-color: #ff5673;
    transform: rotate(45deg);
}

@media screen and (min-width:1280px) {
    .c-accompagnement ul li {
        margin-bottom: 30px;
        font-size: 1.125em;
        letter-spacing: 0.22px;
    }

    .c-accompagnement ul li:before {
        top: 5px;
        left: 12px;
        width: 10px;
        height: 10px;
    }
}

.c-accompagnement .o-boxer {
    position: relative;
}

@media screen and (min-width:768px) {
    .c-accompagnement .o-boxer>div:nth-child(1) {
        grid-column: 2;
    }

    .c-accompagnement .o-boxer>div:nth-child(2) {
        grid-column: 2;
    }

    .c-accompagnement .o-boxer>div:nth-child(3) {
        grid-row: 1;
    }
}

.c-accompagnement {
    position: relative;
}

.c-accompagnement + section {
    position: relative;
}

.c-accompagnement:before {
    content: '';
    display: block;
    position: absolute;
    bottom: calc(0% - 60px);
    left: calc(50% - 145px);
    width: 192px;
    height: 189px;
    background: url('<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/backgrounds/gear-1.svg') no-repeat;
    background-position: 0% 0%;
    background-size: 100%;
}

@media screen and (min-width:768px) {
    .c-accompagnement:before {
        bottom: calc(0% - 84px);
        left: 6vw;
        width: 249px;
        height: 249px;
    }
}

@media screen and (min-width:1024px) {
    .c-accompagnement:before {
        left: 10vw;
    }
}

@media screen and (min-width:1280px) {
    .c-accompagnement:before {
        bottom: calc(0% - 167px);
        left: 6vw;
        width: 449px;
        height: 449px;
    }
}

@media screen and (min-width:1440px) {
    .c-accompagnement:before {
        left: calc(50vw - 600px);
    }
}

@media screen and (min-width:1280px) {
    .c-terramies h2 {
        margin-bottom: 35px;
    }
}

.c-terramies img {
    display: block;
    width: 290px;
    margin: 0 auto 20px auto;
}

@media screen and (min-width:768px) {
    .c-terramies img {
        width: auto;
        margin: 0 0 20px 0;
    }
}

.c-terramies p {
    width: 290px;
    margin: auto;
}

@media screen and (min-width:768px) {
    .c-terramies p {
        width: 310px;
        margin: 0;
    }
}

@media screen and (min-width:1280px) {
    .c-terramies p {
        width: 500px;
        margin: 0;
        margin-bottom: 27px;
        padding: 0 7px;
        font-size: 1.125em;
        line-height: 1.94;
        letter-spacing: 0.22px;
    }
}

.c-terramies ul {
    display: grid;
    width: 290px;
    margin: auto;
    margin-bottom: 28px;
    grid-gap: 10px;
    grid-template-columns: 1fr 1fr 1fr;
}

.c-terramies ul li img {
    width: 100%;
}

@media screen and (min-width:768px) {
    .c-terramies ul {
        width: 310px;
        margin: 0 0 28px 0;
    }
}

@media screen and (min-width:1280px) {
    .c-terramies ul {
        width: 500px;
        margin: 0 0 65px 0;
    }
}

.c-terramies .o-boxer img.o-tab {
    z-index: 1;
    position: absolute;
    top: -53px;
    left: 24px;
    width: 250px;
}

@media screen and (min-width:1280px) {
    .c-terramies .o-boxer img.o-tab {
        top: -81px;
        left: 44px;
        width: 425px;
    }
}

.c-terramies .o-boxer {
    position: relative;
}

@media screen and (min-width:768px) {
    .c-terramies .o-boxer > div {
        grid-column: 2;
        grid-row: 1;
    }
}

.c-footer {
    padding-top: 25px;
    padding-bottom: 45px;
    background: #f5f9fc;
}

.c-footer--logo {
    display: block;
    width: 138px;
    margin: 0 auto 28px auto;
}

@media screen and (min-width:768px) {
    .c-footer--logo {
        margin: 0;
    }
}

.c-footer--title {
    display: block;
    margin-bottom: 13px;
    font-weight: 700;
    line-height: 1.5;
    letter-spacing: 0.14px;
}

.c-footer a {
    display: block;
    width: 200px;
    margin-right: auto;
    margin-left: auto;
}

.c-footer--address {
    display: block;
    margin-top: 20px;
    margin-bottom: 10px;
    line-height: 1.5;
    letter-spacing: 0.14px;
}

.c-footer--mail {
    line-height: 1.33;
    letter-spacing: 0.14px;
}

.c-footer--tel {
    margin-bottom: 25px;
    line-height: 1.33;
    letter-spacing: 0.14px;
}

@media screen and (min-width:768px) {
    .c-footer .o-boxer {
        grid-template-columns: 1fr 1fr 1fr;
    }
}

.c-navmenu {
    display: none;
}

@media screen and (min-width:1280px) {
    .c-navmenu {
        display: block;
        position: absolute;
        top: 13px;
        left: 254px;
    }
}

@media screen and (min-width:1280px) {
    .c-navmenu li {
        display: inline-block;
    }
}

@media screen and (min-width:1280px) {
    .c-navmenu li a {
        display: block;
        position: relative;
        height: 43px;
        padding: 14px 20px 0 20px;
        font-size: 0.9375em;
        font-weight: 700;
        line-height: 1.2;
        letter-spacing: 0.18px;
    }

    .c-navmenu li a:after {
        content: "";
        position: absolute;
        bottom: 5px;
        left: 20px;
        width: 0;
        height: 4px;
        background: #f3d21a;
        transition: width .2s ease-in-out;
    }

    .c-navmenu li a:hover:after {
        width: calc(100% - 40px);
    }
}

.u-cover {
    background-position: center;
    background-size: cover;
}

.u-clear {
    display: inline-block;
    clear: both;
}

.u-unlink {
    color: initial;
}

.u-unlink:hover {
    text-decoration: none;
}

.u-vertical-padding {
    padding-top: 50px;
    padding-bottom: 50px;
}
#bg-homepage{
    width: 100%;
}
#mainNav{
font-family: "Ubuntu",sans-serif !important;
}

#mainNav .menu-app-top .menu-button .fa{
    padding-right:5px;
}

#navbar button{
    color:#144894 !important;
}

#navbar button:hover{
    color:#f3d21a !important;
}

#show-bottom-add i:hover{
    color:#f3d21a !important;
}

.container-live .loader{
    display:none !important;
}

/*#filters-nav{
    top:100px;
}*/
    </style>
  
  <section class="o-wrapper o-landing c-landing">
        <div class="o-boxer">
          <span class="c-landing--image"><img src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/backgrounds/header-fond.jpg'/></span>
          <div>
            <h1 class="c-landing--title">Terrami<span>e</span>s</h1>
            <p class="c-landing--text">Trait d’union entre la société, les acteurs de la métropole Lyonnaise et les jeunes non accompagnés afin de les aider à poursuivre leur projet.</p>
            <!-- <a class="c-button lbh-menu-app" href="javascript:;" data-hash="#apropos">En savoir plus</a> -->
          </div>
        </div>
      </section>

      <section class="o-wrapper c-jeune">
        <h2>Je suis jeune et<br/> accompagné</h2>
        <h3 class="c-jeune--title">Trouver sur le territoire</h3>
        <div class="c-findmenu">
          <a href="javascript:;" class="c-findmenu--item lbh-menu-app" data-hash="#entreprise">Entreprises</a>
          <a href="javascript:;" class="c-findmenu--item lbh-menu-app" data-hash="#loisir">Sports et loisirs</a>
          <a href="javascript:;" class="c-findmenu--item lbh-menu-app" data-hash="#annuaireMedical">Annuaire médical</a>
          <a href="javascript:;" class="c-findmenu--item lbh-menu-app" data-hash="#agenda">Participer à un<br/>évènement</a>
        </div>
      </section>

      <section class="o-wrapper c-travail">
        <h2>Je suis travailleur<br/>social ou bénévole</h2>
         <a href="javascript:;" class="lbh-menu-app" data-hash="#ressourcesPro">
            <div class="c-linklist">
              <p class="c-linklist--item">Ressources juridiques</p>
              <p class="c-linklist--item"> Protocoles et procédures d’accompagnement</p>
              <p class="c-linklist--item">Études et recherches</p>
              <p class="c-linklist--item">Les ateliers Terramies</p>
            </div>
        </a>    
      </section>

      <section class="o-wrapper c-terramies">
        <div class="o-boxer o-boxer--col2">
          <img class="o-mob" src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/backgrounds/head-terramies.png'/>
          <img class="o-tab" src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/backgrounds/head-terramies2.png'/>
          <div>
            <h2>Terramies c'est ...</h2>
            <p>Terramies est un Groupement de Coopération Sociale et Médico-Sociale (GCSMS) fondé en 2019 par la Fondation AJD Maurice Gounon et les associations Prado Rhône Alpes et Acolea..</p>
            <ul>
              <li><a href="https://www.fondation-ajd.com/" target="_blank"><img src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/backgrounds/ajd.png'/></a></li>
              <li><a href="https://www.le-prado.fr/" target="_blank"><img src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/backgrounds/prado.png'/></a></li>
              <li><a href="https://www.slea.asso.fr/" target="_blank"><img src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/backgrounds/acolea.png'/></a></li>
            </ul>
          </div>
        </div>
      </section>

      <section class="o-wrapper c-accompagnement">
        <div class="o-boxer o-boxer--col2">
          <img class="o-mob" src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/backgrounds/head-accompagn.png'/>
          <img class="o-tab" src='<?php echo Yii::app()->getModule("costum")->assetsUrl; ?>/images/terramies/backgrounds/head-accompagn2.png'/>
          <div>
          <h2>Notre<br/>accompagnement<span>au quotidien 24h/24, 365j/an</span></h2>
            <ul>
              <li>Réussite scolaire : Permettre aux jeunes de s'inscrire dans un parcours de formation et de professionnalisation</li>
              <li>Prendre soin de son logement et de soi : vie quotidienne, cuisine, ménage...</li>
              <li>Clarifier les situations administratives et juridiques des jeunes</li>
              <li>S’inscrire dans un parcours de santé et de soins</li>
              <li>Découvrir la France et participer à des activités socioculturelles et sportives</li>
            </ul>
          </div>
        </div>
      </section>

      <section>
        <div class="c-datakey">
          <ul class="c-datakey--list">
            <li class="c-datakey--item"><span>500</span> jeunes accompagnés</li>
            <li class="c-datakey--item"><span>3</span> membres fondateurs (assos & fondations)</li>
          </ul>
        </div>
        <div class="c-soutenez o-wrapper">
          <h3 class="c-soutenez--title">Soutenez-nous !</h3>
          <p class="c-soutenez--text">Vous souhaitez vous investir auprès des jeunes, réaliser du soutien scolaire, les amener voir une pièce de théâtre ou un match de football, organiser un atelier cuisine... N'hésitez pas à  nous contacter.<br/><br/>
  Vous manquez de temps mais vous souhaitez nous soutenir ? Terramies accepte les dons financiers et matériels. </p>
          <div class="o-boxer o-boxer--col2">
            <a class="c-button c-button--alt lbh-menu-app" href="javascript:;" data-hash="#don">Faire un don</a>
            <a class="c-button c-button--alt2 lbh-menu-app" href="javascript:;" data-hash="#annonces">Devenir bénévole</a>
          </div>
        </div>

      </section>

      <section class="o-wrapper c-actu-banner">
        <h2>Actualités</h2>
        <div style="margin-left: 3vw;width: 80vw;/*margin-top: 4vw;*/background-color: white" id="newsstream" class="col-xs-10 col-sm-12">
          <div style="background-color: white;">
          </div>
        </div>
        <div class="o-boxer">
          <header class="c-actu-banner--head"></header>
          <main class="c-actu-banner--main">
            <p>Restez informé et suivez toutes nos actualités</p>
            <a class="c-button c-button--alt lbh-menu-app" href="javascript:;" data-hash="#live">Voir les actualités</a>
          </main>
        </div>
      </section>

      <!-- <section class="o-wrapper c-actu-list">
        <div class="o-boxer">
          <div>
            <h2>Nos dernières<br/>actualités</h2>
          </div>
          <div>
            <div class="c-linklist">
              <a href="" class="c-linklist--item">
                <h3>Titre de l’actualité</h3>
                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo…</p>
                <time>Le 12 Avril 2020</time>
              </a>
              <a href="" class="c-linklist--item">
                <h3>Titre de l’actualité</h3>
                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo…</p>
                <time>Le 12 Avril 2020</time>
              </a>
              <a href="" class="c-linklist--item">
                <h3>Titre de l’actualité</h3>
                <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo…</p>
                <time>Le 12 Avril 2020</time>
              </a>
            </div>
            <a class="c-button c-button--alt">Voir les actualités</a>
          </div>
        </div>
      </section> -->

<script type="text/javascript">
  jQuery(document).ready(function() {
    
    setTitle("Terramies");

    urlNews = "news/co/index/type/"+costum.contextType+"/id/"+costum.contextId+"/formCreate/false/nbCol/2/scroll/false";

    ajaxPost("#newsstream",baseUrl+"/"+urlNews,{search:true, indexStep:3, formCreate:false, scroll:false}, function(news){}, "html");

});
</script>