<?php if($canEdit && Authorisation::isInterfaceAdmin() ){ ?>
  <?php if ($subtype != "supercms") { ?>
    <div class="text-center btn-edit-delete-<?= @$kunik?> edit-inside-sp-block hiddenPreview" >
      <a class="tooltips edit<?php echo @$kunik?>Params" data-path="<?= @$path ?>" data-id="<?= @$id; ?>" data-collection="cms" type="button" data-toggle="tooltip" data-placement="top" title="" data-original-title="Modifier ce <?= @$name?>">
        <i class="fa fa-edit" aria-hidden="true"></i>
      </a>

      <a style="color: #f44336;" class="deleteLine tooltips" data-path="<?= @$path ?>" data-id="<?= @$id; ?>" data-collection="cms" type="button" data-toggle="tooltip" data-placement="top" title="" data-original-title="Supprimer ce <?= @$name?>">
        <i class="fa fa-trash" aria-hidden="true"></i>
      </a>
    </div>
  <?php } ?>
<?php }?>




