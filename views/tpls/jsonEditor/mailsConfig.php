<?php 
    $keyTpl = "mailsConfig";

    $paramsData = [
        "footerTpl" => "",
        "bannedTpl" => [],
    ];
    
    if( isset($this->costum[$keyTpl]) ) {
        foreach($paramsData as $i => $v) {
            if(isset($this->costum[$keyTpl][$i])) 
                $paramsData[$i] =  $this->costum[$keyTpl][$i];   
        }
    }
?>
<?php if($canEdit){ ?> 
    <a class='edit<?php echo $keyTpl ?>Params' href='javascript:;' 
        data-id='<?= $this->costum["contextId"]; ?>' 
        data-collection='<?= $this->costum["contextType"]; ?>' 
        data-key='<?php echo $keyTpl ?>' 
        data-path='costum'>
        <i class="fa fa-envelope-o" aria-hidden="true"></i> Configuration des mails
    </a>
<?php }?>


<script type="text/javascript">
jQuery(document).ready(function() {
    sectionDyf.<?php echo $keyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;

    sectionDyf.<?php echo $keyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "<?php echo Yii::t('cms', 'Mail configuration')?>",
            "icon" : "fa-cog",
            "properties" : {
                "footerTpl" : {
                    "inputType" : "text",
                    "label" : "<?php echo Yii::t('cms', 'Footer template')?>",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.footerTpl
                },
                "bannedTpl": {
                    "inputType" : "array",
                    "label" : "<?php echo Yii::t('cms', 'Banner Template')?>",
                    values :  sectionDyf.<?php echo $keyTpl ?>ParamsData.bannedTpl
                }
            },
            save : function (data) { 
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $keyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    if(val.inputType == "array"){
                        tplCtx.value[k] = getArray('.'+k+val.inputType);
                    }else{
                        tplCtx.value[k] = $("#"+k).val();
                    }
                });

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        toastr.success("<?php echo Yii::t('cms', 'Well added')?>");
                        location.reload();
                    } );
                }

            }
        }
    };

    $(".edit<?php echo $keyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path")+".<?php echo $keyTpl ?>";
        dyFObj.openForm( sectionDyf.<?php echo $keyTpl ?>Params,null, sectionDyf.<?php echo $keyTpl ?>ParamsData);
    });

});
</script>
