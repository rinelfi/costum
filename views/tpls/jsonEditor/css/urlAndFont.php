<?php 
    $keyTpl = "css";
    $subkeyTpl= $keyTpl."urlAndFont";

    $paramsData = [
        "urls" => [],
        "cssCode"  => "",
        "font" => ""
    ];
    if( isset($this->costum[$keyTpl]) ) {
        foreach($paramsData as $i => $v) {
            if(isset($this->costum[$keyTpl][$i])) 
                $paramsData[$i] =  $this->costum[$keyTpl][$i];   
        }
    }

    $initFiles = Document::getListDocumentsWhere(
        array(
            "id"=> $this->costum["contextId"],
            "type"=> $this->costum["contextType"],
            "subKey"=>"costumFont"
        ), "file"
    );
?>
<?php if($canEdit){ ?> 
    <a class='edit<?php echo $subkeyTpl ?>Params' href='javascript:;' 
        data-id='<?= $this->costum["contextId"]; ?>' 
        data-collection='<?= $this->costum["contextType"]; ?>' 
        data-key='<?php echo $keyTpl ?>' 
        data-path='costum.<?php echo $keyTpl ?>'>
        <i class="fa fa-caret-right" aria-hidden="true"></i> Police et CSS
    </a>
<?php }?>

<style>
    .<?php echo $subkeyTpl ?>.font:before {
        content: "Font";
}
#font-url{
    font-size:20px;
}
</style>


<script type="text/javascript">
jQuery(document).ready(function() {

    sectionDyf.<?php echo $subkeyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    sectionDyf.<?php echo $subkeyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "<?php echo Yii::t('cms', 'Font and css files')?>",
            "icon" : "fa-cog",
            "properties" : {
                "urls": {
                    "inputType" : "array",
                    "label" : "<?php echo Yii::t('cms', 'Urls')?> <span class='text-danger'>(<?php echo Yii::t('cms', 'Developer only')?>)</span>",
                    "placeholder" : "css_filename.css",
                    values :  sectionDyf.<?php echo $subkeyTpl ?>ParamsData.urls,
                },
                "cssCode": {
                    "inputType" : "textarea",
                    "markdown" : true,
                    "label" : "<?php echo Yii::t('cms', 'CSS code')?> <span class='text-danger'>(<?php echo Yii::t('cms', 'Developer only')?>)</span>",
                    "placeholder" : "Exemple : body{background-color:red !important;}",
                    values :  sectionDyf.<?php echo $subkeyTpl ?>ParamsData.cssCode,
                },
                "font-useUploader": {
                    "label" : "<?php echo Yii::t('cms', 'Use the uploader')?>",
                    "inputType" : "checkboxSimple",
                    "params" : {
                        "onText" : "<?php echo Yii::t('common', 'Yes')?>",
                        "offText" : "<?php echo Yii::t('common', 'No')?>",
                        "onLabel" : "<?php echo Yii::t('common', 'Yes')?>",
                        "offLabel" : "<?php echo Yii::t('common', 'No')?>",
                        "labelText" : "label"
                    },
                    checked : exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData.font.useUploader) ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.font.useUploader : false 
                },
                "font-url": {
                    "inputType" : "select",
                    "label" : "<?php echo Yii::t('cms', 'Font')?>",
                    "options" : fontObj,
                     "isPolice" : true,

                    value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData.font.url !="undefined") ?
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData.font.url : ""
                },
                "font-uploader": {
                    inputType :"uploader",
                     "docType" : "file",
                    "itemLimit" : 1,
                    "filetypes": ["ttf"],
                    "endPoint" : "/subKey/costumFont",
                    initList : <?php echo json_encode($initFiles); ?>,
                }
            },
            beforeBuild : function(){
                uploadObj.set(costum.contextType,costum.contextId);
            },
            save : function (data) { 
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties , function(k,val) {
                    var kk= k.split("-").join("][");
                    var isActiveDivParent = ($("#"+k).parent().parent().data("activated") != false);
                    if(val.inputType == "array"){
                        tplCtx.value[kk] = getArray('.'+k+val.inputType);
                    }else{
                        if( isActiveDivParent == true){
                            tplCtx.value[kk] = $("#"+k).val();
                        }
                    }
                });

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    tplCtx.updatePartial=true;tplCtx.removeCache=true;
                    tplCtx.format = true;
                    dataHelper.path2Value( tplCtx, function(params) { 
                        dyFObj.commonAfterSave(params,function(){
                            mylog.log("valiny",params);
                          toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                          location.reload();
                        });
                    } );
                }

            }
        }
    };

    $(".edit<?php echo $subkeyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        if(isDeveloper==false){
           delete sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties.urls;
           delete sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties.cssCode;
        }
        dyFObj.openForm( sectionDyf.<?php echo $subkeyTpl ?>Params,null, sectionDyf.<?php echo $subkeyTpl ?>ParamsData);
         var arrFont = [];
        $.each( sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties , function(k,val) {
            var kk= k.split("-");
            if(kk[0] == "font")
                arrFont.push('.'+k+val.inputType);
        });
        // wrapToDiv(list of class[],"parent class","sub class of parent","col-md-x","col-md-offset-x","tplCtx.path","buttonList.app");
        wrapToDiv(arrFont,"<?php echo $subkeyTpl ?>","font",6,"",tplCtx.path,'font');
        $("#imageUploader_paste").attr("placeholder","Coller une police ou un font (ttf)");
            if(jsonHelper.notNull("costum.css.font.useUploader")){
                if(costum.css.font.useUploader == true){
                   
                    $(".font-uploaderuploader").show();
                    $(".font-urlselect").hide();            
                }else{
                    $(".font-uploaderuploader").hide();
                    $(".font-urlselect").show();
                }     
            }else{
                    $(".font-uploaderuploader").hide();
                    if(jsonHelper.notNull("costum.css.font.url"))
                        $(".font-urlselect").show();
                    
            }


        $('.btn-activator').click(function(){
            $(".font-uploaderuploader").hide();
            if(jsonHelper.notNull("costum.css.font.useUploader")){
                if(costum.css.font.useUploader == true){
                    $(".font-uploaderuploader").show();
                    $(".font-urlselect").hide();            
                }else{
                    $(".font-uploaderuploader").hide();
                    $(".font-urlselect").show();
                }     
            }
        })

      

        $('.font-useUploadercheckboxSimple .btn-dyn-checkbox').on('click',function(){
            if($("#font-useUploader").val()=="true"){
                $(".font-uploaderuploader").show();
                $(".font-urlselect").hide();  
            }else{
                $(".font-uploaderuploader").hide();
                $(".font-urlselect").show(); 
            }
        })
    });



});
</script>
