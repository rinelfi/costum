<!-- By RAMIANDRISON Jean Dieu Donné -->
<!-- email: ramiandrison.jdd@gmail.com -->
<?php 
    $keyTpl = "app";
    $subkeyTpl= $keyTpl."allApp";

    $paramsData = [
        // "search"        =>  [],
        // "live"          =>  [],
        // "annonces"      =>  [],
        // "agenda"        =>  [],
        // "dda"           =>  [],
        // "map"           =>  [],
        // "projects"      =>  [],
    ];

    foreach($paramsData as $i => $v) {
        if(isset($this->costum[$keyTpl]["#".$i]))
            $paramsData[$i] =  $this->costum[$keyTpl]["#".$i];
    }

    $arrayKeys = array_keys($paramsData);
    if(isset($this->costum[$keyTpl])){
        foreach ($this->costum[$keyTpl] as $key => $value) {
            if(!in_array($key,$arrayKeys)){
                $paramsData[ltrim($key, '#')] = $this->costum[$keyTpl][$key];
            }
        }
    }

    if(count($paramsData)==0)
        $paramsData = ["" =>  ""];
    // $cssAnsScriptFilesModule = array(
    //     '/plugins/font-awesome/list.js',
    //   );
    // HtmlHelper::registerCssAndScriptsFiles($cssAnsScriptFilesModule, Yii::app()->request->baseUrl);
?>

<?php if($canEdit){ ?> 
    <a class='edit<?php echo $subkeyTpl ?>Params gova' href='javascript:;' 
        data-id='<?= $this->costum["contextId"]; ?>' 
        data-collection='<?= $this->costum["contextType"]; ?>' 
        data-key='<?php echo $keyTpl ?>' 
        data-path='costum.<?php echo $keyTpl ?>'
    >
        <?php echo Yii::t('common', 'App (Menu)')?>" <i class="fa fa-desktop hide-in-menu-json" ></i>
    </a>
<?php } ?>

<style>
    <?php foreach($paramsData as $i => $v) { ?>
        .<?php echo $subkeyTpl ?>.<?php echo $i ?>-app:before {
            content: "<?php echo $i == "dda" ? "sondage" : $i; ?>   <?php echo isset($v["staticPage"]) ? "(Page statique)":""; ?>";
        }
    <?php } ?>
</style>

<script type="text/javascript">
jQuery(document).ready(function() {
    sectionDyf.<?php echo $subkeyTpl ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    var props={};
    sectionDyf.<?php echo $subkeyTpl ?>AddProps = function(appName,subdomainName="",appType=null){
        var hash = "#app.search";
        var urlExtra = "/page/search";
        var types = {
                    "poi" : "<?php echo Yii::t('common', 'Point of interest')?>",
                    "organizations" : "<?php echo Yii::t('common', 'Organizations')?>",
                    "events" : "<?php echo Yii::t('common', 'Events')?>",
                    "projects" : "<?php echo Yii::t('common', 'Projects')?>",
                    "classifieds" : "<?php echo Yii::t('common', 'Classified ads')?>"
                };

        if(appType == "staticPage"){
            hash = "#app.view";
            urlExtra = "/page/"+appName+"/url/costum.views.tpls.staticPage";
        }else if(appName == "search"){
            hash = "#app.search";
            urlExtra = "/page/search"
        }else if(appName == "agenda"){
            hash = "#app.agenda";
            urlExtra = "/page/agenda";
        }else if(appName == "annonce"){
            hash = "#app.search";
            urlExtra = "/page/annonce";
            types = {
                "classifieds" : "<?php echo Yii::t('common', 'Classified ads')?>"
            }
        }else if(appName == "projects"){
            types = {
                "projects" : "<?php echo Yii::t('common', 'Projects')?>"
            };
            urlExtra = "/page/projects"
        }else if(appName =="map"){
            urlExtra = "/page/maps"
        }else if(appName == "annuaire")
            urlExtra = "/page/annuaire"

        if(appType == "staticPage"){
            props[appName+"-isTemplate"]= {
                "inputType" : "hidden",
                value : true
            };
            props[appName+"-staticPage"]= {
                "inputType" : "hidden",
                value : true
            };
            props[appName+"-useFilter"]= {
                "inputType" : "hidden",
                value : false
            };
        }

        // if(appName != "dda")
        //     props[appName+"-useHeader"]= {
        //         "inputType" : "checkboxSimple",
        //         "label" : "Tête de page",
        //         "params" : {
        //             "onText" : "Oui",
        //             "offText" : "Non",
        //             "onLabel" : "Oui",
        //             "offLabel" : "Non",
        //             "labelText" : "Tête de page"
        //         },
        //         checked : (
        //             typeof sectionDyf.<?php //echo $subkeyTpl ?>ParamsData[appName] &&
        //             typeof sectionDyf.<?php //echo $subkeyTpl ?>ParamsData[appName].useHeader !="undefined") ?
        //             sectionDyf.<?php //echo $subkeyTpl ?>ParamsData[appName].useHeader : true
        //     };

        if(appType != "staticPage" && appName != "dda")
            props[appName+"-useFilter"]= {
                "inputType" : "checkboxSimple",
                "label" : "<?php echo Yii::t('cms', 'Filter')?>",
                "params" : {
                    "onText" : "<?php echo Yii::t('common', 'Yes')?>",
                    "offText" : "<?php echo Yii::t('common', 'No')?>",
                    "onLabel" : "<?php echo Yii::t('common', 'Yes')?>",
                    "offLabel" : "<?php echo Yii::t('common', 'No')?>",
                    "labelText" : "Filter"
                },
                checked : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName] &&
                    typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName].useFilter !="undefined") ?
                    sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName].useFilter : true
            };
        // if(appName != "dda")    
        //     props[appName+"-useFooter"]= {
        //         "inputType" : "checkboxSimple",
        //         "label" : " Pied de page",
        //         "params" : {
        //             "onText" : "Oui",
        //             "offText" : "Non",
        //             "onLabel" : "Oui",
        //             "offLabel" : "Non",
        //             "labelText" : " Pied de page"
        //         },
        //         checked : (typeof sectionDyf.<?php //echo $subkeyTpl ?>ParamsData[appName] &&
        //             typeof sectionDyf.<?php //echo $subkeyTpl ?>ParamsData[appName].useFooter !="undefined") ?
        //             sectionDyf.<?php //echo $subkeyTpl ?>ParamsData[appName].useFooter : true
        //     };

        /*if(appType != "staticPage")
            props[appName+"-inMenu"]= {
                "inputType" : "checkboxSimple",
                "label" : "inMenu",
                "params" : {
                    "onText" : "Oui",
                    "offText" : "Non",
                    "onLabel" : "Oui",
                    "offLabel" : "Non",
                    "labelText" : "inMenu"
                },
                checked : (typeof sectionDyf.<?php //echo $subkeyTpl ?>ParamsData[appName] &&
                    typeof sectionDyf.<?php //echo $subkeyTpl ?>ParamsData[appName].inMenu !="undefined") ?
                    sectionDyf.<?php //echo $subkeyTpl ?>ParamsData[appName].inMenu : true
            };*/

        if(appName != "live" && appName != "dda" && appName != "map")
            props[appName+"-hash"] =  {
                "inputType" : "hidden",
                 value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName] &&
                    typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName]["hash"] !="undefined") ?
                     sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName]["hash"] : hash    
            };

        props[appName+"-icon"]= {
            "inputType" : "select",
            "label" : "<?php echo Yii::t('cms', 'Icon')?>",
            "options" : fontAwesome,
            // "rules" : {                              
            //         "required" : true
            // },
            value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName] &&
                typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName].icon !="undefined") ?
                sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName].icon : ""
        };

        if(appName != "dda" && appName !="live")
            props[appName+"-urlExtra"]= {
                "inputType" : "hidden",
                //"label" : "urlExtra",
                //"placeholder" : "/page/search",
                value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName] &&
                    typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName].urlExtra !="undefined") ?
                    sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName].urlExtra : urlExtra
            };

        props[appName+"-subdomainName"]= {
            "inputType" : "text",
            "label" : "<?php echo Yii::t('cms', 'Menu name')?>",
            value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName] &&
                typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName].subdomainName !="undefined") ?
                sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName].subdomainName : subdomainName
        };

        if(appType != "staticPage" && appName != "live" && appName != "agenda" && appName!= "dda" ){
            props[appName+"-results-smartGrid"]= {
                "inputType" : "hidden",
                checked : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName] &&
                    typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName].results !="undefined"
                    && typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName].results.smartGrid !="undefined") ?
                    sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName].results.smartGrid : true
            };

            props[appName+"-results-renderView"]= {
                "inputType" : "hidden",
                value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName] &&
                    typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName].results !="undefined"
                && typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName].results.renderView !="undefined") ?
                    sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName].results.renderView : "directory.elementPanelHtml"
            };

            props[appName+"-filters-types"]= {
                "inputType" : "selectMultiple",
                "label" : "<?php echo Yii::t('common', 'Type')?>",
                "options" : types,
                "rules" : {                              
                    "required" : true
                },
                value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName] &&
                    typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName].filters !="undefined"
                    && typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName].filters.types !="undefined") ?
                    sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName].filters.types : []
            };
        }
/*        if(appType != "staticPage")
            props[appName+"-filters-type"]= {
                "inputType" : "select",
                "label" : "type",
                "options" : poiFilters,
                values : (typeof sectionDyf.<?php //echo $subkeyTpl ?>ParamsData[appName].filters !="undefined"
                    && typeof sectionDyf.<?php //echo $subkeyTpl ?>ParamsData[appName].filters.type !="undefined") ?
                    sectionDyf.<?php //echo $subkeyTpl ?>ParamsData[appName].filters.type : ""
            };
            
        if(appType != "staticPage")
            props[appName+"-filters-forced-type"]= {
                "inputType" : "select",
                "label" : "forced-type",
                "options" : poiFilters,
                values : (typeof sectionDyf.<?php //echo $subkeyTpl ?>ParamsData[appName].filters !="undefined"
                    && typeof sectionDyf.<?php //echo $subkeyTpl ?>ParamsData[appName].filters.forced !="undefined"
                    && typeof sectionDyf.<?php //echo $subkeyTpl ?>ParamsData[appName].filters.forced.type !="undefined") ?
                    sectionDyf.<?php //echo $subkeyTpl ?>ParamsData[appName].filters.forced.type : ""
            };*/

        if(appType != "staticPage" && appName != "live" && appName != "agenda" && appName != "dda")
            props[appName+"-searchObject-indexStep"]= {
                "inputType" : "numeric",
                "label" : "<?php echo Yii::t('cms', 'Results per pages')?>",
                "rules" : {                              
                    "required" : true
                },
                value : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName] &&
                    typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName].searchObject !="undefined"
                    && typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName].searchObject.indexStep !="undefined") ?
                    sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName].searchObject.indexStep : 0
            };

        if(appType != "staticPage" && appName != "dda" && appName != "live" && appName != "agenda")
            props[appName+"-searchObject-sortBy"]= {
                "inputType" : "selectMultiple",
                "label" : "<?php echo Yii::t('cms', 'Sort by')?>",
                "options" : {
                    "name":"<?php echo Yii::t('common', 'Name')?>"
                },
                values : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName] &&
                    typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName].searchObject !="undefined"
                    && typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName].searchObject.sortBy !="undefined") ?
                    sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName].searchObject.sortBy : ""
            };

        /*if(appType != "staticPage")
            props[appName+"-tagsList"]= {
                "inputType" : "tags",
                "label" : "Liste de mots clé",
                value : (typeof sectionDyf.<?php //echo $subkeyTpl ?>ParamsData[appName].tagsList !="undefined") ?
                    sectionDyf.<?php //echo $subkeyTpl ?>ParamsData[appName].tagsList : ""
            } */
        if(appName == "dda"){
            props[appName+"-showInviteBtn"]= {
                "inputType" : "checkboxSimple",
                "label" : "<?php echo Yii::t('cms', 'Show the invite button')?>",
                "params" : {
                    "onText" : "<?php echo Yii::t('common', 'Yes')?>",
                    "offText" : "<?php echo Yii::t('common', 'No')?>",
                    "onLabel" : "<?php echo Yii::t('common', 'Yes')?>",
                    "offLabel" : "<?php echo Yii::t('common', 'No')?>",
                    "labelText" : "Afficher le boutton inviter"
                },
                checked : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName] &&
                    typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName].showInviteBtn !="undefined") ?
                    sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName].showInviteBtn : true
            };
            props[appName+"-showAmendmentBlock"]= {
                "inputType" : "checkboxSimple",
                "label" : "<?php echo Yii::t('cms', 'Activate the amendment')?>",
                "params" : {
                    "onText" : "<?php echo Yii::t('common', 'Yes')?>",
                    "offText" : "<?php echo Yii::t('common', 'No')?>",
                    "onLabel" : "<?php echo Yii::t('common', 'Yes')?>",
                    "offLabel" : "<?php echo Yii::t('common', 'No')?>",
                    "labelText" : "Activer l'amendement"
                },
                checked : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName] &&
                    typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName].showAmendmentBlock !="undefined") ?
                    sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName].showAmendmentBlock : true
            };
            props[appName+"-showExternLinkBlock"]= {
                "inputType" : "checkboxSimple",
                "label" : "<?php echo Yii::t('cms', 'Activate the external link')?>",
                "params" : {
                    "onText" : "<?php echo Yii::t('common', 'Yes')?>",
                    "offText" : "<?php echo Yii::t('common', 'No')?>",
                    "onLabel" : "<?php echo Yii::t('common', 'Yes')?>",
                    "offLabel" : "<?php echo Yii::t('common', 'No')?>",
                    "labelText" : "Activer le lien externe"
                },
                checked : (typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName] &&
                    typeof sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName].showExternLinkBlock !="undefined") ?
                    sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName].showExternLinkBlock : true
            };
        }
    }

    sectionDyf.<?php echo $subkeyTpl ?>CreateArrayForClassAndAddNewProps = function(paramsData,appName=null){
        mylog.log("lionel",paramsData);
        $.each(paramsData,function(k,v){
            var appType = null;
            sectionDyf[k+"<?php echo $subkeyTpl ?>"]=[];
            if(appName != k){
                if(typeof v["staticPage"] != "undefined"){
                    appType = "staticPage";
                    sectionDyf.<?php echo $subkeyTpl ?>AddProps(k,"",appType);
                }else
                    sectionDyf.<?php echo $subkeyTpl ?>AddProps(k);
                
            }
        });
    }

    sectionDyf.<?php echo $subkeyTpl ?>PushToClassArray = function(){
        if(exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData[""]))
            delete sectionDyf.<?php echo $subkeyTpl ?>ParamsData[""];
        else if(exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData["home"]))
            delete sectionDyf.<?php echo $subkeyTpl ?>ParamsData["home"];
        $.each( sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties , function(k,val) {
            var kk= k.split("-");
            $.each(sectionDyf.<?php echo $subkeyTpl ?>ParamsData,function(kd,vd){
                if(kk[0] == kd)
                    sectionDyf[kd+"<?php echo $subkeyTpl ?>"].push('.'+k+val.inputType);
            });                        
        });
    }

    sectionDyf.<?php echo $subkeyTpl ?>WrapAppToDiv = function(paramsData){
    if(exists(paramsData[""]))
            delete paramsData[""];
    if(exists(paramsData["home"]))
            delete paramsData["home"];
        $.each(paramsData,function(k,v){
                wrapToDiv(sectionDyf[k+"<?php echo $subkeyTpl ?>"],"<?php echo $subkeyTpl ?>",k+"-app",3,"",tplCtx.path,k,"#");
        });
    }
    sectionDyf.<?php echo $subkeyTpl ?>toCamelCase = function(str) {
        return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function(match, index) {
        if (+match === 0) return ""; // or if (/\s+/.test(match)) for white spaces
        return index == 0 ? match.toLowerCase() : match.toUpperCase();
      });
    }

    sectionDyf.<?php echo $subkeyTpl ?>select = 
    '<select class="form-control" id="appName" name="appName" placeholder="<?php echo Yii::t("cms", "menu name")?>">'+
        '<option value="search"><?php echo Yii::t("common", "Search")?></option>'+
        '<option value="live"><?php echo Yii::t("common", "Live")?></option>'+
        '<option value="projects"><?php echo Yii::t("common", "Projects")?></option>'+
        '<option value="annonce"><?php echo Yii::t("common", "Annonces")?></option>'+
        '<option value="agenda"><?php echo Yii::t("common", "Agenda")?></option>'+
        '<option value="dda"><?php echo Yii::t("common", "Sondage")?></option>'+
        '<option value="map"><?php echo Yii::t("common", "Map")?></option>'+
        '<option value="annuaire"><?php echo Yii::t("common", "Annuaire")?></option>'+
    '</select>';

    sectionDyf.<?php echo $subkeyTpl ?>text = '<input type="text" class="form-control" id="appName" name="appName" placeholder="<?php echo Yii::t("cms", "menu name")?>">';

    sectionDyf.<?php echo $subkeyTpl ?>addForm =  
        '<form id="appForm" class="form" role="form">'+
            '<div class="form-group">'+
              '<label for="appName"><?php echo Yii::t("cms", "New menu")?></label>'+
              sectionDyf.<?php echo $subkeyTpl ?>text+
            '</div>'+
            '<div class="form-group">'+
                '<label class="radio-inline"><input type="radio" name="appType" value="staticPage" checked><?php echo Yii::t("cms", "Static page")?></label>'+
                '<label class="radio-inline"><input type="radio" name="appType" value="app"><?php echo Yii::t("cms", "App")?></label>'+
            '</div>'+
        '</form>';
    
    sectionDyf.<?php echo $subkeyTpl ?>Params = {
        "jsonSchema" : {    
            "title" : "<?php echo Yii::t('cms', 'Menu')?>",
            "icon" : "cog",
            "properties" : props,
            save : function (data) { 
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $subkeyTpl ?>Params.jsonSchema.properties , function(k,val) { 
                    var kk= k.split("-").join("][");
                    if($("#"+k).parent().parent().data("activated") !=false)
                        tplCtx.value["#"+kk] = $("#"+k).val();
                });

                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                    tplCtx.updatePartial=true;tplCtx.removeCache=true;
                    tplCtx.format=true;
                    mylog.log("valuessss",tplCtx.value);
                    dataHelper.path2Value( tplCtx, function(params) { 
                        $("#ajax-modal").modal('hide');
                        toastr.success("<?php echo Yii::t('cms', 'Well added')?>");
                        location.reload();
                    } );
                }
            }
        }
    };

    $(".edit<?php echo $subkeyTpl ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = $(this).data("path");
        var existApp = Object.keys(sectionDyf.<?php echo $subkeyTpl ?>ParamsData).join(", ");

        bootbox.dialog({
            title: "<h5 class='text-success text-center'><?php echo Yii::t('cms', 'The existing menus')?></h5>"+
                   "<h6 class='text-danger text-bold text-center'>"+existApp+"</h6>",
            message: sectionDyf.<?php echo $subkeyTpl ?>addForm,
            buttons: {
                cancle: {
                   label: "<?php echo Yii::t('common', 'Cancel')?>",
                   className: 'btn-danger',
                },
                noclose: {
                    label: "<?php echo Yii::t('cms', 'Manage existing')?>",
                    className: 'bg-dark',
                    callback: function(){
                        if(exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData[""]))
                                delete sectionDyf.<?php echo $subkeyTpl ?>ParamsData[""];
                         else if(exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData["home"]))
                                delete sectionDyf.<?php echo $subkeyTpl ?>ParamsData["home"];
                        //alert("Le chargement peux prendre quelque seconde!");
                        sectionDyf.<?php echo $subkeyTpl ?>CreateArrayForClassAndAddNewProps(sectionDyf.<?php echo $subkeyTpl ?>ParamsData);
                        dyFObj.openForm( sectionDyf.<?php echo $subkeyTpl ?>Params,null, sectionDyf.<?php echo $subkeyTpl ?>ParamsData);
                        sectionDyf.<?php echo $subkeyTpl ?>PushToClassArray();
                        sectionDyf.<?php echo $subkeyTpl ?>WrapAppToDiv(sectionDyf.<?php echo $subkeyTpl ?>ParamsData);
                    }
                },
                ok: {
                    label: "<?php echo Yii::t('cms', 'Create new')?>",
                    className: 'bg-green-k',
                    callback: function(){
                        
                        var appName = $('#appName').val();
                        var subdomainName = appName;
                        appName = sectionDyf.<?php echo $subkeyTpl ?>toCamelCase($('#appName').val());
                        var appType = $('input[name=appType]:checked', '#appForm').val();
                        if(appName != '' && appType != ''){
                            if(exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData[""]))
                                delete sectionDyf.<?php echo $subkeyTpl ?>ParamsData[""];
                            else if(exists(sectionDyf.<?php echo $subkeyTpl ?>ParamsData["home"]))
                                delete sectionDyf.<?php echo $subkeyTpl ?>ParamsData["home"];
                            //alert("Le chargemement peux prendre quelque seconde . . .");
                            sectionDyf.<?php echo $subkeyTpl ?>ParamsData[appName] = {};
                            sectionDyf.<?php echo $subkeyTpl ?>AddProps(appName,subdomainName,appType);
                            sectionDyf.<?php echo $subkeyTpl ?>CreateArrayForClassAndAddNewProps(sectionDyf.<?php echo $subkeyTpl ?>ParamsData,appName);
                            dyFObj.openForm( sectionDyf.<?php echo $subkeyTpl ?>Params,null, sectionDyf.<?php echo $subkeyTpl ?>ParamsData);
                            sectionDyf.<?php echo $subkeyTpl ?>PushToClassArray();
                            sectionDyf.<?php echo $subkeyTpl ?>WrapAppToDiv(sectionDyf.<?php echo $subkeyTpl ?>ParamsData);
                            $('body').append('<style>.<?php echo $subkeyTpl ?>.'+appName+'-app:before {content : '+"\'"+subdomainName+"\'"+'}</style>');
                        }
                    }
                }
            }
        });
        $('input[name=appType]').click(function(){
            if( $(this).val() == "staticPage"){
                $("#appName").replaceWith(sectionDyf.<?php echo $subkeyTpl ?>text);
                $('#appName').keyup(function(){
                if($(this).val()!="")
                    $('.bootbox-accept').removeAttr("disabled");
                else if($(this).val() =="")
                    $('.bootbox-accept').prop("disabled",true);
                })
            }
            else if($(this).val() == "app"){
                $("#appName").replaceWith(sectionDyf.<?php echo $subkeyTpl ?>select);
                //$('.bootbox-accept').removeAttr("disabled");
                $('.bootbox-accept').prop("disabled",true);
                $('#appName').on('change', function() {
                    if($.inArray($(this).val(),Object.keys(sectionDyf.<?php  echo $subkeyTpl ?>ParamsData)) >= 0 )
                        $('.bootbox-accept').prop("disabled",true);
                    else
                        $('.bootbox-accept').removeAttr("disabled");
                });
            }
            
        })

   
        $('.bootbox-accept').prop("disabled",true); 
        if($('#appName').val() =="")
                $('.bootbox-accept').prop("disabled",true);
        $('#appName').keyup(function(){
            if($(this).val()!="")
                $('.bootbox-accept').removeAttr("disabled");
            else if($(this).val() =="")
                $('.bootbox-accept').prop("disabled",true);
        });
        // $('#appName').change(function(){
        //     alert('ok');
        //     if($(this).val()!="")
        //         $('.bootbox-accept').removeAttr("disabled");
        //     else if($.inArray($(this).val(),Object.keys(sectionDyf.<?php // echo $subkeyTpl ?>ParamsData)) != -1)
        //         $('.bootbox-accept').prop("disabled",true);
        // })
    });
});
</script>
