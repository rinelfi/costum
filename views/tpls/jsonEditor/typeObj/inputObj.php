<script>
	var typeObjInput = {
			<?php echo $keyTpl ?>text : function(k,type,name,label,placeholder){
		        props[k+"-dynFormCostum-beforeBuild-properties-"+name+"-inputType"]= {
		            "inputType" : "text",
		            "label" : name+"-placeholder",
		            value : (typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k] !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.beforeBuild !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties[name] !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties[name][inputType] !="undefined") ?
		                    sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties[name]["inputType"] : type
		        };

		        props[k+"-dynFormCostum-beforeBuild-properties-"+name+"-label"]= {
		            "inputType" : type,
		            "label" : name+"-label",
		            value : (typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k] !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.beforeBuild !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties[name] !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties[name][label] !="undefined") ?
		                    sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties[name]["label"] : label
		        };
		        props[k+"-dynFormCostum-beforeBuild-properties-"+name+"-placeholder"]= {
		            "inputType" : type,
		            "label" : name+"-type",
		            value : (typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k] !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.beforeBuild !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties[name] !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties[name][placeholder] !="undefined") ?
		                    sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties[name]["placeholder"] : placeholder
		        };
		    },

		    <?php echo $keyTpl ?>select : function(k,type,name,label,options){
		        props[k+"-dynFormCostum-beforeBuild-properties-"+name+"-"+label]= {
		            "inputType" : type,
		            "label" : name+"-label",
		            value : (typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k] !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.beforeBuild !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties[name] !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties[name][label] !="undefined") ?
		                    sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties[name][label] : ""
		        };
		        props[k+"-dynFormCostum-beforeBuild-properties-"+name+"-"+options]= {
		            "inputType" : type,
		            "label" : name+"-placeholder",
		            value : (typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k] !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.beforeBuild !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties[name] !="undefined"
		                    && typeof sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties[name][placeholder] !="undefined") ?
		                    sectionDyf.<?php echo $keyTpl ?>ParamsData.typeObj[k].dynFormCostum.properties[name][placeholder] : ""
		        };
    		}
	}
</script>