

<?php if($canEdit && Authorisation::isInterfaceAdmin() ){ 
if(isset($this->costum["app"])){
    $options = '  <option value="welcome">Home</option>';
    foreach ($this->costum["app"] as $kapp => $vapp) {
      $selected = $page==ltrim($kapp, '#') ? "selected":"";
      $options.="<option class='capitalize' value='".ltrim($kapp, '#')."' ".$selected.">".@$vapp["subdomainName"]."</option>";
    }
    $html = 
    '<div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <select name="page" class="moveToPage form-control" data-path="page" data-id="'.@$id.'" data-collection="cms">'.
                  $options
                .'</select>
            </div>
        </div>
    </div>
    ';
}
?>
	<div class="text-center content-btn-action btn-edit-delete-<?= @$kunik?>">
		
          <!-- <ul class="list-inline" id="options">
            <li class="option option-<?= @$kunik?>"> -->
              <button class="material-button option3 editSectionBtns tooltips edit<?php echo @$kunik?>Params" data-path="<?= @$path ?>" data-id="<?= @$id; ?>" data-collection="cms" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Modifier ce bloc">
                <i class="fa fa-edit" aria-hidden="true"></i>
              </button>

            <!-- </li>
            <li class="option">
              <button class="material-button option2 hide" type="button">
                <i class="fa fa-envelope-o" aria-hidden="true"></i>
              </button>
            </li>
             <li class="option option-<?= @$kunik?>"> -->
              <button class="material-button option1 bg-red deleteLine tooltips margin-top-10" data-path="<?= @$path ?>" data-id="<?= @$id; ?>" data-collection="cms" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Supprimer ce bloc">
                <i class="fa fa-trash" aria-hidden="true"></i>
              </button>
              <?php if(isset($this->costum["app"])){ ?>
                <button class="material-button option1 moveToPage tooltips margin-top-10"
                    title="Déplacer vers la page" data-toggle="popover" data-content='<?=  htmlspecialchars($html, ENT_QUOTES, 'UTF-8')?>' data-html="true" 
                    data-path="<?= @$path ?>" data-id="<?= @$id; ?>" data-collection="cms" type="button"  data-placement="left" title="" data-original-title="Déplacer vers une page">
                  <i class="fa fa-hashtag" aria-hidden="true"></i>
                </button>
              <?php } ?>
            <!-- </li>
          </ul>
          <button class="material-button material-button-toggle material-button-toggle-<?= @$kunik?>" type="button">
            <i class="fa fa-pencil" aria-hidden="true"></i>
          </button> -->



        

	</div>
	<script type="text/javascript">
		// $(document).ready(function () {
		//     $('.material-button-toggle-<?= @$kunik?>').on("click", function () {
		//         $(this).toggleClass('open');
		//         $('.option-<?= @$kunik?>').toggleClass('scale-on');
		//     });
		// });
	</script>

<?php }?>




