<?php 
    $me = isset(Yii::app()->session['userId']) ? Person::getById(Yii::app()->session['userId']) : null;
    $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
    HtmlHelper::registerCssAndScriptsFiles([
      "/js/blockcms/fontAwesome/fontAwesome.js"
    ], $assetsUrl);
    $thisContextId = isset($this->costum["contextId"]) ? $this->costum["contextId"] : (string) $el["_id"];
    $thisContextType = isset($this->costum["contextType"]) ? $this->costum["contextType"] : $el["collection"];
    $thisContextSlug = isset($this->costum["contextSlug"]) ? $this->costum["contextSlug"] : $el["slug"];
    $thisSlug = isset($this->costum["slug"]) ? $this->costum["slug"] : $el["slug"];
    $el             = Element::getByTypeAndId($thisContextType, $thisContextId );
    $thisContextName = $el["name"];

    $tplUsingId     = "";
    $tplInitImage   = "";
    $nbrTplUser     = 0; 
    $nbrTplViewer   = 0;
    $paramsData     = [];
    $insideTplInUse = array();
    $cmsList        = array();
    $newCmsId       = array();
    $cmsInUseId     = array();

    $assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
    $page = ( isset($page) && !empty($page) ) ? $page : "welcome";
    $defaultsHomepage = file_get_contents("../../modules/costum/data/blockCms/config/defaultHomepage.json", FILE_USE_INCLUDE_PATH);
?>
 <style>
   <?php if(!Authorisation::isUserSuperAdmin(Yii::app()->session["userId"]) && isset(Yii::app()->session["user"]["slug"]) && !in_array(Yii::app()->session["user"]["slug"], ["mirana", "gova", "ifals", "devchriston", "nicoss","Jaona","anatolerakotoson","yorre"])){      
     ?>
      .socialEntityBtnActions .editDescBlock, .socialEntityBtnActions .deleteCms{
        display: none;
      }
   <?php } ?>

@media screen and (max-height: 450px) {
  .sidenav {padding-top: 15px;}
  .sidenav a {font-size: 18px;}
}
 </style>
<script type="text/javascript">
    var isInterfaceAdmin = false;
    var thisContextId = notNull(costum) ? costum.contextId : contextId;
    var thisContextType = notNull(costum) ? costum.contextType : contextType ;
    var thisContextSlug = notNull(costum) ? costum.contextSlug : slug ;
    var thisContextName = <?= json_encode($thisContextName) ?>;
    var costumAssetsPath = "<?php echo $assetsUrl ?>";
    var itemClassArray = [];
    var sectionDyf = {};
    var tplCtx = {};
    var isInterfaceAdmin = false;
    var page = "<?= $page ?>"
  <?php if(Authorisation::isInterfaceAdmin()){  ?>
    isInterfaceAdmin = true;
  <?php } ?>
    var DFdata = {
      'tpl'  : '<?php echo $tpl ?>',
      "id"   : "<?php echo $thisContextId ?>"
    };
    if(notNull(costum)){
      costum.col = "<?php echo $thisContextType ?>";
      costum.ctrl = "<?php echo Element::getControlerByCollection($thisContextType) ?>";
    }
   

    var configDynForm = <?php echo json_encode((isset($this->costum['dynForm'])) ? $this->costum['dynForm']:null); ?>;
    var tplsList = <?php echo json_encode((isset($this->costum['tpls'])) ? $this->costum['tpls']:null); ?>;
    var tplAuthorisation = <?php echo json_encode((isset($me["roles"]["superAdmin"])) ? $me["roles"]["superAdmin"]:false); ?>;
</script>

<?php
// Get page name to specify blocks-----------------------------------------
$page = ( isset($page) && !empty($page) ) ? $page : "welcome";

// Get all cms can be use-----------------------------------------
$listCmsCanBeUse = Cms::getCmsByWhere(array(
  "type" => "blockCms"),array( 'name' => 1 )
);
// Get all template except back-up-----------------------------------------
$listTemplate = Cms::getCmsByWhere(array(
  "type" => "template",
  "tplsUser.".$thisContextId.".".$page => array('$ne' => "backup"))
);

// Get sample template-----------------------------------------
$sampleTemplate = PHDB::findOne("cms", array("type" => "template","category"  =>  "Sample"));

// Get tpl in use----------------------------------------------------------
$using = Cms::getCmsByWhere(array("tplsUser.".$thisContextId.".".$page => "using"));

// Get cms haven't tpl parent----------------------------------------------
$newCms = Cms::getCmsByWhere(array(
    "page"=>$page,
    "haveTpl" => "false",
    "parent.".$thisContextId => array('$exists'=>1)),
     array("position" => 1)
   );

/*if(empty($newCms) && ($page == "welcome" || $page == "homeElement")){
  $defaultsHomepage = json_decode($defaultsHomepage,true);
  foreach($defaultsHomepage as $kdef => $vdef){
    $defaultsHomepage[$kdef]["parent"] = [
      $thisContextId => [
        "type" => $thisContextType,
        "name" => $thisContextName
      ]
    ];
    $defaultsHomepage[$kdef]["page"]=$page;
  }
  PHDB::batchInsert(Cms::COLLECTION,$defaultsHomepage);
  $newCms = Cms::getCmsByWhere(array(
    "page"=>$page,
    "haveTpl" => "false",
    "parent.".$thisContextId => array('$exists'=>1)),
     array("position" => 1)
   );
}*/

// Get template in use-----------------------------------------------------
if (!empty($using)) {
  $tplUsingId = isset(array_keys($using)['0']) ? array_keys($using)['0'] : "";
  $insideTplInUse = $using[$tplUsingId]; 
  $tplInitImage = Document::getListDocumentsWhere(array("id"=> $tplUsingId, "type"=>'cms'), "file");
// Get cms liste in use----------------------------------------------------
  $cmsList = Cms::getCmsByWhere(array(
      "tplParent" => $tplUsingId,
      "parent.".$thisContextId => array('$exists'=>1),
      "page" => $page),
      array("position" => 1)
  );
}
$tplCreator   = isset($insideTplInUse["parent"]) ? $insideTplInUse["parent"] : [];
$tplCreatorId = isset(array_keys($tplCreator)["0"]) ? array_keys($tplCreator)["0"] : "";
$tplCms       = isset($insideTplInUse["cmsList"]) ? $insideTplInUse["cmsList"] : [];

// Get template's page original needed for update button condition-----------------------------
$tplPage = isset($insideTplInUse["page"]) ? $insideTplInUse["page"] : "";

// Get each Id of cms in use------------------------------------------------------------------
foreach (array_filter($cmsList) as $key => $value) {
 $cmsInUseId[] = (string)$value['_id'];
}  
// Get each cms haven't template (newCms)-----------------------------------------------------
foreach ($newCms as $key => $value) {
  $cmsList[] = $value;
  $newCmsId[] = (string)$value["_id"];
}

// Merge cms id inside template and new cms---------------------------------------------------
  $idCmsMerged = array_merge($cmsInUseId,$newCmsId);

// Update button Condition--------------------------------------------------------------------
  $tplEdited = false;
  if ($newCmsId != []) {
   $tplEdited = true;
  }

  $btnSave = false;
  $btnSaveChange = false;
  if ($tplUsingId != "") {
    $btnSave = ($idCmsMerged != $tplCms && $idCmsMerged != []);
    $btnSaveChange = ($tplCreatorId == $thisContextId && $idCmsMerged != $tplCms && $tplPage == $page);
  }elseif ($newCmsId != null){
    $btnSave = true;
  }
?>

  <?php
   if(Authorisation::isInterfaceAdmin()){  ?>
<div class="saveTemplate whole-page">
   <div id="saveTemplate">
      <nav class="tpl-engine-btn-list">
         <ul>
            <li class="d view-super-cms hiddenPreview" onclick="tplObj.previewTpl()">
               <a class="" href="javascript:;"> <?php echo Yii::t("common", "Preview")?>
               <i class="fa fa-eye"></i>
               </a>
            </li>

            <li class="a hiddenPreview">
               <a href="javascript:;" id="choix" class="tryOtherTpl"> <?php echo Yii::t("common", "Choose a template")?> 
               <i class="fa fa-list"></i>
               </a>
            </li>
            <?php if ($btnSave == true ) { ?>               
            <li class="b hiddenPreview">
               <a href="javascript:;" class="saveThisTpls" onclick="tplObj.saveThisTpls()"> 
                <?php echo Yii::t("common", "Save as new template")?> 
               <i class="fa fa-save"></i>
               </a>
            </li>
            <?php } ?>
            <?php if ($btnSaveChange == true && $idCmsMerged != "") { ?> 
            <li class="c hiddenPreview">
               <a href="javascript:;" class="saveChangeTpl"><?php echo Yii::t("common", "Save change")?>
               <i class="fa fa-refresh"></i>
               </a>
            </li>
            <?php } ?>
            <?php if(isset($this->costum["contextSlug"])){ ?>
                <li class="e hiddenPreview">
                  <a id="go-to-communecter" href="" data-hash="#page.type.<?= $thisContextType ?>.id.<?= $thisContextSlug ?>" target="_blank"> 
                    <?php echo Yii::t("common", "Go to communecter")?>
                  <i class="fa fa-id-card-o" aria-hidden="true">
                </i> 
                  </a>
                </li>
                <li class="g hiddenPreview"><?php echo $this->renderPartial("costum.views.tpls.jsonEditor.app.app2", array("canEdit" => $canEdit)); ?></li>
                <li class="g gCoForm hiddenPreview"><?php echo $this->renderPartial("costum.views.tpls.blocCoForm", array("canEdit" => $canEdit)); ?></li>            
                <li class="i hiddenPreview">
                  <a class="" data-toggle="modal" data-target="#menuJson" href="javascript:;">
                    <?php echo Yii::t("common", "Advanced settings")?>
                    <i class="fa fa-wrench"> </i>
                  </a>
                </li>
            <?php } ?>
            <!-- <li class="j">
                <a class="openFormDocumentation" data-toggle="modal" data-target="" href="javascript:;">
                  Documentation<i class="fa fa-info"> </i>
                </a>
            </li> -->

         </ul>
      </nav>
   </div>
</div>
<style type="text/css">
 .modal-footer {
  padding: 2px 16px;
  color: white;
  text-align: center;
}
.panel-display-cms{
  position: fixed; /* Stay in place */
  z-index: 10000; /* Sit on top */
  right: 0;
  top: 60px;
  width: 10%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
}
#display-cms{
  width: 100%;
}
#list-cms-disponible {
  padding-left: 2px;
  list-style: none;
}
#mySidenav{
  display: none;
}
 .sidenav {
  height: 100%;
  width: 60%;
  position: fixed;
  z-index: 10;
  left: 0;
  background-color: #fff;
  overflow-x: hidden;
  transition: 0.5s;
  padding-top: 60px;
}

/*.sidenav a {
  padding: 8px 8px 8px 32px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
  transition: 0.3s;
}*/

.sidenav a:hover {
  color: #f1f1f1;
}

.tpl-engine-right .close-list-cms {
  color: currentColor;
  position: absolute;
  top: 0;
  right: 25px;
  font-size: 36px;
  margin-left: 50px;
}

#main {
  transition: margin-left .5s;
  margin-left: 60%;
  z-index: 1000000;
  position: relative;
}

.tpl-engine-right{
  z-index: 100000;
  right: 0;
  background-color: #E7E9EB;
  color: #566576;
  overflow: scroll;
  overflow-x: hidden;
  white-space: nowrap;
  height: -webkit-fill-available;
}
</style>

<div class="row panel-display-cms hiddenPreview">
  <div id="mySidenav" class="sidenav">
    <div class="" id="display-cms"></div>
  </div>

  <div id="main" onmouseover="closeNav()" onmouseleave="openNav()"> 
    <nav class="tpl-engine-right blur">
      <div style="width: 100%;height: 50px;">
        <div class="searchBar-filters pull-left">
          <input id="searchInput" onkeyup="search()" type="text" class="form-control pull-left text-center main-search-bar search-bar" data-field="text" placeholder="Que recherchez-vous ?">
          <span class="text-white input-group-addon pull-left main-search-bar-addon " data-field="text">
            <i class="fa fa-arrow-circle-right"></i>
          </span>
        </div>
        <a href="javascript:void(0)" class="close-list-cms">&times;</a>
      </div>
      <ul id="list-cms-disponible"></ul>
    </nav>
  </div>
</div>

<!-- <div class="row panel-display-cms">
  <div class="col-md-10">
    <div id="myModal" class="modal">
      <div class="modal-content content-display-cms">
        <div class="modal-body"id="display-cms"></div>
        <div class="modal-footer">
          <a href="#" class="btn btn-primary"><?php echo Yii::t("cms", "Use this bloc")?></a>
          <a href="#" class="btn btn-primary editParams"><?php echo Yii::t("common", "Settings")?></a>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-2">
    <nav class="tpl-engine-right blur hiddenPreview">
     <ul id="list-cms-disponible"></ul>
   </nav>
 </div>
</div> -->
<?php } ?>

<div class="row" id="all-block-container" style="margin-right: 0;margin-left:0">
    <?php
    $param=[
      "page"       => $page,
      "canEdit"    => true,
      "cmsList"    => $cmsList,
      "me"         => $me,
      "paramsData" => $paramsData,
      "el"         => $el,
      'costum'     => [
          'contextId' => $thisContextId,
          'contextSlug' => $thisContextSlug,
          'contextType' => $thisContextType
      ]
    ]; 
    echo $this->renderPartial("costum.views.tpls.blockCms.cmsEngine",$param); 
    ?>
</div>
<?php 
  $categoryUnique = array();
  $sumCat = 0;
  foreach ($listTemplate as $key => $value) { 
    $categoryUnique[] = isset($value["category"]) ? $value["category"]: "Autre" ;
    $sumCat += count($key);
  }
  $numberCat = array_count_values($categoryUnique);
?>

<!-- menu json -->
<?php if(Authorisation::isInterfaceAdmin() && isset($this->costum["contextId"]) && !empty($this->costum["contextId"])){  
  echo $this->renderPartial("costum.views.tpls.jsonEditor.menuJson",array("canEdit" => $canEdit)); 
}
?>
<?php // if(!empty($me["slug"]) && $me["slug"]=="ramiandrison"){ 
      // echo $this->renderPartial("costum.views.tpls.jsonEditor.newErgo.menuAndApp",array("canEdit" => $canEdit)); 
// } ?>
<!-- Documentation -->
<?php // echo $this->renderPartial("costum.views.tpls.blockDocumentation",array("canEdit" => $canEdit)); ?>

<?php 
    $descriptionTpl = isset($insideTplInUse["description"]) ? $insideTplInUse["description"] : "";
    $nameTpl = isset($insideTplInUse["name"]) ? $insideTplInUse["name"] : "";
    $categoryTpl = isset($insideTplInUse["category"]) ? $insideTplInUse["category"] : "";
?>

<script type="text/javascript"> 
  tplUsingId = "<?php echo $tplUsingId; ?>";
  var sampleTemplate = <?= json_encode($sampleTemplate); ?>;
  var listCmsCanBeUse = <?= json_encode($listCmsCanBeUse); ?>;
  var mode = localStorage.getItem("previewMode");

function openNav() {
  $(".panel-display-cms").css("width","10%")
  document.getElementById("mySidenav").style.width = "95%";
  document.getElementById("main").style.marginLeft = "50%";
}

function closeNav() {
  $(".panel-display-cms").css("width","30%")
  document.getElementById("mySidenav").style.width = "100%";
  document.getElementById("main").style.marginLeft= "5%";
}

function search() {
  var input, filter, liste, item, i, txtValue;
  input = $('#searchInput');
  filter = input.val().toUpperCase();
  liste = $("#list-cms-disponible");
  item = $('.cms-list');
  ti = item.length;
  for (i = 0; i < item.length; i++) {
   paragraph = item[i].getElementsByTagName("a")[0];
   txtValue = paragraph.textContent || paragraph.innerText;
   if (txtValue.toUpperCase().indexOf(filter) > -1) {
    item[i].style.display = "";
  } else {
    item[i].style.display = "none";
  }
}
}

$(".close-list-cms").click(function(){
  $("#mySidenav").hide();
});

  $.each( listCmsCanBeUse , function(k,val) { 
   $("#list-cms-disponible").append(`
    <li class="cms-list"><a class="cms-disponible" data-path="${val.path}" data-id="${k}" href="javascript:;">
    ${val.profilImageUrl ? '<img class="fit-picture" style="height: 50px;max-width: 75px;"src='+val.profilImageUrl+'>' : '<div class="div-img" style="display:inline-block;width: 75px;text-align:center"><i class="fa fa-image fa-2x"></i></div>'} ${val.name}
     </a>
     </li>`)
 });
  $(".cms-disponible").click(function(){
    $("#mySidenav").show();
   var params = {
    "idblock" : $(this).data("id") ,
    "path" : $(this).data("path") 
  };
  ajaxPost(
    "#display-cms", 
    baseUrl+"/costum/blockcms/loadbloccms",
    params,
    "html"
    );

})
   var tplMenu = `
  <?php if(Authorisation::isInterfaceAdmin()){  ?>

  <div id="listeTemplate">
    <div id="tplFiltered">
</div>
     <div class="row">
      <div class="col-lg-12" style="border-bottom: 1px solid rgba(100,100,100,0.1);text-align: center;">
         <div class="col-xs-12 padding-20" style="z-index: 1">
            <div style="width: 100%">
              <h3 class="bg-azure" style="padding: 20px; width: 50%; border-radius: 50px; margin : auto; "><?php echo Yii::t("cms", "Choose a template")?></h3>
            </div>
         </div>
      </div>
        <div class="panel panel-default">
          <div class="panel-body">
            <div class="text-left">
              <div class="btn-group">
                <button type="button" class="btn btn-default btn-filter" data-target="all"><?php echo Yii::t("cms", "All")?><span class="filter-badge"><?= $sumCat ?></span></button>
                <?php  foreach (array_unique($categoryUnique) as $tpl_filtered) { ?>
                <button type="button" class="btn btn-default btn-filter" data-target="<?= preg_replace("/[^a-zA-Z]+/", "",$tpl_filtered) ?>"><?php echo Yii::t("cms", $tpl_filtered)?>
                <span class="filter-badge"><?= $numberCat[$tpl_filtered] ?></span>
              </button>
               <?php } ?>               
                <button type="button" class="btn btn-default btn-filter btnRctlyTpl" data-target="backup" onclick="tplObj.backup()"><?php echo Yii::t("cms", "Old template")?></button>
              </div>
            </div>
            <div class="">
              <div class="table table-filter">
                <div class="text-left">
                  <?php foreach ($listTemplate as $kTpl => $value) { 
                    $valCategory = isset($value["category"]) ? $value["category"]: "Autre" ;
                    $listCms = isset($value["cmsList"])?$value["cmsList"]:"";
                    $img = isset($value["img"])?$value["img"]:"";                     
                    $description = isset($value['description'])?$value['description']:'';
                    $name = isset($value['name'])? $value['name']:'';
                    $type = isset($value["type"])? $value["type"]:"";
                    $profil = isset($value["profilImageUrl"]) ?$value["profilImageUrl"]:"";
                    $currentTplsUser = isset($value["tplsUser"]) ?$value["tplsUser"]:[];
                    $counts = array();
                    foreach ($currentTplsUser as $key=>$subarr) {
                      $counts[] = $subarr;
                    }          
                    $countUsing = 0;
                    $countBackup = 0;

                    forEach($counts as $k => $v){
                      $arrayVal = array_values($v);
                      forEach($arrayVal as $k => $v){
                        if($v =="using") $countUsing ++;
                        if($v =="backup") $countBackup ++;
                      }

                    }
                    $tplKeys = $kTpl;
                    $initImage = Document::getListDocumentsWhere(array("id"=> $tplKeys, "type"=>'cms'), "file");
                    $arrayImg = [];
                    foreach ($initImage as $k => $v) {
                      $arrayImg[]= $v["docPath"];
                    }

                     ?>
                  <div class="tplItems" data-status="<?= preg_replace("/[^a-zA-Z]+/", "", $valCategory) ?>">
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 smartgrid-slide-element classifieds">          
                    <?php if($tplUsingId == $value["_id"]){ ?>      
                       <div class="item-slide" style="box-shadow: 0px 0px 2px 3px #5dd55d, 0 2px 10px 0 rgb(63, 78, 88)!important;">
                    <?php }elseif($listCms == ""){?>  
                       <div class="item-slide" style="box-shadow: 0px 0px 2px 3px rgb(198 43 43), 0 2px 10px 0 rgb(63, 78, 88)!important;">
                    <?php }else{ ?>                     
                       <div class="item-slide">
                    <?php } ?> 
                          <div class="entityCenter">
                             <a href="javascript:;" class="pull-right  lbh-preview-element"><i class="fa fa-laptop bg-azure"></i>
                             </a>
                          </div>
                          <div class="img-back-card">
                             <div class="div-img">
                                <img src="<?= $profil?>">
                             </div>
                             <div class="text-wrap searchEntity">
                                <div class="entityRight profil no-padding">
                                   <a href="#" class="entityName letter-orange">
                                     <font >
                                     <?= $name ?>                  
                                     </font>
                                   </a>    
                                </div>
                             </div>
                          </div>
                          <!-- hover -->
                          <div class="slide-hover co-scroll">
                             <div class="text-wrap">
                                   <div class="entityPrice">
                                         <font class="letter-orange" ><?= $name ?></font>
                                   </div>
                                   <div class="entityType col-xs-12 no-padding">
                                      <p class="p-short">
                                         <font >
                                         <?= $description ?>                    
                                         </font>
                                      </p>
                                   </div>
                                   <hr>
                                   <ul class="tag-list">
                                      <span class="badge bg-turq btn-tag tag padding-5" data-tag-value="appareilphoto" data-tag-label="appareilphoto">
                                      <font >
                                      <i class="fa fa-user">
                                        </i> <?= $countUsing ?> <?php echo Yii::t("cms", "Users")?>
                                      </font>
                                      </span>
                                      <span class="badge bg-turq btn-tag tag padding-5" data-tag-value="appareilphoto" data-tag-label="appareilphoto">
                                      <font >
                                          <i class="fa fa-eye">
                                          </i> <?= $countUsing+$countBackup ?> <?php echo Yii::t("cms", "Seen")?>
                                      </font>
                                      </span>
                                   </ul>
                                <hr>
                                <div class="desc">
                                   <div class="socialEntityBtnActions btn-link-content">                                   
                                      <a href="<?= $profil ?>" class="btn btn-info btn-link thumb-info" data-title="Aperçu de <?= $name ?>" data-lightbox="all"><font style="vertical-align: inherit;"><i class="fa fa-eye">
                                        </i><?php echo Yii::t("cms", "Preview")?></font>
                                      </a>
                                       <?php
                                        if ($me['_id'] == $value['creator'] || @$me["roles"]["superAdmin"]){ ?>
                                        <a href="javascript:;" class="btn btn-info btn-link editDesc" data-id="<?= $value["_id"]?>" data-category="<?= @$value["category"]?>" data-name='<?= $name ?>' data-desc='<?php echo $description ?>' data-img='<?= json_encode($initImage) ?>'>
                                          <font > <?php echo Yii::t("cms", "Edit")?></font>
                                        </a> 
                                      <?php } ?>   
                                      <?php if($tplUsingId == $value["_id"]){ ?>      
                                        <p class="text-green bold"><?php echo Yii::t("cms", "Your current template")?></p>
                                      <?php }elseif($listCms == ""){?>  
                                        <p class="text-red bold"><?php echo Yii::t("cms", "Not available")?></p>
                                      <?php }else{ ?>                             
                                      <a href="javascript:;" class="btn btn-info btn-link" onclick='tplObj.chooseConfirm(<?php echo json_encode($listCms)?>,"<?php echo $value["_id"] ?>");'>
                                        <font ><?php echo Yii::t("cms", "Choose")?></font>
                                      </a> 
                                      <?php } ?>    
                                   </div>
                                </div>
                             </div>
                          </div>
                       </div>
                    </div>
                  </div>
                  <?php }?>              
                </div>
              </div>
              <div id="backupTplsContainer">
              </div> 
            </div>
          </div>
        </div>
     </div>
  </div> 
  <?php } ?>`;

var tplObj = {
    backup : function (){
      $('#backupTplsContainer').fadeIn();
      params = {
        "contextId" : DFdata.id,
        "page" : page
      };
      $.ajax({
        type : 'POST',
        data : params,
        url : baseUrl+"/costum/blockcms/gettemplatebycategory",
        dataType : "json",
        async : false,
        success : function(data){
          var backupStr = ""
          if (data != "") {
             $.each(data, function(keyBackup,valueBackup) {
           backupStr +=`
           <div class="tplItems" data-status="backup">
              <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 smartgrid-slide-element classifieds">  
                <div class="item-slide">
                  <div class="entityCenter">
                    <a href="javascript:;" class="pull-right  lbh-preview-element"><i class="fa fa-laptop bg-azure"></i>
                    </a>
                  </div>
                  <div class="img-back-card">
                    <div class="div-img">
                      <img src="`+valueBackup.profilImageUrl+`">
                    </div>
                    <div class="text-wrap searchEntity">
                      <div class="entityRight profil no-padding">
                        <a href="#" class="entityName letter-orange">
                          <font >`+valueBackup.name+`</font>
                        </a>    
                      </div>
                    </div>
                  </div>
                  <div class="slide-hover co-scroll">
                    <div class="text-wrap">
                      <div class="entityPrice">
                        <font class="letter-orange" >`+valueBackup.name+`</font>
                      </div>
                      <div class="entityType col-xs-12 no-padding">
                        <p class="p-short">
                          <font >`+valueBackup.description+`</font>
                        </p>
                      </div>
                      <hr>
                      <ul class="tag-list">
                        <span class="badge btn-tag tag padding-5">
                          <font >                       
                          </font>
                        </span>
                      </ul>
                      <hr>
                      <div class="desc">
                        <div class="socialEntityBtnActions btn-link-content">                                   
                          <a href="`+valueBackup.profilImageUrl+`" class="btn btn-info btn-link thumb-info" data-title="Aperçu de Creercostum2" data-lightbox="all"><font style="vertical-align: inherit;"><i class="fa fa-eye">
</i><?php echo Yii::t("cms", "Preview")?></font>
                          </a>
                          <a href="javascript:;" class="btn btn-info btn-link" onclick="tplObj.switchTemplate('`+keyBackup+`')">
                            <font > <?php echo Yii::t("cms", "Restore")?></font>
                          </a> 
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
           `;
         }); 
          }else{
            backupStr = `<div class='tplItems' data-status='backup'><?php echo Yii::t("cms", "You don't have any recent template on this page")?>!</div>`
          }          
            $('#backupTplsContainer').html(backupStr);     
          }
      });

    },

    //Save template user-------------------------------------------
    saveTplUser : function (idTpl, stat) {
       var tplCtx = {};
       tplCtx.id = idTpl;
       tplCtx.path = "tplsUser."+thisContextId+".<?= $page?>";
       tplCtx.collection = "cms"; 
       tplCtx.value = stat;
       dataHelper.path2Value( tplCtx, function(params) {
        //toastr.success("Terminé!");
      }); 
     },

    //Chose template-----------------------------------------------
    chooseConfirm : function(ide,tplSelected){
    if (<?php echo json_encode($tplEdited); ?>) {
      var adjF = "<?php echo Yii::t("cms", "a section")?>"
      var t = "."
      if (<?php echo json_encode($newCmsId); ?>.length > 1) {
        adjF = "<?php echo Yii::t("cms", "sections")?>";
        t = "s.";
      }
      bootbox.dialog({
        message: `<p class=""><?php echo Yii::t("cms", "It seems that you have added")?> `+adjF+`, <br><?php echo Yii::t("cms", "Please save or fuss your template")?>.</p>
        <div class="alert alert-danger" role="alert">
          <p><b><i class="fa fa-exclamation-triangle">
          </i> NB</b>: <?php echo Yii::t("cms", "Make sure that if you overwrite, your section will be lost")?>!</p>
        </div>`,
        title: "<?php echo Yii::t("cms", "Template selection")?>",
        onEscape: function() {},
        show: true,
        backdrop: true,
        closeButton: true,
        animate: true,
        className: "my-modal",
        buttons: {
          success: {   
            label: "<?php echo Yii::t("common", "Save")?>",
            className: "btn-success",
            callback: function() {
              tplObj.saveThisTpls();
            }
          },
          "<?php echo Yii::t("cms", "Merge")?>": {
            className: "btn-primary",
            callback: function() {
              tplObj.chooseThis(ide,tplSelected);
            }
          },
          "<?php echo Yii::t("cms", "Overwrite")?>": {
            className: "btn-danger",
            callback: function() {
             bootbox.confirm(`<div role="alert">
          <p><?php echo Yii::t("cms", "<b>Delete for all eternity?</b><br><br>You will lose all the sections you added. We cannot recover them once you delete them.<br><br>Are you sure you want to permanently delete this section(s)?")?>
          </p> 
        </div> `,function(result){
              if (!result) {
                return;
              }else{            
                tplObj.deleteCms(<?php echo json_encode($newCmsId); ?>);            
                tplObj.chooseThis(ide,tplSelected);
              }
              urlCtrl.loadByHash(location.hash);
              }); 
            }
          },
          "<?php echo Yii::t("common", "Cancel")?>": function() {}
        }

      }
      );
      }else{    
          tplObj.chooseThis(ide,tplSelected);
      } 
    },

    chooseThis : function(ide,tplSelected){
          var tplUsedData = Object.keys("");
          //Check if this tpl already used before 
          if($.inArray(tplSelected,tplUsedData) != -1){       
            tplObj.switchTemplate(tplSelected);
          }else{
            tplObj.duplicateCms(ide,tplSelected);
                  //Check if page never uses a template
                  if(tplUsingId == ""){
                    tplObj.saveTplUser(tplSelected,"using");
                  }else{
                    tplObj.switchTemplate(tplSelected);
                  }
                  
                  urlCtrl.loadByHash(location.hash);
                  
                }
      },

    //Switch template used------------------------------------------
    switchTemplate : function (tplSelected){ 
      tplObj.saveTplUser(tplSelected,"using");
      tplObj.saveTplUser(tplUsingId,"backup");
      urlCtrl.loadByHash(location.hash);
    },

    //Delete cms----------------------------------------------------
    deleteCms : function (ide){
      var params = {
        "ide" : ide,
        "action" : "delete" 
      }
      $.ajax({
        type : 'POST',
        data : params,
        url : baseUrl+"/costum/blockcms/getcmsaction",
        dataType : "json",
        async : false,

        success : function(data){
          toastr.success("<?php echo Yii::t("cms", "Element deleted")?>!");
        }
      });
    },

    upCmsList : function (idTpl){  
      var params = {
        "tplId" : idTpl,
        "ide" : <?php echo json_encode($idCmsMerged) ?>,
        "action" : "upCmsList" 
      }
      $.ajax({
        type : 'POST',
        data : params,
        url : baseUrl+"/costum/blockcms/getcmsaction",
        dataType : "json",
        async : false,

        success : function(data){
          toastr.success("<?php echo Yii::t("cms", "Completed")?>!!");
        }
      });
    },

    // Remove cmsList inside tpl if cms is empty--------------------
    removeCmsList : function (idTpl){  
      var params = {
        "tplId" : idTpl,
        "action" : "tplEmpty" 
      }
      $.ajax({
        type : 'POST',
        data : params,
        url : baseUrl+"/costum/blockcms/getcmsaction",
        dataType : "json",
        async : false,

        success : function(data){
          toastr.success("<?php echo Yii::t("cms", "Completed")?>!!");
        }
      });
    },

    // Remove cms' stat---------------------------------------------
    removeCmsStat : function (idTpl,elementId){  
      var params = {
        "tplId" : idTpl,
        "ide" : elementId,
        "action" : "removestat" 
      }
      $.ajax({
        type : 'POST',
        data : params,
        url : baseUrl+"/costum/blockcms/getcmsaction",
        dataType : "json",
        async : false,

        success : function(data){
          toastr.success("<?php echo Yii::t("cms", "Completed")?>!!");
        }
      });
    },

    //Duplicate cms-------------------------------------------------
    duplicateCms : function (ide, tplSelected){  
      var params = {
        "ide" : ide,
        "tplParent"  : tplSelected,
        "page"       : "<?= $page?>",
        "parentId"   : thisContextId,
        "parentSlug" : thisContextSlug,
        "parentType" : thisContextType,
        "action" : "duplicate" 
      }
      $.ajax({
        type : 'POST',
        data : params,
        url : baseUrl+"/costum/blockcms/getcmsaction",
        dataType : "json",
        async : false,

        success : function(data){
          toastr.success("<?php echo Yii::t("cms", "Completed")?>!!");
        }
      });
    },

    saveThisTpls : function(){
      if (<?php echo json_encode($idCmsMerged) ?> == "") {
          alert("<?php echo Yii::t("cms", "You can't save an empty template! Please add at least one block")?>")
        }else{
          var tplCtx = {};
          var activeForm = {
            "jsonSchema" : {
              "title" : "<?php echo Yii::t("cms", "Register template")?>",
              "type" : "object",
              "properties" : {
                page : {
                  inputType : "hidden",
                  value : "<?= $page?>"
                },
                name : {
                  label: "<?php echo Yii::t("cms", "Name of the template")?>",
                  inputType : "text",
                  value : "<?php echo ucfirst($tpl) ?>"
                },
                type : { 
                  "inputType" : "hidden",
                  value : "template" 
                },
                category : {
                  label : "<?php echo Yii::t("common", "category")?>",
                  inputType : "select",
                  rules:{
                    "required":true
                  },
                  options : {
                    "Art & Culture"    : "<?php echo Yii::t("cms", "Art & Culture")?>",
                    "Animals & Pets" : "<?php echo Yii::t("cms", "Animals & Pets")?>",
                    "Design & Photography"  :"<?php echo Yii::t("cms", "Design & Photography")?>",
                    "Electronics"  :"<?php echo Yii::t("cms", "Electronics")?>",
                    "Education & Books"  :"<?php echo Yii::t("cms", "Education & Books")?>",
                    "Business & Services"  :"<?php echo Yii::t("cms", "Business & Services")?>",
                    "Cars & Motorcycles"  :"<?php echo Yii::t("cms", "Cars & Motorcycles")?>",
                    "Sports,_Outdoors & Travel"  :"<?php echo Yii::t("cms", "Sports, Outdoors & Travel")?>",
                    "Fashion & Beauty"  :"<?php echo Yii::t("cms", "Fashion & Beauty")?>",
                    "Computers & Internet"  :"<?php echo Yii::t("cms", "Computers & Internet")?>",
                    "Food & Restaurant"  :"<?php echo Yii::t("cms", "Food & Restaurant")?>",
                    "Home & Family"  :"<?php echo Yii::t("cms", "Home & Family")?>",
                    "Entertainment,_Games & Nightlife"  :"<?php echo Yii::t("cms", "Entertainment, Games & Nightlife")?>",
                    "Holidays,_Gifts & Flowers"  :"<?php echo Yii::t("cms", "Holidays, Gifts & Flowers")?>",
                    "Society & People"  :"<?php echo Yii::t("cms", "Society & People")?>",
                    "Medical_(Healthcare)"  :"<?php echo Yii::t("cms", "Medical (Healthcare)")?>",
                    "Other"  :"<?php echo Yii::t("cms", "Others")?>"
                  },
                  value : ""
                },
               image : dyFInputs.image(),
                description : {
                  label: "<?php echo Yii::t("common", "Description")?>",
                  inputType : "text",
                  value : ""
                }
              },
              beforeBuild : function(){
                dyFObj.setMongoId('cms',function(data){
                  uploadObj.gotoUrl = location.hash;
                  tplCtx.id=dyFObj.currentElement.id;
                  tplCtx.collection=dyFObj.currentElement.type;
                });
              },
              save : function () {
                tplCtx.value = {};
                $.each( activeForm.jsonSchema.properties , function(k,val) { 
                  tplCtx.value[k] = $("#"+k).val();
                });
                tplCtx.value.parent={};
                tplCtx.value.parent[thisContextId] = {
                  type : thisContextType, 
                  name : "<?php echo $tpl ?>"
                };
                tplCtx.value.cmsList = <?php echo json_encode($idCmsMerged) ?>;
                mylog.log(tplCtx);
                tplCtx.value.tplsUser={};
                tplCtx.value.tplsUser[thisContextId] = {
                  "<?= $page?>": "using"
                };
                if(typeof tplCtx.value == "undefined")
                  toastr.error('value cannot be empty!');
                else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("<?php echo Yii::t("cms", "Element well added")?>");
                      $("#ajax-modal").modal('hide');                  
                    });
                  } );
                  if(tplUsingId != ""){
                   tplObj.saveTplUser(tplUsingId,"backup");
                  }
                  //if tpl parent choose "save as new", duplicate and leave currently cms then save it into the new template
                  if (<?php echo json_encode($cmsInUseId) ?> !="") {                
                    tplObj.duplicateCms(<?php echo json_encode($cmsInUseId) ?>,tplCtx.id);
                  }
                  tplObj.removeCmsStat(tplCtx.id,<?php echo json_encode($newCmsId) ?>);
                 urlCtrl.loadByHash(location.hash);
                }
              }
            }
          };    
      dyFObj.openForm( activeForm );
      }
    },

    //Preview template-------------------------------------------------------------

    previewTpl :function () { 
      localStorage.setItem("previewMode","v");
      mode = "v";
        $(hideOnPreview).fadeOut();
        // $(".hiddenEdit").show();
        $('.handleDrag').hide();
        var styleHideTitleOnPreview = 
        `<style id="hideBlocksCmsTitleOnHover">
          .block-parent:hover .title:not(:empty):before,.block-parent:hover .title-1:not(:empty):before,
          .block-parent:hover .subtitle:not(:empty):before,.block-parent:hover .title-2:not(:empty):before,
          .block-parent .description:not(:empty):before,.block-parent .title-3:not(:empty):before,
          .block-parent:hover .other:not(:empty):before,.block-parent:hover .title-4:not(:empty):before,
          .block-parent .title-5:not(:empty):before,
          .block-parent:hover .title-6:not(:empty):before{
              display: none !important;
          }
        </style>`;
        $('head').append(styleHideTitleOnPreview);
        previewMode = true;
        /*if(localStorage.getItem("forgetPreview") != "true"){
          bootbox.alert("<p class='text-center'><?php echo Yii::t("cms", "Press the <kbd>Escape</kbd> key to disable the preview mode")?></p>"+
            "<input type='checkbox' name='forgetPreview' id='forgetPreview'>  <?php echo Yii::t('cms', 'No more spotting')?> !",function(){
              if($("#forgetPreview").prop('checked') == true)
                localStorage.setItem("forgetPreview","true");
              else
                localStorage.removeItem("forgetPreview");
            });
        }*/
    },
    bindPreviewTpl : function(){
      window.onhashchange = function() {
        if($(hideOnPreview).is(':visible') !=true){
          $(hideOnPreview).fadeIn(); 
          previewMode = false;
        }
      }
      $(document).keyup(function(e) {
        if (e.key === "Escape") { 
            mode = "w";
            localStorage.setItem("previewMode","w");
            $(hideOnPreview).fadeIn();
            // $(".hiddenEdit").hide();
            $("#hideBlocksCmsTitleOnHover").remove();
            previewMode = false;
          }
        });
    },
    textModeEdit : function(){
      $('.sp-text').each(function(i, objsptext) {
        let allTexts = $(objsptext).html();
        let p = document.createElement('div');
        p.innerHTML = allTexts;
        let links = p.querySelectorAll('.super-href');
        links.forEach(spx => {
          allTexts = allTexts.replace(spx.outerHTML, "["+spx.innerText+"]("+spx.href+")");
        });
        $(objsptext).html(allTexts);
        var spcmstextId = document.querySelector('spcms');
        if (spcmstextId !== null) {
          var texttextId = spcmstextId.textContent;
          spcmstextId.parentNode.replaceChild(document.createTextNode(texttextId), spcmstextId);
        }
      });
    }
  }

jQuery(document).ready(function() {
if(location.href.indexOf('/costum/co/index/') != -1)
  $('#go-to-communecter').attr("href",baseUrl+"/#@"+thisContextSlug);
else
  $('#go-to-communecter').attr("href","https://www.communecter.org/#@"+thisContextSlug);

$(".btn_tpl_filter").click(function(){
    params = {
    "category" : $(this).data("tplcategory"),
    "type" : "template"
  };
  $.ajax({
    type : 'POST',
    data : params,
    url : baseUrl+"/costum/blockcms/gettemplatebycategory",
    dataType : "JSON",
    async : false,
    success : function(data){
      $.each(data,function(key,value){
        console.log("Template"+ value.name);
      })
    }
  });
});

//$(".editDesc").click(function(){
$('body').on('click',".editDesc", function () {
  mylog.log("sary", $(this).data("img"));
  var tplCtx = {};
  var tplId = $(this).data("id");
  tplCtx.collection = "cms";
  var activeFormEdit = {
        "jsonSchema" : {
          "title" : "<?php echo Yii::t("cms", "Modification of the template")?>",
          "type" : "object",
          "properties" : {
            name : {
              label: "<?php echo Yii::t("cms", "Name of the template")?>",
              inputType : "text",
              value : $(this).data("name")
            },
            type : { 
              "inputType" : "hidden",
              value : "template" 
            },
            category : {
              label : "<?php echo Yii::t("common", "Category")?>",
              inputType : "select",
              rules:{
                "required":true
              },
              options : {
                  "Art & Culture"    : "<?php echo Yii::t("cms", "Art & Culture")?>",
                  "Animals & Pets" : "<?php echo Yii::t("cms", "Animals & Pets")?>",
                  "Design & Photography"  :"<?php echo Yii::t("cms", "Design & Photography")?>",
                  "Electronics"  :"<?php echo Yii::t("cms", "Electronics")?>",
                  "Education & Books"  :"<?php echo Yii::t("cms", "Education & Books")?>",
                  "Business & Services"  :"<?php echo Yii::t("cms", "Business & Services")?>",
                  "Cars & Motorcycles"  :"<?php echo Yii::t("cms", "Cars & Motorcycles")?>",
                  "Sports,_Outdoors & Travel"  :"<?php echo Yii::t("cms", "Sports, Outdoors & Travel")?>",
                  "Fashion & Beauty"  :"<?php echo Yii::t("cms", "Fashion & Beauty")?>",
                  "Computers & Internet"  :"<?php echo Yii::t("cms", "Computers & Internet")?>",
                  "Food & Restaurant"  :"<?php echo Yii::t("cms", "Food & Restaurant")?>",
                  "Home & Family"  :"<?php echo Yii::t("cms", "Home & Family")?>",
                  "Entertainment,_Games & Nightlife"  :"<?php echo Yii::t("cms", "Entertainment, Games & Nightlife")?>",
                  "Holidays,_Gifts & Flowers"  :"<?php echo Yii::t("cms", "Holidays, Gifts & Flowers")?>",
                  "Society & People"  :"<?php echo Yii::t("cms", "Society & People")?>",
                  "Medical_(Healthcare)"  :"<?php echo Yii::t("cms", "Medical (Healthcare)")?>",
                  "Other"  :"<?php echo Yii::t("cms", "Others")?>"
              },
              value : $(this).data("category")
            },
            image : {
            "inputType" : "uploader",
            "label" : "<?php echo Yii::t("cms", "Background image")?>",
            "docType": "image",
            "itemLimit" : 1,
            "filetypes": ["jpeg", "jpg", "gif", "png"],
            "showUploadBtn": false,
            initList : $(this).data("img")
          },
            description : {
              label: "<?php echo Yii::t("common", "Description")?>",
              inputType : "text",
              value : $(this).data("desc")
            }
          },

          beforeBuild : function(){
            uploadObj.set("cms",tplId);
          },

          save : function () {
            tplCtx.id = tplId;
            tplCtx.value = {};
            $.each( activeFormEdit.jsonSchema.properties , function(k,val) { 
              tplCtx.value[k] = $("#"+k).val();
            });
            mylog.log(tplCtx);
            if(typeof tplCtx.value == "undefined")
              toastr.error('value cannot be empty!');
            else {
              tplCtx.value.name = tplCtx.value.name.replace(/'/g, '’');
              tplCtx.value.description = tplCtx.value.description.replace(/'/g, '’');
             dataHelper.path2Value( tplCtx, function(params) {
              dyFObj.commonAfterSave(params,function(){
                if(params.result){
                  toastr.success("Modification enregistré!");
                  $("#ajax-modal").modal('hide');
                urlCtrl.loadByHash(location.hash);
              }else
              toastr.error(params.msg);
            });
            } );
           }
         }
       }
     }; 
  dyFObj.openForm( activeFormEdit );
});

$(".saveChangeTpl").click(function(){
  tplCtx.id = tplUsingId;
  tplCtx.collection = "cms";
    var activeForm = {
      "jsonSchema" : {
        "title" : "<?php echo Yii::t("cms", "Others")?>Enregistrement du template",
        "type" : "object",
        onLoads : {
          onload : function(data){
            $(".parentfinder").css("display","none");
          }
        },
        "properties" : {
          name : {
            label: "<?php echo Yii::t("cms", "Name of the template")?>",
            inputType : "text",
            value : `<?php echo $nameTpl ?>`
          }, 
          category : {
              label : "<?php echo Yii::t("common", "Category")?>",
              inputType : "select",
              rules:{
                "required":true
              },
              options : {
                  "Art & Culture"    : "<?php echo Yii::t("cms", "Art & Culture")?>",
                  "Animals & Pets" : "<?php echo Yii::t("cms", "Animals & Pets")?>",
                  "Design & Photography"  :"<?php echo Yii::t("cms", "Design & Photography")?>",
                  "Electronics"  :"<?php echo Yii::t("cms", "Electronics")?>",
                  "Education & Books"  :"<?php echo Yii::t("cms", "Education & Books")?>",
                  "Business & Services"  :"<?php echo Yii::t("cms", "Business & Services")?>",
                  "Cars & Motorcycles"  :"<?php echo Yii::t("cms", "Cars & Motorcycles")?>",
                  "Sports,_Outdoors & Travel"  :"<?php echo Yii::t("cms", "Sports, Outdoors & Travel")?>",
                  "Fashion & Beauty"  :"<?php echo Yii::t("cms", "Fashion & Beauty")?>",
                  "Computers & Internet"  :"<?php echo Yii::t("cms", "Computers & Internet")?>",
                  "Food & Restaurant"  :"<?php echo Yii::t("cms", "Food & Restaurant")?>",
                  "Home & Family"  :"<?php echo Yii::t("cms", "Home & Family")?>",
                  "Entertainment,_Games & Nightlife"  :"<?php echo Yii::t("cms", "Entertainment, Games & Nightlife")?>",
                  "Holidays,_Gifts & Flowers"  :"<?php echo Yii::t("cms", "Holidays, Gifts & Flowers")?>",
                  "Society & People"  :"<?php echo Yii::t("cms", "Society & People")?>",
                  "Medical_(Healthcare)"  :"<?php echo Yii::t("cms", "Medical (Healthcare)")?>",
                  "Other"  :"<?php echo Yii::t("cms", "Others")?>"
              },
              value : "<?php echo htmlspecialchars_decode($categoryTpl) ?>"
            },
          image : {
            "inputType" : "uploader",
            "label" : "<?php echo Yii::t("cms", "Background image")?>",
            "docType": "image",
            "itemLimit" : 1,
            "filetypes": ["jpeg", "jpg", "gif", "png"],
            "showUploadBtn": false,
            initList : <?php echo json_encode($tplInitImage); ?>
          },
          description : {
            label: "<?php echo Yii::t("common", "Description")?>",
            inputType : "text",
            value : `<?php echo $descriptionTpl ?>`
          }
        }
      }
    };          
     activeForm.jsonSchema.afterBuild = function(){     
            uploadObj.set("cms",tplUsingId);
      }; 

    activeForm.jsonSchema.save = function () {
      tplCtx.path = "allToRoot";
      tplCtx.value = {};
      $.each( activeForm.jsonSchema.properties , function(k,val) { 
        tplCtx.value[k] = $("#"+k).val();
      });
      tplCtx.value.cmsList = [];
      tplCtx.value.cmsList = <?php echo json_encode($idCmsMerged) ?>;
      if(typeof tplCtx.value == "undefined")
        toastr.error('value cannot be empty!');
      else {
        mylog.log("activeForm save tplCtx",tplCtx);
        tplCtx.value.name = tplCtx.value.name.replace(/'/g, '’');
        tplCtx.value.description = tplCtx.value.description.replace(/'/g, '’');
        dataHelper.path2Value( tplCtx, function(params) {
          dyFObj.commonAfterSave(params,function(){
            if(params.result){
              toastr.success("Modification enregistré!");
              $("#ajax-modal").modal('hide'); 
              urlCtrl.loadByHash(location.hash);
              smallMenu.open(tplMenu);
            }  else {
              toastr.error(params.msg);
            tplObj.removeCmsStat(tplCtx.id,<?php echo json_encode($newCmsId) ?>);
            }
          });
        } );
      } 
    }
    dyFObj.openForm( activeForm );
});

$(".editBtn").off().on("click",function() { 
  var activeForm = {
    "jsonSchema" : {
      "title" : "Template config",
      "type" : "object",
      "properties" : {
      }
    }
  };
  if(configDynForm.jsonSchema.properties[ $(this).data("key") ])
    activeForm.jsonSchema.properties[ $(this).data("key") ] = configDynForm.jsonSchema.properties[ $(this).data("key") ];
  else
    activeForm.jsonSchema.properties[ $(this).data("key") ] = { label : $(this).data("label") };

  if($(this).data("label"))
    activeForm.jsonSchema.properties[ $(this).data("key") ].label = $(this).data("label");
  if($(this).data("type")){
    activeForm.jsonSchema.properties[ $(this).data("key") ].inputType = $(this).data("type");
    if($(this).data("type") == "textarea" && $(this).data("markdown") )
      activeForm.jsonSchema.properties[ $(this).data("key") ].markdown = true;
  }
  tplCtx.id = contextData.id;
  tplCtx.collection = contextData.type;
  tplCtx.key = $(this).data("key");
  tplCtx.path = $(this).data("path");

  activeForm.jsonSchema.save = function () {  
    tplCtx.value = $( "#"+tplCtx.key ).val();
    mylog.log("activeForm save tplCtx",tplCtx);
    if(typeof tplCtx.value == "undefined")
      toastr.error('value cannot be empty!');
    else {
      dataHelper.path2Value( tplCtx, function(params) { 
        $("#ajax-modal").modal('hide');
      } );
    }
  }
  dyFObj.openForm( activeForm );
});


   $(".editTpl").off().on("click",function() { 
    var activeForm = {
      "jsonSchema" : {
        "title" : "Edit Question config",
        "type" : "object",
        "properties" : {}
      }
    };

    $.each( formTpls [ $(this).data("key") ] , function(k,val) { 
      mylog.log("formTpls",k,val); 
      activeForm.jsonSchema.properties[ k ] = {
        label : k,
        value : val
      };
    });

    mylog.log("formTpls activeForm.jsonSchema.properties",activeForm.jsonSchema.properties); 
    tplCtx.id = $(this).data("id");
    tplCtx.key = $(this).data("key");
    tplCtx.collection = $(this).data("collection");            
    tplCtx.path = $(this).data("path");

    activeForm.jsonSchema.save = function () {  
      tplCtx.value = {};
      $.each( formTpls [ tplCtx.key ] , function(k,val) { 
        tplCtx.value[k] = $("#"+k).val();
      });
      mylog.log("save tplCtx",tplCtx);
      if(typeof tplCtx.value == "undefined")
        toastr.error('value cannot be empty!');
      else {
        dataHelper.path2Value( tplCtx, function(params) { 
          $("#ajax-modal").modal('hide');
        } );
      }

    }
    dyFObj.openForm( activeForm );
  });

//check if new costum
if(localStorage.getItem("newCostum"+thisContextSlug) == thisContextSlug){
      bootbox.confirm({
          message: "<h5 class='text-success text-center'><?php echo Yii::t('cms', 'Welcome to your new costum')?> !</h5>"+
                    "<p class='text-success text-center'><?php echo Yii::t('cms', 'You have <b>4 steps</b> to create your costum')?> :</p>"+
                    "<h6 class='text-center'><u class='text-danger'><?php echo Yii::t('cms', 'Step')?> 1:</u> <?php echo Yii::t('cms', 'Choose a template')?></h6>"+
                    "<h6 class='text-center'><u class='text-danger'><?php echo Yii::t('cms', 'Step')?> 2:</u> <?php echo Yii::t('cms', 'Add section or block CMS')?></h6>"+
                    "<h6 class='text-center'><u class='text-danger'><?php echo Yii::t('cms', 'Step')?> 3:</u> <?php echo Yii::t('cms', 'Add APP or MENU')?></h6>"+
                    "<h6 class='text-center'><u class='text-danger'><?php echo Yii::t('cms', 'Step')?> 4:</u> <?php echo Yii::t('cms', 'Adding data to the CMS block')?></h6>",
          buttons: {
              confirm: {
                  label: 'Ok',
                  className: 'btn-success'
              },
              cancel: {
                  label: 'No',
                  className: 'hidden'
              }
          },
          callback: function (result) {
              if(result){
                  localStorage.removeItem("newCostum"+thisContextSlug);
                  $(".tpl-engine-btn-list ul li.a").addClass("btn-pulse").on('click',function(){
                      $(this).removeClass("btn-pulse");
                      $(".openListTpls").addClass("btn-pulse").on('click',function(){
                          $(this).removeClass("btn-pulse");
                          $(".tpl-engine-btn-list ul li.g").addClass("btn-pulse").on('click',function(){
                              $(this).removeClass("btn-pulse");
                          })
                      })
                  });

              }
          }
      });
   }
  tplObj.bindPreviewTpl();
});

var hideOnPreview = '.editSectionBtns,.editBtn,.editThisBtn,.editQuestion,.addQuestion,.handleDrag,.previewTpl,.openListTpls,.handleDrag,.content-btn-action,.hiddenPreview';
if (typeof previewMode !== "undefined") {
  delete previewMode;
}

// var previewMode = true;

if (localStorage.getItem("previewMode") == "w") {  
 previewMode = false;
}else{    
 previewMode = true;
}
/*$(hideOnPreview).fadeOut();
bootbox.alert('<h6 class="text-center">Appuyer sur ECHAPE pour passer en mode edit</h6>'+
'<p class="text-center"><input type="checkbox" name="not-remember-preview"> Ne plus repéter</p>',function(result){

})*/

$(".tryOtherTpl").click(function(){
  smallMenu.open(tplMenu);
});

$('body').on('click',"button.btn-filter", function () {
  var $target = $(this).data('target');
  if ($target != 'all') {
    $('.tplItems').css('display', 'none');
    $('.tplItems[data-status="' + $target + '"]').fadeIn();
  } else {
    $('#backupTplsContainer').hide();
    $('.tplItems').css('display', 'none').fadeIn();
  }
});

  $("#openModal").modal("hide");
 //In case of empty cmsId, delete cmsList inside tpl to avoid conflict when a user choose it 
if (<?= json_encode($idCmsMerged) ?> == "" && <?= json_encode($btnSaveChange) ?>) {
  tplObj.removeCmsList(tplUsingId);
}

/**************Supercms required*************
 **************Keep selected text when focus on input color*************/
 var saveSelection, restoreSelection;

if (window.getSelection && document.createRange) {
    saveSelection = function(containerEl) {
        var doc = containerEl.ownerDocument, win = doc.defaultView;
        var range = win.getSelection().getRangeAt(0);
        var preSelectionRange = range.cloneRange();
        preSelectionRange.selectNodeContents(containerEl);
        preSelectionRange.setEnd(range.startContainer, range.startOffset);
        var start = preSelectionRange.toString().length;

        return {
            start: start,
            end: start + range.toString().length
        }
    };

    restoreSelection = function(containerEl, savedSel) {
        var doc = containerEl.ownerDocument, win = doc.defaultView;
        var charIndex = 0, range = doc.createRange();
        range.setStart(containerEl, 0);
        range.collapse(true);
        var nodeStack = [containerEl], node, foundStart = false, stop = false;

        while (!stop && (node = nodeStack.pop())) {
            if (node.nodeType == 3) {
                var nextCharIndex = charIndex + node.length;
                if (!foundStart && savedSel.start >= charIndex && savedSel.start <= nextCharIndex) {
                    range.setStart(node, savedSel.start - charIndex);
                    foundStart = true;
                }
                if (foundStart && savedSel.end >= charIndex && savedSel.end <= nextCharIndex) {
                    range.setEnd(node, savedSel.end - charIndex);
                    stop = true;
                }
                charIndex = nextCharIndex;
            } else {
                var i = node.childNodes.length;
                while (i--) {
                    nodeStack.push(node.childNodes[i]);
                }
            }
        }

        var sel = win.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
    }
} else if (document.selection) {
    saveSelection = function(containerEl) {
        var doc = containerEl.ownerDocument, win = doc.defaultView || doc.parentWindow;
        var selectedTextRange = doc.selection.createRange();
        var preSelectionTextRange = doc.body.createTextRange();
        preSelectionTextRange.moveToElementText(containerEl);
        preSelectionTextRange.setEndPoint("EndToStart", selectedTextRange);
        var start = preSelectionTextRange.text.length;

        return {
            start: start,
            end: start + selectedTextRange.text.length
        }
    };

    restoreSelection = function(containerEl, savedSel) {
        var doc = containerEl.ownerDocument, win = doc.defaultView || doc.parentWindow;
        var textRange = doc.body.createTextRange();
        textRange.moveToElementText(containerEl);
        textRange.collapse(true);
        textRange.moveEnd("character", savedSel.end);
        textRange.moveStart("character", savedSel.start);
        textRange.select();
    };
}

var divSelection;
var editable = null;

/*************END Keep selected text when focus on input color*************/

<?php if(Authorisation::isInterfaceAdmin()){  ?>
  $(document).keyup(function(e) {
    if (e.altKey == true && e.keyCode == 80) {
      if(previewMode === false){
        tplObj.previewTpl();
        previewMode = true;
      }else{
        localStorage.setItem("previewMode","w");
        $(hideOnPreview).fadeIn();
        // $(".hiddenEdit").hide();
        $("#hideBlocksCmsTitleOnHover").remove();
        previewMode = false;
        tplObj.textModeEdit();
      }
    }
  });

  if (localStorage.getItem("previewMode") == "w" || cmsList == 0) {  
    // $(".hiddenEdit").hide();
    mode = "w";
  }else{
    tplObj.previewTpl();
    mode = "v";
  }
<?php } ?>
/*****************check for links [text](url)*****************/

var current_full_Url = location.href;
function convAndCheckLink(elems){  
  $(elems).each(function(i, objelems) {
    current_full_Url = location.href;
    var derText = $(objelems).html();
    /* console.log("super",derText);*/
    if (derText !== undefined) {
      derText = derText.replace(/(?:\r\n|\r|\n)/g, '<br>');
      let elements = derText.match(/\[.*?\)/g);
      if( elements !== null && elements.length > 0){
        for(el of elements){
          console.log("darText",el);
          if (el.match(/\[(.*?)\]/) !== null) {
            let eltxt = el.match(/\[(.*?)\]/);
            let elurl = el.match(/\((.*?)\)/);
            if (elurl !== null && eltxt !== null) {            
              let txt = eltxt[1];
              let spUrl = elurl[1];
              console.log("spUrl Text", txt);
              console.log("spUrl", spUrl);
              console.log("spUrl current_full_Url", current_full_Url);
              if (spUrl.includes("#")) {
                let textBeforHtag = spUrl.substring(0, spUrl.indexOf("#"));
                let textBefor_full_UrlHtag = current_full_Url.substring(0, current_full_Url.indexOf("#"));
                if (textBeforHtag == textBefor_full_UrlHtag) {   
                let hashToLoad = spUrl.slice(spUrl.lastIndexOf('#') + 1);
                  derText = derText.replace(el,`<a onclick="urlCtrl.loadByHash('#`+hashToLoad+`')" class="lbh super-href" href="`+spUrl+`">`+txt+`</a>`)          
                }else{

                  derText = derText.replace(el,'<a target="_blank" href="'+spUrl+'">'+txt+'</a>');
                  //   alert("kay")
                  // if (current_full_Url.includes("#")) {
                  //   let textBefor_full_UrlHtag = textBefor_full_UrlHtag.substring(0, textBefor_full_UrlHtag.indexOf("#"));
                  //   if (textBefor_full_UrlHtag === textBeforHtag) {                      
                  //     derText = derText.replace(el,`<a onclick="urlCtrl.loadByHash('#contact')" class="lbh super-href" href="'+spUrl+'">'+txt+'</a>`)
                  //   }
                  // }
                }
              }else{              
              derText = derText.replace(el,'<a target="_blank" href="'+spUrl+'">'+txt+'</a>')
            }
          }else{
            <?php if(Authorisation::isInterfaceAdmin()){?>
              derText = derText.replace(el,'<spcms style="color:red;background-color:whitesmoke;" data-toggle="tooltip" data-placement="top" title="<?php echo Yii::t("cms", "Syntax error. Please try again")?>!">'+el+'</spcms>')
            <?php } ?>
          }
        }
      }
    }
    $(objelems).html(derText);
  }

}); 
}
/***************End check for links [text](url)*****************/

/************Hide edit btn mouse inactive************/
// var timedelay = 1;
// function delayCheck()
// {
//   if(timedelay == 5)
//   {
//     $('.hiddenEdit').fadeOut( 1600 );
//     timedelay = 1;
//   }
//   timedelay = timedelay+1;
// }
// $(document).mousemove(function() {
//   if (mode == "v") {
//   $('.hiddenEdit').fadeIn();
//   timedelay = 1;
//   clearInterval(_delay);
//   _delay = setInterval(delayCheck, 500);
//   }
// });
// _delay = setInterval(delayCheck, 500)


$(".hiddenEdit").click(function(){
  if(previewMode == false){
    tplObj.previewTpl();
    previewMode = true;
    $(this).html('<i class="fa fa-pencil"> </i> <?php echo Yii::t("common", "Edit")?> cms');
  }else{
    localStorage.setItem("previewMode","w");
    $(this).html('<i class="fa fa-eye"> </i> <?php echo Yii::t("common", "Preview")?>');
    $(hideOnPreview).fadeIn();
        // $(".hiddenEdit").hide();
        $("#hideBlocksCmsTitleOnHover").remove();
        previewMode = false;
        tplObj.textModeEdit();
      }
    })
/************End hide edit btn mouse inactive************/

  $(document).on("click", ".closeMenu" , function(){
    /*alert("yes")*/
    /*$("#toolsBar").hide();
    $("#toolsBar").html("");*/
  });
/***************End supercms required***************/

</script>

<?php 
/* 
Super text:
Create by Ifaliana Arimanana
ifaomega@gmail.com
26 Apr 2021
*/
?>
<style>

    .bold {
      font-weight: bold;
    }
    .italic{
     font-style: italic;
    }

    .right{
     text-align: right;
   }
   .center{
     text-align: center;
   }

   .left{
     text-align: left;
   }
   .justify{
     text-align: justify;
   }
   .form_font{
    display: block;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgb(0 0 0 / 8%);
    box-shadow: inset 0 1px 1px rgb(0 0 0 / 8%);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}



#container #containerHeader{
   /* text-align: center;
    cursor:move;*/
}
#container #itino {
    border: 1px solid grey;
    height: 100px;
    width: 602px;
    margin: 0px auto 0;
    padding:10px;
   }
#container fieldset {
    margin: 2px auto 0px;
    width: 100%;
    min-height:26px;
    background-color: #f2f2f2;
    border:none;
}
#container button {
    width: 5ex;
    text-align: center;
    padding: 1px 3px;
    height:30px;
    width:30px;
    background-repeat: no-repeat;
    background-size: cover;
    border:none;
}
#container img{
      width:100%;
}
#container .bold{
 font-weight: bold;
}
#container .italic{
    font-style: italic;
}
#container .underline{
   text-decoration: underline;
}
#container .color-apply{
    width: 50px;
}
#container #input-font{
    width: 100px;
    height: 25px;
}
/*[contentEditable=true]:empty:not(:focus):before{
    content:attr(data-text);
    color:#888;
}

[contenteditable]:focus {
    outline: 0px solid transparent;
}*/

#container .loader {
    border: 6px solid #f3f3f3; 
    border-top: 6px solid #3498db; 
    border-radius: 50%;
    width: 20px;
    height: 20px;
    animation: spin 2s linear infinite;
    margin: 0 auto;
    line-height: 20px;
}

@media (max-width: 800px) {
 .container<?= @$blockCms['tplParent'] ?>{
  padding-left: 20px;
  padding-right: 20px;
  padding-top: 2px;
  padding-bottom: 2px;
  margin-left: 0px;    
  margin-right: 0px;      
  position: sticky; 
 }

     .whole-container<?= @$blockCms['tplParent'] ?> {
      padding-left: 0px;
      padding-right: 0px;
      margin-left: 0px;    
      margin-right: 0px;   
    }
}

/*a {
 color: inherit;
 font-family: inherit !important;
} 

button {
 font-size: 18px !important;
 font-family: FontAwesome; !important;
 color: inherit;
} 

a:hover, a:focus, a:active, a.active { color: inherit; } */

.edit-mode-textId { 
  text-transform:none;
  box-shadow: #038cce24 0px 0px 0px 1px;
    border-radius: 3px;
    height: min-content;
    width: 100%;
    cursor: pointer;

}

.focus-mode-textId {
  box-shadow: #03a9f4 0px 0px 0px 2px;
  border-radius: 3px;
  height: min-content;
  width: 100%;
  cursor: pointer;
}
.sp-text{
  text-transform:none;
}

</style>

<script type="text/javascript">

/*****************Admin panel*****************/
  <?php if(Authorisation::isInterfaceAdmin()){ ?> 
    $('.sp-text').on('focusin', function() {    
      if (mode == "w") {
        // $(this).attr("id","textEditing");
        $(this).addClass("focus-mode-textId");
        $(this).removeClass("edit-mode-textId");
        // Copy text as text without styles
  //       let ce = document.getElementById('textEditing');
  //       for (let i = 0; i < ce.length; i++) {
  //   // classname[i].addEventListener('click', myFunction, false);
  //   ce[i].addEventListener('paste', function (e) {
  //     e.preventDefault()
  //     let text = e.clipboardData.getData('text/plain')
  //     document.execCommand('insertText', false, text)
  //   })
  // }


}
});

    $('.sp-text').on('focusout', function() {
        // $(this).removeAttr('id');
      if (mode == "w") {
        $(this).removeClass("focus-mode-textId");
        $(this).addClass("edit-mode-textId");
      }
    });




   /*function replace_contenttextId(content)
   {
     var exp_match = /(\b(https?|):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
     var element_content=content.replace(exp_match, "<a href='$1'>$1</a>");
     var new_exp_match =/(^|[^\/])(www\.[\S]+(\b|$))/gim;
     var new_content=element_content.replace(new_exp_match, `<a target="_blank" href="http://$2">$2</a>`);
     return new_content;
   }  
   $('#itino').html(replace_contenttextId($('#itino').html()));*/


/************Preview and read mode************/
$(document).keyup(function(e) {
  if (e.altKey == true && e.keyCode == 80) {
    convAndCheckLink(".sp-text");
    $(".sp-text").removeClass("edit-mode-textId");
    $(".sp-text").removeClass("selected-mode-textId");
 /*   mode = "v"
    let elems = document.getElementById("itino");
    if (elems !== null) {
      document.getElementById("itino").contentEditable = "false";
    }
  $(".textId-super").removeClass("edit-mode-textId");*/
  }
});

if(localStorage.getItem("previewMode") == "v"){ 
 setTimeout(function() {
  mode = "v"
  convAndCheckLink(".sp-text");
  $(".hiddenEdit").html('<i class="fa fa-pencil"> </i> <?php echo Yii::t("common", "Edit")?> cms');
  $(".sp-text").removeClass("edit-mode-textId");
  $(".sp-text").removeClass("selected-mode-textId");
}, 100);
}else{
  tplObj.textModeEdit();
  $(".hiddenEdit").html('<i class="fa fa-eye"> </i> <?php echo Yii::t("common", "Preview")?>');
}

$(".view-super-cms").click(function(){
  mode = "v"
  convAndCheckLink(".sp-text");
  $(".sp-text").removeClass("edit-mode-textId");
  $(".sp-text").removeClass("selected-mode-textId");
  let elems = document.getElementById("itino");
  if (elems !== null) {
    document.getElementById("itino").contentEditable = "false";
  }
});

function spValid(element,sp_message){
 let devElem = $(element).val();
 let strDev = "";
 if (devElem.indexOf('script') > -1 || devElem.indexOf(')') > -1 || devElem.indexOf('}') > -1 || devElem.indexOf(']') > -1){
  $(".sp-message").html(sp_message);
  return false;
}else{
  strDev = devElem;
  $(".sp-message").html("");
  return strDev;
}
}

$(document).keyup(function(e) {
  if (e.key === "Escape") { 
    tplObj.textModeEdit();
  }
});

$(".hiddenEdit").click(function(){
    tplObj.textModeEdit();
})


/************End preview and read mode************/
$("#toolsBar").draggable();
cmsBuilder.init();
/*  document.getElementById("itino").contentEditable = "true";*/

let allText = $(".edit-mode-textId").html();
let p = document.createElement('p');
p.innerHTML = allText;
let links = p.querySelectorAll('a');
links.forEach(x => {
  allText = allText.replace(x.outerHTML, "["+x.innerText+"]("+x.href+")");
});
$(".edit-mode-textId").html(allText);


   function insertTextAtCaret(text) {
    var sel, range;
    if (window.getSelection) {
      sel = window.getSelection();
      selectedText = sel.toString();
      if (selectedText !== "") {
        label = "["+selectedText+"](http://www.) ";
      }else{
        label = text;
      }
      if (sel.getRangeAt && sel.rangeCount) {
        range = sel.getRangeAt(0);
        range.deleteContents();
        range.insertNode( document.createTextNode(label) );
      }
    } else if (document.selection && document.selection.createRange) {
      document.selection.createRange().label = label;
    }
  }

  $(".textIdfont-size").change(function() {        
    var item=$(this);
    var val = (item.val())*1;
    $("#itino").css("fontSize", val);
  });


  $(function () {
    var count = 1;

  /**************Open text settings panel**************/
  
  $(".view-super-cms").on("click", function () { 
     $(".sp-text").attr('contenteditable','false');
     $(".sp-text").removeClass('edit-mode-textId'); 
    $(".hiddenEdit").html('<i class="fa fa-pencil"> </i> <?php echo Yii::t("common", "Edit")?> cms');
  });
  $(".sp-text").on("mouseup", function () { 
    if (mode == "w") {
      editable = $(this)[0];
      divSelection = saveSelection(editable);
    }
  });

  $("#display-cms").hover(function(){
    mode = "w"
  })
  $(document).on("mousedown",".sp-text", function () {
      // $(this).attr('contenteditable','true');
    if (mode == "w") {
      $(this).attr('contenteditable','true');
      let ce = document.querySelector('.sp-text');
      for (let i = 0; i < ce.length; i++) {
        // classname[i].addEventListener('click', myFunction, false);
        ce[i].addEventListener('paste', function (e) {
          e.preventDefault()
          let text = e.clipboardData.getData('text/plain')
          document.execCommand('insertText', false, text)
        })
      }

    }

    sel = window.getSelection();
    $(".sp-container").hide();
     if (mode != "v") {      
        $(this).children(":first").focus();
        $("#toolsBar").show();
          $("#toolsBar").html(`
                  <div class="container-fluid" style="background-color: #f2f2f2;">
                  <div class="text-center cursor-move">
                    <i style="cursor:default;font-size: large;" aria-hidden="true"><?php echo Yii::t("cms", "Text settings")?></i>
                    <i class="fa fa-window-close closeBtn pull-right" aria-hidden="true"></i>
                  </div>
                  <div id="container" >
                    <fieldset>
                      <button class="fontStyle italic" onclick="document.execCommand('italic',false,null);" title="Italicize Highlighted Text"><i class="fa fa-italic"></i></button>
                      <button class="fontStyle bold" onclick="document.execCommand( 'bold',false,null);" title="Bold Highlighted Text"><i class="fa fa-bold"></i></button>
                      <button class="fontStyle underline" onclick="document.execCommand( 'underline',false,null);"><i class="fa fa-underline"></i></button>
                      <i class="fa fa-text-height"></i>
                      <select id="input-font" class="input">
                        
                      </select>
                      <button class="fontStyle strikethrough" onclick="document.execCommand( 'strikethrough',false,null);"><i class="fa fa-strikethrough"></i></button>
                      <button class="fontStyle align-left" onclick="document.execCommand( 'justifyLeft',false,null);"><i class="fa fa-align-left"></i></button>
                      <button class="fontStyle align-justify" onclick="document.execCommand( 'justifyFull',false,null);"><i class="fa fa-align-justify"></i></button>
                      <button class="fontStyle align-center" onclick="document.execCommand( 'justifyCenter',false,null);"><i class="fa fa-align-center"></i></button>
                      <button class="fontStyle align-right" onclick="document.execCommand( 'justifyRight',false,null);"><i class="fa fa-align-right"></i></button>
                     <!-- 
                      <button class="fontStyle orderedlist disabled" onclick="document.execCommand('insertOrderedList', false, null);"><i class="fa fa-list-ol"></i></button>
                      <button class="fontStyle unorderedlist disabled" onclick="document.execCommand('insertUnorderedList',false, null)"><i class="fa fa-list-ul"></i></button> -->  
                      <input class="color-apply sp-color-picker spectrum sp-colorize" type="color" id="myColor" value="#0b0b0b"> 
                      <button onclick="insertTextAtCaret('[label](http://www)')" class="fontStyle" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Ajouter une lien">
                      <i class="fa fa-link" aria-hidden="true"></i><br>
                      </button> 

                      <!-- font size start -->
                      <select id="fontSize">
                        <option value="1">1</option>      
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="12">12</option>
                        <option value="14">14</option>
                        <option value="18">18</option>
                      </select>
                      <!-- font size end -->
                      
                    </fieldset>
                    
                  </div>
                
              </div>

          `);


  $('#input-font').append(fontOptions);
      cmsBuilder.init();
      $(".closeBtn").click(function(){
        $(".sp-text").attr('contenteditable','false');      

        text = $(".edit-mode-textId").html();
        tplCtx = {};
        tplCtx.id = $(this).data("id");
        tplCtx.collection = "cms";
        tplCtx.path = $(this).data("field");
        tplCtx.value = text;

        dataHelper.path2Value( tplCtx, function(params) {          
          toastr.success("Modification enrégistré");
        } );
        $("#toolsBar").hide();
        $("#toolsBar").html("");
        // set element parent to edit mode
        $(".container<?= @$blockCms['tplParent'] ?>").addClass("edit-container<?= @$blockCms['tplParent'] ?>");
        $(".container<?= @$blockCms['tplParent'] ?>").removeClass("selected-mode-container<?= @$blockCms['tplParent'] ?>");

        $(".sp-text").removeClass('edit-mode-textId');  
      });

      $( "#myColor" ).change(function() {
       var mycolor = "";
       restoreSelection(editable, divSelection); 
       mycolor = $(".sp-preview-inner").css("background-color");
       setTimeout(function(){    
        document.execCommand('foreColor', false, mycolor);
      },90);
     });

    $( "#input-font" ).change(function() {
      var myFont = document.getElementById("input-font").value;
      document.execCommand('fontName', false, myFont);
   });

   $( "#fontSize" ).change(function() {
      var mysize = document.getElementById("fontSize").value;
      document.execCommand('fontSize', false, mysize);
   });

    function checkDiv(){
      var editorText = document.getElementById("itino").innerHTML;
      if(editorText === ''){
        document.getElementById("itino").style.border = '5px solid red';
      }
    }

    function removeBorder(){
      document.getElementById("itino").style.border = '1px solid transparent';
    }


     //color
     function loadColorPicker(callback) {
      mylog.log("loadColorPicker");
      if( ! jQuery.isFunction(jQuery.ColorPicker) ) {
        mylog.log("loadDateTimePicker2");
        lazyLoad( baseUrl+'/plugins/colorpicker/js/colorpicker.js', 
          baseUrl+'/plugins/colorpicker/css/colorpicker.css',
          callback);
      }
    }
    function loadSpectrumColorPicker(callback) {
      mylog.log("loadSpectrumColorPicker");
      lazyLoad( baseUrl+'/plugins/spectrum-colorpicker2/spectrum.min.js', 
        baseUrl+'/plugins/spectrum-colorpicker2/spectrum.min.css',
        callback);

    }
    var initColor = function(){
      mylog.log("init .colorpickerInput");
      $(".colorpickerInput").ColorPicker({ 
        color : "pink",
        onSubmit: function(hsb, hex, rgb, el) {
          $(el).val(hex);
          $(el).ColorPickerHide();
        },
        onBeforeShow: function () {
          $(this).ColorPickerSetColor(this.value);
        }
      }).bind('keyup', function(){
        $(this).ColorPickerSetColor(this.value);
      });
    };

    if(  $(".colorpickerInput").length){
      loadColorPicker(initColor);
    }

    loadSpectrumColorPicker(function(){
      if(  $(".sp-color-picker").length){
        $(".sp-color-picker").spectrum({
          type: "text",
          showInput: "true",
          showInitial: "true"
        });
        $('.sp-color-picker').addClass('form-control');
        $('.sp-original-input-container').css("width","100%");
      }
    });
  }
/**************End open text settings panel**************/
      
  });
});

  $('.textId-super').click(function(event){
    event.stopPropagation();
  });

  $(document).on("blur", ".edit-mode-textId",function() {
    text = $(this).html();
    tplCtx = {};
    tplCtx.id = $(this).data("id");;
    tplCtx.collection = "cms";
    tplCtx.path = $(this).data("field");
    tplCtx.value = text;

    dataHelper.path2Value( tplCtx, function(params) {} );

  });


    $("#itino").click(function(){     

    });

   $(".textIdbold").on('click', function(event){
    $("#itino").toggleClass("bold");    
    // $("#textIdtextEdit").toggleClass("bold");
  });
   $(".textIditalic").on('click', function(event){ 
    $("#itino").toggleClass("italic");
    // $("#textIdtextEdit").toggleClass("italic");
  });
   $(".textIdright").on('click', function(event){
    $("#itino").toggleClass("right");
    $("#itino").removeClass("center");
    $("#itino").removeClass("left");
    $("#itino").removeClass("justify");
  });
   $(".textIdcenter").on('click', function(event){
    $("#itino").toggleClass("center");
    $("#itino").removeClass("right");
    $("#itino").removeClass("left");
    $("#itino").removeClass("justify");

    // $("#textIdtextEdit").toggleClass("center");
    // $("#textIdtextEdit").removeClass("right");
    // $("#textIdtextEdit").removeClass("left");
    // $("#textIdtextEdit").removeClass("justify");
  });
   $(".textIdleft").on('click', function(event){
    $("#itino").toggleClass("left");
    $("#itino").removeClass("right");
    $("#itino").removeClass("center");
    $("#itino").removeClass("justify");

    // $("#textIdtextEdit").toggleClass("left");
    // $("#textIdtextEdit").removeClass("right");
    // $("#textIdtextEdit").removeClass("center");
    // $("#textIdtextEdit").removeClass("justify");
  });
   $(".textIdjustify").on('click', function(event){
    $("#itino").toggleClass("justify");
    $("#itino").removeClass("right");
    $("#itino").removeClass("center");
    $("#itino").removeClass("left");

    // $("#textIdtextEdit").toggleClass("justify");
    // $("#textIdtextEdit").removeClass("right");
    // $("#textIdtextEdit").removeClass("center");
    // $("#textIdtextEdit").removeClass("left");
  });
 <?php }else{ ?>

   setTimeout(function() {
    convAndCheckLink(".sp-text");
  }, 100);
<?php } ?>

$(".sp-image").click(function(){
if (localStorage.getItem("previewMode") == "w") {   

    last_edited = $(this);

    last_edited.addClass("sp-img-selected");
    $("#toolsBar").show();    
    /*superCmsilay_sary.viewMode();*/
    $("#toolsBar").html(`
         <div class="container-fluid">
    <div class="text-center">
      <i style="cursor:default;font-size: large;" aria-hidden="true"><?php echo Yii::t("cms", "Image settings")?></i>
      <i class="fa fa-window-close closeBtn" aria-hidden="true"></i>
    </div>
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#home"><?php echo Yii::t("cms", "General")?></a></li>
      <li><a data-toggle="tab" href="#menu1"><?php echo Yii::t("cms", "Shadow")?></a></li>
      <li><a data-toggle="tab" href="#menu2"><?php echo Yii::t("cms", "Border")?></a></li>
    </ul>

    <div class="tab-content padding-top-20">
      <div id="home" class="tab-pane fade in active">
        <div class="row">
          <div class="col-md-6">      
            <div class="panel-group">
              <div >          
                <div class="">
                  <div class="">
                    <label><?php echo Yii::t("cms", "Edit photo")?></label>
                    <div class="input-group">
                      <div class="input-group-btn closeMenu" style="width: 0px">                    
                        <button class="edit-imgParams tooltips btn btn-primary" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="<?php echo Yii::t("cms", "Edit photo")?>">
                          <i class="fa fa-camera" aria-hidden="true"></i><br>
                        </button> 
                      </div>
                    </div>
                    <hr>
                    <label><?php echo Yii::t("cms", "Background color")?></label>
                    <div class="input-group">
                      <input id="fond-color" class="sp-color-picker spectrum sp-colorize form-control" name="" value="transparent" placeholder="transparent" style="">
                      <div class="input-group-btn closeMenu" style="width: 0px">
                      </div>
                    </div>
                  </div>  
                </div>  
              </div>
            </div>
          </div>

          <div class="col-md-6">      
            <div class="panel-group">
              <label><?php echo Yii::t("cms", "Adjustment of the photo")?></label>
                <select class="form-control" id="objfit">
                  <option value="contain"><?php echo Yii::t("cms", "Contain")?></option>
                  <option value="cover"><?php echo Yii::t("cms", "Cover")?></option>
                  <option value="fill"><?php echo Yii::t("cms", "Fill")?></option>
                  <option value="inherit"><?php echo Yii::t("cms", "Inherit")?></option>
                  <option value="initial"><?php echo Yii::t("cms", "Initial")?></option>
                  <option value="none"><?php echo Yii::t("cms", "None")?></option>
                  <option value="revert"><?php echo Yii::t("cms", "Revert")?></option>
                  <option value="scale-down"><?php echo Yii::t("cms", "Scale-down")?></option>
                  <option value="unset"><?php echo Yii::t("cms", "Unset")?></option>
                </select>
            </div>
          </div>
          
          <div class="col-md-12">      
            <div class="panel-group">
              <div >          
                <div class="">
                  <div class="">
                    
                  </div>  
                </div>  
              </div>
            </div>
          </div> 

        </div>  
      </div>

      <div id="menu1" class="tab-pane fade">
        <div class="col-md-12">      
          <div class="panel-group">        
            <label><?php echo Yii::t("common", "Color")?></label>
            <div class="pull-right">
            <input id="check-inset" type="checkbox" >
            <label for="check1"><?php echo Yii::t("cms", "Interior")?></label>
            <p></p>
            </div>
            <input id="shw-color" class="sp-color-picker spectrum sp-colorize form-control" name="" value="transparent" placeholder="transparent" style="">
              <div class="range-slider text-center padding-top-20">
                <span class="range-blur__value"><?php echo Yii::t("cms", "Blur")?> </span>
                <input class="range-slider-blur super-cms-range-slider" type="range" value="0" min="0" max="20" step="0.2">
              </div>
              <div class="range-slider text-center padding-top-20">
                <span class="range-spread__value"><?php echo Yii::t("cms", "Spread")?> </span>
                <input class="range-slider-spread super-cms-range-slider" type="range" value="0" min="0" max="20" step="0.2">
              </div>
              <div class="range-slider text-center padding-top-20">
                <span class="range-x__value"><?php echo Yii::t("cms", "Axis")?> X: </span>
                <input class="range-slider-x super-cms-range-slider" type="range" value="" min="-10" max="10" step="0.2">
              </div>
              <div class="range-slider text-center padding-top-20">
                <span class="range-y__value"><?php echo Yii::t("cms", "Axis")?> Y: </span>
                <input class="range-slider-y super-cms-range-slider" type="range" value="" min="-10" max="10" step="0.2">
              </div>
            </div>
          </div>
        </div>

      <div id="menu2" class="tab-pane fade">
        <div class="col-md-12">      
          <div class="col-md-6">      
            <div class="panel-group">
              <div >          
                <div class="">
                  <div class="">
                    <label><?php echo Yii::t("cms", "Border color")?></label>
                    <input id="border-color" class="sp-color-picker spectrum sp-colorize form-control" name="" value="transparent" placeholder="transparent" style="">
                    
                  </div>  
                </div>  
              </div>
            </div>
          </div>

          <div class="col-md-6">      
            <div class="panel-group">
              <label><?php echo Yii::t("cms", "Border radius")?></label>
                <div class="input-group">
                  <input class="form-control elem-control" type="number" min="0" value="" placeholder="0" data-path="border.radius" data-given="border-radius" data-what="border-control"> 
                  <div class="input-group-btn" style="width: 0px">                    
                    <button  data-toggle="collapse" data-target="#border" class="tooltips btn btn-primary" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="<?php echo Yii::t("cms", "More settings")?>">
                      <i class="fa fa-plus" aria-hidden="true"></i><br>
                    </button> 
                  </div>
                </div>
                <div id="border" class="collapse elem-value">
                <div class="col-elem-value">
                  <div class="element-field"> 
                    <label><?php echo Yii::t("cms", "Top left")?></label>
                    <input class="form-control top-left border-control" type="number" min="0" value="" placeholder="0" data-path="border.radius" data-given="border-top-left-radius"> 
                  </div>                  
                  <div class="element-field">      
                    <label><?php echo Yii::t("cms", "Top right")?></label>
                    <input class="form-control top-right border-control" type="number" min="0" value="" placeholder="0" data-path="border.radius" data-given="border-top-right-radius"> 
                  </div>
                   <div class="element-field"> 
                    <label><?php echo Yii::t("cms", "Bottom left")?></label>
                    <input class="form-control bottom-left border-control" type="number" min="0" value="" data-path="border.radius" data-given="border-bottom-left-radius" placeholder="0"> 
                  </div>                  
                  <div class="element-field">      
                    <label><?php echo Yii::t("cms", "Bottom right")?></label>
                    <input class="form-control bottom-right border-control" type="number" min="0" value="" data-path="border.radius" data-given="border-bottom-right-radius" placeholder="0"> 
                  </div>
                </div>
              </div>

              <label><?php echo Yii::t("cms", "Padding")?>/label>
              <div class="input-group">
              <input class="form-control elem-control" type="number" min="0" value="" placeholder="0" data-path="padding" data-given="padding" data-what="padding-control"> 
                <div class="input-group-btn" style="width: 0px">                           
                  <button  data-toggle="collapse" data-target="#padding" class="tooltips btn btn-primary" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="<?php echo Yii::t("cms", "More settings")?>">
                    <i class="fa fa-plus" aria-hidden="true"></i><br>
                  </button> 
                </div>
              </div>
              <div id="padding" class="collapse elem-value">
                <div class="col-elem-value">
                  <div class="element-field" style="width:100px"> 
                    <label><?php echo Yii::t("cms", "Top")?></label>
                    <input class="form-control top-left padding-control" type="number" min="0" value="" placeholder="0" data-path="padding" data-given="padding-top"> 
                  </div>                  
                  <div class="element-field" style="width:100px">      
                    <label><?php echo Yii::t("cms", "Right")?></label>
                    <input class="form-control top-right padding-control" type="number" min="0" value="" placeholder="0" data-path="padding" data-given="padding-right"> 
                  </div>
                   <div class="element-field" style="width:100px"> 
                    <label><?php echo Yii::t("cms", "Left")?></label>
                    <input class="form-control bottom-left padding-control" type="number" min="0" value="" data-path="padding" data-given="padding-left" placeholder="0"> 
                  </div>                  
                  <div class="element-field" style="width:100px">      
                    <label><?php echo Yii::t("cms", "Bottom")?></label>
                    <input class="form-control bottom-right padding-control" type="number" min="0" value="" data-path="padding" data-given="padding-bottom" placeholder="0"> 
                  </div>
                </div>
              </div> 

            </div>
          </div>
        </div>
    </div> 
  </div> 
    `);     

    cmsBuilder.init();
      $(".closeBtn, .closeMenu").click(function(){      
       /*superCmsilay_sary.editMode();*/
        $("#toolsBar").html(``);
        $("#toolsBar").hide();
        last_edited.removeClass("sp-img-selected");
      });

  /**********Image settings Color*************/

    /*********Color*********/

    if(  $(".colorpickerInput").length){
      loadColorPicker(initColor);
    }

    loadSpectrumColorPicker(function(){
      if(  $(".sp-color-picker").length){
        $(".sp-color-picker").spectrum({
          type: "text",
          showInput: "true",
          showInitial: "true"
        });
        $('.sp-color-picker').addClass('form-control');
        $('.sp-original-input-container').css("width","100%");
      }
    });

    $( "#fond-color" ).change(function() {
      last_edited.css("background-color",$("#fond-color").val());   
      tplCtx = {};
      tplCtx.id = "iddddd";
      /*tplCtx.collection = "cms";*/
      tplCtx.path = "css.background.color"; 
      tplCtx.value = $("#fond-color").val();

      dataHelper.path2Value( tplCtx, function(params) {} );
    });

    $( "#border-color" ).change(function() {
     last_edited.css("border-color",$("#border-color").val());
     tplCtx = {};
     tplCtx.id = "iddddd";
     // tplCtx.collection = "cms";
     tplCtx.path = "css.border.color"; 
     tplCtx.value = $("#border-color").val();

     dataHelper.path2Value( tplCtx, function(params) {} );
   });

   /*********End color*********/

  /************Border**padding***margin*************/
   var thisetting = {};
   
   $(".border-control" ).on('keyup change', function (){
    $('.elem-value .border-control').each(function(k,v){
      pars   = $(this).val() ? parseInt($(this).val()) : '0';
      thisIs = $(this).data("given");
      last_edited.css(thisIs,pars+"px");
      last_edited.css(thisIs,pars+"px");
      $(this).val(pars);
      thisetting[thisIs.replace('-radius','').replace('border-','').replace('padding-','').replace('margin-','')] = $(this).val();
    })
  });
    $(".padding-control" ).on('keyup change', function (){
    $('.elem-value .padding-control').each(function(k,v){
      pars   = $(this).val() ? parseInt($(this).val()) : '0';
      thisIs = $(this).data("given");
      last_edited.css(thisIs,pars+"px");
      $(this).val(pars);
      thisetting[thisIs.replace('-radius','').replace('border-','').replace('padding-','').replace('margin-','')] = $(this).val();
    })
  });
  //   $(".margin-control" ).on('keyup change', function (){
  //   $('.elem-value .margin-control').each(function(k,v){
  //     pars   = $(this).val() ? parseInt($(this).val()) : '0';
  //     thisIs = $(this).data("given");
  //     $(".ilay_sary" ).css(thisIs,pars+"px");
  //     $(this).val(pars);
  //     thisetting[thisIs.replace('-radius','').replace('border-','').replace('padding-','').replace('margin-','')] = $(this).val();
  //   })
  // });


   $(".elem-control" ).on('keyup change', function (){
      globalConf = $(this).val() ? parseInt($(this).val()) : '0';
      $("."+$(this).data("what")).val(globalConf);
      if ($(this).data("given") != "padding") {
        last_edited.css($(this).data("given"),globalConf+"px");
      }
      $('.elem-value .'+$(this).data("what")).each(function(k,v){
        pars   = $(this).val() ? parseInt($(this).val()) : '0';
        thisIs = $(this).data("given");
        last_edited.css(thisIs,pars+"px");
        $(this).val(pars);
        thisetting[thisIs.replace('-radius','').replace('border-','').replace('padding-','').replace('margin-','')] = $(this).val();
      })
    });

   $( ".elem-control, .elem-value .form-control" ).blur(function() {
    tplCtx = {};
    tplCtx.id = "idd";
    // tplCtx.collection = "cms";
    tplCtx.path = "css."+$(this).data("path"); 
    tplCtx.value = thisetting;

    dataHelper.path2Value( tplCtx, function(params) {} );
  });

   /************End border**padding***margin***********/

    /***object-fit***/
   $( "#objfit" ).val("");
   $( "#objfit" ).change(function() {
    var myfit = $( "#objfit" ).val();
    last_edited.css("object-fit",myfit);
      tplCtx = {};
      tplCtx.id = "iddd";
      // tplCtx.collection = "cms";
      tplCtx.path = "css.object-fit"; 
      tplCtx.value = myfit;

      dataHelper.path2Value( tplCtx, function(params) {} );
    });
   /***End object-fit***/

   /*Box shadow*/
     var eltBlur = $('.range-slider-blur').val();
     var eltSpread = $('.range-slider-spread').val();
     var eltAxeX = $('.range-slider-x').val();
     var eltAxeY = $('.range-slider-y').val();
     var eltColor = $("#shw-color").val();
     var inset = "";

     $( "#check-inset" ).change(function() {
      if ($( "#check-inset" ).is( ":checked" ) == true) {      
        inset = "inset";
      }else{
        inset = "";
      }
      last_edited.css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
       saveShadowilay_sary();
    });

     $( "#shw-color" ).change(function() {
      eltColor = $("#shw-color").val();
      last_edited.css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
      saveShadowilay_sary();
    });
     $('.range-slider').mousedown(function(){
       $('.range-slider-blur').mousemove(function(){
        eltBlur = $('.range-slider-blur').val();
        $(".range-blur__value").html("Blur "+eltBlur+"px");
        last_edited.css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
      });
       $('.range-slider-spread').mousemove(function(){
        eltSpread = $('.range-slider-spread').val();
        $(".range-spread__value").html("Spread "+eltSpread+"px");
        last_edited.css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
      });
       $('.range-slider-x').mousemove(function(){
        eltAxeX = $('.range-slider-x').val();
        $(".range-x__value").html("Axe X "+eltAxeX+"px");
        last_edited.css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
      });  
       $('.range-slider-y').mousemove(function(){
        eltAxeY = $('.range-slider-y').val();
        $(".range-y__value").html("Axe Y "+eltAxeY+"px");
        last_edited.css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
      });
     });
     $('.range-slider').mouseup(function(){
      /*saveShadowilay_sary();*/
     });
     function saveShadowilay_sary(){
      tplCtx = {};
      tplCtx.id = "iddd";
      // tplCtx.collection = "cms";
      tplCtx.path = "css.box-shadow"; 
      tplCtx.value = {};
      tplCtx.value.inset =  inset;
      tplCtx.value.color =  eltColor;
      tplCtx.value.x     =  eltAxeX;
      tplCtx.value.y     =  eltAxeY;
      tplCtx.value.blur  =  eltBlur;
      tplCtx.value.spread =  eltSpread;

      dataHelper.path2Value( tplCtx, function(params) {} );
    }
   /*End box shadow*/

   /****************Other class option****************/

    $(".other-class" ).on('keyup', function (){
     last_edited.removeClassExcept("super-container ilay_sary super-cms other-css-ilay_sary newCss-ilay_sary");
     last_edited.addClass($(".other-class" ).val());
    });
    $(".other-class" ).on('blur', function (){
      tplCtx = {};
      tplCtx.id = "iddd";
      // tplCtx.collection = "cms";
      tplCtx.path = "class.other"; 
      tplCtx.value = $(".other-class" ).val();

      dataHelper.path2Value( tplCtx, function(params) {} );
    });
  /*************End other class option****************/

  /****************Other css option****************/
   var css_pro_and_val = {};
    $(".other-css" ).on('keyup', function (){
      //Validate and apply css
      last_edited.removeClass("other-css-ilay_sary"); 
      last_edited.removeAttr("style");
     var other_css = $(".other-css" ).val()
     var array_val = other_css.split(/[;/^\s*(\n)\s*$/]+/);
     $.each(array_val, function(k,val) {
    /* css validation
     if(val==""  || val == " " || val == /^\s*(\n)\s*$/){
        // $(".css_err").text(css_propertyname+" insupporté!"); 
      }else{   */   
       var css_propertyname = val.split(':')[0];  
       var css_value = val.substr(val.lastIndexOf(":") + 1);
       if(CSS.supports(css_propertyname, css_value)){;
         if(css_propertyname != css_value){
         css_pro_and_val[css_propertyname] = css_value;
         }
       }
 /*    }*/
     });
     // let para = document.querySelector($(".ilay_sary" ));
     // let compStyles = window.getComputedStyle(para);
     // console.log(compStyles);
     $(".ilay_sary" ).css(css_pro_and_val);
    });

    $(".other-css" ).on('blur', function (){
      tplCtx = {};
      tplCtx.id = "iddd";
      // tplCtx.collection = "cms";
      tplCtx.path = "css.other"; 
      tplCtx.value = css_pro_and_val;

      dataHelper.path2Value( tplCtx, function(params) {} );
    });
  /*************End other css option****************/

    $(".stop-propagation").click(function(e) {
      e.stopPropagation();
    });

    /* Upload d'image */
      jQuery(document).ready(function() {
          sectionDyf.ilayasarParams = {
            "jsonSchema" : {    
              "title" : "<?php echo Yii::t("cms", "Image import")?>",
              "description" : "Personnaliser votre bloc",
              "icon" : "fa-cog",
              
              "properties" : {    
                "image" :{
                 "inputType" : "uploader",
                 "label" : "image",
                 "docType": "image",
                 "contentKey" : "slider",
                 "itemLimit" : 1,
                 "endPoint": "/subKey/block",
                 "domElement" : "image",
                 "filetypes": ["jpeg", "jpg", "gif", "png"],
                 "label": "Image :",
                 "showUploadBtn": false,
                 initList : "echo json_encode($initImage)"
                },
              },
              beforeBuild : function(){
                  uploadObj.set("cms","iddd");
              },
              save : function (data) {  
                tplCtx.value = {};
                $.each( sectionDyf.ilaysarParams.jsonSchema.properties , function(k,val) { 
                  tplCtx.value[k] = $("#"+k).val();
                  if (k == "parent")
                    tplCtx.value[k] = formData.parent;

                  if(k == "items")
                    tplCtx.value[k] = data.items;
                });
                
                if(typeof tplCtx.value == "undefined")
                  toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) {
                      dyFObj.commonAfterSave(params,function(){
                        toastr.success("<?php echo Yii::t("cms", "Element well added")?>");
                        $("#ajax-modal").modal('hide');
                        urlCtrl.loadByHash(location.hash);
                      });
                    } );
                }

              }
            }
          };

          $(".edit-imgilaysarParams").off().on("click",function() {  
            tplCtx.subKey = "imgParent";
            tplCtx.id = "iddd";
            // tplCtx.collection = "cms";
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.imgilaysarParams,null, null);
          });
        });

/* End upload d'image */
  }
});


/***************END Admin panel*****************/
/*End document ready*/
</script>
