<?php

    $keyTpl = "chartForElement";

    $paramsData = [
        "chartTitleText" => "Here Title",
        "chartTitleColor" => "",
        "chartTitleSize" => 20,
        "chartDataLabel" => "Lorem Ipsum",
        "chartDataToShow" => [],
        "chartBlockType" => "pie",
        "chartAxeX" => true,
        "chartAxeY" => true,
        "chartAxeColor" => "transparent",
        "chartDataColorGradient" => false,
        "chartDataColorBg" => "",
        "chartLegendDisplay" => true,
        "chartLegendPosition" => "bottom",
        "chartLegendSize" => 20,
        "chartLegendColor" => "black",
        "chartBlockHeight" => 250,
        "chartBlockWidth" => 100,
        "chartBlockBackgroundColor" => "transparent",
        "chartBlockBorderRadius" => 2,
        "chartDataElementTypes" => ["organizations"],
        "chartDataFields" => "type",
        "chartDesignImage" => false
    ];

    if (isset($blockCms)) {
        foreach ($paramsData as $e => $v) {
            if (isset($blockCms[$e]) ) {
                $paramsData[$e] = $blockCms[$e];
            }
        }
    }
?>

<style media="screen">
  #chartContainer<?=$kunik?>{
    position: relative;
    width:<?= $paramsData['chartBlockWidth'] ?>%;
    padding: 3%;
    height: <?= $paramsData['chartBlockHeight'] ?>px;
    background: <?= $paramsData['chartBlockBackgroundColor'] ?>;
    border-radius: <?= $paramsData['chartBlockBorderRadius'] ?>px;
    justify-content: center !important;
  }
</style>
<center>
  <div id="chartContainer<?=$kunik?>">
      <canvas id="chart<?=$kunik?>" width="900"></canvas>
  </div>
</center>

<script src="/plugins/Chart-2.8.0/Chart.min.js"></script>
<script type="text/javascript">
    var ctx<?= $kunik ?> = $("#chart<?=$kunik?>"); // Canvas context
    var data<?= $kunik ?> = []; // Data for chart
    var chartData<?= $kunik ?> = []; // Data for chart
    var dataToShow<?= $kunik ?> = <?php echo json_encode( $paramsData["chartDataToShow"] ); ?>; // Data for chart
    var fields<?= $kunik ?> = {}; // Generic fiels to fetch
    var selectedField<?= $kunik ?> = "<?= (is_array($paramsData["chartDataFields"]))?$paramsData["chartDataFields"][0]:$paramsData["chartDataFields"] ?>"; // field to make statistic
    var chartBgColor<?= $kunik ?> = []; // Generic background color
    var chartBrColor<?= $kunik ?> = []; // Generic border color

    sectionDyf.<?=$kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    sectionDyf.<?=$kunik ?>Params = {};

    const getValueByPath = (object, path) => {
        if (path === undefined || path === null) {
            return object;
        }
        const parts = path.split('.');
        for (let i = 0; i < parts.length; ++i) {
            if (object === undefined || object === null) {
                return undefined;
            }
            const key = parts[i];
            object = object[key];
        }
        return object;
    }

    /**
     * liste des fields
     * */
    let extraFields = [];

    if(typeof getValueByPath(costum, "typeObj.organizations.dynFormCostum.beforeBuild.properties")!="undefined"){
        extraFields = costum.typeObj.organizations.dynFormCostum.beforeBuild.properties;
    }

    Object.keys(extraFields).forEach(function(keyValue, k) {
        let keyV = keyValue;
        let keyL = "";

        if(keyValue.indexOf("-")!=-1){
            keyV = keyValue.replace("-", ".");

        }else if(keyValue.indexOf("[")!=-1){
            keyV = keyValue.replace("[", ".").replace("]", "");
        }

        if(typeof extraFields[keyValue] != "undefined" && typeof extraFields[keyValue].label != "undefined"){
            keyL = extraFields[keyValue].label
        }else{
            keyL = keyV;
        }

        fields<?= $kunik ?>[keyV] = keyL;
    });

    fields<?= $kunik ?>["source.keys"] = tradDynForm.network+" communecter";
    fields<?= $kunik ?>["type"] = trad.type;

    if(typeof fields<?= $kunik ?>["tags"]=="undefined"){
        fields<?= $kunik ?>["tags"] = trad.tags;
    }

    var elementTypes<?= $kunik ?> = <?php echo json_encode($paramsData['chartDataElementTypes'])?>;
    if(typeof elementTypes<?= $kunik ?> == "string"){
        elementTypes<?= $kunik ?> = [elementTypes<?= $kunik ?>];
    }

    var searchParams<?= $kunik ?> = {
        //container: "#search-container-<?= $kunik ?>",
        loadEvent: {
            default:"dashboard"
        },
        defaults: {
            types : elementTypes<?= $kunik ?>,
            fields:{...fields<?= $kunik ?>, "tags":"tags", "name":"name"},
            indexStep : 600
        }
    };

    coInterface.showLoader("#chart<?=$kunik?>");
    var search<?= $kunik ?> = {};

    setTimeout(function() {
        search<?= $kunik ?> = searchObj.init(searchParams<?= $kunik ?>);

        search<?= $kunik ?>.dashboard.successComplete = function(fObj, response){
            //alert(JSON.stringify(response.results))
            for (var [key, value] of Object.entries(response.results)) {

                let valueFromPath = getValueByPath(value, selectedField<?= $kunik ?>);

                if(typeof valueFromPath=="undefined"){
                    // To check
                    if(costum.lists[selectedField<?= $kunik ?>]){
                        if(Array.isArray(costum.lists[selectedField<?= $kunik ?>])){
                            valueFromPath = costum.lists[selectedField<?= $kunik ?>].filter(function (x) {
                                return value.tags.includes(x)})
                        }else if(typeof costum.lists[selectedField<?= $kunik ?>] == "object" && notNull(costum.lists[selectedField<?= $kunik ?>])){
                            valueFromPath = Object.keys(costum.lists[selectedField<?= $kunik ?>]).filter(function (x) {
                            return value.tags.includes(x) })
                        }
                    }
                }

                if(typeof valueFromPath !="undefined"){
                    let translatedValue = valueFromPath;

                    if(typeof translatedValue!="undefined" && notNull(translatedValue) && translatedValue!=""){
                        if(typeof(translatedValue)=="string"){
                            translatedValue = [translatedValue]
                        }

                        let labely = "";

                        translatedValue.forEach(function(label, index){
                            labely = label.toLowerCase();
                            if(typeof trad[label]!="undefined"){
                                labely = trad[label].toLowerCase();
                            }

                            if(typeof tradCategory[label]!="undefined"){
                                labely = tradCategory[label].toLowerCase();
                            }
                            if(typeof tradDynForm[label]!="undefined"){
                                labely = tradDynForm[label].toLowerCase();
                            }

                            //labels<?= $kunik ?>[translatedValue];
                            if(typeof data<?= $kunik ?>[labely] != "undefined"){
                                data<?= $kunik ?>[labely] = data<?= $kunik ?>[labely]+1;
                                chartData<?= $kunik ?>[labely] = data<?= $kunik ?>[labely]+1;
                            }else{
                                data<?= $kunik ?>[labely] = 1;
                                chartData<?= $kunik ?>[labely] = 1;
                            }

                            if(dataToShow<?= $kunik ?>.length!=0 && !dataToShow<?= $kunik ?>.includes(labely)){
                                delete chartData<?= $kunik ?>[labely];
                            }
                        })
                    }
                }
            }

            if(costum.lists && costum.lists["<?= $paramsData['chartBlockType']; ?>Color"]){
                chartBgColor<?= $kunik ?> = costum.lists["<?= $paramsData['chartBlockType']; ?>Color"];
                chartBrColor<?= $kunik ?> = costum.lists["<?= $paramsData['chartBlockType']; ?>Color"];
            }else{
                // Générate chart background and border color
                Object.keys(data<?= $kunik ?>).forEach(function(val, index){
                    let one, two, three = 0
                    if(index%2==0){
                        one = 255 - index*23;
                        two = 25 + index*23;
                        three = 130-index*23;
                    }else{
                        one = 75 + index*23;
                        two = 160-index*23;
                        three = 180-index*23;
                    }

                    chartBgColor<?= $kunik ?>.push('rgba('+(one - index*26)+', '+(two+index*9)+', '+(three+index*39)+', 1)');
                    chartBrColor<?= $kunik ?>.push('rgba('+(one - index*26)+', '+(two+index*9)+', '+(three+index*39)+', 1)');
                });
            }

            if("<?= $paramsData['chartDataColorGradient']; ?>"=="true"){
                let bgColors = (typeof chartBgColor<?= $kunik ?>=="string")?[chartBgColor<?= $kunik ?>]:chartBgColor<?= $kunik ?>;
                let brColors = (typeof chartBrColor<?= $kunik ?>=="string")?[chartBrColor<?= $kunik ?>]:chartBrColor<?= $kunik ?>;
                chartBgColor<?= $kunik ?> = [];
                chartBrColor<?= $kunik ?> = [];
                for(var index in bgColors){
                    let gradient = ctx<?= $kunik ?>[0].getContext("2d").createLinearGradient(0, 0, 0, 260);
                    gradient.addColorStop(0, "white");
                    gradient.addColorStop(1, bgColors[index]);

                    // border color
                    let brGradient = ctx<?= $kunik ?>[0].getContext("2d").createLinearGradient(0, 0, 0, 260);
                    brGradient.addColorStop(0, "white");
                    brGradient.addColorStop(1, brColors[index]);

                    chartBgColor<?= $kunik ?>.push(brGradient);
                    chartBrColor<?= $kunik ?>.push(brGradient);
                }

                if(chartBgColor<?= $kunik ?>.length ==1){
                    chartBgColor<?= $kunik ?>=chartBgColor<?= $kunik ?>[0];
                    chartBrColor<?= $kunik ?>=chartBgColor<?= $kunik ?>[0];
                }
            }

            let chart<?= $kunik ?> = new Chart(ctx<?= $kunik ?>, {
                type: "<?= $paramsData['chartBlockType'] ?>",
                data: {
                    labels: Object.keys(chartData<?= $kunik ?>),
                    datasets: [
                        {
                            label: "<?= $paramsData["chartDataLabel"] ?>",
                            data: Object.values(chartData<?= $kunik ?>),
                            backgroundColor: chartBgColor<?= $kunik ?>,
                            borderColor: chartBrColor<?= $kunik ?>,
                            borderWidth: 2
                        }
                    ]
                },
                <?php if($paramsData['chartDesignImage']==true){ ?>
                plugins: [{
                    afterDraw: chart => {
                        var ctx = chart.chart.ctx;
                        var xAxis = chart.scales['x-axis-0'];
                        var yAxis = chart.scales['y-axis-0'];
                        if(typeof xAxis!="undefined"){
                            xAxis.ticks.forEach((value, index) => {
                                if(typeof costum.lists!="undefined" && typeof costum.lists.imgThematic!="undefined" && typeof costum.lists.imgThematic[value]!="undefined"){

                                    var x = xAxis.getPixelForTick(index);
                                    var height = chart.getDatasetMeta(0).data[index]._model.y;

                                    if(costum.lists.imgThematic[value].includes("/")){

                                        var image = new Image(10, "auto");

                                        image.src = "<?= Yii::app()->getModule('costum')->assetsUrl ?>"+costum.lists.imgThematic[value];

                                        ctx.drawImage(image, x-15, height-40, 30, 30);
                                    }else if(costum.lists.imgThematic[value].includes("\\")){

                                        ctx.font='14px FontAwesome';
                                        ctx.fillText(costum.lists.imgThematic[value], x-12, height-12);
                                    }

                                }
                            })
                        }
                    }
                }],
                <?php } ?>
                options: {
                    responsive: true,
                    maintainAspectRatio:false,
                    legend: {
                        display: <?= $paramsData['chartLegendDisplay'] ?>,
                        position: "<?= $paramsData['chartLegendPosition'] ?>",
                        align: "middle",
                        labels:{
                            fontColor: "<?= $paramsData['chartLegendColor'] ?>",
                            fontSize: <?= $paramsData['chartLegendSize'] ?>,
                            fontStyle:"bold",
                            padding : 20,
                            boxWidth:50,
                            usePointStyle:false
                        }
                    },
                    title: {
                        display: true,
                        text: "<?= $paramsData['chartTitleText'] ?>",
                        fontColor:"<?= $paramsData['chartTitleColor'] ?>",
                        fontSize:"<?= $paramsData['chartTitleSize'] ?>"
                    },
                    cutoutPercentage: <?= ($paramsData['chartBlockType']=='pie')?"0":"70" ?>,
                    scales: {
                        <?php if(!in_array($paramsData['chartBlockType'],["pie","doughnut", "polarArea", "radar"])){ ?>
                        yAxes: [{
                            gridLines:{
                                display:<?= $paramsData['chartAxeY'] ?>,
                                color:"<?= $paramsData['chartAxeColor'] ?>",
                            },
                            ticks:{
                                fontColor:"<?= $paramsData['chartAxeColor'] ?>",
                                display:<?= $paramsData['chartAxeY'] ?>,
                                beginAtZero: true,
                                max:Math.max(...Object.values(data<?= $kunik ?>))+1
                            }
                        }],
                        <?php } ?>
                    <?php if(!in_array($paramsData['chartBlockType'],["pie","doughnut", "polarArea", "radar"])){ ?>
                        xAxes: [{
                            categories: "<i class='fa fa-home'></i>",
                            gridLines:{
                                display:<?= $paramsData['chartAxeX'] ?>,
                                color:"<?= $paramsData['chartAxeColor'] ?>",
                            },
                            ticks:{
                                fontColor:"<?= $paramsData['chartAxeColor'] ?>",
                                display:<?= $paramsData['chartAxeX'] ?>
                            }
                        }]
                    <?php } ?>
                    }
                }
            });


            /****** DYNFORM CONFIG OF GRAPH ******/
            sectionDyf.<?=$kunik ?>Params = {
                "jsonSchema" : {
                    "title" : "Configurer votre section",
                    "description" : "Personnaliser votre section",
                    "icon" : "fa-cog",
                    "properties" : {
                        "chartBlockType" :{
                            "inputType" : "select",
                            "label" : "Type de graph",
                            values : sectionDyf.<?=$kunik ?>ParamsData.chartBlockType,
                            "class" : "form-control",
                            "options" : {bar:"bar", pie:"pie", doughnut:"doughnut", line:"line", radar:"radar", polarArea:"polarArea"/*, bubble:"bubble", scatter:"scatter"*/}
                        },
                        "chartBlockWidth" :{
                            "inputType" : "text",
                            "label" : "Largeur (en %)",
                            "rules" : {
                                "number" : true
                            },
                            values : sectionDyf.<?=$kunik ?>ParamsData.chartBlockWidth,
                            "class" : "form-control",
                        },
                        "chartBlockHeight" :{
                            "inputType" : "text",
                            "rules" : {
                                "number" : true
                            },
                            "label" : "Hauteur du graph",
                            "class" : "form-control",
                            values : sectionDyf.<?=$kunik ?>ParamsData.chartBlockHeight,
                        },
                        "chartBlockBackgroundColor" :{
                            "inputType" : "colorpicker",
                            "label" : "Couleur de fond",
                            values : sectionDyf.<?=$kunik ?>ParamsData.chartBlockBackgroundColor,
                            "class" : "form-control",
                        },
                        "chartBlockBorderRadius" :{
                            "inputType" : "text",
                            "rules" : {
                                "number" : true
                            },
                            "label" : "Rond du bordure",
                            "class" : "form-control",
                            values : sectionDyf.<?=$kunik ?>ParamsData.chartBlockBorderRadius,
                        },
                        "chartTitleText" : {
                            "label" : "Titre du chart",
                            "inputType" : "text",
                            "values" :  sectionDyf.<?php echo $kunik?>ParamsData.chartTitleText
                        },
                        "chartTitleColor" : {
                            "label" : "Couleur du titre du chart",
                            "inputType" : "colorpicker",
                            "values" :  sectionDyf.<?php echo $kunik?>ParamsData.chartTitleColor
                        },
                        "chartTitleSize" : {
                            "label" : "Taille du titre du chart",
                            "inputType" : "text",
                            "rules" : {
                                "number" : true
                            },
                            "values" :  sectionDyf.<?php echo $kunik?>ParamsData.chartTitleSize
                        },
                        "chartDataElementTypes" : {
                            "inputType" : "select",
                            "label" : "Type d'élément",
                            "values" : sectionDyf.<?=$kunik ?>ParamsData.chartDataElementTypes,
                            "class" : "form-control",
                            "options" : {
                                "organizations" : trad.organizations,
                                "events" : trad.events,
                                "projects" : trad.projects
                            }
                        },
                        "chartDataFields" :{
                            "inputType" : "select",
                            "label" : "Champs cible",
                            "values" : sectionDyf.<?=$kunik ?>ParamsData.chartDataFields,
                            "class" : "form-control",
                            "options" : fields<?= $kunik ?>
                        },
                        "chartDataLabel" : {
                            "label" : "Libellé du graph",
                            "inputType" : "text",
                            "values" :  sectionDyf.<?php echo $kunik?>ParamsData.chartLabel
                        },
                        "chartDataColorGradient" : {
                            "label" : "Ajouter une effet gradient",
                            "inputType" : "checkboxSimple",
                            "params" : {
                                "onText" : trad.yes,
                                "offText" : trad.no,
                                "onLabel" : trad.yes,
                                "offLabel" : trad.no
                            },
                            "checked" : false,
                            "values" :  sectionDyf.<?php echo $kunik?>ParamsData.chartDataColorGradient
                        },
                        "chartLegendDisplay" : {
                            "label" : "Afficher la légende",
                            "inputType" : "checkboxSimple",
                            "params" : {
                                "onText" : trad.yes,
                                "offText" : trad.no,
                                "onLabel" : trad.yes,
                                "offLabel" : trad.no
                            },
                            "checked" : true,
                            "values" :  sectionDyf.<?php echo $kunik?>ParamsData.chartAxeY
                        },
                        "chartLegendPosition" :{
                            "inputType" : "select",
                            "label" : "Position des légendes",
                            "values" : sectionDyf.<?=$kunik ?>ParamsData.chartLegendPosition,
                            "class" : "form-control",
                            "options" : {
                                "top" : "En Haut",
                                "bottom" : "En Bas",
                                "right" : "Droite",
                                "left" : "Gauche"
                            }
                        },
                        "chartLegendColor" :{
                            "inputType" : "colorpicker",
                            "label" : "Couleur de textes du légende",
                            "values" : sectionDyf.<?=$kunik ?>ParamsData.chartLegendColor,
                            "class" : "form-control",
                        },
                        "chartLegendSize" :{
                            "inputType" : "text",
                            "label" : "Taille de texte",
                            "rules" : {
                                "number" : true
                            },
                            "values" : sectionDyf.<?=$kunik ?>ParamsData.chartLegendSize,
                            "class" : "form-control",
                        },
                        "chartDataToShow" :{
                            "inputType" : "selectMultiple",
                            "isSelect2" : true,
                            "label" : "Données à Afficher",
                            "class" : "form-control",
                            "placeholder" : "Données à Afficher",
                            "options" : Object.keys(data<?= $kunik ?>).reduce((a, v) => ({ ...a, [v]: v}), {}),
                            "values" : sectionDyf.<?=$kunik ?>ParamsData.chartDataToShow
                        },
                        "chartAxeY" : {
                            "label" : "Afficher l'axe Y",
                            "inputType" : "checkboxSimple",
                            "params" : {
                                "onText" : trad.yes,
                                "offText" : trad.no,
                                "onLabel" : trad.yes,
                                "offLabel" : trad.no
                            },
                            "checked" : true,
                            "values" :  sectionDyf.<?php echo $kunik?>ParamsData.chartAxeY
                        },
                        "chartAxeX" : {
                            "label" : "Afficher l'axe X",
                            "inputType" : "checkboxSimple",
                            "params" : {
                                "onText" : trad.yes,
                                "offText" : trad.no,
                                "onLabel" : trad.yes,
                                "offLabel" : trad.no
                            },
                            "checked" : true,
                            "values" :  sectionDyf.<?php echo $kunik?>ParamsData.chartAxeX
                        },
                        "chartAxeColor" :{
                            "inputType" : "colorpicker",
                            "label" : "Couleur",
                            "values" : sectionDyf.<?=$kunik ?>ParamsData.chartAxeColor,
                            "class" : "form-control",
                        },
                        "chartDesignImage" :{
                            "inputType" : "checkboxSimple",
                            "label" : "Afficher pour Image ou Icon des données",
                            "params" : {
                                "onText" : trad.yes,
                                "offText" : trad.no,
                                "onLabel" : trad.yes,
                                "offLabel" : trad.no
                            },
                            "checked" : false,
                            "values" :  sectionDyf.<?php echo $kunik?>ParamsData.chartDesignImage
                        }
                    },
                    beforeBuild : function(){
                        uploadObj.set("cms","<?php echo $blockKey ?>");
                    },
                    save : function (data) {
                        tplCtx.value = {};
                        $.each( sectionDyf.<?=$kunik ?>Params.jsonSchema.properties , function(k,val) {

                            tplCtx.value[k] = $("#"+k).val();

                        });

                        if(typeof tplCtx.value == "undefined")
                            toastr.error('value cannot be empty!');
                        else {
                            dataHelper.path2Value( tplCtx, function(params) {
                                    dyFObj.commonAfterSave(params,function(){
                                        toastr.success("Élément bien ajouter");
                                        $("#ajax-modal").modal('hide');
                                        dyFObj.closeForm();
                                        urlCtrl.loadByHash(location.hash);
                                    });
                            });
                        }
                    }
                }
            }
        }

        search<?= $kunik ?>.search.init(search<?= $kunik ?>);

    $(".edit<?=$kunik ?>Params").off().on("click",function() {
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = "allToRoot";
        dyFObj.openForm( sectionDyf.<?=$kunik ?>Params,null,sectionDyf.<?=$kunik ?>ParamsData);
        alignInput2(sectionDyf.<?=$kunik ?>Params.jsonSchema.properties ,"chartBlock",4,6,null,null,"Propriété générale du chart","#4AB5A1","");
        alignInput2(sectionDyf.<?=$kunik ?>Params.jsonSchema.properties ,"chartTitle",4,6,null,null,"Propriété du titre du graph","#4AB5A1","");
        alignInput2(sectionDyf.<?=$kunik ?>Params.jsonSchema.properties ,"chartLegend",4,6,null,null,"Propriété du legend","#4AB5A1","");
        alignInput2(sectionDyf.<?=$kunik ?>Params.jsonSchema.properties ,"chartData",4,6,null,null,"Propriété de données","#4AB5A1","");
        alignInput2(sectionDyf.<?=$kunik ?>Params.jsonSchema.properties ,"chartAxe",4,6,null,null,"Propriété d'axes","#4AB5A1","");

    });
}, 500)

    /*[
        'rgba(255, 99, 132, 0.4)',
        'rgba(54, 162, 235, 0.4)',
        'rgba(255, 206, 86, 0.4)',
        'rgba(75, 192, 192, 0.4)',
        'rgba(153, 102, 255, 0.4)',
        'rgba(255, 159, 64, 0.4)',
        'rgba(75, 192, 192, 0.4)',
    ]*/

</script>
