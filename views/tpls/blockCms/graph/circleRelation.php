<?php
$keyTpl     = "graph";
$paramsData = [
    "dataSource" => [$this->costum["contextId"] => ["name" => $this->costum["contextSlug"], "type" => $this->costum["contextType"]]],
    "tags" => "",
    "depth" => 1,
    "isSearch" => false,
    "isTagFilter" => false,
    "tagDirect" => true,
    "circlerelations" => [],
    "positions" => []
];
if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if ( isset($blockCms[$e])) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
foreach($paramsData["circlerelations"] as $index => $value){
  $paramsData["circlerelations"][$index] = ["source" => strtolower($paramsData["circlerelations"][$index]["source"]), "target" => strtolower($paramsData["circlerelations"][$index]["target"])];
}
?>


<?php
if (isset($this->costum["contextType"]) && isset($this->costum["contextId"])) {
  $graphAssets = [
    '/plugins/d3/d3.v6.min.js', '/js/graph.js', '/css/graph.css'
  ];
  HtmlHelper::registerCssAndScriptsFiles(
    $graphAssets,
    Yii::app()->request->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl()
  );
}
?>
<script>
  jQuery(document).ready(function() {
    contextData = <?php echo json_encode($el); ?>;
    var contextId = <?php echo json_encode((string) $el["_id"]); ?>;
    var contextType = <?php echo json_encode($this->costum["contextType"]); ?>;
    var contextName = <?php echo json_encode($el["name"]); ?>;
    contextData.id = <?php echo json_encode((string) $el["_id"]); ?>;
  });
</script>


<style>
  #graph-container-<?= $kunik ?>{
    height: 100%;
    width: 100%;
    overflow: hidden;
    background-color: transparent !important;
  }

  .super-cms .graph-panel{
    background-color: transparent !important;
    width: 100%;
    height: 100%;
  }
</style>
<?php if($paramsData["isSearch"] == "true"){ ?>
    <div id="search-container-<?= $kunik ?>" class="col-xs-12 searchObjCSS">
    </div>
<?php } ?>

<div id="graph-container-<?= $kunik ?>" class="graph-panel">

</div>

<script>
    var epsilon = 1;
    var rawTags = "<?= $paramsData["tags"] ?>";
    var authorizedTags = []
    if(rawTags.trim() != ""){
      authorizedTags = rawTags.split(',');
    }
    var l<?= $kunik ?> = {
    container: "#search-container-<?= $kunik ?>",
    loadEvent: {
      default: "graph"
    },
    defaults: {
      types: ["organizations"],
      indexStep: 100
    },
    results: {
      dom: "#loader-container"
    },
    graph: {
      dom: "#graph-container-<?= $kunik ?>",
      authorizedTags: authorizedTags,
      defaultGraph: "circlerelation",
      circlerelations: <?= json_encode($paramsData["circlerelations"]) ?>,
      initPosition: <?= json_encode($paramsData["positions"]) ?>
    },
    filters: {
      text: true,
      
    },
    header: {
      options : {
		  }
    }
  };
  // l<?= $kunik ?>.defaults.forced = {
  //   filters: {
  //       "$or" : {}
  //   }
  //   }
  // var dataSource<?= $kunik ?> = <?= json_encode($paramsData["dataSource"]) ?>;
  // if(Object.keys(dataSource<?= $kunik ?>).length > 0){
  //   l<?= $kunik ?>.defaults["notSourceKey"] = true;
  //   for (const [id, value] of Object.entries(dataSource<?= $kunik ?>)) {
  //     l<?= $kunik ?>.defaults.forced.filters["$or"]["links.memberOf."+id] = {"$exists" : true};    
  //   }
  // }
    if(!window.initedGraph){
      window.initedGraph = [];
    }
    var p<?= $kunik ?> = {};
    function initGraph<?= $kunik ?>() {
        p<?= $kunik ?> = searchObj.init(l<?= $kunik ?>);
        p<?= $kunik ?>.graph.init(p<?= $kunik ?>);
        p<?= $kunik ?>.search.init(p<?= $kunik ?>);
        p<?= $kunik ?>.graph.graph.setBeforeDrag(() => {
          p<?= $kunik ?>.graph.graph.setDraggable("w" == localStorage.getItem("previewMode"));
        });
        p<?= $kunik ?>.graph.graph.setOnDragEnd(() => {
          const positions = {}
          p<?= $kunik ?>.graph.graph.rootG.selectAll("g.divide").each((d) => {
            positions[d.data[0]] = {
              x: d.x,
              y: d.y
            }
          })
          tplCtx = {};
          tplCtx.id = "<?= $blockKey ?>",
          tplCtx.collection = "cms";
          tplCtx.path = "allToRoot";
          tplCtx.value = {
            id: "<?= $blockKey ?>",
            positions: positions
          };
          dataHelper.path2Value( tplCtx, function(params) {
            dyFObj.commonAfterSave(params,function(){
              toastr.success("Position saved");
            });
          } );
        });
        setTimeout(() => {
            p<?= $kunik ?>.graph.graph.initZoom();
        }, 500);
    }
    setTimeout(() => {
      if($("#graph-container-<?= $kunik ?>").is(":visible")){
        window.initedGraph.push('<?= $kunik ?>');
        initGraph<?= $kunik ?>();
      }
    },200)
</script>
<script type="text/javascript">
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;
  jQuery(document).ready(function() {
    const filtersData = {};
    if(costum && costum.lists){
      for (const key of Object.keys(costum.lists)) {
        filtersData[key] = key;
      }
    }
    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {
        "title" : "Configurer votre section",
        "description" : "Personnaliser votre section",
        "icon" : "fa-cog",
        "properties" : {
          "dataSource" : {
            "label" : "Organisation source",
             "inputType": "finder",
             "initType": ["organizations"],
            "openSearch" :true,
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.dataSource
          },
          "isSearch" : {
              "label" : "Ajouter une fonction recherche",
              "inputType" : "checkboxSimple",
                "params" : {
                    "onText" : "Oui",
                    "offText" : "Non",
                    "onLabel" : "Recherche activé",
                    "offLabel" : "Recherche desactivé",
                    "labelText" : "Ajouter une fonction recherche"
                },
                "checked" : false
          },
          "tags" : {
              "label" : "Les tags à visualiser",
              "inputType": "tags"
          },          
          "circlerelations" : {
              "label": "Les relations (utiliser les tags à visualiser)",
              "inputType" : "lists",
              entries:{
                source:{
                  type:"text",
                  label : "source",
                  class:"col-md-5"
                },
                target:{
                  label: "target",
                  type: "text",
                  class: "col-md-5"
                }
            }
          }
        },
        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function () {
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
            tplCtx.value[k] = $("#"+k).val();
            if (k == "parent") {
              tplCtx.value[k] = formData.parent;
            }
            if (k == "dataSource") {
              tplCtx.value[k] = formData.dataSource;
            }
            if (k == "circlerelations"){
              tplCtx.value[k] = formData.circlerelations;
            }
          });
          mylog.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
              dataHelper.path2Value( tplCtx, function(params) {
                dyFObj.commonAfterSave(params,function(){
                  toastr.success("Élément bien ajouter");
                  $("#ajax-modal").modal('hide');
                  urlCtrl.loadByHash(location.hash);
                });
              } );
          }

        }
      }
    };
    mylog.log("sectiondyfff",sectionDyf);
    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });
  });
</script>