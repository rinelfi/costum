<?php
$keyTpl     = "graph";
$paramsData = [
    "dataSource" => [$this->costum["contextId"] => ["name" => $this->costum["contextSlug"], "type" => $this->costum["contextType"]]],
    "tags" => "",
    "graphType" => "circle",
    "depth" => 1,
    "isSearch" => false,
    "isTagFilter" => false,
    "tagDirect" => true,
    "filtersParent" => [],
    "filtersChild" => []
];
if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if ( isset($blockCms[$e])) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>


<?php
if (isset($this->costum["contextType"]) && isset($this->costum["contextId"])) {
  $graphAssets = [
    '/plugins/d3/d3.v6.min.js', '/js/graph.js', '/css/graph.css'
  ];
  HtmlHelper::registerCssAndScriptsFiles(
    $graphAssets,
    Yii::app()->request->baseUrl . Yii::app()->getModule("graph")->getAssetsUrl()
  );
}
?>
<script>
  jQuery(document).ready(function() {
    contextData = <?php echo json_encode($el); ?>;
    var contextId = <?php echo json_encode((string) $el["_id"]); ?>;
    var contextType = <?php echo json_encode($this->costum["contextType"]); ?>;
    var contextName = <?php echo json_encode($el["name"]); ?>;
    contextData.id = <?php echo json_encode((string) $el["_id"]); ?>;
  });
</script>


<style>
  #graph-container-<?= $kunik ?>{
    height: 100%;
    width: 100%;
    overflow: hidden;
  }

  .super-cms .graph-panel{
    background-color: transparent !important;
    width: 100%;
    height: 100%;
  }
</style>
<?php if($paramsData["isSearch"] == "true"){ ?>
    <div id="search-container-<?= $kunik ?>" class="col-xs-12 searchObjCSS">
    </div>
<?php } ?>

<div id="graph-container-<?= $kunik ?>" class="graph-panel">

</div>

<script>
    var rawTags = "<?= $paramsData["tags"] ?>";
    var authorizedTags = []
    if(rawTags.trim() != ""){
      authorizedTags = rawTags.split(',');
    }
    <?php 
      foreach($paramsData["filtersParent"] as $filtersParent){
        ?> 
        if(costum.lists && costum.lists.<?= $filtersParent ?>){
        <?php  
          foreach($paramsData["filtersChild"] as $filtersChild){
        ?>

          if(costum.lists.<?= $filtersParent ?>["<?= $filtersChild ?>"]){
            authorizedTags.push(...Object.keys(costum.lists.<?= $filtersParent ?>["<?= $filtersChild ?>"]));
          }
          <?php 
          }
        ?>
        }
        <?php
      }
    ?>
    var l<?= $kunik ?> = {
    container: "#search-container-<?= $kunik ?>",
    loadEvent: {
      default: "graph"
    },
    defaults: {
      types: ["organizations"],
      indexStep: 100
    },
    results: {
      dom: "#loader-container"
    },
    graph: {
      dom: "#graph-container-<?= $kunik ?>",
      authorizedTags: authorizedTags,
      defaultGraph: "<?= $paramsData["graphType"] ?>",
      mindmapDepth: <?= $paramsData["depth"] ?>,
    },
    filters: {
      text: true,
      
    },
    header: {
      options : {
		  }
    }
  };
  // l<?= $kunik ?>.defaults.forced = {
  //   filters: {
  //       "$or" : {}
  //   }
  //   }
  // var dataSource<?= $kunik ?> = <?= json_encode($paramsData["dataSource"]) ?>;
  // if(Object.keys(dataSource<?= $kunik ?>).length > 0){
  //   l<?= $kunik ?>.defaults["notSourceKey"] = true;
  //   for (const [id, value] of Object.entries(dataSource<?= $kunik ?>)) {
  //     l<?= $kunik ?>.defaults.forced.filters["$or"]["links.memberOf."+id] = {"$exists" : true};    
  //   }
  // }
    if(!window.initedGraph){
      window.initedGraph = [];
    }
    var p<?= $kunik ?> = {};
    function initGraph<?= $kunik ?>() {
        p<?= $kunik ?> = searchObj.init(l<?= $kunik ?>);
        p<?= $kunik ?>.graph.init(p<?= $kunik ?>);
        p<?= $kunik ?>.search.init(p<?= $kunik ?>);
        setTimeout(() => {
            p<?= $kunik ?>.graph.graph.initZoom();
        }, 500);
    }
    setTimeout(() => {
      if($("#graph-container-<?= $kunik ?>").is(":visible")){
        window.initedGraph.push('<?= $kunik ?>');
        initGraph<?= $kunik ?>();
      }
    },200)
</script>
<script type="text/javascript">
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;
  jQuery(document).ready(function() {
    const filtersData = {};
    if(costum && costum.lists){
      for (const key of Object.keys(costum.lists)) {
        filtersData[key] = key;
      }
    }
    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {
        "title" : "Configurer votre section",
        "description" : "Personnaliser votre section",
        "icon" : "fa-cog",
        "properties" : {
          "graphType" : {
            "label" : "Choisir le graphe que vous voulez",
             "inputType": "select",
              options : {
                circle : "Circle Graph",
                mindmap : "Mindmap Graph",
                relation : "Relation Graph",
                network : "Network Graph",
                circlerelation : "Circle Relation Graph"
              }, 
              values :  sectionDyf.<?php echo $kunik ?>ParamsData.graphType
          },
          "depth" : {
            "label" : "Profondeur max pour le collapse",
             "inputType": "quantity",
              values :  sectionDyf.<?php echo $kunik ?>ParamsData.depth
          },
          "dataSource" : {
            "label" : "Organisation source",
             "inputType": "finder",
             "initType": ["organizations"],
            "openSearch" :true,
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.dataSource
          },
          "isSearch" : {
              "label" : "Ajouter une fonction recherche",
              "inputType" : "checkboxSimple",
                "params" : {
                    "onText" : "Oui",
                    "offText" : "Non",
                    "onLabel" : "Recherche activé",
                    "offLabel" : "Recherche desactivé",
                    "labelText" : "Ajouter une fonction recherche"
                },
                "checked" : false
          },
          "isTagFilter" : {
              "label" : "Ajouter une fonction filtre par tags",
              "inputType" : "checkboxSimple",
                "params" : {
                    "onText" : "Oui",
                    "offText" : "Non",
                    "onLabel" : "Filtre activé",
                    "offLabel" : "Filtre desactivé",
                    "labelText" : "Ajouter une fonction filtre par tags"
                },
                "checked" : false
          },
          "filtersParent": {
              "label" : "Choisir un ou plusieurs donnée :",
              "class" : "form-control <?php echo $kunik ?>",
              "inputType" : "select",
              select2 : {
                  multiple : true
              },
              "options" : filtersData,
              "value": sectionDyf.<?php echo $kunik ?>ParamsData.filtersParent
          },
          "filtersChild": {
              "label" : "Choisir un ou plusieurs donnée :",
              "class" : "form-control <?php echo $kunik ?>",
              "inputType" : "select",
              select2 : {
                  multiple : true
              },
              "options" : [],
              "value": sectionDyf.<?php echo $kunik ?>ParamsData.filtersChild
          },
          "tags" : {
              "label" : "Les tags à visualiser",
              "inputType": "tags"
          }
        },
        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        afterBuild : function() {
            if(sectionDyf.<?php echo $kunik ?>ParamsData.filtersParent.length > 0){
              updateInputList(sectionDyf.<?php echo $kunik ?>ParamsData.filtersParent);
              $("#filtersChild.<?php echo $kunik ?>").val(sectionDyf.<?php echo $kunik ?>ParamsData.filtersChild).trigger('change');
            }
        },
        save : function () {
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
            tplCtx.value[k] = $("#"+k).val();
            if (k == "parent") {
              tplCtx.value[k] = formData.parent;
            }
            if (k == "dataSource") {
              tplCtx.value[k] = formData.dataSource;
            }
          });
          mylog.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
              dataHelper.path2Value( tplCtx, function(params) {
                dyFObj.commonAfterSave(params,function(){
                  toastr.success("Élément bien ajouter");
                  $("#ajax-modal").modal('hide');
                  urlCtrl.loadByHash(location.hash);
                });
              } );
          }

        }
      }
    };
    $(document).on("change", "#filtersParent.<?= $kunik ?>", function(){
      const val = $(this).val();
      $("#filtersChild.<?php echo $kunik ?>").empty();
        if(val){
          updateInputList(val.toString().split(","));
        }
    });
    function updateInputList(values) {
      if(!values) return;
      $("#filtersChild.<?php echo $kunik ?>").empty();
      for (const value of values) {
        if(!costum || !costum.lists || !costum.lists[value]) continue;
        const keys = Object.keys(costum.lists[value]);
        for (let i = 0; i < keys.length; i++) {
          const tag = keys[i];
          $("#filtersChild.<?php echo $kunik ?>").append('<option value="'+tag+'">'+tag+'</option>'); 
        }
      }
    }
    function switchFiltre(isDirect) {
      if(isDirect){
        $("#ajaxFormModal .filtersParentselectMultiple").hide();
        $("#ajaxFormModal .filtersChildselect").hide();
        $("#ajaxFormModal .tagstags").show();
      }else{
        $("#ajaxFormModal .filtersParentselectMultiple").show();
        $("#ajaxFormModal .filtersChildselect").show();
        $("#ajaxFormModal .tagstags").hide();
      }
    }
    mylog.log("sectiondyfff",sectionDyf);
    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
      setTimeout(() => {
        onload();
        $("select[name='graphType']").change(onload);
      },1200)
    });
    function onload() {
      if($("select[name='graphType']").val() == "mindmap"){
        $('.depthquantity').show();
      }else{
        $('.depthquantity').hide();
      }
      if($("select[name='graphType']").val() == "circle" || $("select[name='graphType']").val() == "relation"){
        $('.tagstags').show();
      }else{
        $('.tagstags').hide();
      }
    }
  });
</script>