<?php
$keyTpl = "menuFilters";

$paramsData=[
    "titleBlock" => "AFFICHE",
    "cardBackground" => "#1A2660",
    "imgRadius" => "10"
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>

<?php
$assetsUrl = Yii::app()->getModule('costum')->assetsUrl;

HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/swiper/swiper-bundle.min.css","/js/blockcms/swiper/swiper-bundle.min.js"], $assetsUrl);
?>

<!-- ****************get image uploaded************** -->
<?php
$blockKey = (string)$blockCms["_id"];
$initFiles = Document::getListDocumentsWhere(
    array(
        "id"=> $blockKey,
        "type"=>'cms',
        "subKey"=>"notBackground"
    ), "image"
);
$arrayImg = [];
foreach ($initFiles as $key => $value) {
    $arrayImg[]= $value["imagePath"];
}
?>
<!-- ****************end get image uploaded************** -->

<style type="text/css">

    /*Card*/
    .carousel-card-gallery {
        margin-top: 50px;
        width: 90%;
        margin: 50px auto;
        max-width: 1100px;
    }

    #active-filters{
      position: absolute;
      bottom: 10px;
      left: 20px;
      border: 3px solid #49CBE3;
      border-radius: 50%;
      padding: 1px;
      font-size: 14px;
    }

    .carousel-card {
        font-weight: bolder !important;
        font-size: 25px;
        position: relative;
        display: block;
        width: 100%;
        height: 200px;
        margin: 1rem auto 2rem 1rem;
        border-radius: <?php echo $paramsData["imgRadius"]; ?>px;
        box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.3);
        overflow: hidden;
        transition: all .4s ease;
        background-color: <?php echo $paramsData["cardBackground"] ?>;
    }

    @media screen and (min-width: 768px) {
        .carousel-card {
            height: 200px;
        }
    }

    @media screen and (min-width: 768px) {
        .carousel-card {
            display: inline-block;
        }
    }

    .carousel-card-overlay {
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        z-index: 0;
        text-align: center;
        overflow-y: auto;
        overflow-x: hidden;
        background-color: <?php echo $paramsData["cardBackground"] ?>;
    }
    .carousel-card-overlay:after {
        content: '';
        width: 100%;
        height: 100%;
        background-color: transparent;
        position: absolute;
        top: 0;
        z-index: -10;
        left: 0;
        transition: all .3s ease;
    }

    .carousel-card-title {
        margin-bottom: 5px;
        margin-top: 20px;
        font-size: 18px;
        font-weight: bolder;
        color: white;
        text-align: center;
        letter-spacing: 2px;
        transition: all 0.3s cubic-bezier(0.3, 0, 0, 1.3);
        padding-left: 2px;
        padding-right: 2px;
    }

    .carousel-card-logo {
        max-height: 50px !important;
        position: relative;
        margin-top: 30px;
        border-radius: 10px;
        transition: all 0.3s cubic-bezier(0.3, 0, 0, 1.3);
    }

    .carousel-card-icon {
        max-height: 50px !important;
        position: relative;
        margin-top: 30px;
        border-radius: 10px;
        font-size: 45pt;
        color: #49CBE3;
        transition: all 0.3s cubic-bezier(0.3, 0, 0, 1.3);
    }

    .swiper-pagination-bullet {
        margin-top: 2rem;
        background-color: transparent;
        display: inline-block;
        width: 2rem;
        height: 2rem;
        border: 3px solid white;
        opacity: .9;
    }

    .swiper-pagination-bullet-active {
      border: 3px solid #49CBE3;
      opacity: 1;
    }
</style>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 container<?php echo $kunik ?> ">
    <h3 class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="titleBlock"><?php echo $paramsData["titleBlock"]; ?></h3>
    <br>
    <div class="swiper-container">
        <div class="swiper-wrapper"></div>
        <br><br>
        <div class="swiper-pagination"></div>
        <!--div class="swiper-button-next"></div>
        <div class="swiper-button-prev"></div-->
    </div>
</div>

<script type="text/javascript">
    var dyf<?=$kunik?>={
        "onload" : {
            "actions" : {
                "setTitle" : "Ajouter une <?=$kunik?>",
                "html" : {
                    "infocustom" : "<br/>Remplir le formulaire"
                },
                "presetValue" : {
                    "type" : "<?=$kunik?>"
                },
                "hide" : {
                    "breadcrumbcustom" : 1,
                    "parentfinder" : 1,
                }
            }
        }
    };
    dyf<?=$kunik?>.afterSave = function(data){
        dyFObj.commonAfterSave(data,function(){
            mylog.log("data", data);
            urlCtrl.loadByHash(location.hash);
        });
    };


    sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {

        sectionDyf.<?php echo $kunik?>Params = {
            "jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
                "icon" : "fa-cog",
                "properties" : {
                    "imgRadius" : {
                        label : "<?php echo Yii::t('cms', 'Radius of the card')?> (px)",
                        values :  sectionDyf.<?php echo $kunik?>ParamsData.imgRadius
                    }/*,
                    "themes" : {
                        inputType : "lists",
                        label : "ajouter de thèmes",
                        entries: {
                        	icon: {
                                type:"select",
                                label:"Icon",
                                placeholder: "Icon de réseaux sociaux",
                                options: fontAwesome,
                            },
                            link: {
                                type:"colorpicker",
                                label:"Lien",
                                placeholder: "couleur"
                            }
                        }
                    }*/
                },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function () {
                    tplCtx.value = {};

                    $.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) {
                        tplCtx.value[k] = $("#"+k).val();
                    });

                    mylog.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.commonAfterSave(params,function(){
                                toastr.success("Élément bien ajouter");
                                $("#ajax-modal").modal('hide');
                                urlCtrl.loadByHash(location.hash);
                            });
                        } );
                    }
                }
            }
        };

        mylog.log("paramsData",sectionDyf);

        $(".edit<?php echo $kunik?>Params").off().on("click",function() {
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
        });

        /**************************** list **************************/
        var isInterfaceAdmin = false;

        <?php
        if(Authorisation::isInterfaceAdmin()){ ?>
        isInterfaceAdmin = true
        <?php } ?>

        getTagsFilters<?= $kunik ?>();

        function getTagsFilters<?= $kunik ?>(){
            var html = "";
            $.each(costum.lists.themes, function(index, value) {
                html += '<div class="swiper-slide" >' +
                    '<a href="javascript:;" class="carousel-card filterTag"  data-value="'+value+'" data-field="tags" data-type="filters">'+
                    '<div class="carousel-card-overlay text-center co-scroll">' ;
                if (!costum.lists.imgThematic[value].includes("/"))
                    html +='<i class="carousel-card-icon fa fa-'+costum.lists.imgThematic[value]+'"></i>';
                else
                    html +='<img class="carousel-card-logo" src="<?php echo Yii::app()->getModule('costum')->assetsUrl ?>'+costum.lists.imgThematic[value]+'"> ';

                html +='<div class="carousel-card-title">'+value+'</div>';
                if(typeof searchObj.search.obj.filters["tags"]!="undefined" && searchObj.search.obj.filters["tags"].includes(value)){
                            html += '<i id="active-filters" class="fa fa-check text-white"></i>';
                          }
                    html +='<div class="col-xl-12">';
                    html +='</div>' +
                           '</div>';

                    html +='</a>' +
                            '</div>';

            });

            $('.container<?php echo $kunik ?> .swiper-wrapper').html(html);

            var swiper = new Swiper(".container<?php echo $kunik ?> .swiper-container", {
                slidesPerView: 1,
                spaceBetween: 0,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
                /*navigation: { nextEl: '.swiper-button-next', prevEl: '.swiper-button-prev' },*/
                keyboard: {
                    enabled: true,
                },
                breakpoints: {
                    400: {
                        slidesPerView: 2,
                        spaceBetween: 10,
                    },
                    640: {
                        slidesPerView: 2,
                        spaceBetween: 10,
                    },
                    768: {
                        slidesPerView: 3,
                        spaceBetween: 10,
                    },
                    1024: {
                        slidesPerView: 5,
                        spaceBetween: 20,
                    },
                    1200: {
                      slidesPerView: 6,
                      spaceBetween: 20,
                    },
                    1400: {
                      slidesPerView: 7,
                      spaceBetween: 20,
                    }
                }
            });

            coInterface.bindLBHLinks();
        }

        $(".filterTag").click(function(value) {
          if(typeof searchObj.search.obj.filters[$(this).data("field")]!="undefined" && searchObj.search.obj.filters[$(this).data("field")].includes($(this).data("value"))){
            searchObj.filters.manage.removeActive(searchObj, $(this), true);
          }else{
            if(typeof $(this).data("field") != "undefined"){
  						if(typeof searchObj.search.obj.filters == "undefined" ){
                searchObj.search.obj.filters = {};
              }
  						if(typeof searchObj.search.obj.filters[$(this).data("field")] == "undefined" ){
  							searchObj.search.obj.filters[$(this).data("field")] = [];
              }
  	          searchObj.search.obj.filters[$(this).data("field")].push($(this).data("value"));
  	        }
            searchObj.filters.manage.addActive(searchObj, $(this), true);

          }

          urlCtrl.loadByHash(location.hash);
        });
    });

</script>
