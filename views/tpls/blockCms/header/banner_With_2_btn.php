<?php 
$keyTpl = "banner_With_2_btn";
$paramsData = [
	"title" => "   <font color=\"#ffffff\" face=\"OpenSans-Bold\" style=\"font-size: 50px;line-height: 50px;\"> Le monde change,</font><div><font color=\"#ffffff\" face=\"NotoSans-Regular\" style=\"font-size: 31px;line-height: 50px;\">connecter vous au mapping by Technopole </font></div>   ",
	"buttonLink"        => "",
    "buttonLabel"       => "Déposer un projet",
    "buttonTypeLink"              => ""
];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
}  
?>
<style type="text/css">
	#menuTopLeft a {
		text-decoration: none;

	}
	.header_<?= $kunik?> .content{
		margin-top: 15%;
		margin-left: 7%;
	}
	.header_<?= $kunik?> .content .bouton:hover {
		background: transparent;
		color: white !important;
		border: 3px solid #F0FCFF;
	}
	.header_<?= $kunik?> .content p {
		margin-top: 5%;
	}
	.header_<?= $kunik?> .content .bouton{
		background-color: #fff;
		padding: 1% 5% 1% 5%;
		border-radius: 30px;
		font-size: 20px;
		margin-top: 5%;
		margin-bottom: 15%;
		font-weight: bold;
	}
	.header_<?= $kunik?> .content button{
		margin-left : 10px;
	}
	.btn-<?= $kunik?> {
		margin-top : 50px;
	}

	
	@media (max-width: 978px) {
		#menuTopLeft .menu-btn-top:before {
			font-size: 18px;

		}
		.header_<?= $kunik?> .content{
			margin-top: 30%;
		}
		
		.header_<?= $kunik?> .content .bouton{
			width: 39%;
			border-radius: 30px;
			height: 40px;
			margin-top: 7%;
			font-weight: 600;
			padding: 1% 3% 1% 3%;
			margin-bottom: 10%;
			font-size: 18px !important;
		}
	}
	.select2-container-multi .select2-choices{
		border : none;
	}
</style>
<div class="header_<?= $kunik?> search_<?= $kunik?>" >
	<div class="content col-lg-5 col-md-7" >
		<div class="sp-text" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"  > <?= $paramsData["title"]?> </div>
		<div class="btn-<?= $kunik?>">
			<a href="javascript:" id="addProject<?= $kunik?>" class=" btn bouton">
				<?= $paramsData["buttonLabel"]?> 
			</a>
			<a href="javascript:;" id="joinBtn<?= $kunik ?>" class="btn bouton" style="text-decoration : none;"> 
				<?php echo Yii::t('cms', 'Innovation actors')?>   
			</a>
		</div>
		
	</div>
</div>
<script type="text/javascript">	
	sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
	jQuery(document).ready(function() {
		$("#btn-search-<?= $kunik?>").click(function(){
			$( "#search-form<?= $kunik?>" ).submit();
			var str = $("#otherInput").val();
			location.hash = "#search?text="+str;
			urlCtrl.loadByHash(location.hash);
		});
		$( "#search-form<?= $kunik?>" ).submit(function( event ) {
			var str = $("#otherInput").val();
			location.hash = "#search?text="+str;
			urlCtrl.loadByHash(location.hash);
			event.preventDefault();
		});
		sectionDyf.<?php echo $kunik?>Params = {
			"jsonSchema" : {    
				"title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
		        "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
				"icon" : "fa-cog",
				"properties" : {					
					"buttonLabel" : {
						"label" : "<?php echo Yii::t('cms', 'Button label')?>",
						inputType : "text",
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonLabel
					},
					"buttonTypeLink":{ 
						"label" : "<?php echo Yii::t('cms', 'Internal or external link')?> ",
						inputType : "select",
						options : {              
						"lbh " : "<?php echo Yii::t('cms', 'Internal')?> : <?php echo Yii::t('cms', 'full page')?>",
						"lbh-preview-element " : "<?php echo Yii::t('cms', 'Internal')?> : <?php echo Yii::t('cms', 'preview')?>",
						" " : "<?php echo Yii::t('cms', 'External')?>",
						},
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonTypeLink
					},
					"buttonLink" : {
						"label" : "<?php echo Yii::t('cms', 'Button link')?>",
						inputType : "text",
						values :  sectionDyf.<?php echo $kunik ?>ParamsData.buttonLink
					},
				},				
				beforeBuild : function(){
					uploadObj.set("cms","<?php echo $blockKey ?>");
				},
				save : function () {  
					tplCtx.value = {};

					$.each( sectionDyf.<?php echo $kunik?>Params.jsonSchema.properties , function(k,val) { 
						tplCtx.value[k] = $("#"+k).val();
					});

					mylog.log("save tplCtx",tplCtx);

					if(typeof tplCtx.value == "undefined")
						toastr.error('value cannot be empty!');
					else {
						dataHelper.path2Value( tplCtx, function(params) {
							dyFObj.commonAfterSave(params,function(){
                                toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
								$("#ajax-modal").modal('hide');
								urlCtrl.loadByHash(location.hash);
							});
						} );
					}
					
				}
			}
		};
		$(".edit<?php echo $kunik?>Params").off().on("click",function() {  
			tplCtx.id = $(this).data("id");
			tplCtx.collection = $(this).data("collection");
			tplCtx.path = "allToRoot";
			dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
			alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"button",6,6,null,null,"<?php echo Yii::t('cms', 'Button property')?>","#e45858","");
		});
		$("#joinBtn<?= $kunik ?>").off().click(function(){            
            dyFObj.openForm('organization',null,{ type: "GovernmentOrganization",role: "admin"});
		});
		$("#addProject<?= $kunik?>").off().click(function(){			
            dyFObj.openForm('project');
		});

	});
</script>
