<?php
    $current_date = $startDate;
    $dayCount = 0;
    $label = $type == "high" ? "Marée haute":"Marée basse";
    $ACTIVE_TIME = "h00";
?>

<tr class="wave-tr-tide">
    <td colspan="4" class="data-title"><span><?= $label ?></span></td>
    <?php
        while(strtotime($current_date) <= strtotime($endDate)){
            $waveData = isset($data[$current_date]["data"][$ACTIVE_TIME]["wave"])?
                        $data[$current_date]["data"][$ACTIVE_TIME]["wave"]:NULL;
            $tideData = isset($data[$current_date]["data"][$ACTIVE_TIME]["tide"][$type])?
                        $data[$current_date]["data"][$ACTIVE_TIME]["tide"][$type]:NULL;
            $classNames = "";
            foreach(Meteolamer::$TIMES as $time)
                $classNames .= " wave-td-data-day".$dayCount."-h".$time;
    ?>
        <td 
            colspan="4" 
            class="text-center wave-td-data <?= $classNames ?>"
            data-day="<?= $dayCount ?>" 
            data-time="<?= $ACTIVE_TIME ?>"
            data-map="<?= ($waveData?$waveData["map"]:"") ?>"
            data-imap="<?= ($waveData?$waveData["imap"]:"") ?>"
            data-date="<?= strtotime($current_date) ?>"
        >
            <p><?= ($tideData?$tideData[0]["time"]:"") ?></p>
            <p><?= ($tideData?$tideData[1]["time"]:"") ?></p>
        </td>
    <?php
            $current_date = date("Y-m-d", strtotime($current_date." +1 day"));
            $dayCount++;
        }
    ?>
</tr>