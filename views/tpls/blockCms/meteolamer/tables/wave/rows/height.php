<?php
    $current_date = $startDate;
    $dayCount = 0;
    $ACTIVE_TIMES = ["h06", "h18"];
?>

<tr class="wave-tr-hsig">
    <td colspan="4" class="data-title"><span>Hsig (m)</span></td>
    <?php
        while(strtotime($current_date) <= strtotime($endDate)){
            foreach($ACTIVE_TIMES as $time){
                $waveData = isset($data[$current_date]["data"][$time]["wave"])?
                            $data[$current_date]["data"][$time]["wave"]:NULL;
                $className = "wave-td-data-day".$dayCount."-".$time;
                $className .= " wave-td-data-day".$dayCount."-".(($time=="h06")?"h00":"h12");
    ?>
        <td
            colspan="2"
            class="wave-td-data <?= $className ?>"
            data-day="<?= $dayCount ?>"
            data-time="<?= $time ?>"
            data-map="<?= ($waveData ? $waveData["map"]:"") ?>"
            data-imap="<?= ($waveData ? $waveData["imap"]:"") ?>"
            data-date="<?= strtotime($current_date) ?>"
        >
            <span>
                <?= ($waveData ? number_format(round($waveData["height"], 1), 1):"") ?>
            </span>
        </td>
    <?php
            }
            $current_date = date("Y-m-d", strtotime($current_date." +1 day"));
            $dayCount++;
        }
    ?>
</tr>