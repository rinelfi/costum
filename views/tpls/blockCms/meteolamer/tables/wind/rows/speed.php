<?php
    $current_date = $startDate;
    $dayCount = 0;
    $ACTIVE_TIMES = ["h06", "h18"];

    if(!function_exists("kmTokn")){
        function kmTokn($value){
            return $value / 1.852;
        }
    }
?>
<tr class="wind-tr-speed-<?= $unit ?>">
    <td colspan="4" class="data-title"><span>Vent(<?= $unit ?>)</span></td>
    <?php
        while(strtotime($current_date) <= strtotime($endDate)){ 
            foreach($ACTIVE_TIMES as $time){
                $windData = isset($data[$current_date]["data"][$time]["wind"])?
                        $data[$current_date]["data"][$time]["wind"]:NULL;
                $className = "wind-td-data-day".$dayCount."-".$time;
                $className .= " wind-td-data-day".$dayCount."-".(($time=="h06")?"h00":"h12");
    ?>
        <td
            colspan="2" 
            class="wind-td-data <?= $className ?>" 
            data-day="<?=$dayCount?>" 
            data-time="<?= $time ?>"
            data-map="<?= ($windData)?str_replace("./data","",$windData["map"]):"" ?>"
            data-imap="<?= $windData?$windData["imap"]:"" ?>"
            data-date="<?= strtotime($current_date) ?>"
        >
            <?php if($windData){ ?>
                <span>
                    <?= 
                        ($unit=="km")?
                        intval($windData["speed"]):
                        intval(kmTokn(intval($windData["speed"]))) 
                    ?>
                </span>
            <?php } ?>
        </td>
    <?php
            }
            $current_date = date("Y-m-d", strtotime($current_date." +1 day"));
            $dayCount++;
        }
    ?>
</tr>