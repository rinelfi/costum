<?php
    $current_date = $startDate;
    $dayCount = 0;
?>

<tr class="wind-tr-speed-bar">
    <td colspan="4">
        <ul>
            <li>60 -</li>
            <li>40 -</li>
            <li>20 -</li>
            <li>0 -</li>
        </ul>
    </td>
    <?php  
        while(strtotime($current_date) <= strtotime($endDate)){ 
            foreach(Meteolamer::$TIMES as $time){
                $windData = isset($data[$current_date]["data"]["h".$time]["wind"])?
                        $data[$current_date]["data"]["h".$time]["wind"]:NULL;
    ?>
        <td
            class="wind-td-speed-bar wind-td-data wind-td-data-day<?= $dayCount ?>-h<?=$time?>"
            data-day="<?=$dayCount?>" 
            data-time="h<?=$time?>"
            data-map="<?= $windData ? str_replace("./data","",$windData["map"]):"" ?>"
            data-imap="<?= $windData ? $windData["imap"]:"" ?>"
            data-date="<?= strtotime($current_date) ?>"
        >
                <?php if($windData){ ?>
                <div class="animate-height" style="height: <?= $windData["speed"]*2 ?>px;"></div>
                <?php } ?>
        </td>
    <?php
            }
            $current_date = date("Y-m-d", strtotime($current_date." +1 day"));
            $dayCount++;
        }
    ?>
</tr>