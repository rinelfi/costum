<?php
    $ROWS = [
        "histogram",
        ["name"=>"speed", "params" => ["unit"=>"km"]],
        ["name"=>"speed", "params" => ["unit"=>"kn"]],
        "direction",
        "meteo"
    ];
?>
<table id="table-weekly-forecast">
    <thead>
        <tr>
            <th colspan="4"></th>
            <?php setlocale(LC_TIME,'fr_FR.utf8','fra') ?>
            <?php foreach($weekly_data as $date => $data) { ?>
            <th colspan="4">
                <span><?= strftime('%A %d', strtotime($date)); ?></span>
            </th>
            <?php } ?>
        </tr>
        <tr>
            <th colspan="4"></th>
            <?php  for($i=0; $i<7;$i++) { ?>
                <th>00</th>
                <th>06</th>
                <th>12</th>
                <th>18</th>
            <?php } ?>
        </tr>
    </thead>
    <tbody>
        <?php
            foreach($ROWS as $row){
                $rowName = is_array($row) ? $row["name"]:$row;
                $partialParams = [
                    "data" => $weekly_data,
                    "startDate" => $start_date,
                    "endDate" => $end_date
                ];
                if(is_array($row))
                    $partialParams = array_merge($partialParams, $row["params"]);

                echo $this->renderPartial("costum.views.tpls.blockCms.meteolamer.tables.wind.rows.".$rowName, $partialParams);
            }
        ?>
    </tbody>
</table>