<?php
$keyTpl ="filActu";
$paramsData = [ 
  "title" => "Actualité",
    "icon"          =>  "",
  "backgroundBtn" => "#ffffff",
  "colorLabelBtn" => "#327753"
];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
} 

?>
<style type="text/css">
   #services_<?= $kunik?> #containActuality<?= $kunik?> .img-event{
    height : 200px;
    width: 500px;
  }
  #services_<?= $kunik?> .card-event{
    margin-top: -30%;

  }
  #services_<?= $kunik?>{
    margin-top: 0;
  }
  #services_<?= $kunik?> h4{
    font-size: 25px;
    margin-top: 8%;
    margin-bottom: 5%;

  }
  #services_<?= $kunik?> p{
    font-size: 16px;
    line-height: 22px;
  }
  #services_<?= $kunik?> .btn-<?= $kunik?>{
    display: none;
    z-index: 9999;
  }
  #services_<?= $kunik?>:hover .btn-<?= $kunik?>{
    display: block;
    position: absolute;
    top:40%;
    left: 50%;
    transform: translate(-50%,-50%);
  }
  #services_<?= $kunik?> .portfolio_image:hover{
    transform: scale(1.1);
  }
  #services_<?= $kunik?> .portfolio_item {
    margin-bottom: 2%;
  }
  #services_<?= $kunik?> .portfolio_item p a{
    color: <?= $paramsData["colorLabelBtn"]?>;
    text-decoration: none;
    padding: 2%;
    background: <?= $paramsData["backgroundBtn"]?>;
    border-radius: 10px;
  }
  #services_<?= $kunik?> .portfolio_image img {
    max-width: 100%;
    border-radius: 10px;
  }
  @media (max-width: 978px) {

    #services_<?= $kunik?> h4{
      font-size: 20px !important;
      margin: 0px;
      padding:0 !important;
      
    }
    #services_<?= $kunik?> h5{
      padding: 0 !important;
      font-size: 12px !important;
      margin: 0;
    }
    #services_<?= $kunik?>{
      margin-top: 0px;
    }
    #services_<?= $kunik?> #containActuality<?= $kunik?> .img-event {
      margin-bottom: 6%;
    }
  }

</style>


<section id="services_<?= $kunik?>" class="services_<?= $kunik?> content_<?= $kunik?> ">
  <div class="btn-<?= $kunik?> text-center">
   
    <?php if(Authorisation::isInterfaceAdmin()){ ?>
      <button id="add-article<?= $kunik?>" class="btn btn-primary btn-xs" ><?php echo Yii::t('cms', 'Add an article')?></button>
    <?php } ?>
  </div>
  <div class="container">  
    <div class=" text-center" style="" >
      <h1 class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"> 
        <i class="fa <?=$paramsData['icon']?>"></i> 
        <?= $paramsData["title"]?>
      </h1>
    </div>
    <div id="containActuality<?= $kunik?>" class="col-xs-12" >
      <div class="row"> 

      </div>
    </div>
  </div>
</section>

<script type="text/javascript">

  function addArticle(){
      var dyfPoi={
       "onload" : {
            "actions" : {
                  "setTitle" : "<?php echo Yii::t('cms', 'Add an article')?>",
                  "html" : {
                      "infocustom" : "<br/><?php echo Yii::t('cms', 'Fill the form')?>"
                  },
                  "presetValue" : {
                      "type" : "article",
                  },
                  "hide" : {
                      "breadcrumbcustom" : 1,
                      "parentfinder" : 1,
                  }
              }
        }
    };
    dyfPoi.afterSave = function(data){
      dyFObj.commonAfterSave(data,function(){
        mylog.log("data", data);
         //location.hash = "#documentation."+data.id;
         urlCtrl.loadByHash(location.hash);

      });
    };
    dyFObj.openForm('poi',null, null,null,dyfPoi);
  }
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  jQuery(document).ready(function() {
    $("#add-article<?= $kunik?>").click(function(){
      addArticle();
    })
    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {
          "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
          "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
          "icon" : "fa-cog",

        "properties" : {
          "colorTitle" : {
            label : "<?php echo Yii::t('cms', 'Title color')?>",
            inputType : "colorpicker",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorTitle
          },
          "colorContent" : {
            label : "<?php echo Yii::t('cms', 'Content color')?>",
            inputType : "colorpicker",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorContent
          },
          "colorLabelBtn" : {
            label : "<?php echo Yii::t('cms', 'Button label color')?>",
            inputType : "colorpicker",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.colorLabelBtn
          },
          "backgroundBtn" : {
            label : "<?php echo Yii::t('cms', 'Button background color')?>",
            inputType : "colorpicker",
            values :  sectionDyf.<?php echo $kunik ?>ParamsData.backgroundBtn
          }
        },
        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function () {  
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
            tplCtx.value[k] = $("#"+k).val();
            if (k == "parent") {
              tplCtx.value[k] = formData.parent;
            }
          });
          console.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
            dataHelper.path2Value( tplCtx, function(params) {
              dyFObj.commonAfterSave(params,function(){
                toastr.success("Élément bien ajouter");
                $("#ajax-modal").modal('hide');
                urlCtrl.loadByHash(location.hash);
              });
            });
          }

        }
      }
    };

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });

    var params = {
      "source" : thisContextSlug,
      "type"  : "article"
    };
    ajaxPost(
        null,
        baseUrl+"/costum/costumgenerique/getpoi",
        params,
        function(data){
          mylog.log("success", data);
          var str = "";
          if(data.result == true)
          {
            var url = "<?= Yii::app()->getModule('costum')->assetsUrl; ?>/images/templateCostum/no-banner.jpg";
            str += '<div class="portfolio_list_inner"  " >';
            $(data.element).each(function(key,value){
              var img = (typeof value.profilMediumImageUrl  != null) ? baseUrl+value.profilMediumImageUrl : url;
              var description = typeof (value.shortDescription) != "undefined" && value.shortDescription != null ? value.shortDescription : "<?php echo Yii::t('cms', 'No description')?>";

              str += '<div class="portfolio_item col-md-4" >'+
              '<div class="portfolio_image">'+
              '<a href="#page.type.poi.id.'+value.id+'" class="lbh-preview-element">'+
              '<img src="'+img+'" alt="" class="img-event img-responsive " style="visibility: visible;">'+
              '</a>'+
              '</div>'+
              '<div class="" >'+
              '<a href="#page.type.poi.id.'+value.id+'" class="lbh-preview-element">'+
              '<h4>'+value.name+'</h4> '+
              '</a>'+
              '<p>'+description+'</p>'+
              '<p><a href="#page.type.poi.id.'+value.id+'" class="lbh-preview-element"><?php echo Yii::t("cms", "More information")?></a></p>'+
              '</div>'+
              '</div>'+ 
              '</div>';
            });
            str+='</div>';
            mylog.log("str actualité description",str);
          }else
          {
            str += "<center><?php echo Yii::t('cms', 'There is no news')?></center>";
          }
          $("#containActuality<?= $kunik?>").html(str);
        }
      )
  });

</script>
