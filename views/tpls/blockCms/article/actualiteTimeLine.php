<?php 
      $keyTpl ="actualiteTimeLine";
      $paramsData = [
          "title" =>"Actualité",
          "titleIcon" => "fa-newspaper-o",
          "underline" => true
      ];

    if (isset($blockCms)) {
      foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
          $paramsData[$e] = $blockCms[$e];
        }
      }
    }
  ?>

<?php  

	if(isset($costum["contextType"]) && isset($costum["contextId"])){
        $el = Element::getByTypeAndId($costum["contextType"], $costum["contextId"] );
    }

?>
<style type="text/css">
  .actu-<?= $kunik?>{
    text-align: center;
  }

</style>


<!-- Actualité -->
<div class="actu-<?= $kunik?> row content-<?= $kunik?>">
    <div class="col-xs-12 col-sm-12">
        <div class="actu-titre col-xs-6 col-sm-6">
            <h1 class="title actu-<?= $kunik?>">
              <i class="fa <?php echo $paramsData["titleIcon"]; ?>"></i> 
              <span class="sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title"><?php echo $paramsData["title"]; ?></span>
            </h1>
        </div>
        <br>
        <div class="actucard col-xs-12 col-sm-12 col-md-12">
            <div id="newsstream"></div>
        </div>
		
        <div class="col-xs-12 col-sm-12 text-center">
            <a href="javascript:;" data-hash="#live" class="lbh-menu-app h3">
                <i class="fa fa-plus circle"></i>
                <br>Voir plus <br>	d'actualités
            </a>
        </div>
    </div>
</div>

<script type="text/javascript">
	$(document).ready(function(){
	
		urlNews = "/news/co/index/type/"+thisContextType+"/id/"+thisContextId+"/formCreate/false/nbCol/2/scroll/false";
		
		ajaxPost("#newsstream",baseUrl+"/"+urlNews,{search:true, formCreate:false, scroll:false}, function(news){}, "html");
		if(notNull(currentUser)) currentUser.addressCountry = "RE";
		
		contextData = {
			id : "<?php echo $costum["contextId"] ?>",
			type : "<?php echo $costum["contextType"] ?>",
			name : "<?php echo $el['name'] ?>",
			slug : "<?php echo $el['slug'] ?>",
			profilThumbImageUrl : "<?php echo $el['profilThumbImageUrl'] ?>"
		};
	});
</script>
<script type="text/javascript">
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
  jQuery(document).ready(function() {
    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {
          "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
          "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
        "icon" : "fa-cog",
        
        "properties" : {
          
        },
        beforeBuild : function(){
                uploadObj.set("cms","<?php echo $blockKey ?>");
            },
        save : function () {  
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
            tplCtx.value[k] = $("#"+k).val();
            if (k == "parent") {
              tplCtx.value[k] = formData.parent;
            }
          });
          console.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
            dataHelper.path2Value( tplCtx, function(params) {
              dyFObj.commonAfterSave(params,function(){
                toastr.success("Élément bien ajouter");
                $("#ajax-modal").modal('hide');
                urlCtrl.loadByHash(location.hash);
              });
            } );
          }

        }
      }
    };

    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });
  })
</script>