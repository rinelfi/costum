<?php 
/* 
Super image:
Create by Ifaliana Arimanana
ifaomega@gmail.com
26 Apr 2021
*/

/***************** Required *****************/
$keyTpl   = "image";
$myCmsId  = $blockCms["_id"]->{'$id'};
$kunik    = $keyTpl.$myCmsId;
$subtype  = $blockCms["subtype"] ?? $blockCms["subtype"] ?? "";

/*************** End required ***************/

/*****************get image uploaded***************/
$initImage = Document::getListDocumentsWhere(
  array(
    "id"=> $myCmsId,
    "type"=>'cms',
    "subKey"=>'block',
  ), "image"
);

$latestImg = isset($initImage["0"]["imagePath"])?$initImage["0"]["imagePath"]:"empty" ;
// echo "<pre>";
// var_dump($initImage);

// echo "</pre>";
// if($latestImg == "empty"){
//   $latestImg = isset($initImage["0"]["imagePath"])?$initImage["0"]["imagePath"]:"empty" ;
// }
/*****************end get image uploaded***************/
// var_dump($initImage["0"]["imagePath"]);
/* Get settings */

$array_position   = $blockCms["css"]['position'] ?? $blockCms["css"]['position'] ?? [];
$otherClass       = $blockCms["class"]["other"] ?? $blockCms["class"]["other"] ?? "";
$otherCss         = $blockCms["css"]["other"] ?? $blockCms["css"]["other"] ?? [];
$width            = $blockCms["css"]["size"]["width"] ?? $blockCms["css"]["size"]["width"] ?? "320px";
$objfit           = $blockCms["css"]["object-fit"] ?? $blockCms["css"]["object-fit"] ?? "none";
$height           = $blockCms["css"]["size"]["height"] ?? $blockCms["css"]["size"]["height"] ?? "240px";
$borderColor      = $blockCms["css"]["border"]["color"] ?? $blockCms["css"]["border"]["color"] ?? "transparent";
$backgroundColor  = $blockCms["css"]["background"]["color"] ?? $blockCms["css"]["background"]["color"] ?? "transparent";


  /**********Border**********/
$borderColor                = $blockCms["css"]["border"]["color"] ?? $blockCms["css"]["border"]["color"] ?? "transparent";
$borderRadiustopLeft        = $blockCms["css"]["border"]["radius"]["top-left"] ?? $blockCms["css"]["border"]["radius"]["top-left"] ?? "0";
$borderRadiustopRight       = $blockCms["css"]["border"]["radius"]["top-right"] ?? $blockCms["css"]["border"]["radius"]["top-right"] ?? "0";
$borderRadiusbottomRight    = $blockCms["css"]["border"]["radius"]["bottom-right"] ?? $blockCms["css"]["border"]["radius"]["bottom-right"] ?? "0";
$borderRadiusbottomLeft     = $blockCms["css"]["border"]["radius"]["bottom-left"] ?? $blockCms["css"]["border"]["radius"]["bottom-left"] ?? "0";
$borderRadius               = max($borderRadiustopLeft,$borderRadiustopRight,$borderRadiusbottomRight,$borderRadiusbottomLeft);
  /********End border********/

  /**********Padding**********/
$paddingtop     = $blockCms["css"]["padding"]["top"] ?? $blockCms["css"]["padding"]["top"] ?? "0";
$paddingright   = $blockCms["css"]["padding"]["right"] ?? $blockCms["css"]["padding"]["right"] ?? "0";
$paddingleft    = $blockCms["css"]["padding"]["left"] ?? $blockCms["css"]["padding"]["left"] ?? "0";
$paddingbottom  = $blockCms["css"]["padding"]["bottom"] ?? $blockCms["css"]["padding"]["bottom"] ?? "0";
$padding        = max($paddingtop,$paddingright,$paddingleft,$paddingbottom);
  /********End padding********/

  /**********shadow**********/
  $inset    = $blockCms["css"]["box-shadow"]["inset"] ?? $blockCms["css"]["box-shadow"]["inset"] ?? "";
  $shwColor = $blockCms["css"]["box-shadow"]["color"] ?? $blockCms["css"]["box-shadow"]["color"] ?? "transparent";
  $axeX     = $blockCms["css"]["box-shadow"]["x"] ?? $blockCms["css"]["box-shadow"]["x"] ?? "0";
  $axeY     = $blockCms["css"]["box-shadow"]["y"] ?? $blockCms["css"]["box-shadow"]["y"] ?? "0";
  $blur     = $blockCms["css"]["box-shadow"]["blur"] ?? $blockCms["css"]["box-shadow"]["blur"] ?? "0";
  $spread   = $blockCms["css"]["box-shadow"]["spread"] ?? $blockCms["css"]["box-shadow"]["spread"] ?? "0";
  /********End shadow********/

// var_dump($shadow["blur"]);
/* End get settings */
?>


<style type="text/css">
  .edit-<?= $kunik ?> {  
    border: dashed 2px gray;
   
  }

  .view-mode-<?= $kunik ?> {
    background-color: <?= $backgroundColor ?>;
    
  }

  .super-img-<?= $myCmsId ?> {
    object-fit: <?= $objfit ?>;
    width:100%;
    height:100%;    
    border-radius: <?= $borderRadiustopLeft ?>px <?= $borderRadiustopRight ?>px <?= $borderRadiusbottomRight ?>px <?= $borderRadiusbottomLeft ?>px;
    <?php if ( $borderColor !="transparent") { ?>
      border: solid <?= $borderColor ?>;
    <?php } ?>
    background-color: <?= $backgroundColor ?>;    
    padding: <?= $paddingtop ?>px <?= $paddingright ?>px <?= $paddingbottom ?>px <?= $paddingleft ?>px;
  }
  .edit-img<?php echo $kunik ?>Params {
  /*  position: absolute;
    transform: translate(50%, 50%);
    top: 25px;
    right: 25px;
    border-radius: 100%;
    font-size: large;*/
  }

  .selected-mode-<?= $kunik ?> {
    box-shadow: #92bfffa6 0px 0px 2px 2px;
    border : 2px dotted #479dff;
  }


  .<?= $kunik ?>{
    background-color: transparent !important;
    position: relative;
    width : <?= $width ?>;
    height: <?= $height ?>;
    <?php 
      echo "-webkit-box-shadow: ".$inset." ".$shwColor." ".$axeX."px ".$axeY."px ".$blur."px ".$spread."px;"; 
      echo "-moz-box-shadow: ".$inset." ".$shwColor." ".$axeX."px ".$axeY."px ".$blur."px ".$spread."px;"; 
      echo "box-shadow: ".$inset." ".$shwColor." ".$axeX."px ".$axeY."px ".$blur."px ".$spread."px;"; 
    ?> 
    border-radius: <?= $borderRadiustopLeft ?>px <?= $borderRadiustopRight ?>px <?= $borderRadiusbottomRight ?>px <?= $borderRadiusbottomLeft ?>px; 
    <?php foreach ($array_position as $key => $value) {
      echo $key." : ".$value."; "; } ?> 
      overflow:hidden
  }

  .other-css-<?= $kunik ?> { 
    display: inline-table;
    /*position:sticky; */
      <?php 
      foreach ($otherCss as $csskey => $cssvalue) {
        echo $csskey.":".$cssvalue."!important;";
      }
      ?>
    }

  .super-cms-range-slider{ 
    -webkit-appearance: none;
    background: <?= $shwColor."6b" ?>;
    border: solid 1px #999;
    outline: none;
    opacity: 0.7;
    height: 10px;
    -webkit-transition: .2s;
    transition: opacity .2s;
  }

  .super-cms-range-slider::-webkit-slider-thumb {
    -webkit-appearance: none;
    appearance: none;
    width: 20px;
    height: 20px;
    background: <?= $shwColor ?>;
    cursor: pointer;
    border: solid 2px darkgray;
    border-radius: 50%;
  }

  .<?= $kunik ?> .ui-wrapper{
    /*padding: 20px;*/
  }

  .edit-<?= $kunik ?>:hover {
    cursor: context-menu;
  }

  @media (max-width: 800px) {
    .super-img-<?= $myCmsId ?> {
      /*padding: 0px;
      margin: 0px;      */
      /*position: sticky; */
    }
    .<?= $kunik ?> {
      padding: 0px;
      margin: 0px;
      left: 50% !important;
      transform: translate(-50%);
    }
  }

</style>

  <div class="<?= $kunik ?> super-cms">
    <?php if ($latestImg !="empty") { ?>
      <img class="super-img-<?= $myCmsId ?> this-content-<?= $kunik ?> unselectable" src="<?php echo $latestImg ?>">
    <?php }else{ ?>
      <img class="super-img-<?= $myCmsId ?> this-content-<?= $kunik ?> unselectable" src="<?php echo Yii::app()->controller->module->assetsUrl.'/images/thumbnail-default.jpg' ?>">
    <?php } ?>    
  </div>

<?php if(Authorisation::isInterfaceAdmin()){?>
<script type="text/javascript">

  if ("supercms" !== "<?= $subtype ?>" ) {
    tplCtx = {};
    tplCtx.id = "<?php echo $myCmsId ?>";
    tplCtx.collection = "cms";
    tplCtx.path = "allToRoot"; 
    tplCtx.value = {};
    tplCtx.value.subtype = "supercms";

    dataHelper.path2Value( tplCtx, function(params) { } );
  }

/*-----View and edit mode------- */
    superCms<?= $kunik ?> = {
      viewMode: function(){
        // $(".<?= $kunik ?>").addClass("view-mode-<?= $kunik ?>");
        $(".<?= $kunik ?>").removeClass("edit-<?= $kunik ?>");
      },
      editMode: function(){
        // $(".<?= $kunik ?>").removeClass("view-mode-<?= $kunik ?>");
        $(".<?= $kunik ?>").addClass("edit-<?= $kunik ?>");
      }
    }

       
    $(document).keyup(function(e) {
      if (e.altKey == true && e.keyCode == 80) {
        if (previewMode == false) {
          superCms<?= $kunik ?>.viewMode();
          $("#toolsBar").hide();  
        }else{
          superCms<?= $kunik ?>.editMode();
          $("#toolsBar").hide();
        }
      }
    });


    $(".view-super-cms").click(function(){
      superCms<?= $kunik ?>.viewMode();
      $("#toolsBar").hide();   
    });
    $(".hiddenEdit").click(function(){
      if (previewMode == false) {
        superCms<?= $kunik ?>.viewMode();
        $("#toolsBar").hide();  
      }else{
        superCms<?= $kunik ?>.editMode();
        $("#toolsBar").hide();
      }
    })
    $(document).keyup(function(e) {
      if (e.key === "Escape") { 
       superCms<?= $kunik ?>.editMode();
      $("#toolsBar").hide();
      }
    });

/*-----End view and edit mode------- */

if(localStorage.getItem("previewMode") == "w"){
  superCms<?= $kunik ?>.editMode();
}
  
   /*********************Mouse hover edit mode*********************/
   $('.<?= $kunik ?>').mouseover(function(e){
    if (localStorage.getItem("previewMode") === "w" && $("#toolsBar").is(":hidden")) {
      e.stopPropagation();
      $(this).addClass("edit-<?= $kunik ?>");
      $(".container<?= @$blockCms['tplParent'] ?>").addClass("edit-container<?= @$blockCms['tplParent'] ?>");
      $(".container<?= @$blockCms['tplParent'] ?>").removeClass("selected-mode-container<?= @$blockCms['tplParent'] ?>");

      $(".<?= $kunik ?>").removeClass("edit-<?= $kunik ?>");
      $(".<?= $kunik ?>").addClass("selected-mode-<?= $kunik ?>");
    }
  });

   $('.<?= $kunik ?>').mouseleave(function(e){
     if (localStorage.getItem("previewMode") === "w" && $("#toolsBar").is(":hidden")) {
      e.stopPropagation();
      $(".<?= $kunik ?>").removeClass("edit-<?= $kunik ?>");
      $(".<?= $kunik ?>").addClass("selected-mode-<?= $kunik ?>");

      $(".<?= $kunik ?>").addClass("edit-<?= $kunik ?>");
      $(".<?= $kunik ?>").removeClass("selected-mode-<?= $kunik ?>");
    }
  });
   /*********************End mouse hover edit mode*********************/

/***************Drag and resize*******************/
$('.<?= $kunik ?>').mouseenter(function() {      
  $(".<?= $kunik ?>").draggable();
  $(".<?= $kunik ?>").resizable();
  if (localStorage.getItem("previewMode") == "w") {      
  $(".<?= $kunik ?>").draggable('enable');
  $(".<?= $kunik ?>").resizable('enable');
    $(".<?= $kunik ?>").draggable({
      opacity: 0.50,
      cancel: '.editable',
      start : function(event, ui){      
        $("#toolsBar").html(``);
        $("#toolsBar").hide(); 
      },
      stop: function(event, ui){
      //convert position from px to %
      var myLeft = $(".<?= $kunik ?>").css("left");
      var parentWidth = $(".container<?= $blockCms['tplParent'] ?>").width();
      myLeft = parseInt(myLeft);
      LeftPrnt = (myLeft / parentWidth) *100;
      $(".<?= $kunik ?>").css("left", (LeftPrnt)+"%");
      revert: true,
      tplCtx = {};
      tplCtx.id = "<?php echo $myCmsId ?>";
      tplCtx.collection = "cms";
      tplCtx.path = "css.position"; 
      tplCtx.value = {};
      tplCtx.value.top = $(".<?= $kunik ?>").css( "top" );
      tplCtx.value.left = LeftPrnt+"%";

      dataHelper.path2Value( tplCtx, function(params) {} );
    }
  });

    $(function(){
     $(".<?= $kunik ?>").resizable({
      cancel: '.editable',

      start: function(event, ui){
        superCms<?= $kunik ?>.viewMode();
      },
      stop: function(event, ui){
        var myWidth = $(".<?= $kunik ?>").width();
        var myHeight = $(".<?= $kunik ?>").height();
        var parentWidth = $(".container<?= $blockCms['tplParent'] ?>").width();
      // alert(myHeight);
      widthPrnt = (100 * myWidth / parentWidth);
      // alert(widthPrnt);
      $(".<?= $kunik ?>").css("width", (myWidth)+"px");
      $(".<?= $kunik ?>").css("height", (myHeight)+"px");
      // $(".<?= $kunik ?>").css("width", widthPrnt+"%");
      revert: true,
      tplCtx = {};
      tplCtx.id = "<?php echo $myCmsId ?>";
      tplCtx.collection = "cms";
      tplCtx.path = "css.size"; 
      tplCtx.value = {};
      tplCtx.value.width = myWidth+"px";
      tplCtx.value.height = myHeight+"px";

      dataHelper.path2Value( tplCtx, function(params) {        
        superCms<?= $kunik ?>.editMode();
      } );
    }
  });
   }); 
  }else{
    $(".<?= $kunik ?>").draggable('disable');
    $(".<?= $kunik ?>").resizable('disable');
  }
}); 
/*****************End drag and resize*****************/

/********************Display menu**********************/
$(".<?= $kunik ?>").click(function(){
 if (localStorage.getItem("previewMode") == "w") {   
    if(last_edited != ""){
      $("."+last_edited).addClass("edit-"+last_edited);    
    }
    last_edited = "<?= $kunik ?>";

    $(".<?= $kunik ?>").removeClass("selected-mode-<?= $kunik ?>");
    $("#toolsBar").show();    
    $(".sp-settings-btn").hide();
    
    superCms<?= $kunik ?>.viewMode();
    $("#toolsBar").html(`
         <div class="container-fluid">
    <div class="text-center">
      <i style="cursor:default;font-size: large;" aria-hidden="true">Paramètres d'image</i>
      <button class="bg-transparent deleteLine delete-super-cms tooltips" data-path="<?= $blockCms['path'] ?>" data-id="<?= $blockCms['_id']; ?>" data-collection="cms" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Supprimer cet élément d'image">
        <i class="fa fa-trash text-red" aria-hidden="true"></i>
      </button>
      <i class="fa fa-window-close closeBtn" aria-hidden="true"></i>
    </div>
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#home">Général</a></li>
      <li><a data-toggle="tab" href="#menu1">Ombre</a></li>
      <li><a data-toggle="tab" href="#menu2">Autre class</a></li>
      <li><a data-toggle="tab" href="#menu3">Autre css</a></li>
    </ul>

    <div class="tab-content padding-top-20">
      <div id="home" class="tab-pane fade in active">
        <div class="row">
          <div class="col-md-6">      
            <div class="panel-group">
                <label>Ajouter une image</label>
                <div class="input-group">
                  <input id="fond-color" class="sp-color-picker spectrum sp-colorize form-control" name="" value="<?= $backgroundColor ?>" placeholder="#ffffff" style="">
                  <div class="input-group-btn closeMenu" style="width: 0px">                    
                    <button class="edit-img<?php echo $kunik ?>Params tooltips btn btn-primary" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Modifier la photo">
                      <i class="fa fa-camera" aria-hidden="true"></i><br>
                    </button> 
                  </div>
                </div>              
                <div id="border" class="collapse elem-value">
                <div class="col-elem-value">
                  <div class="element-field"> 
                    <label>Haut à gauche</label>                
                    <input class="form-control top-left border-control" type="number" min="0" value="<?= $borderRadiustopLeft ?>" placeholder="0" data-path="border.radius" data-given="border-top-left-radius"> 
                  </div>                  
                  <div class="element-field">      
                    <label>Haut à droite</label>                   
                    <input class="form-control top-right border-control" type="number" min="0" value="<?= $borderRadiustopRight ?>" placeholder="0" data-path="border.radius" data-given="border-top-right-radius"> 
                  </div>
                   <div class="element-field"> 
                    <label>Bas à gauche</label>                           
                    <input class="form-control bottom-left border-control" type="number" min="0" value="<?= $borderRadiusbottomLeft ?>" data-path="border.radius" data-given="border-bottom-left-radius" placeholder="0"> 
                  </div>                  
                  <div class="element-field">      
                    <label>Bas à droite</label>                   
                    <input class="form-control bottom-right border-control" type="number" min="0" value="<?= $borderRadiusbottomRight ?>" data-path="border.radius" data-given="border-bottom-right-radius" placeholder="0"> 
                  </div>
                </div>
              </div>

              <label>Rembourrage (Padding)</label>
              <div class="input-group">
              <input class="form-control elem-control" type="number" min="0" value="<?= $padding ?>" placeholder="0" data-path="padding" data-given="padding" data-what="padding-control"> 
                <div class="input-group-btn" style="width: 0px">                           
                  <button  data-toggle="collapse" data-target="#padding" class="tooltips btn btn-primary" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Plus de réglage">
                    <i class="fa fa-plus" aria-hidden="true"></i><br>
                  </button> 
                </div>
              </div>
              <div id="padding" class="collapse elem-value">
                <div class="col-elem-value">
                  <div class="element-field" style="width:100px"> 
                    <label>En haut</label>                
                    <input class="form-control top-left padding-control" type="number" min="0" value="<?= $paddingtop ?>" placeholder="0" data-path="padding" data-given="padding-top"> 
                  </div>                  
                  <div class="element-field" style="width:100px">      
                    <label>À droite</label>                   
                    <input class="form-control top-right padding-control" type="number" min="0" value="<?= $paddingright ?>" placeholder="0" data-path="padding" data-given="padding-right"> 
                  </div>
                   <div class="element-field" style="width:100px"> 
                    <label>À gauche</label>                           
                    <input class="form-control bottom-left padding-control" type="number" min="0" value="<?= $paddingleft ?>" data-path="padding" data-given="padding-left" placeholder="0"> 
                  </div>                  
                  <div class="element-field" style="width:100px">      
                    <label>En bas</label>                   
                    <input class="form-control bottom-right padding-control" type="number" min="0" value="<?= $paddingbottom ?>" data-path="padding" data-given="padding-bottom" placeholder="0"> 
                  </div>
                </div>
              </div> 

              <label>Ajustement de la photo</label>
                <select class="form-control" id="objfit">
                  <option value="contain">Contain</option>
                  <option value="cover">Cover</option>
                  <option value="fill">Fill</option>
                  <option value="inherit">Inherit</option>
                  <option value="initial">Initial</option>
                  <option value="none">None</option>
                  <option value="revert">Revert</option>
                  <option value="scale-down">Scale-down</option>
                  <option value="unset">Unset</option>
                </select>
            </div>
          </div>
          <div class="col-md-6">      
            <div class="panel-group">
              <div >          
                <div class="">
                  <div class="">
                    <label>Couleur de la bordure</label>
                    <input id="border-color" class="sp-color-picker spectrum sp-colorize form-control" name="" value="<?= $borderColor ?>" placeholder="#ffffff" style="">
                    <label>Rayon de la bordure</label>                      
                    <div class="input-group">
                      <input class="form-control elem-control" type="number" min="0" value="<?= $borderRadius ?>" placeholder="0" data-path="border.radius" data-given="border-radius" data-what="border-control"> 
                      <div class="input-group-btn" style="width: 0px">                    
                        <button  data-toggle="collapse" data-target="#border" class="tooltips btn btn-primary" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Plus de réglage">
                          <i class="fa fa-plus" aria-hidden="true"></i><br>
                        </button> 
                      </div>
                    </div>
                  </div>  
                </div>  
              </div>
            </div>
          </div>
          
          <div class="col-md-12">      
            <div class="panel-group">
              <div >          
                <div class="">
                  <div class="">
                    
                  </div>  
                </div>  
              </div>
            </div>
          </div>        
        </div>  
      </div>
      <div id="menu1" class="tab-pane fade">
        <div class="col-md-12">      
          <div class="panel-group">        
          <label>Couleur</label>
          <div class="pull-right">
          <input id="check-inset" type="checkbox" <?php if ($inset != "") {echo 'checked="checked"';} ?>>
          <label for="check1">Inset</label>
          <p></p>
          </div>
          <input id="shw-color" class="sp-color-picker spectrum sp-colorize form-control" name="" value="<?= $shwColor ?>" placeholder="#ffffff" style="">
            <div class="range-slider text-center padding-top-20">
              <span class="range-blur__value">Blur <?= $blur ?>px</span>
              <input class="range-slider-blur super-cms-range-slider" type="range" value="<?= $blur ?>" min="0" max="20" step="0.2">
            </div>
            <div class="range-slider text-center padding-top-20">
              <span class="range-spread__value">Spread <?= $spread ?>px</span>
              <input class="range-slider-spread super-cms-range-slider" type="range" value="<?= $spread ?>" min="0" max="20" step="0.2">
            </div>
            <div class="range-slider text-center padding-top-20">
              <span class="range-x__value">Axe X: <?= $axeX ?>px</span>
              <input class="range-slider-x super-cms-range-slider" type="range" value="<?= $axeX ?>" min="-10" max="10" step="0.2">
            </div>
            <div class="range-slider text-center padding-top-20">
              <span class="range-y__value">Axe Y: <?= $axeY ?>px</span>
              <input class="range-slider-y super-cms-range-slider" type="range" value="<?= $axeY ?>" min="-10" max="10" step="0.2">
            </div>
          </div>
        </div>
      </div>
      <div id="menu2" class="tab-pane fade">    
        <div class="col-md-12">      
          <div class="panel-group">
            <div class="form-group">
              <label for="">Class bootstrap <span class="text-red" style="font-weight:normal">(Réglage pour le dévéloppeur)<span></label>
              <textarea class="form-control other-class" rows="5"><?= $otherClass ?></textarea>
            </div>
          </div>
        </div>
      </div>
      <div id="menu3" class="tab-pane fade">
        <div class="">        
          <div class="panel-group">
            <div class="form-group">
              <label for="">Styles css <span class="text-red" style="font-weight:normal">(Réglage pour le dévéloppeur)<span></label>
              <textarea class="form-control other-css" rows="5"><?php foreach ($otherCss as $csskey => $cssvalue) {
                echo $csskey.":".$cssvalue.";\r\n";
              } ?></textarea>
              <p class="css_err"></p>
            </div>
          </div>
        </div>
      </div>
    </div> 
  </div> 
    `);     

    cmsBuilder.init();
      $(".closeBtn, .closeMenu").click(function(){      
       superCms<?= $kunik ?>.editMode();
        $("#toolsBar").html(``);
        $("#toolsBar").hide();
        $(".this-content-<?= $kunik ?>").css("cursor", "default");
      });

  /**********Image settings Color*************/

    /*********Color*********/

    if(  $(".colorpickerInput").length){
      loadColorPicker(initColor);
    }

    loadSpectrumColorPicker(function(){
      if(  $(".sp-color-picker").length){
        $(".sp-color-picker").spectrum({
          type: "text",
          showInput: "true",
          showInitial: "true"
        });
        $('.sp-color-picker').addClass('form-control');
        $('.sp-original-input-container').css("width","100%");
      }
    });

    $( "#fond-color" ).change(function() {
      $(".this-content-<?= $kunik ?>" ).css("background-color",$("#fond-color").val());   
      tplCtx = {};
      tplCtx.id = "<?php echo $myCmsId ?>";
      tplCtx.collection = "cms";
      tplCtx.path = "css.background.color"; 
      tplCtx.value = $("#fond-color").val();

      dataHelper.path2Value( tplCtx, function(params) {} );
    });

    $( "#border-color" ).change(function() {
     $(".super-img-<?= $myCmsId ?>" ).css("border-color",$("#border-color").val());
     tplCtx = {};
     tplCtx.id = "<?php echo $myCmsId ?>";
     tplCtx.collection = "cms";
     tplCtx.path = "css.border.color"; 
     tplCtx.value = $("#border-color").val();

     dataHelper.path2Value( tplCtx, function(params) {} );
   });

   /*********End color*********/

  /************Border**padding***margin*************/
   var thisetting = {};
   
   $(".border-control" ).on('keyup change', function (){
    $('.elem-value .border-control').each(function(k,v){
      pars   = $(this).val() ? parseInt($(this).val()) : '0';
      thisIs = $(this).data("given");
      $(".<?= $kunik ?>" ).css(thisIs,pars+"px");
      $(".this-content-<?= $kunik ?>" ).css(thisIs,pars+"px");
      $(this).val(pars);
      thisetting[thisIs.replace('-radius','').replace('border-','').replace('padding-','').replace('margin-','')] = $(this).val();
    })
  });
    $(".padding-control" ).on('keyup change', function (){
    $('.elem-value .padding-control').each(function(k,v){
      pars   = $(this).val() ? parseInt($(this).val()) : '0';
      thisIs = $(this).data("given");
      $(".this-content-<?= $kunik ?>" ).css(thisIs,pars+"px");
      $(this).val(pars);
      thisetting[thisIs.replace('-radius','').replace('border-','').replace('padding-','').replace('margin-','')] = $(this).val();
    })
  });
  //   $(".margin-control" ).on('keyup change', function (){
  //   $('.elem-value .margin-control').each(function(k,v){
  //     pars   = $(this).val() ? parseInt($(this).val()) : '0';
  //     thisIs = $(this).data("given");
  //     $(".<?= $kunik ?>" ).css(thisIs,pars+"px");
  //     $(this).val(pars);
  //     thisetting[thisIs.replace('-radius','').replace('border-','').replace('padding-','').replace('margin-','')] = $(this).val();
  //   })
  // });


   $(".elem-control" ).on('keyup change', function (){
      globalConf = $(this).val() ? parseInt($(this).val()) : '0';
      $("."+$(this).data("what")).val(globalConf);
      if ($(this).data("given") != "padding") {
        $(".<?= $kunik ?>" ).css($(this).data("given"),globalConf+"px");
      }
      $('.elem-value .'+$(this).data("what")).each(function(k,v){
        pars   = $(this).val() ? parseInt($(this).val()) : '0';
        thisIs = $(this).data("given");
        $(".this-content-<?= $kunik ?>" ).css(thisIs,pars+"px");
        $(this).val(pars);
        thisetting[thisIs.replace('-radius','').replace('border-','').replace('padding-','').replace('margin-','')] = $(this).val();
      })
    });

   $( ".elem-control, .elem-value .form-control" ).blur(function() {
    tplCtx = {};
    tplCtx.id = "<?php echo $myCmsId ?>";
    tplCtx.collection = "cms";
    tplCtx.path = "css."+$(this).data("path"); 
    tplCtx.value = thisetting;

    dataHelper.path2Value( tplCtx, function(params) {} );
  });

   /************End border**padding***margin***********/

    /***object-fit***/
   $( "#objfit" ).val("<?= $objfit ?>");
   $( "#objfit" ).change(function() {
    var myfit = $( "#objfit" ).val();
    $(".super-img-<?= $myCmsId ?>" ).css("object-fit",myfit);
      tplCtx = {};
      tplCtx.id = "<?php echo $myCmsId ?>";
      tplCtx.collection = "cms";
      tplCtx.path = "css.object-fit"; 
      tplCtx.value = myfit;

      dataHelper.path2Value( tplCtx, function(params) {} );
    });
   /***End object-fit***/

   /*Box shadow*/
     var eltBlur = $('.range-slider-blur').val();
     var eltSpread = $('.range-slider-spread').val();
     var eltAxeX = $('.range-slider-x').val();
     var eltAxeY = $('.range-slider-y').val();
     var eltColor = $("#shw-color").val();
     var inset = "";

     $( "#check-inset" ).change(function() {
      if ($( "#check-inset" ).is( ":checked" ) == true) {      
        inset = "inset";
      }else{
        inset = "";
      }
      $(".<?= $kunik ?>" ).css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
       saveShadow<?= $kunik ?>();
    });

     $( "#shw-color" ).change(function() {
      eltColor = $("#shw-color").val();
      $(".<?= $kunik ?>" ).css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
      saveShadow<?= $kunik ?>();
    });
     $('.range-slider').mousedown(function(){
       $('.range-slider-blur').mousemove(function(){
        eltBlur = $('.range-slider-blur').val();
        $(".range-blur__value").html("Blur "+eltBlur+"px");
      $(".<?= $kunik ?>" ).css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
      });
       $('.range-slider-spread').mousemove(function(){
        eltSpread = $('.range-slider-spread').val();
        $(".range-spread__value").html("Spread "+eltSpread+"px");
      $(".<?= $kunik ?>" ).css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
      });
       $('.range-slider-x').mousemove(function(){
        eltAxeX = $('.range-slider-x').val();
        $(".range-x__value").html("Axe X "+eltAxeX+"px");
      $(".<?= $kunik ?>" ).css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
      });  
       $('.range-slider-y').mousemove(function(){
        eltAxeY = $('.range-slider-y').val();
        $(".range-y__value").html("Axe Y "+eltAxeY+"px");
      $(".<?= $kunik ?>" ).css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
      });
     });
     $('.range-slider').mouseup(function(){
      saveShadow<?= $kunik ?>();
     });
     function saveShadow<?= $kunik ?>(){
      tplCtx = {};
      tplCtx.id = "<?php echo $myCmsId ?>";
      tplCtx.collection = "cms";
      tplCtx.path = "css.box-shadow"; 
      tplCtx.value = {};
      tplCtx.value.inset =  inset;
      tplCtx.value.color =  eltColor;
      tplCtx.value.x     =  eltAxeX;
      tplCtx.value.y     =  eltAxeY;
      tplCtx.value.blur  =  eltBlur;
      tplCtx.value.spread =  eltSpread;

      dataHelper.path2Value( tplCtx, function(params) {} );
    }
   /*End box shadow*/

   /****************Other class option****************/

    $(".other-class" ).on('keyup', function (){
     $(".<?= $kunik ?>").removeClassExcept("super-container <?= $kunik ?> super-cms other-css-<?= $kunik ?> newCss-<?= $kunik ?>");
     $(".<?= $kunik ?>").addClass($(".other-class" ).val());
    });
    $(".other-class" ).on('blur', function (){
      tplCtx = {};
      tplCtx.id = "<?php echo $myCmsId ?>";
      tplCtx.collection = "cms";
      tplCtx.path = "class.other"; 
      tplCtx.value = $(".other-class" ).val();

      dataHelper.path2Value( tplCtx, function(params) {} );
    });
  /*************End other class option****************/

  /****************Other css option****************/
   var css_pro_and_val = {};
    $(".other-css" ).on('keyup', function (){
      //Validate and apply css
     $(".<?= $kunik ?>").removeClass("other-css-<?= $kunik ?>"); 
     $(".<?= $kunik ?>").removeAttr("style");
     var other_css = $(".other-css" ).val()
     var array_val = other_css.split(/[;/^\s*(\n)\s*$/]+/);
     $.each(array_val, function(k,val) {
    /* css validation
     if(val==""  || val == " " || val == /^\s*(\n)\s*$/){
        // $(".css_err").text(css_propertyname+" insupporté!"); 
      }else{   */   
       var css_propertyname = val.split(':')[0];  
       var css_value = val.substr(val.lastIndexOf(":") + 1);
       if(CSS.supports(css_propertyname, css_value)){;
         if(css_propertyname != css_value){
         css_pro_and_val[css_propertyname] = css_value;
         }
       }
 /*    }*/
     });
     // let para = document.querySelector($(".<?= $kunik ?>" ));
     // let compStyles = window.getComputedStyle(para);
     // console.log(compStyles);
     $(".<?= $kunik ?>" ).css(css_pro_and_val);
    });

    $(".other-css" ).on('blur', function (){
      tplCtx = {};
      tplCtx.id = "<?php echo $myCmsId ?>";
      tplCtx.collection = "cms";
      tplCtx.path = "css.other"; 
      tplCtx.value = css_pro_and_val;

      dataHelper.path2Value( tplCtx, function(params) {} );
    });
  /*************End other css option****************/

    $(".stop-propagation").click(function(e) {
      e.stopPropagation();
    });

    /* Upload d'image */
      jQuery(document).ready(function() {
          sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
              "title" : "Importation d'image",
              "description" : "Personnaliser votre bloc",
              "icon" : "fa-cog",
              
              "properties" : {    
                "image" :{
                 "inputType" : "uploader",
                 "label" : "image",
                 "docType": "image",
                 "contentKey" : "slider",
                 "itemLimit" : 1,
                 "endPoint": "/subKey/block",
                 "domElement" : "image",
                 "filetypes": ["jpeg", "jpg", "gif", "png"],
                 "label": "Image :",
                 "showUploadBtn": false,
                 initList : <?php echo json_encode($initImage) ?>
                },
              },
              beforeBuild : function(){
                  uploadObj.set("cms","<?php echo $myCmsId ?>");
              },
              save : function (data) {  
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                  tplCtx.value[k] = $("#"+k).val();
                  if (k == "parent")
                    tplCtx.value[k] = formData.parent;

                  if(k == "items")
                    tplCtx.value[k] = data.items;
                });
                
                if(typeof tplCtx.value == "undefined")
                  toastr.error('value cannot be empty!');
                else {
                    dataHelper.path2Value( tplCtx, function(params) {
                      dyFObj.commonAfterSave(params,function(){
                        toastr.success("Élément bien ajouté");
                        $("#ajax-modal").modal('hide');
                        urlCtrl.loadByHash(location.hash);
                      });
                    } );
                }

              }
            }
          };

          $(".edit-img<?php echo $kunik ?>Params").off().on("click",function() {  
            tplCtx.subKey = "imgParent";
            tplCtx.id = "<?php echo $myCmsId ?>";
            tplCtx.collection = "cms";
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, null);
          });
        });

/* End upload d'image */
  }
});

/* End display menu */



</script>
<?php } ?> 