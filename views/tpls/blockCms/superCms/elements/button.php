<?php 
/* 
Super container:
Create by Ifaliana Arimanana
ifaomega@gmail.com
26 Apr 2021
*/


/***************** Required *****************/
$keyTpl  ="container";
$myCmsId = $blockCms["_id"]->{'$id'};
$params  = $blockCms["params"] ?? $blockCms["params"] ?? [];
$subtype = $blockCms["subtype"] ?? $blockCms["subtype"] ?? "";
$paramsData = array();      

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
} 
/*************** End required ***************/

/*****************get image uploaded***************/
$initImage = Document::getListDocumentsWhere(
  array(
    "id"=> $myCmsId,
    "type"=>'cms',
    "subKey"=>'block',
  ), "image"
);

  $latestImg = isset($initImage["0"]["imagePath"])?$initImage["0"]["imagePath"]:"empty" ;

/*****************end get image uploaded***************/

/* Get settings */
$otherClass       = $params["class"]["other"] ?? $params["class"]["other"] ?? "";
$otherCss         = $params["css"]["other"] ?? $params["css"]["other"] ?? "text-align:center";
$top              = $params["css"]['position']["top"] ?? $params["css"]['position']["top"] ?? "";
$left             = $params["css"]['position']["left"] ?? $params["css"]['position']["left"] ?? "";
$width            = $params["css"]["size"]["width"] ?? $params["css"]["size"]["width"] ?? "320px";
$objfit           = $params["css"]["object-fit"] ?? $params["css"]["object-fit"] ?? "none";
$height           = $params["css"]["size"]["height"] ?? $params["css"]["size"]["height"] ?? "80px";
$backgroundColor  = $params["css"]["background"]["color"] ?? $params["css"]["background"]["color"] ?? "#f0f0f0";
// var_dump($array_position);


  /**********Border**********/
$borderColor                = $params["css"]["border"]["color"] ?? $params["css"]["border"]["color"] ?? "transparent";
$borderRadiustopLeft        = $params["css"]["border"]["radius"]["top-left"] ?? $params["css"]["border"]["radius"]["top-left"] ?? "0";
$borderRadiustopRight       = $params["css"]["border"]["radius"]["top-right"] ?? $params["css"]["border"]["radius"]["top-right"] ?? "0";
$borderRadiusbottomRight    = $params["css"]["border"]["radius"]["bottom-right"] ?? $params["css"]["border"]["radius"]["bottom-right"] ?? "0";
$borderRadiusbottomLeft     = $params["css"]["border"]["radius"]["bottom-left"] ?? $params["css"]["border"]["radius"]["bottom-left"] ?? "0";
$borderRadius               = max($borderRadiustopLeft,$borderRadiustopRight,$borderRadiusbottomRight,$borderRadiusbottomLeft);
  /********End border********/

  /**********Padding**********/
$paddingtop     = $params["css"]["padding"]["top"] ?? $params["css"]["padding"]["top"] ?? "30";
$paddingright   = $params["css"]["padding"]["right"] ?? $params["css"]["padding"]["right"] ?? "0";
$paddingleft    = $params["css"]["padding"]["left"] ?? $params["css"]["padding"]["left"] ?? "0";
$paddingbottom  = $params["css"]["padding"]["bottom"] ?? $params["css"]["padding"]["bottom"] ?? "0";
$padding        = max($paddingtop,$paddingright,$paddingleft,$paddingbottom);
  /********End padding********/

  /**********Margin**********/
$margintop     = $params["css"]["margin"]["top"] ?? $params["css"]["margin"]["top"] ?? "0";
$marginright   = $params["css"]["margin"]["right"] ?? $params["css"]["margin"]["right"] ?? "0";
$marginleft    = $params["css"]["margin"]["left"] ?? $params["css"]["margin"]["left"] ?? "0";
$marginbottom  = $params["css"]["margin"]["bottom"] ?? $params["css"]["margin"]["bottom"] ?? "0";
$margin        = max($margintop,$marginright,$marginleft,$marginbottom);
  /********End margin********/

  /**********shadow**********/
  $inset    = $params["css"]["box-shadow"]["inset"] ?? $params["css"]["box-shadow"]["inset"] ?? "";
  $shwColor = $params["css"]["box-shadow"]["color"] ?? $params["css"]["box-shadow"]["color"] ?? "transparent";
  $axeX     = $params["css"]["box-shadow"]["x"] ?? $params["css"]["box-shadow"]["x"] ?? "0";
  $axeY     = $params["css"]["box-shadow"]["y"] ?? $params["css"]["box-shadow"]["y"] ?? "0";
  $blur     = $params["css"]["box-shadow"]["blur"] ?? $params["css"]["box-shadow"]["blur"] ?? "0";
  $spread   = $params["css"]["box-shadow"]["spread"] ?? $params["css"]["box-shadow"]["spread"] ?? "0";
  /********End shadow********/

  /**********Link & text**********/
  $text = $params["text"] ?? $params["text"] ?? "Button";
  $link = $params["link"]["url"] ?? $params["link"]["url"] ?? "";
  $type = $params["link"]["type"] ?? $params["link"]["type"] ?? "";
  $blank = $params["link"]["blank"] ?? $params["link"]["blank"] ?? "";
  /********End Link & text********/
/* End get settings */

?>
<style type="text/css">

  .<?= $kunik ?> {  
    width:100%;
    height: inherit;
    /*position: sticky; */
    background-image: url('<?php echo $latestImg ?>');
    <?php 
    if ($borderColor !== "transparent") 
    {
      echo "border: solid ".$borderColor.";";  
    }

     ?>
    background-color: <?= $backgroundColor ?>;    
    border-radius: <?= $borderRadiustopLeft ?>px <?= $borderRadiustopRight ?>px <?= $borderRadiusbottomRight ?>px <?= $borderRadiusbottomLeft ?>px; 
    margin: <?= $margintop ?>px <?= $marginright ?>px <?= $marginbottom ?>px <?= $marginleft ?>px;
    <?php 
      echo "-webkit-box-shadow: ".$inset." ".$shwColor." ".$axeX."px ".$axeY."px ".$blur."px ".$spread."px;"; 
      echo "-moz-box-shadow: ".$inset." ".$shwColor." ".$axeX."px ".$axeY."px ".$blur."px ".$spread."px;"; 
      echo "box-shadow: ".$inset." ".$shwColor." ".$axeX."px ".$axeY."px ".$blur."px ".$spread."px;"; 

     ?>
  }

  .<?= $kunik ?>:hover {
    cursor: pointer;
  }


   .btn-<?= $kunik ?> {  
    width : <?= $width ?>;
    height: <?= $height ?>;  
    padding: <?= $paddingtop ?>px <?= $paddingright ?>px <?= $paddingbottom ?>px <?= $paddingleft ?>px;
    border-radius: <?= $borderRadiustopLeft ?>px <?= $borderRadiustopRight ?>px <?= $borderRadiusbottomRight ?>px <?= $borderRadiusbottomLeft ?>px; 
  }

  .other-css-<?= $kunik ?> {  
    <?php 
    if (is_array($otherCss)) {
     foreach ($otherCss as $csskey => $cssvalue) {
      echo $csskey.":".$cssvalue.";\r\n";
    }
  }else{
    echo $otherCss;
  } ?>
}

  .edit-<?= $kunik ?> {
    border: dashed 2px gray;
  }

  .selected-mode-<?= $kunik ?> {
    box-shadow: #92bfffa6 0px 0px 2px 2px;
    border : 2px dotted #479dff;
  }

  .super-<?= $myCmsId ?>{
    width : <?= $width ?>;
    min-height: <?= $width ?>;
  }

/*  #toolsBar {
    left: 20%;
    top: 40%;
  }

  #toolsBar a{
    cursor: pointer;
  }*/

  .cards-list {
    z-index: 0;
    width: 100%;
    display: flex;
    justify-content: space-around;
    flex-wrap: wrap;
  }

  .card {
    margin: 30px auto;
    width: 110px;
    height: 110px;
    border-radius: 10px;
    box-shadow:  0px 0px 6px 0px rgb(0 0 0 / 22%);
    cursor: pointer;
    transition: 0.4s;
  }

  .card .card_image {
    width: inherit;
    height: inherit;
    border-radius: 10px;
  }

  .card .card_image img {
    width: inherit;
    height: inherit;
    border-radius: 10px;
    object-fit: cover;
  }

  .card .card_title {
    text-align: center;
    font-family: sans-serif;
    font-weight: bold;
    font-size: 30px;
    margin-top: -80px;
  }

  .card:hover {
    transform: scale(0.9, 0.9);
    box-shadow: 5px 5px 30px 15px rgba(0,0,0,0.25), 
    -5px -5px 30px 15px rgba(0,0,0,0.22);
  }

  .title-white {
    color: white;
    top: -20px;
    position: relative;
  }

  .title-black {
    background-color: white;
    color: black;
    border-radius: 10px 10px 0px 0px;
    top:-30px;
    position: relative;
  }

  @media all and (max-width: 500px) {
    .card-list {
      /* On small screens, we are no longer using row direction but column */
      flex-direction: column;
    }
  }
  .super-cms-range-slider{ 
    -webkit-appearance: none;
    background: <?= $shwColor."6b" ?>;
    border: solid 1px #999;
    outline: none;
    opacity: 0.7;
    height: 10px;
    -webkit-transition: .2s;
    transition: opacity .2s;
  }

  .super-cms-range-slider::-webkit-slider-thumb {
    -webkit-appearance: none;
    appearance: none;
    width: 20px;
    height: 20px;
    background: black;
    cursor: pointer;
    border: solid 2px darkgray;
    border-radius: 50%;
  }

  .flex-container {
    /*display: flex;*/
    flex-direction: row;
    /*font-size: 30px;*/
    /*text-align: center;*/
  }

  .col-elem-value { 
    display: flex;
    flex-wrap: wrap;
  }

  .element-field {
    background-color: #47525d0d;
    padding: 10px;
    flex: 50%;
  }

  @media (max-width: 800px) {
    .elem-value {
      flex-direction: column;
    }
    .<?= $kunik ?> {
      left : 0 !important;
    }
    .whole-<?= $kunik ?> {
      width : 100% !important;
      height: <?= $height ?>;
    }
  }
  .bs {
    height: 100%;
    width: 100%;
    position: absolute;
    right: 0;
    top: 0;

    border-color: transparent;
    text-decoration: none !important;
    font-family: inherit !important;

  }
  .whole-<?= $kunik ?> {
    width : <?= $width ?>;
    height: <?= $height ?>;
  }
 .super-container a {
  color: inherit;
 }
</style>
<?php //if ($blockCms["type" != "blockCopy"]) { ?>
 <?php// }else{ } ?>
  <div class="whole-<?= $kunik ?> unselectable" style="margin-top:<?= $top ?>">
    <div class="super-container <?= $kunik ?> <?= $otherClass ?> other-css-<?= $kunik ?> super-cms" style="position:relative;left:<?= $left ?>">
      <a class="bs btn-<?= $kunik ?> <?= $type ?>" onclick="doThis<?= $kunik ?>()"><?= $text ?></a>
    </div>
</div>

<script type="text/javascript">
  let previewMode = true;
  if (localStorage.getItem("previewMode") == "w") {  
   previewMode = false;
 }else{    
   previewMode = true;
 } 
  if ("supercms" !== "<?= $subtype ?>" ) {
    tplCtx = {};
    tplCtx.id = "<?php echo $myCmsId ?>";
    tplCtx.collection = "cms";
    tplCtx.path = "allToRoot"; 
    tplCtx.value = {};
    tplCtx.value.subtype = "supercms";
    dataHelper.path2Value( tplCtx, function(params) { } );
  }

 var link<?= $kunik ?>      = "<?= $link ?>";
 var lbh<?= $kunik ?>       = "<?= $type ?>";
 var blank<?= $kunik ?>     = "<?= $blank ?>";
<?php if(Authorisation::isInterfaceAdmin()){?>
sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
 var margintop<?= $kunik ?> = parseInt($(".whole-<?= $kunik ?>").css("margin-top"));
 /*********Initialize color picker*********/
function loadColorPicker(callback) {
  mylog.log("loadColorPicker");
  if( ! jQuery.isFunction(jQuery.ColorPicker) ) {
    mylog.log("loadDateTimePicker2");
    lazyLoad( baseUrl+'/plugins/colorpicker/js/colorpicker.js', 
      baseUrl+'/plugins/colorpicker/css/colorpicker.css',
      callback);
  }
}
function loadSpectrumColorPicker(callback) {
  mylog.log("loadSpectrumColorPicker");
  lazyLoad( baseUrl+'/plugins/spectrum-colorpicker2/spectrum.min.js', 
    baseUrl+'/plugins/spectrum-colorpicker2/spectrum.min.css',
    callback);

}
var initColor = function(){
  mylog.log("init .colorpickerInput");
  $(".colorpickerInput").ColorPicker({ 
    color : "pink",
    onSubmit: function(hsb, hex, rgb, el) {
      $(el).val(hex);
      $(el).ColorPickerHide();
    },
    onBeforeShow: function () {
      $(this).ColorPickerSetColor(this.value);
    }
  }).bind('keyup', function(){
    $(this).ColorPickerSetColor(this.value);
  });
};

jQuery.fn.removeClassExcept = function (val) {
  return this.each(function (index, el) {
    var keep = val.split(" "),
    reAdd = [],
    $el = $(el);         
    for (var i = 0; i < keep.length; i++){
      if ($el.hasClass(keep[i])) reAdd.push(keep[i]);
    }          
    $el
    .removeClass()
    .addClass(reAdd.join(' '));
  });
};


/*********End color picker initialization*********/
  jQuery(document).ready(function() {
/*-----View and edit mode------- */
   let superCms<?= $kunik ?> = {
      viewMode: function(){
        // $(".<?= $kunik ?>").addClass("default-<?= $kunik ?>");
        $(".<?= $kunik ?>").removeClass("edit-<?= $kunik ?>");
        previewMode = true;
       // mode = "v";
        // $(".<?= $kunik ?>").draggable("destroy").resizable("destroy");
        // $(".<?= $kunik ?>").draggable("destroy").resizable("destroy");
        // $(".<?= $kunik ?>").removeClass("selected-mode-<?= $kunik ?>");
        $(".btn-<?= $kunik ?>").attr("href", link<?= $kunik ?>);
        $(".btn-<?= $kunik ?>").addClass(lbh<?= $kunik ?>);
      },
      editMode: function(){
        $(".<?= $kunik ?>").addClass("edit-<?= $kunik ?>");
        previewMode = false;
        // $(".<?= $kunik ?>").removeClass("default-<?= $kunik ?>");
        // $(".<?= $kunik ?>").removeClass("selected-mode-<?= $kunik ?>");
        $(".btn-<?= $kunik ?>").attr("href", "javascript:void(0);");
        $(".btn-<?= $kunik ?>").removeClass(lbh<?= $kunik ?>);
      },
      selectetMode: function(){
        $(".<?= $kunik ?>").removeClass("edit-<?= $kunik ?>");
        // $(".<?= $kunik ?>").removeClass("default-<?= $kunik ?>");
        // $(".<?= $kunik ?>").addClass("selected-mode-<?= $kunik ?>");
      }
    }

    $(document).keyup(function(e) {
      if (e.altKey == true && e.keyCode == 80) {
        if (previewMode == false) {
          superCms<?= $kunik ?>.viewMode();
          $("#toolsBar").hide();   
        }else{
          superCms<?= $kunik ?>.editMode();
          $("#toolsBar").hide();
        }
      }
    });


    $(".view-super-cms").click(function(){
      superCms<?= $kunik ?>.viewMode();
      $("#toolsBar").hide();   
    });

    $(".hiddenEdit").click(function(){
      if (previewMode == false) {
        superCms<?= $kunik ?>.viewMode();
        $("#toolsBar").hide();   
      }else{
        superCms<?= $kunik ?>.editMode();
        $("#toolsBar").hide();
      }
    })

    $(document).keyup(function(e) {
      if (e.key === "Escape") { 
       superCms<?= $kunik ?>.editMode();
      $("#toolsBar").hide();
      }
    });

/*-----End view and edit mode------- */
  if(previewMode === false ){
    superCms<?= $kunik ?>.editMode();
  }else{
    superCms<?= $kunik ?>.viewMode();
  }


  /*********************Mouse hover edit mode*********************/
//   $('.<?= $kunik ?>').mouseover(function(e){
//     if (mode === "w" && $("#toolsBar").is(":hidden")) {
//       e.stopPropagation();
//       $(this).addClass("edit-<?= $kunik ?>");
//       $(".container<?= @$blockCms['tplParent'] ?>").addClass("edit-container<?= @$blockCms['tplParent'] ?>");
//       $(".container<?= @$blockCms['tplParent'] ?>").removeClass("selected-mode-container<?= @$blockCms['tplParent'] ?>");

//       $(".<?= $kunik ?>").removeClass("edit-<?= $kunik ?>");
//       $(".<?= $kunik ?>").addClass("selected-mode-<?= $kunik ?>");
//     }
//   });

//   $('.<?= $kunik ?>').mouseleave(function(e){
//    if (mode === "w" && $("#toolsBar").is(":hidden")) {
//     e.stopPropagation();
//     $(".<?= $kunik ?>").removeClass("edit-<?= $kunik ?>");
//     $(".<?= $kunik ?>").addClass("selected-mode-<?= $kunik ?>");

//     $(".<?= $kunik ?>").addClass("edit-<?= $kunik ?>");
//     $(".<?= $kunik ?>").removeClass("selected-mode-<?= $kunik ?>");
//   }
// });
  /*********************End mouse hover edit mode*********************/

  $("#toolsBar").draggable().resizable();
/***************Drag and resize*******************/
// Desable drag and resize when view mode
  <?php   if ($blockCms["type"] === "blockChild") { ?>
    $('.edit-<?= $kunik ?>').mouseenter(function() {      
      $(".<?= $kunik ?>").draggable();
      $(".<?= $kunik ?>").resizable();
    if (previewMode === false) {
      $(".<?= $kunik ?>").draggable('enable');
      $(".<?= $kunik ?>").resizable('enable');
      $(".edit-<?= $kunik ?>").draggable({
        cancel: '.editable',
        opacity: 0.50,
        start : function(event, ui){      
          $("#toolsBar").html(``);
          $("#toolsBar").hide(); 
        },
        stop: function(event, ui){
      //convert position from px to %
      var myLeft = $(".<?= $kunik ?>").css("left");
      var myTop = $(".<?= $kunik ?>").css("top");
      myLeft = parseInt(myLeft);
      myTop  = parseInt(myTop);

      var parentWidth = $(".whole-<?= $kunik ?>").width()-20;
      LeftPrnt = (myLeft / parentWidth) *100;
      $(".<?= $kunik ?>").css("left", (LeftPrnt)+"%");
      $(".<?= $kunik ?>").css("top", "");
      margintop<?= $kunik ?> =  myTop+margintop<?= $kunik ?>;
      $(".whole-<?= $kunik ?>").css("margin-top", margintop<?= $kunik ?>+"px");
      // alert(margintop<?= $kunik ?>);
      revert: true,
      tplCtx = {};
      tplCtx.id = "<?php echo $myCmsId ?>";
      tplCtx.collection = "cms";
      tplCtx.path = "params.css.position"; 
      tplCtx.value = {};
      tplCtx.value.top = margintop<?= $kunik ?>+"px";
      tplCtx.value.left = LeftPrnt+"%";

      dataHelper.path2Value( tplCtx, function(params) {} );
    }
  });
      $(".edit-<?= $kunik ?>").resizable({
        stop: function(event, ui){
          superCms<?= $kunik ?>.editMode();
          var myWidth = $(".<?= $kunik ?>").width();
          var myHeight = $(".<?= $kunik ?>").height();
          var parentWidth = $(".container<?= $blockCms['tplParent'] ?>").width();
          if ("<?= $blockCms['type'] ?>" === "blockCopy") {
            parentWidth = $( window ).width();
          }
          widthPrnt = (myWidth / parentWidth) * 100;
          $(".whole-<?= $kunik ?>").css("height", myHeight+"px");
          $(".whole-<?= $kunik ?>").css("width", (widthPrnt+2)+"%");
          $(".<?= $kunik ?>").css("width", "");
          revert: true,
          tplCtx = {};
          tplCtx.id = "<?php echo $myCmsId ?>";
          tplCtx.collection = "cms";
          tplCtx.path = "params.css.size"; 
          tplCtx.value = {};
          tplCtx.value.width = widthPrnt+"%";
          tplCtx.value.height = myHeight+"px";

          dataHelper.path2Value( tplCtx, function(params) {} );
        }
      });
    }else{
      $(".<?= $kunik ?>").draggable('disable');
      $(".<?= $kunik ?>").resizable('disable');
    }
    });
<?php } ?> 
/*****************End drag and resize*****************/

/********************Display menu**********************/
// $(document){}
// $("document").on("click", ".edit-<?= $kunik ?>", function(){
//   alert("yes");
// })

$(".edit-<?= $kunik ?>").on( "click",function(){
  if (previewMode === false) {   
  //   $("#toolsBar").hide();   
  $("#toolsBar").html("");
  if(last_edited != ""){
    $("."+last_edited).addClass("edit-"+last_edited);    
  }
  last_edited = "<?= $kunik ?>";
  //   superCms<?= $kunik ?>.editMode();
  // }else{
    superCms<?= $kunik ?>.selectetMode();
    $("#toolsBar").show();  
    $("#toolsBar").append(
      `
       <div class="container-fluid">
      <div class="text-center">
        <i style="cursor:default;font-size: large;" aria-hidden="true">Paramètres de bouton</i>
      <button class="bg-transparent deleteLine delete-super-cms tooltips" data-path="<?= $blockCms['path'] ?>" data-id="<?= $blockCms['_id']; ?>" data-collection="cms" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Supprimer cet élément">
        <i class="fa fa-trash text-red" aria-hidden="true"></i>
      </button>
        <i class="fa fa-window-close closeBtn view-super-cms" aria-hidden="true"></i>
      </div>

      <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#home">Général</a></li>
        <li><a data-toggle="tab" href="#menu1">Ombre</a></li>
        <li><a data-toggle="tab" href="#menu2">Autre class</a></li>
        <li><a data-toggle="tab" href="#menu3">Autre css</a></li>
      </ul>


      <div class="tab-content padding-top-20">
        <div id="home" class="tab-pane fade in active">
          <div class="row">
            <div class="col-md-6">      
              <div class="panel-group">
                <label>Rayon de la bordure</label>                      
                <div class="input-group">
                  <input class="form-control elem-control" type="number" min="0" value="<?= $borderRadius ?>" placeholder="0" data-path="border.radius" data-given="border-radius" data-what="border-control"> 
                  <div class="input-group-btn" style="width: 0px">                    
                    <button  data-toggle="collapse" data-target="#border" class="tooltips btn btn-primary" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Plus de réglage">
                      <i class="fa fa-plus" aria-hidden="true"></i><br>
                    </button> 
                  </div>
                </div>
                <div id="border" class="collapse elem-value">
                <div class="col-elem-value">
                  <div class="element-field"> 
                    <label>Haut à gauche</label>                
                    <input class="form-control top-left border-control" type="number" min="0" value="<?= $borderRadiustopLeft ?>" placeholder="0" data-path="border.radius" data-given="border-top-left-radius"> 
                  </div>                  
                  <div class="element-field">      
                    <label>Haut à droite</label>                   
                    <input class="form-control top-right border-control" type="number" min="0" value="<?= $borderRadiustopRight ?>" placeholder="0" data-path="border.radius" data-given="border-top-right-radius"> 
                  </div>
                   <div class="element-field"> 
                    <label>Bas à gauche</label>                           
                    <input class="form-control bottom-left border-control" type="number" min="0" value="<?= $borderRadiusbottomLeft ?>" data-path="border.radius" data-given="border-bottom-left-radius" placeholder="0"> 
                  </div>                  
                  <div class="element-field">      
                    <label>Bas à droite</label>                   
                    <input class="form-control bottom-right border-control" type="number" min="0" value="<?= $borderRadiusbottomRight ?>" data-path="border.radius" data-given="border-bottom-right-radius" placeholder="0"> 
                  </div>
                </div>
                </div>
              <label>Couleur de la bordure</label>
              <input id="border-color" class="sp-color-picker spectrum sp-colorize form-control" name="" value="<?= $borderColor ?>" placeholder="#ffffff" style="">
              <label>Ajustement du fond (Photo)</label>
                <select class="form-control" id="objfit">
                  <option value="none">None</option>
                  <option value="auto">Auto</option>
                  <option value="cover">Cover</option>
                  <option value="inherit">Inherit</option>
                  <option value="contain">Contain</option>
                </select>
              </div>  
            </div>
            <div class="col-md-6">      
              <div class="panel-group">
                <div >          
                  <div class="">
                    <div class="">  
                      <label>Rembourrage (Padding)</label>
                      <div class="input-group">
                      <input class="form-control elem-control" type="number" min="0" value="<?= $padding ?>" placeholder="0" data-path="padding" data-given="padding" data-what="padding-control"> 
                        <div class="input-group-btn" style="width: 0px">                           
                          <button  data-toggle="collapse" data-target="#padding" class="tooltips btn btn-primary" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Plus de réglage">
                            <i class="fa fa-plus" aria-hidden="true"></i><br>
                          </button> 
                        </div>
                      </div>
                      <div id="padding" class="collapse elem-value">
                        <div class="col-elem-value">
                          <div class="element-field" style="width:100px"> 
                            <label>En haut</label>                
                            <input class="form-control top-left padding-control" type="number" min="0" value="<?= $paddingtop ?>" placeholder="0" data-path="padding" data-given="padding-top"> 
                          </div>                  
                          <div class="element-field" style="width:100px">      
                            <label>À droite</label>                   
                            <input class="form-control top-right padding-control" type="number" min="0" value="<?= $paddingright ?>" placeholder="0" data-path="padding" data-given="padding-right"> 
                          </div>
                           <div class="element-field" style="width:100px"> 
                            <label>À gauche</label>                           
                            <input class="form-control bottom-left padding-control" type="number" min="0" value="<?= $paddingleft ?>" data-path="padding" data-given="padding-left" placeholder="0"> 
                          </div>                  
                          <div class="element-field" style="width:100px">      
                            <label>En bas</label>                   
                            <input class="form-control bottom-right padding-control" type="number" min="0" value="<?= $paddingbottom ?>" data-path="padding" data-given="padding-bottom" placeholder="0"> 
                          </div>
                        </div>
                      </div> 
                      <label>Marge</label>
                      <div class="input-group">
                      <input class="form-control elem-control" type="number" min="0" value="<?= $margin ?>" placeholder="0" data-path="margin" data-given="margin" data-what="margin-control"> 
                        <div class="input-group-btn" style="width: 0px">                           
                          <button  data-toggle="collapse" data-target="#margin" class="tooltips btn btn-primary" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Plus de réglage">
                            <i class="fa fa-plus" aria-hidden="true"></i><br>
                          </button> 
                        </div>
                      </div>
                      <div id="margin" class="collapse elem-value">
                        <div class="col-elem-value">
                          <div class="element-field" style="width:100px"> 
                            <label>En haut</label>                
                            <input class="form-control top-left margin-control" type="number" value="<?= $margintop ?>" placeholder="0" data-path="margin" data-given="margin-top"> 
                          </div>                  
                          <div class="element-field" style="width:100px">      
                            <label>À droite</label>                   
                            <input class="form-control top-right margin-control" type="number" value="<?= $marginright ?>" placeholder="0" data-path="margin" data-given="margin-right"> 
                          </div>
                           <div class="element-field" style="width:100px"> 
                            <label>À gauche</label>                           
                            <input class="form-control bottom-left margin-control" type="number" value="<?= $marginleft ?>" data-path="margin" data-given="margin-left" placeholder="0"> 
                          </div>                  
                          <div class="element-field" style="width:100px">      
                            <label>En bas</label>                   
                            <input class="form-control bottom-right margin-control" type="number" value="<?= $marginbottom ?>" data-path="margin" data-given="margin-bottom" placeholder="0"> 
                          </div>
                        </div>
                      </div>
                    <label>Couleur du fond</label>
                    <div class="input-group">
                    <input id="fond-color" class="sp-color-picker spectrum sp-colorize form-control" name="" value="<?= $backgroundColor ?>" placeholder="#ffffff" style="">
                        <div class="input-group-btn closeMenu" style="width: 0px">                    
                          <button class="edit-img<?php echo $kunik ?>myCmsId tooltips btn btn-primary" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Plus de réglage">
                            <i class="fa fa-camera" aria-hidden="true"></i><br>
                          </button> 
                        </div>
                      </div>                    
                    </div>  
                  </div>  
                </div>
              </div>
            </div>

            <div class="col-md-12">   
            <label>Texte</label> 
            <input class="form-control super-btn-text" name="" value="<?= $text ?>" placeholder="Button">
            <label>Lien</label>   
              <div class="panel-group">
                <div class="row">          
                  <div class="col-md-6">
                  <input class="form-control super-btn-link" name="" value="" placeholder="#">
                  </div>        
                  <div class="col-md-6">  
                    <div class="form-check mb-2 mr-sm-2">
                      <input class="form-check-input check-lbh" type="checkbox">
                      <label class="form-check-label" for="inlineFormCheck">
                      Load by hash
                      </label>
                    </div>
                    <div class="form-check mb-2 mr-sm-2">
                      <input class="form-check-input check-target-blank" type="checkbox">
                      <label class="form-check-label" for="inlineFormCheck">
                      Ouvrir dans une nouvelle page
                      </label>
                    </div>
                  </div>
                </div>
              </div>
            </div>        
          </div>  
        </div>

        <div id="menu1" class="tab-pane fade">
          <div class="col-md-12">      
            <div class="panel-group">        
            <label>Couleur</label>
            <div class="pull-right">
            <input id="check-inset" type="checkbox" <?php if ($inset != "") {echo 'checked="checked"';} ?>>
            <label for="check1">Inset</label>
            <p></p>
            </div>
            <input id="shw-color" class="sp-color-picker spectrum sp-colorize form-control" name="" value="<?= $shwColor ?>" placeholder="#ffffff" style="">
              <div class="range-slider text-center padding-top-20">
                <span class="range-blur__value">Blur <?= $blur ?>px</span>
                <input class="range-slider-blur super-cms-range-slider" type="range" value="<?= $blur ?>" min="0" max="20" step="0.2">
              </div>
              <div class="range-slider text-center padding-top-20">
                <span class="range-spread__value">Spread <?= $spread ?>px</span>
                <input class="range-slider-spread super-cms-range-slider" type="range" value="<?= $spread ?>" min="0" max="20" step="0.2">
              </div>
              <div class="range-slider text-center padding-top-20">
                <span class="range-x__value">Axe X: <?= $axeX ?>px</span>
                <input class="range-slider-x super-cms-range-slider" type="range" value="<?= $axeX ?>" min="-10" max="10" step="0.2">
              </div>
              <div class="range-slider text-center padding-top-20">
                <span class="range-y__value">Axe Y: <?= $axeY ?>px</span>
                <input class="range-slider-y super-cms-range-slider" type="range" value="<?= $axeY ?>" min="-10" max="10" step="0.2">
              </div>
            </div>
          </div>
        </div>
        
        <div id="menu2" class="tab-pane fade">    
          <div class="col-md-12">      
            <div class="panel-group">
              <div class="form-group">
                <label for="">Class bootstrap <span class="text-red" style="font-weight:normal">(Réglage pour le dévéloppeur)<span></label>
                <textarea class="form-control other-class" rows="5"><?= $otherClass ?></textarea>
              </div>
            </div>
          </div>
        </div>

        <div id="menu3" class="tab-pane fade">
          <div class="">        
            <div class="panel-group">
              <div class="form-group">
                <label for="">Styles css <span class="text-red" style="font-weight:normal">(Réglage pour le dévéloppeur)<span></label>
                <textarea class="form-control other-css" rows="5"><?php 
                if (is_array($otherCss)) {
                 foreach ($otherCss as $csskey => $cssvalue) {
                  echo $csskey.":".$cssvalue.";\r\n";
                }
              }else{
                echo $otherCss;
              } ?></textarea>
                <p class="css_err"></p>
              </div>
            </div>
          </div>
        </div>

      </div> 
    </div> 
  `
  );

  cmsBuilder.init();
    $("#toolsBar").click(function(){
     // superCms<?= $kunik ?>.viewMode();
   });

    $(".closeBtn, .closeMenu").click(function(){
      superCms<?= $kunik ?>.editMode();
      $("#toolsBar").hide();
      $(".<?= $kunik ?>").css("cursor", "default");
    });

  }         

/* Upload d'image */
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "Importation d'image",
            "description" : "Personnaliser votre bloc",
            "icon" : "fa-cog",
            
            "properties" : {    
              "image" :{
               "inputType" : "uploader",
               "label" : "image",
               "docType": "image",
               "contentKey" : "slider",
               "itemLimit" : 1,
               "endPoint": "/subKey/block",
               "domElement" : "image",
               "filetypes": ["jpeg", "jpg", "gif", "png"],
               "label": "Image :",
               "showUploadBtn": false,
               initList : <?php echo json_encode($initImage) ?>
              },
            },
            beforeBuild : function(){
                uploadObj.set("cms","<?php echo $myCmsId ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "items")
                  tplCtx.value[k] = data.items;
              });
              
              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouté");
                      $("#ajax-modal").modal('hide');
                      urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit-img<?php echo $kunik ?>myCmsId").off().on("click",function() {  
          tplCtx.subKey = "imgParent";
          tplCtx.id = "<?php echo $myCmsId ?>";
          tplCtx.collection = "cms";
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, null);
        });
/* End upload d'image */

/**********Color settings *************/
  if(  $(".colorpickerInput").length){
    loadColorPicker(initColor);
  }

  loadSpectrumColorPicker(function(){
    if(  $(".sp-color-picker").length){
      $(".sp-color-picker").spectrum({
        type: "text",
        showInput: "true",
        showInitial: "true"
      });
      $('.sp-color-picker').addClass('form-control');
      $('.sp-original-input-container').css("width","100%");
    }
  });

  $( "#fond-color" ).change(function() {
    $(".btn-<?= $kunik ?>" ).css("background-color",$("#fond-color").val());   
    tplCtx = {};
    tplCtx.id = "<?php echo $myCmsId ?>";
    tplCtx.collection = "cms";
    tplCtx.path = "params.css.background.color"; 
    tplCtx.value = $("#fond-color").val();

    dataHelper.path2Value( tplCtx, function(params) {} );
  });

  $( "#border-color" ).change(function() {
   $(".<?= $kunik ?>" ).css("border-color",$("#border-color").val());
   tplCtx = {};
   tplCtx.id = "<?php echo $myCmsId ?>";
   tplCtx.collection = "cms";
   tplCtx.path = "params.css.border.color"; 
   tplCtx.value = $("#border-color").val();

   dataHelper.path2Value( tplCtx, function(params) {} );
 });

 /*********End color settings*********/

 /************Border**padding***margin*************/
 var thisetting = {};
 
 $(".border-control, .padding-control" ).on('keyup change', function (){
  $('.elem-value .border-control, .padding-control').each(function(k,v){
    pars   = $(this).val() ? parseInt($(this).val()) : '0';
    thisIs = $(this).data("given");
    $(".<?= $kunik ?>" ).css(thisIs,pars+"px");
    $(this).val(pars);
    thisetting[thisIs.replace('-radius','').replace('border-','').replace('padding-','').replace('margin-','')] = $(this).val();
  })
});
  $(".padding-control" ).on('keyup change', function (){
  $('.elem-value .padding-control').each(function(k,v){
    pars   = $(this).val() ? parseInt($(this).val()) : '0';
    thisIs = $(this).data("given");
    $(".btn-<?= $kunik ?>" ).css(thisIs,pars+"px");
    $(this).val(pars);
    thisetting[thisIs.replace('-radius','').replace('border-','').replace('padding-','').replace('margin-','')] = $(this).val();
  })
});
  $(".margin-control" ).on('keyup change', function (){
  $('.elem-value .margin-control').each(function(k,v){
    pars   = $(this).val() ? parseInt($(this).val()) : '0';
    thisIs = $(this).data("given");
    $(".btn-<?= $kunik ?>" ).css(thisIs,pars+"px");
    $(this).val(pars);
    thisetting[thisIs.replace('-radius','').replace('border-','').replace('padding-','').replace('margin-','')] = $(this).val();
  })
});


 $(".elem-control" ).on('keyup change', function (){
    globalConf = $(this).val() ? parseInt($(this).val()) : '0';
    $("."+$(this).data("what")).val(globalConf);
    $(".<?= $kunik ?> " ).css($(this).data("given"),globalConf+"%");
    $(".btn-<?= $kunik ?> " ).css($(this).data("given"),globalConf+"%");

    $('.elem-value .'+$(this).data("what")).each(function(k,v){
      pars   = $(this).val() ? parseInt($(this).val()) : '0';
      thisIs = $(this).data("given");
      $(".<?= $kunik ?>" ).css(thisIs,pars+"px");
      $(this).val(pars);
      thisetting[thisIs.replace('-radius','').replace('border-','').replace('padding-','').replace('margin-','')] = $(this).val();
    })
  });

 $( ".elem-control, .elem-value .form-control" ).blur(function() {
  tplCtx = {};
  tplCtx.id = "<?php echo $myCmsId ?>";
  tplCtx.collection = "cms";
  tplCtx.path = "params.css."+$(this).data("path"); 
  tplCtx.value = thisetting;

  dataHelper.path2Value( tplCtx, function(params) {} );
});

 /************End border**padding***margin***********/

  /***object-fit***/
 $( "#objfit" ).val("<?= $objfit ?>");
 $( "#objfit" ).change(function() {
  var myfit = $( "#objfit" ).val();
  $(".<?= $kunik ?>" ).css("background-size",myfit);
    tplCtx = {};
    tplCtx.id = "<?php echo $myCmsId ?>";
    tplCtx.collection = "cms";
    tplCtx.path = "params.css.object-fit"; 
    tplCtx.value = myfit;

    dataHelper.path2Value( tplCtx, function(params) {} );
  });
 /***End object-fit***/

  /***Link & text***/
 $( ".super-btn-link" ).val("<?= $link ?>");
 $( ".super-btn-link" ).blur(function() {
  link<?= $kunik ?> = $( ".super-btn-link" ).val();
    tplCtx = {};
    tplCtx.id = "<?php echo $myCmsId ?>";
    tplCtx.collection = "cms";
    tplCtx.path = "params.link"; 
    tplCtx.value = {};
    tplCtx.value.url = $( ".super-btn-link" ).val();
    tplCtx.value.type = $( ".super-btn-link" ).val();

    dataHelper.path2Value( tplCtx, function(params) {} );
  });

   $(".super-btn-text" ).on('keyup', function (){
   $(".btn-<?= $kunik ?>").text($(".super-btn-text" ).val());
  });

   if (lbh<?= $kunik ?> == "lbh") {
    $( ".check-lbh" ).prop( "checked", true );
  }else{    
    $( ".check-lbh" ).prop( "checked", false );
  }
     $( ".check-lbh" ).change(function() {
    if ($( ".check-lbh" ).is( ":checked" ) == true) {      
      lbh<?= $kunik ?> = "lbh";
    }else{
      lbh<?= $kunik ?> = "";
    }
    tplCtx = {};
    tplCtx.id = "<?php echo $myCmsId ?>";
    tplCtx.collection = "cms";
    tplCtx.path = "params.link.type"; 
    tplCtx.value = lbh<?= $kunik ?>;

    dataHelper.path2Value( tplCtx, function(params) {} );
  });

   if (blank<?= $kunik ?> == "blank") {
    $( ".check-target-blank" ).prop( "checked", true );
  }else{    
    $( ".check-target-blank" ).prop( "checked", false );
  }
     $( ".check-target-blank" ).change(function() {
    if ($( ".check-target-blank" ).is( ":checked" ) == true) {      
      blank<?= $kunik ?> = "blank";
    }else{
      blank<?= $kunik ?> = "";
    }
    tplCtx = {};
    tplCtx.id = "<?php echo $myCmsId ?>";
    tplCtx.collection = "cms";
    tplCtx.path = "params.link.blank"; 
    tplCtx.value = blank<?= $kunik ?>;

    dataHelper.path2Value( tplCtx, function(params) {} );
  });

  $( ".super-btn-text" ).blur(function() {
  text<?= $kunik ?> = $( ".super-btn-text" ).val();
    tplCtx = {};
    tplCtx.id = "<?php echo $myCmsId ?>";
    tplCtx.collection = "cms";
    tplCtx.path = "params.text"; 
    tplCtx.value = $( ".super-btn-text" ).val();

    dataHelper.path2Value( tplCtx, function(params) {} );
  });
 /***End Link & text***/

 /*Box shadow*/
   var eltBlur   = $('.range-slider-blur').val();
   var eltSpread = $('.range-slider-spread').val();
   var eltAxeX   = $('.range-slider-x').val();
   var eltAxeY   = $('.range-slider-y').val();
   var eltColor  = $("#shw-color").val();
   var inset     = "";

   $( "#check-inset" ).change(function() {
    if ($( "#check-inset" ).is( ":checked" ) == true) {      
      inset = "inset";
    }else{
      inset = "";
    }
    $(".<?= $kunik ?>" ).css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
     saveShadow<?= $kunik ?>();
  });

   $( "#shw-color" ).change(function() {
    eltColor = $("#shw-color").val();
    $(".<?= $kunik ?>" ).css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
    saveShadow<?= $kunik ?>();
  });
   $('.range-slider').mousedown(function(){
     $('.range-slider-blur').mousemove(function(){
      eltBlur = $('.range-slider-blur').val();
      $(".range-blur__value").html("Blur "+eltBlur+"px");
    $(".<?= $kunik ?>" ).css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
    });
     $('.range-slider-spread').mousemove(function(){
      eltSpread = $('.range-slider-spread').val();
      $(".range-spread__value").html("Spread "+eltSpread+"px");
    $(".<?= $kunik ?>" ).css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
    });
     $('.range-slider-x').mousemove(function(){
      eltAxeX = $('.range-slider-x').val();
      $(".range-x__value").html("Axe X "+eltAxeX+"px");
    $(".<?= $kunik ?>" ).css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
    });  
     $('.range-slider-y').mousemove(function(){
      eltAxeY = $('.range-slider-y').val();
      $(".range-y__value").html("Axe Y "+eltAxeY+"px");
    $(".<?= $kunik ?>" ).css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
    });
   });
   $('.range-slider').mouseup(function(){
    saveShadow<?= $kunik ?>();
   });
   function saveShadow<?= $kunik ?>(){
    tplCtx = {};
    tplCtx.id = "<?php echo $myCmsId ?>";
    tplCtx.collection = "cms";
    tplCtx.path = "params.css.box-shadow"; 
    tplCtx.value = {};
    tplCtx.value.inset =  inset;
    tplCtx.value.color =  eltColor;
    tplCtx.value.x     =  eltAxeX;
    tplCtx.value.y     =  eltAxeY;
    tplCtx.value.blur  =  eltBlur;
    tplCtx.value.spread =  eltSpread;

    dataHelper.path2Value( tplCtx, function(params) {} );
  }
 /*End box shadow*/

/****************Other class option****************/

  $(".other-class" ).on('keyup', function (){
   $(".<?= $kunik ?>").removeClassExcept("super-container <?= $kunik ?> super-cms <?= $otherClass ?> other-css-<?= $kunik ?> newCss-<?= $kunik ?>");
   $(".<?= $kunik ?>").addClass($(".other-class" ).val());
  });
  $(".other-class" ).on('blur', function (){
    tplCtx = {};
    tplCtx.id = "<?php echo $myCmsId ?>";
    tplCtx.collection = "cms";
    tplCtx.path = "params.class.other"; 
    tplCtx.value = $(".other-class" ).val();

    dataHelper.path2Value( tplCtx, function(params) {} );
  });
/*************End other class option****************/

/****************Other css option****************/
 var other_css = {};
  $(".other-css" ).on('keyup', function (){
    //Validate and apply css
       $(".<?= $kunik ?>").removeClass("newCss-<?= $kunik ?>"); 
       $(".<?= $kunik ?>").removeClass("other-css-<?= $kunik ?>");
       $("#myStyleId<?= $kunik ?>").remove();
      other_css = $(".other-css" ).val()
        var element  = document.createElement("style");
        element.id = "myStyleId<?= $kunik ?>" ;
        element.innerHTML = ".newCss-<?= $kunik ?> {"+other_css+"}" ;
        var header = document.getElementsByTagName("HEAD")[0] ;
        header.appendChild(element) ;
        $(".<?= $kunik ?>").addClass("newCss-<?= $kunik ?>"); 
  });

  $(".other-css" ).on('blur', function (){
    tplCtx = {};
    tplCtx.id = "<?php echo $myCmsId ?>";
    tplCtx.collection = "cms";
    tplCtx.path = "params.css.other"; 
    tplCtx.value = other_css;

    dataHelper.path2Value( tplCtx, function(params) {} );
  });
/*************End other css option****************/


});

/* End display menu */

$(function () {
  var count = 1;
  // Create click event handler on the document.
  $(document).on("click", function (event) {
    // If the target is not the container or a child of the container, then process
    // the click event for outside of the container.
   //  if ($(event.target).closest(".<?= $kunik ?> , #toolsBar").length === 0) {      
   //    $("#toolsBar").html("");
   //    $("#toolsBar").hide();    
   //    $(".<?= $kunik ?>").removeClass("edit-<?= $kunik ?>");
   //    $(".<?= $kunik ?>").addClass("<?= $kunik ?>");
   //    $(".<?= $kunik ?>").removeClass("selected-mode-<?= $kunik ?>");      
   //    $(".<?= $kunik ?>").resizable();
   //    $(".<?= $kunik ?>").resizable("destroy");
   // }
 });
  $(".<?= $kunik ?>").on("click", function () {
    $(".<?= $myCmsId ?>-tools").show();
  });
});
$('.<?= $kunik ?>').click(function(event){
  event.stopPropagation();
});

  sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {    
        "title" : "Configurer le fond de votre section",
        "description" : "Personnalisation de votre fond",
        "icon" : "fa-cog",
        
        "properties" : {       
        },
        beforeBuild : function(){
            uploadObj.set("cms","<?php echo (string)$blockCms["_id"] ?>");
        },
        save : function (data) {  
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
            tplCtx.value[k] = $("#"+k).val();
            if (k == "parent")
              tplCtx.value[k] = formData.parent;
          });
          console.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
            else {
              dataHelper.path2Value( tplCtx, function(params) {
                dyFObj.commonAfterSave(params,function(){
                  toastr.success("Élément bien ajouté");
                  $("#ajax-modal").modal('hide');
                  urlCtrl.loadByHash(location.hash);
                });
              } );
            }

        }
      }
    };
    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });
});    
   

<?php } ?> 

 function doThis<?= $kunik ?>(){
    
    <?php if(Authorisation::isInterfaceAdmin()){?>
      if (previewMode === true && link<?= $kunik ?> =="") {
        bootbox.alert({
          message: `<br>
          <div class="alert alert-warning text-center" role="alert" style="font-size: 18px;">
          <p class="padding-left-20">L'URL de ce bouton est vide!</p>
          </div>
          <p class="padding-left-20 text-center">Ce message s'affiche uniquement pour l'administrateur.</p>`
          ,
          size: 'medium'
        });
      } else if (previewMode === true ) {
        if (lbh<?= $kunik ?> =="lbh") {
          urlCtrl.loadByHash(link<?= $kunik ?>);
        }else if(blank<?= $kunik ?> === 'blank'){
          window.open( link<?= $kunik ?> );    
        }
      }
    <?php }else{ ?>
      if (link<?= $kunik ?> !="") {
        if (lbh<?= $kunik ?> =="lbh") {
          urlCtrl.loadByHash(link<?= $kunik ?>);
        }else if(blank<?= $kunik ?> === 'blank'){
          window.open( link<?= $kunik ?> );    
        }else{          
          window.location.href = link<?= $kunik ?>;
        }
      } 
    <?php  } ?> 
}
</script>