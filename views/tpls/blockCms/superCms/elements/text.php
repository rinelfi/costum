<?php 
/* 
Super text:
Create by Ifaliana Arimanana
ifaomega@gmail.com
26 Apr 2021
*/
    $keyTpl ="text";
    $paramsData = [
        "title"=>"",
        "text" => "",
        "fontSize" => "20"
    ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) {
        $paramsData[$e] = $blockCms[$e];
      }
    }
  }

  $myCmsId = $blockCms["_id"]->{'$id'};
  $subtype = $blockCms["subtype"] ?? $blockCms["subtype"] ?? "";

  $array_position = $blockCms['position'] ?? $blockCms['position'] ?? [];


  $params = array();

  if (!empty($blockCms["element"])) {
    $idElem = $blockCms["element"];
  }else{
    // $idElem = $blockCms["element"];
  }

?>

<style>
    .text-<?= $kunik?>{
        font-size: <?=$paramsData["fontSize"]?>pt !important;
        background-color: #f4f4f4;
    }
    .bold {
      font-weight: bold;
    }
    .italic{
     font-style: italic;
    }

    .right{
     text-align: right;
   }
   .center{
     text-align: center;
   }

   .left{
     text-align: left;
   }
   .justify{
     text-align: justify;
   }
   .form_font{
    display: block;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgb(0 0 0 / 8%);
    box-shadow: inset 0 1px 1px rgb(0 0 0 / 8%);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
}



#container #containerHeader{
   /* text-align: center;
    cursor:move;*/
}
#container #<?= $myCmsId ?>textShow {
    border: 1px solid grey;
    height: 100px;
    width: 602px;
    margin: 0px auto 0;
    padding:10px;
   }
#container fieldset {
    margin: 2px auto 0px;
    width: 100%;
    min-height:26px;
    background-color: #f2f2f2;
    border:none;
}
#container button {
    width: 5ex;
    text-align: center;
    padding: 1px 3px;
    height:30px;
    width:30px;
    background-repeat: no-repeat;
    background-size: cover;
    border:none;
}
#container img{
      width:100%;
}
#container .bold{
 font-weight: bold;
}
#container .italic{
    font-style: italic;
}
#container .underline{
   text-decoration: underline;
}
#container .color-apply{
    width: 50px;
}
#container #input-font{
    width: 100px;
    height: 25px;
}
[contentEditable=true]:empty:not(:focus):before{
    content:attr(data-text);
    color:#888;
}

[contenteditable]:focus {
    outline: 0px solid transparent;
}

#container .loader {
    border: 6px solid #f3f3f3; 
    border-top: 6px solid #3498db; 
    border-radius: 50%;
    width: 20px;
    height: 20px;
    animation: spin 2s linear infinite;
    margin: 0 auto;
    line-height: 20px;
}

@media (max-width: 800px) {
 .container<?= @$blockCms['tplParent'] ?>{
  padding-left: 20px;
  padding-right: 20px;
  padding-top: 2px;
  padding-bottom: 2px;
  margin-left: 0px;    
  margin-right: 0px;      
  /*position: sticky; */
 }

     .whole-container<?= @$blockCms['tplParent'] ?> {
      padding-left: 0px;
      padding-right: 0px;
      margin-left: 0px;    
      margin-right: 0px;   
    }
}

a {
 color: inherit;
 font-family: inherit !important;
} 

button {
 font-size: 18px !important;
 font-family: FontAwesome; !important;
 color: inherit;
} 

a:hover, a:focus, a:active, a.active { color: inherit; } 

.edit-mode-<?= $myCmsId ?> { 
  box-shadow: #038cce24 0px 0px 0px 1px;
    border-radius: 3px;
    height: min-content;
    width: 100%;
    cursor: pointer;

}

.focus-mode-<?= $myCmsId ?> {
  box-shadow: #03a9f4 0px 0px 0px 2px;
  border-radius: 3px;
  height: min-content;
  width: 100%;
  cursor: pointer;
}

</style>
  <div class="<?= $myCmsId ?>-<?= $keyTpl ?> stop-propagation edit-mode-<?= $myCmsId ?>" style="width: 100%;">
    <div id="<?= $myCmsId ?>textShow" style="white-space: pre-line;font-family: 'ml';cursor: auto;" data-text="Ecrire une texte...." class="editable sp-text-<?= $myCmsId ?>"><?php echo $paramsData["text"];?></div>
  </div>



<script type="text/javascript">

  $('#<?= $myCmsId ?>textShow').on('focusin', function() {    
    if (mode == "w") {
    $(".<?= $myCmsId ?>-<?= $keyTpl ?>").addClass("focus-mode-<?= $myCmsId ?>");
    $(".<?= $myCmsId ?>-<?= $keyTpl ?>").removeClass("edit-mode-<?= $myCmsId ?>");
    }
  });

  $('#<?= $myCmsId ?>textShow').on('focusout', function() {
    $(".<?= $myCmsId ?>-<?= $keyTpl ?>").removeClass("focus-mode-<?= $myCmsId ?>");
    $(".<?= $myCmsId ?>-<?= $keyTpl ?>").addClass("edit-mode-<?= $myCmsId ?>");
  });

  if ("supercms" !== "<?= $subtype ?>" ) {
    tplCtx = {};
    tplCtx.id = "<?php echo $myCmsId ?>";
    tplCtx.collection = "cms";
    tplCtx.path = "allToRoot"; 
    tplCtx.value = {};
    tplCtx.value.subtype = "supercms";
    dataHelper.path2Value( tplCtx, function(params) { } );
  }

  document.getElementById("<?= $myCmsId ?>textShow").contentEditable = "false";
// Copy text as text without styles
var ce = document.getElementsByClassName('sp-text-<?= $myCmsId ?>');
for (var i = 0; i < ce.length; i++) {
    // classname[i].addEventListener('click', myFunction, false);
    ce[i].addEventListener('paste', function (e) {
      e.preventDefault()
      var text = e.clipboardData.getData('text/plain')
      document.execCommand('insertText', false, text)
    })
}




   /*function replace_content<?= $myCmsId ?>(content)
   {
     var exp_match = /(\b(https?|):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
     var element_content=content.replace(exp_match, "<a href='$1'>$1</a>");
     var new_exp_match =/(^|[^\/])(www\.[\S]+(\b|$))/gim;
     var new_content=element_content.replace(new_exp_match, `<a target="_blank" href="http://$2">$2</a>`);
     return new_content;
   }  
   $('#<?= $myCmsId ?>textShow').html(replace_content<?= $myCmsId ?>($('#<?= $myCmsId ?>textShow').html()));*/





// $("#<?= $myCmsId ?>textShow *").attr('contenteditable','true');

/*****************Admin panel*****************/
  <?php if(Authorisation::isInterfaceAdmin()){ ?> 
  cmsBuilder.init();
  /************Preview and read mode************/
  $(document).keyup(function(e) {
    if (e.altKey == true && e.keyCode == 80) {
      // alert(previewMode)
      if (previewMode == false) {
       mode = "v"
       convAndCheckLink("#<?= $myCmsId ?>textShow");
       let elems = document.getElementById("<?= $myCmsId ?>textShow");
       if (elems !== null) {
        document.getElementById("<?= $myCmsId ?>textShow").contentEditable = "false";
      }
      $(".<?= $myCmsId ?>-<?= $keyTpl ?>").removeClass("edit-mode-<?= $myCmsId ?>");
    }else{
      let allText = $("#<?= $myCmsId ?>textShow").html();
      let p = document.createElement('p');
      p.innerHTML = allText;
      let links = p.querySelectorAll('a');
      links.forEach(x => {
        allText = allText.replace(x.outerHTML, "["+x.innerText+"]("+x.href+")");
      });
      $("#<?= $myCmsId ?>textShow").html(allText);
      if (document.getElementById("<?= $myCmsId ?>textShow") !== null) {
       document.getElementById("<?= $myCmsId ?>textShow").contentEditable = "true";
     }
     $(".<?= $myCmsId ?>-<?= $keyTpl ?>").addClass("edit-mode-<?= $myCmsId ?>");
     var spcms<?= $myCmsId ?> = document.querySelector('spcms');
     if (spcms<?= $myCmsId ?> !== null) {
      var text<?= $myCmsId ?> = spcms<?= $myCmsId ?>.textContent;
      spcms<?= $myCmsId ?>.parentNode.replaceChild(document.createTextNode(text<?= $myCmsId ?>), spcms<?= $myCmsId ?>);
    }
  }
  
}
});

if(localStorage.getItem("previewMode") !== "w"){ 
 setTimeout(function() {
   mode = "v"
   convAndCheckLink("#<?= $myCmsId ?>textShow");
   let elems = document.getElementById("<?= $myCmsId ?>textShow");
   if (elems !== null) {
    document.getElementById("<?= $myCmsId ?>textShow").contentEditable = "false";
  }
  $(".<?= $myCmsId ?>-<?= $keyTpl ?>").removeClass("edit-mode-<?= $myCmsId ?>");
}, 100);
}

$(".view-super-cms").click(function(){
      mode = "v"
    convAndCheckLink("#<?= $myCmsId ?>textShow");
    let elems = document.getElementById("<?= $myCmsId ?>textShow");
    if (elems !== null) {
      document.getElementById("<?= $myCmsId ?>textShow").contentEditable = "false";
    }
  $(".<?= $myCmsId ?>-<?= $keyTpl ?>").removeClass("edit-mode-<?= $myCmsId ?>");
});

$(".hiddenEdit").click(function(){
 if (previewMode == false) {
       mode = "v"
       convAndCheckLink("#<?= $myCmsId ?>textShow");
       let elems = document.getElementById("<?= $myCmsId ?>textShow");
       if (elems !== null) {
        document.getElementById("<?= $myCmsId ?>textShow").contentEditable = "false";
      }
      $(".<?= $myCmsId ?>-<?= $keyTpl ?>").removeClass("edit-mode-<?= $myCmsId ?>");
    }else{
      let allText = $("#<?= $myCmsId ?>textShow").html();
      let p = document.createElement('p');
      p.innerHTML = allText;
      let links = p.querySelectorAll('a');
      links.forEach(x => {
        allText = allText.replace(x.outerHTML, "["+x.innerText+"]("+x.href+")");
      });
      $("#<?= $myCmsId ?>textShow").html(allText);
      if (document.getElementById("<?= $myCmsId ?>textShow") !== null) {
       document.getElementById("<?= $myCmsId ?>textShow").contentEditable = "true";
     }
     $(".<?= $myCmsId ?>-<?= $keyTpl ?>").addClass("edit-mode-<?= $myCmsId ?>");
     var spcms<?= $myCmsId ?> = document.querySelector('spcms');
     if (spcms<?= $myCmsId ?> !== null) {
      var text<?= $myCmsId ?> = spcms<?= $myCmsId ?>.textContent;
      spcms<?= $myCmsId ?>.parentNode.replaceChild(document.createTextNode(text<?= $myCmsId ?>), spcms<?= $myCmsId ?>);
    }
  }
})

$(document).keyup(function(e) {
  if (e.key === "Escape") { 
    let allText = $("#<?= $myCmsId ?>textShow").html();
    let p = document.createElement('p');
    p.innerHTML = allText;
    let links = p.querySelectorAll('a');
    links.forEach(x => {
      allText = allText.replace(x.outerHTML, "["+x.innerText+"]("+x.href+")");
    });
    $("#<?= $myCmsId ?>textShow").html(allText);
    if (document.getElementById("<?= $myCmsId ?>textShow") !== null) {
     document.getElementById("<?= $myCmsId ?>textShow").contentEditable = "true";
   }
  $(".<?= $myCmsId ?>-<?= $keyTpl ?>").addClass("edit-mode-<?= $myCmsId ?>");
  var spcms<?= $myCmsId ?> = document.querySelector('spcms');
  if (spcms<?= $myCmsId ?> !== null) {
    var text<?= $myCmsId ?> = spcms<?= $myCmsId ?>.textContent;
    spcms<?= $myCmsId ?>.parentNode.replaceChild(document.createTextNode(text<?= $myCmsId ?>), spcms<?= $myCmsId ?>);
  }
 }
});


/************End preview and read mode************/
  document.getElementById("<?= $myCmsId ?>textShow").contentEditable = "true";

    let allText = $("#<?= $myCmsId ?>textShow").html();
    let p = document.createElement('p');
    p.innerHTML = allText;
    let links = p.querySelectorAll('a');
    links.forEach(x => {
      allText = allText.replace(x.outerHTML, "["+x.innerText+"]("+x.href+")");
    });
    $("#<?= $myCmsId ?>textShow").html(allText);

     
   function insertTextAtCaret(text) {
    var sel, range;
    if (window.getSelection) {
      sel = window.getSelection();
      selectedText = sel.toString();
      if (selectedText !== "") {
        label = "["+selectedText+"](http://www.) ";
      }else{
        label = text;
      }
      if (sel.getRangeAt && sel.rangeCount) {
        range = sel.getRangeAt(0);
        range.deleteContents();
        range.insertNode( document.createTextNode(label) );
      }
    } else if (document.selection && document.selection.createRange) {
      document.selection.createRange().label = label;
    }
  }

// function makeEditableAndHighlight<?= $myCmsId ?>(colour) {
//     var range, sel = window.getSelection();
//     if (sel.rangeCount && sel.getRangeAt) {
//         range = sel.getRangeAt(0);
//     }
//     document.designMode = "on";
//     if (range) {
//         sel.removeAllRanges();
//         sel.addRange(range);
//     }
//     // Use HiliteColor since some browsers apply BackColor to the whole block
//     if (!document.execCommand("HiliteColor", false, colour)) {
//         document.execCommand("Color", false, colour);
//     }
//     document.designMode = "off";
// }

// function highlight<?= $myCmsId ?>(colour) {
//     var range, sel;
//     if (window.getSelection) {
//         // IE9 and non-IE
//         try {
//             if (!document.execCommand("Color", false, colour)) {
//                 makeEditableAndHighlight<?= $myCmsId ?>(colour);
//             }
//         } catch (ex) {
//             makeEditableAndHighlight<?= $myCmsId ?>(colour)
//         }
//     } else if (document.selection && document.selection.createRange) {
//         // IE <= 8 case
//         range = document.selection.createRange();
//         range.execCommand("Color", false, colour);
//     }
// }

  $(".<?= $myCmsId ?>font-size").change(function() {        
    var item=$(this);
    var val = (item.val())*1;
    $("#<?= $myCmsId ?>textShow").css("fontSize", val);
  });

 // $(".content-<?= $kunik?>").draggable().resizable();
    //$(".<?= $myCmsId ?>toolbar").hide();

  $(function () {
    var count = 1;

  // Create click event handler on the document.
/*  $(document).on("click", function (event) {
    // If the target is not the container or a child of the container, then process
    // the click event for outside of the container.
    if ($(event.target).closest(".<?= $myCmsId ?>-<?= $keyTpl ?>").length === 0) {
      $(".<?= $myCmsId ?>toolbar").hide();
    }
  });*/
/*  $(document).off().on('click', '.sp-container', function () {
    alert("yes");
  });*/
  // Create click event handler on the container element.

  /**************Open text settings panel**************/
  $(".<?= $myCmsId ?>-<?= $keyTpl ?>").on("mouseup", function () { 
    editable = $("#<?= $myCmsId ?>textShow")[0];
    divSelection = saveSelection(editable);
  });
  $(".<?= $myCmsId ?>-<?= $keyTpl ?>").on("mousedown", function () {
     if (mode != "v") {    
    setTimeout(function(){    
      $(".sp-preview-inner").css("background-color","#000")
    },90);
    sel = window.getSelection();
    $(".sp-container").hide();  
        $(this).children(":first").focus();
        $("#toolsBar").show();
        $(".sp-settings-btn").hide();
          $("#toolsBar").html(`
                  <div class="container-fluid">
                  <div class="text-center">
                    <i style="cursor:default;font-size: large;" aria-hidden="true">Paramètres de texte</i>
                    <button class="bg-transparent deleteLine delete-super-cms tooltips" data-path="<?= $blockCms['path'] ?>" data-id="<?= $blockCms['_id']; ?>" data-collection="cms" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Supprimer tout le texte">
                      <i class="fa fa-trash text-red" aria-hidden="true"></i>
                    </button>
                    <i class="fa fa-window-close closeBtn pull-right" aria-hidden="true"></i>
                  </div>
                  <div id="container" >
                    <fieldset>
                      <button class="fontStyle italic" onclick="document.execCommand('italic',false,null);" title="Italicize Highlighted Text"><i class="fa fa-italic"></i></button>
                      <button class="fontStyle bold" onclick="document.execCommand( 'bold',false,null);" title="Bold Highlighted Text"><i class="fa fa-bold"></i></button>
                      <button class="fontStyle underline" onclick="document.execCommand( 'underline',false,null);"><i class="fa fa-underline"></i></button>
                      <i class="fa fa-text-height"></i>
                      <select id="input-font" class="input">
                        
                      </select>
                      <button class="fontStyle strikethrough" onclick="document.execCommand( 'strikethrough',false,null);"><i class="fa fa-strikethrough"></i></button>
                      <button class="fontStyle align-left" onclick="document.execCommand( 'justifyLeft',false,null);"><i class="fa fa-align-left"></i></button>
                      <button class="fontStyle align-justify" onclick="document.execCommand( 'justifyFull',false,null);"><i class="fa fa-align-justify"></i></button>
                      <button class="fontStyle align-center" onclick="document.execCommand( 'justifyCenter',false,null);"><i class="fa fa-align-center"></i></button>
                      <button class="fontStyle align-right" onclick="document.execCommand( 'justifyRight',false,null);"><i class="fa fa-align-right"></i></button>
                     <!-- 
                      <button class="fontStyle orderedlist disabled" onclick="document.execCommand('insertOrderedList', false, null);"><i class="fa fa-list-ol"></i></button>
                      <button class="fontStyle unorderedlist disabled" onclick="document.execCommand('insertUnorderedList',false, null)"><i class="fa fa-list-ul"></i></button> -->  
                      <input class="color-apply sp-color-picker spectrum sp-colorize" type="color" id="myColor" value="#0b0b0b"> 
                      <button onclick="insertTextAtCaret('[label](http://www)')" class="fontStyle" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Ajouter une lien">
                      <i class="fa fa-link" aria-hidden="true"></i><br>
                      </button> 

                      <!-- font size start -->
                      <select id="fontSize">
                        <option value="1">1</option>      
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="12">12</option>
                        <option value="14">14</option>
                        <option value="18">18</option>
                      </select>
                      <!-- font size end -->
                      
                    </fieldset>
                    
                  </div>
                
              </div>

          `);

 /*<button class="fontStyle undo-apply" onclick="document.execCommand( 'undo',false,null);"><i class="fa fa-undo"></i></button>
 <button class="fontStyle redo-apply" onclick="document.execCommand( 'redo',false,null);"><i class="fa fa-repeat"></i></button>*/

  $('#input-font').append(fontOptions);
      cmsBuilder.init();
      $(".closeBtn").click(function(){
        text = $("#<?= $myCmsId ?>textShow").html();
        tplCtx = {};
        tplCtx.id = "<?php echo $myCmsId ?>";
        tplCtx.collection = "cms";
        tplCtx.path = "text"; 
        tplCtx.value = text;

        dataHelper.path2Value( tplCtx, function(params) {          
          toastr.success("Modification enrégistré");
        } );
        $("#toolsBar").hide();
        $("#toolsBar").html("");
        // set element parent to edit mode
        $(".container<?= @$blockCms['tplParent'] ?>").addClass("edit-container<?= @$blockCms['tplParent'] ?>");
        $(".container<?= @$blockCms['tplParent'] ?>").removeClass("selected-mode-container<?= @$blockCms['tplParent'] ?>");
      });

      $( "#myColor" ).change(function() {
        var mycolor = "";
        restoreSelection(editable, divSelection); 
        mycolor = $(".sp-preview-inner").css("background-color");
        setTimeout(function(){    
          document.execCommand('foreColor', false, mycolor);
        },90);
      });

    $( "#input-font" ).change(function() {
      var myFont = document.getElementById("input-font").value;
      document.execCommand('fontName', false, myFont);
   });

   $( "#fontSize" ).change(function() {
      var mysize = document.getElementById("fontSize").value;
      document.execCommand('fontSize', false, mysize);
   });

    function checkDiv(){
      var editorText = document.getElementById("<?= $myCmsId ?>textShow").innerHTML;
      if(editorText === ''){
        document.getElementById("<?= $myCmsId ?>textShow").style.border = '5px solid red';
      }
    }

    function removeBorder(){
      document.getElementById("<?= $myCmsId ?>textShow").style.border = '1px solid transparent';
    }


     //color
     function loadColorPicker(callback) {
      mylog.log("loadColorPicker");
      if( ! jQuery.isFunction(jQuery.ColorPicker) ) {
        mylog.log("loadDateTimePicker2");
        lazyLoad( baseUrl+'/plugins/colorpicker/js/colorpicker.js', 
          baseUrl+'/plugins/colorpicker/css/colorpicker.css',
          callback);
      }
    }
    function loadSpectrumColorPicker(callback) {
      mylog.log("loadSpectrumColorPicker");
      lazyLoad( baseUrl+'/plugins/spectrum-colorpicker2/spectrum.min.js', 
        baseUrl+'/plugins/spectrum-colorpicker2/spectrum.min.css',
        callback);

    }
    var initColor = function(){
      mylog.log("init .colorpickerInput");
      $(".colorpickerInput").ColorPicker({ 
        color : "pink",
        onSubmit: function(hsb, hex, rgb, el) {
          $(el).val(hex);
          $(el).ColorPickerHide();
        },
        onBeforeShow: function () {
          $(this).ColorPickerSetColor(this.value);
        }
      }).bind('keyup', function(){
        $(this).ColorPickerSetColor(this.value);
      });
    };

    if(  $(".colorpickerInput").length){
      loadColorPicker(initColor);
    }

    loadSpectrumColorPicker(function(){
      if(  $(".sp-color-picker").length){
        $(".sp-color-picker").spectrum({
          type: "text",
          showInput: "true",
          showInitial: "true"
        });
        $('.sp-color-picker').addClass('form-control');
        $('.sp-original-input-container').css("width","100%");
      }
    });
  }
/**************End open text settings panel**************/
      
  });
});

  $('.<?= $myCmsId ?>-<?= $keyTpl ?>').click(function(event){
    event.stopPropagation();
  });
  // $( ".<?= $myCmsId ?>-<?= $keyTpl ?>" ).blur(function() {
  //   $(".<?= $myCmsId ?>toolbar").hide();
  // });

  $( "#<?= $myCmsId ?>textShow" ).blur(function() {
    text = $("#<?= $myCmsId ?>textShow").html();
    tplCtx = {};
    tplCtx.id = "<?php echo $myCmsId ?>";
    tplCtx.collection = "cms";
    tplCtx.path = "text"; 
    tplCtx.value = text;

    dataHelper.path2Value( tplCtx, function(params) {} );

  });


    $("#<?= $myCmsId ?>textShow").click(function(){     

    });

   $(".<?= $myCmsId ?>bold").on('click', function(event){
    $("#<?= $myCmsId ?>textShow").toggleClass("bold");    
    // $("#<?= $myCmsId ?>textEdit").toggleClass("bold");
  });
   $(".<?= $myCmsId ?>italic").on('click', function(event){ 
    $("#<?= $myCmsId ?>textShow").toggleClass("italic");
    // $("#<?= $myCmsId ?>textEdit").toggleClass("italic");
  });
   $(".<?= $myCmsId ?>right").on('click', function(event){
    $("#<?= $myCmsId ?>textShow").toggleClass("right");
    $("#<?= $myCmsId ?>textShow").removeClass("center");
    $("#<?= $myCmsId ?>textShow").removeClass("left");
    $("#<?= $myCmsId ?>textShow").removeClass("justify");

    // $("#<?= $myCmsId ?>textEdit").toggleClass("right");
    // $("#<?= $myCmsId ?>textEdit").removeClass("center");
    // $("#<?= $myCmsId ?>textEdit").removeClass("left");
    // $("#<?= $myCmsId ?>textEdit").removeClass("justify");
  });
   $(".<?= $myCmsId ?>center").on('click', function(event){
    $("#<?= $myCmsId ?>textShow").toggleClass("center");
    $("#<?= $myCmsId ?>textShow").removeClass("right");
    $("#<?= $myCmsId ?>textShow").removeClass("left");
    $("#<?= $myCmsId ?>textShow").removeClass("justify");

    // $("#<?= $myCmsId ?>textEdit").toggleClass("center");
    // $("#<?= $myCmsId ?>textEdit").removeClass("right");
    // $("#<?= $myCmsId ?>textEdit").removeClass("left");
    // $("#<?= $myCmsId ?>textEdit").removeClass("justify");
  });
   $(".<?= $myCmsId ?>left").on('click', function(event){
    $("#<?= $myCmsId ?>textShow").toggleClass("left");
    $("#<?= $myCmsId ?>textShow").removeClass("right");
    $("#<?= $myCmsId ?>textShow").removeClass("center");
    $("#<?= $myCmsId ?>textShow").removeClass("justify");

    // $("#<?= $myCmsId ?>textEdit").toggleClass("left");
    // $("#<?= $myCmsId ?>textEdit").removeClass("right");
    // $("#<?= $myCmsId ?>textEdit").removeClass("center");
    // $("#<?= $myCmsId ?>textEdit").removeClass("justify");
  });
   $(".<?= $myCmsId ?>justify").on('click', function(event){
    $("#<?= $myCmsId ?>textShow").toggleClass("justify");
    $("#<?= $myCmsId ?>textShow").removeClass("right");
    $("#<?= $myCmsId ?>textShow").removeClass("center");
    $("#<?= $myCmsId ?>textShow").removeClass("left");

    // $("#<?= $myCmsId ?>textEdit").toggleClass("justify");
    // $("#<?= $myCmsId ?>textEdit").removeClass("right");
    // $("#<?= $myCmsId ?>textEdit").removeClass("center");
    // $("#<?= $myCmsId ?>textEdit").removeClass("left");
  });
 <?php }else{ ?>

   setTimeout(function() {
    convAndCheckLink("#<?= $myCmsId ?>textShow");
  }, 100);
   $(".<?= $myCmsId ?>-<?= $keyTpl ?>").removeClass("edit-mode-<?= $myCmsId ?>");
<?php } ?>

/***************END Admin panel*****************/
/*End document ready*/
</script>
