<?php 
/* 
Super container:
Create by Ifaliana Arimanana
ifaomega@gmail.com
26 Apr 2021
*/


/***************** Required *****************/
$keyTpl ="container";
$myCmsId = $blockCms["_id"]->{'$id'};
$params = array();
/*************** End required ***************/

/*****************get image uploaded***************/
$initImage = Document::getListDocumentsWhere(
  array(
    "id"=> $myCmsId,
    "type"=>'cms',
    "subKey"=>'block',
  ), "image"
);

$latestImg = isset($initImage["0"]["imageMediumPath"])?$initImage["0"]["imageMediumPath"]:"empty" ;
if($latestImg == "empty"){
  $latestImg = isset($initImage["0"]["imagePath"])?$initImage["0"]["imagePath"]:"empty" ;
}
/*****************end get image uploaded***************/

/* Get settings */

$array_position   = $blockCms['position'] ?? $blockCms['position'] ?? [];
$widthClass       = $blockCms["class"]["width"] ?? $blockCms["class"]["width"] ?? "col-md-4";
$borderColor      = $blockCms["css"]["border"]["color"] ?? $blockCms["css"]["border"]["color"] ?? "transparent";
$borderRadius     = $blockCms["css"]["border"]["radius"] ?? $blockCms["css"]["border"]["radius"] ?? "";
$paddingGeneral   = $blockCms["css"]["padding"]["general"] ?? $blockCms["css"]["padding"]["general"] ?? "20";
$backgroundColor  = $blockCms["css"]["background"]["color"] ?? $blockCms["css"]["background"]["color"] ?? "transparent";

/* End get settings */

?>
<style type="text/css">
  .content-<?= $myCmsId ?> {  
    border: dashed 2px gray;
    background-color: <?= $backgroundColor ?>;
    <?php if ( $borderRadius !="") { ?>
      border-radius: <?= $borderRadius ?>px;
    <?php } ?>
    <?php if ( $paddingGeneral !="") { ?>
      padding: <?= $paddingGeneral ?>px;
    <?php } ?>
  }

  .view-mode-<?= $myCmsId ?> {
    border: solid <?= $borderColor ?> !important;
    background-color: <?= $backgroundColor ?> !important;
    <?php if ( $borderRadius !="") { ?>
      border-radius: <?= $borderRadius ?>px !important;
    <?php } ?>
    <?php if ( $paddingGeneral !="") { ?>
      padding: <?= $paddingGeneral ?>px !important;
    <?php } ?>
  }

  .selected-mode-<?= $myCmsId ?> {
    border: solid 1px lightblue;
    box-shadow: 0 6px 12px lightblue;
    background-color: <?= $backgroundColor ?>;
    <?php if ( $borderRadius !="") { ?>
      border-radius: <?= $borderRadius ?>px;
    <?php } ?>
    <?php if ( $paddingGeneral !="") { ?>
      padding: <?= $paddingGeneral ?>px;
    <?php } ?>
  }

  #toolsBar {
    left: 40%;
    top: 40%;
  }


</style>
  <div class="whole-content-<?= $myCmsId ?> <?= $widthClass ?>">
    <div class="container-fluid content-<?= $myCmsId ?> this-content-<?= $myCmsId ?> super-cms" style="min-height: 50px;position: relative; <?php foreach ($array_position as $key => $value) {
      echo $key." : ".$value." !important; "; } ?>">
      <div class="stop-propagation">
        <div class="row">
          <?php
          $idElem = $childForm = PHDB::find("cms", array("type" => "blockChild" , "tplParent" => $myCmsId ));
          if (!empty($idElem)) {
            foreach ($idElem as $key => $value) {
              $pathExplode = explode('.', $value["path"]);
              $count = count($pathExplode);
              $superKunik = $pathExplode[$count-1].$value["_id"];
              $blockKey = (string)$value["_id"];
              $cmsElement = $value["path"];       
              $params = [
                "blockCms"  =>  $value,
                "kunik"     =>  $superKunik
              ];
            echo $this->renderPartial("costum.views.".$cmsElement,$params); 
            if(Authorisation::isInterfaceAdmin()){?>
              <div class="hiddenPreview" style="position: absolute;right: 10px">
                <?php 
                echo $this->renderPartial("costum.views.tpls.editSuperCms", [
                  "canEdit" => true, 
                  "kunik"   => $superKunik,
                  "subtype" => $value["subtype"],
                  "id"      => $blockKey
                ]);  
                ?>
              </div>
            <?php } 
          }

        }else{ ?>  
          <div class="hiddenPreview" style="
          height: 25vh;
          top: 50%;
          margin: 2px 0 0;
          font-size: 35px;
          color: slategrey;
          text-align: center;
          ">
          <h3 style="font-weight: normal;">Aucun element</h3>
          </div><?php } ?>  
        </div>
      </div>
    </div>
  </div>
<script type="text/javascript">
/*-----View and edit mode------- */
    $(".view-super-cms").click(function(){
      $(".this-content-<?= $myCmsId ?>").addClass("view-mode-<?= $myCmsId ?>");
      $(".this-content-<?= $myCmsId ?>").removeClass("content-<?= $myCmsId ?>");
      $(".this-content-<?= $myCmsId ?>").removeClass("selected-mode-<?= $myCmsId ?>");
      $("#toolsBar").hide();   
    });
    $(document).keyup(function(e) {
      if (e.key === "Escape") { 
      $(".this-content-<?= $myCmsId ?>").removeClass("view-mode-<?= $myCmsId ?>");
      $(".this-content-<?= $myCmsId ?>").addClass("content-<?= $myCmsId ?>");
      $(".this-content-<?= $myCmsId ?>").removeClass("selected-mode-<?= $myCmsId ?>");
      $("#toolsBar").fadeOut();
      }
    });
/*-----End view and edit mode------- */

$(document).ready(function(){
    $(".this-content-<?= $myCmsId ?>").click(function(){

          superCms = {
            viewMode: function(){
              $(".this-content-<?= $myCmsId ?>").addClass("view-mode-<?= $myCmsId ?>");
              $(".this-content-<?= $myCmsId ?>").removeClass("content-<?= $myCmsId ?>");
              $(".this-content-<?= $myCmsId ?>").removeClass("selected-mode-<?= $myCmsId ?>");
            },
            editMode: function(){
              $(".this-content-<?= $myCmsId ?>").removeClass("view-mode-<?= $myCmsId ?>");
              $(".this-content-<?= $myCmsId ?>").addClass("content-<?= $myCmsId ?>");
              $(".this-content-<?= $myCmsId ?>").removeClass("selected-mode-<?= $myCmsId ?>");
            },
            selectetMode: function(){
              $(".this-content-<?= $myCmsId ?>").removeClass("view-mode-<?= $myCmsId ?>");
              $(".this-content-<?= $myCmsId ?>").removeClass("content-<?= $myCmsId ?>");
              $(".this-content-<?= $myCmsId ?>").addClass("selected-mode-<?= $myCmsId ?>");
            }
          }

          if ($.trim($("#toolsBar").html()) != "") {   
            $("#toolsBar").hide();   
            $("#toolsBar").html("");
            superCms.editMode();
          }else{      
              $("#toolsBar").show();    
              superCms.selectetMode();
                 $("#toolsBar").html(` 
            <div class="container-fluid">
              <div class="text-center">
                <i style="text-transform: capitalize; cursor:default;font-size: large;" aria-hidden="true"><?= $keyTpl ?>' settings</i>
                <i class="fa fa-window-close closeBtn" aria-hidden="true"></i>
              </div><hr>
              <ul class="nav navbar-nav" style="padding: 10px;">
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Insérer<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li class="add-text" data-path="tpls.blockCms.superCms.text"><a href="#">Texte</a></li>
                    <li class="add-text" data-path="tpls.blockCms.superCms.image"><a href="#">image</a></li>
                    <li class="add-text" data-path="tpls.blockCms.superCms.button"><a href="#">Bouton</a></li>
                    <li class="add-text" data-path="tpls.blockCms.superCms.container"><a href="#">Panel</a></li>
                    <li class="add-type-cms" onclick="cmsBuilder.openModal();"><a href="#">Bloc cms</a></li>
                  </ul>
                </li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Fond<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li>Fond<input id="fond-color" class="sp-color-picker spectrum sp-colorize form-control" name="" value="<?= $backgroundColor ?>" placeholder="#ffffff" style="">
                    </li>
                    <li>Bordure<input id="border-color" class="sp-color-picker spectrum sp-colorize form-control" name="" value="<?= $borderColor ?>" placeholder="#ffffff" style="">
                    </li>
                  </ul>
                </li>
                <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Taille<span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li class="content-size" data-size="col-md-2"><a href="#">Plus petit</a></li>
                    <li class="content-size" data-size="col-md-4"><a href="#">Petit</a></li>
                    <li class="content-size" data-size="col-md-6"><a href="#">Moyen</a></li>
                    <li class="content-size" data-size="col-md-8"><a href="#">Grand</a></li>
                    <li class="content-size" data-size="col-md-12"><a href="#">Très grand</a></li>
                    <li class="content-size" data-size="container"><a href="#">Container</a></li>
                  </ul>
                </li>
              </ul><br>
              <div class="row">
                  <div class="col-md-12">      
                    <div class="panel-group">
                      <div >          
                        <div class="">
                          <div class="">  
                            Padding<input class="form-control padding-control" type="number" value="<?= $paddingGeneral ?>" placeholder="0">
                            Border radius<input class="form-control border-control" type="number" value="<?= $borderRadius ?>" placeholder="0">
                          </div>  
                        </div>  
                      </div>
                    </div>
                  </div>
                  <div class="">      
                    <div class="panel-group">
                      <div >          
                        <div class="">
                          <div class="">  
                          </div>  
                        </div>  
                      </div>
                    </div>
                  </div>
                </div>  
                <li class="dropdown"><a  data-toggle="collapse" href="#testest">Autre css </a>
                  <div class="">        
                    <div class="panel-group">
                      <div >          
                        <div id="testest" class="panel-collapse collapse">
                          <div class="">  
                            <textarea></textarea>
                          </div>  
                        </div>  
                      </div>
                    </div>
                  </div>
                </li>   
            </div>`);
              }
  
   // $(".this-content-<?= $myCmsId ?>" ).css("border-color","<?= $borderColor ?>");

   $(".closeBtn").click(function(){
    $("#toolsBar").html(``);
    $("#toolsBar").hide();
    superCms.editMode();
    $(".this-content-<?= $myCmsId ?>").css("cursor", "default");
  });
   /*********End menu*********/ 

   $(".this-content-<?= $myCmsId ?>").css("cursor", "move");
   /*Add new element*/ 
   $(".add-type-cms").click(function(){
    localStorage.setItem("parentCmsIdForChild", "<?php echo $myCmsId ?>");
  });

   $(".add-text").click(function(){
    $("#toolsBar").html(``);
    var tplCtx = {};
    tplCtx.collection= "cms";
    tplCtx.value = {};
    tplCtx.value.path = $(this).data("path");
    tplCtx.value.tplParent = "<?php echo $myCmsId ?>";
    tplCtx.value.type = "blockChild";
    tplCtx.value.subtype = "superBlock";
    tplCtx.value.parent={};
    tplCtx.value.parent[thisContextId] = {
      type : thisContextType, 
      name : "<?php echo $tpl ?>"
    };
    if(typeof tplCtx.value == "undefined")
      toastr.error('value cannot be empty!');
    else {
      dataHelper.path2Value( tplCtx, function(params) {
        dyFObj.commonAfterSave(params,function(){
          toastr.success("Élément bien ajouté");
          $("#ajax-modal").modal('hide');                  
        });
      } );      
      urlCtrl.loadByHash(location.hash);
    }
  });

   /*This element settings*/

   //color
   function loadColorPicker(callback) {
    mylog.log("loadColorPicker");
    if( ! jQuery.isFunction(jQuery.ColorPicker) ) {
      mylog.log("loadDateTimePicker2");
      lazyLoad( baseUrl+'/plugins/colorpicker/js/colorpicker.js', 
        baseUrl+'/plugins/colorpicker/css/colorpicker.css',
        callback);
    }
  }
  function loadSpectrumColorPicker(callback) {
    mylog.log("loadSpectrumColorPicker");
    lazyLoad( baseUrl+'/plugins/spectrum-colorpicker2/spectrum.min.js', 
      baseUrl+'/plugins/spectrum-colorpicker2/spectrum.min.css',
      callback);

  }
  var initColor = function(){
    mylog.log("init .colorpickerInput");
    $(".colorpickerInput").ColorPicker({ 
      color : "pink",
      onSubmit: function(hsb, hex, rgb, el) {
        $(el).val(hex);
        $(el).ColorPickerHide();
      },
      onBeforeShow: function () {
        $(this).ColorPickerSetColor(this.value);
      }
    }).bind('keyup', function(){
      $(this).ColorPickerSetColor(this.value);
    });
  };

  if(  $(".colorpickerInput").length){
    loadColorPicker(initColor);
  }

  loadSpectrumColorPicker(function(){
    if(  $(".sp-color-picker").length){
      $(".sp-color-picker").spectrum({
        type: "text",
        showInput: "true",
        showInitial: "true"
      });
      $('.sp-color-picker').addClass('form-control');
      $('.sp-original-input-container').css("width","100%");
    }
  });


  $( "#fond-color" ).change(function() {
   $(".view-mode-<?= $myCmsId ?>" ).css("background-color",$("#fond-color").val());

   tplCtx = {};
   tplCtx.id = "<?php echo $myCmsId ?>";
   tplCtx.collection = "cms";
   tplCtx.path = "css.background.color"; 
   tplCtx.value = $("#fond-color").val();

   dataHelper.path2Value( tplCtx, function(params) {} );
 });

  $( "#border-color" ).change(function() {
   $(".view-mode-<?= $myCmsId ?>" ).css("border-color",$("#border-color").val());

   tplCtx = {};
   tplCtx.id = "<?php echo $myCmsId ?>";
   tplCtx.collection = "cms";
   tplCtx.path = "css.border.color"; 
   tplCtx.value = $("#border-color").val();

   dataHelper.path2Value( tplCtx, function(params) {} );
 });

  $("#toolsBar").click(function(){
   superCms.viewMode();
 });


  $( ".border-control" ).change(function() {
    $(".view-mode-<?= $myCmsId ?>" ).css("border-radius","");
    $(".view-mode-<?= $myCmsId ?>" ).css("border-radius",$(".border-control").val()+"px");
  });
  $( ".border-control" ).keyup(function() {
    $(".view-mode-<?= $myCmsId ?>" ).css("border-radius","");
    $(".view-mode-<?= $myCmsId ?>" ).css("border-radius",$(".border-control").val()+"px");
  });
  $( ".border-control" ).blur(function() {
    tplCtx = {};
    tplCtx.id = "<?php echo $myCmsId ?>";
    tplCtx.collection = "cms";
    tplCtx.path = "css.border.radius"; 
    tplCtx.value = $(".border-control").val();

    dataHelper.path2Value( tplCtx, function(params) {} );
  });

  $( ".padding-control" ).change(function() {
    $(".view-mode-<?= $myCmsId ?>" ).css("padding",$(".padding-control").val()+"px");
  });
  $( ".padding-control" ).keyup(function() {
    $(".view-mode-<?= $myCmsId ?>" ).css("padding",$(".padding-control").val()+"px");
  });
  $( ".padding-control" ).blur(function() {
    tplCtx = {};
    tplCtx.id = "<?php echo $myCmsId ?>";
    tplCtx.collection = "cms";
    tplCtx.path = "css.padding.general"; 
    tplCtx.value = $(".padding-control").val();

    dataHelper.path2Value( tplCtx, function(params) {} );
  });


  $(".content-size").click(function(){
    var $el = $('.whole-content-<?= $myCmsId ?>');
    var classList = $el.attr('class').split(' ');

    $.each(classList, function(id, item) {
      if (item.indexOf('col-md-') == 0) $el.removeClass(item);
    });

    $el.addClass($(this).data("size"));
    tplCtx = {};
    tplCtx.id = "<?php echo $myCmsId ?>";
    tplCtx.collection = "cms";
    tplCtx.path = "class.width"; 
    tplCtx.value = $(this).data("size");

    dataHelper.path2Value( tplCtx, function(params) {} );
  });


  $(".this-content-<?= $myCmsId ?>").draggable({
    cancel: '.editable',
    start : function(event, ui){      
      $("#toolsBar").html(``);
    },
    stop: function(event, ui){
      revert: true,
      tplCtx = {};
      tplCtx.id = "<?php echo $myCmsId ?>";
      tplCtx.collection = "cms";
      tplCtx.path = "position"; 
      tplCtx.value = {};
      tplCtx.value.top = $(".this-content-<?= $myCmsId ?>").css( "top" );
      tplCtx.value.left = $(".this-content-<?= $myCmsId ?>").css( "left" );

      dataHelper.path2Value( tplCtx, function(params) {} );
    }
  });

  $(this).children(".row").toggle();
});

$(".stop-propagation").click(function(e) {
  e.stopPropagation();
});
});


$("#toolsBar").draggable();

$(function () {
  var count = 1;

  // Create click event handler on the document.
  $(document).on("click", function (event) {
    // If the target is not the container or a child of the container, then process
    // the click event for outside of the container.
    if ($(event.target).closest(".this-content-<?= $myCmsId ?> , #toolsBar").length === 0) {      
      $("#toolsBar").html("");
      $("#toolsBar").hide();    
      if ( typeof superCms == 'function' ) { 
       superCms.editMode();
     }
   }
  });

  $(".this-content-<?= $myCmsId ?>").on("click", function () {
    $(".<?= $myCmsId ?>-tools").show();
  });
});
  $('.this-content-<?= $myCmsId ?>').click(function(event){
    event.stopPropagation();
  });

// sectionDyf.<?php echo $kunik ?>Params = {
//       "jsonSchema" : {    
//         "title" : "Configurer votre section",
//         "description" : "Personnaliser votre gallerie",
//         "icon" : "fa-cog",
        
//         "properties" : {       
//         },
//         beforeBuild : function(){
//             uploadObj.set("cms","<?php echo (string)$blockCms["_id"] ?>");
//         },
//         save : function (data) {  
//           tplCtx.value = {};
//           $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
//             tplCtx.value[k] = $("#"+k).val();
//             if (k == "parent")
//               tplCtx.value[k] = formData.parent;
//           });

//           if(typeof tplCtx.value == "undefined")
//             toastr.error('value cannot be empty!');
//             else {
//               dataHelper.path2Value( tplCtx, function(params) {
//                 dyFObj.commonAfterSave(params,function(){
//                   toastr.success("Élément bien ajouter");
//                   $("#ajax-modal").modal('hide');
//                   urlCtrl.loadByHash(location.hash);
//                 });
//               } );
//             }

//         }
//       }
//     };
//     $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
//       tplCtx.id = $(this).data("id");
//       tplCtx.collection = $(this).data("collection");
//       tplCtx.path = "allToRoot";
//       dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, null);
//     });

</script>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque faucibus dolor sit amet eros elementum rutrum. Sed eget vulputate libero. Donec eleifend, ante in varius pharetra, eros nunc blandit massa, in sodales massa neque non massa. Pellentesque vehicula tincidunt mauris a mattis. Suspendisse tincidunt vehicula lorem ut mattis. Suspendisse rhoncus congue tincidunt. Sed feugiat venenatis dolor, sed ultrices nisi imperdiet sit amet. Vivamus lacinia massa odio, quis sollicitudin augue ultrices quis. Quisque non finibus nisi, at efficitur lorem. Duis ut velit sit amet ligula commodo egestas nec eget tellus.
