<?php 
/* 
Super container:
Create by Ifaliana Arimanana
ifaomega@gmail.com
26 Apr 2021
*/


/***************** Required *****************/
$keyTpl     ="container";
$myCmsId    = @$blockCms["_id"]->{'$id'};
$subtype    = $blockCms["subtype"] ?? $blockCms["subtype"] ?? "";
$params     = array();
$paramsData = array();

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
    if (  isset($blockCms[$e]) ) {
      $paramsData[$e] = $blockCms[$e];
    }
  }
} 
/*************** End required ***************/

/*****************get image uploaded***************/
$initImage = Document::getListDocumentsWhere(
  array(
    "id"=> $myCmsId,
    "type"=>'cms',
    "subKey"=>'block',
  ), "image"
);

  $latestImg = isset($initImage["0"]["imagePath"])?$initImage["0"]["imagePath"]:"empty" ;

/*****************End get image uploaded***************/

/* Get settings */

$widthClass       = $blockCms["class"]["width"] ?? $blockCms["class"] ?? "50";
$otherClass       = $blockCms["class"]["other"] ?? $blockCms["class"]["other"] ?? "";
$otherCss         = $blockCms["css"]["other"] ?? $blockCms["css"]["other"] ?? "height: max-content;";
$otherHtml        = $blockCms["html"] ?? $blockCms["html"] ?? "";
$pageHtml         = $blockCms["pageHtml"] ?? $blockCms["pageHtml"] ?? "";
$array_position   = $blockCms["css"]['position'] ?? $blockCms["css"]['position'] ?? [];
$width            = $blockCms["css"]["size"]["width"] ?? $blockCms["css"]["size"]["width"] ?? "50%";
$objfit           = $blockCms["css"]["object-fit"] ?? $blockCms["css"]["object-fit"] ?? "none";
$height           = $blockCms["css"]["size"]["height"] ?? $blockCms["css"]["size"]["height"] ?? "";
$backgroundColor  = $blockCms["css"]["background"]["color"] ?? $blockCms["css"]["background"]["color"] ?? "transparent";


  /**********Border**********/
$borderColor                = $blockCms["css"]["border"]["color"] ?? $blockCms["css"]["border"]["color"] ?? "transparent";
$borderRadiustopLeft        = $blockCms["css"]["border"]["radius"]["top-left"] ?? $blockCms["css"]["border"]["radius"]["top-left"] ?? "0";
$borderRadiustopRight       = $blockCms["css"]["border"]["radius"]["top-right"] ?? $blockCms["css"]["border"]["radius"]["top-right"] ?? "0";
$borderRadiusbottomRight    = $blockCms["css"]["border"]["radius"]["bottom-right"] ?? $blockCms["css"]["border"]["radius"]["bottom-right"] ?? "0";
$borderRadiusbottomLeft     = $blockCms["css"]["border"]["radius"]["bottom-left"] ?? $blockCms["css"]["border"]["radius"]["bottom-left"] ?? "0";
$borderRadius               = max($borderRadiustopLeft,$borderRadiustopRight,$borderRadiusbottomRight,$borderRadiusbottomLeft);
  /********End border********/

  /**********Padding**********/
$paddingtop     = $blockCms["css"]["padding"]["top"] ?? $blockCms["css"]["padding"]["top"] ?? "0";
$paddingright   = $blockCms["css"]["padding"]["right"] ?? $blockCms["css"]["padding"]["right"] ?? "0";
$paddingleft    = $blockCms["css"]["padding"]["left"] ?? $blockCms["css"]["padding"]["left"] ?? "0";
$paddingbottom  = $blockCms["css"]["padding"]["bottom"] ?? $blockCms["css"]["padding"]["bottom"] ?? "0";
$padding        = max($paddingtop,$paddingright,$paddingleft,$paddingbottom);
  /********End padding********/

  /**********Margin**********/
$margintop     = $blockCms["css"]["margin"]["top"] ?? $blockCms["css"]["margin"]["top"] ?? "0";
$marginright   = $blockCms["css"]["margin"]["right"] ?? $blockCms["css"]["margin"]["right"] ?? "0";
$marginleft    = $blockCms["css"]["margin"]["left"] ?? $blockCms["css"]["margin"]["left"] ?? "0";
$marginbottom  = $blockCms["css"]["margin"]["bottom"] ?? $blockCms["css"]["margin"]["bottom"] ?? "0";
$margin        = max($margintop,$marginright,$marginleft,$marginbottom);
  /********End margin********/

  /**********shadow**********/
  $inset    = $blockCms["css"]["box-shadow"]["inset"] ?? $blockCms["css"]["box-shadow"]["inset"] ?? "";
  $shwColor = $blockCms["css"]["box-shadow"]["color"] ?? $blockCms["css"]["box-shadow"]["color"] ?? "transparent";
  $axeX     = $blockCms["css"]["box-shadow"]["x"] ?? $blockCms["css"]["box-shadow"]["x"] ?? "0";
  $axeY     = $blockCms["css"]["box-shadow"]["y"] ?? $blockCms["css"]["box-shadow"]["y"] ?? "0";
  $blur     = $blockCms["css"]["box-shadow"]["blur"] ?? $blockCms["css"]["box-shadow"]["blur"] ?? "0";
  $spread   = $blockCms["css"]["box-shadow"]["spread"] ?? $blockCms["css"]["box-shadow"]["spread"] ?? "0";
  /********End shadow********/

  /************Separtor************/
  $lineSeparator    = $blockCms["css"]["lineSeparator"] ?? $blockCms["css"]["lineSeparator"] ?? "";


/* End get settings */

?>
<style type="text/css">
  .unselectable {
    -moz-user-select: none;
    -webkit-user-select: none;
    -ms-user-select:none;
    user-select:none;
    -o-user-select:none;
  }

  .<?= $kunik ?> {  
    display: inline-block;
    width:100%;
    /*height: auto;*/
    /*position: sticky; */
    <?php if ($latestImg !== "empty") { ?>
    background-image: url('<?php echo $latestImg ?>');
    <?php } ?>     
    border: solid <?= $borderColor ?>;
    background-color: <?= $backgroundColor ?>;
    border-radius: <?= $borderRadiustopLeft ?>% <?= $borderRadiustopRight ?>% <?= $borderRadiusbottomRight ?>% <?= $borderRadiusbottomLeft ?>%; 
    padding: <?= $paddingtop ?>% <?= $paddingright ?>% <?= $paddingbottom ?>% <?= $paddingleft ?>%;
    margin: <?= $margintop ?>% <?= $marginright ?>% <?= $marginbottom ?>% <?= $marginleft ?>%;
    <?php 
      echo "-webkit-box-shadow: ".$inset." ".$shwColor." ".$axeX."px ".$axeY."px ".$blur."px ".$spread."px;"; 
      echo "-moz-box-shadow: ".$inset." ".$shwColor." ".$axeX."px ".$axeY."px ".$blur."px ".$spread."px;"; 
      echo "box-shadow: ".$inset." ".$shwColor." ".$axeX."px ".$axeY."px ".$blur."px ".$spread."px;"; 
     ?>
  }

  .other-css-<?= $kunik ?> {  
    <?php 
    if (is_array($otherCss)) {
     foreach ($otherCss as $csskey => $cssvalue) {
      echo $csskey.":".$cssvalue.";\r\n";
    }
  }else{
    echo $otherCss;
  } ?>
}

  .edit-<?= $kunik ?> {
    border: dashed 2px gray;
  }


  .selected-mode-<?= $kunik ?>:hover {
    cursor: context-menu;
  }
  .selected-mode-<?= $kunik ?>:hover > .stop-propagation {
    cursor: default;
  }

  .selected-mode-<?= $kunik ?> {
    box-shadow: #92bfffa6 0px 0px 2px 2px;
    border : 2px dotted white;
  }

  .bg-transparent{
    background-color: transparent;
  }

  .super-<?= $myCmsId ?>{
    width : <?= $width ?>;
    min-height: <?= $width ?>;
  }

   .cards-list {
    z-index: 0;
    width: 100%;
    display: flex;
    justify-content: space-around;
    flex-wrap: wrap;
  }

  .card {
    margin: 5px auto;
    width: 90px;
    height: 85px;
    border-radius: 10px;
    box-shadow:  0px 0px 6px 0px rgb(0 0 0 / 22%);
    cursor: pointer;
    transition: 0.4s;
  }

  .card .card_image {
    width: inherit;
    height: inherit;
    border-radius: 10px;
  }

  .card .card_image img {
    width: inherit;
    height: inherit;
    border-radius: 10px;
    object-fit: cover;
  }

  .card .card_title {
    text-align: center;
    font-family: sans-serif;
    font-size: 16px;
    margin-top: -55px;
  }

  .card:hover {
    transform: scale(0.9, 0.9);
    box-shadow: 0px 0px 0px 6px rgb(0 0 0 / 10%);
  }

  .title-white {
    color: white;
    top: -20px;
    position: relative;
  }

  #toolsBar .nav>li>a{
    padding: 0px 10px;
    font-family: 'Circular-Loom';
  }

  #toolsBar .nav{
    background-color: #ffffff42;
  }

  #toolsBar .tool-scms-title{
    background-color: #ffffff42;
  }

  #toolsBar .input-group {
    width: 100%;
    /* position: relative; */
    display: table;
    /* border-collapse: separate; */
  }

  .title-black {
    background-color: white;
    color: #37475e;
    border-radius: 10px 10px 0px 0px;
    top:-30px;
    position: relative;
  }
  .sp-cms-options {
    background-color: #cecece59;
    padding-bottom: 5px;
    padding-top: 10px;
  }

  @media all and (max-width: 500px) {
    .card-list {
      flex-direction: column;
    }
  }
  .super-cms-range-slider{ 
    -webkit-appearance: none;
    background: <?= $shwColor."6b" ?>;
    border: solid 1px #999;
    outline: none;
    opacity: 0.7;
    height: 10px;
    -webkit-transition: .2s;
    transition: opacity .2s;
  }

  .super-cms-range-slider::-webkit-slider-thumb {
    -webkit-appearance: none;
    appearance: none;
    width: 20px;
    height: 20px;
    background: black;
    cursor: pointer;
    border: solid 2px darkgray;
    border-radius: 50%;
  }

  .block-container-<?= $kunik ?> {
    text-align: center;
  }

  .col-elem-value { 
    display: flex;
    flex-wrap: wrap;
  }

  .element-field {
    background-color: #47525d0d;
    padding: 10px;
    flex: 50%;
  }

  .super-element {
    padding: 5px;
  }

  @media (max-width: 800px) {
    .elem-value {
      flex-direction: column;
    }
  }

   @media (max-width: 800px) {
      .<?= $kunik ?> {
        width: 100% !important;
       /* height: auto !important;*/
        /*min-height: auto !important;*/
        background-size: contain;
        padding-left: 0px;
        padding-right: 0px;
        margin-left: 0px;
        margin-right: 0px;
      }

    div {
    margin-right: 0px !important;
    margin-left: 0px !important;
    }

    <?php if ($blockCms["type"] === "blockCopy") { ?>
      .block-container-<?= $kunik ?> {
        margin-top: 1px !important;
        margin-bottom: 1px !important;
        /*padding-top: 10px !important;
        padding-bottom: 10px !important;*/
        padding: 0 !important;
      }
    <?php }else{ ?>
      .whole-<?= $kunik ?> {
        padding-left: 0px !important;
        padding-right: 0px !important;
        margin-left: 0px !important;
        margin-right: 0px !important;
      }

      

    <?php } ?>
  }
  .spDrag-<?= $kunik ?> {
    top: -15px;
    z-index: 100000;
    position: absolute;
    right: 50px;
    cursor: pointer;
    display: none;
    font-size: 20px;
    -webkit-text-fill-color: #37474f;
    -webkit-text-stroke-width: 1px;
    -webkit-text-stroke-color: #ffffff;
  }  
  .spCustom-<?= $kunik ?> {
    top: -15px;
    z-index: 100000;
    position: absolute;
    right: 70px;
    cursor: pointer;
    display: none;
    font-size: 20px;
    -webkit-text-fill-color: #37474f;
    -webkit-text-stroke-width: 1px;
    -webkit-text-stroke-color: #ffffff;
  }
</style>

    <div class="whole-<?= $kunik ?> sp-cms-<?= $widthClass ?> <?= $widthClass ?> <?= $kunik ?> super-cms <?= $otherClass ?> other-css-<?= $kunik ?> text-left" style="<?php foreach ($array_position as $key => $value) {
      echo $key." : ".$value."; "; } ?>;width : <?= $width ?>;min-height: <?= $height ?>;">

      <a class="spDrag-<?= $kunik ?> tooltips sp-settings-btn" data-placement="left" data-original-title="Maintenir et glisser votre souris pour déplacer"><i class="fa fa-arrows"></i></a>
      <a class="spCustom-<?= $kunik ?> tooltips sp-settings-btn" data-placement="left" data-original-title="Paramétres"><i class="fa fa-cog"></i></a>      
      <div class="html<?= $kunik ?>" style="background-size:100%;width: 100%;display: none;"><?= $otherHtml ?></div>

          <?php
          $idElem = PHDB::find("cms", array("type" => "blockChild" , "tplParent" => $myCmsId ));
          if (!empty($idElem)) {
            foreach ($idElem as $key => $value) {
              $pathExplode = explode('.', $value["path"]);
              $count = count($pathExplode);
              $superKunik = $pathExplode[$count-1].$value["_id"];
              $blockKey = (string)$value["_id"];
              $cmsElement = $value["path"];       
              $params = [
                "blockCms"  =>  $value,
                "kunik"     =>  $superKunik,
                "blockKey"  =>  $blockKey,
                "el"        =>  $el
              ];
              echo $this->renderPartial("costum.views.".$cmsElement,$params); 
            if(Authorisation::isInterfaceAdmin()){?>
                <?php 
                echo $this->renderPartial("costum.views.tpls.editSuperCms", [
                  "canEdit" => true, 
                  "kunik"   => $superKunik,
                  "parentN" => $blockCms["name"],
                  "name"    => $value["name"],
                  "subtype" => isset($value["subtype"]) ? $value["subtype"] : "",
                  "id"      => $blockKey
                ]);  
                ?>
            <?php } 
            }


          }elseif ($latestImg !== "empty") { ?> 
       

          <?php
          }elseif($otherHtml !==""){ ?> 
          <script type="text/javascript">
            $(".html<?= $kunik ?>").css("display","block");
         </script>
         <?php

          }elseif (($viewFile = $this->getViewFile("costum.views.custom.".$this->costum["contextSlug"].".".$pageHtml)) !== false && $otherHtml !==""){
                      
              echo $this->renderPartial("costum.views.custom.".$this->costum["contextSlug"].".".$pageHtml);
           
          }elseif(Authorisation::isInterfaceAdmin()){ ?>  
          <div class="hiddenPreview empty-sp-element<?= $kunik ?>" style="
          height: 25vh;
          width: 100%;
          top: 50%;
          margin: 2px 0 0;
          font-size: 35px;
          color: slategrey;
          text-align: center;
          ">
          <h3 style="font-weight: normal;">Aucun element</h3>
          </div><?php } ?>
        </div>
  

<script type="text/javascript">  
  var size<?= $kunik ?> = "<?= $width ?>";
  var widthClass<?= $kunik ?> = "<?= $widthClass ?>";
  var pageHtml<?= $kunik ?> = "<?= $pageHtml ?>";
  var other_css<?= $kunik ?> = <?= json_encode($otherCss) ?>;
  var other_class<?= $kunik ?> = `<?= $otherClass ?>`;
  var otherHtml<?= $kunik ?> = `<?= $otherHtml ?>`;

  if ("supercms" !== "<?= $subtype ?>" ) {
    tplCtx = {};
    tplCtx.id = "<?php echo $myCmsId ?>";
    tplCtx.collection = "cms";
    tplCtx.path = "allToRoot"; 
    tplCtx.value = {};
    tplCtx.value.subtype = "supercms";
    dataHelper.path2Value( tplCtx, function(params) { } );
  }
  
/*$(".<?= $kunik ?>").mousemove(function( event ) {     
  curs_prop = $(".<?= $kunik ?>").css('cursor'); 
  });*/
/*$(document).mousemove(function(){
  var css_property = $(".<?= $kunik ?>").css('cursor');
  console.log("cursor", css_property);
})*/

var lineSeparator = "";
<?php if (isset($blockCms["css"]["separator"]) == "true") { ?>
  $('.whole-<?= $kunik ?>').append(`<hr class="hr-<?= $kunik ?>" style="width:40%; margin:20px auto; border: 1px dashed #6CC3AC;">`)
  lineSeparator = "true";
<?php } ?>
/******************Responsivity manager******************/
//  var $el = $('.whole-<?= $kunik ?>');
//  var classList = $el.attr('class').split(' ');
//  $.each(classList, function(id, item) {
//   if (item.indexOf('col-md-') == 0) $el.removeClass(item);
//     // alert(item);
//   });
//  $el.addClass("col-md-12");
//  $('.sortable-<?= $kunik ?>').wrap('<div class="mod container"></div>');
//   // $('.sortable-<?= $kunik ?>').css('width','500px');
//   $('.mod').css('width','400px');
//    // $el.addClass("col-md-12");

//   $(function() {
//   function response() {
//     if ($(window).width() > 480 && $(window).width() < 768) {
//       $("body").css({
//         "background-color": "red",
//         color: "black"
//       });
//     } else {
//       $("body").css({
//         "background-color": "black",
//         color: "white"
//       });
//     }
//   }

//   response();

//   $(window).resize(function() {
//     response();
//     $(".windowWidth").html("Window Width: " + $(this).width() + "px");
//   });
// });

/******************End responsivity manager******************/
mode = localStorage.getItem("previewMode");
<?php if(Authorisation::isInterfaceAdmin()){?>
  function changeParent<?= $kunik ?>() {
    tplCtx = {};
    tplCtx.id = "<?php echo $myCmsId ?>";
    tplCtx.collection = "cms";
    tplCtx.path = "tplParent"; 
    tplCtx.value = $("#prt<?= $kunik ?>").val();
    dataHelper.path2Value( tplCtx, function(params) {  
      toastr.success("Parent updated!");
      urlCtrl.loadByHash(location.hash);
     } );
  }
  

sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
last_edited = "";
 /*********Initialize color picker*********/
function loadColorPicker(callback) {
  mylog.log("loadColorPicker");
  if( ! jQuery.isFunction(jQuery.ColorPicker) ) {
    mylog.log("loadDateTimePicker2");
    lazyLoad( baseUrl+'/plugins/colorpicker/js/colorpicker.js', 
      baseUrl+'/plugins/colorpicker/css/colorpicker.css',
      callback);
  }
}
function loadSpectrumColorPicker(callback) {
  mylog.log("loadSpectrumColorPicker");
  lazyLoad( baseUrl+'/plugins/spectrum-colorpicker2/spectrum.min.js', 
    baseUrl+'/plugins/spectrum-colorpicker2/spectrum.min.css',
    callback);

}
var initColor = function(){
  mylog.log("init .colorpickerInput");
  $(".colorpickerInput").ColorPicker({ 
    color : "pink",
    onSubmit: function(hsb, hex, rgb, el) {
      $(el).val(hex);
      $(el).ColorPickerHide();
    },
    onBeforeShow: function () {
      $(this).ColorPickerSetColor(this.value);
    }
  }).bind('keyup', function(){
    $(this).ColorPickerSetColor(this.value);
  });
};

jQuery.fn.removeClassExcept = function (val) {
  return this.each(function (index, el) {
    var keep = val.split(" "),
    reAdd = [],
    $el = $(el);         
    for (var i = 0; i < keep.length; i++){
      if ($el.hasClass(keep[i])) reAdd.push(keep[i]);
    }          
    $el
    .removeClass()
    .addClass(reAdd.join(' '));
  });
};


/*********End color picker initialization*********/
  jQuery(document).ready(function() {
/*-----View and edit mode------- */
    superCms<?= $kunik ?> = {
      viewMode: function(){
        // $(".<?= $kunik ?>").addClass("default-<?= $kunik ?>");
        $(".<?= $kunik ?>").resizable();
        $(".<?= $kunik ?>").resizable("destroy");
        $(".<?= $kunik ?>").removeClass("edit-<?= $kunik ?>");
        $(".<?= $kunik ?>").removeClass("selected-mode-<?= $kunik ?>");
      },
      editMode: function(){
        $(".<?= $kunik ?>").addClass("edit-<?= $kunik ?>");
        mode = "w";
        // $(".<?= $kunik ?>").removeClass("default-<?= $kunik ?>");
        // $(".<?= $kunik ?>").removeClass("selected-mode-<?= $kunik ?>");
        $(".<?= $kunik ?>").resizable({          
          handles: 'e, w',
          start:function(event, ui){            
            // $(".whole-<?= $kunik ?>").css("min-height", "auto");
          },
          stop: function(event, ui){
            var myWidth = $(".whole-<?= $kunik ?>").width();
            var myHeight = $(".whole-<?= $kunik ?>").height();
            var parentWidth = $(".whole-<?= $kunik ?>").parent().width();
            if ("<?= $blockCms['type'] ?>" === "blockCopy") {
              parentWidth = $( window ).width();
            }
            widthPrnt = (myWidth / parentWidth) * 100;
            $(".whole-<?= $kunik ?>").css("height", "inherit");
            // $(".whole-<?//= $kunik ?>").css("min-height", (myHeight+4)+"px");
            $(".whole-<?= $kunik ?>").css("width", (widthPrnt+0.374)+"%");
            revert: true,
            tplCtx = {};
            tplCtx.id = "<?php echo $myCmsId ?>";
            tplCtx.collection = "cms";
            tplCtx.path = "css.size"; 
            tplCtx.value = {};
            tplCtx.value.css = {};
            tplCtx.value.width = widthPrnt+"%";
            // tplCtx.value.height = myHeight+"px";

            dataHelper.path2Value( tplCtx, function(params) { toastr.success("Modification enrégistré");} );
            superCms<?= $kunik ?>.editMode();
            size<?= $kunik ?> = widthPrnt+0.374+"%";
          }
        });

      },
      selectetMode: function(){
        $(".<?= $kunik ?>").removeClass("edit-<?= $kunik ?>");
        // $(".<?= $kunik ?>").removeClass("default-<?= $kunik ?>");
        // $(".<?= $kunik ?>").addClass("selected-mode-<?= $kunik ?>");
      }
    }

    if(mode == "w"){
     superCms<?= $kunik ?>.editMode();
   }

  
   /*********************Mouse hover edit mode*********************/


   $('.<?= $kunik ?>').mouseenter(function(e){
    if (mode === "w" && $("#toolsBar").is(":hidden")) {
      e.stopPropagation();
      $(this).addClass("edit-<?= $kunik ?>");
      $(".edit-<?= $kunik ?>").removeClass("edit-<?= $kunik ?>");
      $(".<?= $kunik ?>").addClass("selected-mode-<?= $kunik ?>");
      $(".container<?= @$blockCms['tplParent'] ?>").addClass("edit-container<?= @$blockCms['tplParent'] ?>");
      $(".selected-mode-container<?= @$blockCms['tplParent'] ?>").removeClass("selected-mode-container<?= @$blockCms['tplParent'] ?>");
      $(".spDrag-container<?= @$blockCms['tplParent'] ?>").hide();
      $(".spCustom-container<?= @$blockCms['tplParent'] ?>").hide();

      $(".spDrag-<?= $kunik ?>").show();
      $(".spCustom-<?= $kunik ?>").show();
/*      var spElems = $(document);
      var classList = spElems.attr('class').split(' ');
      $.each(classList, function(id, item) {
        if (item.indexOf('selected-mode-container') == 0) spElems.removeClass(item);
      });
      spElems.addClass("edit-<?= $kunik ?>");*/

     
    }
  });

   $('.<?= $kunik ?>').mouseleave(function(e){
     if (mode === "w" && $("#toolsBar").is(":hidden")) {
      e.stopPropagation();
      $(".edit-<?= $kunik ?>").removeClass("edit-<?= $kunik ?>");
      $(".<?= $kunik ?>").addClass("selected-mode-<?= $kunik ?>");
      $(".spDrag-<?= $kunik ?>").hide();
      $(".spCustom-<?= $kunik ?>").hide();
      $(".<?= $kunik ?>").addClass("edit-<?= $kunik ?>");
      $(".selected-mode-<?= $kunik ?>").removeClass("selected-mode-<?= $kunik ?>");
    }
  });
   /*********************End mouse hover edit mode*********************/


   $(document).keyup(function(e) {
    if (e.altKey == true && e.keyCode == 80) {
      if (previewMode == false) {
        superCms<?= $kunik ?>.viewMode();
        mode = "v"
        $("#toolsBar").hide(); 
        $(".spCustom-<?= $kunik ?>").hide(); 
      }else{
        superCms<?= $kunik ?>.editMode();
        mode = "w";
        $("#toolsBar").hide();
      }
    }

    if (e.altKey == true && e.keyCode == 8) {
      $(".showPrt<?= $kunik ?>").removeClass("hidden");
    }
  });

    $(".view-super-cms").click(function(){
      mode = "v"
      superCms<?= $kunik ?>.viewMode();
      $("#toolsBar").hide();   
      $(".spCustom-<?= $kunik ?>").hide();   
    });

    $(".hiddenEdit").click(function(){
       if (previewMode == false) {
        superCms<?= $kunik ?>.viewMode();
        mode = "v"
        $("#toolsBar").hide(); 
        $(".spCustom-<?= $kunik ?>").hide(); 
      }else{
        superCms<?= $kunik ?>.editMode();
        mode = "w";
        $("#toolsBar").hide();
      }
   })

    $(document).keyup(function(e) {
      if (e.key === "Escape") { 
       superCms<?= $kunik ?>.editMode();
       mode = "w";
      $("#toolsBar").hide();
      }
    });

/*-----End view and edit mode------- */

/***************Drag and resize*******************/
var canDrag<?= $kunik ?> = true;

/*$(".spDrag-<?= $kunik ?>").off().on("click",function(){
    alert(canDrag<?= $kunik ?>);
});*/
// Desable drag when container has more item (Flexbox arrange item possition Automatically)
// if ($(".container<?//= $blockCms['tplParent'] ?> .super-container ").size() === 1) {
  <?php   if ($blockCms["type"] === "blockChild") { ?>    
   $(".spDrag-<?= $kunik ?>").hide();
   $(".spDrag-<?= $kunik ?>").off().on("mousedown",function(){
   if (canDrag<?= $kunik ?> === false) {   
     $(".<?= $kunik ?>").draggable("enable"); 
     canDrag<?= $kunik ?> = true; 
   }
   /* $(document).on('click', '.spDrag-<?= $kunik ?>', function () {*/
      $(".<?= $kunik ?>").draggable({
        cancel: '.editable',
        start : function(event, ui){      
          $("#toolsBar").html(``);
          $("#toolsBar").hide(); 

          superCms<?= $kunik ?>.viewMode();
          $("#toolsBar").hide();   
          $(".spCustom-<?= $kunik ?>").hide();
          $(".spDrag-<?= $kunik ?>").hide();   
        },
        stop: function(event, ui){
          //convert position from px to %
          var myLeft = $(".<?= $kunik ?>").css("left");
          var parentWidth = $(".whole-<?= $kunik ?>").width()-20;
          myLeft = parseInt(myLeft);
          LeftPrnt = (myLeft / parentWidth) *100;
         /* $(".<?= $kunik ?>").css("left", (LeftPrnt)+"%");*/
          revert: true,
          tplCtx = {};
          tplCtx.id = "<?php echo $myCmsId ?>";
          tplCtx.collection = "cms";
          tplCtx.path = "css.position"; 
          tplCtx.value = {};
          tplCtx.value.top = $(".<?= $kunik ?>").css( "top" );
          tplCtx.value.left = myLeft+"px";

          dataHelper.path2Value( tplCtx, function(params) { toastr.success("Modification enrégistré");} );

          $(".<?= $kunik ?>").draggable({ disabled: true });
          canDrag<?= $kunik ?> = false ;

          superCms<?= $kunik ?>.editMode();
          $("#toolsBar").hide();   
          $(".spCustom-<?= $kunik ?>").show();   
          $(".spDrag-<?= $kunik ?>").show();   
        }
      });
  });

/*   $(".spDrag-<?= $kunik ?>").mouseup(function(){
  });*/

<?php } ?> 
// }
/*****************End drag and resize*****************/

/********************Display menu**********************/
$(".spCustom-<?= $kunik ?>").click(function(){
  if (mode == "w") { 
    var highlightedText = "";
  if (window.getSelection) {
      highlightedText = window.getSelection().toString();
  }
    if (highlightedText == "") {
    // if ($.trim($("#toolsBar").html()) != "") {   
    //   $("#toolsBar").hide(); 
      if(last_edited != ""){
        $("."+last_edited).addClass("edit-"+last_edited);    
      }
      last_edited = "<?= $kunik ?>";

      $(".<?= $kunik ?>").removeClass("selected-mode-<?= $kunik ?>");
      $("#toolsBar").html("");
      //   superCms<?= $kunik ?>.editMode();
      // }else{
        superCms<?= $kunik ?>.selectetMode();

        $(".spCustom-<?= $kunik ?>").hide();
        $(".spDrag-<?= $kunik ?>").hide();
        $("#toolsBar").show();  
        $("#toolsBar").append(
          `
          <div class="text-center tool-scms-title cursor-move">
            <i style="cursor:default;font-size: large;" aria-hidden="true">Paramètres de containeur</i>
            <button class="bg-transparent deleteLine delete-super-cms tooltips" data-path="<?= $blockCms['path'] ?>" data-id="<?= $blockCms['_id']; ?>" data-collection="cms" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Supprimer cet élément">
              <i class="fa fa-trash text-red" aria-hidden="true"></i>
            </button>
            <i class="fa fa-window-close closeBtn view-super-cms" aria-hidden="true"></i>
          </div>

          <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#addElt">Insértion</a></li>
            <li><a data-toggle="tab" href="#home">Général</a></li>
            <li><a data-toggle="tab" href="#menu1">Ombre</a></li>
            <li><a data-toggle="tab" href="#menu2">Autre class</a></li>
            <li><a data-toggle="tab" href="#menu3">Autre css</a></li>
            <li><a data-toggle="tab" href="#menu4">HTML</a></li>
            <li class="dropdown sp-cms-20">Taille<input class="dropdown-toggle sizes" data-toggle="dropdown" style="width: 70px;height: 25px;border: solid 1px #e0e0e0; margin-left: 5px;">
              <ul class="dropdown-menu">
                <li class="content-size text-center" data-size="10"><a>10</a></li>
                <li class="content-size text-center" data-size="20"><a>20</a></li>
                <li class="content-size text-center" data-size="30"><a>30</a></li>
                <li class="content-size text-center" data-size="40"><a>40</a></li>
                <li class="content-size text-center" data-size="50"><a>50</a></li>
                <li class="content-size text-center" data-size="60"><a>60</a></li>
                <li class="content-size text-center" data-size="70"><a>70</a></li>
                <li class="content-size text-center" data-size="80"><a>80</a></li>
                <li class="content-size text-center" data-size="90"><a>90</a></li>
                <li class="content-size text-center" data-size="100"><a>100</a></li>
                <li class="content-size text-center" data-size="container"><input id="check-container" type="checkbox" disabled>
                    <label for="check1">Container</label><a></a></li>
              </ul>
            </li>
          </ul>


          <div class="tab-content">
            <div id="home" class="tab-pane fade">
              <div class="col-md-12 sp-cms-options">
                <div class="col-md-6">      
                  <div class="panel-group">
                    <label>Rayon de la bordure</label>                      
                    <div class="input-group">
                      <input class="form-control elem-control" type="number" min="0" value="<?= $borderRadius ?>" placeholder="0" data-path="border.radius" data-given="border-radius" data-what="border-control"> 
                      <div class="input-group-btn">                    
                        <button  data-toggle="collapse" data-target="#border" class="tooltips btn btn-primary" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Plus de réglage">
                          <i class="fa fa-plus" aria-hidden="true"></i><br>
                        </button> 
                      </div>
                    </div>
                    <div id="border" class="collapse elem-value">
                    <div class="col-elem-value">
                      <div class="element-field"> 
                        <label>Haut à gauche</label>                
                        <input class="form-control top-left border-control" type="number" min="0" value="<?= $borderRadiustopLeft ?>" placeholder="0" data-path="border.radius" data-given="border-top-left-radius"> 
                      </div>                  
                      <div class="element-field">      
                        <label>Haut à droite</label>                   
                        <input class="form-control top-right border-control" type="number" min="0" value="<?= $borderRadiustopRight ?>" placeholder="0" data-path="border.radius" data-given="border-top-right-radius"> 
                      </div>
                       <div class="element-field"> 
                        <label>Bas à gauche</label>                           
                        <input class="form-control bottom-left border-control" type="number" min="0" value="<?= $borderRadiusbottomLeft ?>" data-path="border.radius" data-given="border-bottom-left-radius" placeholder="0"> 
                      </div>                  
                      <div class="element-field">      
                        <label>Bas à droite</label>                   
                        <input class="form-control bottom-right border-control" type="number" min="0" value="<?= $borderRadiusbottomRight ?>" data-path="border.radius" data-given="border-bottom-right-radius" placeholder="0"> 
                      </div>
                    </div>
                    </div>
                  <label>Couleur de la bordure</label>
                  <input id="border-color" class="sp-color-picker spectrum sp-colorize form-control" name="" value="<?= $borderColor ?>" placeholder="#ffffff" style="">
                  <label>Ajustement du fond (Photo)</label>
                    <select class="form-control" id="objfit">
                      <option value="none">None</option>
                      <option value="auto">Auto</option>
                      <option value="cover">Cover</option>
                      <option value="inherit">Inherit</option>
                      <option value="contain">Contain</option>
                    </select>
                  </div>  
                </div>
                <div class="col-md-6">      
                  <div class="panel-group">
                      <label>Rembourrage (Padding)</label>
                      <div class="input-group">
                      <input class="form-control elem-control" type="number" min="0" value="<?= $padding ?>" placeholder="0" data-path="padding" data-given="padding" data-what="padding-control"> 
                        <div class="input-group-btn">                           
                          <button  data-toggle="collapse" data-target="#padding" class="tooltips btn btn-primary" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Plus de réglage">
                            <i class="fa fa-plus" aria-hidden="true"></i><br>
                          </button> 
                        </div>
                      </div>
                      <div id="padding" class="collapse elem-value">
                        <div class="col-elem-value">
                          <div class="element-field" style="width:100px"> 
                            <label>En haut</label>                
                            <input class="form-control top-left padding-control" type="number" min="0" value="<?= $paddingtop ?>" placeholder="0" data-path="padding" data-given="padding-top"> 
                          </div>                  
                          <div class="element-field" style="width:100px">      
                            <label>À droite</label>                   
                            <input class="form-control top-right padding-control" type="number" min="0" value="<?= $paddingright ?>" placeholder="0" data-path="padding" data-given="padding-right"> 
                          </div>
                           <div class="element-field" style="width:100px"> 
                            <label>À gauche</label>                           
                            <input class="form-control bottom-left padding-control" type="number" min="0" value="<?= $paddingleft ?>" data-path="padding" data-given="padding-left" placeholder="0"> 
                          </div>                  
                          <div class="element-field" style="width:100px">      
                            <label>En bas</label>                   
                            <input class="form-control bottom-right padding-control" type="number" min="0" value="<?= $paddingbottom ?>" data-path="padding" data-given="padding-bottom" placeholder="0"> 
                          </div>
                        </div>
                      </div> 
                      <label>Marge</label>
                      <div class="input-group">
                      <input class="form-control elem-control" type="number" min="0" value="<?= $margin ?>" placeholder="0" data-path="margin" data-given="margin" data-what="margin-control"> 
                        <div class="input-group-btn">                           
                          <button  data-toggle="collapse" data-target="#margin" class="tooltips btn btn-primary" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Plus de réglage">
                            <i class="fa fa-plus" aria-hidden="true"></i><br>
                          </button> 
                        </div>
                      </div>
                      <div id="margin" class="collapse elem-value">
                        <div class="col-elem-value">
                          <div class="element-field" style="width:100px"> 
                            <label>En haut</label>                
                            <input class="form-control top-left margin-control" type="number" value="<?= $margintop ?>" placeholder="0" data-path="margin" data-given="margin-top"> 
                          </div>                  
                          <div class="element-field" style="width:100px">      
                            <label>À droite</label>                   
                            <input class="form-control top-right margin-control" type="number" value="<?= $marginright ?>" placeholder="0" data-path="margin" data-given="margin-right"> 
                          </div>
                           <div class="element-field" style="width:100px"> 
                            <label>À gauche</label>                           
                            <input class="form-control bottom-left margin-control" type="number" value="<?= $marginleft ?>" data-path="margin" data-given="margin-left" placeholder="0"> 
                          </div>                  
                          <div class="element-field" style="width:100px">      
                            <label>En bas</label>                   
                            <input class="form-control bottom-right margin-control" type="number" value="<?= $marginbottom ?>" data-path="margin" data-given="margin-bottom" placeholder="0"> 
                          </div>
                        </div>
                      </div>
                    <label>Couleur du fond</label>
                    <div class="input-group">
                    <input id="fond-color" class="sp-color-picker spectrum sp-colorize form-control" name="" value="<?= $backgroundColor ?>" placeholder="#ffffff" style="">
                        <div class="input-group-btn closeMenu">                    
                          <button class="edit-img<?php echo $kunik ?>myCmsId tooltips btn btn-primary" type="button" data-toggle="tooltip" data-placement="left" title="" data-original-title="Plus de réglage">
                            <i class="fa fa-camera" aria-hidden="true"></i><br>
                          </button> 
                        </div>
                    </div>
                  </div>
                </div>

                <div class="col-md-12">      
                  <div class="panel-group">
                    <input id="check-lineSeparator" type="checkbox" <?php if ($lineSeparator != "") {echo 'checked="checked"';} ?>>
                    <label for="check1">Activer la ligne separatrice</label>
                    <p></p>
                  </div>
                </div>        
              </div>  
            </div>
            <div id="menu1" class="tab-pane fade">
              <div class="col-md-12 sp-cms-options">      
                <div class="panel-group">        
                <label>Couleur</label>
                <div class="pull-right">
                <input id="check-inset" type="checkbox" <?php if ($inset != "") {echo 'checked="checked"';} ?>>
                <label for="check1">Inset</label>
                <p></p>
                </div>
                <input id="shw-color" class="sp-color-picker spectrum sp-colorize form-control" name="" value="<?= $shwColor ?>" placeholder="#ffffff" style="">
                  <div class="range-slider text-center padding-top-20">
                    <span class="range-blur__value">Blur <?= $blur ?>px</span>
                    <input class="range-slider-blur super-cms-range-slider" type="range" value="<?= $blur ?>" min="0" max="20" step="0.2">
                  </div>
                  <div class="range-slider text-center padding-top-20">
                    <span class="range-spread__value">Spread <?= $spread ?>px</span>
                    <input class="range-slider-spread super-cms-range-slider" type="range" value="<?= $spread ?>" min="0" max="20" step="0.2">
                  </div>
                  <div class="range-slider text-center padding-top-20">
                    <span class="range-x__value">Axe X: <?= $axeX ?>px</span>
                    <input class="range-slider-x super-cms-range-slider" type="range" value="<?= $axeX ?>" min="-10" max="10" step="0.2">
                  </div>
                  <div class="range-slider text-center padding-top-20">
                    <span class="range-y__value">Axe Y: <?= $axeY ?>px</span>
                    <input class="range-slider-y super-cms-range-slider" type="range" value="<?= $axeY ?>" min="-10" max="10" step="0.2">
                  </div>
                </div>
              </div>
            </div>
            <div id="addElt" class="tab-pane fade in active">    
              <div class="col-md-12 sp-cms-options"> 
              <div class="cards-list">

                  <div class="super-element">
                    <div class="card 4 add-type-cms" onclick="cmsBuilder.openModal();">
                      <div class="card_image">
                        <img src="<?php echo Yii::app()->getModule('costum')->assetsUrl?>/images/openCms/giphy.gif" />
                        </div>
                      <div class="card_title title-black">
                        <p>Bloc cms</p>
                      </div>
                    </div>
                  </div>
                  

                  <div class="super-element">
                    <div class="card 1 add-super-element" data-name="Super bloc" data-subtype="supercms" data-path="tpls.blockCms.superCms.elements.text">
                      <div class="card_image"> <img src="<?php echo Yii::app()->getModule('costum')->assetsUrl?>/images/openCms/super-text.png" /> </div>
                        <div class="card_title  title-black">
                          <p>Texte</p>
                        </div>
                      </div>
                    </div>

                  <div class="super-element">
                    <div class="card 2 add-super-element" data-name="Super bloc" data-subtype="supercms" data-path="tpls.blockCms.superCms.elements.image">
                      <div class="card_image">
                        <img src="<?php echo Yii::app()->getModule('costum')->assetsUrl?>/images/openCms/super-img.png" />
                        </div>
                      <div class="card_title title-black">
                        <p>Image</p>
                      </div>
                    </div>
                  </div>

                  <div class="super-element hidden">
                    <div class="card 2 add-super-element" data-name="Super bloc" data-subtype="supercms" data-path="tpls.blockCms.superCms.elements.forms">
                      <div class="card_image">
                        <img src="<?php echo Yii::app()->getModule('costum')->assetsUrl?>/images/openCms/super-img.png" />
                        </div>
                      <div class="card_title title-black">
                        <p>Forms</p>
                      </div>
                    </div>
                  </div>

                  <div class="super-element">
                    <div class="card 3 add-super-element" data-name="Super bloc" data-subtype="supercms" data-path="tpls.blockCms.superCms.elements.button">
                      <div class="card_image">
                        <img src="<?php echo Yii::app()->getModule('costum')->assetsUrl?>/images/openCms/super-button.png" />
                      </div>
                      <div class="card_title title-black">
                        <p>Bouton</p>
                      </div>
                    </div>
                  </div>

                  <div class="super-element">
                    <div class="card 3 add-super-element" data-name="Super bloc" data-subtype="supercms" data-path="tpls.blockCms.superCms.container">
                      <div class="card_image">
                        <img src="<?php echo Yii::app()->getModule('costum')->assetsUrl?>/images/openCms/super-column.png" />
                      </div>
                      <div class="card_title title-black">
                        <p>Colonne</p>
                      </div>
                    </div>
                  </div>

                  <div class="super-element">
                    <div class="card 3 add-super-element" data-name="graph bloc" data-subtype="graph" data-path="tpls.blockCms.graph.graph">
                      <div class="card_image">
                        <img src="<?php echo Yii::app()->getModule('costum')->assetsUrl?>/images/openCms/graph.png" />
                      </div>
                      <div class="card_title title-black">
                        <p>Graph</p>
                      </div>
                    </div>
                  </div>

                </div>    
              </div>
            </div>
            <div id="menu2" class="tab-pane fade">    
              <div class="col-md-12 sp-cms-options">      
                <div class="panel-group">
                  <div class="form-group">
                    <label for="">Class bootstrap <span class="text-red" style="font-weight:normal">(Réglage pour le dévéloppeur)<span></label>
                    <textarea class="form-control other-class" rows="5"><?= $otherClass ?></textarea>
                  </div>
                </div>
              </div>
            </div>
            <div id="menu3" class="tab-pane fade">
              <div class="col-md-12 sp-cms-options">        
                <div class="panel-group">
                  <div class="form-group">
                    <label for="">Styles css <span class="text-red" style="font-weight:normal">(Réglage pour le dévéloppeur)<span></label>
                    <textarea class="form-control other-css" rows="5"><?php 
                        if (is_array($otherCss)) {
                         foreach ($otherCss as $csskey => $cssvalue) {
                          echo $csskey.":".$cssvalue.";\r\n";
                        }
                      }else{
                        echo $otherCss;
                      } ?></textarea>
                    <p class="css_err"></p>
                  </div>
                </div>
              </div>
              <div class="text-center showPrt<?= $kunik ?> hidden">
                <b>ID:</b><input value="<?= @$myCmsId ?>" disabled class="dropdown-toggle" data-toggle="dropdown" style="width: 40%;height: 25px;border: solid 1px #e0e0e0; margin-left: 5px;">
                <b> Parent:</b><input id="prt<?= $kunik ?>" class="dropdown-toggle" data-toggle="dropdown" style="width: 40%;height: 25px;border: solid 1px #e0e0e0; margin-left: 5px;" value="<?= @$blockCms['tplParent'] ?>">
                <button onclick="changeParent<?= $kunik ?>()" class="btn btn-sm" style="font-family: sans-serif;">OK</button>
              </div>
            </div>

            <div id="menu4" class="tab-pane fade">
              <div class="col-md-12 sp-cms-options">        
                <div class="panel-group">
                  <div class="form-group">
                    <label for="">Insértion de html <span class="text-red" style="font-weight:normal">(Réglage pour le dévéloppeur)<span></label>
                    <textarea class="form-control other-html" rows="5"><?php echo $otherHtml;?></textarea>
                    <p class="css_err"></p>
                  </div>
                </div>
              </div>
              <div class="sp-cms-std" style="width: 100%;padding: 10px;">
                <label style="width:100%">Insérer page <span class="text-red" style="font-weight:normal">(Réglage pour le dévéloppeur)<span></label>
                    <input class="form-control" type="text" id="addPage" style="width: 90%;height: auto;border-radius: inherit;">                    
                <button class="btn btn-sm saveHtml" style="font-family: sans-serif;width: 9.9%;border-radius: inherit;background-color: #ffffff6b">OK</button>
              </div>
              <div class="text-center showPrt<?= $kunik ?> hidden">
                <b>ID:</b><input value="<?= @$myCmsId ?>" disabled class="dropdown-toggle" data-toggle="dropdown" style="width: 40%;height: 25px;border: solid 1px #e0e0e0; margin-left: 5px;">
                <b> Parent:</b><input id="prt<?= $kunik ?>" class="dropdown-toggle" data-toggle="dropdown" style="width: 40%;height: 25px;border: solid 1px #e0e0e0; margin-left: 5px;" value="<?= @$blockCms['tplParent'] ?>">
                <button onclick="changeParent<?= $kunik ?>()" class="btn btn-sm" style="font-family: sans-serif;">OK</button>
              </div>
            </div>
            <div class="sp-message text-center"></div>
          </div> 
      `
      );

      cmsBuilder.init();
      // remCms();
        $("#toolsBar").click(function(){
         // superCms<?= $kunik ?>.viewMode();
       });

       setTimeout(function() {
        $(".sizes").val(size<?= $kunik ?>); 
        $("#addPage").val("<?= $pageHtml ?>");
        if (typeof other_css<?= $kunik ?> === 'string') {
          $(".other-css").val(other_css<?= $kunik ?>);
        }
        $(".other-html").val(otherHtml<?= $kunik ?>);

        $(".other-class").val(other_class<?= $kunik ?>);

        if (widthClass<?= $kunik ?> == "sp-cms-container") {

          $("#check-container").prop('checked', true);

        }
      }, 100); 

        $(".closeBtn, .closeMenu").click(function(){
          superCms<?= $kunik ?>.editMode();
          $("#toolsBar").hide();
          $("#toolsBar").html("");
        });

      // }         

    /* Upload d'image */
            sectionDyf.<?php echo $kunik ?>Params = {
              "jsonSchema" : {    
                "title" : "Importation d'image",
                "description" : "Personnaliser votre bloc",
                "icon" : "fa-cog",
                
                "properties" : {    
                  "image" :{
                   "inputType" : "uploader",
                   "label" : "image",
                   "docType": "image",
                   "contentKey" : "slider",
                   "itemLimit" : 1,
                   "endPoint": "/subKey/block",
                   "domElement" : "image",
                   "filetypes": ["jpeg", "jpg", "gif", "png"],
                   "label": "Image :",
                   "showUploadBtn": false,
                   initList : <?php echo json_encode($initImage) ?>
                  },
                },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $myCmsId ?>");
                },
                save : function (data) {  
                  tplCtx.value = {};
                  $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                    tplCtx.value[k] = $("#"+k).val();
                    if (k == "parent")
                      tplCtx.value[k] = formData.parent;

                    if(k == "items")
                      tplCtx.value[k] = data.items;
                  });
                  
                  if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                  else {
                      dataHelper.path2Value( tplCtx, function(params) {
                        dyFObj.commonAfterSave(params,function(){
                          toastr.success("Élément bien ajouter");
                          $("#ajax-modal").modal('hide');
                          urlCtrl.loadByHash(location.hash);
                        });
                      } );
                  }

                }
              }
            };

            $(".edit-img<?php echo $kunik ?>myCmsId").off().on("click",function() {  
              tplCtx.subKey = "imgParent";
              tplCtx.id = "<?php echo $myCmsId ?>";
              tplCtx.collection = "cms";
              tplCtx.path = "allToRoot";
              dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, null);
            });
    /* End upload d'image */

    /**********Color settings *************/
      if(  $(".colorpickerInput").length){
        loadColorPicker(initColor);
      }

      loadSpectrumColorPicker(function(){
        if(  $(".sp-color-picker").length){
          $(".sp-color-picker").spectrum({
            type: "text",
            showInput: "true",
            showInitial: "true"
          });
          $('.sp-color-picker').addClass('form-control');
          $('.sp-original-input-container').css("width","100%");
        }
      });

      $( "#fond-color" ).change(function() {
        $(".<?= $kunik ?>" ).css("background-color",$("#fond-color").val());   
        tplCtx = {};
        tplCtx.id = "<?php echo $myCmsId ?>";
        tplCtx.collection = "cms";
        tplCtx.path = "css.background.color"; 
        tplCtx.value = $("#fond-color").val();

        dataHelper.path2Value( tplCtx, function(params) { toastr.success("Modification enrégistré");} );
      });

      $( "#border-color" ).change(function() {
       $(".<?= $kunik ?>" ).css("border-color",$("#border-color").val());
       tplCtx = {};
       tplCtx.id = "<?php echo $myCmsId ?>";
       tplCtx.collection = "cms";
       tplCtx.path = "css.border.color"; 
       tplCtx.value = $("#border-color").val();

       dataHelper.path2Value( tplCtx, function(params) { toastr.success("Modification enrégistré");} );
     });

     /*********End color settings*********/

     /************Border**padding***margin*************/
     var thisetting = {};
     
     $(".border-control, .padding-control" ).on('keyup change', function (){
      $('.elem-value .border-control, .padding-control').each(function(k,v){
        pars   = $(this).val() ? parseInt($(this).val()) : '0';
        thisIs = $(this).data("given");
        $(".<?= $kunik ?>" ).css(thisIs,pars+"%");
        $(this).val(pars);
        thisetting[thisIs.replace('-radius','').replace('border-','').replace('padding-','').replace('margin-','')] = $(this).val();
      })
    });
      $(".padding-control" ).on('keyup change', function (){
      $('.elem-value .padding-control').each(function(k,v){
        pars   = $(this).val() ? parseInt($(this).val()) : '0';
        thisIs = $(this).data("given");
        $(".<?= $kunik ?>" ).css(thisIs,pars+"%");
        $(this).val(pars);
        thisetting[thisIs.replace('-radius','').replace('border-','').replace('padding-','').replace('margin-','')] = $(this).val();
      })
    });
      $(".margin-control" ).on('keyup change', function (){
      $('.elem-value .margin-control').each(function(k,v){
        pars   = $(this).val() ? parseInt($(this).val()) : '0';
        thisIs = $(this).data("given");
        $(".<?= $kunik ?>" ).css(thisIs,pars+"%");
        $(this).val(pars);
        thisetting[thisIs.replace('-radius','').replace('border-','').replace('padding-','').replace('margin-','')] = $(this).val();
      })
    });


     $(".elem-control" ).on('keyup change', function (){
        globalConf = $(this).val() ? parseInt($(this).val()) : '0';
        $("."+$(this).data("what")).val(globalConf);
        $(".<?= $kunik ?>" ).css($(this).data("given"),globalConf+"%");

        $('.elem-value .'+$(this).data("what")).each(function(k,v){
          pars   = $(this).val() ? parseInt($(this).val()) : '0';
          thisIs = $(this).data("given");
          $(".<?= $kunik ?>" ).css(thisIs,pars+"%");
          $(this).val(pars);
          thisetting[thisIs.replace('-radius','').replace('border-','').replace('padding-','').replace('margin-','')] = $(this).val();
        })
      });

     $( ".elem-control, .elem-value .form-control" ).blur(function() {
      tplCtx = {};
      tplCtx.id = "<?php echo $myCmsId ?>";
      tplCtx.collection = "cms";
      tplCtx.path = "css."+$(this).data("path"); 
      tplCtx.value = thisetting;

      dataHelper.path2Value( tplCtx, function(params) {} );
    });

     /************End border**padding***margin***********/

      /***object-fit***/
     $( "#objfit" ).val("<?= $objfit ?>");
     $( "#objfit" ).change(function() {
      var myfit = $( "#objfit" ).val();
      $(".<?= $kunik ?>" ).css("background-size",myfit);
        tplCtx = {};
        tplCtx.id = "<?php echo $myCmsId ?>";
        tplCtx.collection = "cms";
        tplCtx.path = "css.object-fit"; 
        tplCtx.value = myfit;

        dataHelper.path2Value( tplCtx, function(params) {} );
      });
     /***End object-fit***/

     /*Box shadow*/
       var eltBlur   = $('.range-slider-blur').val();
       var eltSpread = $('.range-slider-spread').val();
       var eltAxeX   = $('.range-slider-x').val();
       var eltAxeY   = $('.range-slider-y').val();
       var eltColor  = $("#shw-color").val();
       var inset     = "";


       if (lineSeparator == "true") {
        $( "#check-lineSeparator" ).prop('checked', true);
       }

       $( "#check-lineSeparator" ).change(function() {
        if ($( "#check-lineSeparator" ).is( ":checked" ) == true) {      
          lineSeparator = "true";
          $('.whole-<?= $kunik ?>').append(`<hr class="hr-<?= $kunik ?>" style="width:40%; margin:20px auto; border: 1px dashed #6CC3AC;">`)
        }else{
          lineSeparator = "";
          $(".hr-<?= $kunik ?>").remove();

        }
        tplCtx = {};
        tplCtx.id = "<?php echo $myCmsId ?>";
        tplCtx.collection = "cms";
        tplCtx.path = "css.separator"; 
        tplCtx.value = lineSeparator;

        dataHelper.path2Value( tplCtx, function(params) { toastr.success("Modification enrégistré");} );
      });

       $( "#check-inset" ).change(function() {
        if ($( "#check-inset" ).is( ":checked" ) == true) {      
          inset = "inset";
        }else{
          inset = "";
        }
        $(".<?= $kunik ?>" ).css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
        saveShadow<?= $kunik ?>();
      });

       $( "#shw-color" ).change(function() {
        eltColor = $("#shw-color").val();
        $(".<?= $kunik ?>" ).css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
        saveShadow<?= $kunik ?>();
      });
       $('.range-slider').mousedown(function(){
         $('.range-slider-blur').mousemove(function(){
          eltBlur = $('.range-slider-blur').val();
          $(".range-blur__value").html("Blur "+eltBlur+"px");
        $(".<?= $kunik ?>" ).css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
        });
         $('.range-slider-spread').mousemove(function(){
          eltSpread = $('.range-slider-spread').val();
          $(".range-spread__value").html("Spread "+eltSpread+"px");
        $(".<?= $kunik ?>" ).css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
        });
         $('.range-slider-x').mousemove(function(){
          eltAxeX = $('.range-slider-x').val();
          $(".range-x__value").html("Axe X "+eltAxeX+"px");
        $(".<?= $kunik ?>" ).css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
        });  
         $('.range-slider-y').mousemove(function(){
          eltAxeY = $('.range-slider-y').val();
          $(".range-y__value").html("Axe Y "+eltAxeY+"px");
        $(".<?= $kunik ?>" ).css('box-shadow', inset+' '+eltColor+' '+eltAxeX+'px '+eltAxeY+'px '+eltBlur+'px '+eltSpread+'px');
        });
       });
       $('.range-slider').mouseup(function(){
        saveShadow<?= $kunik ?>();
       });
       function saveShadow<?= $kunik ?>(){
        tplCtx = {};
        tplCtx.id = "<?php echo $myCmsId ?>";
        tplCtx.collection = "cms";
        tplCtx.path = "css.box-shadow"; 
        tplCtx.value = {};
        tplCtx.value.inset =  inset;
        tplCtx.value.color =  eltColor;
        tplCtx.value.x     =  eltAxeX;
        tplCtx.value.y     =  eltAxeY;
        tplCtx.value.blur  =  eltBlur;
        tplCtx.value.spread =  eltSpread;

        dataHelper.path2Value( tplCtx, function(params) { toastr.success("Modification enrégistré");} );
      }
     /*End box shadow*/

    /**************Content size**************/    
      $(".content-size").click(function(){
        size<?= $kunik ?> = $(this).data("size");
       /* if (size == "custom") {      
          tplCtx.value = "super-<?= $myCmsId ?>";
          // dataHelper.path2Value( tplCtx, function(params) {} );      
          superCms<?= $kunik ?>.editMode();*/
    
      /*  }else{*/
          // $(".whole-<?= $kunik ?>").resizable();
          // $(".whole-<?= $kunik ?>").resizable("destroy");
          var $el = $('.whole-<?= $kunik ?>');
          $(".whole-<?= $kunik ?>").removeClass("super-<?= $myCmsId ?>");

          $(".whole-<?= $kunik ?>").removeClass("sp-cms-container");
          var classList = $el.attr('class').split(' ');
          $.each(classList, function(id, item) {
            if (item.indexOf('sp-cms-') == 0) $el.removeClass(item);
          });
          if (size<?= $kunik ?> == "container") {
            if ($("#check-container").is(':checked')) {
             $("#check-container").prop('checked', false);
             size<?= $kunik ?> = "100%";
             $el.addClass('sp-cms-std');
             widthClass<?= $kunik ?> = 'sp-cms-std';
             $(".whole-<?= $kunik ?>").css("width", size<?= $kunik ?>);
             $(".sizes").val(size<?= $kunik ?>); 

             tplCtx = {};
             tplCtx.id = "<?php echo $myCmsId ?>";
             tplCtx.collection = "cms";
             tplCtx.path = "class.width"; 
             tplCtx.value = widthClass<?= $kunik ?>;
             dataHelper.path2Value( tplCtx, function(params) {
              tplCss = {};
              tplCss.id = "<?php echo $myCmsId ?>";
              tplCss.collection = "cms";
              tplCss.path = "css.size.width"; 
              tplCss.value = size<?= $kunik ?>;
              dataHelper.path2Value( tplCss, function(params) { toastr.success("Modification enrégistré");} );
             } );

             

           }else{
             $("#check-container").prop('checked', true);
             size<?= $kunik ?> = "100%";
             $(".sizes").val(size<?= $kunik ?>); 
             widthClass<?= $kunik ?> = 'sp-cms-container';
             $el.addClass('sp-cms-container');

             tplCtx = {};
             tplCtx.id = "<?php echo $myCmsId ?>";
             tplCtx.collection = "cms";
             tplCtx.path = "class.width"; 
             tplCtx.value = widthClass<?= $kunik ?>;
             dataHelper.path2Value( tplCtx, function(params) {
              tplCss = {};
              tplCss.id = "<?php echo $myCmsId ?>";
              tplCss.collection = "cms";
              tplCss.path = "css.size.width"; 
              tplCss.value = size<?= $kunik ?>;
              dataHelper.path2Value( tplCss, function(params) { toastr.success("Modification enrégistré");} );
            } );

           }
         }else{             
           $el.addClass('sp-cms-std');
           widthClass<?= $kunik ?> = 'sp-cms-std';
           size<?= $kunik ?> = size<?= $kunik ?>+"%";
           $(".whole-<?= $kunik ?>").css("width", size<?= $kunik ?>);
           $(".sizes").val(size<?= $kunik ?>); 

           tplCtx = {};
           tplCtx.id = "<?php echo $myCmsId ?>";
           tplCtx.collection = "cms";
           tplCtx.path = "class.width"; 
           tplCtx.value = widthClass<?= $kunik ?>;
           dataHelper.path2Value( tplCtx, function(params) {
            tplCss = {};
            tplCss.id = "<?php echo $myCmsId ?>";
            tplCss.collection = "cms";
            tplCss.path = "css.size.width"; 
            tplCss.value = size<?= $kunik ?>;
            // dataHelper.path2Value( tplCss, function(params) { toastr.success("Modification enrégistré");} );
          } );
         }



       /*    }*/
     });

      $(".sizes").keyup(function(){        
        $(".whole-<?= $kunik ?>").css("width", $(".sizes").val());
        size<?= $kunik ?> = $(".sizes").val();
      }); 

      $(".sizes").blur(function(){ 
        setTimeout(function() {          
          let regex = /[+-]?\d+(\.\d+)?/g;
          tplCtx = {};
          tplCtx.id = "<?php echo $myCmsId ?>";
          tplCtx.collection = "cms";
          tplCtx.path = "css.size.width"; 
          tplCtx.value = {};

          if( size<?= $kunik ?>.indexOf('%') >= 0){          
            tplCtx.value = size<?= $kunik ?>.match(regex).map(function(v) { return parseFloat(v); })+"%";
          }else if(size<?= $kunik ?> === "container"){
            tplCtx.value = "100%";
          }else{
            tplCtx.value = size<?= $kunik ?>.match(regex).map(function(v) { return parseFloat(v); })+"px";
          }       
          dataHelper.path2Value( tplCtx, function(params) { toastr.success("Modification enrégistré");} );
      }, 600);


      }); 
    /**************End content size**************/

    /*********************other-html*********************/    
    $(".other-html" ).off().on('keyup', function (){
      let valValid = spValid(".other-html", "<p class='text-red bg-white'>Oops! Invalid html 🤯</p>");
      if (valValid !== false) {
        if (valValid !== "") {
         $(".empty-sp-element<?= $kunik ?>").hide();
        }else{
          $(".empty-sp-element<?= $kunik ?>").show();
        }
       $(".html<?= $kunik ?>").show();
       $(".html<?= $kunik ?>").html(valValid);
       otherHtml<?= $kunik ?> = valValid;
     }  
   });

    $(".other-html" ).on('blur', function (){
     let valValid = spValid(".other-html", "<p class='text-red bg-white'>Oops! Invalid html 🤯</p>");
     if (valValid !== false) {
      tplCtx = {};
      tplCtx.id = "<?php echo $myCmsId ?>";
      tplCtx.collection = "cms";
      tplCtx.path = "html"; 
      tplCtx.value = valValid;

      dataHelper.path2Value( tplCtx, function(params) { toastr.success("Modification enrégistré");} );
      otherHtml<?= $kunik ?> = valValid;
    }  
  });

    $(".saveHtml" ).on('click', function (){
      tplCtx = {};
      tplCtx.id = "<?php echo $myCmsId ?>";
      tplCtx.collection = "cms";
      tplCtx.path = "pageHtml"; 
      tplCtx.value = $("#addPage").val();

      dataHelper.path2Value( tplCtx, function(params) { 

        urlCtrl.loadByHash(location.hash);

        ;} );
    });
    /*******************End other-html*******************/

    /****************Other class option****************/

      $(".other-class" ).on('keyup', function (){
      let valValid = spValid(".other-class", "<p class='text-red bg-white'>Oops! 🤯<br>Invalid class!</p>");
      console.log("class",valValid);
        if (valValid !== false) {
         $(".<?= $kunik ?>").removeClassExcept("sp-cms-container <?= $kunik ?> super-cms other-css-<?= $kunik ?> newCss-<?= $kunik ?> sp-cms-10 sp-cms-20 sp-cms-30 sp-cms-40 sp-cms-50 sp-cms-60 sp-cms-60 sp-cms-80 sp-cms-90 sp-cms-100 super-container sp-cms-std");
         $(".<?= $kunik ?>").addClass(valValid);
         other_class<?= $kunik ?> = valValid;
       }    
      });

      $(".other-class" ).on('blur', function (){
      let valValid = spValid(".other-class", "<p class='text-red bg-white'>Oops! 🤯<br>Invalid class!</p>");
       if (valValid !==false) {
        tplCtx = {};
        tplCtx.id = "<?php echo $myCmsId ?>";
        tplCtx.collection = "cms";
        tplCtx.path = "class.other"; 
        tplCtx.value = valValid;

        dataHelper.path2Value( tplCtx, function(params) { toastr.success("Modification enrégistré");} );
      }  
    });
    /*************End other class option****************/

    /****************Other css option***************
     * Create css class then insert it into target element
     * Remove it and reinsert on keydown
     * **********************************************/
    $(".other-css" ).on('keyup', function (){ 
       $(".<?= $kunik ?>").removeClass("newCss-<?= $kunik ?>"); 
       $(".<?= $kunik ?>").removeClass("other-css-<?= $kunik ?>");
       $("#myStyleId<?= $kunik ?>").remove();
      other_css<?= $kunik ?> = $(".other-css" ).val()
        var element  = document.createElement("style");
        element.id = "myStyleId<?= $kunik ?>" ;
        element.innerHTML = ".newCss-<?= $kunik ?> {"+other_css<?= $kunik ?>+"}" ;
        var header = document.getElementsByTagName("HEAD")[0] ;
        header.appendChild(element) ;
        $(".<?= $kunik ?>").addClass("newCss-<?= $kunik ?>"); 
      });

      $(".other-css" ).on('blur', function (){
        tplCtx = {};
        tplCtx.id = "<?php echo $myCmsId ?>";
        tplCtx.collection = "cms";
        tplCtx.path = "css.other"; 
        tplCtx.value = other_css<?= $kunik ?>;

        dataHelper.path2Value( tplCtx, function(params) { toastr.success("Modification enrégistré");} );
      });
    /*************End other css option****************/

     /************Add element************/
     $(".add-type-cms").click(function(){
      localStorage.setItem("parentCmsIdForChild", "<?php echo $myCmsId ?>");
    });

     $(".add-super-element").click(function(){
      $("#toolsBar").hide();
      $("#toolsBar").html(``);
        var tplCtx = {};
        tplCtx.collection= "cms";
        tplCtx.value = {};
        tplCtx.value.path = $(this).data("path");
        tplCtx.value.name = $(this).data("name");
        tplCtx.value.page = page;
        tplCtx.value.subtype = $(this).data("subtype");;
        tplCtx.value.tplParent = "<?php echo $myCmsId ?>";
        tplCtx.value.type = "blockChild";
        tplCtx.value.parent={};
        tplCtx.value.parent[thisContextId] = {
          type : thisContextType, 
          name : thisContextName
        };
        if(typeof tplCtx.value == "undefined")
          toastr.error('value cannot be empty!');
        else {
          dataHelper.path2Value( tplCtx, function(params) {
            dyFObj.commonAfterSave(params,function(){
              toastr.success("Élément bien ajouter");
              $("#ajax-modal").modal('hide');                  
            });
          } );      
          urlCtrl.loadByHash(location.hash);
        }
      });
     $(".stop-propagation").click(function(e) {
      e.stopPropagation();
    });
     /************End add element************/
    }
    }
});

/* End display menu */

$(function () {
  var count = 1;
  // Create click event handler on the document.
  $(document).on("click", function (event) {
    // If the target is not the container or a child of the container, then process
    // the click event for outside of the container.
   //  if ($(event.target).closest(".<?= $kunik ?> , #toolsBar").length === 0) {      
   //    $("#toolsBar").html("");
   //    $("#toolsBar").hide();    
   //    $(".<?= $kunik ?>").removeClass("edit-<?= $kunik ?>");
   //    $(".<?= $kunik ?>").addClass("<?= $kunik ?>");
   //    $(".<?= $kunik ?>").removeClass("selected-mode-<?= $kunik ?>");      
   //    $(".<?= $kunik ?>").resizable();
   //    $(".<?= $kunik ?>").resizable("destroy");
   //    $(".sb").removeClass("sb");
   // }
 });
  $(".<?= $kunik ?>").on("click", function () {
    $(".<?= $myCmsId ?>-tools").show();
  });
});
$('.stop-propagation').click(function(event){
  event.stopPropagation();
});

  sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {    
        "title" : "Configurer le fond de votre section",
        "description" : "Personnalisation de votre fond",
        "icon" : "fa-cog",
        
        "properties" : {       
        },
        beforeBuild : function(){
            uploadObj.set("cms","<?php echo (string)$blockCms["_id"] ?>");
        },
        save : function (data) {  
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
            tplCtx.value[k] = $("#"+k).val();
            if (k == "parent")
              tplCtx.value[k] = formData.parent;
          });
          console.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
            else {
              dataHelper.path2Value( tplCtx, function(params) {
                dyFObj.commonAfterSave(params,function(){
                  toastr.success("Élément bien ajouté");
                  $("#ajax-modal").modal('hide');
                  urlCtrl.loadByHash(location.hash);
                });
              } );
            }

        }
      }
    };
    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });
});    
   
<?php }else{ ?>   
  $(".<?= $kunik ?>").draggable();
  $(".<?= $kunik ?>").draggable("destroy");
<?php  } ?> 
</script>