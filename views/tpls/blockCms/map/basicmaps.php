<?php 
  $keyTpl ="basicmaps";
  
  $paramsData = [ 
    "activeFilter" => true,
    "mapHeight" => 550,
    "elementsType" => ["projects","organizations","events", "citoyens"],
    "filterHeaderColor" =>  "#AFCB21",
    "btnText" =>  "Voir dans l'annuaire",
    "btnBackground" =>  "#AFCB21",
    "btnBorderColor" =>  "#AFCB21",
    "btnBorderRadius" =>  2,
    "btnTextColor" =>  "white",
  ];

  if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
      if (isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
      }
    }
  }

  if($paramsData["elementsType"]==""){
    $paramsData["elementsType"] = ["projects","organizations","events", "citoyens"];
  }

 ?>
<style>
    .block-container-<?= $kunik?> #menuRightmap<?php echo $kunik ?>{
        position: absolute !important;
    }
    .block-container-<?= $kunik?> a.title-4:focus,.block-container-<?= $kunik?> a.title-4:hover{
        font-weight: bold;
        text-decoration:none !important;
        border-bottom: 3px solid <?= @$blockCms["blockCmsColorTitle4"]; ?>;
    }
    .block-container-<?= $kunik?> a.title-4{
        padding-right: 0px;
        padding-left: 0px;
        margin: 0 15px;
    }
    .block-container-<?= $kunik?> .title-1{
        margin-top:-100px;
    }
    .menuRight .menuRight_header, .menuRight .panel_map, .menuRight .btn-panel {
        background-color: <?php echo $paramsData['filterHeaderColor']; ?> !important;
        box-shadow: none !important;
    }

    .leaflet-popup-tip-container {
        bottom: -29px;
    }

    .m-1{
        margin: 0.3em;
    }

    .rounded<?= $kunik ?>{
        border-radius: 40px;
        text-transform: uppercase;
    }

    #thematic<?= $kunik ?>{
        background: #eee;
        padding: 0.3em;
    }

    #thematic<?= $kunik ?> .btn-white{
        color: black;
        background: white;
        font-weight: 900 !important;
    }

    #annuaireButton<?= $kunik ?> {
        position: absolute;
        top: 20px;
        right: 20px;
        padding: 10px 20px;
        z-index: 400;
        background: <?= $paramsData["btnBackground"]?>;
        color: <?= $paramsData["btnTextColor"]?>;
        border-radius : <?= $paramsData["btnBorderRadius"]?>px;
        border-color : <?= $paramsData["btnBorderColor"]?>px;
        border-width: 1px;
        padding: 10px 15px;
        text-align: center;
        font-size: 20px;
    }

</style>
<!--div id="filters<?= $kunik ?>" class="searchObjCSS text-center menuFilters menuFilters-vertical col-xs-12 bg-light text-center"></div>
<br><br><br><br-->
<div id="map<?= $kunik ?>" style="position: relative; height: <?php echo $paramsData["mapHeight"]; ?>px"></div>
<a id="annuaireButton<?= $kunik ?>" href="#search" class="btn lbh" data-filters=""><?php echo $paramsData["btnText"] ?></a>
<div id="thematic<?= $kunik ?>"></div>

<script>
    var contextData = <?php echo json_encode($el); ?>;
    var filteredTheme = "";

    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    
    $(function(){

        var customIcon=(typeof paramsMapCO.mapCustom!="undefined") ? paramsMapCO.mapCustom : {tile : "mapbox"};
        var map<?= $kunik ?> = new CoMap({
            container : "#map<?= $kunik ?>",
            activePopUp : true,
            mapOpt:{
                menuRight : <?= ($paramsData["activeFilter"]!="")?$paramsData["activeFilter"]:"true" ?>,
                btnHide : false,
                doubleClick : true,
                scrollWheelZoom: false,
                zoom : 2,
            },
            mapCustom:customIcon,
            elts : []
        });
        

        mylog.log("basic map map<?= $kunik ?>", map<?= $kunik ?>);

        // map<?= $kunik ?> = $.extend(true, {}, paramsMapCO, map<?= $kunik ?>);

        /*map<?= $kunik ?>.once('focus', function() { 
            map<?= $kunik ?>.scrollWheelZoom.enable();
        });*/
/**
        $("#mapContent").remove();
        
        try{
            createFilters("#filters<?= $kunik ?>", false, "#map<?= $kunik ?>");
            filterSearch = searchObj.init(paramsFilter);
        }catch(e){

        }
*/
        
        function getElements<?= $kunik ?>(criteriaParams = null){
            params = {        
                searchType : <?= json_encode($paramsData["elementsType"]) ?>,
                fields : ["urls","address","geo","geoPosition", "tags", "type"],
                indexStep: "0"
            };

            if(criteriaParams!=null && criteriaParams!=""){
                params["searchTags"] = criteriaParams.split(",");
            }

            console.log("loza", params);
            ajaxPost(
                null,
                baseUrl+"/" + moduleId + "/search/globalautocomplete",
                params,
                function(data){
                    mylog.log("basic map ajax map<?= $kunik ?>", map<?= $kunik ?>);
                    map<?= $kunik ?>.clearMap();
                    map<?= $kunik ?>.addElts(data.results);
                    /*if(criteriaParams == null || criteriaParams.trim() == ""){
                        map<?= $kunik ?>.addElts(data.results);
                    }else{
                        $.each(data.results,function(k,v){
                            if(data.results[k].tags){
                                let filteredElement = data.results[k].tags.filter(function(tag) {
                                    return criteriaParams.indexOf(tag.toLowerCase()) !== -1;
                                });

                                if(filteredElement.length==0){
                                    delete data.results[k];
                                }
                            }else{
                                delete data.results[k];
                            }
                        });

                        
                    }*/
                    //filterSearch.mapOpj.map = map<?= $kunik ?>;
                }
            )
        }
        
        getElements<?= $kunik ?>(); 

        /********* Filter thematics *******************/
        var thematic = [];

        if(costum && costum.lists && costum.lists.theme){
            for (const [keyT, valueT] of Object.entries(costum.lists.theme)) {
                var asTags = [];
                let labelT = "";

                if(typeof valueT == "string"){
                    asTags.push(valueT);
                    labelT = valueT;
                }else{
                    labelT = keyT;
                    for (const [keyChild, valueChild] of Object.entries(valueT)){
                        if(!thematic.includes(valueChild)){
                            thematic.push(valueChild);
                        }
                        asTags.push(valueChild);
                    }
                }

                //$("#thematic<?= $kunik ?>").append('<a type="button" href="#search?tags='+asTags.join(',')+'" class="btn btn-white m-1 rounded<?= $kunik ?> thematic<?= $kunik ?>" data-filters="'+asTags.join(',')+'">'+labelT+'<!--span class="badge">0</span--></a>');
                $("#thematic<?= $kunik ?>").append('<a type="button" class="btn btn-white m-1 rounded<?= $kunik ?> thematic<?= $kunik ?>" data-filters="'+asTags.join(',')+'">'+labelT+'<!--span class="badge">0</span--></a>');
            }   
        }else{
            $("#thematic<?= $kunik ?>").remove();
        }

        $(".thematic<?= $kunik ?>").off().on("click",function(){
            if(filteredTheme.indexOf($(this).data("filters")) !== -1){
                filteredTheme = filteredTheme.replace($(this).data("filters"), "");
                $(this).removeClass("btn-primary");
                $(this).addClass("btn-white");
            }else{
                $(this).removeClass("btn-white");
                $(this).addClass("btn-primary");
                if(filteredTheme==""){
                    filteredTheme = $(this).data("filters");
                }else{
                    filteredTheme+=","+$(this).data("filters");
                }
                $("#annuaireButton<?= $kunik ?>").attr("href", "#search?tags="+filteredTheme);
            }

            if(filteredTheme.charAt(0)==","){
                filteredTheme = filteredTheme.substring(1);
            }

            if(filteredTheme.length==0){
                $("#annuaireButton<?= $kunik ?>").attr("href", "#search");
            }

            getElements<?= $kunik ?>(filteredTheme);
        });

        /*********dynform*******************/
        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {    
            "title" : "Configurer votre section",
            "description" : "Personnaliser votre section",
            "icon" : "fa-cog",
            
            "properties" : {
                "activeFilter" :{
                      "inputType" : "select",
                      "label" : "Filtre",
                      "class" : "form-control",
                      values : sectionDyf.<?php echo $kunik ?>ParamsData.activeFilter,
                      "options" : {
                        "true" : "Map avec filtre",
                        "false" : "Map sans filtre"
                    }
                },
                "mapHeight" :{
                      "inputType" : "text",
                      "rules" : {
                        "number" : true
                      },
                      "label" : "Hauteur du map",
                      "class" : "form-control",
                      values : sectionDyf.<?php echo $kunik ?>ParamsData.mapHeight,
                },
                "elementsType" :{
                    "inputType" : "selectMultiple",
                    "label" : "Elément du map",
                    values : sectionDyf.<?php echo $kunik ?>ParamsData.elementsType,
                    "class" : "multi-select select2 form-control",
                    "options" : {
                        "organizations" : trad.organizations,
                        "citoyens" : tradCategory.citizen,
                        "events" : trad.events,
                        "projects" : trad.projects
                    }
                },
                "btnText" :{
                      "inputType" : "text",
                      "label" : "Text sur bouton annuaire",
                      "class" : "form-control",
                      values : sectionDyf.<?php echo $kunik ?>ParamsData.btnText
                },
                "btnBackground" : {
                    "label" : "Couleur de fond du bouton",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik?>ParamsData.btnBackground
                },
                "btnTextColor" : {
                    "label" : "Couleur du label du bouton",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik?>ParamsData.btnTextColor
                },
                "btnBorderRadius" : {
                    "label" : "Rond du bordure",
                    inputType : "text",
                    "rules" : {
                        "number":true
                    },
                    values :  sectionDyf.<?php echo $kunik?>ParamsData.btnBorderRadius
                },
                "btnBorderColor" : {
                    "label" : "Couleur du bordure",
                    inputType : "colorpicker",
                    values :  sectionDyf.<?php echo $kunik?>ParamsData.btnBorderColor
                },
                "filterHeaderColor" : {
                    "inputType" : "colorpicker",
                    "label" : "Couleur de l'entête du filtre",
                    value :  sectionDyf.<?php echo $kunik ?>ParamsData.filterHeaderColor
                }
            },
            beforeBuild : function(){
              uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "description")
                  tplCtx.value[k] = data.description;
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
              else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("Élément bien ajouter");
                      $("#ajax-modal").modal('hide');
                      dyFObj.closeForm();
                      urlCtrl.loadByHash(location.hash);
                    });
                  } );
              }

            }
          }
        };

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
          tplCtx.id = $(this).data("id");
          tplCtx.collection = $(this).data("collection");
          tplCtx.path = "allToRoot";
          dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
          alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties ,"btn",4,6,null,null,"Propriété du bouton de lien vers l'annuaire","green","");
        });

    });
</script>   