<?php
$keyTpl     = "download_pdf";
$paramsData = [
    "labelButton"       => "Boutton",
    "colorlabelButton"  => "",
    "colorbutton"       => "",
    "colorBorderButton" => "",
    "colorButton"       => "",
     "pdf1"=>""
];
if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if ( isset($blockCms[$e])) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>

<?php 
$blockKey = (string)$blockCms["_id"];
$initImage1 = Document::getListDocumentsWhere(
    array(
      "id"=> $blockKey,
      "type"=>'cms',
      "subKey"=>'pdf1',
    ), "file"
  );
?>
<style>
  .contain<?= $kunik?>{
    padding-top: 10%;
  }
  .col<?= $kunik?> h4{
    text-transform: none;
    font-size: 17px;
  }
  .col<?= $kunik?> p{
    font-size: 16px;
  }
  .col<?= $kunik?> img{
    margin-bottom : 20px;
  }
  .col<?= $kunik?> {
    height: 320px;
    border-radius: 20px;
    box-shadow: 0px 0px 6px silver;
    color : white;
    padding: 36px 20px 25px 20px;
    display: -webkit-box;
    word-wrap: break-word;
    -webkit-box-orient: vertical;
    /* margin: 15px 20px; */
  }
  .float<?= $kunik?>{
    /* float: none; */
    margin-bottom: 20px;
    display: inline-block;
  }
  .col<?= $kunik?> .btn<?= $kunik?> a{
    border-radius: 20px;
    color: white;
    padding: 5px 20px 5px 20px;
    text-decoration : none;
    border: 3px solid white;
  }

  .btn<?= $kunik?> {
    margin-top : 30px;
  }
  .text-<?= $kunik?> {
    height : 110px;
    -webkit-line-clamp: 4; 
    overflow-y: auto;
    overflow-x: hidden;

  }
  .edit<?= $kunik?>.editSectionBtns{
    position: absolute;
    top: 15%;
    z-index: 2;
    left: 50%;
  }
  .contain<?= $kunik?> .co-scroll::-webkit-scrollbar {
    width: 3px;
    background-color: #F5F5F5;
  }

  .col-xs-5ths,
  .col-sm-5ths,
  .col-md-5ths,
  .col-lg-5ths {
      position: relative;
      min-height: 1px;
      padding-right: 15px;
      padding-left: 15px;
  }

  .col-xs-5ths {
      width: 100%;
      float: left;
  }

  @media (min-width: 768px) {
      .col-sm-5ths {
          width: 50%;
          float: left;
      }
  }

  @media (min-width: 992px) {
      .col-md-5ths {
          width: 33.33%;
          float: left;
      }
  }

  @media (min-width: 1200px) {
      .col-lg-5ths {
          width: 20%;
          float: left;
      }
  }
</style>
<div class="text-center edit<?= $kunik?> editSectionBtns">
    <div class="" style="width: 100%; display: inline-table; padding: 10px;">
      <?php if(Authorisation::isInterfaceAdmin()){?>
        <div class="text-center addElement<?= $kunik ?>">
          <button class="btn btn-primary"><?php echo Yii::t('cms', 'Add content')?></button>
        </div>  
      <?php } ?>
    </div>
  </div>   
<div class="col-md-12 col-sm-12 col-xs-12 col-lg-12 contain<?= $kunik?> text-center ">
  <div class="row ">
    <?php if (isset($blockCms["content"])) {
      $content = $blockCms["content"];
      foreach ($content as $key => $value) {
        // background
        ${'initFiles' . $key}= Document::getListDocumentsWhere(
          array(
            "id"=> $blockKey,
            "type"=>'cms',
            "subKey"=> 'pdf'.(string)$key 
          ), "file"
        );
        ${'arrFile' . $key}= [];
        foreach (${'initFiles' . $key} as $k => $v) {
          ${'arrFile' . $key}[] =$v['docPath'];
        }
        //icone
        ${'initFilesIcone' . $key}= Document::getListDocumentsWhere(
          array(
            "id"=> $blockKey,
            "type"=>'cms',
            "subKey"=> (string)$key 
          ), "image"
        );
        ${'arrFileIcone' . $key}= [];
        foreach (${'initFilesIcone' . $key} as $k => $v) {
          ${'arrFileIcone' . $key}[] =$v['imagePath'];
        }
        ?>
        <div class=" float<?= $kunik?> col-lg-5ths col-md-5ths col-sm-5ths col-xs-5ths ">
        <div class="col<?= $kunik?> "> 
          <?php if (count(${'arrFileIcone' . $key})!=0) {?>
            <img src="<?php echo ${'arrFileIcone' . $key}[0] ?>" alt="">
          <?php } ?>

          <?php if(Authorisation::isInterfaceAdmin()){ ?>
              <div 
              class="text-center editSectionBtns" >
              <a  href="javascript:;"
              class="btn  btn-primary editElement<?= $blockCms['_id'] ?>"
                data-key="<?= $key ?>" 
                data-title="<?= $value["title"] ?>" 
                data-logo='<?php echo json_encode(${"initFilesIcone" . $key}) ?>' 
                data-pdf='<?php echo json_encode(${"initFiles" . $key}) ?>' 
                data-description="<?= $value["description"] ?>">
                <i class="fa fa-edit"></i>
              </a>
                  
                <a  href="javascript:;"
                class="btn  bg-red text-center deleteElement<?= $kunik?> "
                data-id ="<?= $blockKey ?>"
                data-path="content.<?= $key ?>"
                data-collection = "cms"
                >
                <i class="fa fa-trash"></i>
              </a>
              </div>					
            <?php } ?>
          <div>
            <div class="text-<?= $kunik?> co-scroll">
              <h4 class="sp-text" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="content.<?= $key?>.title"><?= $value["title"]?></h4>
              <p class="sp-text" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="content.<?= $key?>.description"><?= $value["description"]?></p>
            </div>
            <div class="btn<?= $kunik?>">
              <?php if (count(${'arrFile' . $key})!=0) {?>
                <a href="<?php echo ${'arrFile' . $key}[0] ?>" data-title="<?= $value["title"]?>" class="<?= $kunik?> button_<?=$kunik?>"> Télécharger la pdf</a>
              <?php } ?>
            </div>
          </div>
        </div>
        </div>

      <?php  }
    }?>
  </div>
</div>

<script>
  (function(a){
    a.createModal=function(b){
      defaults={
        title:"",message:"Your Message Goes Here!",closeButton:true,scrollable:false
      };
      var b=a.extend({},defaults,b);var c=(b.scrollable===true)?'style="max-height: 420px;overflow-y: auto;"':"";
        html='<div class="portfolio-modal modal fade" id="myModalDoc" tabindex="-1" role="dialog" aria-hidden="true">';
          
        html+='<div class="modal-content padding-top-15">';
        html+='<div class="close-modal" data-dismiss="modal"><div class="lr"><div class="rl"></div></div></div>';
        html+='<div class="modal-header">';
        if(b.title.length>0){
          html+='<h4 class="modal-title">'+b.title+"</h4>"
        }
        html+="</div>";
        html+='<div class="modal-body" '+c+">";
        html+=b.message;
        html+="</div>";
        html+="</div>";
        html+="</div>";
        a("body").prepend(html);
        a("#myModalDoc").modal().on("hidden.bs.modal",function(){
          a(this).remove()
        })
    }
  })(jQuery);
  sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode($paramsData); ?>;
  jQuery(document).ready(function() {
    sectionDyf.<?php echo $kunik ?>Params = {
      "jsonSchema" : {
        "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
        "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
        "icon" : "fa-cog",
        "properties" : {
        },

        beforeBuild : function(){
          uploadObj.set("cms","<?php echo $blockKey ?>");
        },
        save : function () {
          tplCtx.value = {};
          $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
            tplCtx.value[k] = $("#"+k).val();
            if (k == "parent") {
              tplCtx.value[k] = formData.parent;
            }
          });
          mylog.log("save tplCtx",tplCtx);

          if(typeof tplCtx.value == "undefined")
            toastr.error('value cannot be empty!');
          else {
              dataHelper.path2Value( tplCtx, function(params) {
                dyFObj.commonAfterSave(params,function(){
                	//alert("bien ajouter");
                  toastr.success("Élément bien ajouter");
                  $("#ajax-modal").modal('hide');
                  urlCtrl.loadByHash(location.hash);
                });
              } );
          }

        }
      }
    };
    mylog.log("sectiondyfff",sectionDyf);
    $(".edit<?php echo $kunik ?>Params").off().on("click",function() {
      tplCtx.id = $(this).data("id");
      tplCtx.collection = $(this).data("collection");
      tplCtx.path = "allToRoot";
      dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
    });
    $(".addElement<?= $kunik ?>").click(function() {        
      var keys<?= $blockCms['_id'] ?> = Object.keys(<?php echo json_encode($content); ?>);
			var	lastContentK = 0; 
			if (keys<?= $blockCms['_id'] ?>.length!=0) 
				lastContentK = parseInt((keys<?= $blockCms['_id'] ?>[(keys<?= $blockCms['_id'] ?>.length)-1]), 10);
      var tplCtx = {};
      tplCtx.id = "<?= $blockKey ?>";
      tplCtx.collection = "cms";

      tplCtx.path = "content."+(lastContentK+1);
      var obj = {
          subKey : (lastContentK+1),
          icon :     $(this).data("icon"),
          title :     $(this).data("title"),
          description:    $(this).data("description")
          
      };
      var activeForm = {
        "jsonSchema" : {
          "title" : "<?php echo Yii::t('cms', 'Add a file')?>",
          "type" : "object",
          onLoads : {
            onload : function(data){
              $(".parentfinder").css("display","none");
            }
          },
          "properties" : getProperties(obj),
          beforeBuild : function(){
            uploadObj.set("cms","<?= $blockCms['_id'] ?>");
          },
          save : function (data) {  
            tplCtx.value = {};
            $.each( activeForm.jsonSchema.properties , function(k,val) { 
              tplCtx.value[k] = $("#"+k).val();
            });
            mylog.log("save tplCtx",tplCtx);

            if(typeof tplCtx.value == "undefined")
              toastr.error('value cannot be empty!');
            else {
                dataHelper.path2Value( tplCtx, function(params) {
                  dyFObj.commonAfterSave(params,function(){
                    toastr.success("Élément bien ajouter");
                    $("#ajax-modal").modal('hide');
                   urlCtrl.loadByHash(location.hash);
                  });
                } );
            }
          }
        }
      };
      dyFObj.openForm( activeForm );
    });
    $(".editElement<?= $blockCms['_id'] ?>").click(function() {  
			var contentLength = Object.keys(<?php echo json_encode($content); ?>).length;
			var key = $(this).data("key");
			var tplCtx = {};
			tplCtx.id = "<?= $blockCms['_id'] ?>"
			tplCtx.collection = "cms";
			tplCtx.path = "content."+(key);
			var obj = {
        subKey : key,
				title : 		$(this).data("title"),
				description:    $(this).data("description"),
				logo:      		$(this).data("logo"),
				pdf:      		$(this).data("pdf"),
			};
			var activeForm = {
				"jsonSchema" : {
					"title" : "<?php echo Yii::t('cms', 'File modification')?>"+$(this).data("title"),
					"type" : "object",
					onLoads : {
						onload : function(data){
							$(".parentfinder").css("display","none");
						}
					},
					"properties" : getProperties(obj,key),
					beforeBuild : function(){
						uploadObj.set("cms","<?php echo $blockKey ?>");
					},
          save : function (data) {  
            tplCtx.value = {};
            $.each( activeForm.jsonSchema.properties , function(k,val) { 
              tplCtx.value[k] = $("#"+k).val();
            });

            tplCtx.value.description = tplCtx.value.description.replace(/'/g, '’');
            if(typeof tplCtx.value == "undefined")
              toastr.error('value cannot be empty!');
            else {
              dataHelper.path2Value( tplCtx, function(params) {
                dyFObj.commonAfterSave(params,function(){
                  toastr.success("Élément bien ajouter");
                  $("#ajax-modal").modal('hide');
                  urlCtrl.loadByHash(location.hash);
                });
              } );
            }
          }
				}
				};          
				dyFObj.openForm( activeForm );				 
    });

    $('.<?= $kunik?>').on('click',function(){
        var pdf_link = $(this).attr('href');
        var iframe = '<div class="iframe-container"><iframe src="'+pdf_link+'"></iframe></div>'
        $.createModal({
          title:$(this).data('title'),
          message: iframe,
          closeButton:true,
          scrollable:false
        });
        return false;        
    });
    $(".deleteElement<?= $kunik?>").click(function() { 
			var deleteObj ={};
			deleteObj.id = $(this).data("id");
			deleteObj.path = $(this).data("path");			
			deleteObj.collection = "cms";
			deleteObj.value = null;
			bootbox.confirm("<?php echo Yii::t('cms', 'Are you sure you want to delete this element')?> ?",

				function(result){
					if (!result) {
						return;
					}else {
						dataHelper.path2Value( deleteObj, function(params) {
							mylog.log("deleteObj",params);
							toastr.success("<?php echo Yii::t('cms', 'Deleted element')?>");
							urlCtrl.loadByHash(location.hash);
						});
					}
				}); 
			
		});
    function getProperties(obj={},newContent){
      mylog.log("objjjjj",obj);
      var props = {
        title : {
          label : "<?php echo Yii::t('cms', 'Title')?>",
          inputType : "text",
          value : obj["title"]
        },
        description : {
          inputType : "textarea",
          label : "<?php echo Yii::t('cms', 'Description')?>",
          markdown : true,
          value :  obj["description"]
        },
				logo : {
					inputType : "uploader",	
					label : "<?php echo Yii::t('cms', 'Logo')?>",
          docType: "image",
					contentKey : "slider",
					domElement : obj["subKey"],		
          filetype : ["jpeg", "jpg", "gif", "png"],
          showUploadBtn: false,
          endPoint :"/subKey/"+obj["subKey"],
          initList : obj["logo"]
				},
        pdf : {
          inputType : "uploader",
          label : "<?php echo Yii::t('cms', 'Your pdf file')?>",
          showUploadBtn : false,
          docType : "file",
          itemLimit : 1,
          contentKey : "file",
          domElement : "pdf1",
          placeholder : "PDF",
          endPoint : "/subKey/pdf"+obj["subKey"],
          filetypes : [
              "pdf"
          ],
          initList : obj["pdf"]
        },
      };
      return props;
    } 
  });
</script>