<style>
    .<?= $kunik ?>{
        padding: 60px 0px;
    }
    .<?= $kunik ?> .video-item{
        height: 250px;
        border-radius:5px;
        background-size: cover;
        display: flex;
        align-items: flex-end;
        padding: 15px;
        color: white;
        position: relative
    }

    .<?= $kunik ?> .video-item p {
        font-size: 14px;
        font-weight: bold;
    }

    .<?= $kunik ?> .video-item:hover > .btn-play-video-container{
        visibility: visible;
        opacity: 1;
    }
    .<?= $kunik ?> .video-item:hover > .btn-play-video-container a{
        opacity: 1;
        transform: translateY(0);
    }

    .<?= $kunik ?> .video-item .btn-play-video-container{
        position: absolute;
        top:0;
        left:0;
        width:100%;
        height:100%;
        display:flex;
        align-items: center;
        justify-content: center;
        border-radius:5px;
        background-color: rgba(0,0,0,.6);
        visibility: hidden;
        opacity: 0;
        transition: 0.3s;
        transition-delay: 0.0s;
    }

    .<?= $kunik ?> .video-item .btn-play-video-container a{
        display: block;
        padding: 8px 20px;
        background-color: #ff0000;
        border-radius: 8px;
        opacity: 0;
        transform: translateY(20px);
        transition: 0.3s;
        transition-delay: 0.2s;
    }

    .<?= $kunik ?> .video-item .btn-play-video-container a i{
        font-size: 1.5em;
        color: white;
    }

    .<?= $kunik ?> .youtube-player-container{
        width:100%;
        height: 100vh;
        position: absolute;
        top:0;
        left:0;
        background: rgba(0, 0, 0, .9);
        z-index:9999999;
        display: flex;
        justify-content: center;
        align-items:center;
        opacity:0;
        visibility:hidden;
        transition: .3s;
    }

    .<?= $kunik ?> .youtube-player-content{
        width: 60%;
    }

    .<?= $kunik ?> .youtube-player-header{
        width:100%;
        text-align: right;
    }

    .<?= $kunik ?> .btn-close-youtube-player{
        background-color: transparent;
        color: white;
        border: none;
        border-radius: 100%;
        top: -10px;
        position: relative;
        font-size: 1.2em;
    }

    .<?= $kunik ?> .youtubevideos-header h1{
        margin-bottom: 30px;
        font-size: 2em;
    }
</style>