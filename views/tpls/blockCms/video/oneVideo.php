<?php 
$keyTpl = "oneVideo";
$kunik = $keyTpl.(string)$blockCms["_id"];
$blockKey = (string)$blockCms["_id"];
$paramsData = [
    "linkVideo" =>"https://www.youtube.com/embed/eY_kosDAAsc"   
    
];

if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}

if (strpos($paramsData["linkVideo"], "=") !== false) {
  $videoId = array_reverse(explode("=", $paramsData["linkVideo"]))[0];
} else {
  $videoId = array_reverse(explode("/", $paramsData["linkVideo"]))[0];
}

if (strpos($paramsData["linkVideo"],"vimeo") !== false) {
  $src = "https://player.vimeo.com/video/".$videoId;
}
if (strpos($paramsData["linkVideo"], "youtu") !== false) {
  $src = "https://www.youtube.com/embed/".$videoId;
}
if (strpos($paramsData["linkVideo"],"dailymotion")!== false) {
  $src = "https://www.dailymotion.com/embed/video/".$videoId;
}

?>
<style type="text/css">
    .<?= $kunik?> iframe{
        width : 560px;
         height : 315px
    }
    @media (max-width: 978px) {
        .<?= $kunik?> iframe{
          width: 100%;
      }
  }
</style>
<div class="<?= $kunik?>">  
<iframe  src="<?= $src?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 
</div>

<script type="text/javascript">
   sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik ?>Params = {
            "jsonSchema" : {    
            "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
            "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
                "icon" : "fa-cog",
                "properties" : {  
                "linkVideo" : {
                    label : "<?php echo Yii::t('cms', 'Url of the video')?> ",
                    "inputType" : "urls",
                    values :  sectionDyf.<?php echo $kunik?>ParamsData.linkVideo
                }
            },
            beforeBuild : function(){
                uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
                tplCtx.value = {};
                $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                   
                    tplCtx.value[k] = $("#"+k).val();
                    if(k == "allContent"){
                        $.each(data.allContent,function(inc,va){
                            var keycontente = "content."
                            tplCtx.value[k][inc] = va;
                      })
                  }
                });
                mylog.log("save tplCtx",tplCtx);
                if(typeof tplCtx.value == "undefined")
                    toastr.error('value cannot be empty!');
                else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                      $("#ajax-modal").modal('hide');
                      urlCtrl.loadByHash(location.hash);
                  });
                } );
              }
          }
      }
  };
  

  $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
        tplCtx.id = $(this).data("id");
        tplCtx.collection = $(this).data("collection");
        tplCtx.path = "allToRoot";
        dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params, null , sectionDyf.<?php echo $kunik ?>ParamsData);
    });
 
})
</script>