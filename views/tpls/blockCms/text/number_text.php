<?php
$keyTpl = "number_text";
$paramsData = [
    "title" => "Titre block",
    "content"=>" "

];
if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>

<style type="text/css">
    .box<?= $kunik?>{
        padding:60px 0px;
    }
    .box<?= $kunik?> .box-title h1 {
        color: #49CBE3;
    }

    .box<?= $kunik?> .box-part{
        background:#202A5F;
        border-radius:15px;
        padding:20px 10px;
        margin:30px 0px;
        box-shadow: 0px 0px 6px silver;
    }
    .box<?= $kunik?> .box-part span{
        font-size: 18px;
        color:#ffffff;
    }
    .box<?= $kunik?> .text{
        margin:20px 0px;
    }



</style>
<div class="box<?= $kunik?> col-lg-10 col-lg-offset-1 col-md-12 col-sm-12 col-xs-12">
    <h2 class="title<?php echo $kunik ?> Oswald sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title">
        <?php echo $paramsData["title"] ?>
    </h2>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="box-part text-center">
            <div class="box-title">
                <h1 class="single">589</h1>
            </div>
            <div class="text">
                <span>Startups accueillies</span>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="box-part text-center">
            <div class="box-title">
                <h1 class="single">740</h1>
            </div>
            <div class="text">
                <span>Programmes d’incubations</span>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
        <div class="box-part text-center">
            <div class="box-title">
                <h1 class="single">432</h1>
            </div>
            <div class="text">
                <span>Programmes d’accélération</span>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
    sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik?>Params = {
            "jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
                "icon" : "fa-cog",
                "properties" : {

                },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function () {
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                        tplCtx.value[k] = $("#"+k).val();

                    });
                    mylog.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.commonAfterSave(params,function(){
                                toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                                $("#ajax-modal").modal('hide');
                                urlCtrl.loadByHash(location.hash);
                            });
                        } );
                    }

                }
            }
        };
        $(".edit<?php echo $kunik?>Params").off().on("click",function() {
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
        });
    });
    var speed = 20;
    function incEltNbr(elt) {
        endNbr = Number(elt.innerHTML);
        incNbrRec(0, endNbr, elt);
    }

    /*A recursive function to increase the number.*/
    function incNbrRec(i, endNbr, elt) {
        if (i <= endNbr) {
            elt.innerHTML = i;
            setTimeout(function() {//Delay a bit before calling the function again.
                incNbrRec(i + 1, endNbr, elt);
            }, speed);
        }
    }

    /*window.addEventListener('scroll', function() {
        if ( $(document).scrollTop() > 1200 ) {*/
            $.each($(".single"), function(k,v){
                incEltNbr(v);
            });
    /*    }
    });*/




</script>