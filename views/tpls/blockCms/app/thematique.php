<?php
$keyTpl = "thematique";
$paramsData = [
    "title" => "Titre block",
    "content"=>" "

];
if (isset($blockCms)) {
    foreach ($paramsData as $e => $v) {
        if (  isset($blockCms[$e]) ) {
            $paramsData[$e] = $blockCms[$e];
        }
    }
}
?>

<style type="text/css">
    .card<?= $kunik?>{
        padding:30px 0px;
    }
    .card<?= $kunik?> .box-title h1 {
        color: #49CBE3;
    }

    .card<?= $kunik?> .card-container {
        -webkit-perspective: 800px;
        -moz-perspective: 800px;
        -o-perspective: 800px;
        perspective: 800px;
        margin-bottom: 30px;
    }
    /* flip the pane when hovered */
    .card<?= $kunik?> .card-container:not(.manual-flip):hover .card,
    .card<?= $kunik?> .card-container.hover.manual-flip .card{
        -webkit-transform: rotateY( 180deg );
        -moz-transform: rotateY( 180deg );
        -o-transform: rotateY( 180deg );
        transform: rotateY( 180deg );
    }

    /* flip speed goes here */
    .card<?= $kunik?> .card {
        -webkit-transition: -webkit-transform .5s;
        -moz-transition: -moz-transform .5s;
        -o-transition: -o-transform .5s;
        transition: transform .5s;
        -webkit-transform-style: preserve-3d;
        -moz-transform-style: preserve-3d;
        -o-transform-style: preserve-3d;
        transform-style: preserve-3d;
        position: relative;
    }

    /* hide back of pane during swap */
    .card<?= $kunik?> .front, .card<?= $kunik?> .back {
        -webkit-backface-visibility: hidden;
        -moz-backface-visibility: hidden;
        -o-backface-visibility: hidden;
        backface-visibility: hidden;
        position: absolute;
        top: 0;
        left: 0;
        background-color: #1A2660;
        box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.14);
    }

    /* front pane, placed above back */
    .card<?= $kunik?> .front {
        z-index: 2;
        touch-action: manipulation;
        cursor: pointer;
    }

    /* back, initially hidden pane */
    .card<?= $kunik?> .back {
        -webkit-transform: rotateY( 180deg );
        -moz-transform: rotateY( 180deg );
        -o-transform: rotateY( 180deg );
        transform: rotateY( 180deg );
        z-index: 3;
    }

    .card<?= $kunik?> .back .btn-simple{
        /*position: absolute;*/
        left: 0;
        bottom: 4px;
    }
    /*        Style       */


    .card<?= $kunik?> .card{
        background: none repeat scroll 0 0 #FFFFFF;
        border-radius: 4px;
        color: #444444;
    }
    .card<?= $kunik?> .card-container, .card<?= $kunik?> .front, .card<?= $kunik?> .back {
        width: 100%;
        height: 350px;
        border-radius: 15px;
        -webkit-box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.16);
        -moz-box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.16);
        /*box-shadow: 0px 0px 19px 0px rgba(0,0,0,0.16);*/
        box-shadow: 0px 0px 6px silver;
    }
    .card<?= $kunik?> .card .cover{
        height: 280px;
        overflow: hidden;
        border-radius: 15px 15px 0px 0px;
    }
    .card<?= $kunik?> .card .cover img{
        width: 100%;
        height: 280px;
        object-fit: cover;
        object-position: center;
        /*touch-action: manipulation;
        cursor: pointer;*/
    }

    .card<?= $kunik?> .card .back .content .main {
        height: 210px;
        color: #fff;
        overflow-y: auto;
        overflow-x: hidden;
    }
    .card .back .content .main p{
        font-size: 14px;
    }
    .card .back .content .main h4{
        text-transform: none;
    }

    .card<?= $kunik?> .card h5{
        margin: 5px 0;
        font-weight: 400;
        line-height: 20px;
    }

    .card<?= $kunik?> .card .footer {
        border-top: 1px solid #EEEEEE;
        color: #999999;
        margin: 15px 0 0;
        padding: 10px 0 0;
        text-align: center;
    }

    .card<?= $kunik?> .front .footer {
        margin: 0px 0 0;
    }
    .card<?= $kunik?> .front .footer h4 {
        text-transform: none;
        color: #49CBE3;
    }
    .card<?= $kunik?> .card .footer .btn-simple{
        margin-top: -6px;
        color: #fff;
    }
    .card<?= $kunik?> .card .header {
        padding: 15px 20px;
        height: 75px;
        position: relative;
    }
    .card<?= $kunik?> .card .motto{
        border-bottom: 1px solid #EEEEEE;
        color: #49CBE3;
        font-size: 14px;
        font-weight: 400;
        padding-bottom: 10px;
        text-align: center;
        text-transform: none;
    }

    /*      Just for presentation        */



    .card<?= $kunik?> .btn-simple{
        opacity: .8;
        color: #666666;
        background-color: transparent;
    }

    .card<?= $kunik?> .btn-simple:hover,
    .card<?= $kunik?> .btn-simple:focus{
        background-color: transparent;
        box-shadow: none;
        opacity: 1;
    }
    .card<?= $kunik?> .btn-simple i{
        font-size: 16px;
    }

    .card<?= $kunik?> .co-scroll::-webkit-scrollbar {
        width: 3px;
        background-color: #F5F5F5;
        color: #49CBE3;
    }

    .card<?= $kunik?> ul {
        padding: 0.5em 1em 0.5em 2.3em;
        position: relative;
    }

    .card<?= $kunik?> ul li {
        line-height: 1.5;
        padding: 5px 0;
        list-style-type: none!important;
        margin-left: 5px;
        font-size: 16px;
    }

    .card<?= $kunik?> ul li:before {
        font-family: FontAwesome;
        content: "\f138";
        position: absolute;
        left : 1em;
        color: #49CBE3;
    }



    /*       Fix bug for IE      */

    @media screen and (-ms-high-contrast: active), (-ms-high-contrast: none) {
        .card<?= $kunik?> .front, .card<?= $kunik?> .back{
            -ms-backface-visibility: visible;
            backface-visibility: visible;
        }

        .card<?= $kunik?> .back {
            visibility: hidden;
            -ms-transition: all 0.2s cubic-bezier(.92,.01,.83,.67);
        }
        .card<?= $kunik?> .front{
            z-index: 4;
        }
        .card<?= $kunik?> .card-container:not(.manual-flip):hover .back,
        .card<?= $kunik?> .card-container.manual-flip.hover .back{
            z-index: 5;
            visibility: visible;
        }
    }



</style>
<?php
    $collapse = [
        [
            "title" => "Je suis porteur de projet",
            "image" => "https://technopole-reunion.com/wp-content/uploads/2020/04/annie-spratt-hCb3lIB8L8E-unsplash-scaled.jpg",
             "items" => [   "J'aimerai déposer un projet",
                            "Découvrir des entraide, formes d'accompagnement, formations et financements.",
                            "Me nourrir d'autres projets d'innovation",
                            "Beneficier des tiers lieux et de leurs compétences autour de moi",
                            "Vous êtes une personne morale disposant d'une ETI ?",
                            "Vous êtes une personne morale disposant d'une GE ?"
                        ]

        ],

        [
            "title" => "Je suis un citoyen je m'informe",
            "image" => "https://technopole-reunion.com/wp-content/uploads/2020/04/annie-spratt-hCb3lIB8L8E-unsplash-scaled.jpg",
            "items" => [   "Visualiser et naviguer dans l'innovation 974",
                "Navigation par centre d'interêt et thématiqye",
                "Quelque parcours type",
                "Les aides",
                "Les accompagnements",
                "Les financements",
                "La communauté, l'écosystème",
                "Communication",
                "Recherche associé"
            ]

        ],

        [
            "title" => "Je suis une institution (collectivité, Ademe, caisse de dépôt)",
            "image" => "https://technopole-reunion.com/wp-content/uploads/2020/04/annie-spratt-hCb3lIB8L8E-unsplash-scaled.jpg",
            "items" => [   "Voir les projets innovants soutenus et leus parcours sur un Territoire",
                "Voir l'activité de l'innovation",
                "Voir les acteurs de l'innovation",
                "Voir les dispositif pour l'innovation",
                "Feedback et Rex des projets accompagnés"
            ]

        ],

        [
            "title" => "Organisme de financement",
            "image" => "https://technopole-reunion.com/wp-content/uploads/2020/04/annie-spratt-hCb3lIB8L8E-unsplash-scaled.jpg",
            "items" => [   "Voir les projets innovants soutenus et leur parcours",
                "Voir les projets que je soutiens",
                "Voir les financements par thématiques",
                "Je met à jour les dispositifs de financeent.",
                "Feedback des projets accompagnés"
            ]

        ],

        [
            "title" => "Acteurs travaillant avec et pour des projets innovants",
            "image" => "https://technopole-reunion.com/wp-content/uploads/2020/04/annie-spratt-hCb3lIB8L8E-unsplash-scaled.jpg",
            "items" => [   "Voir les projets innovants accompagnés",
                "Les services et accompagnemets",
                "Feedback des projets accompagnés"
            ]

        ],

        [
            "title" => "Partenaire de l'innovation",
            "image" => "https://technopole-reunion.com/wp-content/uploads/2020/04/annie-spratt-hCb3lIB8L8E-unsplash-scaled.jpg",
            "items" => ["Voir les projets innovants accompagnés"]

        ],

    ]
?>
<div class="card<?= $kunik?> col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <h2 class="title<?php echo $kunik ?> Oswald sp-text img-text-bloc" id="sp-<?= $blockKey ?>" data-id="<?= $blockKey ?>" data-field="title">
        <?php echo $paramsData["title"] ?>
    </h2>
    <?php foreach ($collapse as $key => $value) { ?>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
            <div class="card-container manual-flip">
                <div class="card">
                    <div class="front" onclick="rotateCard(this)">
                        <div class="cover">
                            <img  src="https://technopole-reunion.com/wp-content/uploads/2020/04/annie-spratt-hCb3lIB8L8E-unsplash-scaled.jpg"/>
                        </div>
                        <div class="footer">
                            <h4 class="text-center"><?= @$value["title"] ?></h4>
                        </div>
                    </div> <!-- end front panel -->
                    <div class="back">
                        <div class="header">
                            <h5 class="motto"><?= @$value["title"] ?></h5>
                        </div>
                        <div class="content">
                            <div class="main co-scroll">
                                <ul>
                                    <?php foreach ($value["items"] as $ki => $vi) { ?>
                                    <li><?php echo $vi ?></li>
                                    <?php } ?>
                                </ul>
                            </div>
                        </div>
                        <div class="footer">
                            <button class="btn btn-simple" rel="tooltip" title="Flip Card" onclick="rotateCard(this)">
                                <i class="fa fa-reply"></i> Retour
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>



</div>
<script type="text/javascript">
    $().ready(function(){
        $('[rel="tooltip"]').tooltip();

    });

    function rotateCard(btn){
        var $card = $(btn).closest('.card-container');
        console.log($card);
        if($card.hasClass('hover')){
            $card.removeClass('hover');
        } else {
            $card.addClass('hover');
        }
    }
    sectionDyf.<?php echo $kunik?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    jQuery(document).ready(function() {
        sectionDyf.<?php echo $kunik?>Params = {
            "jsonSchema" : {
                "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
                "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
                "icon" : "fa-cog",
                "properties" : {

                },
                beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
                },
                save : function () {
                    tplCtx.value = {};
                    $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) {
                        tplCtx.value[k] = $("#"+k).val();

                    });
                    mylog.log("save tplCtx",tplCtx);

                    if(typeof tplCtx.value == "undefined")
                        toastr.error('value cannot be empty!');
                    else {
                        dataHelper.path2Value( tplCtx, function(params) {
                            dyFObj.commonAfterSave(params,function(){
                                toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                                $("#ajax-modal").modal('hide');
                                urlCtrl.loadByHash(location.hash);
                            });
                        } );
                    }

                }
            }
        };
        $(".edit<?php echo $kunik?>Params").off().on("click",function() {
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            dyFObj.openForm( sectionDyf.<?php echo $kunik?>Params,null, sectionDyf.<?php echo $kunik?>ParamsData);
        });
    });
</script>