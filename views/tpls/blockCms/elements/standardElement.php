<?php 
$paramsData = [ 
  "title" => "AGENDA",
  "titleBg" => "transparent",
  "title2" => "1",
  "elType" => "events",
  "elPoiType" => "",
  "elDesign" => "1",
  "imgShow" => true,
  "imgDefault" => "",
  "imgWidth" => "352px",
  "imgHeight"  => "352px",
  "imgRound" => "0%",
  "imgBg" => "#f0ad16",
  "infoBg" => "#f0ad16",
  "imgBorder" => "",
  "imgBorderColor" => "",
  "infoDistanceFromImage" => "-14px",
  "infoMarginLeft" => "0px",
  "infoMarginRight" => "0px",
  "infoDateFormat" => "1",
  "infoOrder" => true,
  "infoAlign" => "center",
  "infoCenter" => true,
  "infoBorderRadius" => "0%",
  "infoHeight" => "auto",
  "infoOnHover" => true,
  "cardBg" => "transparent",
  "cardBoxShadow" => false,
  "cardAutoPlay" => true,
  "cardNumberPhone" => 1,
  "cardNumberTablet" => 2,
  "cardNumberDesktop" => 3,
  "cardSpaceBetween" => 10,
  "cardNbResult" => 6,
  "cardBtnShowMore" => true,
  "showName" => true,
  "showType" => true,
  "showShortDescription" => true,
  "showDescription" => true,
  "showDate" => true,
  "showAdress" => true,
  "showTags" => true,
  "showRoles" => true,
  "formTitle" => "",
  "formName" => true,
  "buttonShowAll" => false,
  "buttonShowAllText" => "Afficher tout",
];

if (isset($blockCms)) {
  foreach ($paramsData as $e => $v) {
      if (  isset($blockCms[$e]) ) 
            $paramsData[$e] = $blockCms[$e];
    }
}

$initImage = Document::getListDocumentsWhere(
  array(
    "id"=> $blockKey,
    "type"=>'cms',
    "subKey"=>'image',
  ), "image"
);

foreach ($initImage as $key => $value) {
  $paramsData["imgDefault"] = $value["imagePath"];
}

$assetsUrl = Yii::app()->getModule('costum')->assetsUrl;
HtmlHelper::registerCssAndScriptsFiles(["/css/blockcms/swiper/swiper-bundle.min.css","/js/blockcms/swiper/swiper-bundle.min.js"], $assetsUrl);
$addType = $paramsData["elType"];
if($paramsData["elType"] == "events" || $paramsData["elType"] == "organizations" || $paramsData["elType"] == "projects" /*substr($addType, -1) == "s"*/)
    $addType = substr($addType, 0, -1);
?>
<style>
  .block-container-<?= $kunik ?> .btn-container button{display: none;}
  .block-container-<?= $kunik ?> .btn-container{
    min-height: 35px;
    margin-bottom: 10px;
  }
  .block-container-<?= $kunik ?>:hover .btn-container button{
    display: block;
    position: relative;
    top: 10px;
    left: 50%;
    transform: translateX(-50%);
    z-index: 9;
  }

  .title-bg-<?= $kunik ?> {
    background-color: <?= $paramsData["titleBg"]?>;
  }

</style>
<div class="stdr-elem">
  <div class="title-bg-<?= $kunik ?> padding-top-10 padding-bottom-10 padding-left-10">
    <h3 class="title-1"><?= $paramsData["title"]?></h3>
  </div>
  <h4 class="title-2"><?= $paramsData["title2"]?></h4>
  <?php if(Authorisation::isInterfaceAdmin()){ ?>
    <div class="text-center btn-container">
      <button class="btn btn-primary btn-sm add hiddenPreview" onclick="dyFObj.openForm('<?= $addType ?>',null,null,null,commonElObj)"></button>
    </div>
  <?php }  ?>
</div>
<?php if(isset($paramsData["buttonShowAll"]) && $paramsData["buttonShowAll"]==true){ ?>
<div class="text-center">
  <a href="#search?types=<?php echo $paramsData['elType'] ?>" class="btn btn-success lbh">
    <?php echo $paramsData["buttonShowAllText"] ?> ( <?php echo $paramsData["elType"] ?> )
  </a>
</div>
<?php } ?>

<script>

  <?php if(!empty($paramsData["imgDefault"])){ ?>
    if(carouselObj && carouselObj.costumDefaultImage){
      carouselObj.costumDefaultImage["<?= $kunik ?>"] = "<?= $paramsData["imgDefault"] ?>";
    }
  <?php } ?>
                  
  <?php if($blockCms["type"] == "blockChild") { ?>
    $( ".stdr-elem" ).wrap( `<div class="block-container-<?= $kunik ?> col-lg-12 col-md-12 col-sm-12 col-xs-12"> </div>` );
  <?php } ?>
    sectionDyf.<?php echo $kunik ?>ParamsData = <?php echo json_encode( $paramsData ); ?>;
    var addType = "<?= $addType ?>";
    var contextData = <?php echo json_encode($el); ?>;

    var commonElObj = {
        "beforeBuild":{
            "properties" : {
                "shortDescription" : {
                    "inputType" : "textarea",
                    "label" : "<?php echo Yii::t('common', 'shortDescription')?>",
                    "rules" : {
                        "maxlength" : 200
                    }
                },
            }
        },
        "onload" : {
            "actions" : {
                "src" : { 
                  "infocustom" : "Remplir le champ"
                },
                /*"hide" : {
                      "breadcrumbcustom" : 1,
                      "tagstags" :1,
                      "descriptiontextarea":1,
                      "locationlocation":1,
                      "formLocalityformLocality":1,
                      "parentfinder" : 1,
                      "removePropLineBtn" : 1
                }*/
            }
        }
    };    
    commonElObj.afterSave = function(data){
        dyFObj.commonAfterSave(data, function(){
          urlCtrl.loadByHash(location.hash);
        });
    }

    if(sectionDyf.<?php echo $kunik ?>ParamsData["elPoiType"] != "")
        commonElObj.onload.actions.presetValue = {"type" : "<?= $paramsData["elPoiType"] ?>"}
    if(sectionDyf.<?php echo $kunik ?>ParamsData["formTitle"] != "")
        commonElObj.onload.actions.setTitle =  <?= json_encode($paramsData["formTitle"]) ?>


  $(function(){
   $(".block-container-<?= $kunik ?> button.add").text(exists(trad.add<?= $paramsData["elType"] ?>) ? trad.add<?= $paramsData["elType"] ?> : trad.add<?= $addType ?> );

        sectionDyf.<?php echo $kunik ?>Params = {
          "jsonSchema" : {
              "title" : "<?php echo Yii::t('cms', 'Set up your section')?>",
              "description" : "<?php echo Yii::t('cms', 'Customize your section')?>",
            "icon" : "fa-cog",
            "properties" : {

            },
            beforeBuild : function(){
                    uploadObj.set("cms","<?php echo $blockKey ?>");
            },
            save : function (data) {  
              tplCtx.value = {};
              $.each( sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties , function(k,val) { 
                tplCtx.value[k] = $("#"+k).val();
                if (k == "parent")
                  tplCtx.value[k] = formData.parent;

                if(k == "description")
                  tplCtx.value[k] = data.description;
              });
              console.log("save tplCtx",tplCtx);

              if(typeof tplCtx.value == "undefined")
                toastr.error('value cannot be empty!');
                else {
                  dataHelper.path2Value( tplCtx, function(params) {
                    dyFObj.commonAfterSave(params,function(){
                      toastr.success("<?php echo Yii::t('cms', 'Element well added')?>");
                      $("#ajax-modal").modal('hide');
                      //urlCtrl.loadByHash(location.hash);
                    });
                  } );
                }

            }
          }
        };

        sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties = Object.assign(
            sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,
            carouselObj.commonProperties(sectionDyf.<?php echo $kunik ?>ParamsData)
        )

        $(".edit<?php echo $kunik ?>Params").off().on("click",function() {  
            tplCtx.id = $(this).data("id");
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            tplCtx.format = true;
            dyFObj.openForm( sectionDyf.<?php echo $kunik ?>Params,null, sectionDyf.<?php echo $kunik ?>ParamsData);
            alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"buttonShowAll",6,6,null,null,"Bouton pour afficher tout","#e45858","");
            alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"title",6,6,null,null,"Titre","#e45858","");
            alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"el",6,6,null,null,"Options","#e45858","");
            alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"img",4,6,null,null,"Image","#e45858","");
            alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"info",2,6,null,null,"Information","#e45858","");
            alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"card",4,6,null,null,"Carte","#e45858","");
            alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"cardNumber",4,6,null,null,"Nombre","#000","");
            alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"form",4,6,null,null,"Formulaire","#000","");
            alignInput2(sectionDyf.<?php echo $kunik ?>Params.jsonSchema.properties,"show",2,6,null,null,"Afficher les <i class='fa fa-chevron-down'></i>","#e45858","");
            


            $('#elDesign').change(function(){
              var elDesign = $(this).val();
              tplCtx.value = {};
              tplCtx.value["elDesign"] = elDesign;
              dataHelper.path2Value(tplCtx, function(params) {
                toastr.success("Chargement . . .");
                  //urlCtrl.loadByHash(location.hash);
                  location.reload();
              });           
            });

            if($('#elType').val()=="poi")
                $('.elPoiTypeselect').fadeIn();
              else
                $('.elPoiTypeselect').hide();                


            $('#elType').change(function(){
              if($(this).val()=="poi")
                $('.elPoiTypeselect').fadeIn();
              else
                $('.elPoiTypeselect').fadeOut();
            })
        });

    <?php if($paramsData["elDesign"]==1 || $paramsData["elDesign"]==3 || $paramsData["elDesign"]==4 || $paramsData["elDesign"]== 5 ){ ?> 
      carouselObj.multiCart.init(sectionDyf.<?php echo $kunik ?>ParamsData,"<?= $kunik ?>");
    <?php } ?>
    <?php if($paramsData["elDesign"]==2 || $paramsData["elDesign"]==6){ ?> 
     carouselObj.cardBlock.init(sectionDyf.<?php echo $kunik ?>ParamsData,"<?= $kunik ?>");
    <?php } ?>    
  })
</script>