
paramsMapCO = $.extend(true, {}, paramsMapCO, {
	mapCustom:{
		icon: {
			getIcon:function(params){
				var elt = params.elt;
				mylog.log("icone ftl", elt.tags);
				var myCustomColour = '#4ab5a1';
			
				var markerHtmlStyles = `
					background-color: ${myCustomColour};
					width: 3.5rem;
					height: 3.5rem;
					display: block;
					left: -1.5rem;
					top: -1.5rem;
					position: relative;
					border-radius: 3rem 3rem 0;
					transform: rotate(45deg);
					border: 1px solid #FFFFFF`;
			
				var myIcon = L.divIcon({
					className: "my-custom-pin",
					iconAnchor: [0, 24],
					labelAnchor: [-6, 0],
					popupAnchor: [0, -36],
					html: `<span style="${markerHtmlStyles}" />`
				});
				return myIcon;
			}
		},
		getClusterIcon:function(cluster){
			var childCount = cluster.getChildCount();
			// var c = ' marker-cluster-';
			// if (childCount < 100) {
			// 	c += 'small-ftl';
			// } else if (childCount < 1000) {
			// 	c += 'medium-ftl';
			// } else {
			// 	c += 'large-ftl';
			// }
			return L.divIcon({ html: '<div style="color: white;font-size: 14px;font-weight: 700;text-align: center;border-radius: 50%!important;margin-left: -27px !important;margin-top: -27px !important;padding: 10px !important;width: 40px !important;height: 40px !important;background-image: none;background-color: #4ab5a1 !important;">' + childCount + '</div>', className: '', iconSize: new L.Point(40, 40) });
		},
		getThumbProfil: function (data) {
                var imgProfilPath = modules.map.assets + "/images/thumb/default.png";
                if (typeof data.profilThumbImageUrl !== "undefined" && data.profilThumbImageUrl != "")
                    imgProfilPath = baseUrl + data.profilThumbImageUrl;
                else
                    imgProfilPath = modules.map.assets + "/images/thumb/default_" + data.collection + ".png";
    
                return imgProfilPath;
            },
		getPopup: function(data){
                var id = data._id ? data._id.$id:data.id;
                var imgProfil = paramsMapCO.mapCustom.getThumbProfil(data)

                var eltName = data.title ? data.title:data.name;
                var popup = "";
                popup += "<div class='padding-5' id='popup" + id + "'>";
                popup += "<img src='" + imgProfil + "' height='30' width='30' class='' style='display: inline; vertical-align: middle; border-radius:100%;'>";
                popup += "<span style='margin-left : 5px; font-size:18px'>" + eltName + "</span>";

                if(data.tags && data.tags.length > 0){
				    popup += "<div style='margin-top : 5px;'>";
                    var totalTags = 0;
                    $.each(data.tags, function(index, value){
                        totalTags++;
                        if (totalTags < 3) {
                            popup += "<div class='popup-tags'>#" + value + " </div>";
                        }
                    })
                    popup += "</div>";
                }
                if(data.address){
                    var addressStr="";
                    if(data.address.streetAddress)
                        addressStr += data.address.streetAddress;
                    if(data.address.postalCode)
                        addressStr += ((addressStr != "")?", ":"") + data.address.postalCode;
                    if(data.address.addressLocality)
                        addressStr += ((addressStr != "")?", ":"") + data.address.addressLocality;
                    popup += "<div class='popup-address text-dark'>";
                    popup += 	"<i class='fa fa-map-marker'></i> "+addressStr;
                    popup += "</div>";
                }
                if(data.shortDescription && data.shortDescription != ""){
                    popup += "<div class='popup-section'>";
					popup += "<div class='popup-subtitle'>Description</div>";
					popup += "<div class='popup-shortDescription'>" + data.shortDescription + "</div>";
				    popup += "</div>";
                }
                if((data.url && typeof data.url == "string") || data.email || data.telephone){
                    popup += "<div id='pop-contacts' class='popup-section'>";
                    popup += "<div class='popup-subtitle'>Contacts</div>";
                    
                    if(data.url && typeof data.url === "string"){
                        popup += "<div class='popup-info-profil'>";
                        popup += "<i class='fa fa fa-desktop fa_url'></i> ";
                        popup += "<a href='" + data.url + "' target='_blank'>" + data.url + "</a>";
                        popup += "</div>";
                    }

                    if(data.email){
                        popup += "<div class='popup-info-profil'>";
                        popup += "<i class='fa fa-envelope fa_email'></i> " + data.email;
                        popup += "</div>";
                    }

                    if(data.telephone){
                        popup += "<div class='popup-info-profil'>";
                        popup += "<i class='fa fa-phone fa_phone'></i> ";
                        var tel = ["fixe", "mobile"];
                        var iT = 0;
                        $.each(tel, function(keyT, valT){
                            if(data.telephone[valT]){
                                $.each(data.telephone[valT], function(keyN, valN){
                                    if(iT > 0)
                                        popup += ", ";
                                    popup += valN;
                                    iT++; 
                                })
                            }
                        })
                        popup += "</div>";
                    }

                    popup += "</div>";
				    popup += "</div>";
                }
                var url = baseUrl+'/costum/co/index/slug/' + data.slug ;
                popup += "<div class='popup-section'>";
        //         if(paramsMapCO.activePreview)
				    // popup += "<a href='" + url + "' class='lbh-preview-element item_map_list popup-marker' id='popup" + id + "'>";
        //         else
                popup += "<a href='" + url + "' target='_blank' class='item_map_list popup-marker' id='popup" + id + "'>";
                popup += '<div class="btn btn-sm btn-more col-md-12">';
                popup += '<i class="fa fa-hand-pointer-o"></i>' + trad.knowmore;
                popup += '</div></a>';
                popup += '</div>';
                popup += '</div>';

                return popup;
            }
	}
});

dyFObj.unloggedMode=true;

costum.searchExist = function (type,id,name,slug,email) { 
			mylog.log("costum searchExist : "+type+", "+id+", "+name+", "+slug+", "+email); 
			var data = {
				type : type,
				id : id,
				map : { slug : slug }
			}
			//alert("here");
			$("#similarLink").hide();
			$("#ajaxFormModal #name").val("");


			// TODO - set a condition ONLY if can edit element (authorization)
			dyFObj.editElement( type,id, null, costum.typeObj[type].dynFormCostum);
			
			
	    
		};

costum[costum.slug]={
	init : function(){
		dyFObj.formInMap.forced.countryCode="FR";
		dyFObj.formInMap.forced.map={"center":["46.7342232", "2.74686000"], zoom: 5};
		dyFObj.formInMap.forced.showMap=true;



		// var html =	'<div class="modal fade text-center py-5"  id="addConfirmModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">'+
		// 			    '<div class="modal-dialog modal-md" role="document">'+
		// 			        '<div class="modal-content">'+
		// 			            '<div class="modal-body">'+
		// 			                '<div class="text-center cont-icon">'+
		// 			                    '<img src="'+assetPath+'/images/franceTierslieux/cartographie.png" alt="image"/>'+
		// 			                '</div>'+
		// 			                '<h3 class="pt-5 mb-0 text-secondary text-confirm">Votre tiers-lieu a bien été enregistré</h3>'+
		// 			                '<div class="append-confirm-message">'+
		// 			                '</div>'+
		// 			            '</div>'+
		// 			        '</div>'+
		// 			    '</div>'+
		// 			'</div>';	

		// $('body').append(html);
		//$(window).on('load',function(){
	   
	},
	"organizations" : {
		formData : function(data){
			//alert("formdata");
			mylog.log("formData",data);
			//if(dyFObj.editMode){
			$.each(data, function(e, v){
				//alert(e, v){
				if(typeof costum.lists[e] != "undefined" && e!="level"){
					if(notNull(v)){
						
						if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
						if(typeof v == "string")
							data.tags.push(v);
						else{
							$.each(v, function(i,tag){
								data.tags.push(tag);	
							});
						}
					}
					delete data[e];
				}
			});
			if(typeof data.category != "undefined" && data.category=="network"){
				if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
				data.tags.push("Réseau de tiers-lieux");
				//delete data.mainTag;
			}
			if(typeof data.mainTag != "undefined"){
				if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
				data.tags.push(data.mainTag);
				//delete data.mainTag;
			}
			return data;
		},
		afterSave:function(data){
			uploadObj.afterLoadUploader=false;

			//alert("nicos ici le callBack number 1");
			dyFObj.commonAfterSave(data, function(){
				mylog.log("callback aftersaved",data);
				// var category="tiers-lieu";
				// if(typeof data.map.tags!="undefined" && $.inArray("Réseau de tiers-lieux",data.map.tags)>-1 ){
				// 	category="réseau";
				// }
				// mylog.log("category aftersave",category);
				// $("#catTl").html(category);
				// $("#ajax-modal").modal('hide');
				// $("#addConfirmModal").modal('show');

				//  }
				if(userId==""){
					if(!dyFObj.unloggedProcess.isAlreadyRegister)
						$("#addConfirmModal .append-confirm-message").html("<span>Votre compte est maintenant créé<br>Il ne vous reste plus qu'à valider votre email sur votre boite éléctronique</span>");

					$('#addConfirmModal').on('hidden.bs.modal', function () {
						onchangeClick=false;
						history.replaceState({}, null, uploadObj.gotoUrl);
	//					location.hash(uploadObj.gotoUrl);
					  	//window.location.reload();
					});
				}else{
					//urlCtrl.loadByHash(uploadObj.gotoUrl);
				}
			});

			var tplCtx={};

			tplCtx.id = data.id;
			data=data.map;
			tplCtx.collection = data.collection;

			tplCtx.path = "allToRoot";
			tplCtx.value = {
				level : data.level
			};

			dataHelper.path2Value( tplCtx, function(params) { 
	                        
	        });

			tplCtx.path = "costum.slug";
			tplCtx.value ="reseauTierslieux";

			dataHelper.path2Value( tplCtx, function(params) { 
	                        
	        });

	        tplCtx.path = "costum.colors";
			tplCtx.value = {
				"main1" : data.main1,
				"main2" : data.main2
			};
			dataHelper.path2Value( tplCtx, function(params) { 
	                        
	        } );

	        tplCtx.path = "main1";
			dataHelper.unsetPath( tplCtx, function(params) { 	                        
	        } );

	        tplCtx.path = "main2";
			dataHelper.unsetPath( tplCtx, function(params) { 	                        
	        } );

			var urlTags = "";
			$.each(data.tags, function(e, v){
				if($.inArray(v,costum.lists.typePlace)>-1){
					urlTags+= v+"," ;
				}
			});
			if (urlTags!="" && urlTags.slice(-1)==","){
				urlTags=urlTags.slice(0, -1);
			}	
	        
            mylog.log("Themes in url",urlTags);

            $("#ajax-modal").hide();
                                  

	        var url = baseUrl+'/costum/co/index/slug/'+data.slug+'#?tags='+urlTags+'&originId='+costum.contextId;
	        setTimeout(function(){
	        		window.open(url, '_blank').focus();
	        	},3000);
	        
	  			
		}
		// afterBuild : function(data){
		// 	alert("after build TL");
		// 	if(userId==""){
		// 		$("#ajaxFormModal").before(
		// 			'<div class="first-register">'+
		// 				'<form id="check-register-by-email" role="form">'+
			
  //                   		'<div class="form-group input-group" style="margin-top: 20px;">'+
  //                       		'<span class="input-group-addon">@</span>'+
  //                       		'<input type="text" class="form-control" name="registerEmail" id="check-email-register" placeholder="Entrez e-mail" required>'+
  //                   		'</div>'+
  //                   		'<div class="contain-btn">'+
  //                   			'<a href="javascript:dyFObj.closeForm(); " class="mainDynFormCloseBtn btn btn-default btn-cancel" style="margin-right:10px;">'+
  //                   				'<i class="fa fa-times "></i> Annuler'+
  //                   			'</a>'+
		//                         '<button type="submit" class="btn btn-default btn-validate-email">Valider '+
		//                             '<i class="fa fa-arrow-circle-right"></i>'+
		//                         '</button>'+
  //                   		'</div>'+
  //   					'</form>'+
		// 			'</div>');
		// 		$("#ajaxFormModal").hide();
		// 		$("#check-register-by-email").validate({
		// 	      	rules: {
		// 	        	"registerEmail": {
		// 	            	"email": true
		// 	         	}
		// 	       	},
		// 	       	submitHandler: function(form){
		// 			    /* CACHER LE BOUTON */
		// 			    $(".btn-validate-email").html("Valider <i class='fa fa-spin fa-spinner'></i>");
		// 			    if(checkUniqueEmail($(".first-register #check-email-register").val())){

		// 			    }else{

		// 			    }
		// 			    jQuery("#btn_submit").hide();
		// 			}

		// 		});
		// 		// $('second-step-register').append("En validant, vous acceptez que l’ensemble des données soient affichées sur la cartographie et acceptez nos <a href='#' target='_blank'>CGU</a>.");
		// 		// $(".compagnonselect label").append(" <i class='fa fa-info-circle'></i>");
		// 		// $(".compagnonselect").append("<a href='#' target='_blank'><span class='col-xs-12' style='color:#FF286B'><i class='fa fa-info-circle'></i> Qu’est-ce qu’un tiers-lieu compagnon ?</span></a>");
		// 	}
		// 	// $(".compagnonselect label").append(" <i class='fa fa-info-circle'></i>");
		// 	// $(".compagnonselect").append("<a href='https://francetierslieux.fr/wp-content/uploads/2020/12/Charte_TLCompagnon.pdf' target='_blank'><span class='col-xs-12' style='color:#FF286B'><i class='fa fa-info-circle'></i> Qu’est-ce qu’un tiers-lieu compagnon ?</span></a>");
		// }

		
	}
};
	
