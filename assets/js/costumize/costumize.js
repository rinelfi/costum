$(function(){
    if(costum.type=="aap"){
        function lazyLoadAapLogo(time){
            if($("a[href=#welcome].menu-btn-top").length !=0 )
                $("a[href=#welcome].menu-btn-top").replaceWith(`
                    <a href="#welcome" class="btn btn-link pull-left lbh no-padding menu-btn-top">
                        <img src="${co2AssetPath}/images/OCECO.png" class="image-menu hidden-xs pull-left padding-left-10" height="50" width="auto">
                        <img src="${co2AssetPath}/images/OCECO.png" class="image-menu visible-xs pull-left padding-left-10" height="40">
                    </a>
                `);
            else
              setTimeout(function(){
                lazyLoadAapLogo(time+200)
              }, time);
        }
        lazyLoadAapLogo(200);
    }
})