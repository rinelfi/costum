adminPanel.views.badges = function(){

	var data={
		title : "Etiquetages (badges)",
		//types : [ "badges"],
		table : {
			// type:{
			// 	name : "Type",
			// 	notLink : true
			// },
			name: {
                name : "Nom",
                preview : true
            },
			description:{
                name : "Description"
            },
            category:{
                name : "Categorie"
            },
            tags:{
                name : "Utilisations"
            }
            
			//pdf : true,
			//status : true
		},
		paramsFilter : {
			container : "#filterContainer",
			defaults : {
				types : [ "badges" ],
				//private : true
			},
			filters : {
				text : true
			}
		},
	};
	if(typeof costum.isCostumAdmin != "undefined" && costum.isCostumAdmin){
		data.actions={
			update : true,
			delete : true
		};
	}

	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};

adminDirectory.events.delete = function(aObj){
	$("#"+aObj.container+" .deleteBtn").off().on("click", function(){
		mylog.log("adminDirectory..delete ", $(this).data("id"), $(this).data("type"));
		directory.deleteElement($(this).data("type"), $(this).data("id"), $(this), afterDelete);
	});
}

function afterDelete(data, type, id) {
	if(type == "badges"){
		for (const category of Object.keys(costum.badges)) {
			delete costum.badges[category][id];
		}
		costum = prepareList(costum);
	}
}
