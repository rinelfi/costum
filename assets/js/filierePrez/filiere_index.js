costum.filiere = {
    badges : {
		formData : function(data){
			if(typeof data.linksProjects){
				if(typeof data.links == "undefined") data.links={};
				data.links.projects=data.linksProjects;
				delete data.linksProjects;
			}
			return data;
		},
		afterSave : function(data){
			if(dyFObj.editMode){
				if($.inArray(data.map.category, ["domainAction"]) >= 0){
					// change data into badges
					oldName = costum.badges[data.map.category][data.id].name;
					costum.badges[data.map.category][data.id] = data.map;
					costum = prepareList(costum);
				}
			}else{
				if($.inArray(data.map.category, ["domainAction"]) >= 0){
					if(!costum.badges){
						costum.badges = {}
					}
					if(!costum.badges[data.map.category]){
						costum.badges[data.map.category] = {}
					}
					costum.badges[data.map.category][data.id] = data.map;
					costum = prepareList(costum);
				}
			}
			dyFObj.commonAfterSave();
		}
	},
}

function prepareList(costum) {
	for (const category of Object.keys(costum.badges)) {
		costum.lists[category] = {}
		const badges = costum.badges[category];
		const keys = Object.keys(badges);
		const list = {};
		for (const key of keys) {
			const element = costum.badges[category][key];
			var count = 0;
			if(element.parent){
				for(const parentKey of Object.keys(element.parent))
				if(keys.includes(parentKey)){
					if(!list[badges[parentKey].name]){
						list[badges[parentKey].name] = []
					}
					list[badges[parentKey].name].push(element.name);
					count++;
				}
			}
			if(!element.parent || count == 0){
				list[element.name] = [];
			}
		}
		costum.lists[category] = list;
	}
	return costum;
}