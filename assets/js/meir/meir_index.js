costum.meir = {
    "organizations": {
        afterSave: function(data) {
            dyFObj.commonAfterSave(data, function() {
                var tplCtx = {};
                var phone = data.map.mobile;
                tplCtx.id = data.map._id.$id;
                tplCtx.path = "telephone.mobile";
                tplCtx.value = [phone];
                tplCtx.collection = "organizations";
                dataHelper.path2Value(tplCtx, function(params) {
                    urlCtrl.loadByHash("#mapping");
                });
            });
        }
    }

}