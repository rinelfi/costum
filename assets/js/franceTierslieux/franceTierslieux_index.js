
paramsMapCO = $.extend(true, {}, paramsMapCO, {
	mapCustom:{
		icon: {
			getIcon:function(params){
				var elt = params.elt;
				mylog.log("icone ftl", elt.tags);
				if (typeof elt.tags != "undefined" && elt.tags != null ){
					if($.inArray("Compagnon France Tiers-Lieux", elt.tags)==-1){
						var myCustomColour = '#4623C9';
					}else {
						var myCustomColour = '#FF286B';
					}
				}else{
					var myCustomColour = '#4623C9';
				}
			
				var markerHtmlStyles = `
					background-color: ${myCustomColour};
					width: 3.5rem;
					height: 3.5rem;
					display: block;
					left: -1.5rem;
					top: -1.5rem;
					position: relative;
					border-radius: 3rem 3rem 0;
					transform: rotate(45deg);
					border: 1px solid #FFFFFF`;
			
				var myIcon = L.divIcon({
					className: "my-custom-pin",
					iconAnchor: [0, 24],
					labelAnchor: [-6, 0],
					popupAnchor: [0, -36],
					html: `<span style="${markerHtmlStyles}" />`
				});
				return myIcon;
			}
		},
		getClusterIcon:function(cluster){
			var childCount = cluster.getChildCount();
			var c = ' marker-cluster-';
			if (childCount < 100) {
				c += 'small-ftl';
			} else if (childCount < 1000) {
				c += 'medium-ftl';
			} else {
				c += 'large-ftl';
			}
			return L.divIcon({ html: '<div>' + childCount + '</div>', className: 'marker-cluster' + c, iconSize: new L.Point(40, 40) });
		}
	}
});

$("#show-button-map").addClass("changelabel");

$("#main-search-bar").attr("placeholder","Quel tiers-lieu recherchez-vous ?");
//$("#panel_map").css('display','block');
//$(".menuRight_header_title").text("Résultat");
$("#btn-filters").parent().css('display','none');
$("#btn-panel").html('<i class="fa fa-tags"></i><span>Filtre par mot-clé</span>');
//$("#result").parent().attr("class","col-xs-4");
$("#btn-panel").parent().attr("class","col-xs-6 pull-right");
//$("#btn-panel").parent().css("overflow","hidden")
$("#input_name_filter").attr("placeholder","Filtre par nom ...");
$('.form-register').find(".agreeMsg").removeClass("letter-red pull-left").html("En validant, vous acceptez que l’ensemble des données soient affichées sur la cartographie et acceptez nos <a href='https://francetierslieux.fr/cgu/' target='_blank' style='color: #FF286B;'>CGU</a>.");
$(".form-email-activation .container .row div:first-child .name img").attr("src", assetPath+"/images/franceTierslieux/logo-02.png");
$(".form-email-activation .container .row div:first-child h3").replaceWith("<div class=col-xs-12><h3 style='color: #5158ca' class='text-title'>VOUS N'AVEZ PAS REÇU VOTRE MAIL AFIN DE VALIDER VOTRE COMPTE</h3><span class='interogation'>?</span></div>");
$("#modalSendAgainSuccess .modal-dialog .modal-content .modal-header").removeClass("bg-green");
$("#modalNewPasswordSuccess .modal-dialog .modal-content .modal-header").removeClass("bg-green");
$("#modalRegisterSuccess .modal-dialog .modal-content .modal-header").removeClass("bg-green");
$("#modalSendAgainSuccess .modal-dialog .modal-content .modal-body h4:first-child").removeClass("letter-green");
$("#modalNewPasswordSuccess .modal-dialog .modal-content .modal-body h4:first-child").removeClass("letter-green");
$("#modalRegisterSuccess .modal-dialog .modal-content .modal-body .fa-unlock + b").removeClass("letter-green");
$("#modalSendAgainSuccess .modal-dialog .modal-content .modal-body .fa-unlock + b").removeClass("letter-green");
$("#modalNewPasswordSuccess .modal-dialog .modal-content .modal-body .fa-unlock + b").removeClass("letter-green");
$("#modalRegisterSuccess .modal-dialog .modal-content .modal-body .fa-unlock + b").removeClass("letter-green");
// <a href='' target='_blank' class='text></a>

var changeLabel = function() {
		$("#menuRight").on("click", function() {
				    if($(this).find("a").hasClass("changelabel")) {
				    	$(this).find("a").empty().html("<i class='fa fa-map-marker'></i> Afficher la carte");
				    }
				    else{
				    	$(this).find("a").empty().html("<i class='fa fa-list'></i> Afficher l’annuaire");
				    }
				//    text.nodeValue =  "Afficher "+ ($(this).find("a").hasClass("changelabel") ? "chap" : "la carte");
				    $(this).find("a").toggleClass("changelabel");
 			  	});
	};

dyFObj.unloggedMode=true;
costum.franceTierslieux={
	init : function(){
		dyFObj.formInMap.forced.countryCode="FR";
		dyFObj.formInMap.forced.map={"center":["46.7342232", "2.74686000"], zoom: 5};
		dyFObj.formInMap.forced.showMap=true;
		$('#mapContent').append( '<div class="pull-right BtnFiltersLieux"></div>');
            $('.BtnFiltersLieux').html( 
			  '<div id="mainMenuFilter ">'+
			    '<div class="list-group panel" id="filters-nav">  '+
			        '<a href="javascript:;"><button data-type="tags" data-key="0" data-value="Compagnon France Tiers-Lieux" class="btn-filters-select compagnon" id="colCompagnon"><span class="tooltips" data-toggle="tooltip" data-placement="bottom" title="Tiers-lieux accueillant et donnant conseil à des porteurs de projet qui souhaitent en savoir plus sur les tiers-lieux"><i class="icon-info fa fa-info-circle"></i></span> Tiers-lieux compagnons</button></a>'+
			  //  	'<a href="javascript:;"><button data-field="category" data-type="category" data-key="network" data-value="network" class="btn-filters-select network" id="colNetwork"><span class="tooltips" data-toggle="tooltip" data-placement="bottom" title=""><i class="icon-info fa fa-info-circle"></i></span> Les réseaux</button></a>'+
			    '</div>'+
			  '</div>'
			  );


		$(".logoLoginRegister").attr("src", assetPath+"/images/franceTierslieux/logo-02.png");
		var html = '<div id="popup-Tlieux" class="modal fade in animated bounceInDown" role="dialog">'+
					'<div class="modal-dialog">'+
						'<div class="modal-content row">'+
							'<div class="modal-header custom-modal-header">'+
								'<button type="button" class="close" data-dismiss="modal">×</button>'+
							'</div>'+
							'<div class="modal-body">'+
								'<div class="row">'+
									'<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs left-Tlieux">'+
										'<div class="contact-info">'+
											/*'<div class="text-center cont-icon">'+
												'<i class="fa fa-map-marker fa-4x mb-3 animated bounce"></i>'+
												'<img src="'+assetPath+'/images/franceTierslieux/soulignement.svg" alt="image"/>'+
											'</div>'+*/
											'<img src="'+assetPath+'/images/franceTierslieux/logo.svg" alt="image"/>'+
										'</div>'+
									'</div>'+
									'<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">'+
										'<div class="welcome-txt text-center">'+
											'Bienvenue sur<br> <strong>la cartographie nationale des tiers-lieux</strong>, pour en savoir plus sur les tiers-lieux les plus proches de chez vous !'+
										'</div>'+
										'<div class="text-center">'+
											'<img class="space-paragraph" src="'+assetPath+'/images/franceTierslieux/cartographie.png" alt="image"/>'+
										'</div>'+
										'<p class="section-paragraph text-center">'+
											'Si votre tiers-lieu ne se trouve pas sur la carte, <a href="javascript:;" data-form-type="organization" class="btn-open-form" id="btn-add-tl">vous pouvez l’ajouter vous-même</a> !'+
										'</p>'+
									'</div>'+
								'</div>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>';
		//modal de confirmation
		html+=	'<div class="modal fade text-center py-5"  id="addConfirmModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">'+
					    '<div class="modal-dialog modal-md" role="document">'+
					        '<div class="modal-content">'+
					            '<div class="modal-body">'+
					                '<div class="text-center cont-icon">'+
					                    '<img src="'+assetPath+'/images/franceTierslieux/cartographie.png" alt="image"/>'+
					                '</div>'+
					                '<h3 class="pt-5 mb-0 text-secondary text-confirm">Votre <span id="catTl"></span> a bien été enregistré</h3>'+
					                '<div class="append-confirm-message">'+
					                '</div>'+
					            '</div>'+
					        '</div>'+
					    '</div>'+
					'</div>';	
					

		$('body').append(html);
		//$(window).on('load',function(){
	    $('#popup-Tlieux').modal('show');
	    //});

	    //Ajout band sur le top et dessous du modal
	  /*$( '.modal-content' ).prepend( '<div class="top-strip" style="display: none;"></div>' );
	    $( '.modal-content' ).append( '<div class="bottom-strip" style="display: none;"></div>' );*/

	    // Ajouter style avec logo sous le titre du modal
	    var underStyle = '<div class="text-center under-title" style="display: none;">'+
		                    '<img class="space-paragraph" src="'+assetPath+'/images/franceTierslieux/cartographie.png" alt="image">'+
		                '</div>';


	    $( '.modal-header' ).append(underStyle);

		//Add Tiers lieux on popup
	    $('#btn-add-tl').on('click', function() {
	    	//alert("okok");
		    $("#popup-Tlieux").modal('hide');
		  });


	    const popupTlieux = localStorage.getItem('popupTlieux');

		if(popupTlieux === 'false'){
		  $('#popup-Tlieux').modal('hide');
		}

		$('#popup-Tlieux .custom-modal-header .close').on('click', function(){
		  localStorage.setItem('popupTlieux', 'false');
		});

		function getNetwork(){
		    mylog.log("---------getNetwork");

		    var params = {
		        contextId : costum.contextId,
		        contextType : costum.contextType
		    };

		    ajaxPost(
		        null,
		        baseUrl + "/costum/franceTierslieux/getnetwork",
		        params,
		        function (data) {
		        	//alert("dd");
		        	mylog.log("callbacknet",data);
		        	$.each(data, function(e, v){
		        		var posNet=$.inArray(v.name,costum.lists.network);
		        		if (posNet>-1){
		        			mylog.log("networks",posNet,costum.lists.network);
		        			costum.lists.network.splice(posNet, 1);


		        			// var posNet=	$.inArray(v,costum.lists.network);
		        			// alert(posNet);
		        		}
		        	});	
		        },
		        null,
		        null,
		        {async:false}
		    );    

		};

		var networkFullList =  [
            "A+ C'est Mieux",
            "Actes‐IF",
            "Artfactories/Autresparts",
            "Bienvenue dans la Canopée",
            "Cap Tiers‐Lieux",
            "Cédille Pro",
            "Collectif des Tiers‐Lieux Ile‐de‐France",
            "Collectif Hybrides",
            "Compagnie des Tiers‐Lieux",
            "Coopérative des Tiers‐Lieux",
            "Coordination Nationale des Lieux Intermédiaires et Indépendants (CNLII)",
            "Cowork'in Tarn",
            "Coworking Grand Lyon",
            "CRLII Occitanie",
            "GIP Recia",
            "Hub France Connectée",
            "L'ALIM (l'Assemblée des lieux intermédiaires marseillais)",
            "La Trame 07",
            "Label Tiers‐Lieux Occitanie",
            "Label C3",
            "Label Tiers‐Lieux Normandie",
            "Le DOG",
            "Le LIEN",
            "Lieux Intermédiaires en région Centre",
            "Réseau des tiers‐lieux Bourgogne Franche Comté",
            "Réseau Français des Fablabs",
            "Réseau Médoc",
            "Réseau TELA",
            "Tiers‐Lieux Edu"
        ];
		coInterface.bindButtonOpenForm = function(){
			$(".btn-open-form").off().on("click",function(){
				mylog.log("networkFullList",networkFullList);
		        	//pb de reset de la liste network au clic
		        	costum.lists.network=networkFullList;
		        var typeForm = $(this).data("form-type");
		        if(typeForm =="reseau"){
		        	getNetwork();
		        }
		        currentKFormType = ($(this).data("form-subtype")) ? $(this).data("form-subtype") : null;
		        //dynFormCostum = (costum && costum.typeObj && costum.typeObj[typeForm]) ? costum.typeObj[typeForm]["dynFormCostum"] : null;
		        if(contextData && contextData.type && contextData.id ){
		            
		            dyFObj.openForm(typeForm,"sub");
		        }
		        else{
		        	
		            dyFObj.openForm(typeForm);
		        }
		    });
		};    
		coInterface.bindButtonOpenForm();
		changeLabel();

		

	},
	"organizations" : {
		formData : function(data){
			//alert("formdata");
			mylog.log("formData",data);
			//if(dyFObj.editMode){
			$.each(data, function(e, v){
				//alert(e, v){
				if(typeof costum.lists[e] != "undefined" && e!="level"){
					if(notNull(v)){
						
						if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
						if(typeof v == "string")
							data.tags.push(v);
						else{
							$.each(v, function(i,tag){
								data.tags.push(tag);	
							});
						}
					}
					delete data[e];
				}
			});
			if(typeof data.category != "undefined" && data.category=="network"){
				if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
				data.tags.push("Réseau de tiers-lieux");
				//delete data.mainTag;
			}
			if(typeof data.mainTag != "undefined"){
				if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
				data.tags.push(data.mainTag);
				//delete data.mainTag;
			}
			return data;
		},
		afterSave:function(data){
			uploadObj.afterLoadUploader=false;

			//alert("nicos ici le callBack number 1");
			dyFObj.commonAfterSave(data, function(){
				mylog.log("callback aftersaved",data);
				var category="tiers-lieu";
				if(typeof data.map.tags!="undefined" && $.inArray("Réseau de tiers-lieux",data.map.tags)>-1 ){
					category="réseau";
				}
				mylog.log("category aftersave",category);
				$("#catTl").html(category);
				$("#ajax-modal").modal('hide');
				$("#addConfirmModal").modal('show');

				//  }
				if(userId==""){
					if(!dyFObj.unloggedProcess.isAlreadyRegister)
						$("#addConfirmModal .append-confirm-message").html("<span>Votre compte est maintenant créé<br>Il ne vous reste plus qu'à valider votre email sur votre boite éléctronique</span>");

					$('#addConfirmModal').on('hidden.bs.modal', function () {
						onchangeClick=false;
						history.replaceState({}, null, uploadObj.gotoUrl);
	//					location.hash(uploadObj.gotoUrl);
					  	window.location.reload();
					});
				}else{
					urlCtrl.loadByHash(uploadObj.gotoUrl);
				}
			});
		}
		// afterBuild : function(data){
		// 	alert("after build TL");
		// 	if(userId==""){
		// 		$("#ajaxFormModal").before(
		// 			'<div class="first-register">'+
		// 				'<form id="check-register-by-email" role="form">'+
  //                   		'<div class="form-group input-group" style="margin-top: 20px;">'+
  //                       		'<span class="input-group-addon">@</span>'+
  //                       		'<input type="text" class="form-control" name="registerEmail" id="check-email-register" placeholder="Entrez e-mail" required>'+
  //                   		'</div>'+
  //                   		'<div class="contain-btn">'+
  //                   			'<a href="javascript:dyFObj.closeForm(); " class="mainDynFormCloseBtn btn btn-default btn-cancel" style="margin-right:10px;">'+
  //                   				'<i class="fa fa-times "></i> Annuler'+
  //                   			'</a>'+
		//                         '<button type="submit" class="btn btn-default btn-validate-email">Valider '+
		//                             '<i class="fa fa-arrow-circle-right"></i>'+
		//                         '</button>'+
  //                   		'</div>'+
  //   					'</form>'+
		// 			'</div>');
		// 		$("#ajaxFormModal").hide();
		// 		$("#check-register-by-email").validate({
		// 	      	rules: {
		// 	        	"registerEmail": {
		// 	            	"email": true
		// 	         	}
		// 	       	},
		// 	       	submitHandler: function(form){
		// 			    /* CACHER LE BOUTON */
		// 			    $(".btn-validate-email").html("Valider <i class='fa fa-spin fa-spinner'></i>");
		// 			    if(checkUniqueEmail($(".first-register #check-email-register").val())){

		// 			    }else{

		// 			    }
		// 			    jQuery("#btn_submit").hide();
		// 			}

		// 		});
		// 		// $('second-step-register').append("En validant, vous acceptez que l’ensemble des données soient affichées sur la cartographie et acceptez nos <a href='#' target='_blank'>CGU</a>.");
		// 		// $(".compagnonselect label").append(" <i class='fa fa-info-circle'></i>");
		// 		// $(".compagnonselect").append("<a href='#' target='_blank'><span class='col-xs-12' style='color:#FF286B'><i class='fa fa-info-circle'></i> Qu’est-ce qu’un tiers-lieu compagnon ?</span></a>");
		// 	}
		// 	// $(".compagnonselect label").append(" <i class='fa fa-info-circle'></i>");
		// 	// $(".compagnonselect").append("<a href='https://francetierslieux.fr/wp-content/uploads/2020/12/Charte_TLCompagnon.pdf' target='_blank'><span class='col-xs-12' style='color:#FF286B'><i class='fa fa-info-circle'></i> Qu’est-ce qu’un tiers-lieu compagnon ?</span></a>");
		// }

		
	}
};
	
