

costum.terramies={
  init : function(){
    dyFObj.formInMap.forced.countryCode="FR";
    dyFObj.formInMap.forced.map={"center":["45.75784", "4.83510"], zoom: 10};
    dyFObj.formInMap.forced.showMap=true;
    }
}



poi.filters={};

poi.filters["atelier"] = {
"label" : "Ateliers Terramies", "key": "atelier", "icon": "comment"
};

poi.filters["recherche"] = {
"label": "Etudes et Recherches", "key": "recherche", "icon": "book"
};

poi.filters["mesure"] = {
"label": "Ressources juridiques", "key": "mesure", "icon": "gavel"
};

poi.filters["procedure"] = {
"label": "Protocoles et procédures", "key": "procedure", "icon": "cog"}
;

tradCategory["atelier"] = "Ateliers Terramies";
tradCategory["recherche"] = "Etudes et Recherches";
tradCategory["mesure"] = "Ressources juridiques";
tradCategory["procedure"] = "Protocoles et procédures";

$('.form-register').find(".agreeMsg").removeClass("letter-red pull-left").html("En cochant cette case, vous acceptez nos <a href='javascript:;' data-hash='#cgu' class='text-yellow lbh-menu-app'>CGU</a> et notre <a href='javascript:;' data-hash='#confidentialite' class='text-yellow lbh-menu-app'>Politique de Confidentialité,</a> et la collecte de vos informations dans un fichier informatisé. <br>Vous acceptez de recevoir des mails et des sollicitations de notre part. Sachez que vous pouvez vous désinscrire à tout moment de la newsletter via le lien de désinscription.");

jQuery(document).ready(function() {
    $("#filters-nav").addClass("col-sm-11 col-sm-offset-1 col-md-11 col-md-offset-1");
});
// 	"organizations" : {
// 		formData : function(data){
// 			//if(dyFObj.editMode){
// 			$.each(data, function(e, v){
// 				if(typeof costum.lists[e] != "undefined"){
// 					if(notNull(v)){
						
// 						if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
// 						if(typeof v == "string")
// 							data.tags.push(v);
// 						else{
// 							$.each(v, function(i,tag){
// 								data.tags.push(tag);	
// 							});
// 						}
// 					}
// 					delete data[e];
// 				}
// 			});
// 			if(typeof data.mainTag != "undefined"){
// 				data.tags.push(data.mainTag);
// 				//delete data.mainTag;
// 			}
// 			return data;
// 		}
// 	}
// };

// var filterObj = {
// 	container : "#filterContainer" ,
// 	urlData : baseUrl+"/" + moduleId + "/search/globalautocomplete",
// 	init : function(pInit = null){
// 		mylog.log("coeurNumerique filterObj.init",pInit);
// 		//Init variable
// 		var copyFilters = jQuery.extend(true, {}, filterObj);
// 		copyFilters.initVar(pInit);
// 		return copyFilters;

// 	},
// 	initVar : function(pInit){
// 		mylog.log("coeurNumerique filterObj.initVar",pInit);
// 		this.container =  ( (pInit != null && typeof pInit.container != "undefined") ? pInit.container : "#filterContainer" );
// 		this.urlData =  ( (pInit != null && typeof pInit.urlData != "undefined") ? pInit.urlData : "#filterContainer" );
// 		this.initDefaults(pInit);
// 		this.initViews(pInit);
// 		this.initActions(pInit);
// 	},
// 	initDefaults : function(pInit){
// 		mylog.log("coeurNumerique filterObj.initDefaults",pInit);
// 		var str = "";
// 		var fObj = this;
// 		if(typeof pInit.defaults != "undefined"){
// 			mylog.log("coeurNumerique filterObj.initDefaults defaults");
// 			if(typeof pInit.defaults.fields != "undefined"){
// 				$.each(pInit.defaults.fields,function(k,v){
// 					mylog.log("coeurNumerique filterObj.initDefaults fields",k, v);
// 					if(typeof searchObject.filters == "undefined" )
// 						searchObject.filters = {};
// 					if(typeof searchObject.filters[k] == "undefined" )
// 						searchObject.filters[k] = [];
// 					searchObject.filters[k].push(v);
					
// 				});
// 			}
// 		}
// 	},
// 	initActions : function(pInit){
// 		mylog.log("coeurNumerique filterObj.initActions",pInit);
// 		var str = "";
// 		var fObj = this;
// 		if(typeof pInit.filters != "undefined"){
// 			$.each(pInit.filters,function(k,v){
// 				mylog.log("coeurNumerique filterObj.initActions each", k,v);
// 				if(typeof v.action != "undefined" && typeof fObj.actions[v.action] != "undefined"){
// 					fObj.actions[v.action](fObj);
// 				}
// 			});
// 		}
// 	},
// 	initViews : function(pInit){
// 		mylog.log("coeurNumerique filterObj.initViews",pInit);
// 		var str = '<div class="pull-left text-dark labelFilterTit"><i class="fa fa-filter"></i> <span class="hidden-sm">Filtres</span></div>';
// 		var fObj = this;
// 		if(typeof pInit.filters != "undefined"){
// 			$.each(pInit.filters,function(k,v){
// 				mylog.log("coeurNumerique filterObj.initViews each", k,v);
// 				if(typeof v.view != "undefined" && typeof fObj.views[v.view] != "undefined"){
// 					str += fObj.views[v.view](k,v);
// 				}
// 			});
// 		}
// 		str+='<div id="activeFilters" class="col-xs-12 padding-5"></div>';
// 		$(fObj.container).html(str);
// 		fObj.initFilters();

// 	},
// 	initFilters : function(){
// 		mylog.log("coeurNumerique filterObj.initFilters");
// 		fObj=this;
// 		getParamsUrls=location.hash.split("?");
// 		if(typeof getParamsUrls[1] != "undefined" ){
// 			var parts = getParamsUrls[1].split("&");
// 	        var $_GET = {};
// 	         var initScopesResearch={"key":"open","ids":[]};
// 	        for (var i = 0; i < parts.length; i++) {
// 	            var temp = parts[i].split("=");
// 	            $_GET[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
// 	        }
// 	        if(Object.keys($_GET).length > 0){
// 	            $.each($_GET, function(e,v){
// 	                v=decodeURI(v);
// 	                if(e=="tags"){
// 	                	tags=v.split(",");
// 	                	$.each(tags, function(i, tag){
// 	                		fObj.manage.addActive(fObj,"tags",tag);
// 	                	});
// 	                }
// 	                if(e=="cities" || e=="zones"){
// 	                	if($.inArray(e,["cities","zones","cp"]) > -1) $.each(v.split(","), function(i, j){ initScopesResearch.ids.push(j) });
// 	                    if(initScopesResearch.key!="" && initScopesResearch.ids.length > 0)
// 	                    	checkMyScopeObject(initScopesResearch, $_GET, function(){
// 	                    		if(typeof myScopes.open != "undefined" && Object.keys(myScopes.open).length > 0){
// 			                    	$.each(myScopes.open, function(i, scope){
// 			                    		if(typeof scope.active != "undefined" && scope.active)
// 			                				fObj.manage.addActive(fObj, "scope",scope.name,i);
// 			                		});
// 			                	}
// 	                    	});
// 	                    mylog.log(myScopes, "checkherescopes" );
// 	                    if(typeof myScopes.open != "undefined" && Object.keys(myScopes.open).length > 0){
// 	                    	$.each(myScopes.open, function(i, scope){
// 	                    		if(typeof scope.active != "undefined" && scope.active)
// 	                				fObj.manage.addActive(fObj, "scope",scope.name,i);
// 	                		});
// 	                	}
// 	                }    
// 	         	});
// 	        }
// 	    }
// 	},
// 	search : function(){
// 		searchObject.nbPage=0;
// 		searchObject.count=true;
// 		mylog.log("coeurNumerique filterObj.search", searchObject);
// 		startSearch(0/*, null, function(){ if($(".searchEntityContainer").length == 0) $(".projectsProgress").show(); }*/);
// 	},
// 	views : {
// 		tags : function(k,v){
// 			mylog.log("coeurNumerique filterObj.views.tags", k,v);
// 			var str = "";
// 			return str;
// 		},
// 		select : function(k,v){
// 			mylog.log("coeurNumerique filterObj.views.select", k,v);
// 			var str = "<select>";
// 			str += '<option value="-1" >ALL</option>';
// 			if(typeof v.list != "undefined"){
// 				$.each( v.list ,function(kL,vL){
// 					str +='<option value="'+vL+'" >'+vL+'</option>';
// 				});
// 			}
// 			str += "</select>";
// 			return str;
// 		},
// 		selectList : function(k,v){
// 			mylog.log("coeurNumerique filterObj.views.selectList", k,v);
// 			var tradCategory = [];
// 							tradCategory["measure"]="Ressources juridiques";
// 							tradCategory["todo"]="Protocoles et procédures";
// 							tradCategory["doc"]="Etudes et Recherches";
// 							tradCategory["forum"]="Ateliers Terramies";
// 							tradCategory["entreprise"]="Entreprises";
// 							tradCategory["loisir"]="Sports et Loisirs";
// 							tradCategory["annuaireMedical"]="Annuaire Médical";
// 			label=(typeof v.name != "undefined") ? v.name: "Ajouter un filtre";
// 			str='<li class="dropdown">'+
// 					'<a href="javascript:;" class="dropdown-toggle menu-button btn-menu"  type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-label-xs="types" data-toggle="tooltip" data-placement="bottom">'+
// 						label+' <i class="fa fa-angle-down margin-left-5"></i>'+
// 					'</a>'+
// 					'<div class="dropdown-menu arrow_box" style="overflow-y: auto;" aria-labelledby="dropdownTypes">'+
// 						'<div class="list-filters">';
// 						if(typeof v.list != "undefined"){
// 							includeSubList=(typeof v.list=="object" && !Array.isArray(v.list)) ? true : false;
// 							$.each( v.list ,function(kL,vL){
// 							if(includeSubList){
// 									str +='<button class="mainTitle col-xs-12"  data-field="'+v.field+'" data-type="'+v.type+'" data-value="'+kL+'" >'+kL+'</button>';
// 									if(Object.keys(vL).length > 0){
// 										$.each(vL, function(i, sub){
// 											str +='<button class="col-xs-12"  data-field="'+v.field+'" data-type="'+v.type+'" data-value="'+sub+'" >'+sub+'</button>';
// 										});
// 									}
// 								}else{
// 									str +='<button class="col-xs-12" data-field="'+v.field+'" data-type="'+v.type+'" data-value="'+vL+'" >'+tradCategory[vL]+'</button>';
// 								}
// 							});
// 						}
// 					str+='</div></div>'+
// 				'</li>';
// 			return str;
// 		},
// 		scope : function(){
// 			mylog.log("coeurNumerique filterObj.views.scope");
// 			return 	'<div id="costum-scope-search"><div id="input-sec-search">'+
// 							'<div class="input-group shadow-input-header">'+
// 								'<span class="input-group-addon scope-search-trigger">'+
// 									'<i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>'+
// 								'</span>'+
// 								'<input type="text" class="form-control input-global-search" autocomplete="off"'+
// 									' id="searchOnCity" placeholder="Où ?">'+
// 							'</div>'+
// 							'<div class="dropdown-result-global-search col-xs-12 col-sm-5 col-md-5 col-lg-5 no-padding" '+
// 								' style="display: none;">'+
// 								'<div class="content-result">'+
// 								'</div>'+
// 							'</div>'+
// 					'</div></div>';
// 		}
// 	},
// 	actions : {
// 		tags : function(fObj){
// 			mylog.log("coeurNumerique filterObj.actions.tags");
// 			$("button[data-type='tags']").off().on("click",function(){
// 				mylog.log("coeurNumerique filterObj.actions.tags click");
// 				searchObject.tags.push($(this).data("value"));
// 				fObj.search();
// 				fObj.manage.addActive(fObj,"tags",$(this).data("value"), $(this).data("key"), $(this).data("field"));
// 			});
// 		},
// 		filters : function(fObj){
// 			mylog.log("coeurNumerique filterObj.actions.filters");
// 			$("button[data-type='filters']").off().on("click",function(){
// 				mylog.log("coeurNumerique filterObj.actions.filters click");
// 				if(typeof $(this).data("field") != "undefined"){
					
// 					if(typeof searchObject.filters == "undefined" )
// 						searchObject.filters = {};
					
// 					if(typeof searchObject.filters[$(this).data("field")] == "undefined" )
// 						searchObject.filters[$(this).data("field")] = [];
// 					searchObject.filters[$(this).data("field")].push($(this).data("value"));
// 					fObj.search();
// 					fObj.manage.addActive(fObj, "filters",$(this).data("value"), $(this).data("key"), $(this).data("field"));
// 				}
				
// 			});
// 		},
// 		scope : function(fObj){
// 			mylog.log("coeurNumerique filterObj.actions.scope");
// 			myScopes.open={};
// 			bindSearchCity("#costum-scope-search", function(){
// 				$(".item-globalscope-checker").off().on('click', function(){ 
// 					mylog.log("coeurNumerique filterObj.actions.scope click");
// 					$("#costum-scope-search .input-global-search").val("");
// 					$(".dropdown-result-global-search").hide(700).html("");
// 					myScopes.type="open";
// 					mylog.log("coeurNumerique filterObj.actions.scope globalscope-checker",  $(this).data("scope-name"), $(this).data("scope-type"));
// 					newScope=scopeObject(myScopes.search[$(this).data("scope-value")]);
// 					$.each(newScope, function(e, v){
// 						if(typeof v.active == "undefined" || !v.active)
// 							delete newScope[e];
// 						else
// 							newKeyScope=e;
// 					});
// 					myScopes.open[newKeyScope]=newScope[newKeyScope];
// 					localStorage.setItem("myScopes",JSON.stringify(myScopes));
// 					fObj.manage.addActive(fObj, "scope",$(this).data("scope-name"),newKeyScope);
// 					fObj.search();
// 				});
// 			});
// 		}
// 	},
// 	manage:{
// 		addActive: function(fObj, type, value, key, field){
// 			mylog.log("coeurNumerique filterObj.manage.addActive", type, value, key);
// 			var dataKey = (typeof key != "undefined") ? 'data-key="'+key+'" ' : "" ;
// 			var dataField = (typeof field != "undefined") ? 'data-field="'+field+'" ' : "" ;
// 			var tradCategory = [];
// 							tradCategory["measure"]="Ressources juridiques";
// 							tradCategory["todo"]="Protocoles et procédures";
// 							tradCategory["doc"]="Etudes et Recherches";
// 							tradCategory["forum"]="Ateliers Terramies";
// 							tradCategory["entreprise"]="Entreprises";
// 							tradCategory["loisir"]="Sports et Loisirs";
// 							tradCategory["annuaireMedical"]="Annuaire Médical";
// 			str='<div class="filters-activate tooltips" data-position="bottom" data-title="Effacer" '+
// 						dataKey +
// 						dataField +
// 						'data-value="'+value+'" '+
// 						'data-name="'+name+'" '+
// 						'data-type="'+type+'">' + 
// 					"<i class='fa fa-times-circle'></i>"+
// 					"<span "+
// 						"class='activeFilters' "+
// 						dataKey +
// 						dataField +
// 						'data-value="'+value+'" '+
// 						'data-scope-name="'+name+'" '+
// 						'data-type="'+type+'">' + 
// 						tradCategory[value] + 
// 					"</span>"+
// 				"</div>";
// 			$(fObj.container+" #activeFilters").append(str);
// 			coInterface.initHtmlPosition();
// 			$(fObj.container+" .filters-activate").off().on("click",function(){
// 				fObj.manage.removeActive(fObj, $(this).data("type"), $(this).data("value"), $(this).data("key"), $(this).data("field"));
// 				$(this).fadeOut(200);
// 				coInterface.initHtmlPosition();
// 			});
// 		},
// 		removeActive : function(fObj, type, value, key, field){
// 			mylog.log("coeurNumerique filterObj.manage.removeActive", type, value, key, field);
// 			if(type=="filters"){
// 				mylog.log("coeurNumerique filterObj.manage.removeActive filters", searchObject);
// 				if( typeof searchObject.filters != "undefined" && typeof searchObject.filters[field] != "undefined")
// 					searchObject.filters[field].splice(searchObject.filters[field].indexOf(value),1);
// 			}
// 			else if(type=="tags")
// 				searchObject.tags.splice(searchObject.tags.indexOf(value),1);
// 			else if(type=="scope"){
// 				delete myScopes.open[key];
// 			}
// 			fObj.search();
// 		}
// 	}
// };



// jQuery(document).ready(function(){

//      $(".logo-menutop").attr('src','/ph/assets/20806250/images/terramies/logo.png');


// });



document.addEventListener("DOMContentLoaded", function() {

  var title_loop = document.querySelectorAll("ul.c-datakey--list");

  title_loop.forEach(function(el) {
    var _clone = el.cloneNode(true);
    var _clone2 = el.cloneNode(true);
    el.parentNode.appendChild(_clone);
    el.parentNode.appendChild(_clone2);
  });

});




// var filterObj = {
// 	container : "#filterContainer" ,
// 	urlData : baseUrl+"/" + moduleId + "/search/globalautocomplete",
// 	init : function(pInit = null){
// 		mylog.log("terramies filterObj.init",pInit);
// 		//Init variable
// 		var copyFilters = jQuery.extend(true, {}, filterObj);
// 		copyFilters.initVar(pInit);
// 		return copyFilters;

// 	},
// 	initVar : function(pInit){
// 		mylog.log("terramies filterObj.initVar",pInit);
// 		this.container =  ( (pInit != null && typeof pInit.container != "undefined") ? pInit.container : "#filterContainer" );
// 		this.urlData =  ( (pInit != null && typeof pInit.urlData != "undefined") ? pInit.urlData : "#filterContainer" );
// 		this.initDefaults(pInit);
// 		this.initViews(pInit);
// 		this.initActions(pInit);
// 	},
// 	initDefaults : function(pInit){
// 		mylog.log("terramies filterObj.initDefaults",pInit);
// 		var str = "";
// 		var fObj = this;
// 		if(typeof pInit.defaults != "undefined"){
// 			mylog.log("terramies filterObj.initDefaults defaults");
// 			if(typeof pInit.defaults.fields != "undefined"){
// 				$.each(pInit.defaults.fields,function(k,v){
// 					mylog.log("terramies filterObj.initDefaults fields",k, v);
// 					if(typeof searchObject.filters == "undefined" )
// 						searchObject.filters = {};
// 					if(typeof searchObject.filters[k] == "undefined" )
// 						searchObject.filters[k] = [];
// 					searchObject.filters[k].push(v);
					
// 				});
// 			}
// 		}
// 	},
// 	initActions : function(pInit){
// 		mylog.log("terramies filterObj.initActions",pInit);
// 		var str = "";
// 		var fObj = this;
// 		if(typeof pInit.filters != "undefined"){
// 			$.each(pInit.filters,function(k,v){
// 				mylog.log("terramies filterObj.initActions each", k,v);
// 				if(typeof v.action != "undefined" && typeof fObj.actions[v.action] != "undefined"){
// 					fObj.actions[v.action](fObj);
// 				}
// 			});
// 		}
// 	},
// 	initViews : function(pInit){
// 		mylog.log("terramies filterObj.initViews",pInit);
// 		var str = '<div class="pull-left text-dark labelFilterTit"><i class="fa fa-filter"></i> <span class="hidden-sm">Filtres</span></div>';
// 		var fObj = this;
// 		if(typeof pInit.filters != "undefined"){
// 			$.each(pInit.filters,function(k,v){
// 				mylog.log("terramies filterObj.initViews each", k,v);
// 				if(typeof v.view != "undefined" && typeof fObj.views[v.view] != "undefined"){
// 					str += fObj.views[v.view](k,v);
// 				}
// 			});
// 		}
// 		str+='<div id="activeFilters" class="col-xs-12 padding-5"></div>';
// 		$(fObj.container).html(str);
// 		fObj.initFilters();

// 	},
// 	initFilters : function(){
// 		mylog.log("terramies filterObj.initFilters");
// 		fObj=this;
// 		getParamsUrls=location.hash.split("?");
// 		if(typeof getParamsUrls[1] != "undefined" ){
// 			var parts = getParamsUrls[1].split("&");
// 	        var $_GET = {};
// 	         var initScopesResearch={"key":"open","ids":[]};
// 	        for (var i = 0; i < parts.length; i++) {
// 	            var temp = parts[i].split("=");
// 	            $_GET[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
// 	        }
// 	        if(Object.keys($_GET).length > 0){
// 	            $.each($_GET, function(e,v){
// 	                v=decodeURI(v);
// 	                if(e=="tags"){
// 	                	tags=v.split(",");
// 	                	$.each(tags, function(i, tag){
// 	                		fObj.manage.addActive(fObj,"tags",tag);
// 	                	});
// 	                }
// 	                if(e=="cities" || e=="zones"){
// 	                	if($.inArray(e,["cities","zones","cp"]) > -1) $.each(v.split(","), function(i, j){ initScopesResearch.ids.push(j) });
// 	                    if(initScopesResearch.key!="" && initScopesResearch.ids.length > 0)
// 	                    	checkMyScopeObject(initScopesResearch, $_GET, function(){
// 	                    		if(typeof myScopes.open != "undefined" && Object.keys(myScopes.open).length > 0){
// 			                    	$.each(myScopes.open, function(i, scope){
// 			                    		if(typeof scope.active != "undefined" && scope.active)
// 			                				fObj.manage.addActive(fObj, "scope",scope.name,i);
// 			                		});
// 			                	}
// 	                    	});
// 	                    mylog.log(myScopes, "checkherescopes" );
// 	                    if(typeof myScopes.open != "undefined" && Object.keys(myScopes.open).length > 0){
// 	                    	$.each(myScopes.open, function(i, scope){
// 	                    		if(typeof scope.active != "undefined" && scope.active)
// 	                				fObj.manage.addActive(fObj, "scope",scope.name,i);
// 	                		});
// 	                	}
// 	                }    
// 	         	});
// 	        }
// 	    }
// 	},
// 	search : function(){
// 		searchObject.nbPage=0;
// 		searchObject.count=true;
// 		mylog.log("terramies filterObj.search", searchObject);
// 		startSearch(0/*, null, function(){ if($(".searchEntityContainer").length == 0) $(".projectsProgress").show(); }*/);
// 	},
// 	views : {
// 		tags : function(k,v){
// 			mylog.log("terramies filterObj.views.tags", k,v);
// 			var str = "";
// 			return str;
// 		},
// 		select : function(k,v){
// 			mylog.log("terramies filterObj.views.select", k,v);
// 			var str = "<select>";
// 			str += '<option value="-1" >ALL</option>';
// 			if(typeof v.list != "undefined"){
// 				$.each( v.list ,function(kL,vL){
// 					str +='<option value="'+vL+'" >'+vL+'</option>';
// 				});
// 			}
// 			str += "</select>";
// 			return str;
// 		},
// 		selectList : function(k,v){
// 			mylog.log("terramies filterObj.views.selectList", k,v);
// 			label=(typeof v.name != "undefined") ? v.name: "Ajouter un filtre";
// 			str='<li class="dropdown">'+
// 					'<a href="javascript:;" class="dropdown-toggle menu-button btn-menu"  type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-label-xs="types" data-toggle="tooltip" data-placement="bottom">'+
// 						label+' <i class="fa fa-angle-down margin-left-5"></i>'+
// 					'</a>'+
// 					'<div class="dropdown-menu arrow_box" style="overflow-y: auto;" aria-labelledby="dropdownTypes">'+
// 						'<div class="list-filters">';
// 						if(typeof v.list != "undefined"){
// 							includeSubList=(typeof v.list=="object" && !Array.isArray(v.list)) ? true : false;
// 							$.each( v.list ,function(kL,vL){
// 							if(includeSubList){
// 									str +='<button class="mainTitle col-xs-12"  data-field="'+v.field+'" data-type="'+v.type+'" data-value="'+kL+'" >'+kL+'</button>';
// 									if(Object.keys(vL).length > 0){
// 										$.each(vL, function(i, sub){
// 											str +='<button class="col-xs-12"  data-field="'+v.field+'" data-type="'+v.type+'" data-value="'+sub+'" >'+sub+'</button>';
// 										});
// 									}
// 								}else{
// 									str +='<button class="col-xs-12" data-field="'+v.field+'" data-type="'+v.type+'" data-value="'+vL+'" >'+vL+'</button>';
// 								}
// 							});
// 						}
// 					str+='</div></div>'+
// 				'</li>';
// 			return str;
// 		},
// 		scope : function(){
// 			mylog.log("terramies filterObj.views.scope");
// 			return 	'<div id="costum-scope-search"><div id="input-sec-search">'+
// 							'<div class="input-group shadow-input-header">'+
// 								'<span class="input-group-addon scope-search-trigger">'+
// 									'<i class="fa fa-map-marker fa-fw" aria-hidden="true"></i>'+
// 								'</span>'+
// 								'<input type="text" class="form-control input-global-search" autocomplete="off"'+
// 									' id="searchOnCity" placeholder="Où ?">'+
// 							'</div>'+
// 							'<div class="dropdown-result-global-search col-xs-12 col-sm-5 col-md-5 col-lg-5 no-padding" '+
// 								' style="display: none;">'+
// 								'<div class="content-result">'+
// 								'</div>'+
// 							'</div>'+
// 					'</div></div>';
// 		}
// 	},
// 	actions : {
// 		tags : function(fObj){
// 			mylog.log("terramies filterObj.actions.tags");
// 			$("button[data-type='tags']").off().on("click",function(){
// 				mylog.log("terramies filterObj.actions.tags click");
// 				searchObject.tags.push($(this).data("value"));
// 				fObj.search();
// 				fObj.manage.addActive(fObj,"tags",$(this).data("value"), $(this).data("key"), $(this).data("field"));
// 			});
// 		},
// 		filters : function(fObj){
// 			mylog.log("terramies filterObj.actions.filters");
// 			$("button[data-type='filters']").off().on("click",function(){
// 				mylog.log("terramies filterObj.actions.filters click");
// 				if(typeof $(this).data("field") != "undefined"){
					
// 					if(typeof searchObject.filters == "undefined" )
// 						searchObject.filters = {};
					
// 					if(typeof searchObject.filters[$(this).data("field")] == "undefined" )
// 						searchObject.filters[$(this).data("field")] = [];
// 					searchObject.filters[$(this).data("field")].push($(this).data("value"));
// 					fObj.search();
// 					fObj.manage.addActive(fObj, "filters",$(this).data("value"), $(this).data("key"), $(this).data("field"));
// 				}
				
// 			});
// 		},
// 		scope : function(fObj){
// 			mylog.log("terramies filterObj.actions.scope");
// 			myScopes.open={};
// 			bindSearchCity("#costum-scope-search", function(){
// 				$(".item-globalscope-checker").off().on('click', function(){ 
// 					mylog.log("terramies filterObj.actions.scope click");
// 					$("#costum-scope-search .input-global-search").val("");
// 					$(".dropdown-result-global-search").hide(700).html("");
// 					myScopes.type="open";
// 					mylog.log("terramies filterObj.actions.scope globalscope-checker",  $(this).data("scope-name"), $(this).data("scope-type"));
// 					newScope=scopeObject(myScopes.search[$(this).data("scope-value")]);
// 					$.each(newScope, function(e, v){
// 						if(typeof v.active == "undefined" || !v.active)
// 							delete newScope[e];
// 						else
// 							newKeyScope=e;
// 					});
// 					myScopes.open[newKeyScope]=newScope[newKeyScope];
// 					localStorage.setItem("myScopes",JSON.stringify(myScopes));
// 					fObj.manage.addActive(fObj, "scope",$(this).data("scope-name"),newKeyScope);
// 					fObj.search();
// 				});
// 			});
// 		}
// 	},
// 	manage:{
// 		addActive: function(fObj, type, value, key, field){
// 			mylog.log("terramies filterObj.manage.addActive", type, value, key);
// 			var dataKey = (typeof key != "undefined") ? 'data-key="'+key+'" ' : "" ;
// 			var dataField = (typeof field != "undefined") ? 'data-field="'+field+'" ' : "" ;
// 			str='<div class="filters-activate tooltips" data-position="bottom" data-title="Effacer" '+
// 						dataKey +
// 						dataField +
// 						'data-value="'+value+'" '+
// 						'data-name="'+name+'" '+
// 						'data-type="'+type+'">' + 
// 					"<i class='fa fa-times-circle'></i>"+
// 					"<span "+
// 						"class='activeFilters' "+
// 						dataKey +
// 						dataField +
// 						'data-value="'+value+'" '+
// 						'data-scope-name="'+name+'" '+
// 						'data-type="'+type+'">' + 
// 						value + 
// 					"</span>"+
// 				"</div>";
// 			$(fObj.container+" #activeFilters").append(str);
// 			coInterface.initHtmlPosition();
// 			$(fObj.container+" .filters-activate").off().on("click",function(){
// 				fObj.manage.removeActive(fObj, $(this).data("type"), $(this).data("value"), $(this).data("key"), $(this).data("field"));
// 				$(this).fadeOut(200);
// 				coInterface.initHtmlPosition();
// 			});
// 		},
// 		removeActive : function(fObj, type, value, key, field){
// 			mylog.log("terramies filterObj.manage.removeActive", type, value, key, field);
// 			if(type=="filters"){
// 				mylog.log("terramies filterObj.manage.removeActive filters", searchObject);
// 				if( typeof searchObject.filters != "undefined" && typeof searchObject.filters[field] != "undefined")
// 					searchObject.filters[field].splice(searchObject.filters[field].indexOf(value),1);
// 			}
// 			else if(type=="tags")
// 				searchObject.tags.splice(searchObject.tags.indexOf(value),1);
// 			else if(type=="scope"){
// 				delete myScopes.open[key];
// 			}
// 			fObj.search();
// 		}
// 	}
// };

// // // // Pour quand on passe sur le menu
// var t = true;
// $("#navbar").mouseover(function(){
//     if(t = true){
//     $(".nameMenuTop").slideDown(1350, function(){
//         t = false;
//     });
// 	}
// });

// $("#navbar").mouseleave(function(){
//     $(".nameMenuTop").slideUp(1350, function(){
//             t = true; 
//     });
// });
