adminPanel.views.directory = function(){
	var data={
		title : "Gestion des actions",
		table : {
            name: {
                name : "Nom"
            },
            description : {
            	"name" : "Description",
            	class : "col-xs-2 text-center"
            },
            private : { 
            	"name" : "Affichage",
        		class : "col-xs-1 text-center"
        	},
            //pdf : { "name" : "PDF"},
            status : { 
            	"name" : "Statut",
            	class : "col-xs-2 text-center"
        	},
        	
        	category : {
        		"name" : "Catégorie",
            	class : "col-xs-2 text-center"
        	}
        },
        paramsFilter : {
            container : "#filterContainer",
            defaults : {
               types : [ "projects" ],
            },
            filters : {
                text : true
            }
        },
        actions : {
            update : true,
            validated : true,
            private : true
        }
	};


	
	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};

