var dyfParcours = {
    beforeBuild: {
        properties: {
            name: {
                label: "Parcours",
                placeholder: "Etudier à l'EMIT",
                order: 1,
            },
            type: {
                value: "parcours",
            },
            organizer: {
                label: "Auteur(s)",
            },
            shortDescription: {
                order: 2,
                rules: {
                    required: true,
                },
            },
            endDate: {
                rules: {
                    required: false,
                },
            },
        },
    },
    onload: {
        actions: {
            setTitle: "Ajouter un parcours",
            src: {
                infocustom: "Remplir le champ",
            },
            presetValue: {
                public: true,
                type: "parcours",
            },
            hide: {
                publiccheckboxSimple: 1,
                typeselect: 1,
                imageuploader: 1,
                recurrencycheckbox: 1,
                formLocalityformLocality: 1,
                emailtext: 1,
                tagstags: 1,
                parentfinder: 1,
                organizerfinder: 1,
                urltext: 1,
                infocustom: 1,
            },
        },
    },
};

dyfParcours.afterSave = function(data) {
    dyFObj.commonAfterSave(data, function() {
        dyFObj.closeForm();
        refreshDataTimeline(width, height);
    });
};

function buildTimeline(width, height, isAdmin, groupNode) {
    const g = groupNode ? groupNode : d3
        .select("g#menu-parcours")
        .select("svg")
        .style("overflow", "visible")
        .select("g");
    const foreign = g
        .append("foreignObject")
        .attr("x", 0)
        .attr("y", 0)
        .attr("width", width)
        .attr("height", height)
        .style("overflow", "visible");
    const container = foreign.append("xhtml:div").classed("container", true);
    const ul_timeline = container
        .append("xhtml:ul")
        .attr("id", "timeline-list-container");
    if (isAdmin) {
        const li_add = ul_timeline
            .append("xhtml:li")
            .style("height", "60px")
            .append("xhtml:div")
            .classed("timeline-graph-badge timeline-graph-add  primary", true)
            .append("xhtml:a")
            .append("xhtml:i")
            .attr("class", "fa fa-plus-circle")
            .style("color", "#455a64");
        li_add.on("click", (e) => {
            e.stopPropagation();
            dyFObj.openForm("event", null, null, null, dyfParcours);
        });
    }
    return ul_timeline;
}

var maxI = 0;
var color = d3.scaleOrdinal(d3.schemeTableau10);
const colors = {};

function timeline(width, height, dataTimeline, groupNode = null, isAdmin = true) {
    console.log(dataTimeline);
    var ul_timeline = d3.select("ul#timeline-list-container");
    ul_timeline = ul_timeline.node() ? ul_timeline : buildTimeline(width, height, isAdmin, groupNode);
    const data_li = ul_timeline
        .classed("timeline-graph", true)
        .selectAll("li.timeline-content")
        .data(dataTimeline, d => d.id + d.title + d.debut + d.fin)
        .join(
            (enter) =>
            enterLi(
                enter
                .append("xhtml:li")
                .classed("timeline-content", true)
                .classed("timeline-graph-inverted", (d, i) => i % 2 == 1)
            ),
            (update) => update,
            (exit) => exitLi(exit)
        );

    function handleEditBtn(event, data) {
        dyFObj.editElement("event", data.id, null, dyfParcours);
    }

    function handleDeleteBtn(event, data) {
        bootbox.confirm("Voulez vous vraiment supprimer ce parcours?", (res) => {
            if (res) {
                var url = baseUrl + "/" + moduleId + "/element/delete/id/" + data.id + "/type/events";
                ajaxPost(
                    null,
                    url,
                    null,
                    function(data) {
                        if (data.result) {
                            toastr.success("Ce parcours a été supprimé avec succes");
                            refreshDataTimeline(width, height);
                        } else {
                            toastr.error(data.msg);
                        }
                    }
                );
            }
        })

    }

    function enterLi(li_timeline) {
        li_timeline
            .append("xhtml:div")
            .classed("timeline-graph-badge primary", true)
            .append("xhtml:a")
            .append("xhtml:i")
            .attr("class", "fa fa-dot-circle")
            .style("color", (d) => colors[d.id] ? colors[d.id] : colors[d.id] = color(maxI++));
        const panel_timeline = li_timeline
            .append("xhtml:div")
            .classed("timeline-graph-panel", true);
        const head_timeline = panel_timeline
            .append("xhtml:div")
            .classed("timeline-graph-heading", true)
            .append("div")
            .text((d) => d.debut + " - " + d.fin);
        const body_timeline = panel_timeline
            .append("xhtml:div")
            .classed("timeline-graph-body", true);
        body_timeline
            .append("xhtml:h3")
            .style("color", (d) => d.color)
            .text((d) => d.title);
        body_timeline.append("xhtml:p").text((d) => d.description);
        if (isAdmin) {
            const footer_timeline = panel_timeline
                .append("xhtml:div")
                .classed("timeline-graph-footer", true)
                .append("xhtml:div");
            const editBtn = footer_timeline
                .append("xhtml:i")
                .classed("fa fa-edit text-success", true)
                .on("click", handleEditBtn);
            const deleteBtn = footer_timeline
                .append("xhtml:i")
                .classed("fa fa-close text-danger", true)
                .on("click", handleDeleteBtn);
        }
    }

    function exitLi(li_timeline) {
        li_timeline.remove();
        ul_timeline
            .selectAll("li")
            .classed("timeline-graph-inverted", (d, i) => i % 2 == 0);
    }
}