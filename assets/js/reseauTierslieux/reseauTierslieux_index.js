
 var main1Color = costum.colors.main1;
 var main2Color = costum.colors.main2;
 var scopeCountryParam= ["FR","RE","MQ","GP","GF","YT"];
 var scopeLevelParam = ["3"];
 getDataCostum();
 function paramsMapLatLon(lat,lon){
	mylog.log("latlon",lat,lon);
	// forced coordinates centered on FR or RE
	lat = (costum.slug=="LaReunionDesTiersLieux" || costum.slug=="lesTiersLieuxCollaboratifsdelaReunion") ? "-21.115141" : "46.7342232";
	lon = (costum.slug=="LaReunionDesTiersLieux" || costum.slug=="lesTiersLieuxCollaboratifsdelaReunion") ?	"55.536384" : "2.74686000";
	zoom = (costum.slug=="LaReunionDesTiersLieux" || costum.slug=="lesTiersLieuxCollaboratifsdelaReunion") ? 9 : 5 ;
	//
	dyFObj.formInMap.forced.countryCode=(costum.slug=="LaReunionDesTiersLieux" || costum.slug=="lesTiersLieuxCollaboratifsdelaReunion") ? "RE" : "FR";
		dyFObj.formInMap.forced.map={"center":[lat, lon],"zoom" : zoom};
		dyFObj.formInMap.forced.showMap=true;
	paramsMapCO = $.extend(true, {}, paramsMapCO, {
		
		mapCustom:{
			icon: {
				getIcon:function(params){
					var elt = params.elt;
					mylog.log("icone ftl", elt.tags);
					if (typeof elt.tags != "undefined" && elt.tags != null ){
						if($.inArray("Compagnon France Tiers-Lieux", elt.tags)==-1){
							var myCustomColour = main1Color;
						}else {
							var myCustomColour = main2Color;
						}
					}else{
						var myCustomColour = main1Color;
					}
				
					var markerHtmlStyles = `
						background-color: ${myCustomColour};
						width: 3.5rem;
						height: 3.5rem;
						display: block;
						left: -1.5rem;
						top: -1.5rem;
						position: relative;
						border-radius: 3rem 3rem 0;
						transform: rotate(45deg);
						border: 1px solid #FFFFFF`;
				
					var myIcon = L.divIcon({
						className: "my-custom-pin",
						iconAnchor: [0, 24],
						labelAnchor: [-6, 0],
						popupAnchor: [0, -36],
						html: `<span style="${markerHtmlStyles}" />`
					});
					return myIcon;
				}
			},
			getClusterIcon:function(cluster){
				var childCount = cluster.getChildCount();
				var c = ' marker-cluster-';
				if (childCount < 20) {
					c += 'small-ftl';
				} else if (childCount < 50) {
					c += 'medium-ftl';
				} else {
					c += 'large-ftl';
				}
				return L.divIcon({ html: '<div>' + childCount + '</div>', className: 'marker-cluster' + c, iconSize: new L.Point(40, 40) });
			}
		}
	});
	if (notNull(lat) && notNull(lon)){
		paramsMapCO.forced =
			{
				latLon : [ 
			    	lat,
			    	lon
			    ]
			};
	}		
	//mapObj.init(paramsMapCO);

 }

$("#show-button-map").addClass("changelabel bg-main1");
$("#main-search-bar").attr("placeholder","Quel tiers-lieu recherchez-vous ?");

$("#btn-filters").parent().css('display','none');

//$("#input_name_filter").attr("placeholder","Filtre par nom ...");
$('.form-register').find(".agreeMsg").removeClass("letter-red pull-left").html("En validant, vous acceptez que l’ensemble des données soient affichées sur la cartographie.");
$(".form-email-activation .container .row div:first-child .name img").attr("src", assetPath+"/images/franceTierslieux/logo-02.png");

//$(".form-email-activation .container .row div:first-child h3").replaceWith("<div class=col-xs-12><h3 style='color: #5158ca' class='text-title'>VOUS N'AVEZ PAS REÇU VOTRE MAIL AFIN DE VALIDER VOTRE COMPTE</h3><span class='interogation'>?</span></div>");

// $("#modalSendAgainSuccess .modal-dialog .modal-content .modal-header").removeClass("bg-green");
// $("#modalNewPasswordSuccess .modal-dialog .modal-content .modal-header").removeClass("bg-green");
// $("#modalRegisterSuccess .modal-dialog .modal-content .modal-header").removeClass("bg-green");
// $("#modalSendAgainSuccess .modal-dialog .modal-content .modal-body h4:first-child").removeClass("letter-green");
// $("#modalNewPasswordSuccess .modal-dialog .modal-content .modal-body h4:first-child").removeClass("letter-green");
// $("#modalRegisterSuccess .modal-dialog .modal-content .modal-body .fa-unlock + b").removeClass("letter-green");
// $("#modalSendAgainSuccess .modal-dialog .modal-content .modal-body .fa-unlock + b").removeClass("letter-green");
// $("#modalNewPasswordSuccess .modal-dialog .modal-content .modal-body .fa-unlock + b").removeClass("letter-green");
// $("#modalRegisterSuccess .modal-dialog .modal-content .modal-body .fa-unlock + b").removeClass("letter-green");


var changeLabel = function() {
		$("#menuRight").on("click", function() {
				    if($(this).find("a").hasClass("changelabel")) {
				    	$(this).find("a").empty().html("<i class='fa fa-map-marker'></i> Afficher la carte");
				    }
				    else{
				    	$(this).find("a").empty().html("<i class='fa fa-list'></i> Afficher l’annuaire");
				    }
				//    text.nodeValue =  "Afficher "+ ($(this).find("a").hasClass("changelabel") ? "chap" : "la carte");
				    $(this).find("a").toggleClass("changelabel");
 			  	});
	}


function getDataCostum(){
			ajaxPost(
				null,
				baseUrl+"/co2/element/get/type/"+costum.contextType+"/id/"+costum.contextId,
				null,
				function(data){
					mylog.log("callback contextorga",data);
					var data = data.map;
					var level=data.level;
					//alert(level);

					var levelId = data.address[level]
					//alert(levelId);
					var levelNb = level.substr(5);

					if (typeof levelId=="undefined" && levelNb=="5"){
						alert("Nous ne trouvons pas la Métropole au sein de laquelle votre réseau agit. Nous vous proposons une portée départementale. Veuillez s'il vous plaît nous contacter afin de nous remonter la problématique à contact@communecter.org.")
						levelNb=levelNb-1;
						levelId=data.address["level"+levelNb];
					}

					
					//var scopeLevel = 
					//if(level!="level1"){
						scopeCountryParam = [data.address.addressCountry];
						scopeLevelParam = (level=="1") ? [Number(levelNb)+2] : [Number(levelNb)+1];
						if(data.address.addressCountry="RE"){
							scopeLevelParam=["6"];

						}
						upperLevelId=levelId;
						costum.address=data.geo;
						costum.parentId=data._id.$id;
					//}
					//alert(level);
					getZone(data.address[level],level);	
					referenceTl(levelNb,data.address[level]);	        	
				}, 
				null,
				null,
				{async:false}
			);
		}
function getZone(zoneId,level){
			mylog.log("getzone",zoneId,level);
			ajaxPost(
				null,
				baseUrl+"/co2/zone/getscopebyids",
				{zones:{zoneId}},
				function(data){ 
					//alert(level);
					var coord = {};
					mylog.log("scopes",data.scopes[zoneId+level].latitude);
					var latit = (typeof data.scopes[zoneId+level].latitude !="undefined") ? data.scopes[zoneId+level].latitude : null;
					//var longit = ;
					coord = {
						latitude : data.scopes[zoneId+level].latitude,
						longitude : data.scopes[zoneId+level].longitude
					};
					mylog.log("coord1",coord);

					//Ne pas forcer coordonnée
					//coord.latitude,coord.longitude
					paramsMapLatLon();						          	
				}, 
				null,
				null,
				{async:false}
			);
		}

function referenceTl(levelNb,levelId){
	mylog.log("referenceTl",levelNb,levelId);
	var refTags = {};
	
	if (typeof costum.loaded=="undefined" || costum.loaded==false){
		
		var dataCustom = {};
		dataCustom.id = costum.contextId;
		dataCustom.collection = costum.contextType;
		dataCustom.path = "costum.loaded";
		dataCustom.value=true;
 		dataHelper.path2Value(dataCustom,function(response){
	        if(response.result){
				toastr.success("added");
	            mylog.log("loaded costum",response);       	
			}
	    });

	    searchInterface.init();

	    if (typeof searchObject.tags !="undefined"){
			mylog.log("searchObject.tags",searchObject.tags);
			refTags = searchObject.tags;
		}

		
		var originId = (typeof window.location.href.split('originId=').pop()!="undefined") ? window.location.href.split('originId=').pop() : "" ;		

		ajaxPost(
			null,
			baseUrl+"/survey/form/duplicatetemplate/type/template/templateCostumId/"+originId+"/active/true",
			null,
			function(data){
				toastr.success("Formulaire dupliqué");
				mylog.log("Formulaire dupliqué",data);
				if(typeof(costum.dashboard.config)!="undefined"){
					$.each(costum.dashboard.config, function(k,v){
				    	if(typeof(v.graph.data.coform)!="undefined"){
				       		v.graph.data.coform=data.map._id.$id;
					    }
					});
					dataCustom.path = "costum.dashboard";
					dataCustom.value=costum.dashboard;
					dataHelper.path2Value(dataCustom,function(response){
				        if(response.result){
							toastr.success("added");
				            mylog.log("loaded costum",response);       	
						}
				    });
				}
				
                data=data.map;
                $.each(data.params, function(key, value){
                    mylog.log("valeur form",value);
                    if(key.indexOf("validateStep")>-1 && value.urlRedirect){
                        tplCtx={
                            id : data._id.$id,
                            collection : "forms",
                            path : "params."+key+".urlRedirect",
                            value : baseUrl+"/costum/co/index/slug/"+costum.slug
                        };
                        dataHelper.path2Value(tplCtx,function(response){
                            if(response.result){
                                toastr.success("Redirection mise à jour");
                                mylog.log("Redirection mise à jour",response);          
                            }
                        });
     				}
                });
			},
			null,
			null,
			{async:false}
		);

		
		ajaxPost(
			null,
			baseUrl+"/costum/blockcms/getcmsbywhere",
			{"source.key" : costum.assetsSlug, type:"template" },
			function(data){
				mylog.log("get cms template callback",data);
				var key = Object.keys(data)[0];
				var par = data[key];
				mylog.log("get cms template callback",par);
				var params = {
			        "ide" : (par['cmsList'])?par['cmsList']:"",
			        "tplParent"  : par._id.$id,
			        "page"       : par.page,
			        "parentId"   : costum.contextId,
			        "parentSlug" : costum.contextSlug,
			        "parentType" : costum.contextType,
			        "action" : "duplicate" 
			    }

			    dataCustom.id = par._id.$id;
				dataCustom.collection = "cms";
				dataCustom.path = "tplsUser."+costum.contextId+".welcome";
				dataCustom.value="using";
				dataHelper.path2Value(dataCustom,function(response){
					if(response.result){
					toastr.success("Modèle graphique importé");
					mylog.log("tplsUSER ADDED",response);       	
					}
				});

			      $.ajax({
			        type : 'POST',
			        data : params,
			        url : baseUrl+"/costum/blockcms/getcms",
			        dataType : "json",
			        async : false,

			        success : function(data){
					  	mylog.log("duplicate cms template callback",data);


					 //  	var dataCustom = {};
						// //dataCustom.arrayForm = true;

						// // duplicate coform
						// var contextParams = {
					 //        "contextId"   : costum.contextId,
					 //        "contextSlug" : costum.contextSlug,
					 //        "contextType" : costum.contextType,
					 //        "creator" : userId
					 //    }

						// $.ajax({
					 //        type : 'POST',
					 //        data : contextParams,
					 //        url : baseUrl+"/costum/tierslieux/duplicateform",
					 //        dataType : "json",
					 //        async : false,
					 //        success : function(data){
					 //        	toastr.success("Terminé!!");
					 //        	//document.location.reload();
					 //        	bootbox.dialog({message:`<div class="alert-white text-center">
						// 			<br>
						// 			<strong>Vous pouvez maintenant configurer le formulaire de connaissance de vos tiers lieux</strong>
						// 			<br>
						// 			<br>
						// 			<a href="${baseUrl}/#${costumSlug}.view.forms" class="btn btn-info">Aller à la paramètrage du formulaire</a>
						// 			</div>`}
						// 		);
					 //        }
					 //    });
			        }
			      });
			},
			null,
			null,
			{async:false}
		);
		//urlCtrl.loadByHash("#welcome");
	}
	
	if(Object.keys(refTags).length==0 && typeof(costum.reference)!="undefined" && typeof(costum.reference.themes)!="undefined"){
		refTags=costum.reference.themes;
	}	

		//alert("fere");
		ajaxPost(
					null,
					baseUrl+"/costum/francetierslieux/multireference/type/organizations",
					{zoneNb:levelNb,zoneId:levelId,tags:refTags},
					function(data){ 
						mylog.log("taggedTl",data);	
						if(refTags){
							var dataCustom = {};
							//dataCustom.arrayForm = true;
							dataCustom.id = costum.contextId;
							dataCustom.collection = costum.contextType;
							dataCustom.path = "costum.reference.themes";
							dataCustom.value=refTags;
					 		dataHelper.path2Value(dataCustom,function(response){
						        if(response.result){
									toastr.success("added");
						            mylog.log("themes reference costum",response);       	
								}
						    });
						}    

					}, 
					null,
					null,
					{async:false}
				);
	//}
}		

function lightenColor(color, percent) {
var num = parseInt(color.replace("#",""),16),
amt = Math.round(2.55 * percent),
R = (num >> 16) + amt,
B = (num >> 8 & 0x00FF) + amt,
G = (num & 0x0000FF) + amt;
return "#" + (0x1000000 + (R<255?R<1?0:R:255)*0x10000 + (B<255?B<1?0:B:255)*0x100 + (G<255?G<1?0:G:255)).toString(16).slice(1);
};

 var lighterMain1 = lightenColor(main1Color,10);
 var darkerMain1 = lightenColor(main1Color,-10);

		  				

dyFObj.unloggedMode=true;
costum[costum.slug]={
	init : function(){	
		var contextData=null;
		 var scopeCountryParam= ["FR","RE","MQ","GP","GF","YT"];
 		var scopeLevelParam = ["3"];
 		var upperLevelId = "";
 		costum.address={};
		document.documentElement.style.setProperty('--main1', main1Color);
		document.documentElement.style.setProperty('--main2', main2Color);
		document.documentElement.style.setProperty('--lighterMain1', lighterMain1);
		document.documentElement.style.setProperty('--darkerMain1', darkerMain1);
		
		$('#mapContent').append( '<div class="pull-right BtnFiltersLieux"></div>');
            $('.BtnFiltersLieux').html( 
			  '<div id="mainMenuFilter ">'+
			    '<div class="list-group panel" id="filters-nav">  '+
			        '<a href="javascript:;"><button data-type="tags" data-key="0" data-value="Compagnon France Tiers-Lieux" class="btn-filters-select compagnon" id="colCompagnon"><span class="tooltips" data-toggle="tooltip" data-placement="bottom" title="Tiers-lieux accueillant et donnant conseil à des porteurs de projet qui souhaitent en savoir plus sur les tiers-lieux"><i class="icon-info fa fa-info-circle"></i></span> Tiers-lieux compagnons</button></a>'+
		//	    	'<a href="javascript:;"><button data-field="category" data-type="category" data-key="network" data-value="Réseaux de Tiers-lieux" class="btn-filters-select network" id="colNetwork"><span class="tooltips" data-toggle="tooltip" data-placement="bottom" title=""><i class="icon-info fa fa-info-circle"></i></span> Les réseaux</button></a>'+
			    '</div>'+
			  '</div>'
			  );


		//$(".logoLoginRegister").attr("src", assetPath+"/images/franceTierslieux/logo-02.png");
		
		// var html = '<div id="popup-Tlieux" class="modal fade in animated bounceInDown" role="dialog">'+
		// 			'<div class="modal-dialog">'+
		// 				'<div class="modal-content row">'+
		// 					'<div class="modal-header custom-modal-header">'+
		// 						'<button type="button" class="close" data-dismiss="modal">×</button>'+
		// 					'</div>'+
		// 					'<div class="modal-body">'+
		// 						'<div class="row">'+
		// 							'<div class="col-lg-3 col-md-3 col-sm-3 hidden-xs left-Tlieux">'+
		// 								'<div class="contact-info">'+
		// 									/*'<div class="text-center cont-icon">'+
		// 										'<i class="fa fa-map-marker fa-4x mb-3 animated bounce"></i>'+
		// 										'<img src="'+assetPath+'/images/franceTierslieux/soulignement.svg" alt="image"/>'+
		// 									'</div>'+*/
		// 									'<img src="'+assetPath+'/images/franceTierslieux/logo.svg" alt="image"/>'+
		// 								'</div>'+
		// 							'</div>'+
		// 							'<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">'+
		// 								'<div class="welcome-txt text-center">'+
		// 									'Bienvenue sur<br> <strong>la cartographie nationale des tiers-lieux</strong>, pour en savoir plus sur les tiers-lieux les plus proches de chez vous !'+
		// 								'</div>'+
		// 								'<div class="text-center">'+
		// 									'<img class="space-paragraph" src="'+assetPath+'/images/franceTierslieux/cartographie.png" alt="image"/>'+
		// 								'</div>'+
		// 								'<p class="section-paragraph text-center">'+
		// 									'Si votre tiers-lieu ne se trouve pas sur la carte, <a href="javascript:;" data-form-type="organization" class="btn-open-form" id="btn-add-tl">vous pouvez l’ajouter vous-même</a> !'+
		// 								'</p>'+
		// 							'</div>'+
		// 						'</div>'+
		// 					'</div>'+
		// 				'</div>'+
		// 			'</div>'+
		// 		'</div>';
				//modal de confirmation
	var html =	'<div class="modal fade text-center py-5"  id="addConfirmModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">'+
					    '<div class="modal-dialog modal-md" role="document">'+
					        '<div class="modal-content">'+
					            '<div class="modal-body">'+
					                '<div class="text-center cont-icon">'+
					                    '<img src="'+assetPath+'/images/franceTierslieux/cartographie.png" alt="image"/>'+
					                '</div>'+
					                '<h3 class="pt-5 mb-0 text-secondary text-confirm">Votre tiers-lieu a bien été enregistré</h3>'+
					                '<div class="append-confirm-message">'+
					                '</div>'+
					            '</div>'+
					        '</div>'+
					    '</div>'+
					'</div>';	

		$('body').append(html);
		//$(window).on('load',function(){
	        $('#popup-Tlieux').modal('show');
	    //});

	    //Ajout band sur le top et dessous du modal
	  /*$( '.modal-content' ).prepend( '<div class="top-strip" style="display: none;"></div>' );
	    $( '.modal-content' ).append( '<div class="bottom-strip" style="display: none;"></div>' );*/

	    // Ajouter style avec logo sous le titre du modal
	    var underStyle = '<div class="text-center under-title" style="display: none;">'+
		                    '<img class="space-paragraph" src="'+assetPath+'/images/franceTierslieux/cartographie.png" alt="image">'+
		                '</div>';


	    $( '.modal-header' ).append(underStyle);

		//Add Tiers lieux on popup
	    $('#btn-add-tl').on('click', function() {
	    	//alert("okok");
		    $("#popup-Tlieux").modal('hide');
		  });


	    const popupTlieux = localStorage.getItem('popupTlieux');

		if(popupTlieux === 'false'){
		  $('#popup-Tlieux').modal('hide');
		}

		$('#popup-Tlieux .custom-modal-header .close').on('click', function(){
		  localStorage.setItem('popupTlieux', 'false');
		});
		
		changeLabel();
		getNetwork();

		function getNetwork(){
		    mylog.log("---------getNetwork");

		    var params = {
		        contextId : costum.contextId,
		        contextType : costum.contextType
		    };

		    ajaxPost(
		        null,
		        baseUrl + "/costum/franceTierslieux/getnetwork",
		        params,
		        function (data) {
		        	//alert("dd");
		        	mylog.log("callbacknet",data);
		        	$.each(data, function(e, v){
		        		var posNet=$.inArray(v.name,costum.lists.network);
		        		if (posNet>-1){
		        			mylog.log("networks",posNet,costum.lists.network);
		        			costum.lists.network.splice(posNet, 1);


		        			// var posNet=	$.inArray(v,costum.lists.network);
		        			// alert(posNet);
		        		}
		        	});	
		        }
		    );    

		}

	},
	"organizations" : {
		formData : function(data){
			//alert("formdata");
			mylog.log("formData",data);
			//if(dyFObj.editMode){
			$.each(data, function(e, v){
				//alert(e, v){
				if(typeof costum.lists[e] != "undefined"){
					if(notNull(v)){
						
						if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
						if(typeof v == "string")
							data.tags.push(v);
						else{
							$.each(v, function(i,tag){
								data.tags.push(tag);	
							});
						}
					}
					delete data[e];
				}
			});
			if(typeof data.mainTag != "undefined"){
				if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
				data.tags.push(data.mainTag);
				//delete data.mainTag;
			}
			return data;
		},
		afterSave:function(data){
			uploadObj.afterLoadUploader=false;

		//	alert("nicos ici le callBack number 1");
			dyFObj.commonAfterSave(data, function(){
				mylog.log("callback aftersaved",data)
				$("#ajax-modal").modal('hide');
				$("#addConfirmModal").modal('show');
				// if(map.category=="network" && map.name){
					

				// // 	data.name
				//  }
				if(userId==""){
					if(!dyFObj.unloggedProcess.isAlreadyRegister)
						$("#addConfirmModal .append-confirm-message").html("<span>Votre compte est maintenant créé<br>Il ne vous reste plus qu'à valider votre email sur votre boite éléctronique</span>");

					$('#addConfirmModal').on('hidden.bs.modal', function () {
						onchangeClick=false;
						history.replaceState({}, null, uploadObj.gotoUrl);
	//					location.hash(uploadObj.gotoUrl);
					  	window.location.reload();
					});
				}else{
					urlCtrl.loadByHash(uploadObj.gotoUrl);
				}
			});
		}
		// afterBuild : function(data){
		// 	alert("after build TL");
		// 	if(userId==""){
		// 		$("#ajaxFormModal").before(
		// 			'<div class="first-register">'+
		// 				'<form id="check-register-by-email" role="form">'+
  //                   		'<div class="form-group input-group" style="margin-top: 20px;">'+
  //                       		'<span class="input-group-addon">@</span>'+
  //                       		'<input type="text" class="form-control" name="registerEmail" id="check-email-register" placeholder="Entrez e-mail" required>'+
  //                   		'</div>'+
  //                   		'<div class="contain-btn">'+
  //                   			'<a href="javascript:dyFObj.closeForm(); " class="mainDynFormCloseBtn btn btn-default btn-cancel" style="margin-right:10px;">'+
  //                   				'<i class="fa fa-times "></i> Annuler'+
  //                   			'</a>'+
		//                         '<button type="submit" class="btn btn-default btn-validate-email">Valider '+
		//                             '<i class="fa fa-arrow-circle-right"></i>'+
		//                         '</button>'+
  //                   		'</div>'+
  //   					'</form>'+
		// 			'</div>');
		// 		$("#ajaxFormModal").hide();
		// 		$("#check-register-by-email").validate({
		// 	      	rules: {
		// 	        	"registerEmail": {
		// 	            	"email": true
		// 	         	}
		// 	       	},
		// 	       	submitHandler: function(form){
		// 			    /* CACHER LE BOUTON */
		// 			    $(".btn-validate-email").html("Valider <i class='fa fa-spin fa-spinner'></i>");
		// 			    if(checkUniqueEmail($(".first-register #check-email-register").val())){

		// 			    }else{

		// 			    }
		// 			    jQuery("#btn_submit").hide();
		// 			}

		// 		});
		// 		// $('second-step-register').append("En validant, vous acceptez que l’ensemble des données soient affichées sur la cartographie et acceptez nos <a href='#' target='_blank'>CGU</a>.");
		// 		// $(".compagnonselect label").append(" <i class='fa fa-info-circle'></i>");
		// 		// $(".compagnonselect").append("<a href='#' target='_blank'><span class='col-xs-12' style='color:#FF286B'><i class='fa fa-info-circle'></i> Qu’est-ce qu’un tiers-lieu compagnon ?</span></a>");
		// 	}
		// 	// $(".compagnonselect label").append(" <i class='fa fa-info-circle'></i>");
		// 	// $(".compagnonselect").append("<a href='https://francetierslieux.fr/wp-content/uploads/2020/12/Charte_TLCompagnon.pdf' target='_blank'><span class='col-xs-12' style='color:#FF286B'><i class='fa fa-info-circle'></i> Qu’est-ce qu’un tiers-lieu compagnon ?</span></a>");
		// }

		
	}
};
	
