window.addEventListener('scroll', function() {
    if ( $(document).scrollTop() > 150 ) {
        // Navigation Bar
        $("#mainNav").addClass("navbar_affix animated fadeInDown");
        $('#mainNav').removeClass('fadeIn');
    } else {
        $("#mainNav").removeClass("navbar_affix");
        $('#mainNav').removeClass('fadeInDown');
        $("#mainNav").addClass("animated fadeIn");
    }
});


