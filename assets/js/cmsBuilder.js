var cmsBuilder = {
	init : function(){
		cmsBuilder.bindEvents();
    cmsBuilder.manage.delete();
		$(".openListTpls").off().on("click", function(){
        cmsBuilder.openModal();
        localStorage.removeItem("parentCmsIdForChild");
		});
	},
  openModal : function(){
      smallMenu.openAjaxHTML(baseUrl+'/'+moduleId+'/cms/directory');
  },
	bindEvents: function(){
    $(".deleteCms").off().on("click", function() {
      var id = $(this).data("id");
      bootbox.confirm(`<div role="alert">
        <p><b class="">Supprimer le bloc</b> <b class="text-red">`+$(this).data("name")+`</b> <b class="">pour toute l'éternité?</b><br><br>
        Nous ne pouvons pas le récupérer une fois que vous le supprimez.<br>
        Êtes-vous sûr de vouloir supprimer définitivement ce bloc?
        </p> 
        </div> `,
                function(result) {
                    if (!result) {
                        return;
                    } else {
                        var type = "cms";
                        var urlToSend = baseUrl + "/co2/cms/delete/id/" + id;
                        $.ajax({
                                type: "POST",
                                url: urlToSend,
                                dataType: "json"
                            })
                            .done(function(data) {
                                if (data && data.result) {
                                    toastr.success("Elément effacé");
                                    $("#" + type + id).remove();
                                    cmsBuilder.openModal();
                                } else {
                                    toastr.error("something went wrong!! please try again.");
                                }
                            });
                    }
                });
        });

        $(document).off().on("click",".editDescBlock", function() {
            var tplId = $(this).data("id");
            var tplCtx = {};

            var dynFormCostum = {
                "beforeBuild": {
                    "properties": {
                        image: dyFInputs.image(),
                        path: {
                            label: "Chemin",
                            inputType: "text",
                            order: 2
                        }
                    }
                },
                "onload": {
                    "setTitle": "Modifier le cms",
                    "actions": {
                        "html": {
                            "nametext>label": "Nom du bloc",
                            "infocustom": ""
                        },

                        "hide": {
                            "documentationuploader": 1,
                            "structagstags": 1
                        }
                    }
                },
                afterSave: function() {
                    dyFObj.commonAfterSave(params, function() {
                        dyFObj.closeForm();
                        //$("#ajax-modal").modal('hide');
                        cmsBuilder.openModal();
                    });

                }
            };
            dyFObj.editElement("cms", tplId, null, dynFormCostum);

        });


        $(document).off().on("click",".thisTpls", function() {

            //tplCtx.key = $(this).data("key");
            tplCtx = {};
            tplCtx.collection = $(this).data("collection");
            tplCtx.path = "allToRoot";
            tplCtx.value = {
                name: $(this).data("name"),
                type: "blockCopy",
                path: $(this).data("path"),
                page: pageCms
            };

            tplCtx.value.parent = {};

            tplCtx.value.parent[thisContextId] = {
                type: thisContextType,
                name: thisContextSlug
            };
            tplCtx.value.haveTpl = false;
            if (localStorage.getItem("parentCmsIdForChild") != null) {
              tplCtx.value.tplParent = localStorage.getItem("parentCmsIdForChild");
              tplCtx.value.type = "blockChild";
              delete tplCtx.value.haveTpl;
            }
            if(typeof tplCtx.value == "undefined")
              toastr.error("value cannot be empty!");
            else {
              dataHelper.path2Value( tplCtx, function(params) {
                if(params.result){
                  toastr.success("Bloc bien ajouter");
                  $("#ajax-modal").modal("hide");
                  urlCtrl.loadByHash(location.hash);
                  // location.reload();
                }
                else 
                  toastr.error(params.msg);
                  
                  
              } );
            }
        });
        $(".createCMS").off().on("click", function(e) {
            e.preventDefault();
            var tplCtx = {};
            var dynFormCostum = {
                beforeBuild: {
                    "properties": {
                        image: dyFInputs.image(),
                        path: {
                            label: "Chemin",
                            inputType: "text",
                            value: "tpls.blockCms.[type de block].[nom du block]",
                            order: 2
                        },
                        type: {
                            "inputType": "hidden",
                            value: "blockCms"
                        }
                    }
                },
                onload: {
                    "setTitle": "Créer un nouveau cms",
                    "actions": {
                        "html": {
                            "nametext>label": "Nom du bloc cms",
                            "infocustom": ""
                        },
                        "hide": {
                            "documentationuploader": 1,
                            "structagstags": 1
                        }
                    }
                },
                afterSave: function(params) {
                    dyFObj.commonAfterSave(params, function() {
                        //alert();
                        //toastr.success("Élément bien ajouter");
                        dyFObj.closeForm();
                        //$("#ajax-modal").modal('hide');
                        cmsBuilder.openModal();
                    });
                }
            };
            dyFObj.openForm("cms", null, null, null, dynFormCostum);
        });
        /* dyFObj.editElement("cms" , tplId, null, dynFormCostum);
         var activeForm = {
           "jsonSchema" : {
             "title" : "Ajouter un nouveau bloc CMS",
             "type" : "object",
             onLoads : {
               onload : function(data){
                 $(".parentfinder").css("display","none");
               }
             },
             "properties" : {
               type : { 
                 "inputType" : "hidden",
                 value : "blockCms" 
               },
               path : {
                 label : "Chemin",
                 "inputType" : "text",
                 value : "tpls.blockCms.[type de block].[nom du block]"
               },
               name : { 
                 label : "Nom du bloc cms",
                 "inputType" : "text",
                 value : "" 
               },
                 image : dyFInputs.image(),
                 description : {
                   label : "Description",
                   inputType : "text",
                   value : ""
                 },

                 parent : {
                   inputType : "finder",
                   label : tradDynForm.whoiscarrypoint,
                   multiple : true,
                   rules : { lengthMin:[1, "parent"]}, 
                   initType: ["organizations", "projects", "events"],
                   openSearch :true
                 }
               }
             },

           };          

           activeForm.jsonSchema.afterBuild = function(){
             dyFObj.setMongoId('cms',function(data){
               uploadObj.gotoUrl = location.hash;
               tplCtx.id=dyFObj.currentElement.id;
               tplCtx.collection=dyFObj.currentElement.type;
             });
           };


           activeForm.jsonSchema.save = function () {
             tplCtx.value = {};
             $.each( activeForm.jsonSchema.properties , function(k,val) { 
               tplCtx.value[k] = $("#"+k).val();
             });
             if(typeof tplCtx.value == "undefined")
               toastr.error('value cannot be empty!');
             else {
               mylog.log("activeForm save tplCtx",tplCtx);
               dataHelper.path2Value( tplCtx, function(params) {
                 dyFObj.commonAfterSave(params,function(){
                   toastr.success("Élément bien ajouter");
                   $("#ajax-modal").modal('hide');
                   location.reload();
                 });
               } );
             }
           }*/
    },
    manage: {
        create: function() {

        },

        delete: function() {
            $(".deleteLine").off().on("click", function() {
                $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
                var id = $(this).data("id");
                var type = $(this).data("collection");
                var urlToSend = baseUrl+"/co2/cms/delete/id/"+id;
                bootbox.confirm(trad.areyousuretodelete,
                    function(result) {
                        if (!result) {
                            return;
                        } else {
                            $.ajax({
                                    type: "POST",
                                    url: urlToSend,
                                    dataType: "json"
                                })
                                .done(function(data) {
                                    if (data && data.result) {
                                        toastr.success("Element effacé");
                                        $("#" + type + id).remove();
                                    } else {
                                        toastr.error("something went wrong!! please try again.");
                                    }
                                    urlCtrl.loadByHash(location.hash);
                                });
                        }
                    }
                );
            });
        },

        addToTpl: function() {

        },
    },
    duplicateBlocks : function( slug ){
        ajaxPost(
            null,
            baseUrl+"/costum/blockcms/getcmsbywhere",
            {"source.key" : slug , type:"template" } , 

            function(d){

                var key = Object.keys(d)[0];
                var par = d[key];
                var params = {
                      "ide" : par.cmsList,
                      "tplParent"  : par._id.$id,
                      "page"       : par.page,
                      "parentId"   : costum.contextId,
                      "parentSlug" : costum.contextSlug,
                      "parentType" : costum.contextType,
                      "action" : "duplicate" 
                } 

                var dataCustom = {
                    id : par._id.$id,
                    collection : "cms",
                    path : "tplsUser."+costum.contextId+".welcome",
                    value:"using"
                }

                dataHelper.path2Value(dataCustom,function(r){
                    if(r.result)
                        toastr.success("Template importé");
                })

                $.ajax({
                    type : 'POST',
                    data : params,
                    url : baseUrl+"/costum/blockcms/getcms",
                    dataType : "json",
                    async : false,

                    success : function(data){
                        toastr.success("Terminé!!");
                        document.location.reload();
                        }
                });


        });
    }

}

/*carousel******************************************/
var carouselObj = {
    costumDefaultImage:{},
    initCarousel: function(paramsData, opt) {
        var options = {
            onlyExternal: true,
            slidesPerView: 1,
            spaceBetween: 10,
            keyboard: {
                enabled: true,
            },
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            breakpoints: {
                640: {
                    slidesPerView: exists(opt.xs) ? opt.xs : 1,
                    spaceBetween: 20,
                },
                768: {
                    slidesPerView: exists(opt.md) ? opt.md : 2,
                    spaceBetween: 20,
                },
                1024: {
                    slidesPerView: exists(opt.lg) ? opt.lg : 4,
                    spaceBetween: 20,
                },
            }
        };
        if (exists(paramsData["cardAutoPlay"]) && paramsData["cardAutoPlay"] == true) {
            options.autoplay = {
                delay: 6000,
                disableOnInteraction: true,
            }
        }
        if(exists(paramsData["elDesign"]) && (paramsData["elDesign"] != 6))
            var swiper = new Swiper(opt.container, options);
    },
    bindEvents: function(paramsData, kunik) {
        $('.delete-element-' + kunik).on('click', function() {
            var idEvent = $(this).data('id');
            bootbox.confirm(trad.areyousuretodelete, function(result) {
                if (result) {
                    var url = baseUrl + "/" + moduleId + "/element/delete/id/" + idEvent + "/type/" + paramsData["elType"];
                    var param = new Object;
                    ajaxPost(null, url, param, function(data) {
                        if (data.result) {
                            toastr.success(data.msg);
                            urlCtrl.loadByHash(location.hash);
                        } else {
                            toastr.error(data.msg);
                        }
                    })
                }
            })
        });
        $('.edit-element-' + kunik).on('click', function() {
            dyFObj.editElement(paramsData["elType"], $(this).data('id'),null,commonElObj);
        });
        $('.container1-' + kunik + ' .smartgrid-slide-element').removeClass('col-lg-4 col-md-4 col-sm-6');
        $('.container1-' + kunik + ' .swiper-slide .lbh').removeClass('lbh').addClass('lbh-preview-element');
        $('.container1-' + kunik + ' .swiper-slide .see-more').removeClass('lbh-preview-element').addClass('lbh');
        $(".block-container-"+kunik+" button.add").text(exists(trad["add"+paramsData["elType"]]) ? trad["add"+paramsData["elType"]] : "Ajouter")

        document.querySelectorAll('img.lzy_img').forEach((v) => {
            imageObserver.observe(v);
        });
        coInterface.bindLBHLinks();
    },
    toSameHeight: function(selector = null) {
        var maxHeight = Math.max.apply(null, $(selector).map(function() {
            return $(this).height();
        }).get());
        $(selector).css("height", maxHeight);
    },
    commonProperties : function(paramsData){
        var properties = {
            "title" : {
                "inputType" : "text",
                "label" : "Titre",
            },
            "title2" : {
                "inputType" : "text",
                "label" : "Sous titre",
            },
            "titleBg" : {
                "inputType" : "colorpicker",
                "label" : "Fond du titre",
            },
            "elType" : {
                "inputType" : "select",
                  "label" : "Tyle de l'élément",
                  "options" : elTypeOptions,
            },
            "elPoiType" : {
                  "inputType" : "select",
                  "label" : "Type de "+trad.poi,
                  "options" : poiOptions,
            },
            "elDesign" : {
              "inputType" : "select",
                "label" : "Mode d'affichage",
                "options" : {
                  "1" : "Carousel multi-carte configurable",
                  "3" : "Non carousel multi-carte configurable",
                  "2" : "Carousel à une carte horizontal",
                  "6" : "Non Carousel à une carte horizontal",
                  "4" : "Carousel multi-carte de communecter",
                  "5" : "Non Carousel multi-carte de communecter",
                },
                "noOrder":true    
            },
            "imgShow" : {
                "label": "Afficher l'image",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
                
            },
            "imgWidth" : {
                "inputType" : "text",
                "label" : "Largeur de l'image (% ou px)",
                "placeholder" : "359px"
            },
            "imgHeight" : {
                "inputType" : "text",
                "label" : "Hauteur de l'image",
                "placeholder" : "359px"
            },
            "imgRound" : {
                "inputType" : "text",
                "label" : "Image ronde (% ou px)",
                "placeholder" : "50%"
            },
            "imgBg" :{
                "inputType" : "colorpicker",
                "label" : "Fond de l'image",
            },
            "imgBorder" : {
                "inputType" : "text",
                "label" : "Epaisseur de la bordure (px)",
                "placeholder" : "5px"
            },
            "imgBorderColor" :{
                "inputType" : "colorpicker",
                "label" : "Couleur de la bordure",
            },
            "imgDefault" : {
                "label": "Image par défaut d'un item",
                "inputType" : "uploader",
                "docType": "image",
                "contentKey" : "slider",
                "itemLimit" : 1,
                "filetypes": ["jpeg", "jpg", "gif", "png"],
                "showUploadBtn": false,
                "domElement" : "image",
                "endPoint" :"/subKey/image",
            },
            "cardBoxShadow" : {
              "label": "Ombre sur la carte",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
                
            },
            "cardBg" : {
                "inputType" : "colorpicker",
                "label" : "Fond de la carte",
            },
            "cardNbResult" : {
                "inputType" : "text",
                "label" : "Nombre de resultat",
                rules : {
                  number:true,
                  max:100,
                  min:0,
                },
            },
            "cardBtnShowMore" : { 
              "label": "Bouton voir plus",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
                
            },
            "infoBg" : {
                "inputType" : "colorpicker",
                "label" : "Fond des informations",
            },
            "infoHeight" : {
              "label": "Hauteur des informations textuelle",
                "inputType" : "text",
                "placeholder" :"default : auto"
            },
            "showName" : { 
                "label": "Nom",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            },
            "showType" : { 
                "label": "Type",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
                
            },                
            "showShortDescription" : { 
                "label": " Description courte",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            },
            "showDescription" : { 
                "label": "Description longue",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            },
            "showDate" : { 
                "label": "Date",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            },
            "showAdress" : { 
                "label": "Adresse",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            },
            "showTags" : { 
                "label": "Mots clef",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            },
            "showRoles" : { 
                "label": "Roles",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            },
            "formTitle" : {
                "label": "Titre",
                "inputType" : "text",          
            },
            "buttonShowAll" : {
              "label": "Bouton afficher tout les élément",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            },
            "buttonShowAllText" : {
              "label": "Text du bouton afficher tout les élément",
                "inputType" : "text",
            },
        };

        if(exists(paramsData["elDesign"]) && paramsData["elDesign"] != 3){
            properties["cardAutoPlay"] = { 
              "label": "Slide automatique",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
                
            }
        }

        if(exists(paramsData["elDesign"]) && paramsData["elDesign"] == 3){ 
            properties["cardSpaceBetween"] = {
                "inputType" : "text",
                "label" : "Espace entre les cartes",
                rules : {
                  number:true,
                  max:100,
                  min:0,
                },
            }          
        }

        if(exists(paramsData["elDesign"]) && paramsData["elDesign"] == 1 || paramsData["elDesign"] == 3){
            properties["infoDistanceFromImage"] = {
                "inputType" : "text",
                "label" : "Distance des infos et image('ex: -150px')",
                "placeholder" : "-144px"
            };
            properties["infoMarginLeft"] = {
                "inputType" : "text",
                "label" : "Marge à gauche",
                "placeholder" : "0px"
            };
            properties["infoMarginRight"] = {
                "inputType" : "text",
                "label" : "Marge à droite",
                "placeholder" : "0px"
            };
            properties["infoBorderRadius"] = {
                "inputType" : "text",
                "label" : "Rayon de bordure",
                "placeholder" : "0px"
            };
            properties["infoOnHover"] = { 
                "label": "Afficher les informations au survol",
                "inputType" : "checkboxSimple",
                "params" : checkboxSimpleParams,
            };
        }

        if(exists(paramsData["elDesign"]) && paramsData["elDesign"] == 2){
            properties["infoAlign"] = {
                "inputType" : "select",
                "label" : "Alignement",
                "options" : {
                  "flex-start" : "Gauche",
                  "center" : "Centre",
                  "flex-end" : "Droite"
                }           
            };
            properties["infoOrder"] = {
                "inputType" : "checkboxSimple",
                "label" : "Image à gauche",
                "params" : checkboxSimpleParams,    
            };
            properties["infoCenter"] = {
                "inputType" : "checkboxSimple",
                "label" : "Centrer verticalement",
                "params" : checkboxSimpleParams,
            };
        }

        if(exists(paramsData["elDesign"]) && paramsData["elType"] == "events"){
            properties["infoDateFormat"] = {
                  "inputType" : "select",
                  "label" : "Format de la date",
                  "options" : {
                    "1" : "Format 1",
                    "2" : "Format 2"
                }       
            }
        }
        
        if(exists(paramsData["elDesign"]) && paramsData["elDesign"] != 2 && paramsData["elDesign"] != 6){    
            properties["cardNumberPhone"] = {
                "inputType" : "text",
                "label" : "Nombre de la carte sur téléphone",
                rules : {
                  number:true,
                  max:2,
                  min:0,
                },
            };
            properties["cardNumberTablet"] = {
                "inputType" : "text",
                "label" : "Nombre de la carte sur tablet",
                rules : {
                  number:true,
                  max:3,
                  min:0,
                },
            };
            properties["cardNumberDesktop"] = {
                "inputType" : "text",
                "label" : "Nombre de la carte sur desktop",
                rules : {
                  number:true,
                  max:10,
                  min:0,
                },
            };
        }
        return properties;
    },
    //******************FIRST DESIGN*************************/
    multiCart: {
        init: function(paramsData, kunik,autocomplete=true,externalData=null) {
            var $this = this;
            $this.cssCarousel(paramsData, kunik);
            if ($(".container1-" + kunik + " .swiper-wrapper").length <= 0) {
                $this.fetchData(paramsData, kunik,autocomplete,externalData);
            }
            var carOption = {
                xs: exists(paramsData["cardNumberPhone"]) ? paramsData["cardNumberPhone"] : 1,
                md: exists(paramsData["cardNumberTablet"]) ? paramsData["cardNumberTablet"] : 2,
                lg: exists(paramsData["cardNumberDesktop"]) ? paramsData["cardNumberDesktop"] : 3,
            }

            if (exists(paramsData["elDesign"]) && paramsData["elDesign"] != 3)
                carOption["container"] = ".container1-" + kunik + " .swiper-container";

            setTimeout(function() {
                carouselObj.initCarousel(paramsData, carOption);
                carouselObj.toSameHeight(".container1-" + kunik + " .el-info-container>div");
                carouselObj.bindEvents(paramsData, kunik);
            }, 2500)
            console.log("carOptionko", carOption)
        },
        fetchData: function(paramsData, kunik,autocomplete=true,externalData=null) {
            var html = "";
            var btns = "";
            var dataCount=0;
            var contextParent = "parent."+contextData._id.$id;
            params = {
                searchType: [paramsData["elType"]],
                /*indexStep:2 */
                filters :{'$or':{
                    "source.keys":contextData.slug
                }}
            };

            params.filters['$or'][contextParent] = {'$exists':true};
            /*if (exists(paramsData["cardNbResult"]) && paramsData["cardNbResult"] != "")
                params["indexStep"] = paramsData["cardNbResult"];*/

            if (exists(paramsData["elPoiType"]) && exists(paramsData["elType"]) && paramsData["elType"]=="poi" && paramsData["elPoiType"] != ""){
                params.filters.type = paramsData["elPoiType"];
                params.types = ["poi"];
            }

            if(autocomplete == true)
                ajaxPost(
                    null,
                    baseUrl + "/" + moduleId + "/search/globalautocomplete",
                    params,
                    function(data) {
                        dataCount = Object.keys(data.results).length;
                        var i = 1;
                        $.each(data.results, function(k, v) {
                            if( i <= paramsData["cardNbResult"]){
                                if (exists(paramsData["elDesign"]) && (paramsData["elDesign"] == 4 || paramsData["elDesign"] == 5)) {
                                    v = directory.prepParamsHtml(v);
                                    html += `<div class="swiper-slide">`;
                                    if (paramsData["elType"] == "events")
                                        html += directory.eventPanelHtml(v);
                                    else if (paramsData["elType"] == "classifieds")
                                        html += directory.classifiedPanelHtml(v);
                                    else
                                        html += directory.elementPanelHtml(v);

                                    if (isInterfaceAdmin) {
                                        html += `
                                              <button class="btn btn-sm btn-success edit-element-${kunik} hiddenPreview" data-id="${v._id.$id}">
                                                        <i class="fa fa-edit"></i>
                                              </button>
                                              <button class="btn btn-sm btn-danger delete-element-${kunik} hiddenPreview" data-id="${v._id.$id}">
                                                        <i class="fa fa-trash"></i>
                                              </button>
                                        `;
                                    }
                                    html += `</div>`;
                                } else{
                                    html += carouselObj.multiCart.view(v, paramsData, kunik);                                  
                                }
                            }      
                          i++;
                        });

                        if(exists(paramsData["cardNbResult"]) && notEmpty(paramsData["cardNbResult"]) && dataCount > paramsData["cardNbResult"] && paramsData["buttonShowAll"]==true){
                          if (paramsData["elType"] == "events")
                              html +=
                              `<div class="swiper-slide">
                                  <a href="#agenda" class="btn btn-sm btn-primary lbh see-more">
                                    <i class="fa fa-plus"></i> VOIR PLUS
                                  </a>
                              </div>`;
                          else if (paramsData["elType"] == "classifieds")
                              html +=
                              `<div class="swiper-slide">
                                  <a href="#annonces" class="btn btn-sm btn-primary lbh see-more">
                                    <i class="fa fa-plus"></i> VOIR PLUS
                                  </a>
                              </div>`; 
                          else                           
                            html +=
                                `<div class="swiper-slide">
                                  <a href="#search?types=${paramsData["elType"]}" class="btn btn-sm btn-primary lbh see-more">
                                    <i class="fa fa-plus"></i> VOIR PLUS
                                  </a>
                              </div>`;
                        }
                        carouselObj.multiCart.htmlCarousel(kunik);
                        $('.container1-' + kunik + ' .swiper-wrapper').html(html);
                    }
                );

            else{
                var i =1;
                dataCount = Object.keys(externalData).length;
                $.each(externalData, function(k, v) {
                  console.log("externalData",externalData);
                  if(exists(paramsData["cardNbResult"]) && i <= paramsData["cardNbResult"])
                    html += carouselObj.multiCart.view(v, paramsData, kunik);
                    i++;
                });
                 if(exists(paramsData["cardNbResult"]) && notEmpty(paramsData["cardNbResult"]) && dataCount > paramsData["cardNbResult"] && paramsData["buttonShowAll"]==true)
                html +=
                    `<div class="swiper-slide">
                      <a href="#search?types=${paramsData["elType"]}" class="btn btn-sm btn-primary lbh see-more">
                        <i class="fa fa-plus"></i> VOIR PLUS
                      </a>
                  </div>`;
                carouselObj.multiCart.htmlCarousel(kunik);
                $('.container1-' + kunik + ' .swiper-wrapper').html(html);            
            }

        },
        view: function(v, paramsData, kunik) {
            var html = "";
            var df = getDateFormatedCms(v);
            if (typeof v.collection == "undefined" ) 
              v.collection = v.type;
            
            html +=
                `<div class="swiper-slide">
                      <div class="row">
                        <div class="col-xs-12 el-img-container">
                            <a href="#page.type.${v.collection}.id.${v._id.$id}" class="lbh-preview-element">`;
            if (typeof v.profilMediumImageUrl != "undefined" && v.profilMediumImageUrl!=""){
                html += `<img class="img-agenda" src="${v.profilMediumImageUrl}" alt="" style="">`;
            }else if(typeof carouselObj.costumDefaultImage[kunik]!="undefined"){
                html += `<img class="img-agenda defaultImage" src="${carouselObj.costumDefaultImage[kunik]}" alt="" style="">`;
            }else{
                html += `<img class="img-agenda" src="${defaultImage}" alt="" style="">`;
            }
                  
            html += `</a>
                </div>
                        <div class="col-xs-12 el-info-container blur">
                          <div>`;
            if (exists(paramsData["infoDateFormat"]) && paramsData["infoDateFormat"] == "1") {
                if (exists(df.startDay) && exists(paramsData["showDate"]) && paramsData["showDate"]==true)
                    html += `<h6 class="title-2" style="font-size: 32px;">${df.startDay}</h6>
                      <h6 class="title-3" style="font-size: 22px;">${df.startMonth+' '+df.startYear+(exists(df.endTime) ? ' <i class="fa fa-clock-o"></i> '+df.endTime : '')}</h6>`;
            }

            html += `<h4 class="text-center el-name">
                        <a href="#page.type.${v.collection}.id.${v._id.$id}" class="lbh-preview-element event-place text-center Oswald title-6">
                        ${v.name}
                        </a>
                    </h4>`;
            if(exists(paramsData["showRoles"]) && paramsData["showRoles"]==true && exists(v.roles) && typeof v.roles == "object" && v.roles.length !=0)
                html += `<h5 class="text-center title-5">
                        ${v.roles.join(",")}
                    </h5>`;

            if (exists(paramsData["showType"]) && paramsData["showType"]==true &&  v.collection == "poi")
                html += `<h6 class="title-5">${v.type}</h6>`;

            if (v.type == "classifieds") {
                if (exists(v.price) && exists(v.devise))
                    html += `<h6 class="title-5">${v.price+' '+v.devise }</h6>`;
                if ($.inArray(v.collection, ['classifieds', 'ressources']) >= 0 && typeof v.category != 'undefined') {
                    html += `<h6 class="title-4">`;
                    html += tradCategory[v.section];
                    if (typeof v.category != 'undefined' && v.type != 'poi') html += ' > ' + tradCategory[v.category];
                    subtype = (typeof v.subtype != 'undefined' && typeof tradCategory[v.subtype] != 'undefined') ? ' > ' + tradCategory[v.subtype] : ' > ' + v.subtype;
                    html += subtype;
                    html += `</h6>`;
                }
            }

            if (exists(paramsData["showAdress"]) && paramsData["showAdress"]==true && exists(v.address))
                html += `<h5 class="text-center">
                                  <a href="#page.type.${v.collection}.id.${v._id.$id}" class="lbh-preview-element event-place text-center Oswald other">
                                  ${exists(v.address.level4Name) ? "<i class='fa fa-map-marker'></i> " + v.address.level4Name : ""}
                                    ${exists(v.address.streetAddress) ? v.address.streetAddress :"" }
                                  </a>
                              </h5>`;

            if (exists(paramsData["infoDateFormat"]) && paramsData["infoDateFormat"] == "2" && exists(paramsData["showDate"]) && paramsData["showDate"]==true) {
                if (exists(df.startDay))
                    html += `<h6 class="title-5">${df.startLbl+' '+df.startDayNum+' '+df.startDay+' '+df.startMonth+' '+df.startYear+' <i class="fa fa-clock-o"></i> '+df.startTime}</h6>`;
                if (exists(df.endDay))
                    html += `<h6 class="title-5">${df.endLbl+' '+df.endDayNum+' '+df.endDay+' '+df.endMonth+' '+df.endYear+' <i class="fa fa-clock-o"></i> '+df.endTime}</h6>`;
            }
            if (exists(paramsData["showShortDescription"]) && paramsData["showShortDescription"]==true && exists(v.shortDescription))
                html += `<p class="title-3">${v.shortDescription != null ? v.shortDescription : ""}</p>`;

            if (exists(paramsData["showDescription"]) && paramsData["showDescription"]==true && exists(v.description))
                html += `<p class="title-3">${v.description != null ? v.description : ""}</p>`;
            if (isInterfaceAdmin) {
                html += `<button class="btn btn-sm btn-success edit-element-${kunik} hiddenPreview" data-id="${v._id.$id}">
                          <i class="fa fa-edit"></i>
                        </button>
                        <button class="btn btn-sm btn-danger delete-element-${kunik} hiddenPreview" data-id="${v._id.$id}">
                          <i class="fa fa-trash"></i>
                        </button>`;
            }

            html += `</div>
                        </div>
                      </div>
                  </div>`;
            return html;
        },
        htmlCarousel: function(kunik) {
            var html =
            `<div class="container1-${kunik} col-md-12">
                <div id="filtersContainer${kunik}"></div>
                <div class="swiper-container">
                  <div class="swiper-wrapper">
                    <div style="position: relative;width:100%;height: 100%">
                      <p class="text-center"><i class="fa fa-spinner fa-spin fa-5x"></i></p>
                    </div>
                  </div>
                  <div class="swiper-pagination"></div>
                  <div class="swiper-button-next"></div>
                  <div class="swiper-button-prev"></div>
                </div>
            </div>`;
            $('.block-container-' + kunik).append(html);
        },
        cssCarousel: function(paramsData, kunik) {
            var containerKunik = ".container1-" + kunik;
            var css = `<style>
          ${containerKunik} .swiper-container {
                width: 100%;
                height: 100%;
                padding-bottom: 50px;
                padding-top: 5px;
           }
          ${containerKunik} .swiper-slide {
              text-align: center;
              font-size: 18px;
              background: transparent;
              display: -webkit-box;
              display: -ms-flexbox;
              display: -webkit-flex;
              display: flex;
              -webkit-box-pack: center;
              -ms-flex-pack: center;
              -webkit-justify-content: center;
              justify-content: center;
              -webkit-box-align: center;
              -ms-flex-align: center;
              -webkit-align-items: center;
              align-items: center;
              }`;

            if (exists(paramsData["elDesign"]) && (paramsData["elDesign"] == 3 || paramsData["elDesign"] == 5)) {
                css += `.container1-${kunik} .swiper-pagination,
                  .container1-${kunik} .swiper-button-next,
                  .container1-${kunik} .swiper-button-prev{
                      display:none
                  }

                  .container1-${kunik} .swiper-wrapper{
                    flex-wrap : wrap;
                    justify-content: space-evenly;                    
                  }
                  ${containerKunik} .swiper-slide{
                    margin-right: 0px !important;
                  }
                  @media (max-width:400px){
                    ${containerKunik} .swiper-slide{
                        flex-basis : calc(95%/ ${exists(paramsData["cardNumberPhone"]) ? paramsData["cardNumberPhone"] : 1}) !important;
                    }
                  }
                  @media (max-width:768px){
                    ${containerKunik} .swiper-slide{
                        flex-basis : calc(95%/ ${exists(paramsData["cardNumberTablet"]) ? paramsData["cardNumberTablet"] : 2}) !important;
                    }
                  }
                  @media (min-width:769px){
                    ${containerKunik} .swiper-slide{
                        flex-basis : calc(95%/ ${exists(paramsData["cardNumberDesktop"]) ? paramsData["cardNumberDesktop"] : 3}) !important;
                    }
                  }
                  `;
            }

            css += `
              .toolsMenu a{ display: none }
              ${containerKunik} .description{
                color: white;
                padding:0 5px 0 5px;
                overflow: hidden;
                text-overflow: ellipsis;
                display: -webkit-box;
                -webkit-line-clamp: 2; /* number of lines to show */
                -webkit-box-orient: vertical;
                font-size: 13px;
                margin-top: 20px;
              }
              ${containerKunik} h3{
                  padding:0 10px 0 10px !important;
              }
              ${containerKunik} .add-event-caroussel{
                visibility: hidden
              }
              ${containerKunik}:hover .add-event-caroussel{
                visibility: visible;
              }
              ${containerKunik} .img-agenda,${containerKunik} .swiper-slide img{
                  position: relative;
                  width: ${exists(paramsData["imgWidth"]) ? paramsData["imgWidth"]+" !important" : "100% !important" };
                  height: ${exists(paramsData["imgHeight"]) ? paramsData["imgHeight"]+" !important" : "auto !important"};
                  border-radius: ${exists(paramsData["imgRound"]) ? paramsData["imgRound"] : "0%"  };
                  display: ${(exists(paramsData["imgShow"]) && paramsData["imgShow"] == true ) ? "initial"  : "none" };
                  border : ${exists(paramsData["imgBorder"]) ? paramsData["imgBorder"] : "0px"} solid ${exists(paramsData["imgBorderColor"]) ? paramsData["imgBorderColor"] : "#ffffff"};
                  object-fit: cover;
                  object-position: center;
                  ${(exists(paramsData["elDesign"]) && (paramsData["elDesign"] != 4 && paramsData["elDesign"] != 5)) ? "z-index:15;" : ""}
              }
              ${containerKunik} .el-info-container > div{
                position:relative;
                margin: 0 auto;
                margin: ${exists(paramsData["infoDistanceFromImage"]) ? paramsData["infoDistanceFromImage"] : "-14px" } 0 0 0;
                background: ${exists(paramsData["infoBg"]) ? paramsData["infoBg"] : "" } ;
                margin-left: ${exists(paramsData["infoMarginLeft"]) ? paramsData["infoMarginLeft"] :"" } ;
                margin-right: ${exists(paramsData["infoMarginRight"]) ? paramsData["infoMarginRight"] :"" } ;
                border-radius: ${exists(paramsData["infoBorderRadius"]) ? paramsData["infoBorderRadius"] :"0%" }; 
                height : ${exists(paramsData["infoHeight"]) ? paramsData["infoHeight"] :"auto" };
                padding: 5px;
                ${(exists(paramsData["elDesign"]) && (paramsData["elDesign"] != 4 && paramsData["elDesign"] != 5)) ? "z-index:15;" : ""}
              }
              ${containerKunik} .swiper-slide > .row:before,${containerKunik} .swiper-slide > .row:after{
                width:100%;height:100%;background:rgba(11,33,47,.9);position:absolute;top:0;left:0;opacity:0;transition:all .5s ease 0s
              }
              ${containerKunik} .swiper-slide > .row{
                margin-left: 0;
                margin-right: 0;
                box-shadow: ${(exists(paramsData["cardBoxShadow"]) && paramsData["cardBoxShadow"]== true )  ? "0 2px 5px 0 #3f4e58, 0 2px 10px 0 #3f4e58" : "" };
                background: ${exists(paramsData["cardBg"]) ? paramsData["cardBg"] : "" }; 
              }
              ${containerKunik} .swiper-slide >.row>.col-xs-12{
                padding-left: 0;
                padding-right: 0;
                cardBoxShadow : ${(exists(paramsData["cardBoxShadow"]) && paramsData["cardBoxShadow"]== true ) ? "padding-top: 0px;" : "padding-top: 10px;"}
                background: ${exists(paramsData["imgBg"]) ? paramsData["imgBg"] : "" }; 
                
              }`;
          if(exists(paramsData["infoOnHover"]) && paramsData["infoOnHover"] == true)
            css+=`
                ${containerKunik} .el-info-container > div > * { 
                  display: none; 
                }
                ${containerKunik} .swiper-slide .el-name{
                    display:block !important;
                    position: relative;
                    z-index: 99;
                }
                ${containerKunik} .el-info-container{
                    position: absolute;
                    height: 100%;
                    visibility:visible;
                    background: ${exists(paramsData["infoBg"]) ? paramsData["infoBg"] : "" } ;
                    z-index: 99;
                    top:100%;
                    transition: all .5s ease;
                    -moz-transition: all .5s ease;
                    -webkit-transition: all .5s ease; 
                }
                ${containerKunik} .swiper-slide:hover .el-info-container{
                    top:0;
                    height: 100%;
                    visibility:visible;
                    transition: all .5s ease;
                    -moz-transition: all .5s ease;
                    -webkit-transition: all .5s ease;                  
                }
                ${containerKunik} .swiper-slide:hover .el-info-container> div > * { 
                  display: block !important; 
                }
                ${containerKunik} .el-info-container > div{
                    height : 100% !important;
                    margin:0px !important;
                }
                ${containerKunik} .swiper-slide > .row{
                    position : relative
                }`;


          css+= `${containerKunik} .edit-element-${kunik},${containerKunik} .delete-element-${kunik}{
                display: none
              }
              ${containerKunik} .swiper-slide:hover .edit-element-${kunik}{
                position : absolute;
                left:10px;
                bottom:5px;
                display: initial;
                z-index:99;
              }
              ${containerKunik} .swiper-slide:hover .delete-element-${kunik}{
                position : absolute;
                left:57px;
                bottom:5px;
                display: initial;
                z-index:99;
              }
              @media (max-width: 420px){
                ${containerKunik} .img-agenda{
                    width: 100%;
                    height: auto;
                    border-radius: 0%;
                    object-fit: contain;
                    object-position: center;
                }
                ${containerKunik} .el-info-container > div{
                  border-radius: 0px;
                }
              }
              ${containerKunik} .see-more{
                line-height: 90px;
                border-radius: 50%;
               margin-top : 82px;
              }
            </style>`;
            $('head').append(css);
        }
    },


    //******************SECOND DESIGN*************************/  
    cardBlock: {
        init: function(paramsData, kunik) {
            var $this = this;
            $this.cssCarousel(paramsData, kunik);
            if ($(".container2-" + kunik + " .swiper-wrapper").length <= 0) {
                $this.fetchData(paramsData, kunik);
            }
            var carOption = {
                xs: 1,
                md: 1,
                lg: 1,
            }

            if (exists(paramsData["elDesign"]) && paramsData["elDesign"] != 3)
                carOption["container"] = ".container2-" + kunik + " .swiper-container";

            setTimeout(function() {
                carouselObj.bindEvents(paramsData, kunik);
                carouselObj.initCarousel(paramsData, carOption);
                carouselObj.toSameHeight(".container2-" + kunik + " .el-info-container>div");
            }, 2500)
            console.log("carOptionko", carOption)
        },

        fetchData: function(paramsData, kunik) {
            var html = "";
            var dataCount = 0;
            params = {
                searchType: [paramsData["elType"]]
            };
            // if (exists(paramsData["cardNbResult"]) && paramsData["cardNbResult"] != "")
            //     params["indexStep"] = paramsData["cardNbResult"]
            ajaxPost(
                null,
                baseUrl + "/" + moduleId + "/search/globalautocomplete",
                params,
                function(data) {
                    dataCount = Object.keys(data.results).length;
                    var i = 1;
                    $.each(data.results, function(k, v) {
                        if(exists(paramsData["cardNbResult"]) && i <= paramsData["cardNbResult"])
                          html += carouselObj.cardBlock.view(v, paramsData, kunik);
                        i++;
                    });
                    if(exists(paramsData["cardNbResult"]) && notEmpty(paramsData["cardNbResult"]) && dataCount > paramsData["cardNbResult"] && paramsData["buttonShowAll"] ==true)
                      html +=
                          `<div class="swiper-slide">
                              <a href="#search?types=${paramsData["elType"]}" class="btn btn-sm btn-primary lbh see-more">
                                <i class="fa fa-plus"></i> VOIR PLUS
                              </a>
                          </div>`;
                    carouselObj.cardBlock.htmlCarousel(kunik);
                    $('.container2-' + kunik + ' .swiper-wrapper').html(html);
                    document.querySelectorAll('img.lzy_img').forEach((v) => {
                        imageObserver.observe(v);
                    });
                }
            )
        },
        view: function(v, paramsData, kunik) {
            var $this = this;
            var df = getDateFormatedCms(v);
            var html = "";
            html += `
                <div class="swiper-slide">
                      <div class="swiper-slide-container">
                          <div class="col-info">
                            <h4 class="text-center">
                                <a href="#page.type.${v.collection}.id.${v._id.$id}" class="lbh-preview-element event-place text-center  title-6">
                                ${v.name}
                                </a>
                            </h4>`;

            if (v.type == "classifieds") {
                if (exists(v.price) && exists(v.devise))
                    html += `<h6 class="title-5">${v.price+' '+v.devise }</h6>`;
                if ($.inArray(v.collection, ['classifieds', 'ressources']) >= 0 && typeof v.category != 'undefined') {
                    html += `<h6 class="title-3">`;
                    html += tradCategory[v.section];
                    if (typeof v.category != 'undefined' && v.type != 'poi') html += ' > ' + tradCategory[v.category];
                    subtype = (typeof v.subtype != 'undefined' && typeof tradCategory[v.subtype] != 'undefined') ? ' > ' + tradCategory[v.subtype] : ' > ' + v.subtype;
                    html += subtype;
                    html += `</h6>`;
                }
            }

            if (exists(df.startDay) && exists(paramsData["showDate"]) && paramsData["showDate"]==true)
                html += `<h6 class="title-5">${df.startLbl+' '+df.startDayNum+' '+df.startDay+' '+df.startMonth+' '+df.startYear+' <i class="fa fa-clock-o"></i> '+df.startTime}</h6>`;
            if (exists(df.endDay) && exists(paramsData["showDate"]) && paramsData["showDate"]==true)
                html += `<h6 class="title-5">${df.endLbl+' '+df.endDayNum+' '+df.endDay+' '+df.endMonth+' '+df.endYear+' <i class="fa fa-clock-o"></i> '+df.endTime}</h6>`;
            
            if (exists(paramsData["showShortDescription"]) && paramsData["showShortDescription"]==true && exists(v.shortDescription))
                html += `<p class="title-3">${v.shortDescription != null ? v.shortDescription : ""}</p>`;

            if (exists(paramsData["showDescription"]) && paramsData["showDescription"]==true && exists(v.description))
                html += `<p class="title-3">${v.description != null ? v.description : ""}</p>`;

            if (exists(paramsData["showAdress"]) && paramsData["showAdress"]==true && exists(v.address))
                html += `<h5 class="text-center">
                              <a href="#page.type.${v.collection}.id.${v._id.$id}" class="lbh-preview-element event-place text-center Oswald other">
                              ${exists(v.address.level4Name) ? "<i class='fa fa-map-marker'></i> " + v.address.level4Name : ""}
                                ${exists(v.address.streetAddress) ? v.address.streetAddress :"" }
                              </a>
                          </h5>`;

            if (isInterfaceAdmin) {
                html += `<button class="btn btn-sm btn-success edit-element-${kunik} hiddenPreview" data-id="${v._id.$id}">
                                        <i class="fa fa-edit"></i>
                                      </button>
                                  <button class="btn btn-sm btn-danger delete-element-${kunik} hiddenPreview" data-id="${v._id.$id}">
                                        <i class="fa fa-trash"></i>
                                      </button>`;
            }

            html += `</div>
                        <div class="col-img">
                            <a href="#page.type.${v.collection}.id.${v._id.$id}" class="lbh-preview-element">`;
            if (typeof v.profilMediumImageUrl != "undefined" && v.profilMediumImageUrl!=""){
                html += `<img class="img-agenda" src="${v.profilMediumImageUrl}" alt="" style="">`;
            }else if(typeof carouselObj.costumDefaultImage[kunik]!="undefined"){
                html += `<img class="img-agenda defaultImage" src="${carouselObj.costumDefaultImage[kunik]}" alt="" style="">`;
            }else{
                html += `<img class="img-agenda" src="${defaultImage}" alt="" style="">`;
            }
            html += `</a>
                        </div>
                      </div>
                  </div>`;
            return html;
        },
        htmlCarousel: function(kunik) {
            var html =
            `<div class="container2-${kunik} col-md-12">
              <div id="filtersContainer${kunik}"></div>
              <div class="swiper-container">
                  <div class="swiper-wrapper">

                  </div>
                  <div class="swiper-pagination"></div>
              </div>
            </div>`;
            $('.block-container-' + kunik).append(html);
        },
        cssCarousel: function(paramsData, kunik) {
            var css = `<style>
            .container2-${kunik} .swiper-container {
              width: 100%;
              height: 100%;
            }
            .container2-${kunik} .swiper-slide {
              text-align: center;
              font-size: 18px;
              background: transparent;
              display: -webkit-box;
              display: -ms-flexbox;
              display: -webkit-flex;
              display: flex;
              -webkit-box-pack: center;
              -ms-flex-pack: center;
              -webkit-justify-content: center;
              justify-content: center;
              -webkit-box-align: center;
              -ms-flex-align: center;
              -webkit-align-items: center;
              align-items: center;
            }`;

            if(exists(paramsData["elDesign"]) && (paramsData["elDesign"] == 6)){
                css +=
                `.container2-${kunik} .swiper-wrapper {
                    flex-direction:column;
                    flex-wrap:wrap;"
                }
                .container2-${kunik} .swiper-pagination,
                .container2-${kunik} .swiper-button-next,
                .container2-${kunik} .swiper-button-prev{
                    display:none
                }
                .container2-${kunik} .swiper-slide{
                  margin-bottom:7px
                }`;              
            }

            css +=`.container2-${kunik} .swiper-slide-container{
              display: flex;
              flex-direction: row;
              width: 100%;
              box-shadow: ${(exists(paramsData["cardBoxShadow"]) && paramsData["cardBoxShadow"]== true )  ? "0 2px 5px 0 #3f4e58, 0 2px 10px 0 #3f4e58" : "" };
              height : ${exists(paramsData["infoHeight"]) ? paramsData["infoHeight"] :"auto" };
            }
            .container2-${kunik} .col-info{
              flex-basis: ${(exists(paramsData["imgShow"]) && paramsData["imgShow"]== false) ? "100%" : "60%" };
              padding: 5px;
              display: flex;
              flex-direction: column;
              align-items: ${exists(paramsData["infoAlign"]) ? paramsData["infoAlign"] : "center" };
              justify-content: ${(exists(paramsData["infoCenter"]) && paramsData["infoCenter"] == true) ? "center" : "" };
              order: ${(exists(paramsData["infoOrder"]) && paramsData["infoOrder"] == true) ? 1 : 0 }; 
              background: ${exists(paramsData["infoBg"]) ? paramsData["infoBg"] : "#f0ad16" };
            }
            .container2-${kunik} .col-img{
              flex-basis: 40%;
              background : background: ${exists(paramsData["imgBg"]) ? paramsData["imgBg"] : "" }; 
              display: ${(exists(paramsData["imgShow"]) && paramsData["imgShow"] == true ) ? "initial"  : "none" };
            }
            .container2-${kunik} .col-img img {
              object-fit: cover;
              width: ${exists(paramsData["imgWidth"]) ? paramsData["imgWidth"]+" !important" : "100% !important" };
              height: ${exists(paramsData["imgHeight"]) ? paramsData["imgHeight"]+" !important" : "auto !important"};
              border-radius: ${exists(paramsData["imgRound"]) ? paramsData["imgRound"] : "0%"  };
            }
          .container2-${kunik} .edit-element-${kunik},.container2-${kunik} .delete-element-${kunik}{
            display: none
          }
          .container2-${kunik} .swiper-slide:hover .edit-element-${kunik},.container2-${kunik} .swiper-slide:hover .delete-element-${kunik}{
            display: initial;
          }
          @media (max-width: 957px){
            .container2-${kunik} .swiper-slide-container{
              display: flex;
              flex-direction: column;
            }
            .container2-${kunik} .col-info{
              order: 2;
            }
            .container2-${kunik} .col-img{
              order: 1;
            }
          }
          .container2-${kunik} .see-more{
            line-height: 90px;
            border-radius: 50%;
           margin-top : 82px;
          }
        </style>`;
            $('head').append(css);
        },
    },
    initDesign3: function() {

    }
}