var widthLabel = {
  "modeLg":tradCms.largemode,
  "modeMd":tradCms.desktopmode,
  "modeSm":tradCms.tabletmode,
  "modeXs":tradCms.phonemode
},marginLabel = {
  "marginTop":tradCms.top,
  "marginBottom":tradCms.bottom,
  /*"marginLeft":"Gauche",
  "marginRight":"Droite"*/
},paddingLabel = {
  "paddingTop":tradCms.top,
  "paddingBottom":tradCms.bottom,
  "paddingLeft":tradCms.left,
  "paddingRight":tradCms.right
},
blockCmsBgLabel = {
"blockCmsBgTarget" : tradCms.targetblc,
"blockCmsBgType" : trad.type,
"blockCmsBgColor": tradCms.color,
"blockCmsBgImg"  : trad.image,
"blockCmsBgPaint": tradCms.paintedblc,
"blockCmsBgSize"  : tradCms.size,
"blockCmsBgPosition"  : tradCms.position,
"blockCmsBgRepeat"  : tradCms.repeat,
},
blockCmsPoliceLabel = {
  "blockCmsPoliceTitle1" : tradCms.title+" 1",
  "blockCmsPoliceTitle2" : tradCms.title+" 2",
  "blockCmsPoliceTitle3" : tradCms.title+" 3",
  "blockCmsPoliceTitle4" : tradCms.title+" 4",
  "blockCmsPoliceTitle5" : tradCms.title+" 5",
  "blockCmsPoliceTitle6" : tradCms.title+" 6",
},
blockCmsTextSizeLabel = {
"blockCmsTextSizeTitle1" : tradCms.title+" 1 (px)",
"blockCmsTextSizeTitle2" : tradCms.title+" 2 (px)",
"blockCmsTextSizeTitle3" : tradCms.title+" 3 (px)",
"blockCmsTextSizeTitle4" : tradCms.title+" 4 (px)",
"blockCmsTextSizeTitle5" : tradCms.title+" 5 (px)",
"blockCmsTextSizeTitle6" : tradCms.title+" 6 (px)",
},
blockCmsTextLineHeightLabel = {
"blockCmsTextLineHeightTitle1" : tradCms.title+" 1",
"blockCmsTextLineHeightTitle2" : tradCms.title+" 2",
"blockCmsTextLineHeightTitle3" : tradCms.title+" 3",
"blockCmsTextLineHeightTitle4" : tradCms.title+" 4",
"blockCmsTextLineHeightTitle5" : tradCms.title+" 5",
"blockCmsTextLineHeightTitle6" : tradCms.title+" 6",
},
blockCmsTextUnderlineLabel = {
"blockCmsUnderlineTitle1": tradCms.title+" 1 (T1)",
"blockCmsUnderlineTitle2": tradCms.title+" 2 (T2)",
"blockCmsUnderlineTitle3": tradCms.title+" 3 (T3)",
"blockCmsUnderlineTitle4": tradCms.title+" 4 (T4)",
"blockCmsUnderlineTitle5": tradCms.title+" 5 (T5)",
"blockCmsUnderlineTitle6": tradCms.title+" 6 (T6)",

"blockCmsUnderlineColorTitle1" : tradCms.color+" T1",
"blockCmsUnderlineColorTitle2" : tradCms.color+" T2",
"blockCmsUnderlineColorTitle3" : tradCms.color+" T3",
"blockCmsUnderlineColorTitle4" : tradCms.color+" T4",
"blockCmsUnderlineColorTitle5" : tradCms.color+" T5",
"blockCmsUnderlineColorTitle6" : tradCms.color+" T6",

"blockCmsUnderlineWidthTitle1" : tradCms.size+" T1",
"blockCmsUnderlineWidthTitle2" : tradCms.size+" T2",
"blockCmsUnderlineWidthTitle3" : tradCms.size+" T3",
"blockCmsUnderlineWidthTitle4" : tradCms.size+" T4",
"blockCmsUnderlineWidthTitle5" : tradCms.size+" T5",
"blockCmsUnderlineWidthTitle6" : tradCms.size+" T6",


"blockCmsUnderlineHeightTitle1" : tradCms.height+" T1",
"blockCmsUnderlineHeightTitle2" : tradCms.height+" T2",
"blockCmsUnderlineHeightTitle3" : tradCms.height+" T3",
"blockCmsUnderlineHeightTitle4" : tradCms.height+" T4",
"blockCmsUnderlineHeightTitle5" : tradCms.height+" T5",
"blockCmsUnderlineHeightTitle6" : tradCms.height+" T6",

"blockCmsUnderlineSpaceTitle1" : tradCms.spacebetween+" T1",
"blockCmsUnderlineSpaceTitle2" : tradCms.spacebetween+" T2",
"blockCmsUnderlineSpaceTitle3" : tradCms.spacebetween+" T3",
"blockCmsUnderlineSpaceTitle4" : tradCms.spacebetween+" T4",
"blockCmsUnderlineSpaceTitle5" : tradCms.spacebetween+" T5",
"blockCmsUnderlineSpaceTitle6" : tradCms.spacebetween+" T6",

"blockCmsUnderlineMargeBottomTitle1" : tradCms.marginbottom+" T1",
"blockCmsUnderlineMargeBottomTitle2" : tradCms.marginbottom+" T2",
"blockCmsUnderlineMargeBottomTitle3" : tradCms.marginbottom+" T3",
"blockCmsUnderlineMargeBottomTitle4" : tradCms.marginbottom+" T4",
"blockCmsUnderlineMargeBottomTitle5" : tradCms.marginbottom+" T5",
"blockCmsUnderlineMargeBottomTitle6" : tradCms.marginbottom+" T6",
},
blockCmsBorderLabel = {
  "blockCmsBorderTop" : tradCms.top,
  "blockCmsBorderBottom" : tradCms.bottom,
  "blockCmsBorderLeft" : tradCms.left,
  "blockCmsBorderRight": tradCms.right,
  "blockCmsBorderColor": tradCms.color,
  "blockCmsBorderWidth": tradCms.thickness,
  "blockCmsBorderType": trad.type
},
blockCmsLineSeparatorLabel = {
  "blockCmsLineSeparatorTop" : tradCms.top,
  "blockCmsLineSeparatorBottom" : tradCms.bottom,
  "blockCmsLineSeparatorWidth" : tradCms.length+"(%)",
  "blockCmsLineSeparatorHeight" : tradCms.height,
  "blockCmsLineSeparatorBg" : tradCms.color,
  "blockCmsLineSeparatorPosition" : tradCms.position,
  "blockCmsLineSeparatorIcon" : tradCms.smalliconinthemiddle+" :"
},
blockCmsTextAlignLabel = {
"blockCmsTextAlignTitle1" : tradCms.title+" 1",
"blockCmsTextAlignTitle2" : tradCms.title+" 2",
"blockCmsTextAlignTitle3" : tradCms.title+" 3",
"blockCmsTextAlignTitle4" : tradCms.title+" 4",
"blockCmsTextAlignTitle5" : tradCms.title+" 5",
"blockCmsTextAlignTitle6" : tradCms.title+" 6",

},
blockCmsColorLabel = {
  "blockCmsColorTitle1" : tradCms.title+" 1",
  "blockCmsColorTitle2" : tradCms.title+" 2",
  "blockCmsColorTitle3" : tradCms.title+" 3",
  "blockCmsColorTitle4" : tradCms.title+" 4",
  "blockCmsColorTitle5" : tradCms.title+" 5",
  "blockCmsColorTitle6" : tradCms.title+" 6",
},
blockCmsScrollAnimation = {
  "blockCmsAos" : "Animation",
  "blockCmsAosDuration" : "Duration",
},
checkboxSimpleParams={
  "onText" : trad.yes,
  "offText" : trad.no,
  "onLabel" : trad.yes,
  "offLabel" : trad.no,
  "labelText" : tradCms.label
}

var elTypeOptions = {
    "poi" : trad.poi,
    "events" : trad.events,
    "projects" : trad.project,
    "ressources" : trad.ressources,
    "classifieds" : trad.classifieds,
    "organizations" : trad.organization,
    "citoyens" :trad.members
};
var aosAnimation = {
  "fade-up": tradCms.fadeup,
  "fade-down": tradCms.fadedown,
  "fade-right": tradCms.faderight,
  "fade-left": tradCms.fadeleft,
  "fade-up-right": tradCms.fadeupright,
  "fade-up-left": tradCms.fadeupleft,
  "fade-down-right": tradCms.fadedownright,
  "fade-down-left": tradCms.fadedownleft,
  "flip-left": tradCms.flipleft,
  "flip-right": tradCms.flipright,
  "flip-up": tradCms.flipup,
  "flip-down": tradCms.flipdown,
  "zoom-in": tradCms.zoomin,
  "zoom-in-up": tradCms.zoominup,
  "zoom-in-down": tradCms.zoomindown,
  "zoom-in-left": tradCms.zoominleft,
  "zoom-in-right": tradCms.zoominright,
  "zoom-out": tradCms.zoomout,
  "zoom-out-up": tradCms.zoomoutup,
  "zoom-out-down": tradCms.zoomoutdown,
  "zoom-out-right": tradCms.zoomoutright,
  "zoom-out-left": tradCms.zoomoutleft
}

var poiOptions = {};
$.each(poi.filters,function(k,v){
  poiOptions[k] = typeof trad[k] != "undefined" ? trad[k] : k  ;
})

var fieldsetClass=[];
function lazyWelcomeDyFObj(time){
    if(typeof dyFObj != "undefined" && typeof dyFObj.openForm != "undefined")
       surchargeDyFObj();
    else
      setTimeout(function(){
        lazyWelcomeDyFObj(time+200)
      }, time);
}

/*surcharge openForm*/
function surchargeDyFObj(){
  dyFObj.openForm = function  (type, afterLoad,data, isSub, dynFormCostumIn,mode) { 
      dyFObj.dynFormCostum = null; //was some times persistant between forms 
      mylog.warn("openForm","--------------- Open Form ",type, afterLoad,data, isSub, dynFormCostumIn);
      $.unblockUI();
      $("#openModal").modal("hide");
      
      mylog.dir(data);
      uploadObj.contentKey="profil"; 
      if(notNull(data)){
        if(typeof data.images != "undefined")
          uploadObj.initList.image=data.images;
        if(typeof data.files != "undefined" )
          uploadObj.initList.file=data.files;

        data = dyFObj.prepData(data);

      }else{
        uploadObj.initList={};
      }
      dyFObj.activeElem = (isSub) ? "subElementObj" : "elementObj";
      dyFObj.activeModal = (isSub) ? "#openModal" : "#ajax-modal";

      if( typeof dynFormCostumIn != "undefined"){
        mylog.warn("openForm", "dynFormCostum",dynFormCostumIn);
        dyFObj.dynFormCostum = dynFormCostumIn;
      } else {
        mylog.warn("openForm", "no dynFormCostum");
      }

      if(notNull(finder))
        finder.initVar();

      if(notNull(scopeObj))
        scopeObj.selected = {};

      dyFInputs.locationObj.initVar();

      if(dyFObj.unloggedMode || userId)
      {
        if(typeof formInMap != 'undefined')
          formInMap.formType = type;

        if(typeof dyFObj.formInMap != 'undefined')
          dyFObj.formInMap.formType = type;

        dyFObj.getDynFormObj(type, function() { 
          dyFObj.startBuild(afterLoad,data);
        },afterLoad, data, dynFormCostumIn);
      } else {
        dyFObj.openFormAfterLogin = {
          type : type, 
          afterLoad : afterLoad,
          data : data
        };
        toastr.error(tradDynForm.mustbeconnectforcreateform);
        $('#modalLogin').modal("show");
      }

      if (typeof type.jsonSchema != "undefined" && type.jsonSchema.properties != "undefined" && isInterfaceAdmin) {
        //setTimeout(function(){
          createFieldsets(type.jsonSchema.properties);
          $(".fieldsetblockCmsPolice").before(`<div class="col-xs-12">
            <button type="button" class="btn  bg-green-k text-white more-config"><i class="fa fa-arrow-down"></i>&nbsp;`+tradCms.moreconfig+` <i class="fa fa-arrow-down"></i></button>
            <button type="button" class="btn  btn-block bg-green-k text-white less-config" style='display:none'><i class="fa fa-arrow-up"></i>&nbsp;`+tradCms.hide+` <i class="fa fa-arrow-up"></i></button>
            </div>`);
          $('.more-config').click(function(){
            $(this).hide();
            $('.less-config').show();
            $('.fieldset.toHide').css({
              "margin": "15px 0 15px 0",
              "padding":"15px 0px 15px 0",
              "visibility" :"visible",
              "height" : "auto", 
            });
          })
          $('.less-config').click(function(){
            $(this).hide();
            $('.more-config').show();
            $('.fieldset.toHide').css({
              "margin": "0px",
              "padding":"0px",
              "visibility" :"hidden",
              "height" : "0px",
            });
          })

          //toogle background choice
          $('#blockCmsBgType').click(function(){
            if($(this).val() == 'color'){
              $(".blockCmsBgColorcolorpicker").fadeIn("slow");
              $(".blockCmsBgPaintselect,.blockCmsBgSizeselect,.blockCmsBgPositionselect,.blockCmsBgRepeatselect").fadeOut("slow");
              $(".blockCmsBgImguploader").css({
                  'visibility':'hidden',
                  'height' : '0px'
              });
            }
            if($(this).val() == 'image'){
              $(".blockCmsBgImguploader").css({
                  'display':'block',
                  'visibility':'visible',
                  'height' : 'auto'
              });
              $(".blockCmsBgColorcolorpicker,.blockCmsBgPaintselect").fadeOut("slow");
              $('.blockCmsBgSizeselect,.blockCmsBgPositionselect,.blockCmsBgRepeatselect').fadeIn("slow");
            }
            if($(this).val() == 'paint'){
              $(".blockCmsBgPaintselect,.blockCmsBgSizeselect,.blockCmsBgPositionselect,.blockCmsBgRepeatselect").fadeIn("slow");
              $(".blockCmsBgColorcolorpicker").fadeOut("slow");
              $(".blockCmsBgImguploader").css({
                  'visibility':'hidden',
                  'height' : '0px'
              });
            }
          })

          
          $("select[id^='blockCmsPoliceTitle'],select[id*=' blockCmsPoliceTitle']").append(fontOptions);
          $('#blockCmsLineSeparatorIcon').append(fontAwesomeOptions);
          $(".fieldset.fieldsetmode").append("<div class='info tooltips' data-toggle='tooltip' data-placement='left' data-html='true' data-original-title='- "+tradCms.rowandhalf+"'><i class='fa fa-question-circle'></i></div>");
          $('.tooltips').tooltip();
          paperPaintObj.init();

        //},1000)   
      }
    }
}

function deleteBlockNotFound(id,type){
    $(this).empty().html('<i class="fa fa-spinner fa-spin"></i>');
    var btnClick = $(this);
    var urlToSend = baseUrl+"/"+moduleId+"/cms/delete/id/"+id;

    bootbox.confirm(tradCms.confirmdeletblock+" ?",
    function(result)
    {
      if(result==true)
          ajaxPost(
            null,
            urlToSend,
            {},
            function(data){ 
                if (data.result ) {
                    toastr.success(tradCms.Blocksuccessfullydeleted);
                    $("#"+id).remove();
                    urlCtrl.loadByHash(location.hash);
                } else {
                     toastr.error(tradCms.erroroccurred);
                }
            }
          );
    });
}

function sortBlock(classItem){
  setTimeout(function(){
    $(classItem).wrapAll('<div id="sortable"></div>');
        $("#sortable" ).sortable({
            handle : ".handleDrag",
            placeholder: "ui-state-highlight",
            stop: function(event, ui) {
              $('.block-parent').css("border","0px solid black");
              var idAndPosition = $(this).sortable('toArray');
              $.ajax({
                  type : 'POST',
                  data : {
                    id : thisContextId,
                    collection : thisContextType,
                    idAndPosition : idAndPosition
                  },
                  url : baseUrl+"/costum/blockcms/dragblock",
                  success : function(data){
                    if(data.result)
                      toastr.success("Bien déplacé");
                    else
                      toastr.success(trad.somethingwrong);
                  }
              });
            },
            sort: function(event,ui){
                $('.block-parent').css("border","2px dotted red")
            }
        });
        //*$( "#sortable" ).disableSelection();
    },900);
}

var paperPaintObj = {
  init : function(){
      this.append(function(){
        $("#blockCmsBgPaint").imagepicker({
          hide_select : false,
          show_label  : true
        });
      })
  },
  append : function(callback){
    $("#blockCmsBgPaint").addClass("show-html").append(`
    `);

    $('.more-config').click(function(){
        $(".blockCmsBgPaintselect").animate({
            scrollTop: ((exists($(".thumbnail.selected").offset()) && exists($(".thumbnail.selected").offset().top)) ? $(".thumbnail.selected").offset().top :null)
        }, 1000);
    })

    if(typeof callback == "function")
      callback();
  }
}

  function getDateFormatedCms(params, onlyStr, allInfos){
    var dateObj = {};
    if(exists(params.recurrency != 'undefined') && params.recurrency){
      dateObj.initOpeningHours = initOpeningHoursCms(params, allInfos);
      return dateObj;
    }else{
      params.startDateDB = notEmpty(params.startDate) ? params.startDate : null;
      params.startDay = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('DD') : '';
      params.startMonth = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('MM') : '';
      params.startYear = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('YYYY') : '';
      params.startDayNum = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('d') : '';
      params.startTime = notEmpty(params.startDate) ? moment(params.startDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('HH:mm') : '';
        
      params.endDateDB = notEmpty(params.endDate) ? params.endDate: null;
      params.endDay = notEmpty(params.endDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('DD') : '';
      params.endMonth = notEmpty(params.endDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('MM') : '';
      params.endYear = notEmpty(params.startDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('YYYY') : '';
      params.endDayNum = notEmpty(params.startDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).format('d') : '';
      params.endTime = notEmpty(params.endDate) ? moment(params.endDate/*,"YYYY-MM-DD HH:mm"*/).local().locale('fr').format('HH:mm') : '';
      params.startDayNum = directory.getWeekDayName(params.startDayNum);
      params.endDayNum = directory.getWeekDayName(params.endDayNum);

      params.startMonth = directory.getMonthName(params.startMonth);
      params.endMonth = directory.getMonthName(params.endMonth);
      

      var startLbl = (params.endDay != params.startDay) ? trad['fromdate'] : '';
      var endTime = ( params.endDay == params.startDay && params.endTime != params.startTime) ? ' - ' + params.endTime : '';
      mylog.log('params.allDay', !notEmpty(params.allDay), params.allDay);
       
        
      var str = '';
      var dStart = params.startDay + params.startMonth + params.startYear;
      var dEnd = params.endDay + params.endMonth + params.endYear;
      mylog.log('DATEE', dStart, dEnd);

      if(params.startDate != null){
        if(notNull(onlyStr)){
          if(params.endDate != null && dStart != dEnd)
          dateObj.fromdate = trad.fromdate;
          dateObj.startDay = params.startDay;

          if(params.endDate == null || dStart == dEnd || (params.startMonth != params.endMonth || params.startYear != params.endYear))
            dateObj.startMonth = params.startMonth;
          if(params.endDate == null || dStart == dEnd || params.startYear != params.endYear)
            dateObj.startYear = params.startYear;

          if(params.endDate != null && dStart != dEnd){
            dateObj.endLbl = trad['todate'];
            dateObj.endDay = params.endDay;
            dateObj.endMonth = params.endMonth.substring(0,3);
            dateObj.endYear = params.endYear;
          }
            dateObj.startTime = params.startTime;
            dateObj.endTime = endTime;            
        }else{
          dateObj.startLbl = startLbl;
          dateObj.startDayNum = params.startDayNum.substring(0,3);
          dateObj.startDay = params.startDay;
          dateObj.startMonth = params.startMonth.substring(0,3);
          dateObj.startYear = params.startYear;
          if(params.collection == "events")
            dateObj.startTime = params.startTime;
        }
      }    
        
      if(params.endDate != null && dStart != dEnd && !notNull(onlyStr)){
            dateObj.endLbl = trad['todate'];
            dateObj.endDayNum = params.endDayNum.substring(0,3);
            dateObj.endDay = params.endDay;
            dateObj.endMonth = params.endMonth.substring(0,3);
            dateObj.endYear = params.endYear;
            if(params.collection == "events")
              dateObj.endTime = params.endTime;
      }   
      return dateObj
    }
}

function initOpeningHoursCms (params, allInfos) {
    mylog.log('initOpeningHours', params, allInfos);
    var html = '<h5 class="text-center">';
    html += (notNull(allInfos)) ? '<span class=\' ttr-desc-4 bold uppercase" style="padding-left: 15px; padding-bottom: 10px;"\'><i class="fa fa-calendar"></i>&nbsp;'+trad.eachWeeks+'</span>' : '<span class=\' title-2 uppercase bold no-padding\'>'+trad.each+' </span>' ;
    mylog.log('initOpeningHours contextData.openingHours', params.openingHours);
    if(notNull(params.openingHours) ){
      var count=0;

      var openHour = '';
      var closesHour = '';
      $.each(params.openingHours, function(i,data){
        mylog.log('initOpeningHours data', data, data.allDay, notNull(data));
        mylog.log('initOpeningHours notNull data', notNull(data), typeof data, data.length);
        if( (typeof data == 'object' && notNull(data) ) || (typeof data == 'string' && data.length > 0) ) {
          var day = '' ;
          var dayNum=(i==6) ? 0 : (i+1);
          if(notNull(allInfos)){
            mylog.log('initOpeningHours data.hours', data.hours);
            day = '<li class="tl-item">';
            day += '<div class="item-title">'+moment().day(dayNum).local().locale(mainLanguage).format('dddd')+'</div>';
            $.each(data.hours, function(i,hours){
              mylog.log('initOpeningHours hours', hours);
              day += '<div class="item-detail">'+hours.opens+' : '+hours.closes+'</div>';
            });
            day += '</li>';
            if( moment().format('dd') == data.dayOfWeek )
              html += day;
            else
              html += day;
          }else{
            if(count > 0) html+=', ';

          
            var color = '';
          
            if( typeof agenda != 'undefined' && agenda != null && 
            (moment(agenda.getStartMoment(agenda.dayCount)).isoWeekday() - 1 ) == i &&
            typeof data.hours[i] != 'undefined' && 
            typeof data.hours[i].opens != 'undefined' && 
            typeof data.hours[i].closes != 'undefined' ) {

              openHour = data.hours[i].opens;
              closesHour = data.hours[i].closes;
            }
  

            html+= '<b style=\'font-variant: small-caps;font-size:x-large !important;\' class=\'title-3'+color+'\'>'+moment().day(dayNum).local().locale(mainLanguage).format('dddd')+'</b>';
          
          }
          count++;
        }
        
      });

      html +=  '&nbsp<small class=" margin-top-5"><b><i class="fa fa-clock-o"></i> '+openHour+'-'+closesHour+'</b></small>';
    } else 
      html = '<i>'+trad.notSpecified+'</i>'; 

    return html+'</h5>';
}

var createFieldsets = function(objProperties){ 
    alignInput2(objProperties,"margin",2,6,2,null,tradCms.Margininpx,"blue","hidden");
    alignInput2(objProperties,"padding",2,6,2,null,tradCms.Paddinginpx,"blue","hidden");
    alignInput2(objProperties,"mode",2,6,2,null,tradCms.screensize,"blue","hidden");
    alignInput2(objProperties,"blockCmsBg",8,12,2,2,tradCms.blockbackgroundcolororimage,"blue","");
    alignInput2(objProperties,"blockCmsPolice",2,6,null,null,tradCms.font,"#fa0470","hidden");
    alignInput2(objProperties,"blockCmsTextAlign",2,6,null,null,tradCms.textalignment,"#fa0470","hidden");
    alignInput2(objProperties,"blockCmsTextSize",2,6,null,null,tradCms.textsize,"#fa0470","hidden");
    alignInput2(objProperties,"blockCmsTextLineHeight",2,6,null,null,tradCms.interline,"#fa0470","hidden");
    alignInput2(objProperties,"blockCmsUnderline",2,3,null,null,tradCms.textunderline,"#fa0470","hidden");
    alignInput2(objProperties,"blockCmsColor",2,6,null,null,tradCms.textcolor,"#fa0470","hidden");
    alignInput2(objProperties,"blockCmsBorder",2,6,null,null,tradCms.sectionborder,"blue","hidden");
    alignInput2(objProperties,"blockCmsLineSeparator",2,6,null,null,tradCms.dividinglinebetweensections,"blue","hidden");
    alignInput2(objProperties,"blockCmsAos",6,6,null,null,"Animation","blue","hidden");
}
