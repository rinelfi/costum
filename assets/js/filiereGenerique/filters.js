var paramsFilter;

function createFilters(filterContainer, isMapActive, mapContainer=null){

	var pageApp= window.location.href.split("#")[1];
    
    //var appConfig=<?php echo json_encode(@$appConfig); ?>;
	
	var thematic = {};

	var defaultType = {"NGO" : trad.ong,
		"LocalBusiness" : trad.entreprise,
		"GovernmentOrganization" : trad.servicepublic};

	if(costum.lists.group){
		Object.assign(defaultType, costum.lists.group);
	}

	if(costum.lists.theme){
		Object.assign(thematic, costum.lists.theme);
	}

	let defaultScopeList = [];

	if(costum && costum.slug && costum.slug=="ries"){
		defaultScopeList = ["IT"];
	}else{
		defaultScopeList = ["FR"];
	}

	/*var mapFilter = new CoMap({
            container : mapContainer,
            activePopUp : true,
            mapOpt:{
                btnHide : false,
                doubleClick : true,
                zoom : 2,
            },
            mapCustom:{
                tile : "mapbox"
            },
            elts : []
        });
*/
	//$("#filters-nav").addClass("nav navbar-nav");

	paramsFilter= {
		container : filterContainer,//"#filters-nav",
		mapObj : {
			parentContainer : mapContainer,
		},
		defaults : {
		 	types : ["organizations"],
		},
		filters : {
		 	theme : {
	 			view : "megaMenuDropdown",
	 			type : "tags",
	 			name : trad.searchbytheme,//"<?php echo Yii::t("common", "Search by theme")?>",
	 			event : "tags",
	 			keyValue: true,
	 			list : thematic
	 		},
	 		
	 		type : {
		 		view : "dropdownList",
		 		type : "tags",
		 		name : trad.searchbytype,//"<?php echo Yii::t("common", "Search by type")?>",
		 		event : "tags",
		 		list : defaultType
		 	},
		 	name : {
		 		view : "dropdownList",
		 		type : "tags",
		 		name : trad.searchbynetwork,// "<?php echo Yii::t("common", "Search by network")?>",
		 		event : "tags",
		 		list : ["RIES", "Fuorimercato", "Arci", "Genuino Clandestino"]
		 	},
 			scopeList : {
	 			name : trad.searchbyplace,//"<?php echo Yii::t("common", "Search by place")?>",
	 			params : {
	 				countryCode : defaultScopeList, 
	 				level : ["3"]
	 			}
	 		},
	 		text : true
	 	},
	 	results : {
	 		map: {active: isMapActive},
			renderView: "directory.classifiedPanelHtml",
		 	smartGrid : true
		},
	};

	if(isMapActive){
		$("#mapContent").show();
	}
}