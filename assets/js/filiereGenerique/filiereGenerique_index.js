dyFObj.unloggedMode=true;

costum[costum.slug] = {
	init : function(){
		coInterface.bindButtonOpenForm = function(){
			$(".btn-open-form, #joinBtn").off().on("click",function(){
				setTimeout(function() {
					$('.addListLineBtncertificationsAwards').text(trad.addrow);
					if (costum.slug == "ries") {                        
						$('#btn-submit-form').attr("disabled", true);
					}
					$('.extraInfo-agreecheckboxSimple').click(function(){
						if ($('#extraInfo-agree').val() == "true")
							$('#btn-submit-form').attr("disabled", false);
						else
							$('#btn-submit-form').attr("disabled", true);
					});
					$(".typeselect").hide();

				}, 800);

				var typeForm = $(this).data("form-type");

				if(typeForm =="reseau"){
					getNetwork();
				}
				currentKFormType = ($(this).data("form-subtype")) ? $(this).data("form-subtype") : null;
				if(contextData && contextData.type && contextData.id ){
					dyFObj.openForm(typeForm);
				}else{
					/*let dynFormCostumIn = {}
					if(typeof costum[costum.slug][typeForm]!="undefined" && typeof costum[costum.slug][typeForm].afterSave != "undefined"){
						dynFormCostumIn.afterSave = costum[costum.slug][typeForm]
					}*/
					dyFObj.openForm(typeForm);
				}
			});
		};    
		coInterface.bindButtonOpenForm();
	},
	"organizations" : {
		formData : function(data){
			var certifications = [];
			$.each(data, function(e, v){
				if(e.indexOf("-") != -1) {					
					ee = e.split("-").join("[")+"]";
					data[ee] = data[e];
					delete data[e]
				}

				if(typeof costum.lists[e] != "undefined"){
					if(notNull(v)){
						if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
						
						if(typeof v == "string")
							data.tags.push(v);
						else{
							$.each(v, function(i,tag){
								data.tags.push(tag);	
							});
						}
					}

					delete data[e];
				}				
			});
			return data;
		},
		afterSave : function(orga){
			var data = {
				type : orga.map.type,
				id : orga.map._id["$id"],
				collection:"organizations",
				path : "allToRoot",
				value : {"type": orga.map.group}
			};
			dataHelper.path2Value( data, function(params) {
				urlCtrl.loadByHash("#@"+orga.map.slug+".view.detail");
			} );
		}
	}
}

costum.lists.budget = {
	"Da 1 a 25.000 euro" : "Da 1 a 25.000 euro",
	"Da 25.000 a 50.000 euro" : "Da 25.000 a 50.000 euro",
	"Da 50.000 a 100.000 euro" : "Da 50.000 a 100.000 euro",
	"Da 100.000 a 500.000 euro" : "Da 100.000 a 500.000 euro",
	"Oltre 500.000 euro" : "Oltre 500.000 euro"
}

costum.lists.types =  {
	"NGO" : trad.ong,
	"Associazione":"Associazione", 
	"Cooperative":"Cooperativa", 
	"Consorzio":"Consorzio",
	"Impresa sociale":"Impresa sociale",
	"LocalBusiness" : trad.entreprise,
	"GovernmentOrganization" : trad.servicepublic,
	"university" : trad.university,
	"international organization" : trad.internationalorganization,
	"foundation" : trad.foundation,
	"entity association or religious community" : trad.entityassociationorreligiouscommunity,
	"self-propelled social space" : trad.selfpropelledsocialspace,
	"political collective" : trad.politicalcollective,
	"syndicate" : trad.syndicate,
	"party" : trad.politicalParty,
	"media" : "media",
	"Group" : "Altri",
}

costum.lists.theme = {};

costum.lists.theme[tradCategory.food] =  {
	"agricultural production" : trad.agriculturalproduction,
	"GAS" : "GAS",
	"Shops of the world" : trad.shopsoftheworld,
	"CSA" : "CSA",
	"Urban gardens" : trad.urbangardens,
	"Farmers markets" : trad.farmersmarkets,
	"Catering and administration" : trad.cateringandadministration
}

costum.lists.theme[tradCategory.productionandservices] =  {
	"Carpenters" : trad.carpenters,
	"Social tailors" : trad.socialtailors,
	"Fair trade" : trad.fairtrade
}


costum.lists.theme[tradCategory.education] =  {
	"Cultural activities" : trad.culturalactivities,
	"GAS" : "GAS",
	"Education and training" : trad.educationandtraining,
	"Research" : trad.research,
	"Information" : trad.information
}


costum.lists.theme[tradCategory.dwelling] =  {
	"Sustainable living" : trad.sustainableliving,
	"Territory care" : trad.territorycare,
	"Ciclofficine" : "Ciclofficine",
	"Other forms of sustainable mobility" : trad.otherformsofsustainablemobility,
	"Building" : trad.building
}


costum.lists.theme[trad.finance] =  {
	"Social coins" : trad.socialcoins,
	"MAG" : "MAG",
	"Shops of the world" : trad.shopsoftheworld
}


costum.lists.theme[tradCategory.health] =  {
	"Sport" : "Sport",
	"Health and care" : trad.healthandcare,
	"Counters for psychological listening" : trad.countersforpsychologicallistening,
	"Mutual aid benches" : trad.mutualaidbenches
}


costum.lists.theme[trad.energie] =  {
	"Energy supplies" : trad.energysupplies,
	"Energy production" : trad.energyproduction,
	"Wireless Networks" : trad.wirelessnetworks,
	"Repair café" : "Repair café",
	"Fablab" : "Fablab"
}


costum.lists.theme[tradCategory.citizen] =  {
	"Legal branches" : trad.legalbranches,
	"Advocacy and local policies" : trad.advocacyandlocalpolicies,
	"Neighborhood committees" : trad.neighborhoodcommittees
}

costum.lists.theme[trad.travelandhospitality] =  {
	"Travel and hospitality" : trad.travelandhospitality
}
costum.typeObj.organizations.dynFormCostum.beforeBuild.properties.group["label"] = trad.choosemoretypeoforganization
costum.typeObj.organizations.dynFormCostum.beforeBuild.properties.group["placeholder"] =  trad.select
costum.typeObj.organizations.dynFormCostum.beforeBuild.properties.theme["label"] = trad.themes
costum.typeObj.organizations.dynFormCostum.beforeBuild.properties.theme["placeholder"] =  trad.select

costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["type"] = {
	"inputType" : "select",
	"labelInformation" : "typo",
	"class" : "hidden",
	"value" : "group"
}

costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["group"] =  {
	"inputType" : "select",
	"label" : trad.typeoforganization,
	"order" : "3",
	"noOrder" : true,
	"class" : "form-control",
	"labelInformation" : "groupo",
	"list" : "types"
}

costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["activityPlace"] = {
	"placeholder" : trad.select,
	"inputType" : "selectMultiple",
	"label" : trad.wheredoyoudoyourbusiness,
	"class" : "multi-select",
	"options" : {
		"head office" : trad.headoffice,
		"online" : trad.activityonline,
		"home" : trad.athome
	}
}

costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["extraInfo[contacts]"] = {
	"placeholder" : trad.addressedto,
	"inputType" : "selectMultiple",
	"label" : "Principali destinatari dell’attività",
	"class" : "multi-select contacts",
	"options" : {
		"Aimed at everyone" : trad.aimedateveryone,
		"Public institutions" : trad.publicinstitutions,
		"Religious bodies" : trad.religiousbodies,
		"Organization NGO" : trad.ong,
		"Companies" : trad.companies,
		"Inhabitants of the neighborhood" : trad.inhabitantsneighborhood,
		"Solidarity Purchase Groups" : trad.solidaritypurchasegroups
	}
}
costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["certificationsAwards"] = {
	"label" : trad.certificationsorawards,
	"inputType" : "lists",
	"entries" : {
		"certAward" : {
			"label" : trad.certificationsorawards,
			"type" : "text"
		}
	}
}


costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["description"] = {
	"label" : tradDynForm.longDescription,
	"inputType" : "textarea",
	"order" : "13"
}


costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["extraInfo-reteofapartment"] = {
	"label" : trad.reteofapartment,
	"inputType" : "tags",
	"placeholder" : "RIES, Fuorimercato, Arci, Genuino Clandestino, ...	",
	"values" : []
}

costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["extraInfo-dimensions"] = {
	"label" : trad.dimensions,
	"inputType" : "select",
	"class" : "form-control",
	"placeholder" : trad.select,
	"options" : {
		"Regional" : trad.regional,
		"National" : trad.national,
		"Local" : trad.local,
		"International" : trad.international
	}
}
costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["extraInfo-transformativehorizons"] = {
	"label" : trad.transformativehorizons,
	"inputType" : "selectMultiple",
	"class" : "multi-select",
	"placeholder" : trad.select,
	"options" : {
		"Economia sociale e solidale": trad.socialandsolidarityeconomy,
		"Economia sociale" : trad.socialeconomy,
		"Ecological economics" : trad.ecologicaleconomics,
		"Non-violent economy" : trad.nonviolenteconomy,
		"Circular economy" : trad.circulareconomy,
		"Economy of the common good" : trad.economyofthecommongood,
		"Community economy" : trad.communityeconomy,
		"Gift economy" : trad.gifteconomy,
		"Collaborative economies" : trad.collaborativeeconomies,
		"Civil economy" : trad.civileconomy,
		"Ecofeminist economics" : trad.ecofeministeconomics,
		"Economies of degrowth" : trad.economiesofdegrowth,
		"Migrant protagonist" : trad.migrantprotagonist,
		"Inclusive economies" : trad.Inclusiveeconomies,
		"Ethical finance" : trad.ethicalfinance,
		"Mutualism" : trad.mutualism,
		"Agroecology" : trad.agroecology

	}
}

costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["extraInfo-budget"] =  {
	"inputType" : "select",
	"label" : "Dimensione economica (budget)",
	"class" : "form-control",
	"labelInformation" : "Dimensione economica",
	"noOrder" : true,
	"list" : "budget"
}

costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["extraInfo-agree"] = {
	"label" : trad.readandacceptprivacyinformation,
	"inputType" : "checkboxSimple",
	"rules" : {
		"required" : true
	},
	"params" : {
		"onText" : trad.yes,
		"offText" : trad.no,
		"onLabel" : trad.yes+" <a href='#privacy' target='_blank'><b>("+trad.readmore+")</b></a>",
		"offLabel" : trad.no+" <a href='#privacy' target='_blank'><b>("+trad.readmore+")</b></a>"
	},
	"checked" : false
}

costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["extraInfo-receiveInfo"] = {
	"label" : trad.wouldliketoreceiveinformationfromtheobservatory,
	"inputType" : "checkboxSimple",
	"params" : {
		"onText" : trad.yes,
		"offText" : trad.no,
		"onLabel" : trad.yes,
		"offLabel" : trad.no
	},
	"checked" : false
}
costum.typeObj.organizations.dynFormCostum.beforeBuild.properties["people"] = {
	"inputType" : "custom",
	"order" : "16",
	"html" : "<label class='col-xs-12 text-left control-label no-padding'><i class='fa fa-chevron-down'></i>"+trad.howmanypeopleareinvolvedintheactivities+"</label><div class='col-xs-12 no-padding'><div class='listEntry col-xs-12 no-padding' data-num='0'><div class='col-md-5'><div class='padding-5 col-xs-12'><h6>"+trad.workers+" </h6></div><div class='space5'></div><input type='number' min='0' name='extraInfo[peopleinvolved][workers]' data-entry='workers' id='extraInfo[peopleinvolved][workers]' class=' form-control input-md col-xs-12 no-padding workers' value='' placeholder=''></div><div class='col-md-5 '><div class='padding-5 col-xs-12'><h6>"+trad.volunteers+" </h6></div><div class='space5'></div><input type='number' min='0' name='extraInfo[peopleinvolved][volunteers]' data-entry='volunteers' id='extraInfo[peopleinvolved][volunteers]' class='form-control input-md col-xs-12 no-padding volunteers' value='' placeholder=''></div></div></div>"
}

for(const [key, val] of Object.entries(costum.lists.types)){
	if(typeObj && typeof typeObj[key] == "undefined"){
		typeObj[key] = {
			"name": key,
		    "sameAs": "organization",
		    "color": "dark-gray",
		    "icon": "group"
		}
	}else{
		typeObj[key]["name"] = val;
	}
}

typeObj.organization["name"] = trad.organization;