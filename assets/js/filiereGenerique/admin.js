adminPanel.views.badges = function(){

	var data={
		title : "Etiquetages (badges)",
		//types : [ "badges"],
		table : {
			// type:{
			// 	name : "Type",
			// 	notLink : true
			// },
			name: {
                name : "Nom",
                preview : true
            },
			description:{
                name : "Description"
            },
            category:{
                name : "Categorie"
            },
            tags:{
                name : "Utilisations"
            }
            
			//pdf : true,
			//status : true
		},
		paramsFilter : {
			container : "#filterContainer",
			defaults : {
				types : [ "badges" ],
				//private : true
			},
			filters : {
				text : true
			}
		},
	};
	if(typeof costum.isCostumAdmin != "undefined" && costum.isCostumAdmin){
		data.actions={
			update : true,
			delete : true
		};
	}

	ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
};

adminDirectory.events.delete = function(aObj){
	$("#"+aObj.container+" .deleteBtn").off().on("click", function(){
		mylog.log("adminDirectory..delete ", $(this).data("id"), $(this).data("type"));
		directory.deleteElement($(this).data("type"), $(this).data("id"), $(this), afterDelete);
	});
}

function afterDelete(data, type, id) {
	if(type == "badges"){
		for (const category of Object.keys(costum.badges)) {
			delete costum.badges[category][id];
		}
		costum = prepareList(costum);
	}
}

costum.filiere = {
    badges : {
		formData : function(data){
			if(typeof data.linksProjects){
				if(typeof data.links == "undefined") data.links={};
				data.links.projects=data.linksProjects;
				delete data.linksProjects;
			}
			return data;
		},
		afterSave : function(data){
			if(dyFObj.editMode){
				if($.inArray(data.map.category, ["domainAction"]) >= 0){
					// change data into badges
					oldName = costum.badges[data.map.category][data.id].name;
					costum.badges[data.map.category][data.id] = data.map;
					costum = prepareList(costum);
				}
			}else{
				if($.inArray(data.map.category, ["domainAction"]) >= 0){
					if(!costum.badges){
						costum.badges = {}
					}
					if(!costum.badges[data.map.category]){
						costum.badges[data.map.category] = {}
					}
					costum.badges[data.map.category][data.id] = data.map;
					costum = prepareList(costum);
				}
			}
			dyFObj.commonAfterSave();
		}
	},
}

function prepareList(costum) {
	for (const category of Object.keys(costum.badges)) {
		costum.lists[category] = {}
		const badges = costum.badges[category];
		const keys = Object.keys(badges);
		const list = {};
		for (const key of keys) {
			const element = costum.badges[category][key];
			var count = 0;
			if(element.parent){
				for(const parentKey of Object.keys(element.parent))
				if(keys.includes(parentKey)){
					if(!list[badges[parentKey].name]){
						list[badges[parentKey].name] = []
					}
					list[badges[parentKey].name].push(element.name);
					count++;
				}
			}
			if(!element.parent || count == 0){
				list[element.name] = [];
			}
		}
		costum.lists[category] = list;
	}
	return costum;
}


if(costum && typeof costum.slug != "undefined" && costum.slug == "cressReunion"){
	
	adminPanel.views.importsiret = function(){
		ajaxPost('#content-view-admin', baseUrl+'/costum/cressreunion/importsiret/', {}, function(){},"html");
	};

	adminPanel.views.organizations = function(){
		var data={
			title : "Les organisations !",
			table : {
	            name : {
	                name : "Nom"
	            },
	            siret: {
	                name : "SIRET"
	            },
	            waldec : {
	                name : "WALDEC"
	            },
	            email : {
	                name : "E-mail"
	            },
	            famille : {
	                name : "Famille"
	            },
	            arrondissement : {
	                name : "Arrondissement"
	            },
	            tags : { 
	                name : "Mots clés" 
	            }
	        },
	        paramsFilter : {
	            container : "#filterContainer",
	            defaults : {
	                types : [ "organizations" ]
	            },
	            filters : {
	                text : true
	            }
	        },
	        actions : {
	            update : true,
	        	delete : true
	        },
	        csv : [
	            {
	                url : baseUrl+'/co2/export/csv/',
	                defaults : {
	                        indexStep : 0,
	                        fields : [
	                            "name", "siret", "siren", "sigle", "waldec", "email", "famille", "arrondissement", "interco", "apet700", "secteurEtablissement", "typeEtablissement", "telephone.fixe", "telephone.mobile", "url", "shortDescription", "description", "tags","address.streetAddress","address.postalCode","address.addressLocality","address.addressCountry","ambassadeurName","ambassadeurFirstName","ambassadeurTel","ambassadeurMail","ambassadeurFonction", "socialNetwork.facebook","socialNetwork.twitter","socialNetwork.instagram"
	                        ]
	                }
	            }
	        ]
		};
		ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
	};

	adminPanel.views.importorga = function(){
		ajaxPost('#content-view-admin', baseUrl+'/costum/cressreunion/importorga/', {}, function(){},"html");
	};

	adminDirectory.values.siren = function(e, id, type, aObj){
	    mylog.log("adminDirectory.values siren", e, id, type, aObj);
	    var str = "";
	    if( typeof e.siren != "undefined"){
	        str = e.siren;
	    }
	    return str;
	};

	adminDirectory.values.siret = function(e, id, type, aObj){
	    mylog.log("adminDirectory.values siret", e, id, type, aObj);
	    var str = "";
	    if( typeof e.siret != "undefined"){
	        str = e.siret;
	    }
	    return str;
	};

	adminDirectory.values.famille = function(e, id, type, aObj){
	    mylog.log("adminDirectory.values famille", e, id, type, aObj);
	    var str = "";
	    if( typeof e.famille != "undefined"){
	        str = e.famille;
	    }
	    return str;
	};

	adminDirectory.values.arrondissement = function(e, id, type, aObj){
	    mylog.log("adminDirectory.values arrondissement", e, id, type, aObj);
	    var str = "";
	    if( typeof e.arrondissement != "undefined"){
	        str = e.arrondissement;
	    }
	    return str;
	};

	adminDirectory.values.secteurEtablissement = function(e, id, type, aObj){
	    mylog.log("adminDirectory.values secteurEtablissement", e, id, type, aObj);
	    var str = "";
	    if( typeof e.secteurEtablissement != "undefined"){
	        str = e.secteurEtablissement;
	    }
	    return str;
	};

	adminPanel.views.diff = function(){
	    var data={
	        title : "Les organisations à modéré",
	        paramsFilter : {
	            container : "#filterContainer",
	            defaults : {
	                types : [ "organizations" ],
	                filters : {
	                      "source.toBeValidated.cressReunion" : { '$exists' : 1 }
	                }
	            },
	            filters : {
	                text : true
	            
	            }
	        },
	        table : {
	            name : {
	                name : "Nom"
	            },
	            siret: {
	                name : "SIRET"
	            },
	            waldec : {
	                name : "WALDEC"
	            },
	            email : {
	                name : "E-mail"
	            },
	            famille : {
	                name : "Famille"
	            },
	            arrondissement : {
	                name : "Arrondissement"
	            },
	            validated : { 
	                name : "Valider",
	                class : "col-xs-2 text-center"
	            }
	        },
	        actions : {
	            update : true,
	            delete : true,
	            validated : true
	        }
	    };
	    ajaxPost('#content-view-admin', baseUrl+'/'+moduleId+'/admin/directory/', data, function(){},"html");
	};

	// adminPanel.views.openform = function(){
	//     var idF = null;
	//     if(typeof costum.forms != "undefined"){
	//         $.each(costum.forms, function(kF, vF){
	//             idF = kF;
	//         });
	//     }

	//     if(idF != null)
	//         ajaxPost('#content-view-admin', baseUrl+'/survey/form/edit/id/'+idF, {}, function(){},"html");
	// };

	adminPanel.views.listforms = function(){
	    //ajaxPost('#content-view-admin', baseUrl+'/survey/form/edit/id/', {}, function(){},"html");

	    var data={
	        title : "Gestion des formulaires",
	        id : costum.contextId,
	        collection : costum.contextType,
	        slug : costum.contextSlug,
	        url : baseUrl+'/survey/form/admindirectory/slug/'+costum.contextSlug,
	        table : {
	            name: {
	                name : "Form"
	            }
	        }
	    };
	    if((typeof costum.isCostumAdmin != "undefined" && costum.isCostumAdmin) 
	        || (typeof canEdit != "undefined" && canEdit) ){
	        data.actions={};
	    }
	            
	    ajaxPost('#content-view-admin', data.url, data, function(){},"html");
	};

	adminPanel.views.candidatures = function(){
	    var data={
	        title : "Gestion des dossiers",
	        id : costum.contextId,
	        collection : costum.contextType,
	        slug : costum.contextSlug,
	        url : baseUrl+'/survey/answer/admindirectory/slug/'+costum.contextSlug,
	        table : {
	            name: {
	                name : "Dossier"
	            },
	            comment : {
	                name : "Commentaires"
	            }
	        },
	        csv : {
	            post : [
	                {
	                    url : baseUrl+'/co2/export/csvelement/type/answers/slug/cressReunion/'
	                }
	            ]
	        }
	    };
	    if((typeof costum.isCostumAdmin != "undefined" && costum.isCostumAdmin) 
	        || (typeof canEdit != "undefined" && canEdit) ){
	        data.actions={
	            pdf : true,
	            deleteAnswer: true
	        };
	    }
	            
	    ajaxPost('#content-view-admin', data.url, data, function(){},"html");
	}
}