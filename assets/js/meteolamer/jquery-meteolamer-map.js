$.fn.extend({
    meteolamerMap: function(options){
        var WIND_COLORS = [
            {
                speed:[0, 5],
                bgColor:"#0101aa",
                bgColorShade:"#010198", 
                contentColor:"light"
            },
            {
                speed:[5, 10],
                bgColor:"#0100ff",
                bgColorShade:"#0000eb", 
                contentColor:"light"
            },
            {
                speed:[10, 20],
                bgColor:"#0060df",
                bgColorShade:"#0058cc", 
                contentColor:"light"
            },
            {
                speed:[20, 30],
                bgColor:"#01bfbf",
                bgColorShade:"#01adad", 
                contentColor:"dark"
            },
            {
                speed:[30, 40],
                bgColor:"#aeff57",
                bgColorShade:"#a4ff42", 
                contentColor:"dark"
            },
            {
                speed:[40, 50],
                bgColor:"#ffff00",
                bgColorShade:"#ebeb00", 
                contentColor:"dark"
            },
            {
                speed:[50, 60],
                bgColor:"#ffbf00",
                bgColorShade:"#ebb000", 
                contentColor:"dark"
            },
            {
                speed:[60, 70],
                bgColor:"#ff8000",
                bgColorShade:"#eb7500", 
                contentColor:"dark"
            },
            {
                speed:[70, 80],
                bgColor:"#cc5900",
                bgColorShade:"#b85000", 
                contentColor:"light"
            },
            {
                speed:[80, Infinity],
                bgColor:"#993300",
                bgColorShade:"#852c00", 
                contentColor:"light"
            }
        ];
        var WAVE_COLORS = [
            {
                height: [0, 0.5],
                bgColor:"#0101aa",
                bgColorShade:"#010198", 
                contentColor:"light"
            },
            {
                height: [0.5, 1],
                bgColor:"#0100ff",
                bgColorShade:"#0000eb", 
                contentColor:"light"
            },
            {
                height: [1, 1.5],
                bgColor:"#0060df",
                bgColorShade:"#0058cc", 
                contentColor:"light"
            },
            {
                height: [1.5, 2],
                bgColor:"#01bfbf",
                bgColorShade:"#01adad", 
                contentColor:"dark"
            },
            {
                height: [2, 2.5],
                bgColor:"#aeff57",
                bgColorShade:"#a4ff42", 
                contentColor:"dark"
            },
            {
                height: [2.5, 3],
                bgColor:"#ffff00",
                bgColorShade:"#ebeb00", 
                contentColor:"dark"
            },
            {
                height: [3, 4],
                bgColor:"#ffbf00",
                bgColorShade:"#ebb000", 
                contentColor:"light"
            },
            {
                height: [4, 4.5],
                bgColor:"#ff8000",
                bgColorShade:"#eb7500", 
                contentColor:"light"
            },
            {
                height: [4.5, 5],
                bgColor:"#cc5900",
                bgColorShade:"#b85000", 
                contentColor:"light"
            },
            {
                height: [5, Infinity],
                bgColor:"#993300",
                bgColorShade:"#852c00", 
                contentColor:"light"
            }
        ];

        var context = this;
        context.coMap = null;
        context.map = null;
        context.activeSpotmarkers = null;

        var coMapOptions = {
            container:"#"+$(context).attr("id"),
            mapOpt:{
                zoom:14,
                centerOneZoom:14,
                latLon : ["-21", "55"],
                onclickMarker: function(params){
                    if(options.onClickSpot)
                        options.onClickSpot(params.elt);
                }
            },
            mapCustom: {
                markers:{
                    getMarker:function(){
                        return ASSETS_URL + "/images/meteolamer/meteolamer-marker-default.png"
                    }
                },
                icon:{
                    options:{
                        iconAnchor: [23.5, 55]
                    }
                }
            }
        }

        var themes = {
            getWindTheme: function(speed){
                var themeColor = null;
                WIND_COLORS.forEach(function(item){
                    if(item.speed[0] < speed && speed <= item.speed[1])
                        themeColor = item;
                })
                return themeColor;
            },
            getWaveTheme: function(height){
                var themeColor = null;
                WAVE_COLORS.forEach(item => {
                    if(item.height[0] < height && height <= item.height[1])
                        themeColor = item;
                })
                return themeColor;
            }
        }

        var popup = {
            getWindPopup:function(data){
                var windThemeColor = themes.getWindTheme(data.speed),
                    imgFilter = windThemeColor.contentColor==="light"?0:1,
                    color = windThemeColor.contentColor==="light"?"white":"black";
                return `
                    <div class="poopup-container" style="background-color: ${windThemeColor.bgColor};color:${color};">
                        <div class="icon" style="background-color: ${windThemeColor.bgColor};">
                            <img 
                                src="${ ASSETS_URL }/images/meteolamer/wind.png" 
                                style="filter: invert(${imgFilter});"
                            />
                        </div>
                        <ul>
                            <li>${data.speed} knot</li>
                            <li>
                                <img 
                                    src="${ ASSETS_URL }/images/meteolamer/dir.png" 
                                    style="transform: rotate(${data.degree-180}deg); filter: invert(${imgFilter});"
                                />
                            </li>
                        </ul>
                    </div>
                `
            },
            getWavePopup:function(data){
                var waveThemeColor = themes.getWaveTheme(data.height),
                    imgFilter = waveThemeColor.contentColor==="light"?0:1,
                    color = waveThemeColor.contentColor==="light"?"white":"black";
                return `
                    <div class="poopup-container" style="background-color: ${waveThemeColor.bgColor};color:${color};">
                        <div class="icon" style="background-color: ${waveThemeColor.bgColor};">
                            <img 
                                src="${ ASSETS_URL }/images/meteolamer/big-wave.png"
                                style="filter: invert(${imgFilter});"
                            />
                        </div>
                        <ul>
                            <li>${Number.parseFloat(data.height).toFixed(2)}m</li>
                            <li>${Number.parseFloat(data.period).toFixed(2)}s</li>
                            <li>
                                <img 
                                    src="${ ASSETS_URL }/images/meteolamer/dir.png" 
                                    style="transform: rotate(${data.degree-180}deg); filter: invert(${imgFilter});"
                                />
                            </li>
                        </ul>
                    </div>
                `;
            }
        }

        var icon = {
            getWindIcon:function(data){
                var windThemeColor = themes.getWindTheme(data.speed),
                    imgFilter = windThemeColor.contentColor==="light"?0:1;
                return L.divIcon({
                    html:`
                        <div class="arrow-marker" style="background-color: ${windThemeColor.bgColor};">
                            <img src="${ ASSETS_URL }/images/meteolamer/wind.png" style="filter: invert(${imgFilter});"/>
                        </div>
                    `,
                    className:"",
                    iconAnchor: [15, 15]
                })
            },
            getWaveIcon:function(data){
                var waveThemeColor = themes.getWaveTheme(data.height),
                    imgFilter = waveThemeColor.contentColor==="light"?0:1;
                return L.divIcon({
                    html:`
                        <div class="arrow-marker" style="background-color: ${waveThemeColor.bgColor};">
                            <img 
                                src="${ ASSETS_URL }/images/meteolamer/big-wave.png" 
                                style="filter: invert(${imgFilter});"
                            />
                        </div>
                    `,
                    className:"",
                    iconAnchor: [15, 15]
                })
            },
            getWindWaveIcon:function(windData, waveData){
                var windThemeColor = themes.getWindTheme(windData.speed),
                    waveThemeColor = themes.getWindTheme(waveData.speed);
                return L.divIcon({
                    html:`
                        <div style="width:40px; height:40px; border-radius: 100%;">
                            <div style="background:${waveThemeColor.bgColor};width:40px; height: 20px; display:flex; justify-content:center; align-items:center; border-radius: 20px 20px 0px 0px">
                                <img src="${ ASSETS_URL }/images/meteolamer/big-wave.png" width="16" height="16" style="filter: invert(${waveThemeColor.contentColor==="light"?0:1});"/>
                            </div>
                            <div style="background:${windThemeColor.bgColor};width:40px; height: 20px; display:flex; justify-content:center; align-items:center; border-radius:0px 0px 20px 20px;">
                                <img src="${ ASSETS_URL }/images/meteolamer/wind.png" width="16" height="16" style="filter: invert(${windThemeColor.contentColor==="light"?0:1});"/>
                            </div>
                        </div>
                    `,
                    className:"",
                    iconAnchor: [20, 35]
                })
            }
        }

        var arrowDistance = {
            getArrowWindDistance: function(windSpeed){
                if(windSpeed > 80)
                    return 2;
                else{
                    var d = (windSpeed*2)/80;
                    d = d < 0.5 ? 0.5:d;
                    return d;
                }
            },
            getArrowWaveDistance: function(waveHeight){
                if(waveHeight > 5)
                    return 2;
                else{
                    var d = (waveHeight*2)/5;
                    d = d < 0.5 ? 0.5:d;
                    return d;
                }
            }
        }

        var legends = {
            getWaveLegende: function(){
                var legende = $('<ul></ul>');
                WAVE_COLORS.reverse().map(color => {
                    legende.append(`
                        <li>
                            <i class="fa fa-square" style="color:${color.bgColor};"></i>
                            ${ color.height[1] === Infinity ? ("plus de "+color.height[0]+"m"):("["+Number.parseFloat(color.height[0]).toFixed(1)+" ; "+Number.parseFloat(color.height[1]).toFixed(1)+"[")}
                        </li>
                    `)
                })
                return legende;
            },
            getWindLegende: function(){
                var legende = $('<ul></ul>');
                WIND_COLORS.reverse().map(color => {
                    legende.append(`
                        <li>
                            <i class="fa fa-square" style="color:${color.bgColor};"></i>
                            ${ color.speed[1] === Infinity ? ("plus de "+color.speed[0]+"knot"):("["+color.speed.join(" ; ")+"[")}
                        </li>
                    `)
                })
                return legende;
            },
            render: function(){
                var waveLegend = legends.getWaveLegende(),
                    windLegend = legends.getWindLegende();
                
                $("#meteolamer-wave-legende .ledende-content").html(waveLegend);
                $("#meteolamer-wind-legende .ledende-content").html(windLegend);

                $('.meteolamer-legende-container .legend-header').click(function(){
                    $(this).toggleClass('active')
                    $('.meteolamer-legende-container .legend-body').toggle('speed')
                })
            }
        }

        context.addPulseMarker = function(latLng){
            var cssStyle = `
                width: 10px;
                height: 10px;
                background: white;
                color: white;
                box-shadow: 0 0 0 white;
                position: relative;
            `;
            var pulseIcon = L.divIcon({
                html: `<span style="${cssStyle}" class="pulse"/>`,
                // empty class name to prevent the default leaflet-div-icon to apply
                className: ''
            });
            var pulseMarker = L.marker(latLng, {
                icon: pulseIcon, 
                zIndexOffset:-1
            }).addTo(context.map);

            return pulseMarker;
        }

        context.addArrow = function(params){
            var arrowData = {
                latlng: L.latLng(params.latLng[0],params.latLng[1]),
                degree: params.degree,
                distance: params.distance
            }
            var arrowOptions = {
                distanceUnit: 'km',
                isWindDegree: true,
                stretchFactor: 1,
                arrowheadLength: 0.1,
                color:params.color
            }

            var arrow = new L.Arrow(arrowData, arrowOptions),
                arrowMarker = null;
            arrow.addTo(context.map);

            if(params.icon){
                var arrowCenter = arrow.getBounds().getCenter();
                arrowMarker = L.marker([arrowCenter.lat, arrowCenter.lng], { 
                    icon:params.icon 
                });
                arrowMarker.addTo(context.map)
            }
            return {arrow:arrow, arrowMarker: arrowMarker};
        }

        context.addDashCircleMarker = function(latLng){
            var dashcircle = L.circle(latLng, {
                dashArray: "20,20",
                dashSpeed: -30,
                radius: 1500,
                fillOpacity: 0.2,
                fillColor:"white",
                color:"white"
            }).addTo(context.map);
            return dashcircle;
        }

        context.removeActiveSpotMarkers = function(){
            if(context.activeSpotmarkers){
                var layers = [
                    context.activeSpotmarkers.pulseMarker,
                    context.activeSpotmarkers.dashCircleMarker
                ];
                if(context.activeSpotmarkers.windWaveArrow){
                    layers = layers.concat([
                        context.activeSpotmarkers.windWaveArrow.arrow,
                        context.activeSpotmarkers.windWaveArrow.arrowMarker
                    ])
                }else{
                    layers = layers.concat([
                        context.activeSpotmarkers.windArrow.arrow,
                        context.activeSpotmarkers.windArrow.arrowMarker,
                        context.activeSpotmarkers.waveArrow.arrow,
                        context.activeSpotmarkers.waveArrow.arrowMarker
                    ])
                }
                layers.forEach(function(layer){
                    context.map.removeLayer(layer)
                })
            }
        }

        context.addActiveSpotMarkers= function(spotData){
            var latLng = spotData.latLng,
                waveDegree = spotData.wave.degree,
                windDegree = spotData.wind.degree;
                windThemeColor = themes.getWindTheme(spotData.wind.speed),
                waveThemeColor = themes.getWaveTheme(spotData.wave.height);

            context.removeActiveSpotMarkers();

            context.activeSpotmarkers = {
                pulseMarker: context.addPulseMarker(latLng),
                dashCircleMarker: context.addDashCircleMarker(latLng)
            }

            if(waveDegree !== windDegree){
                context.activeSpotmarkers.windArrow = context.addArrow({
                    latLng:latLng,
                    degree:windDegree,
                    distance: arrowDistance.getArrowWindDistance(spotData.wind.speed),
                    icon: icon.getWindIcon(spotData.wind),
                    popup: popup.getWindPopup(spotData.wind),
                    color:windThemeColor.bgColor
                })
                context.activeSpotmarkers.waveArrow = context.addArrow({
                    latLng:latLng,
                    degree:waveDegree,
                    distance: arrowDistance.getArrowWaveDistance(spotData.wave.height),
                    icon: icon.getWaveIcon(spotData.wave),
                    popup: popup.getWavePopup(spotData.wave),
                    color:waveThemeColor.bgColor
                })
            }else{
                context.activeSpotmarkers.windWaveArrow = context.addArrow({
                    latLng:latLng,
                    degree:windDegree,
                    distance: 1.5,
                    icon: icon.getWindWaveIcon(spotData.wind, spotData.wave),
                    popup: popup.getWindPopup(spotData.wind),
                    color:waveThemeColor.bgColor
                })
            }
        }

        
        //initialize map
        context.coMap = new CoMap(coMapOptions);
        context.map = context.coMap.getMap();
        if(options.spots){
            context.coMap.addElts(options.spots);
        }
        legends.render();

        return this;
    }
})



