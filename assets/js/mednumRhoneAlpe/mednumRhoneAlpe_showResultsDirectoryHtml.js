
directory.elementPanelHtml = function(params, edit){
	mylog.log("elementPanelHtml hubTerritoires","Params",params);

	var str = '';

	var grayscale = ( ( notNull(params.isInviting) && params.isInviting == true) ? 'grayscale' : '' ) ;
	var tipIsInviting = ( ( notNull(params.isInviting) && params.isInviting == true) ? trad['Wait for confirmation'] : '' ) ;
	var classType=params.collection;
	classType +=(typeof params.category != 'undefined' && notNull(params.category)) ? ' '+params.category : '';
	if(typeof params.type != 'undefined' && notNull(params.type)) 
		classType += ' '+params.type;
	if(typeof params.id == "undefined")
		params.id=params._id.$id;
	var hashElt="#page.type."+params.collection+".id."+params.id;
	var typeElt=(typeof params.type != "undefined") ? params.type : null;
	var settingsTypeElt=directory.getTypeObj(params.collection, typeElt);
	var colorElt=(typeof settingsTypeElt.color != "undefined") ? settingsTypeElt.color : "grey"; 
	var iconElt = (typeof settingsTypeElt.icon != "undefined") ? settingsTypeElt.icon : "circle";

	if(typeof edit  != 'undefined' && notNull(edit))
		 str += this.getAdminToolBar(params);

	//--------------image-----------------------
	var defaultImgDirectory = '<div class="div-img"><i class="fa fa-picture-o fa-5x"></i></div>';
	if('undefined' != typeof directory.costum && notNull(directory.costum)  
                  && typeof directory.costum.results != 'undefined' 
                  && typeof directory.costum.results[params.collection] != 'undefined' 
                  && typeof directory.costum.results[params.collection].defaultImg != 'undefined')
		defaultImgDirectory= '<img class="img-responsive" src="'+assetPath+directory.costum.results[params.type].defaultImg+'" style="margin-top:0px"/>';       
	if('undefined' != typeof params.profilMediumImageUrl && params.profilMediumImageUrl != '')
		defaultImgDirectory= '<img class="img-responsive" src="'+baseUrl+params.profilMediumImageUrl+'" style="margin-top:0px"/>';
	//onload="directory.checkImage(this);"
	//-------------------type------------------------------
	typeElt = (typeof tradCategory[params.type] != "undefined")  ? tradCategory[params.type] : '';

	//-------------------short description------------------------------
	var shortDescription ='';
	shortDescription = (typeof params.shortDescription != "undefined") ? params.shortDescription : '';

		//-------------------social to html------------------------------
	var socialToolsHtml=null
	if(userId != null && userId != '' && typeof params.id != 'undefined' && typeof params.collection != 'undefined') 
	socialToolsHtml = directory.socialToolsHtml(params);

	//----------------------------Address------------------------------
	var thisLocality = '';
	params.streetAddress='', params.postalCode = '', params.city='',params.cityName='',params.mapMarker='';
	if (params.address != null) {
		params.streetAddress= params.address.streetAddress ? params.address.streetAddress : '';
		params.postalCode = params.address.postalCode ? params.address.postalCode : '';
		params.city = params.address.addressLocality ? params.address.addressLocality : '' ;
		if(params.streetAddress != '' || params.postalCode!='' || params.city !='')
		params.mapMarker='<i class="fa fa-map-marker text-green text-bold"></i> ';
	}
	thisLocality = params.mapMarker+' '+params.streetAddress+' '+params.postalCode+' '+params.city;

	//----------------------------Count Contributors followers attendees------------------------------
	params.thisContributorsCounter='',params.thisFollowersCounter='',params.thisAttendeesCounter='';
	params.thisContributorsCounterUnity='',params.thisFollowersCounterUnity='',params.thisAttendeesCounterUnity='';
	params.thisContributorsCounterIcon='',params.thisFollowersCounterIcon='',params.thisAttendeesCounterIcon='';
	if(typeof params.links != "undefined" && typeof params.links.contributors != "undefined" && Object.keys( params.links.contributors).length != 0){
		params.thisContributorsCounter = Object.keys( params.links.contributors).length;
		params.thisContributorsCounterUnity = Object.keys( params.links.contributors).length == 1 ? trad["attendee"] : trad["attendees"];
		params.thisContributorsCounterIcon = '<i class="fa fa-group"></i>';
	}
	if(typeof params.links != "undefined" && typeof params.links.followers != "undefined" && Object.keys( params.links.followers).length != 0){
		params.thisFollowersCounter = Object.keys( params.links.followers).length;
		params.thisFollowersCounterUnity = Object.keys( params.links.followers).length == 1 ? trad["follower"] : trad["followers"];
		params.thisFollowersCounterIcon='<i class="fa fa-link"></i>';
	}
	if(typeof params.links != "undefined" && typeof params.links.attendees != "undefined" && Object.keys( params.links.attendees).length != 0){
		params.thisAttendeesCounter = Object.keys( params.links.attendees).length;
		params.thisAttendeesCounterUnity = Object.keys( params.links.attendees).length == 1 ? trad["attendee"] : trad["attendees"];
		params.thisAttendeesCounterIcon='<i class="fa fa-group"></i>';
	}
	var dateStr="";
	if(typeof params.updated != "undefined" && notNull(params.updated))
  		dateStr += '<div class="dateUpdated dateUpdated-sm date-position">'+directory.showDatetimePost(params.collection, params.id, params.updated)+'</div>';
	else if(typeof params.created != "undefined" && notNull(params.created))
  		dateStr += '<div class="dateUpdated dateUpdated-sm date-position">'+directory.showDatetimePost(params.collection, params.id, params.created)+'</div>';
	var linkAction = ( $.inArray(params.collection, ['poi','classifieds'])>=0 ) ? ' lbh-preview-element' : ' lbh';

	str += 
	    '<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 smartgrid-slide-element">'+
			'<div class="item-slide">'+
					'<div class="entityCenter" style="position: absolute;">'+
						'<a href="'+hashElt+'" class="add2fav lbh"><i class="fa fa-'+iconElt+' bg-'+colorElt+'"></i></a>'+
					'</div>'+
					'<div class="img-back-card">'+
						defaultImgDirectory +
						'<div class="text-wrap searchEntity">'+
							'<h4 class="entityName">'+
								'<a href="'+hashElt+'" class="uppercase text-orange '+linkAction+'">'+params.name+'</a>'+
							'</h4>';
							//'<h6 class="text-bold entityType">'+typeElt+'</h6>'+
							'<div class="deal-data text-center">';
								if (typeElt != '') {
	str +=							'<div class="text-center entityType"  style="width:100%">'+	
										'<span class="text-white">'+typeElt+'</span>'+
									'</div>';
								}
								if (typeof params.typePlace != 'undefined') {
	str +=							'<div class="text-center entityType"  style="width:100%">'+	
										'<span style="font-size:14px;" class="text-white">'+params.typePlace+'</span>'+
									'</div>';
								}
								if (thisLocality != '   ') {
	str +=							'<div class=" no-padding text-center">'+
										'<a href="'+hashElt+'" class="text-white locality '+linkAction+'"> '+thisLocality+'</a>'+
									'</div>';
								}
								if ( params.collection == "projects" && params.categ != "undefined" && params.subcateg != "undefined" ) {
									subsubcateg = (typeof params.subsubcateg != "undefined") ? ">"+params.subsubcateg : "";
	str +=							'<div class="entityCateg">'+
										'<p class="text-white">'+params.categ+'>'+params.subcateg+subsubcateg+'</p>';
									'</div>';
								}
	str +=						'</div>'+
							'<div class="desc"></div>'+
						'</div>'+
					'</div>'+
					//hover
					'<div class="slide-hover">'+
						'<div class="text-wrap">'+
							'<h4 class="entityName">'+
								'<a href="'+hashElt+'" class="text-orange'+linkAction+'">'+params.name+'</a>'+
							'</h4>'+
							'<h5 class="deal-data text-center">';
								if (typeElt != '') {
	str +=						'<div class="text-center entityType"  style="width:100%">'+	
									'<span class="text-white">'+typeElt+'</span>'+
								'</div>';
								}

								if ( params.collection == "projects" && params.categ != "undefined" && params.subcateg != "undefined" ) {
								subsubcateg = (typeof params.subsubcateg != "undefined") ? ">"+params.subsubcateg : "";
	str +=						'<div class="entityCateg">'+
									'<span class="text-white">'+params.categ+'>'+params.subcateg+subsubcateg+'</span>'+
								'</div>';
								}

								if (thisLocality != '   ') {
	str +=							'<div class="text-center">'+
										'<a href="'+hashElt+'" class="text-white locality '+linkAction+'"> '+thisLocality+'</a>'+
									'</div>';
								}
	str +=					'</h5>'+
							'<hr>'+
							'<p class="p-short">'+shortDescription+'</p>'+
							'<ul class="tag-list">'+
							  params.tagsLbl+
							'</ul>';
							
	str +=				    '<div class="counter text-bold">';
								if (params.thisContributorsCounter != '') {
	str +=							//'<li class="small"><span class="count-icon"></span> <span class="count-number"></span><br><span class="count-label"></span></li>';
									'<div class="list-dash">'+
							            '<h3 class="pull-left">'+params.thisContributorsCounterIcon+'</h3>'+
							            '<h4 class="list-dash-item">'+params.thisContributorsCounter+'</h4>'+
							            '<p class="list-dash-text">'+params.thisContributorsCounterUnity+'</p>'+
							        '</div>';
								}
								if (params.thisFollowersCounter != '') {
	str +=							//'<li class="small"><span class="count-icon">'+params.thisFollowersCounterIcon+'</span> <span class="count-number">'+params.thisFollowersCounter+'</span><br><span class="count-label">'+params.thisFollowersCounterUnity+'</span></li>';
									'<div class="list-dash">'+
							            '<h3 class="pull-left">'+params.thisFollowersCounterIcon+'</h3>'+
							            '<h4 class="list-dash-item">'+params.thisFollowersCounter+'</h4>'+
							            '<p class="list-dash-text">'+params.thisFollowersCounterUnity+'</p>'+
							        '</div>';
								}
								if (params.thisAttendeesCounter != '') {
	str +=							//'<li class="small"><span class="count-icon">'+params.thisAttendeesCounterIcon+'</span> <span class="count-number">'+params.thisAttendeesCounter+'</span><br><span class="count-label">'+params.thisAttendeesCounterUnity+'</span></li>';
									'<div class="list-dash">'+
							            '<h3 class="pull-left">'+params.thisAttendeesCounterIcon+'</h3>'+
							            '<h4 class="list-dash-item">'+params.thisAttendeesCounter+'</h4>'+
							            '<p class="list-dash-text">'+params.thisAttendeesCounter+'</p>'+
							        '</div>';
								}

	str +=					'</div>'+
							'<div class="desc">'+
								  socialToolsHtml+		
							'</div>'+
						'</div>'+
					'</div>'+
			'</div>'+
		'</div>';
	return str;
}

