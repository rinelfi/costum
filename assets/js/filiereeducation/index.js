$(".menuRight_header_title").text("Résultat");
$("#input_name_filtermapContent").attr("placeholder", "Filtrer par nom...");

costum.coeurNumerique = {
	badges : {
		afterSave : function(data){
			if(dyFObj.editMode){
				if($.inArray(data.map.category, ["domainAction"]) >= 0){
					oldName=costum.badges[data.map.category][data.id].name;
					if(data.map.name!=oldName){
						if(typeof costum.badges[data.map.category][data.id].parent != "undefined"){
							parentNameB=costum.badges[data.map.category][Object.keys(costum.badges[data.map.category][data.id].parent)[0]].name;
							costum.lists[data.map.category][parentNameB].splice(costum.lists[data.map.category][parentNameB].indexOf(oldName), 1,data.map.name);
						}else{
							costum.lists[data.map.category][data.map.name]=costum.lists[data.map.category][oldName];
							delete costum.lists[data.map.category][oldName];
						}
						costum.badges[data.map.category][data.id].name=data.map.name;
					}
				}
			}
			dyFObj.commonAfterSave();
		}
	},

	organizations : {
		formData : function(data){
			//if(dyFObj.editMode){
			$.each(data, function(e, v){
				if(typeof costum.lists[e] != "undefined"){
					if(notNull(v)){
						
						if(typeof data.tags == "undefined" || !notNull(data.tags)) data.tags=[];
						if(typeof v == "string")
							data.tags.push(v);
						else{
							$.each(v, function(i,tag){
								data.tags.push(tag);	
							});
						}
					}
					delete data[e];
				}
			});
			if(typeof data.mainTag != "undefined"){
				data.tags.push(data.mainTag);
				//delete data.mainTag;
			}
			return data;
		}
	}
}